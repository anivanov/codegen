#!/bin/bash
#SBATCH --job-name=daint_search
#SBATCH --ntasks-per-node=32
#SBATCH --cpus-per-task=1
#SBATCH --time=0-00:30:00
#SBATCH --nodes=2
#SBATCH --account=g34
#SBATCH --output=daint_search-%j.out
#SBATCH --error=daint_search-%j.err
#SBATCH --constraint=mc
#SBATCH --mem=0
#SBATCH --exclusive
set -x

rm -rf generated_*
rm -rf core.*

srun --export=ALL,OMP_NUM_THREADS=1 --cpu-bind=v,cores --mem-bind=v,local python3 distributed_search.py -s beam -l --beams 100 --sample--beams 100 --sample