#pragma once

#if defined(__AVX__)

#include <immintrin.h>
#include "sleef.h"

#define f32x8 __m256
#define f64x4 __m256d

inline __m256 allreduce_max(__m256 x) {
    __m256 y = _mm256_permute2f128_ps(x, x, 1);
    __m256 m1 = _mm256_max_ps(x, y);
    __m256 m2 = _mm256_permute_ps(m1, _MM_SHUFFLE(2, 3, 0, 1));
    __m256 m3 = _mm256_max_ps(m1, m2);
    __m256 m4 = _mm256_permute_ps(m3, _MM_SHUFFLE(1, 0, 3, 2));
    __m256 result = _mm256_max_ps(m3, m4);
    return result;
}

inline f32 reduce_add_f32x8(f32x8 x) {
  __m128 y = _mm_add_ps(_mm256_castps256_ps128(x), _mm256_extractf128_ps(x, 1));
  y = _mm_hadd_ps(y, y);
  y = _mm_hadd_ps(y, y);
  return _mm_cvtss_f32(y);
}

inline f32 reduce_max_f32x8(f32x8 x) {
  __m128 y = _mm_max_ps(_mm256_castps256_ps128(x), _mm256_extractf128_ps(x, 1));
  y = _mm_max_ps(y, _mm_movehl_ps(y, y));
  y = _mm_max_ps(y, _mm_shuffle_ps(y, y, 1));
  return _mm_cvtss_f32(y);
}

inline f32 reduce_min_f32x8(f32x8 x) {
  __m128 y = _mm_min_ps(_mm256_castps256_ps128(x), _mm256_extractf128_ps(x, 1));
  y = _mm_min_ps(y, _mm_movehl_ps(y, y));
  y = _mm_min_ps(y, _mm_shuffle_ps(y, y, 1));
  return _mm_cvtss_f32(y);
}

inline f32x8 vector_add_f32x8(f32x8 x, f32x8 y) {
  return _mm256_add_ps(x, y);
}

inline f32x8 vector_sub_f32x8(f32x8 x, f32x8 y) {
  return _mm256_sub_ps(x, y);
}

inline f32x8 vector_mul_f32x8(f32x8 x, f32x8 y) {
  return _mm256_mul_ps(x, y);
}

inline f32x8 vector_div_f32x8(f32x8 x, f32x8 y) {
  return _mm256_div_ps(x, y);
}

inline f32x8 vector_fmadd_f32x8(f32x8 a, f32x8 b, f32x8 c) {
  return _mm256_fmadd_ps(a, b, c);
}

inline f32x8 vector_fmsub_f32x8(f32x8 a, f32x8 b, f32x8 c) {
  return _mm256_fmsub_ps(a, b, c);
}

inline f32x8 vector_expf_f32x8(f32x8 x) {
  return Sleef_expf8_u10(x);
}

inline f32x8 vector_logf_f32x8(f32x8 x) {
  return Sleef_logf8_u10(x);
}

inline f32x8 vector_sqrtf_f32x8(f32x8 x) {
  return _mm256_sqrt_ps(x);
}

inline f32x8 vector_rsqrtf_f32x4(f32x8 x) {
  return _mm256_rsqrt_ps(x);
}

inline f32x8 vector_fmaxf_f32x8(f32x8 x, f32x8 y) {
  return _mm256_max_ps(x, y);
}

inline f32x8 vector_fminf_f32x8(f32x8 x, f32x8 y) {
  return _mm256_min_ps(x, y);
}

inline f32x8 vector_load_f32x8(const f32* ptr) {
  return _mm256_loadu_ps(ptr);
}

inline void vector_store_f32x8(f32* ptr, f32x8 x) {
  _mm256_storeu_ps(ptr, x);
}

inline f32x8 vector_bcast_f32x8(f32 x) {
  return _mm256_set1_ps(x);
}

inline f32 vec_to_scalar_s256(f32x8 x, int idx){
    union {
      f32x8 m;
      f32 a[8];
    } vector;
    vector.m = x;
    return vector.a[idx];
}

inline double vec_to_scalar_d256(f64x4 x, int idx) {
  union {
    f64x4 m;
    double a[4];
  } vector;
  vector.m = x;
  return vector.a[idx];
}

#endif // __AVX__

#if defined(__AVX512F__)

inline f32 vec_to_scalar_s512(f32x16 x, int idx) {
  union {
    f32x16 m;
    float a[16];
  } vector;
  vector.m = x;
  return vector.a[idx];
}

inline f32 vec_to_scalar_d512(f64x8 x, int idx) {
  union {
    f64x8 m;
    double a[8];
  } vector;
  vector.m = x;
  return vector.a[idx];
}

#endif // __AVX512F__

#if defined(__AVX512FP16__)

#define f16 _Float16
#define f16x16 __m256h
#define f16x32 __m512h
#define f16x32 __m512h
#define f32x16 __m512
#define f64x8 __m512d

inline f16 reduce_add_f16x32(f16x32 x) {
  return _mm512_reduce_add_ph(x);
}

inline f16x32 vector_add_f16x32(f16x32 x, f16x32 y) {
  return _mm512_add_ph(x, y);
}

inline f16x32 vector_sub_f16x32(f16x32 x, f16x32 y) {
  return _mm512_sub_ph(x, y);
}

inline f16x32 vector_mul_f16x32(f16x32 x, f16x32 y) {
  return _mm512_mul_ph(x, y);
}

inline f16x32 vector_div_f16x32(f16x32 x, f16x32 y) {
  return _mm512_div_ph(x, y);
}

inline f16x32 vector_fmadd_f16x32(f16x32 a, f16x32 b, f16x32 c) {
  return _mm512_fmadd_ph(a, b, c);
}

inline f16x16 vector_load_f16x16(const f16* ptr) {
  return _mm256_loadu_ph(ptr);
}

inline void vector_store_f16x16(f16* ptr, f16x16 x) {
  _mm256_storeu_ph(ptr, x);
}

inline f16x16 vector_bcast_f16x16(f16 x) {
  return _mm256_set1_ph(x);
}

inline f16x32 vector_load_f16x32(const f16* ptr) {
  return _mm512_loadu_ph(ptr);
}

inline void vector_store_f16x32(f16* ptr, f16x32 x) {
  _mm512_storeu_ph(ptr, x);
}

inline f16x32 vector_bcast_f16x32(f16 x) {
  return _mm512_set1_ph(x);
}

inline f16 vec_to_scalar_h256(f16x16 x, int idx) {
  union {
    f16x16 m;
    f16 a[16];
  } vector;
  vector.m = x;
  return vector.a[idx];
}

inline f16 vec_to_scalar_h512(f16x32 x, int idx) {
  union {
    f16x32 m;
    f16 a[32];
  } vector;
  vector.m = x;
  return vector.a[idx];
}

#endif // __AVX512FP16__
