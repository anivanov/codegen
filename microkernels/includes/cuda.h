#pragma once

#if __CUDACC__

#include <cuda.h>
#include <cuda_fp16.h>
#include <mma.h>
using namespace nvcuda;  // wmma

#define f16 __half

#endif // __CUDACC__

#if __CUDACC__
    #define HOST_DEVICE __host__ __device__
#else
    #define HOST_DEVICE
#endif // __CUDACC__