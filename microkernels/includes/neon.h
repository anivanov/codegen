#pragma once

#if defined(__ARM_NEON)

#include <arm_neon.h> // 128-bit neon
#include "sleef.h"

#define f32x4 float32x4_t
#define f32x2 float32x2_t

inline f32 reduce_add_f32x4(f32x4 x) {
  f32x2 y = vadd_f32(vget_low_f32(x), vget_high_f32(x));
  y = vpadd_f32(y, y);
  return vget_lane_f32(y, 0);
}

inline f32 reduce_max_f32x4(f32x4 x) {
  f32x2 y = vpmax_f32(vget_low_f32(x), vget_high_f32(x));
  y = vpmax_f32(y, y);
  return vget_lane_f32(y, 0);
}

inline f32 reduce_min_f32x4(f32x4 x) {
  f32x2 y = vpmin_f32(vget_low_f32(x), vget_high_f32(x));
  y = vpmin_f32(y, y);
  return vget_lane_f32(y, 0);
}

inline f32x4 vector_load_f32x4(const f32* ptr) {
  return vld1q_f32(ptr);
}

inline void vector_store_f32x4(f32* ptr, f32x4 x) {
  vst1q_f32(ptr, x);
}

inline f32x4 vector_bcast_f32x4(f32 x) {
  return vdupq_n_f32(x);
}

inline f32x4 vector_add_f32x4(f32x4 x, f32x4 y) {
  return vaddq_f32(x, y);
}

inline f32x4 vector_sub_f32x4(f32x4 x, f32x4 y) {
  return vsubq_f32(x, y);
}

inline f32x4 vector_mul_f32x4(f32x4 x, f32x4 y) {
  return vmulq_f32(x, y);
}

inline f32x4 vector_div_f32x4(f32x4 x, f32x4 y) {
  return vdivq_f32(x, y);
}

inline f32x4 vector_fmadd_f32x4(f32x4 a, f32x4 b, f32x4 c) {
  return vfmaq_f32(c, a, b);
}

inline f32x4 vector_fmsub_f32x4(f32x4 a, f32x4 b, f32x4 c) {
  return -vfmsq_f32(c, a, b); // it is defined as "c - a * b" so we need to negate it
}

inline f32x4 vector_expf_f32x4(f32x4 x) {
  return Sleef_expf4_u10(x);
}

inline f32x4 vector_logf_f32x4(f32x4 x) {
  return Sleef_logf4_u10(x);
}

inline f32x4 vector_sqrtf_f32x4(f32x4 x) {
  return vsqrtq_f32(x);
}

inline f32x4 vector_rsqrtf_f32x4(f32x4 x) {
  float32x4_t estimate = vrsqrteq_f32(x);
  estimate = vrsqrtsq_f32(x, estimate);
  estimate = vrsqrtsq_f32(x, estimate);
  return vrsqrteq_f32(x);
}

inline f32x4 vector_fmaxf_f32x4(f32x4 x, f32x4 y) {
  return vmaxq_f32(x, y);
}

inline f32x4 vector_fminf_f32x4(f32x4 x, f32x4 y) {
  return vminq_f32(x, y);
}

#endif // __ARM_NEON
