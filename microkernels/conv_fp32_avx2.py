from kernels import *
import torch


def conv_handoptimized(N, M, C, H_in, W_in, K):
    
    H = pooling_out_dim(input=H_in, kernel=K, stride=1, pad=0, dilation=1)
    W = pooling_out_dim(input=W_in, kernel=K, stride=1, pad=0, dilation=1)
    
    input = np.random.uniform(-10, 10, (N, C, H_in, W_in)).astype(np.float32)
    weight = np.random.uniform(-10, 10, (M, C, K, K)).astype(np.float32)
    tinput = torch.from_numpy(input)
    tweight = torch.from_numpy(weight)

    def init_src():
        input[:] = np.random.uniform(-10, 10, (N, C, H_in, W_in)).astype(np.float32)
        weight[:] = np.random.uniform(-10, 10, (M, C, K, K)).astype(np.float32)
      
    pt_mean, pt_std, pt_builtin_runtime = time_prof(10, lambda: torch.nn.functional.conv2d(tinput, tweight), init_src)
    print(f"Pytorch conv2d time [ms]: avg {pt_mean * 1e3:.6f} std {pt_std * 1e3:.6f} reps {len(pt_builtin_runtime)}")  

    toutput = torch.nn.functional.conv2d(tinput, tweight)
    output = toutput.numpy()
    
    def eval_func(program, ignore=True):
        if ignore:
            return
        print(program.text())
        time = test_native_code(
            program,
            {'input': input, 'weight': weight, 'output': output},
            min_reps=10,
            remove_tmp_files=False,
            silent=False,
        )[3:]
        print(f"My conv2d [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")
    
    program_text = f"""
        Name: conv2d
        In: input, weight
        Out: output
        Declarations:
            input f32 [{N}, {C}, {H_in}, {W_in}] heap
            weight f32 [{M}, {C}, {K}, {K}] heap
            output f32 [{N}, {M}, {H}, {W}] heap
        Code:
        {N} {M} {H} {W} {C} {K} {K} output[{{0}}, {{1}}, {{2}}, {{3}}] += input[{{0}}, {{4}}, {{2}} + {{5}}, {{3}} + {{6}}] * weight[{{1}}, {{4}}, {{5}}, {{6}}]
    """
    
    program = parse_program(program_text); eval_func(program, ignore=False)
    
    # swap_nested_scopes(program, 'output#3'); eval_func(program)
    # swap_nested_scopes(program, 'output#2'); eval_func(program)
    # parallelize_scope(program, 'output#4'); eval_func(program, ignore=False)
    
    swap_nested_scopes(program, 'output#3'); eval_func(program)
    swap_nested_scopes(program, 'output#2'); eval_func(program)
    swap_nested_scopes(program, 'output#5'); eval_func(program)
    swap_nested_scopes(program, 'output#6'); eval_func(program)
    parallelize_scope(program, 'output#6'); eval_func(program, ignore=False)
    

if __name__ == '__main__':
    conv_handoptimized(N=8, M=10, C=3, H_in=512, W_in=512, K=5)
