// tvm target: c -keys=cpu 
#define TVM_EXPORTS
#include "tvm/runtime/c_runtime_api.h"
#include "tvm/runtime/c_backend_api.h"
#include <math.h>
#ifdef __cplusplus
extern "C"
#endif
TVM_DLL int32_t tvmgen_default_fused_nn_leaky_relu(double* p0, double* T_leaky_relu) {
  for (int32_t ax0 = 0; ax0 < 16; ++ax0) {
    for (int32_t ax1_inner = 0; ax1_inner < 16; ++ax1_inner) {
      int32_t cse_var_1 = ((ax0 * 16) + ax1_inner);
      T_leaky_relu[cse_var_1] = ((0.000000e+00 < p0[cse_var_1]) ? p0[cse_var_1] : (p0[cse_var_1] * 5.000000e-02));
    }
  }
  return 0;
}

#ifdef __cplusplus
extern "C"
#endif
TVM_DLL int32_t tvmgen_default___tvm_main__(double* input0_buffer_var, double* output_buffer_var) {
  if (tvmgen_default_fused_nn_leaky_relu(input0_buffer_var, output_buffer_var) != 0 ) return -1;
  return 0;
}

