// tvm target: c -keys=cpu 
#define TVM_EXPORTS
#include "tvm/runtime/c_runtime_api.h"
#include "tvm/runtime/c_backend_api.h"
#include <math.h>

#ifdef __cplusplus
extern "C" {
#endif
static const double __attribute__((section(".rodata.tvm"), aligned(16))) fused_transpose_constant[16] = {
    0x1p-1     , 0x1p-1     , 0x1p-1     , 0x1p-1     , 
    0x1p-1     , 0x1p-1     , 0x1p-1     , 0x1p-1     , 
    0x1p-1     , 0x1p-1     , 0x1p-1     , 0x1p-1     , 
    0x1p-1     , 0x1p-1     , 0x1p-1     , 0x1p-1     
};
#ifdef __cplusplus
}  // extern "C"
#endif
#ifdef __cplusplus
extern "C"
#endif
TVM_DLL int32_t tvmgen_default_fused_nn_contrib_dense_pack(double* p0, double* p1, double* compute) {
  for (int32_t x_outer_y_outer_fused = 0; x_outer_y_outer_fused < 4; ++x_outer_y_outer_fused) {
    double compute_global[64];
    for (int32_t x_c_init = 0; x_c_init < 8; ++x_c_init) {
      compute_global[x_c_init] = 0.000000e+00;
    }
    for (int32_t x_c_init_1 = 0; x_c_init_1 < 8; ++x_c_init_1) {
      compute_global[(x_c_init_1 + 8)] = 0.000000e+00;
    }
    for (int32_t x_c_init_2 = 0; x_c_init_2 < 8; ++x_c_init_2) {
      compute_global[(x_c_init_2 + 16)] = 0.000000e+00;
    }
    for (int32_t x_c_init_3 = 0; x_c_init_3 < 8; ++x_c_init_3) {
      compute_global[(x_c_init_3 + 24)] = 0.000000e+00;
    }
    for (int32_t x_c_init_4 = 0; x_c_init_4 < 8; ++x_c_init_4) {
      compute_global[(x_c_init_4 + 32)] = 0.000000e+00;
    }
    for (int32_t x_c_init_5 = 0; x_c_init_5 < 8; ++x_c_init_5) {
      compute_global[(x_c_init_5 + 40)] = 0.000000e+00;
    }
    for (int32_t x_c_init_6 = 0; x_c_init_6 < 8; ++x_c_init_6) {
      compute_global[(x_c_init_6 + 48)] = 0.000000e+00;
    }
    for (int32_t x_c_init_7 = 0; x_c_init_7 < 8; ++x_c_init_7) {
      compute_global[(x_c_init_7 + 56)] = 0.000000e+00;
    }
    for (int32_t k_outer = 0; k_outer < 16; ++k_outer) {
      for (int32_t x_c = 0; x_c < 8; ++x_c) {
        compute_global[x_c] = (compute_global[x_c] + (p0[(((x_outer_y_outer_fused & 1) * 128) + k_outer)] * p1[((((x_outer_y_outer_fused >> 1) * 128) + (k_outer * 8)) + x_c)]));
      }
      for (int32_t x_c_1 = 0; x_c_1 < 8; ++x_c_1) {
        int32_t cse_var_1 = (x_c_1 + 8);
        compute_global[cse_var_1] = (compute_global[cse_var_1] + (p0[((((x_outer_y_outer_fused & 1) * 128) + k_outer) + 16)] * p1[((((x_outer_y_outer_fused >> 1) * 128) + (k_outer * 8)) + x_c_1)]));
      }
      for (int32_t x_c_2 = 0; x_c_2 < 8; ++x_c_2) {
        int32_t cse_var_2 = (x_c_2 + 16);
        compute_global[cse_var_2] = (compute_global[cse_var_2] + (p0[((((x_outer_y_outer_fused & 1) * 128) + k_outer) + 32)] * p1[((((x_outer_y_outer_fused >> 1) * 128) + (k_outer * 8)) + x_c_2)]));
      }
      for (int32_t x_c_3 = 0; x_c_3 < 8; ++x_c_3) {
        int32_t cse_var_3 = (x_c_3 + 24);
        compute_global[cse_var_3] = (compute_global[cse_var_3] + (p0[((((x_outer_y_outer_fused & 1) * 128) + k_outer) + 48)] * p1[((((x_outer_y_outer_fused >> 1) * 128) + (k_outer * 8)) + x_c_3)]));
      }
      for (int32_t x_c_4 = 0; x_c_4 < 8; ++x_c_4) {
        int32_t cse_var_4 = (x_c_4 + 32);
        compute_global[cse_var_4] = (compute_global[cse_var_4] + (p0[((((x_outer_y_outer_fused & 1) * 128) + k_outer) + 64)] * p1[((((x_outer_y_outer_fused >> 1) * 128) + (k_outer * 8)) + x_c_4)]));
      }
      for (int32_t x_c_5 = 0; x_c_5 < 8; ++x_c_5) {
        int32_t cse_var_5 = (x_c_5 + 40);
        compute_global[cse_var_5] = (compute_global[cse_var_5] + (p0[((((x_outer_y_outer_fused & 1) * 128) + k_outer) + 80)] * p1[((((x_outer_y_outer_fused >> 1) * 128) + (k_outer * 8)) + x_c_5)]));
      }
      for (int32_t x_c_6 = 0; x_c_6 < 8; ++x_c_6) {
        int32_t cse_var_6 = (x_c_6 + 48);
        compute_global[cse_var_6] = (compute_global[cse_var_6] + (p0[((((x_outer_y_outer_fused & 1) * 128) + k_outer) + 96)] * p1[((((x_outer_y_outer_fused >> 1) * 128) + (k_outer * 8)) + x_c_6)]));
      }
      for (int32_t x_c_7 = 0; x_c_7 < 8; ++x_c_7) {
        int32_t cse_var_7 = (x_c_7 + 56);
        compute_global[cse_var_7] = (compute_global[cse_var_7] + (p0[((((x_outer_y_outer_fused & 1) * 128) + k_outer) + 112)] * p1[((((x_outer_y_outer_fused >> 1) * 128) + (k_outer * 8)) + x_c_7)]));
      }
    }
    for (int32_t x_inner_inner = 0; x_inner_inner < 8; ++x_inner_inner) {
      compute[((((x_outer_y_outer_fused & 1) * 128) + ((x_outer_y_outer_fused >> 1) * 8)) + x_inner_inner)] = compute_global[x_inner_inner];
    }
    for (int32_t x_inner_inner_1 = 0; x_inner_inner_1 < 8; ++x_inner_inner_1) {
      compute[(((((x_outer_y_outer_fused & 1) * 128) + ((x_outer_y_outer_fused >> 1) * 8)) + x_inner_inner_1) + 16)] = compute_global[(x_inner_inner_1 + 8)];
    }
    for (int32_t x_inner_inner_2 = 0; x_inner_inner_2 < 8; ++x_inner_inner_2) {
      compute[(((((x_outer_y_outer_fused & 1) * 128) + ((x_outer_y_outer_fused >> 1) * 8)) + x_inner_inner_2) + 32)] = compute_global[(x_inner_inner_2 + 16)];
    }
    for (int32_t x_inner_inner_3 = 0; x_inner_inner_3 < 8; ++x_inner_inner_3) {
      compute[(((((x_outer_y_outer_fused & 1) * 128) + ((x_outer_y_outer_fused >> 1) * 8)) + x_inner_inner_3) + 48)] = compute_global[(x_inner_inner_3 + 24)];
    }
    for (int32_t x_inner_inner_4 = 0; x_inner_inner_4 < 8; ++x_inner_inner_4) {
      compute[(((((x_outer_y_outer_fused & 1) * 128) + ((x_outer_y_outer_fused >> 1) * 8)) + x_inner_inner_4) + 64)] = compute_global[(x_inner_inner_4 + 32)];
    }
    for (int32_t x_inner_inner_5 = 0; x_inner_inner_5 < 8; ++x_inner_inner_5) {
      compute[(((((x_outer_y_outer_fused & 1) * 128) + ((x_outer_y_outer_fused >> 1) * 8)) + x_inner_inner_5) + 80)] = compute_global[(x_inner_inner_5 + 40)];
    }
    for (int32_t x_inner_inner_6 = 0; x_inner_inner_6 < 8; ++x_inner_inner_6) {
      compute[(((((x_outer_y_outer_fused & 1) * 128) + ((x_outer_y_outer_fused >> 1) * 8)) + x_inner_inner_6) + 96)] = compute_global[(x_inner_inner_6 + 48)];
    }
    for (int32_t x_inner_inner_7 = 0; x_inner_inner_7 < 8; ++x_inner_inner_7) {
      compute[(((((x_outer_y_outer_fused & 1) * 128) + ((x_outer_y_outer_fused >> 1) * 8)) + x_inner_inner_7) + 112)] = compute_global[(x_inner_inner_7 + 56)];
    }
  }
  return 0;
}

#ifdef __cplusplus
extern "C"
#endif
TVM_DLL int32_t tvmgen_default_fused_transpose_multiply_layout_transform(double* p0, double* T_layout_trans) {
  for (int32_t ax0_ax1_fused = 0; ax0_ax1_fused < 32; ++ax0_ax1_fused) {
    for (int32_t ax2_inner = 0; ax2_inner < 8; ++ax2_inner) {
      int32_t cse_var_1 = (ax0_ax1_fused & 15);
      T_layout_trans[((ax0_ax1_fused * 8) + ax2_inner)] = (p0[(((cse_var_1 * 16) + ((ax0_ax1_fused >> 4) * 8)) + ax2_inner)] * ((double*)fused_transpose_constant)[cse_var_1]);
    }
  }
  return 0;
}

#ifdef __cplusplus
extern "C"
#endif
TVM_DLL int32_t tvmgen_default___tvm_main__(double* input0_buffer_var, double* input1_buffer_var, double* output_buffer_var) {
  void* sid_2 = TVMBackendAllocWorkspace(1, 0, (uint64_t)2048, 0, 8);
  if (sid_2 == NULL) {
    return -1;
  }
  if (tvmgen_default_fused_transpose_multiply_layout_transform(input1_buffer_var, sid_2) != 0 ) return -1;
  if (tvmgen_default_fused_nn_contrib_dense_pack(input0_buffer_var, sid_2, output_buffer_var) != 0 ) return -1;
  if (TVMBackendFreeWorkspace(1, 0, sid_2) != 0) {
    return -1;
  }
  return 0;
}

