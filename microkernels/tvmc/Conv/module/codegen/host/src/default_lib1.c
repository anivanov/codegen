// tvm target: c -keys=cpu 
#define TVM_EXPORTS
#include "tvm/runtime/c_runtime_api.h"
#include "tvm/runtime/c_backend_api.h"
#include <math.h>
#ifdef __cplusplus
extern "C"
#endif
TVM_DLL int32_t tvmgen_default_fused_layout_transform(double* p0, double* T_layout_trans) {
  for (int32_t ax0_ax1_fused_ax2_fused = 0; ax0_ax1_fused_ax2_fused < 10; ++ax0_ax1_fused_ax2_fused) {
    for (int32_t ax3 = 0; ax3 < 10; ++ax3) {
      for (int32_t ax4_inner = 0; ax4_inner < 4; ++ax4_inner) {
        T_layout_trans[(((ax0_ax1_fused_ax2_fused * 40) + (ax3 * 4)) + ax4_inner)] = p0[(((ax4_inner * 100) + (ax0_ax1_fused_ax2_fused * 10)) + ax3)];
      }
    }
  }
  return 0;
}

#ifdef __cplusplus
extern "C"
#endif
TVM_DLL int32_t tvmgen_default_fused_layout_transform_1(double* p0, double* T_layout_trans) {
  for (int32_t ax0_ax1_fused_ax2_fused = 0; ax0_ax1_fused_ax2_fused < 3; ++ax0_ax1_fused_ax2_fused) {
    for (int32_t ax3 = 0; ax3 < 3; ++ax3) {
      for (int32_t ax4 = 0; ax4 < 4; ++ax4) {
        for (int32_t ax5_inner = 0; ax5_inner < 4; ++ax5_inner) {
          T_layout_trans[((((ax0_ax1_fused_ax2_fused * 48) + (ax3 * 16)) + (ax4 * 4)) + ax5_inner)] = p0[((((ax5_inner * 36) + (ax4 * 9)) + (ax0_ax1_fused_ax2_fused * 3)) + ax3)];
        }
      }
    }
  }
  return 0;
}

#ifdef __cplusplus
extern "C"
#endif
TVM_DLL int32_t tvmgen_default_fused_layout_transform_2(double* p0, double* T_layout_trans) {
  for (int32_t ax0_ax1_fused = 0; ax0_ax1_fused < 4; ++ax0_ax1_fused) {
    for (int32_t ax2 = 0; ax2 < 8; ++ax2) {
      for (int32_t ax3_inner = 0; ax3_inner < 8; ++ax3_inner) {
        T_layout_trans[(((ax0_ax1_fused * 64) + (ax2 * 8)) + ax3_inner)] = p0[(((ax2 * 32) + (ax3_inner * 4)) + ax0_ax1_fused)];
      }
    }
  }
  return 0;
}

#ifdef __cplusplus
extern "C"
#endif
TVM_DLL int32_t tvmgen_default_fused_nn_contrib_conv2d_NCHWc(double* p0, double* p1, double* conv2d_NCHWc) {
  for (int32_t n_oc_chunk_fused_oh_fused = 0; n_oc_chunk_fused_oh_fused < 8; ++n_oc_chunk_fused_oh_fused) {
    double conv2d_NCHWc_global[32];
    for (int32_t oc_block_c_init = 0; oc_block_c_init < 4; ++oc_block_c_init) {
      conv2d_NCHWc_global[oc_block_c_init] = 0.000000e+00;
    }
    for (int32_t oc_block_c_init_1 = 0; oc_block_c_init_1 < 4; ++oc_block_c_init_1) {
      conv2d_NCHWc_global[(oc_block_c_init_1 + 4)] = 0.000000e+00;
    }
    for (int32_t oc_block_c_init_2 = 0; oc_block_c_init_2 < 4; ++oc_block_c_init_2) {
      conv2d_NCHWc_global[(oc_block_c_init_2 + 8)] = 0.000000e+00;
    }
    for (int32_t oc_block_c_init_3 = 0; oc_block_c_init_3 < 4; ++oc_block_c_init_3) {
      conv2d_NCHWc_global[(oc_block_c_init_3 + 12)] = 0.000000e+00;
    }
    for (int32_t oc_block_c_init_4 = 0; oc_block_c_init_4 < 4; ++oc_block_c_init_4) {
      conv2d_NCHWc_global[(oc_block_c_init_4 + 16)] = 0.000000e+00;
    }
    for (int32_t oc_block_c_init_5 = 0; oc_block_c_init_5 < 4; ++oc_block_c_init_5) {
      conv2d_NCHWc_global[(oc_block_c_init_5 + 20)] = 0.000000e+00;
    }
    for (int32_t oc_block_c_init_6 = 0; oc_block_c_init_6 < 4; ++oc_block_c_init_6) {
      conv2d_NCHWc_global[(oc_block_c_init_6 + 24)] = 0.000000e+00;
    }
    for (int32_t oc_block_c_init_7 = 0; oc_block_c_init_7 < 4; ++oc_block_c_init_7) {
      conv2d_NCHWc_global[(oc_block_c_init_7 + 28)] = 0.000000e+00;
    }
    for (int32_t kh = 0; kh < 3; ++kh) {
      for (int32_t kw = 0; kw < 3; ++kw) {
        for (int32_t ic_inner = 0; ic_inner < 4; ++ic_inner) {
          for (int32_t oc_block_c = 0; oc_block_c < 4; ++oc_block_c) {
            conv2d_NCHWc_global[oc_block_c] = (conv2d_NCHWc_global[oc_block_c] + (p0[((((kh * 40) + (n_oc_chunk_fused_oh_fused * 40)) + (kw * 4)) + ic_inner)] * p1[((((kh * 48) + (kw * 16)) + (ic_inner * 4)) + oc_block_c)]));
          }
          for (int32_t oc_block_c_1 = 0; oc_block_c_1 < 4; ++oc_block_c_1) {
            int32_t cse_var_1 = (oc_block_c_1 + 4);
            conv2d_NCHWc_global[cse_var_1] = (conv2d_NCHWc_global[cse_var_1] + (p0[(((((kh * 40) + (n_oc_chunk_fused_oh_fused * 40)) + (kw * 4)) + ic_inner) + 4)] * p1[((((kh * 48) + (kw * 16)) + (ic_inner * 4)) + oc_block_c_1)]));
          }
          for (int32_t oc_block_c_2 = 0; oc_block_c_2 < 4; ++oc_block_c_2) {
            int32_t cse_var_2 = (oc_block_c_2 + 8);
            conv2d_NCHWc_global[cse_var_2] = (conv2d_NCHWc_global[cse_var_2] + (p0[(((((kh * 40) + (n_oc_chunk_fused_oh_fused * 40)) + (kw * 4)) + ic_inner) + 8)] * p1[((((kh * 48) + (kw * 16)) + (ic_inner * 4)) + oc_block_c_2)]));
          }
          for (int32_t oc_block_c_3 = 0; oc_block_c_3 < 4; ++oc_block_c_3) {
            int32_t cse_var_3 = (oc_block_c_3 + 12);
            conv2d_NCHWc_global[cse_var_3] = (conv2d_NCHWc_global[cse_var_3] + (p0[(((((kh * 40) + (n_oc_chunk_fused_oh_fused * 40)) + (kw * 4)) + ic_inner) + 12)] * p1[((((kh * 48) + (kw * 16)) + (ic_inner * 4)) + oc_block_c_3)]));
          }
          for (int32_t oc_block_c_4 = 0; oc_block_c_4 < 4; ++oc_block_c_4) {
            int32_t cse_var_4 = (oc_block_c_4 + 16);
            conv2d_NCHWc_global[cse_var_4] = (conv2d_NCHWc_global[cse_var_4] + (p0[(((((kh * 40) + (n_oc_chunk_fused_oh_fused * 40)) + (kw * 4)) + ic_inner) + 16)] * p1[((((kh * 48) + (kw * 16)) + (ic_inner * 4)) + oc_block_c_4)]));
          }
          for (int32_t oc_block_c_5 = 0; oc_block_c_5 < 4; ++oc_block_c_5) {
            int32_t cse_var_5 = (oc_block_c_5 + 20);
            conv2d_NCHWc_global[cse_var_5] = (conv2d_NCHWc_global[cse_var_5] + (p0[(((((kh * 40) + (n_oc_chunk_fused_oh_fused * 40)) + (kw * 4)) + ic_inner) + 20)] * p1[((((kh * 48) + (kw * 16)) + (ic_inner * 4)) + oc_block_c_5)]));
          }
          for (int32_t oc_block_c_6 = 0; oc_block_c_6 < 4; ++oc_block_c_6) {
            int32_t cse_var_6 = (oc_block_c_6 + 24);
            conv2d_NCHWc_global[cse_var_6] = (conv2d_NCHWc_global[cse_var_6] + (p0[(((((kh * 40) + (n_oc_chunk_fused_oh_fused * 40)) + (kw * 4)) + ic_inner) + 24)] * p1[((((kh * 48) + (kw * 16)) + (ic_inner * 4)) + oc_block_c_6)]));
          }
          for (int32_t oc_block_c_7 = 0; oc_block_c_7 < 4; ++oc_block_c_7) {
            int32_t cse_var_7 = (oc_block_c_7 + 28);
            conv2d_NCHWc_global[cse_var_7] = (conv2d_NCHWc_global[cse_var_7] + (p0[(((((kh * 40) + (n_oc_chunk_fused_oh_fused * 40)) + (kw * 4)) + ic_inner) + 28)] * p1[((((kh * 48) + (kw * 16)) + (ic_inner * 4)) + oc_block_c_7)]));
          }
        }
      }
    }
    for (int32_t ow_inner = 0; ow_inner < 8; ++ow_inner) {
      for (int32_t oc_block = 0; oc_block < 4; ++oc_block) {
        int32_t cse_var_8 = (ow_inner * 4);
        conv2d_NCHWc[(((n_oc_chunk_fused_oh_fused * 32) + cse_var_8) + oc_block)] = conv2d_NCHWc_global[(cse_var_8 + oc_block)];
      }
    }
  }
  return 0;
}

#ifdef __cplusplus
extern "C"
#endif
TVM_DLL int32_t tvmgen_default___tvm_main__(double* input0_buffer_var, double* input1_buffer_var, double* output_buffer_var) {
  void* sid_2 = TVMBackendAllocWorkspace(1, 0, (uint64_t)3200, 0, 8);
  if (sid_2 == NULL) {
    return -1;
  }
  void* sid_3 = TVMBackendAllocWorkspace(1, 0, (uint64_t)1152, 0, 8);
  if (sid_3 == NULL) {
    return -1;
  }
  void* sid_4 = TVMBackendAllocWorkspace(1, 0, (uint64_t)2048, 0, 8);
  if (sid_4 == NULL) {
    return -1;
  }
  if (tvmgen_default_fused_layout_transform(input0_buffer_var, sid_2) != 0 ) return -1;
  if (tvmgen_default_fused_layout_transform_1(input1_buffer_var, sid_3) != 0 ) return -1;
  if (tvmgen_default_fused_nn_contrib_conv2d_NCHWc(sid_2, sid_3, sid_4) != 0 ) return -1;
  if (tvmgen_default_fused_layout_transform_2(sid_4, output_buffer_var) != 0 ) return -1;
  if (TVMBackendFreeWorkspace(1, 0, sid_4) != 0) {
    return -1;
  }
  if (TVMBackendFreeWorkspace(1, 0, sid_3) != 0) {
    return -1;
  }
  if (TVMBackendFreeWorkspace(1, 0, sid_2) != 0) {
    return -1;
  }
  return 0;
}

