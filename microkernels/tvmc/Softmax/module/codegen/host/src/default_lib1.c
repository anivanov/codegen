// tvm target: c -keys=cpu 
#define TVM_EXPORTS
#include "tvm/runtime/c_runtime_api.h"
#include "tvm/runtime/c_backend_api.h"
#include <math.h>
#ifdef __cplusplus
extern "C"
#endif
TVM_DLL int32_t tvmgen_default_fused_nn_softmax(double* p0, double* T_softmax_norm) {
  for (int32_t i0 = 0; i0 < 8; ++i0) {
    double T_softmax_maxelem[1];
    double T_softmax_exp[8];
    T_softmax_maxelem[0] = -1.797693e+308;
    for (int32_t k = 0; k < 8; ++k) {
      double __1 = T_softmax_maxelem[0];
      double __2 = p0[((i0 * 8) + k)];
      T_softmax_maxelem[0] = ((__1) > (__2) ? (__1) : (__2));
    }
    for (int32_t i1 = 0; i1 < 8; ++i1) {
      T_softmax_exp[i1] = exp((p0[((i0 * 8) + i1)] - T_softmax_maxelem[0]));
    }
    T_softmax_maxelem[0] = 0.000000e+00;
    for (int32_t k_1 = 0; k_1 < 8; ++k_1) {
      T_softmax_maxelem[0] = (T_softmax_maxelem[0] + T_softmax_exp[k_1]);
    }
    for (int32_t i1_1 = 0; i1_1 < 8; ++i1_1) {
      T_softmax_norm[((i0 * 8) + i1_1)] = (T_softmax_exp[i1_1] / T_softmax_maxelem[0]);
    }
  }
  return 0;
}

#ifdef __cplusplus
extern "C"
#endif
TVM_DLL int32_t tvmgen_default___tvm_main__(double* input0_buffer_var, double* output_buffer_var) {
  if (tvmgen_default_fused_nn_softmax(input0_buffer_var, output_buffer_var) != 0 ) return -1;
  return 0;
}

