#ifndef TVMGEN_DEFAULT_H_
#define TVMGEN_DEFAULT_H_
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/*!
 * \brief Input tensor input0 size (in bytes) for TVM module "default" 
 */
#define TVMGEN_DEFAULT_INPUT0_SIZE 48894048
/*!
 * \brief Input tensor input1 size (in bytes) for TVM module "default" 
 */
#define TVMGEN_DEFAULT_INPUT1_SIZE 49025424
/*!
 * \brief Input tensor input3 size (in bytes) for TVM module "default" 
 */
#define TVMGEN_DEFAULT_INPUT3_SIZE 45646080
/*!
 * \brief Input tensor input4 size (in bytes) for TVM module "default" 
 */
#define TVMGEN_DEFAULT_INPUT4_SIZE 48871328
/*!
 * \brief Input tensor input2 size (in bytes) for TVM module "default" 
 */
#define TVMGEN_DEFAULT_INPUT2_SIZE 49251152
/*!
 * \brief Output tensor output2 size (in bytes) for TVM module "default" 
 */
#define TVMGEN_DEFAULT_OUTPUT2_SIZE 49102960
/*!
 * \brief Output tensor output1 size (in bytes) for TVM module "default" 
 */
#define TVMGEN_DEFAULT_OUTPUT1_SIZE 48837760
/*!
 * \brief Output tensor output0 size (in bytes) for TVM module "default" 
 */
#define TVMGEN_DEFAULT_OUTPUT0_SIZE 49018352
/*!
 * \brief Input tensor pointers for TVM module "default" 
 */
struct tvmgen_default_inputs {
  void* input0;
  void* input1;
  void* input2;
  void* input3;
  void* input4;
};

/*!
 * \brief Output tensor pointers for TVM module "default" 
 */
struct tvmgen_default_outputs {
  void* output0;
  void* output1;
  void* output2;
};

/*!
 * \brief entrypoint function for TVM module "default"
 * \param inputs Input tensors for the module 
 * \param outputs Output tensors for the module 
 */
int32_t tvmgen_default_run(
  struct tvmgen_default_inputs* inputs,
  struct tvmgen_default_outputs* outputs
);
/*!
 * \brief Workspace size for TVM module "default" 
 */
#define TVMGEN_DEFAULT_WORKSPACE_SIZE 16480

#ifdef __cplusplus
}
#endif

#endif // TVMGEN_DEFAULT_H_
