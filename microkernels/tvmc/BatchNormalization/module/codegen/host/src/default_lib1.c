// tvm target: c -keys=cpu 
#define TVM_EXPORTS
#include "tvm/runtime/c_runtime_api.h"
#include "tvm/runtime/c_backend_api.h"
#include <math.h>
#ifdef __cplusplus
extern "C"
#endif
TVM_DLL int32_t tvmgen_default_fused_add_rsqrt_multiply_expand_dims_multiply_negative_multiply_add_expand_dims__dd902264fd7a268a_(double* p0, double* p1, double* p2, double* p3, double* p4, double* T_add) {
  for (int32_t ax0_ax1_fused = 0; ax0_ax1_fused < 16; ++ax0_ax1_fused) {
    for (int32_t ax2 = 0; ax2 < 8; ++ax2) {
      for (int32_t ax3_inner = 0; ax3_inner < 8; ++ax3_inner) {
        int32_t cse_var_2 = (ax0_ax1_fused & 3);
        int32_t cse_var_1 = (((ax0_ax1_fused * 64) + (ax2 * 8)) + ax3_inner);
        T_add[cse_var_1] = ((p2[cse_var_1] * ((1.000000e+00 / sqrt((p0[cse_var_2] + 1.000000e-05))) * p1[cse_var_2])) + (((0.000000e+00 - p3[cse_var_2]) * ((1.000000e+00 / sqrt((p0[cse_var_2] + 1.000000e-05))) * p1[cse_var_2])) + p4[cse_var_2]));
      }
    }
  }
  return 0;
}

#ifdef __cplusplus
extern "C"
#endif
TVM_DLL int32_t tvmgen_default_fused_nn_batch_norm(double* p0, double* p1, double* p2, double* p3, double* p4, double* T_add, double* T_multiply, double* T_multiply_1) {
  double T_reshape[4];
  void* T_subtract = TVMBackendAllocWorkspace(1, 0, (uint64_t)8192, 2, 64);
  if (T_subtract == NULL) {
    return -1;
  }
  double T_reshape_1[4];
  double T_reshape_2[4];
  for (int32_t ax1 = 0; ax1 < 4; ++ax1) {
    T_reshape[ax1] = p3[ax1];
  }
  for (int32_t ax0 = 0; ax0 < 4; ++ax0) {
    for (int32_t ax1_1 = 0; ax1_1 < 4; ++ax1_1) {
      for (int32_t ax2 = 0; ax2 < 8; ++ax2) {
        for (int32_t ax3 = 0; ax3 < 8; ++ax3) {
          int32_t cse_var_1 = ((((ax0 * 256) + (ax1_1 * 64)) + (ax2 * 8)) + ax3);
          ((double*)T_subtract)[cse_var_1] = (p0[cse_var_1] - T_reshape[ax1_1]);
        }
      }
    }
  }
  for (int32_t ax1_2 = 0; ax1_2 < 4; ++ax1_2) {
    T_reshape[ax1_2] = p4[ax1_2];
  }
  for (int32_t ax1_3 = 0; ax1_3 < 4; ++ax1_3) {
    T_reshape[ax1_3] = (T_reshape[ax1_3] + 1.000000e-05);
  }
  for (int32_t i1 = 0; i1 < 4; ++i1) {
    T_reshape[i1] = sqrt(T_reshape[i1]);
  }
  for (int32_t ax1_4 = 0; ax1_4 < 4; ++ax1_4) {
    T_reshape_1[ax1_4] = p1[ax1_4];
  }
  for (int32_t ax1_5 = 0; ax1_5 < 4; ++ax1_5) {
    T_reshape_2[ax1_5] = p2[ax1_5];
  }
  for (int32_t ax0_ax1_fused_ax2_fused = 0; ax0_ax1_fused_ax2_fused < 128; ++ax0_ax1_fused_ax2_fused) {
    for (int32_t ax3_1 = 0; ax3_1 < 8; ++ax3_1) {
      int32_t cse_var_3 = ((ax0_ax1_fused_ax2_fused & 31) >> 3);
      int32_t cse_var_2 = ((ax0_ax1_fused_ax2_fused * 8) + ax3_1);
      T_add[cse_var_2] = (((((double*)T_subtract)[cse_var_2] / T_reshape[cse_var_3]) * T_reshape_1[cse_var_3]) + T_reshape_2[cse_var_3]);
    }
  }
  for (int32_t ax0_1 = 0; ax0_1 < 4; ++ax0_1) {
    T_multiply[ax0_1] = p3[ax0_1];
  }
  for (int32_t ax0_2 = 0; ax0_2 < 4; ++ax0_2) {
    T_multiply_1[ax0_2] = p4[ax0_2];
  }
  if (TVMBackendFreeWorkspace(1, 0, T_subtract) != 0) {
    return -1;
  }
  return 0;
}

#ifdef __cplusplus
extern "C"
#endif
TVM_DLL int32_t tvmgen_default___tvm_main__(double* input0_buffer_var, double* input1_buffer_var, double* input2_buffer_var, double* input3_buffer_var, double* input4_buffer_var, double* output_buffer_var, double* output2_buffer_var, double* output3_buffer_var) {
  void* sid_6 = TVMBackendAllocWorkspace(1, 0, (uint64_t)8192, 0, 8);
  if (sid_6 == NULL) {
    return -1;
  }
  if (tvmgen_default_fused_add_rsqrt_multiply_expand_dims_multiply_negative_multiply_add_expand_dims__dd902264fd7a268a_(input4_buffer_var, input1_buffer_var, input0_buffer_var, input3_buffer_var, input2_buffer_var, output_buffer_var) != 0 ) return -1;
  if (tvmgen_default_fused_nn_batch_norm(input0_buffer_var, input1_buffer_var, input2_buffer_var, input3_buffer_var, input4_buffer_var, sid_6, output2_buffer_var, output3_buffer_var) != 0 ) return -1;
  if (TVMBackendFreeWorkspace(1, 0, sid_6) != 0) {
    return -1;
  }
  return 0;
}

