#include "tvm/runtime/c_runtime_api.h"
#ifdef __cplusplus
extern "C" {
#endif
#include <tvmgen_default.h>
TVM_DLL int32_t tvmgen_default___tvm_main__(void* input0,void* input1,void* input2,void* input3,void* input4,void* output0,void* output1,void* output2);
int32_t tvmgen_default_run(struct tvmgen_default_inputs* inputs,struct tvmgen_default_outputs* outputs) {return tvmgen_default___tvm_main__(inputs->input0,inputs->input1,inputs->input2,inputs->input3,inputs->input4,outputs->output0,outputs->output1,outputs->output2);
}
#ifdef __cplusplus
}
#endif
;