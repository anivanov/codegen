// tvm target: c -keys=cpu 
#define TVM_EXPORTS
#include "tvm/runtime/c_runtime_api.h"
#include "tvm/runtime/c_backend_api.h"
#include <math.h>
#ifdef __cplusplus
extern "C"
#endif
TVM_DLL int32_t tvmgen_default_fused_add(double* p0, double* p1, double* T_add) {
  for (int32_t ax0 = 0; ax0 < 16; ++ax0) {
    for (int32_t ax1_inner = 0; ax1_inner < 16; ++ax1_inner) {
      int32_t cse_var_1 = ((ax0 * 16) + ax1_inner);
      T_add[cse_var_1] = (p0[cse_var_1] + p1[cse_var_1]);
    }
  }
  return 0;
}

#ifdef __cplusplus
extern "C"
#endif
TVM_DLL int32_t tvmgen_default___tvm_main__(double* input0_buffer_var, double* input1_buffer_var, double* output_buffer_var) {
  if (tvmgen_default_fused_add(input0_buffer_var, input1_buffer_var, output_buffer_var) != 0 ) return -1;
  return 0;
}

