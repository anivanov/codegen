from .kernels import *
from .gen import *
from .measurements import RuntimeCache, RuntimeMeasurement, MeasurementStatus

import torch
import argparse
import os
import itertools
import functools
import operator


class SearchNode:
    def __init__(self, runtime, edges):
        self.runtime = runtime
        self.edges = edges  # edge = (tname, *targs)
        self.subtree = {}  # edge -> SearchNode


class SearchStateGreedy:
    def __init__(self, program, runtime):
        self.program = copy.deepcopy(program)
        self.explored_paths = {tuple(): runtime}  # path -> runtime
        self.seen_hashes = set()  # prevent looping
        self.tree_root = SearchNode(runtime, self.find_edges_for_path([]))
        self.seen_hashes.add(self.program.program_hash())
    
    def find_edges_for_path(self, path):
        program_copy = copy.deepcopy(self.program)
        for tname, *targs in path:
            tapply, _ = TRANSFORMATIONS_LIST[tname]
            tapply(program_copy, *targs)
        if program_copy.program_hash() in self.seen_hashes:
            return tuple()
        edges = tuple(
            (tname, *targs)
            for tname, (tapply, tcheck) in TRANSFORMATIONS_LIST.items()
            for targs in tcheck(program_copy)
        )
        return edges
    
    def update(self, path, runtime, program_hash):
        assert len(path) > 0
        # find where to insert
        node = self.tree_root
        for tname_with_targs in path[:-1]:
            node = node.subtree[tname_with_targs]
        # insert
        edges = self.find_edges_for_path(path)
        self.seen_hashes.add(program_hash)
        node.subtree[path[-1]] = SearchNode(runtime, edges)
        # insert to the explored paths
        self.explored_paths[path] = runtime

    def query(self):
        # find math with minimum runtime that is still unexplored
        for path, cost in sorted(self.explored_paths.items(), key=lambda x: x[1]):
            node = self.tree_root
            for tname, *targs in path:
                node = node.subtree[(tname, *targs)]
            for edge in node.edges:
                if edge not in node.subtree:
                    return (*path, edge)
        return None
    
    
class SearchStateShortAndGreedy:
    def __init__(self, program, runtime):
        self.program = copy.deepcopy(program)
        self.explored_paths = {tuple(): runtime}  # path -> runtime
        self.seen_hashes = set()  # prevent looping
        self.tree_root = SearchNode(runtime, self.find_edges_for_path([]))
        self.seen_hashes.add(self.program.program_hash())
    
    def find_edges_for_path(self, path):
        program_copy = copy.deepcopy(self.program)
        for tname, *targs in path:
            tapply, _ = TRANSFORMATIONS_LIST[tname]
            tapply(program_copy, *targs)
        if program_copy.program_hash() in self.seen_hashes:
            return tuple()
        edges = tuple(
            (tname, *targs)
            for tname, (tapply, tcheck) in TRANSFORMATIONS_LIST.items()
            for targs in tcheck(program_copy)
        )
        return edges
    
    def update(self, path, runtime, program_hash):
        assert len(path) > 0
        # find where to insert
        node = self.tree_root
        for tname_with_targs in path[:-1]:
            node = node.subtree[tname_with_targs]
        # insert
        edges = self.find_edges_for_path(path)
        self.seen_hashes.add(program_hash)
        node.subtree[path[-1]] = SearchNode(runtime, edges)
        # insert to the explored paths
        self.explored_paths[path] = runtime

    def query(self):
        # find math with minimum runtime that is still unexplored
        for path, cost in sorted(self.explored_paths.items(), key=lambda x: (1 + 0.05 * math.log(1+len(x[0]))) * x[1]):
            node = self.tree_root
            for tname, *targs in path:
                node = node.subtree[(tname, *targs)]
            for edge in node.edges:
                if edge not in node.subtree:
                    return (*path, edge)
        return None


class SearchNodeLeastExploredPaths:
    def __init__(self, runtime, edges):
        self.runtime = runtime
        self.edges = edges  # edge = (tname, *targs)
        self.subtree = {}  # edge -> SearchNode
        self.num_explored_subpath = 0


class SearchLeastExploredPaths:
    def __init__(self, program, runtime):
        self.program = copy.deepcopy(program)
        self.explored_paths = {tuple(): runtime}  # path -> runtime
        self.seen_hashes = set()  # prevent looping
        self.tree_root = SearchNodeLeastExploredPaths(runtime, self.find_edges_for_path([]))
        self.seen_hashes.add(self.program.program_hash())
    
    def find_edges_for_path(self, path):
        program_copy = copy.deepcopy(self.program)
        for tname, *targs in path:
            tapply, _ = TRANSFORMATIONS_LIST[tname]
            tapply(program_copy, *targs)
        if program_copy.program_hash() in self.seen_hashes:
            return tuple()
        edges = tuple(
            (tname, *targs)
            for tname, (tapply, tcheck) in TRANSFORMATIONS_LIST.items()
            for targs in tcheck(program_copy)
        )
        return edges
    
    def update(self, path, runtime, program_hash):
        assert len(path) > 0
        # find where to insert
        node = self.tree_root
        node.num_explored_subpath += 1
        for tname_with_targs in path[:-1]:
            node = node.subtree[tname_with_targs]
            node.num_explored_subpath += 1
        # insert
        edges = self.find_edges_for_path(path)
        self.seen_hashes.add(program_hash)
        node.subtree[path[-1]] = SearchNodeLeastExploredPaths(runtime, edges)
        # insert to the explored paths
        self.explored_paths[path] = runtime

    def get_num_subpath_list(self, path):
        result = [1]
        node = self.tree_root
        parent_paths = node.num_explored_subpath + 1
        for tname_with_targs in path:
            node = node.subtree[tname_with_targs]
            result.append(node.num_explored_subpath / parent_paths)
        return result

    def query(self):

        for path, cost in sorted(self.explored_paths.items(), key=lambda x: np.sum(self.get_num_subpath_list(x[0])) * x[1]):
            node = self.tree_root
            for tname, *targs in path:
                node = node.subtree[(tname, *targs)]
            for edge in node.edges:
                if edge not in node.subtree:
                    return (*path, edge)
        return None
    

class SearchNodeLeastExploredOrFastest:
    def __init__(self, runtime, edges):
        self.runtime = runtime
        self.edges = edges  # edge = (tname, *targs)
        self.subtree = {}  # edge -> SearchNode
        self.is_exhausted = False
        self.min_runtime = runtime


class SearchLeastExploredOrFastest:
    def __init__(self, program, runtime):
        self.program = copy.deepcopy(program)
        self.seen_hashes = set()  # prevent looping
        self.tree_root = SearchNodeLeastExploredOrFastest(runtime, self.find_edges_for_path([]))
        self.seen_hashes.add(self.program.program_hash())
    
    def find_edges_for_path(self, path):
        program_copy = copy.deepcopy(self.program)
        for tname, *targs in path:
            tapply, _ = TRANSFORMATIONS_LIST[tname]
            tapply(program_copy, *targs)
        if program_copy.program_hash() in self.seen_hashes:
            return tuple()
        edges = tuple(
            (tname, *targs)
            for tname, (tapply, tcheck) in TRANSFORMATIONS_LIST.items()
            for targs in tcheck(program_copy)
        )
        return edges
    
    def update(self, path, runtime, program_hash):
        assert len(path) > 0
        # find where to insert
        node = self.tree_root
        node.min_runtime = min(node.min_runtime, runtime)
        for tname_with_targs in path[:-1]:
            node = node.subtree[tname_with_targs]
            node.min_runtime = min(node.min_runtime, runtime)
        # insert
        edges = self.find_edges_for_path(path)
        self.seen_hashes.add(program_hash)
        node.subtree[path[-1]] = SearchNodeLeastExploredOrFastest(runtime, edges)

    def query(self):
        node = self.tree_root
        path = []
        while not self.tree_root.is_exhausted:
            # for every edge, assign cost
            costs = {}
            # first, process only edges that were already taken
            for edge, nested_node in node.subtree.items():
                if len(nested_node.edges) == 0:
                    continue  # there is nothing to explore and node was already considred
                if nested_node.is_exhausted:
                    continue  # do not consider fully explored nodes
                explored_fraction = len(nested_node.subtree) / len(nested_node.edges)
                costs[edge] = (nested_node.min_runtime, explored_fraction)
            average_min_runtime = np.mean([r for r, e in costs.values()]) if costs else 0.0
            # second add edges that were never taken
            for edge in node.edges:
                if edge in node.subtree:
                    continue  # already considered
                costs[edge] = (average_min_runtime, 0.0)
            if not costs:
                # we end up in fully explored node, mark the node as fully explored and restart
                node.is_exhausted = True
                node = self.tree_root
                path = []
                continue
            # find the least explored path candidate
            least_explored_val = min([e for r, e in costs.values()])
            least_runtime_val = min([r for r, e in costs.values()])
            fastest_candidate = [k for k, (r, e) in costs.items() if r == least_runtime_val]
            least_explored_candidate = [k for k, (r, e) in costs.items() if e == least_explored_val]
            # randomly choose among candidate
            if random.random() < 0.3:
                candidate = least_explored_candidate
            else:
                candidate = fastest_candidate
            edge = random.choice(candidate)
            path.append(edge)
            if edge in node.subtree:
                node = node.subtree[edge]
            else:
                return tuple(path)
        return None


class SearchNodeProbabilistic:
    def __init__(self, runtime, edges):
        self.runtime = runtime
        self.edges = edges  # edge = (tname, *targs)
        self.subtree = {}  # edge -> SearchNode
        self.is_exhausted = False
        self.min_runtime = runtime


class SearchProbabilistic:
    def __init__(self, program, runtime):
        self.program = copy.deepcopy(program)
        self.seen_hashes = set()  # prevent looping
        self.tree_root = SearchNodeProbabilistic(runtime, self.find_edges_for_path([]))
        self.seen_hashes.add(self.program.program_hash())
    
    def find_edges_for_path(self, path):
        program_copy = copy.deepcopy(self.program)
        for tname, *targs in path:
            tapply, _ = TRANSFORMATIONS_LIST[tname]
            tapply(program_copy, *targs)
        if program_copy.program_hash() in self.seen_hashes:
            return tuple()
        edges = tuple(
            (tname, *targs)
            for tname, (tapply, tcheck) in TRANSFORMATIONS_LIST.items()
            for targs in tcheck(program_copy)
        )
        return edges
    
    def update(self, path, runtime, program_hash):
        assert len(path) > 0
        # find where to insert
        node = self.tree_root
        node.min_runtime = min(node.min_runtime, runtime)
        for tname_with_targs in path[:-1]:
            node = node.subtree[tname_with_targs]
            node.min_runtime = min(node.min_runtime, runtime)
        # insert
        edges = self.find_edges_for_path(path)
        self.seen_hashes.add(program_hash)
        node.subtree[path[-1]] = SearchNodeProbabilistic(runtime, edges)

    def query(self):
        node = self.tree_root
        path = []
        while not self.tree_root.is_exhausted:
            # for every edge, assign cost
            costs = {}
            # first, process only edges that were already taken
            for edge, nested_node in node.subtree.items():
                if len(nested_node.edges) == 0:
                    continue  # there is nothing to explore and node was already considred
                if nested_node.is_exhausted:
                    continue  # do not consider fully explored nodes
                costs[edge] = nested_node.min_runtime
            average_min_runtime = np.mean(list(costs.values())) if costs else 1.0  # 1.0 is chosen as any constant except 0 is allowed
            # second add edges that were never taken
            for edge in node.edges:
                if edge in node.subtree:
                    continue  # already considered
                costs[edge] = average_min_runtime
            if not costs:
                # we end up in fully explored node, mark the node as fully explored and restart
                node.is_exhausted = True
                node = self.tree_root
                path = []
                continue
            edges = list(costs.keys())
            cost_vals = list(costs.values())
            profits = np.array([1.0 / c for c in cost_vals])
            normalized_profits = profits / np.sum(profits)
            # randomly choose among candidate
            sampled_edge_idx = np.random.choice(len(edges), p=normalized_profits)
            edge = edges[sampled_edge_idx]
            path.append(edge)
            if edge in node.subtree:
                node = node.subtree[edge]
            else:
                return tuple(path)
        return None


class SearchProbabilisticLimitedMin:
    def __init__(self, program, runtime):
        self.program = copy.deepcopy(program)
        self.seen_hashes = set()  # prevent looping
        self.tree_root = SearchNodeProbabilistic(runtime, self.find_edges_for_path([]))
        self.seen_hashes.add(self.program.program_hash())
    
    def find_edges_for_path(self, path):
        program_copy = copy.deepcopy(self.program)
        for tname, *targs in path:
            tapply, _ = TRANSFORMATIONS_LIST[tname]
            tapply(program_copy, *targs)
        if program_copy.program_hash() in self.seen_hashes:
            return tuple()
        edges = tuple(
            (tname, *targs)
            for tname, (tapply, tcheck) in TRANSFORMATIONS_LIST.items()
            for targs in tcheck(program_copy)
        )
        return edges
    
    def update(self, path, runtime, program_hash):
        assert len(path) > 0
        # find where to insert
        node = self.tree_root
        node.min_runtime = min(node.min_runtime, runtime)
        for i, tname_with_targs in enumerate(path[:-1]):
            node = node.subtree[tname_with_targs]
            if i > len(path) - 3:
                # only update the last 3 nodes
                node.min_runtime = min(node.min_runtime, runtime)
        # insert
        edges = self.find_edges_for_path(path)
        self.seen_hashes.add(program_hash)
        node.subtree[path[-1]] = SearchNodeProbabilistic(runtime, edges)

    def query(self):
        node = self.tree_root
        path = []
        while not self.tree_root.is_exhausted:
            # for every edge, assign cost
            costs = {}
            # first, process only edges that were already taken
            for edge, nested_node in node.subtree.items():
                if len(nested_node.edges) == 0:
                    continue  # there is nothing to explore and node was already considred
                if nested_node.is_exhausted:
                    continue  # do not consider fully explored nodes
                costs[edge] = nested_node.min_runtime
            average_min_runtime = np.mean(list(costs.values())) if costs else 1.0  # 1.0 is chosen as any constant except 0 is allowed
            # second add edges that were never taken
            for edge in node.edges:
                if edge in node.subtree:
                    continue  # already considered
                costs[edge] = average_min_runtime
            if not costs:
                # we end up in fully explored node, mark the node as fully explored and restart
                node.is_exhausted = True
                node = self.tree_root
                path = []
                continue
            edges = list(costs.keys())
            cost_vals = list(costs.values())
            profits = np.array([1.0 / c for c in cost_vals])
            normalized_profits = profits / np.sum(profits)
            # randomly choose among candidate
            sampled_edge_idx = np.random.choice(len(edges), p=normalized_profits)
            edge = edges[sampled_edge_idx]
            path.append(edge)
            if edge in node.subtree:
                node = node.subtree[edge]
            else:
                return tuple(path)
        return None


class SearchNodeProbGeneric:
    def __init__(self, runtime, edges):
        self.runtime = runtime
        self.edges = edges  # edge = (tname, *targs)
        self.subtree = {}  # edge -> SearchNode
        self.is_exhausted = False
        self.runtimes = [runtime]


class SearchProbabilisticLimitedAvg:
    def __init__(self, program, runtime):
        self.program = copy.deepcopy(program)
        self.seen_hashes = set()  # prevent looping
        self.tree_root = SearchNodeProbGeneric(runtime, self.find_edges_for_path([]))
        self.seen_hashes.add(self.program.program_hash())
    
    def find_edges_for_path(self, path):
        program_copy = copy.deepcopy(self.program)
        for tname, *targs in path:
            tapply, _ = TRANSFORMATIONS_LIST[tname]
            tapply(program_copy, *targs)
        if program_copy.program_hash() in self.seen_hashes:
            return tuple()
        edges = tuple(
            (tname, *targs)
            for tname, (tapply, tcheck) in TRANSFORMATIONS_LIST.items()
            for targs in tcheck(program_copy)
        )
        return edges
    
    def update(self, path, runtime, program_hash):
        assert len(path) > 0
        # find where to insert
        node = self.tree_root
        for i, tname_with_targs in enumerate(path[:-1]):
            node = node.subtree[tname_with_targs]
            if i > len(path) - 3:
                # only update the last 3 nodes
                node.runtimes.append(runtime)
        # insert
        edges = self.find_edges_for_path(path)
        self.seen_hashes.add(program_hash)
        node.subtree[path[-1]] = SearchNodeProbGeneric(runtime, edges)

    def query(self):
        node = self.tree_root
        path = []
        while not self.tree_root.is_exhausted:
            # for every edge, assign cost
            costs = {}
            # first, process only edges that were already taken
            for edge, nested_node in node.subtree.items():
                if len(nested_node.edges) == 0:
                    continue  # there is nothing to explore and node was already considred
                if nested_node.is_exhausted:
                    continue  # do not consider fully explored nodes
                costs[edge] = np.mean(nested_node.runtimes)
            average_min_runtime = np.mean(list(costs.values())) if costs else 1.0  # 1.0 is chosen as any constant except 0 is allowed
            # second add edges that were never taken
            for edge in node.edges:
                if edge in node.subtree:
                    continue  # already considered
                costs[edge] = average_min_runtime
            if not costs:
                # we end up in fully explored node, mark the node as fully explored and restart
                node.is_exhausted = True
                node = self.tree_root
                path = []
                continue
            edges = list(costs.keys())
            cost_vals = list(costs.values())
            profits = np.array([1.0 / c for c in cost_vals])
            normalized_profits = profits / np.sum(profits)
            # randomly choose among candidate
            sampled_edge_idx = np.random.choice(len(edges), p=normalized_profits)
            edge = edges[sampled_edge_idx]
            path.append(edge)
            if edge in node.subtree:
                node = node.subtree[edge]
            else:
                return tuple(path)
        return None


class SearchProbabilisticLimitedAvg2:
    def __init__(self, program, runtime):
        self.program = copy.deepcopy(program)
        self.seen_hashes = set()  # prevent looping
        self.tree_root = SearchNodeProbGeneric(runtime, self.find_edges_for_path([]))
        self.seen_hashes.add(self.program.program_hash())
    
    def find_edges_for_path(self, path):
        program_copy = copy.deepcopy(self.program)
        for tname, *targs in path:
            tapply, _ = TRANSFORMATIONS_LIST[tname]
            tapply(program_copy, *targs)
        if program_copy.program_hash() in self.seen_hashes:
            return tuple()
        edges = tuple(
            (tname, *targs)
            for tname, (tapply, tcheck) in TRANSFORMATIONS_LIST.items()
            for targs in tcheck(program_copy)
        )
        return edges
    
    def update(self, path, runtime, program_hash):
        assert len(path) > 0
        # find where to insert
        node = self.tree_root
        for i, tname_with_targs in enumerate(path[:-1]):
            node = node.subtree[tname_with_targs]
            if i > len(path) - 2:
                # only update the last 2 nodes
                node.runtimes.append(runtime)
        # insert
        edges = self.find_edges_for_path(path)
        self.seen_hashes.add(program_hash)
        node.subtree[path[-1]] = SearchNodeProbGeneric(runtime, edges)

    def query(self):
        node = self.tree_root
        path = []
        while not self.tree_root.is_exhausted:
            # for every edge, assign cost
            costs = {}
            # first, process only edges that were already taken
            for edge, nested_node in node.subtree.items():
                if len(nested_node.edges) == 0:
                    continue  # there is nothing to explore and node was already considred
                if nested_node.is_exhausted:
                    continue  # do not consider fully explored nodes
                costs[edge] = np.mean(nested_node.runtimes)
            average_min_runtime = np.mean(list(costs.values())) if costs else 1.0  # 1.0 is chosen as any constant except 0 is allowed
            # second add edges that were never taken
            for edge in node.edges:
                if edge in node.subtree:
                    continue  # already considered
                costs[edge] = average_min_runtime
            if not costs:
                # we end up in fully explored node, mark the node as fully explored and restart
                node.is_exhausted = True
                node = self.tree_root
                path = []
                continue
            edges = list(costs.keys())
            cost_vals = list(costs.values())
            profits = np.array([1.0 / c for c in cost_vals])
            normalized_profits = profits / np.sum(profits)
            # randomly choose among candidate
            sampled_edge_idx = np.random.choice(len(edges), p=normalized_profits)
            edge = edges[sampled_edge_idx]
            path.append(edge)
            if edge in node.subtree:
                node = node.subtree[edge]
            else:
                return tuple(path)
        return None


class SearchProbabilisticAvg:
    def __init__(self, program, runtime):
        self.program = copy.deepcopy(program)
        self.seen_hashes = set()  # prevent looping
        self.tree_root = SearchNodeProbGeneric(runtime, self.find_edges_for_path([]))
        self.seen_hashes.add(self.program.program_hash())
    
    def find_edges_for_path(self, path):
        program_copy = copy.deepcopy(self.program)
        for tname, *targs in path:
            tapply, _ = TRANSFORMATIONS_LIST[tname]
            tapply(program_copy, *targs)
        if program_copy.program_hash() in self.seen_hashes:
            return tuple()
        edges = tuple(
            (tname, *targs)
            for tname, (tapply, tcheck) in TRANSFORMATIONS_LIST.items()
            for targs in tcheck(program_copy)
        )
        return edges
    
    def update(self, path, runtime, program_hash):
        assert len(path) > 0
        # find where to insert
        node = self.tree_root
        for i, tname_with_targs in enumerate(path[:-1]):
            node = node.subtree[tname_with_targs]
            node.runtimes.append(runtime)
        # insert
        edges = self.find_edges_for_path(path)
        self.seen_hashes.add(program_hash)
        node.subtree[path[-1]] = SearchNodeProbGeneric(runtime, edges)

    def query(self):
        node = self.tree_root
        path = []
        while not self.tree_root.is_exhausted:
            # for every edge, assign cost
            costs = {}
            # first, process only edges that were already taken
            for edge, nested_node in node.subtree.items():
                if len(nested_node.edges) == 0:
                    continue  # there is nothing to explore and node was already considred
                if nested_node.is_exhausted:
                    continue  # do not consider fully explored nodes
                costs[edge] = np.mean(nested_node.runtimes)
            average_min_runtime = np.mean(list(costs.values())) if costs else 1.0  # 1.0 is chosen as any constant except 0 is allowed
            # second add edges that were never taken
            for edge in node.edges:
                if edge in node.subtree:
                    continue  # already considered
                costs[edge] = average_min_runtime
            if not costs:
                # we end up in fully explored node, mark the node as fully explored and restart
                node.is_exhausted = True
                node = self.tree_root
                path = []
                continue
            edges = list(costs.keys())
            cost_vals = list(costs.values())
            profits = np.array([1.0 / c for c in cost_vals])
            normalized_profits = profits / np.sum(profits)
            # randomly choose among candidate
            sampled_edge_idx = np.random.choice(len(edges), p=normalized_profits)
            edge = edges[sampled_edge_idx]
            path.append(edge)
            if edge in node.subtree:
                node = node.subtree[edge]
            else:
                return tuple(path)
        return None





def compute_normalized_profits(profits, importance):
    # copy profits
    profits = {k: list(v) for k, v in profits.items()}
    # normalize importance
    importance = np.array(importance) / np.sum(importance)
    # for each compontent of profits, normalize it across the same component
    profit_sum = np.zeros_like(importance)
    for k, v in profits.items():
        for i, c in enumerate(v):
            profit_sum[i] += c
    for i in range(len(profit_sum)):
        if profit_sum[i] < 1e-4:
            profit_sum[i] = len(profits)
            for k in profits:
                profits[k][i] = 1.0
    # compute normalized profits
    normalized_profits = {}
    for k, v in profits.items():
        normalized_profits[k] = [c / profit_sum[i] for i, c in enumerate(v)]
    # finally, combine profits vector into scalar using importance
    combined_profits = {k: float(np.dot(np.array(v), importance)) for k, v in normalized_profits.items()}
    return combined_profits


class SearchNodeNoEval:
    def __init__(self, edges):
        self.edges = edges  # edge = (tname, *targs)
        self.subtree = {}  # edge -> SearchNode
        self.is_exhausted = False
        self.runtimes = []


class SearchProbRareEval:
    def __init__(self, program, runtime):
        self.program = copy.deepcopy(program)
        self.seen_hashes = set()  # prevent looping
        self.tree_root = SearchNodeNoEval(self.find_edges_for_path([]))
        self.seen_hashes.add(self.program.program_hash())
    
    def find_edges_for_path(self, path):
        program_copy = program_for_path(self.program, path)
        if program_copy.program_hash() in self.seen_hashes:
            return tuple()
        edges = tuple(
            (tname, *targs)
            for tname, (tapply, tcheck) in TRANSFORMATIONS_LIST.items()
            for targs in tcheck(program_copy)
        )
        return edges
    
    def fill(self, path):
        assert len(path) > 0
        # find where to insert
        node = self.tree_root
        for i, tname_with_targs in enumerate(path[:-1]):
            node = node.subtree[tname_with_targs]
        # insert
        edges = self.find_edges_for_path(path)
        node.subtree[path[-1]] = SearchNodeNoEval(edges)
    
    def update(self, path, runtime, program_hash):
        assert len(path) > 0
        # find where to insert
        node = self.tree_root
        for i, tname_with_targs in enumerate(path[:-1]):
            node = node.subtree[tname_with_targs]
            node.runtimes.append(runtime)
        # insert
        edges = self.find_edges_for_path(path)
        self.seen_hashes.add(program_hash)
        if path[-1] in node.subtree:
            node.subtree[path[-1]].runtimes.append(runtime)
        else:
            node.subtree[path[-1]] = SearchNodeProbGeneric(runtime, edges)

    def query(self):
        node = self.tree_root
        path = []
        while not self.tree_root.is_exhausted:
            # for every edge, assign cost
            costs = {}
            # first, process only edges that were already taken
            for edge, nested_node in node.subtree.items():
                if len(nested_node.edges) == 0:
                    continue  # there is nothing to explore and node was already considered
                if nested_node.is_exhausted:
                    continue  # do not consider fully explored nodes
                if not nested_node.runtimes:
                    continue  # do not consider nodes without any evaluations (we will guess their runtime later)
                costs[edge] = np.mean(nested_node.runtimes)
            average_min_runtime = np.mean(list(costs.values())) if costs else 1.0  # 1.0 is chosen as any constant except 0 is allowed
            # second add edges that were never taken
            for edge in node.edges:
                if edge in node.subtree:
                    continue  # already considered
                costs[edge] = average_min_runtime
            if not costs:
                # we end up in fully explored node, mark the node as fully explored and restart
                node.is_exhausted = True
                node = self.tree_root
                path = []
                continue
            # now, for all candidate collected so far, assign costs based on the program assessment
            profits = {}
            
            min_num_scopes = float('inf')
            max_num_scopes = 0
            for edge in costs:
                candidate_path = path + [edge]
                program_copy = program_for_path(self.program, candidate_path)
                runtime = costs[edge]
                num_scopes = num_scopes_in_program(program_copy)
                buf_size = bufsize_in_program(program_copy)
                parallel_depth = parallelism_depth(program_copy)
                profits[edge] = (runtime, num_scopes, buf_size, parallel_depth)
                
                min_num_scopes = min(min_num_scopes, num_scopes)
                max_num_scopes = max(max_num_scopes, num_scopes)
            if max_num_scopes-min_num_scopes > 0:
                for edge, (r, s, b, p) in profits.items():
                    profits[edge] = (1/r, (max_num_scopes-s)/(max_num_scopes-min_num_scopes), 1/b, 1-p)
            
            normalized_profits = compute_normalized_profits(profits, importance=(0.1, 0.5, 0.3, 0.1))            
            # randomly choose among candidate
            edges = list(normalized_profits.keys())
            sampled_edge_idx = np.random.choice(len(normalized_profits), p=list(normalized_profits.values()))
            edge = edges[sampled_edge_idx]
            path.append(edge)
            if edge not in node.subtree:
                self.fill(path)
            do_measurement = random.random() < 0.05
            no_more_transforms = not node.subtree[edge].edges
            if do_measurement or no_more_transforms:
                return tuple(path)
            else:
                node = node.subtree[edge]
        return None


CACHE_program_for_path = {}

def program_for_path(program, path):
    cache_key = (program.name, tuple(path))
    cached = CACHE_program_for_path.get(cache_key, None)
    if cached is not None:
        return copy.deepcopy(cached)
    new_program = copy.deepcopy(program)
    for tname, *targs in path:
        tapply, _ = TRANSFORMATIONS_LIST[tname]
        tapply(new_program, *targs)
    CACHE_program_for_path[cache_key] = copy.deepcopy(new_program)
    return new_program


def node_for_path(node, path):
    for tname_with_targs in path:
        if tname_with_targs not in node.subtree:
            return None
        node = node.subtree[tname_with_targs]
    return node


def num_scopes_in_program(program):
    return len(list(recursive_scopes(program)))


def bufsize_in_program(program):
    return sum(
        buf.size_bytes()
        for buf in program.buffers.values()
    )


def parallelism_depth(program):
    deepest_parallelism = 0
    for scopes, op in get_flattened_ops_with_scopes_textual(program):
        first_parallel_scope = next((i for i, s in enumerate(scopes) if s.options.get('parallel', False)), 0)
        current_parallelism_depth = 0 if len(scopes) <= 1 else first_parallel_scope / (len(scopes) - 1)
        deepest_parallelism = max(deepest_parallelism, current_parallelism_depth)
    return deepest_parallelism


class SearchProbabilisticRS:
    def __init__(self, program, runtime):
        self.program = copy.deepcopy(program)
        self.seen_hashes = set()  # prevent looping
        self.tree_root = SearchNodeProbabilistic(runtime, self.find_edges_for_path([]))
        self.seen_hashes.add(self.program.program_hash())
    
    def find_edges_for_path(self, path):
        program_copy = program_for_path(self.program, path)
        if program_copy.program_hash() in self.seen_hashes:
            return tuple()
        edges = tuple(
            (tname, *targs)
            for tname, (tapply, tcheck) in TRANSFORMATIONS_LIST.items()
            for targs in tcheck(program_copy)
        )
        return edges
    
    def update(self, path, runtime, program_hash):
        assert len(path) > 0
        # find where to insert
        node = self.tree_root
        node.min_runtime = min(node.min_runtime, runtime)
        for tname_with_targs in path[:-1]:
            node = node.subtree[tname_with_targs]
            node.min_runtime = min(node.min_runtime, runtime)
        # insert
        edges = self.find_edges_for_path(path)
        self.seen_hashes.add(program_hash)
        node.subtree[path[-1]] = SearchNodeProbabilistic(runtime, edges)

    def query(self):
        node = self.tree_root
        path = []
        while not self.tree_root.is_exhausted:
            # for every edge, assign cost
            costs = {}
            # first, process only edges that were already taken
            for edge, nested_node in node.subtree.items():
                if len(nested_node.edges) == 0:
                    continue  # there is nothing to explore and node was already considred
                if nested_node.is_exhausted:
                    continue  # do not consider fully explored nodes
                program_copy = program_for_path(self.program, path + [edge])
                num_scopes = num_scopes_in_program(program_copy)
                costs[edge] = (nested_node.min_runtime, num_scopes)
            average_min_runtime = np.mean([r for r, s in costs.values()]) if costs else 1.0  # 1.0 is chosen as any constant except 0 is allowed
            # second add edges that were never taken
            for edge in node.edges:
                if edge in node.subtree:
                    continue  # already considered
                program_copy = program_for_path(self.program, path + [edge])
                num_scopes = num_scopes_in_program(program_copy)
                costs[edge] = (average_min_runtime, num_scopes)
            if not costs:
                # we end up in fully explored node, mark the node as fully explored and restart
                node.is_exhausted = True
                node = self.tree_root
                path = []
                continue            
            edges = list(costs.keys())
            profit_runtimes = np.array(list((1 / r) for r, s in costs.values()))
            profit_scopes = np.array(list((1 / s) for r, s in costs.values()))
            # normalize profits
            profit_runtimes = profit_runtimes / np.sum(profit_runtimes)
            profit_scopes = profit_scopes / np.sum(profit_scopes)
            profits = 0.5 * profit_runtimes + 0.5 * profit_scopes
            # randomly choose among candidate
            sampled_edge_idx = np.random.choice(len(edges), p=profits)
            edge = edges[sampled_edge_idx]
            path.append(edge)
            if edge in node.subtree:
                node = node.subtree[edge]
            else:
                return tuple(path)
        return None


class SearchProbabilisticRSB:
    def __init__(self, program, runtime):
        self.program = copy.deepcopy(program)
        self.seen_hashes = set()  # prevent looping
        self.tree_root = SearchNodeProbabilistic(runtime, self.find_edges_for_path([]))
        self.seen_hashes.add(self.program.program_hash())
    
    def find_edges_for_path(self, path):
        program_copy = program_for_path(self.program, path)
        if program_copy.program_hash() in self.seen_hashes:
            return tuple()
        edges = tuple(
            (tname, *targs)
            for tname, (tapply, tcheck) in TRANSFORMATIONS_LIST.items()
            for targs in tcheck(program_copy)
        )
        return edges
    
    def update(self, path, runtime, program_hash):
        assert len(path) > 0
        # find where to insert
        node = self.tree_root
        node.min_runtime = min(node.min_runtime, runtime)
        for tname_with_targs in path[:-1]:
            node = node.subtree[tname_with_targs]
            node.min_runtime = min(node.min_runtime, runtime)
        # insert
        edges = self.find_edges_for_path(path)
        self.seen_hashes.add(program_hash)
        node.subtree[path[-1]] = SearchNodeProbabilistic(runtime, edges)

    def query(self):
        node = self.tree_root
        path = []
        while not self.tree_root.is_exhausted:
            # for every edge, assign cost
            costs = {}
            # first, process only edges that were already taken
            for edge, nested_node in node.subtree.items():
                if len(nested_node.edges) == 0:
                    continue  # there is nothing to explore and node was already considred
                if nested_node.is_exhausted:
                    continue  # do not consider fully explored nodes
                program_copy = program_for_path(self.program, path + [edge])
                num_scopes = num_scopes_in_program(program_copy)
                buf_sizes = bufsize_in_program(program_copy)
                costs[edge] = (nested_node.min_runtime, num_scopes, buf_sizes)
            average_min_runtime = np.mean([r for r, s, b in costs.values()]) if costs else 1.0  # 1.0 is chosen as any constant except 0 is allowed
            # second add edges that were never taken
            for edge in node.edges:
                if edge in node.subtree:
                    continue  # already considered
                program_copy = program_for_path(self.program, path + [edge])
                num_scopes = num_scopes_in_program(program_copy)
                buf_sizes = bufsize_in_program(program_copy)
                costs[edge] = (average_min_runtime, num_scopes, buf_sizes)
            if not costs:
                # we end up in fully explored node, mark the node as fully explored and restart
                node.is_exhausted = True
                node = self.tree_root
                path = []
                continue            
            edges = list(costs.keys())
            profit_runtimes = np.array(list((1 / r) for r, s, b in costs.values()))
            profit_scopes = np.array(list((1 / s) for r, s, b in costs.values()))
            profit_bufsizes = np.array(list((1 / b) for r, s, b in costs.values()))
            # normalize profits
            profit_runtimes = profit_runtimes / np.sum(profit_runtimes)
            profit_scopes = profit_scopes / np.sum(profit_scopes)
            profit_bufsizes = profit_bufsizes / np.sum(profit_bufsizes)
            profits = 0.33 * profit_runtimes + 0.33 * profit_scopes + 0.34 * profit_bufsizes
            # randomly choose among candidate
            sampled_edge_idx = np.random.choice(len(edges), p=profits)
            edge = edges[sampled_edge_idx]
            path.append(edge)
            if edge in node.subtree:
                node = node.subtree[edge]
            else:
                return tuple(path)
        return None


class SearchProbabilisticRSBP:
    def __init__(self, program, runtime):
        self.program = copy.deepcopy(program)
        self.seen_hashes = set()  # prevent looping
        self.tree_root = SearchNodeProbabilistic(runtime, self.find_edges_for_path([]))
        self.seen_hashes.add(self.program.program_hash())
    
    def find_edges_for_path(self, path):
        program_copy = program_for_path(self.program, path)
        if program_copy.program_hash() in self.seen_hashes:
            return tuple()
        edges = tuple(
            (tname, *targs)
            for tname, (tapply, tcheck) in TRANSFORMATIONS_LIST.items()
            for targs in tcheck(program_copy)
        )
        return edges
    
    def update(self, path, runtime, program_hash):
        assert len(path) > 0
        # find where to insert
        node = self.tree_root
        node.min_runtime = min(node.min_runtime, runtime)
        for tname_with_targs in path[:-1]:
            node = node.subtree[tname_with_targs]
            node.min_runtime = min(node.min_runtime, runtime)
        # insert
        edges = self.find_edges_for_path(path)
        self.seen_hashes.add(program_hash)
        node.subtree[path[-1]] = SearchNodeProbabilistic(runtime, edges)

    def query(self):
        node = self.tree_root
        path = []
        while not self.tree_root.is_exhausted:
            # for every edge, assign cost
            costs = {}
            # first, process only edges that were already taken
            for edge, nested_node in node.subtree.items():
                if len(nested_node.edges) == 0:
                    continue  # there is nothing to explore and node was already considred
                if nested_node.is_exhausted:
                    continue  # do not consider fully explored nodes
                program_copy = program_for_path(self.program, path + [edge])
                num_scopes = num_scopes_in_program(program_copy)
                buf_sizes = bufsize_in_program(program_copy)
                parallelism_depth_val = parallelism_depth(program_copy)
                costs[edge] = (nested_node.min_runtime, num_scopes, buf_sizes, parallelism_depth_val)
            average_min_runtime = np.mean([r for r, s, b, p in costs.values()]) if costs else 1.0  # 1.0 is chosen as any constant except 0 is allowed
            # second add edges that were never taken
            for edge in node.edges:
                if edge in node.subtree:
                    continue  # already considered
                program_copy = program_for_path(self.program, path + [edge])
                num_scopes = num_scopes_in_program(program_copy)
                buf_sizes = bufsize_in_program(program_copy)
                parallelism_depth_val = parallelism_depth(program_copy)
                costs[edge] = (average_min_runtime, num_scopes, buf_sizes, parallelism_depth_val)
            if not costs:
                # we end up in fully explored node, mark the node as fully explored and restart
                node.is_exhausted = True
                node = self.tree_root
                path = []
                continue            
            edges = list(costs.keys())
            profit_runtimes = np.array(list((1 / r) for r, s, b, p in costs.values()))
            profit_scopes = np.array(list((1 / s) for r, s, b, p in costs.values()))
            profit_bufsizes = np.array(list((1 / b) for r, s, b, p in costs.values()))
            profit_par_depth = np.array(list(1 - p for r, s, b, p in costs.values()))
            # normalize profits
            profit_runtimes = profit_runtimes / np.sum(profit_runtimes)
            profit_scopes = profit_scopes / np.sum(profit_scopes)
            profit_bufsizes = profit_bufsizes / np.sum(profit_bufsizes)
            profit_par_depth = profit_par_depth / np.sum(profit_par_depth) if np.sum(profit_par_depth) else 1 / len(profit_par_depth)
            profits = 0.7 * profit_runtimes + 0.1 * profit_scopes + 0.1 * profit_bufsizes + 0.1 * profit_par_depth
            # randomly choose among candidate
            sampled_edge_idx = np.random.choice(len(edges), p=profits)
            edge = edges[sampled_edge_idx]
            path.append(edge)
            if edge in node.subtree:
                node = node.subtree[edge]
            else:
                return tuple(path)
        return None


class SearchProbabilisticRSBP:
    def __init__(self, program, runtime):
        self.program = copy.deepcopy(program)
        self.seen_hashes = set()  # prevent looping
        self.tree_root = SearchNodeProbabilistic(runtime, self.find_edges_for_path([]))
        self.seen_hashes.add(self.program.program_hash())
    
    def find_edges_for_path(self, path):
        program_copy = program_for_path(self.program, path)
        if program_copy.program_hash() in self.seen_hashes:
            return tuple()
        edges = tuple(
            (tname, *targs)
            for tname, (tapply, tcheck) in TRANSFORMATIONS_LIST.items()
            for targs in tcheck(program_copy)
        )
        return edges
    
    def update(self, path, runtime, program_hash):
        assert len(path) > 0
        # find where to insert
        node = self.tree_root
        node.min_runtime = min(node.min_runtime, runtime)
        for tname_with_targs in path[:-1]:
            node = node.subtree[tname_with_targs]
            node.min_runtime = min(node.min_runtime, runtime)
        # insert
        edges = self.find_edges_for_path(path)
        self.seen_hashes.add(program_hash)
        node.subtree[path[-1]] = SearchNodeProbabilistic(runtime, edges)

    def query(self):
        node = self.tree_root
        path = []
        while not self.tree_root.is_exhausted:
            # for every edge, assign cost
            costs = {}
            # first, process only edges that were already taken
            for edge, nested_node in node.subtree.items():
                if len(nested_node.edges) == 0:
                    continue  # there is nothing to explore and node was already considred
                if nested_node.is_exhausted:
                    continue  # do not consider fully explored nodes
                program_copy = program_for_path(self.program, path + [edge])
                num_scopes = num_scopes_in_program(program_copy)
                buf_sizes = bufsize_in_program(program_copy)
                parallelism_depth_val = parallelism_depth(program_copy)
                costs[edge] = (nested_node.min_runtime, num_scopes, buf_sizes, parallelism_depth_val)
            average_min_runtime = np.mean([r for r, s, b, p in costs.values()]) if costs else 1.0  # 1.0 is chosen as any constant except 0 is allowed
            # second add edges that were never taken
            for edge in node.edges:
                if edge in node.subtree:
                    continue  # already considered
                program_copy = program_for_path(self.program, path + [edge])
                num_scopes = num_scopes_in_program(program_copy)
                buf_sizes = bufsize_in_program(program_copy)
                parallelism_depth_val = parallelism_depth(program_copy)
                costs[edge] = (average_min_runtime, num_scopes, buf_sizes, parallelism_depth_val)
            if not costs:
                # we end up in fully explored node, mark the node as fully explored and restart
                node.is_exhausted = True
                node = self.tree_root
                path = []
                continue            
            edges = list(costs.keys())
            profit_runtimes = np.array(list((1 / r) for r, s, b, p in costs.values()))
            profit_scopes = np.array(list((1 / s) for r, s, b, p in costs.values()))
            profit_bufsizes = np.array(list((1 / b) for r, s, b, p in costs.values()))
            profit_par_depth = np.array(list(1 - p for r, s, b, p in costs.values()))
            # normalize profits
            profit_runtimes = profit_runtimes / np.sum(profit_runtimes)
            profit_scopes = profit_scopes / np.sum(profit_scopes)
            profit_bufsizes = profit_bufsizes / np.sum(profit_bufsizes)
            profit_par_depth = profit_par_depth / np.sum(profit_par_depth) if np.sum(profit_par_depth) else 1 / len(profit_par_depth)
            profits = 0.7 * profit_runtimes + 0.1 * profit_scopes + 0.1 * profit_bufsizes + 0.1 * profit_par_depth
            # randomly choose among candidate
            sampled_edge_idx = np.random.choice(len(edges), p=profits)
            edge = edges[sampled_edge_idx]
            path.append(edge)
            if edge in node.subtree:
                node = node.subtree[edge]
            else:
                return tuple(path)
        return None


class SearchNode:
    def __init__(self, runtime, edges):
        self.runtime = runtime
        self.edges = edges  # edge = (tname, *targs)
        self.subtree = {}  # edge -> SearchNode


class SearchSimulatedAnnealing:
    def __init__(self, program, runtime):
        self.program = copy.deepcopy(program)
        self.current_path = []
        self.candidate_path = []
        # self.seen_hashes = set()  # prevent looping
        self.tree_root = SearchNode(runtime, self.find_edges_for_path([]))
        # self.seen_hashes.add(self.program.program_hash())
        self.temperature = 10.0
        self.total_steps = 100
        self.steps_left = self.total_steps
        self.cooling_factor = 0.99
    
    def find_edges_for_path(self, path):
        program_copy = program_for_path(self.program, path)
        # if program_copy.program_hash() in self.seen_hashes:
        #     return tuple()
        edges = tuple(
            (tname, *targs)
            for tname, (tapply, tcheck) in TRANSFORMATIONS_LIST.items()
            for targs in tcheck(program_copy)
        )
        return edges
    
    def update(self, path, runtime, program_hash):
        # sometimes there can be backtracking and the same paths may be inserted multiple times
        if len(path) == 0:
            return  # if it is root again, just ignore it
        # find where to insert
        node = node_for_path(self.tree_root, path[:-1])
        if path[-1] in node.subtree:
            return  # skip if already considered
        edges = self.find_edges_for_path(path)
        # self.seen_hashes.add(program_hash)
        node.subtree[path[-1]] = SearchNode(runtime, edges)

    def query(self):
        if self.steps_left <= 0:
            # restart search
            self.steps_left = self.total_steps
            self.current_path = []
            self.candidate_path = []
        if self.current_path == self.candidate_path:
            # we need to generate new candidate
            current_node = node_for_path(self.tree_root, self.current_path)
            if self.current_path:
                current_edges = [None, *current_node.edges]  # None means to go back to the root
            else:
                current_edges = current_node.edges
            # uniformly pick direction
            # print("Selecting from the list of available choices: ", current_edges)
            edge = random.choice(current_edges)
            # print("Current path:", self.current_path)
            # print("Selected candidate:", edge)
            if edge is None:
                candidate_path = self.current_path[:-1]
            else:
                candidate_path = self.current_path + [edge]
            self.candidate_path = candidate_path
        else:
            # we already have a candidate
            # we decide to pick it or reject it
            current_node = node_for_path(self.tree_root, self.current_path)
            candidate_node = node_for_path(self.tree_root, self.candidate_path)
            current_energy = current_node.runtime
            candidate_energy = candidate_node.runtime
            # accept or reject
            if candidate_energy < current_energy:
                # print("Candidate accepted (lower energy)")
                # accept lower energy unconditionally
                self.current_path = self.candidate_path
            elif random.random() < np.exp((current_energy - candidate_energy) / self.temperature):
                # print("Candidate accepted (higher energy)")
                # accept higher energy
                self.current_path = self.candidate_path
            else:
                # print("Candidate rejected")
                # reject higher energy
                self.candidate_path = self.current_path
            # update temperature and steps
            self.temperature *= self.cooling_factor
            self.steps_left -= 1
        print('steps left:', self.steps_left)
        return self.candidate_path


def run_search(program, ref_data, min_repeats, searcher):
    
    cache = RuntimeCache('cached_runtime.db')
    
    runtime_ms, runtime_std = eval_program(program, ref_data, min_repeats, 5000, cache)
    ss = searcher(program=program, runtime=runtime_ms)
    print(f"Initial runtime [ms]: avg {runtime_ms:.6f} std {runtime_std:.6f}")
    best_runtime_ms = runtime_ms
    
    timeout_ms = 1000
    
    while True:
        transformation_sequence = ss.query()
        if transformation_sequence is None:
            break
        new_program = copy.deepcopy(program)
        for tname, *targs in transformation_sequence:
            tapply, _ = TRANSFORMATIONS_LIST[tname]
            tapply(new_program, *targs)
        
        new_program_hash = new_program.program_hash()
        
        print("Transformations: ", end="")
        print("; ".join(f"{tname_path}(program, {args_as_str(targs_path)})" for tname_path, *targs_path in transformation_sequence))
        print(new_program.text())

        # timeout_ms = 2 * best_runtime_ms        
        #timeout_ms = float('inf')
        timeout_ms = 1000
        runtime_ms, runtime_std = eval_program(new_program, ref_data, min_repeats, timeout_ms, cache)

        ss.update(transformation_sequence, runtime_ms, new_program_hash)        

        if runtime_ms < best_runtime_ms:
            print(f"Cost improved. Old: {best_runtime_ms} -> New: {runtime_ms}")
            best_runtime_ms = runtime_ms
    
    print("Beam search exhausted.")


def run_kernel(kernel_creator, size_str, min_repeats, min_seconds, timeout_ms, Search, Cost, Candidate, cache, timeout_adjust, verify, reinit_input):
    params = kernel_creator.parse_params(size_str)
    program = kernel_creator.create_kernel(params)
    ref_data = kernel_creator.create_reference_data(params)
    kernel_creator.evaluate_baseline(ref_data, min_repeats)
    evaluator = Evaluator(ref_data, min_repeats, min_seconds, timeout_ms, RuntimeCache(cache), timeout_adjust, verify, reinit_input)
    cost = Cost(evaluator)
    candidate = Candidate()
    search = Search(program, ref_data, min_repeats, candidate, cost)
    search.run()


def run_layernorm(M, N, min_repeats, searcher):
    program_text = f"""
        Name: layernorm
        In: src, gamma, beta
        Out: dst
        Declarations:
            src f32 [{M}, {N}] heap
            gamma f32 [{N}] heap
            beta f32 [{N}] heap
            dst f32 [{M}, {N}] heap
            m f32 [{M}] heap
            mu f32 [{M}] heap
            diff f32 [{M}, {N}] heap
            q f32 [{M}] heap
            tmp f32 [{M}, {N}] heap
            qeps f32 [{M}] heap
            sigma f32 [{M}] heap
        Code:
        {M} {N} m[{{0}}] += src[{{0}}, {{1}}]
        {M} mu[{{0}}] = m[{{0}}] / {N}
        {M} {N} diff[{{0}}, {{1}}] = src[{{0}}, {{1}}] - mu[{{0}}]
        {M} {N} q[{{0}}] += diff[{{0}}, {{1}}] * diff[{{0}}, {{1}}]
        {M} {N} tmp[{{0}}, {{1}}] = diff[{{0}}, {{1}}] * gamma[{{1}}]
        {M} qeps[{{0}}] = q[{{0}}] * {1./(N - 1.)} + 0.00001
        {M} sigma[{{0}}] = 1 / sqrtf(qeps[{{0}}])
        {M} {N} dst[{{0}}, {{1}}] = sigma[{{0}}] * tmp[{{0}}, {{1}}] + beta[{{1}}]
    """
    
    program = parse_program(program_text)
    print(program.text())
    
    src = np.random.uniform(-1, 1, (M, N)).astype(np.float32)
    gamma = np.random.uniform(-1, 1, (N,)).astype(np.float32)
    beta = np.random.uniform(-1, 1, (N,)).astype(np.float32)
    
    tsrc = torch.from_numpy(src)
    tgamma  = torch.from_numpy(gamma)
    tbeta = torch.from_numpy(beta)
    
    warmup = math.ceil(0.3 * min_repeats)
    pt_runtime = timeit.repeat(lambda: torch.nn.functional.layer_norm(tsrc, [N], weight=tgamma, bias=tbeta, eps=1e-5), repeat=warmup+min_repeats, number=1)[warmup:]
    print(f"Pytorch layernorm time [ms]: avg {np.mean(pt_runtime) * 1e3:.6f} std {np.std(pt_runtime) * 1e3:.6f}")
    
    
    tdst = torch.nn.functional.layer_norm(tsrc, [N], weight=tgamma, bias=tbeta, eps=1e-5)
    dst = tdst.numpy()
    
    ref_data = {'src': src, 'gamma': gamma, 'beta': beta, 'dst': dst}
    
    run_search(program, ref_data, min_repeats=min_repeats, searcher=searcher)


def run_conv(N, M, C, H_in, W_in, K, min_repeats, searcher):
    H = pooling_out_dim(input=H_in, kernel=K, stride=1, pad=0, dilation=1)
    W = pooling_out_dim(input=W_in, kernel=K, stride=1, pad=0, dilation=1)
    
    program_text = f"""
        Name: conv2d
        In: input, weight
        Out: output
        Declarations:
            input f32 [{N}, {C}, {H_in}, {W_in}] heap
            weight f32 [{M}, {C}, {K}, {K}] heap
            output f32 [{N}, {M}, {H}, {W}] heap
        Code:
        {N} {M} {H} {W} {C} {K} {K} output[{{0}}, {{1}}, {{2}}, {{3}}] += input[{{0}}, {{4}}, {{2}} + {{5}}, {{3}} + {{6}}] * weight[{{1}}, {{4}}, {{5}}, {{6}}]
    """
    
    program = parse_program(program_text)
    
    print(program.text())
    
    input = np.random.uniform(-1, 1, (N, C, H_in, W_in)).astype(np.float32)
    weight = np.random.uniform(-1, 1, (M, C, K, K)).astype(np.float32)
    tinput = torch.from_numpy(input)
    tweight = torch.from_numpy(weight)
    toutput = torch.nn.functional.conv2d(tinput, tweight)
    output = toutput.numpy()
        
    pt_builtin_runtime = timeit.repeat(lambda: torch.nn.functional.conv2d(tinput, tweight), repeat=10, number=1)[3:]
    print(f"Pytorch builtin conv2d time [ms]: avg {np.mean(pt_builtin_runtime) * 1e3:.6f} std {np.std(pt_builtin_runtime) * 1e3:.6f}")
    
    ref_data = {'input': input, 'weight': weight, 'output': output}
    
    run_search(program, ref_data, min_repeats=min_repeats, searcher=searcher)


def run_log_softmax(M, N, min_repeats, searcher):
    program_text = f"""
        Name: func_log_softmax
        In: x
        Out: z
        Declarations:
            x f32 [{M}, {N}] heap
            z f32 [{M}, {N}] heap
            max_val f32 [{M}] heap
            diff f32 [{M}, {N}] heap
            exp_diff f32 [{M}, {N}] heap
            sum_val f32 [{M}] heap
            ln_sum f32 [{M}] heap
        Code:
        {M} {N} max_val[{{0}}] fmaxf= (x[{{0}}, {{1}}])
        {M} {N} diff[{{0}}, {{1}}] = x[{{0}}, {{1}}] - max_val[{{0}}]
        {M} {N} exp_diff[{{0}}, {{1}}] = expf(diff[{{0}}, {{1}}])
        {M} {N} sum_val[{{0}}] += exp_diff[{{0}}, {{1}}]
        {M} ln_sum[{{0}}] = logf(sum_val[{{0}}])
        {M} {N} z[{{0}}, {{1}}] = diff[{{0}}, {{1}}] - ln_sum[{{0}}]
    """
    
    program = parse_program(program_text)
    print(program.text())
    
    x = np.random.uniform(-10, 10, (M, N)).astype(np.float32)
    tx = torch.from_numpy(x)
    tz = torch.nn.functional.log_softmax(tx, dim=1)
    z = tz.numpy()
    
    warmup = math.ceil(0.3 * min_repeats)
    pt_builtin_runtime = timeit.repeat(lambda: torch.nn.functional.log_softmax(tx, dim=1), repeat=warmup+min_repeats, number=1)[warmup:]
    print(f"Pytorch log_softmax time [ms]: avg {np.mean(pt_builtin_runtime) * 1e3:.6f} std {np.std(pt_builtin_runtime) * 1e3:.6f}")
    
    run_search(program, {'x': x, 'z': z}, min_repeats=min_repeats, searcher=searcher)


def run_reducemean(M, N, min_repeats, searcher):
    program_text = f"""
        Name: reducemean
        In: x
        Out: z
        Declarations:
            x f32 [{M}, {N}] heap
            t f32 [{M}] heap
            z f32 [{M}] heap
        Code:
        {M} {N} t[{{0}}] += x[{{0}}, {{1}}]
        {M} z[{{0}}] = t[{{0}}] / {N}
    """
    
    program = parse_program(program_text)
    print(program.text())
    
    x = np.random.uniform(-10, 10, (M, N)).astype(np.float32)
    tx = torch.from_numpy(x)
    z = np.mean(x, axis=1)
    
    warmup = math.ceil(0.3 * min_repeats)
    pt_runtime = timeit.repeat(lambda: torch.mean(tx, dim=1), repeat=warmup+min_repeats, number=1)[warmup:]
    print(f"Pytorch mean time [ms]: avg {np.mean(pt_runtime) * 1e3:.6f} std {np.std(pt_runtime) * 1e3:.6f}")

    ref_data = {'x': x, 'z': z}
    run_search(program, ref_data, min_repeats=min_repeats, searcher=searcher)


class BaseSearch:
    def __init__(self, program, ref_data, min_repeats, candidate, cost):
        self.program = program
        self.ref_data = ref_data
        self.min_repeats = min_repeats
        self.candidate = candidate
        self.cost = cost


class GreedySearch(BaseSearch):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        
        self.seen_paths = {}  # path -> cost
        self.explored_paths = set()
        self.seen_candidates = {}  # hash -> path, avoid alternative paths for same program

    def run(self):
        current_path = []
        cost = self.cost.eval(self.program, current_path)
        self.seen_paths[tuple(current_path)] = cost
        while True:
            candidates = self.candidate.query(self.program, current_path)
            filtered_candidates = []
            for c in candidates:
                c_tuple = tuple(c)
                c_hash = program_for_path(self.program, c).program_hash()
                if c_hash not in self.seen_candidates:
                    self.seen_candidates[c_hash] = c
                else:
                    existing_path = self.seen_candidates[c_hash]
                    if c_tuple != existing_path:
                        # different paths for same hash, skipping
                        continue
                filtered_candidates.append(c)
            candidates = filtered_candidates
            for c in candidates:
                candidate_cost = self.cost.eval(self.program, c)
                self.seen_paths[tuple(c)] = candidate_cost
            self.explored_paths.add(tuple(current_path))
            sorted_paths = sorted([(t, p) for (p, t) in self.seen_paths.items() if p not in self.explored_paths])
            if not sorted_paths:
                break
            current_path = list(sorted_paths[0][1])
        print("Search exhausted.")


class GreedyTopSearch(BaseSearch):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        
        self.unexplored_hashes = {}  # hash -> (path, parent_cost)
        self.explored_hashes = set()  # hashes that should not be explored again

    def run(self):
        current_path = []
        self.unexplored_hashes[program_for_path(self.program, current_path).program_hash()] = (current_path, -1)  # add root
        iteration = 0
        best_cost = float('inf')
        best_iter = 0
        while True:
            # evaluate current path
            cost = self.cost.eval(self.program, current_path)
            current_hash = program_for_path(self.program, current_path).program_hash()
            self.explored_hashes.add(current_hash)  # mark currrent path as explored
            del self.unexplored_hashes[current_hash]  # remove current path from candidates to explore
            
            print(f"GreedyTopSearch cost {cost} at iteration {iteration} (best {best_cost} at iteration {best_iter})")
            if cost < best_cost:
                best_cost = cost
                best_iter = iteration
            iteration += 1
            
            # find new candidates
            candidates = self.candidate.query(self.program, current_path)
            for c_path in candidates:
                c_hash = program_for_path(self.program, c_path).program_hash()
                if c_hash in self.explored_hashes:
                    continue                    
                if c_hash not in self.unexplored_hashes:
                    # new candidate found
                    self.unexplored_hashes[c_hash] = (c_path, cost)
                else:
                    # candidate already seen but not explored
                    # update its cost if candidate reached through parent with lower cost
                    seen_path, seen_parent_cost = self.unexplored_hashes[c_hash]
                    if cost < seen_parent_cost:
                        self.unexplored_hashes[c_hash] = (c_path, cost)
                        
            # pick next candidate
            if not self.unexplored_hashes:
                break  # no more candidates to explore
            costs = [c for h, (p, c) in self.unexplored_hashes.items()]
            probs = torch.softmax(-torch.tensor(costs, dtype=torch.float32), dim=0)
            [current_path] = random.choices([p for h, (p, c) in self.unexplored_hashes.items()], weights=probs)
            current_path = list(current_path)  # cast from tuple to list
            # remove unexplored paths that have low probability of being selected
            highest_prob = probs.max().item()
            new_unexplored_hashes = {}
            for (h, (p, c)), prob in zip(self.unexplored_hashes.items(), probs):
                if prob > 0.05 * highest_prob:
                    new_unexplored_hashes[h] = (p, c)
                    self.explored_hashes.add(h)
        print("Search exhausted.")
        
class NoSearch(BaseSearch):
    def run(self):
        candidates = self.candidate.query(self.program, [])
        current_path = candidates[0]
        cost_base = self.cost.eval(self.program, [])
        print(f"Cost without search (default): {cost_base:.6f}")
        cost = self.cost.eval(self.program, current_path)
        print(f"Cost without search (first candidate): {cost:.6f}")


class HeuristicPass(BaseSearch):
    def run(self):
        candidates = self.candidate.query(self.program, [])
        current_path = candidates[0]
        cost_base = self.cost.eval(self.program, [])
        print(f"Cost without search (default): {cost_base:.6f}")
        cost = self.cost.eval(self.program, current_path)
        print(f"Cost without search (first candidate): {cost:.6f}")


class SimulatedAnnealingSearch(BaseSearch):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        
        self.temperature = 10.0
        self.total_steps = 100
        self.cooling_factor = 0.99
        
    def run(self):
        while True:
            current_path = []
            current_energy = self.cost.eval(self.program, current_path)
            current_temperature = self.temperature
            for _ in range(self.total_steps):
                candidate_paths = self.candidate.query(self.program, current_path)
                # uniformly pick direction
                candidate_path = random.choice(candidate_paths)
                # we decide to pick candidate or reject it
                candidate_energy = self.cost.eval(self.program, candidate_path)
                # accept or reject
                if candidate_energy < current_energy:
                    # print("Candidate accepted (lower energy)")
                    # accept lower energy unconditionally
                    current_path = candidate_path
                elif random.random() < np.exp((current_energy - candidate_energy) / current_temperature):
                    # print("Candidate accepted (higher energy)")
                    # accept higher energy
                    current_path = candidate_path
                else:
                    # print("Candidate rejected")
                    # reject higher energy
                    pass
                # update temperature and steps
                current_temperature *= self.cooling_factor


class MCTSNode:
    def __init__(self, cost, num_subtrees):
        self.cost = cost
        self.subtree_costs = []
        self.num_subtrees = num_subtrees
        self.taken_subtrees = 0


class MCTSearch(BaseSearch):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.nodes = {}  # transformation_sequence -> MCTSNode
        self.seen_candidates = {}  # hash -> transformation_sequence, to keep search graph as a tree

    def run(self):
        # before starting search, evaluate root node
        cost = self.cost.eval(self.program, [])
        # create entry for it
        self.nodes = {tuple(): MCTSNode(cost, len(self.candidate.query(self.program, [])))}
        while True:  # this loop starts new search from root
            current_path = []
            visited_nodes = [self.nodes[tuple(current_path)]]
            while True:  # this loop goes until the first unvisited node is found
                candidates = self.candidate.query(self.program, current_path)
                unique_candidates = []
                for c in candidates:
                    c_tuple = tuple(c)
                    c_hash = program_for_path(self.program, c).program_hash()
                    if c_hash not in self.seen_candidates:
                        self.seen_candidates[c_hash] = c
                    else:
                        existing_path = self.seen_candidates[c_hash]
                        if c_tuple != existing_path:
                            # different paths for same hash, skipping
                            continue
                    unique_candidates.append(c)
                candidates = unique_candidates
                if not candidates:
                    print("No candidates, Restarting from root")
                    break  # can't go further, restart from root
                candidate_costs = []
                for c in candidates:
                    c_tuple = tuple(c)
                    if c_tuple in self.nodes:
                        candidate_costs.append(self.nodes[c_tuple].cost)
                    else:
                        candidate_costs.append(None)
                # replace unknown costs with average of known costs
                known_costs = [c for c in candidate_costs if c is not None]
                known_costs = known_costs or [1.0]
                average_cost = sum(known_costs) / len(known_costs)
                for i, c in enumerate(candidate_costs):
                    if c is None:
                        candidate_costs[i] = average_cost
                # compute probabilities based on costs
                candidate_probs = [1 / c for c in candidate_costs]
                # normallize probabilities
                probs_sum = sum(candidate_probs)
                candidate_probs = [p / probs_sum for p in candidate_probs]
                # sample candidate
                [candidate_idx] = random.choices(list(range(len(candidate_probs))), weights=candidate_probs)
                current_path = candidates[candidate_idx]
                if tuple(current_path) in self.nodes:
                    # this node was already evaluated, proceed into it
                    candidate_node = self.nodes[tuple(current_path)]
                    visited_nodes.append(candidate_node)
                else:
                    # this node is new, create it
                    candidate_cost = self.cost.eval(self.program, current_path)
                    candidate_node = MCTSNode(candidate_cost, len(self.candidate.query(self.program, current_path)))
                    self.nodes[tuple(current_path)] = candidate_node
                    # update nodes from root to current with costs
                    for node in visited_nodes:
                        node.subtree_costs.append(candidate_cost)
                        node.cost = min(node.cost, min(node.subtree_costs))
                    # number of taken subtrees of parent node should be increased by one
                    visited_nodes[-1].taken_subtrees += 1
                    break  # restart from root


class EdgesCandidate:
    def __init__(self):
        pass
    
    def query(self, base_program, path):
        program = program_for_path(base_program, path)
        edges = [
            (tname, *targs)
            for tname, (tapply, tcheck) in TRANSFORMATIONS_LIST.items()
            for targs in tcheck(program)
        ]
        candidates = [path + [e] for e in edges]
        if path:
            candidates.append(path[:-1])
        return candidates


# nca python -m pdb -c c heuristic_search_new.py --kernel matmul --size=768x1024x1024 --min_repeats 5 --search annealing --candidate edges_backtrack --cost runtime
#random (no good heuristic)
HEURISTIC_TRANSFORMATIONS_LIST1 = {
    # "swap_ops": (swap_ops, find_swappable_ops),
    # "tile_scope": (tile_scope, find_tileable_scopes),
    # "create_operand_temporary": (create_operand_temporary, find_creatable_operand_temporaries),
    "swap_nested_scopes": (swap_nested_scopes, find_swappable_nested_scopes),
    # "create_dimension": (create_dimension, find_creatable_dimensions),
}

# exhaustive
HEURISTIC_TRANSFORMATIONS_LIST2 = {
    "tile_buffer": (tile_buffer, find_tileable_buffers),
    "join_scopes": (join_scopes, find_joinable_scopes),
    "reuse_arr_dims": (reuse_arr_dims, find_reusable_arr_dims),
    "move_buf_to_stack": (move_buf_to_stack, find_bufs_movable_to_stack),
}

# random (has heuristic)
HEURISTIC_TRANSFORMATIONS_LIST3 = {
    "swap_buffer_dims": (swap_buffer_dims, find_swappable_buffer_dims),
}

# exhaustive
HEURISTIC_TRANSFORMATIONS_LIST4 = {   
    "reuse_buffers": (reuse_buffers, find_reusable_buffers),
}

# random (has heuristic)
HEURISTIC_TRANSFORMATIONS_LIST5 = {   
    # "unroll_scope": (unroll_scope, find_unrollable_scopes),
    "parallelize_scope": (parallelize_scope, find_parallelizable_scopes),
}


def apply_exhaustive_transform_chain(program, transforms_list):
    """
        Modifies the program in place. Returns the list of transformations applied.
    """
    
    transform_chain = []
        
    still_transforming = True
    while still_transforming:
        still_transforming = False
        for tname, (transform_func, find_transforms_func) in transforms_list.items():
            transforms = find_transforms_func(program)
            transforms = sorted(transforms)
            if not transforms:
                continue
            args = transforms.pop()
            transform_chain.append((tname, *args))
            transform_func(program, *args)
            still_transforming = True
            break
    
    return transform_chain


def find_edges_for_program(program, transforms_list):
    edges = [
        (tname, *targs)
        for tname, (tapply, tcheck) in transforms_list.items()
        for targs in tcheck(program)
    ]
    return edges


def remove_bad_parallelization_candidates(program, candidates):
    """ 
        Good candidates usually
        - have more iterations than number of cores
        - appear closer to the root of the scope nest rather than leaf
    """
    result = []
    for (scope_name,) in candidates:
        keep_candidate = True
        for scope_nest, op in recursive_scopes_with_ops(program):
            total_depth = len(scope_nest)
            for scope_depth, scope in enumerate(scope_nest):
                if scope_name != scope.name:
                    continue
                num_threads = int(os.environ.get('OMP_NUM_THREADS', 1))
                if scope.dim.value < num_threads * 0.75:
                    keep_candidate = False
                    break
                if scope_depth > 0.5 * total_depth:
                    keep_candidate = False
                    break
            if not keep_candidate:
                break
        if keep_candidate:
            p, i = find_scope_by_name(program, scope_name)
            result.append((scope_name,))
    return result


def find_scope_tiling_candidates(program, current_path):
    initial_path = []
    first_candidate = current_path == initial_path
    
    tiling_candidates = find_tileable_scopes(program, initial_path)
    # filter out scopes with small number of iterations
    scope_sizes = {}  # scope name -> size
    for s, t in tiling_candidates:
        scope_parent, scope_idx = find_scope_by_name(program, s)
        scope = scope_parent.ops[scope_idx]
        scope_sizes[s] = scope.dim.value
        
    # tile up to 4 biggest scopes per scope nest
    scope_sizes = sorted(scope_sizes.items(), key=lambda x: -x[1])
    tilings_per_nest = {}  # op_name in scope -> num tiled scopes
    scopes_to_tile = set()
    for scope_name, _ in scope_sizes:
        op_name, scope_depth = scope_name.split('#')
        tilings_per_nest.setdefault(op_name, 0)
        if tilings_per_nest[op_name] >= 4:
            continue
        tilings_per_nest[op_name] += 1
        scopes_to_tile.add(scope_name)
    
    filtered_candidates = []
    for scope_name, tile_size in tiling_candidates:
        if scope_name not in scopes_to_tile:
            continue
        scope_parent, scope_idx = find_scope_by_name(program, scope_name)
        scope = scope_parent.ops[scope_idx]
        if scope.dim.value < 20:
            continue
        outer_tile_size = scope.dim.value // tile_size
        if (outer_tile_size <= 10 or tile_size <= 10):
            continue
        filtered_candidates.append((scope_name, tile_size))
    tiling_candidates = filtered_candidates
    
    # we need to process candidates from outermost to innermost to make sure that scope names are still valid candidates after transformations
    tiling_candidates = sorted(tiling_candidates, key=lambda x: -int(x[0].split('#')[1]))
    tiling_dict = {}  # maps scope name to the list of tile sizes
    for (scope_name, tile_size) in tiling_candidates:
        tiling_dict.setdefault(scope_name, []).append(tile_size)
    
    candidates = []
    if first_candidate:
        # for the path initialization we generate only one heuristically chosen candidate
        # now build initial sequence of transformations
        candidate = []
        for scope_name, tile_sizes in tiling_dict.items():
            sorted_sizes = sorted(tile_sizes)
            if len(sorted_sizes) > 1:
                # among the tile_size candidates, we select candidate strictly larger than median (heuristic)
                tile_size = sorted_sizes[math.ceil(len(sorted_sizes) / 2)]
            else:
                tile_size = sorted_sizes[0]
            candidate.append(('tile_scope', scope_name, tile_size))
        if candidate:
            candidates.append(candidate)
        assert all(len(c) > len(current_path) for c in candidates)

    else:
        # if path is already initialized, we try to slightly adjust tiling transformations while keeping other transformations the same
        # for this, we find tiling candidates as if we were starting from scratch
        
        # for each scope tiled in existing path, find chosen tile size
        existing_scope_to_size = {}
        for tname, *targs in current_path:
            if tname == 'tile_scope':
                scope_name, tile_size = targs
                existing_scope_to_size[scope_name] = tile_size
        
        # now suggest up to two tile sizes for each loop, higher and lower than the current one
        for scope_name, tile_sizes in tiling_dict.items():
            [index_in_current_path] = [i for i, (tname, *targs) in enumerate(current_path) if tname == 'tile_scope' and targs[0] == scope_name]
            sorted_sizes = sorted(tile_sizes)
            existing_size = existing_scope_to_size[scope_name]
            existing_size_idx = sorted_sizes.index(existing_size)
            if existing_size_idx < len(sorted_sizes) - 1:
                size_higher = sorted_sizes[existing_size_idx + 1]
                candidate = current_path[:]
                candidate[index_in_current_path] = ('tile_scope', scope_name, size_higher)
                candidates.append(candidate)
            if existing_size_idx < len(sorted_sizes) - 2:
                size_higher = sorted_sizes[existing_size_idx + 2]
                candidate = current_path[:]
                candidate[index_in_current_path] = ('tile_scope', scope_name, size_higher)
                candidates.append(candidate)
            if existing_size_idx > 0:
                size_lower = sorted_sizes[existing_size_idx - 1]
                candidate = current_path[:]
                candidate[index_in_current_path] = ('tile_scope', scope_name, size_lower)
                candidates.append(candidate)
            if existing_size_idx > 1:
                size_lower = sorted_sizes[existing_size_idx - 2]
                candidate = current_path[:]
                candidate[index_in_current_path] = ('tile_scope', scope_name, size_lower)
                candidates.append(candidate)
        assert all(len(c) == len(current_path) for c in candidates)
    
    assert all(c != current_path for c in candidates)
    assert len(set(tuple(c) for c in candidates)) == len(candidates)
    
    return candidates


def is_scope_permutation_candidate_valid(original_tile_indices, inner_tile_indices, perm, premuted_reduction_idx):
    # innermost reduce dimension should be surrounded by non-reduce dimensions
    reduction_indices = [i for i, is_reduction in enumerate(premuted_reduction_idx) if is_reduction]
    if reduction_indices:
        smallest_reduction_idx = reduction_indices[0]
        if smallest_reduction_idx != 0:
            if smallest_reduction_idx - 1 in reduction_indices:
                return False
        if smallest_reduction_idx != len(premuted_reduction_idx) - 1:
            if smallest_reduction_idx + 1 in reduction_indices:
                return False
    # it should no have consequitive dimensions referring to the same outer/inner tile
    for i in range(1, len(perm)):
        if perm[i - 1] in original_tile_indices and perm[i] in inner_tile_indices:
            return False
    # it should keep the same relative order for all outer and inner tiles
    for inner_tile_index in inner_tile_indices:
        outer_tile_index = inner_tile_index + 1
        if perm.index(inner_tile_index) > perm.index(outer_tile_index):
            return False
    return True


def generate_heuristic_scope_permutation_candidate(original_tile_indices, inner_tile_indices, is_reduction_idx, scope_sizes):
    """Generates list of indicies that define the permutation of scopes in the scope nest.
    All inputs and outputs refer to the innermost scope as 0.
    """
    assert len(original_tile_indices) + len(inner_tile_indices) == len(is_reduction_idx)
    assert set(original_tile_indices) & set(inner_tile_indices) == set()
    assert len(set(original_tile_indices) | set(inner_tile_indices))
    permutation_len = len(is_reduction_idx)
    assert all(i < permutation_len for i in original_tile_indices)
    assert all(i < permutation_len for i in inner_tile_indices)
    is_inner_tile_idx = [i in inner_tile_indices for i in range(permutation_len)]
    num_reductions = sum(is_reduction_idx)
    num_non_reductions = permutation_len - num_reductions
    
    # we sequentially try match different patterns, if some of them ends up in valid permutation, we return it
    # if none of the patterns is valid, we return the original permutation
    # all patterns from innermost to outermost scope

    desired_order = inner_tile_indices + original_tile_indices
    desired_order = sorted(desired_order, key=lambda x: (not is_inner_tile_idx[x], not is_reduction_idx[x], scope_sizes[x]))
    
    # pattern NNRNR...N
    if num_reductions >= 2 and num_non_reductions >= 4:
        permutation = []
        while True:
            for s in desired_order:
                if s in permutation:
                    continue  # already taken
                if (len(permutation) in {0, 1, 3} or len(permutation) == permutation_len - 1) and (not is_reduction_idx[s]):
                    permutation.append(s)
                    break
                if (len(permutation) in {2, 4}) and is_reduction_idx[s]:
                    permutation.append(s)
                    break
                if 5 <= len(permutation) and len(permutation) < permutation_len - 1:
                    permutation.append(s)
                    break
            else:
                # this is exectured if we didn't find a candidate on current iteration
                break
        if len(permutation) == permutation_len:
            return permutation
    
    # pattern NNR...N
    if num_reductions >= 1 and num_non_reductions >= 3:
        permutation = []
        while True:
            for s in desired_order:
                if s in permutation:
                    continue  # already taken
                if (len(permutation) in {0, 1} or len(permutation) == permutation_len - 1) and (not is_reduction_idx[s]):
                    permutation.append(s)
                    break
                if (len(permutation) == 2) and is_reduction_idx[s]:
                    permutation.append(s)
                    break
                if 3 <= len(permutation) and len(permutation) < permutation_len - 1:
                    permutation.append(s)
                    break
            else:
                # this is exectured if we didn't find a candidate on current iteration
                break
        if len(permutation) == permutation_len:
            return permutation
    
    # pattern R...N
    if num_reductions >= 1 and num_non_reductions >= 1:
        permutation = []
        while True:
            for s in desired_order:
                if s in permutation:
                    continue  # already taken
                if (len(permutation) == 0) and is_reduction_idx[s]:
                    permutation.append(s)
                    break
                if (len(permutation) == permutation_len - 1) and (not is_reduction_idx[s]):
                    permutation.append(s)
                    break
                if 1 <= len(permutation) and len(permutation) < permutation_len - 1:
                    permutation.append(s)
                    break
            else:
                # this is exectured if we didn't find a candidate on current iteration
                break
        if len(permutation) == permutation_len:
            return permutation

    return list(range(permutation_len))
        


def find_swap_nested_scopes_candidates(program, current_path, is_initial):
    # we undo non-tiling transformations in current path to explore alternative swap candidates
    initial_path = []
    for tname, *targs in current_path:
        if tname == 'tile_scope':
            initial_path.append((tname, *targs))
        else:
            break

    initial_program = program_for_path(program, initial_path)
    scopes_with_reduction = find_scopes_with_reduction(initial_program)
    
    # we need to build a view on inner and outer tiles created by earlier transformations
    inner_tile_indices = {}  # op_name -> [scope_depth], contain new inner scopes generated by tiling
    original_tile_indices = {}  # op_name -> [scope_depth], contain non-tiled scopes and outer tiled scopes
    for scope_nest, op in recursive_scopes_with_ops(initial_program):
        # find transforms applied so far to know which loops relate to each other
        tiling_depths = []  # [int], contain depths of scopes (in initial program) to which tiling was applied
        for tname, scope_name, tile_size in initial_path:
            op_name, scope_depth = scope_name.split('#')
            if op_name == op.name:
                tiling_depths.append(int(scope_depth))
        # discovered tiling_depths refer to the scopes before tiling, we need to infer how they relate to the current scope nest
        # 2, 1, 0 -> 5 4 | 3 2 | 1 0
        # 2, 0 -> 4 3 | 2 | 1 0
        tiling_depths = sorted(tiling_depths)
        inner_tile_indices[op.name] = []
        original_tile_indices[op.name] = []
        adjusted_index = 0
        current_index = 0
        while current_index < len(scope_nest):
            if current_index - adjusted_index in tiling_depths:
                # this is inner tile, it adds +1 to the number of scopes
                inner_tile_indices[op.name].append(current_index)
                adjusted_index += 1
                original_tile_indices[op.name].append(current_index + 1)  # add outer tile
                current_index += 2  # move to the next scope to consider
            else:
                original_tile_indices[op.name].append(current_index)
                current_index += 1

    candidates = []
    if is_initial:
        # generating initial candidates
        # we go through each scopes nest and for each scope nest try to find the best permutation
        candidate_extension = []
        for scope_nest, op in recursive_scopes_with_ops(initial_program):
            perm_base = list(range(len(scope_nest)))  # base permutation, which has no effect
            # we process scopes from innermost to outermost, picking the candidate either from inner or original tile according to heuristic
            is_reduction_idx = [(s.name in scopes_with_reduction) for s in reversed(scope_nest)]
            scope_sizes = [s.dim.value for s in reversed(scope_nest)]
            loop_nest_candidate = generate_heuristic_scope_permutation_candidate(
                original_tile_indices[op.name], inner_tile_indices[op.name], is_reduction_idx, scope_sizes
            )
            # let's convert it into the sequence of transformations, we reverse indices as build_loop_reordering_sequence accepts indices ascending from outermost to innermost
            nest_candidate = build_loop_reordering_sequence(op.name, list(reversed(perm_base)), list(reversed(loop_nest_candidate)))
            candidate_extension.extend(nest_candidate)
        if candidate_extension:
            candidates.append(initial_path[:] + candidate_extension)
    if not is_initial:
        # generating alternative candidates
        # we go thorugh each scope nest, determine current permutaiton, 
        # and try to find all (including non-adjacent) pairs of dimensions to swap
        # if swap is valid, we generate another candidate with such swap applied
        
        for scope_nest, op in recursive_scopes_with_ops(initial_program):
            perm_base = list(range(len(scope_nest)))
            # first we need to determine the current permutation
            perm_current = perm_base[:]
            for tname, *targs in current_path:
                if tname != 'swap_nested_scopes':
                    continue
                [scope_name] = targs
                [op_name, scope_depth] = scope_name.split('#')
                scope_depth = int(scope_depth)
                if op_name != op.name:
                    continue
                perm_current[scope_depth], perm_current[scope_depth-1] = perm_current[scope_depth-1], perm_current[scope_depth]
                
            # now generate all pairwise swaps that can be obtained from current permutation
            is_reduction_idx = [(s.name in scopes_with_reduction) for s in reversed(scope_nest)]
            valid_permutations = []
            for swp1 in range(len(perm_current)):
                for swp2 in range(0, swp1):
                    perm = perm_current[:]
                    perm[swp1], perm[swp2] = perm[swp2], perm[swp1]
                    premuted_reduction_idx = [is_reduction_idx[i] for i in perm]
                    # check if the permutation satisfies the constraints
                    valid = is_scope_permutation_candidate_valid(original_tile_indices[op.name], inner_tile_indices[op.name], perm, premuted_reduction_idx)
                    if valid:
                        valid_permutations.append(perm)
            if len(valid_permutations) < 30:
                valid_permutations = []
                for swp1 in range(len(perm_current)):
                    for swp2 in range(0, swp1):
                        perm = perm_current[:]
                        perm[swp1], perm[swp2] = perm[swp2], perm[swp1]
                        valid_permutations.append(perm)
            
            # find range of swap_nested_scopes transformations to replace
            start_idx = None
            end_idx = None
            for idx, (tname, *targs) in enumerate(current_path):
                if tname != 'swap_nested_scopes':
                    continue
                [scope_name] = targs
                [op_name, scope_depth] = scope_name.split('#')
                if op_name != op.name:
                    continue
                if start_idx is None:
                    start_idx = idx
                end_idx = idx

            # it is possible that there are no swaps to replace because original transformation does not permute anything
            # in this case we put swaps after tiling transformations
            if start_idx is None and end_idx is None:
                start_idx = len(initial_path)
                end_idx = len(initial_path) - 1
            
            # add every valid permutation as new candidate, that replace original permutation
            for perm in valid_permutations:
                scope_nest_candidate = build_loop_reordering_sequence(op.name, perm_base, perm)
                candidate = current_path[:start_idx] + scope_nest_candidate + current_path[end_idx+1:]
                candidates.append(candidate)
    return candidates


def find_create_temporary_candidates(program, current_path):
    # we create temporaries for inputs and outputs of each reduction
    # to allow search, we consider multiple variants:
    # - temporary only for output
    # - temporary only for inputs
    # - temporary for both inputs and outputs
    
    # we undo transformations in current path to explore alternative create_temporary candidates
    initial_path = []
    for tname, *targs in current_path:
        if tname in {'tile_scope', 'swap_nested_scopes'}:
            initial_path.append((tname, *targs))
        else:
            break
    base_program = program_for_path(program, [])  # without tiling
    initial_program = program_for_path(program, initial_path)  # with tiling
    scopes_with_reduction = find_scopes_with_reduction(initial_program)

    candidates = []
    
    if initial_path == current_path:
        # generating initial candidates (or explicitly considering no temporary creation)
        path_extension = []
        for scope_nest, op in recursive_scopes_with_ops(initial_program):
            if {s.name for s in scope_nest} & scopes_with_reduction:
                # take operation from the base program, because tiling can create complex indices
                op_parent, op_idx = find_op_by_name(base_program, op.name)
                base_op = op_parent.ops[op_idx]
                # heuristic: create input temporaries only if there are two inputs (meaningless with one input)
                if len(base_op.inp_elem) > 1:
                    for idx, elem in enumerate(base_op.inp_elem):
                        # heuristic: no need to create temporary for inputs that have non-trivial index computation                        
                        if any(isinstance(i, IndexExpr) for i in elem.indices):
                            continue
                        path_extension.append(('create_operand_temporary', base_op.name, 1 + idx))   
                # output temporary
                path_extension.append(('create_operand_temporary', base_op.name, 0))
        if path_extension:
            # if extension is empty, we avoid returning the same candidate as the current path
            candidates.append(initial_path + path_extension)
    else:
        # generating alternative candidates
        return []  # this doesn't seem to need exploration and has deterministic rule
        for scope_nest, op in recursive_scopes_with_ops(initial_program):
            if {s.name for s in scope_nest} & scopes_with_reduction:
                # find first and last transformation that affect the current op
                # also collect indices of operands for which temporary creation was enabled
                start_idx = None
                end_idx = None
                enabled_operand_indices = set()
                for idx, (tname, *targs) in enumerate(current_path):
                    if tname != 'create_operand_temporary':
                        continue
                    [op_name, operand_idx] = targs
                    if op_name != op.name:
                        continue
                    if start_idx is None:
                        start_idx = idx
                    end_idx = idx
                    enabled_operand_indices.add(operand_idx)
                
                # if there are no temporary creations for this op, we put them after scope nest reordering transformations
                if start_idx is None and end_idx is None:
                    start_idx = len(initial_path)
                    end_idx = len(initial_path) - 1
                
                # generate new candidates for current op, flip the choise of temporary creation for each operand
                # loop over flip candidates, for each create new candidate transformation sequence
                for flip_candidate_idx, flip_elem in enumerate(op.all_elem):
                    if not isinstance(flip_elem, Array):
                        continue  # we only consider arrays
                    new_candidate = []
                    # loop over operands for temporary creation (reversed because output temporary creation changes op name)
                    for idx, elem in reversed(list(enumerate(op.all_elem))):
                        if not isinstance(elem, Array):
                            continue
                        previously_enabled = idx in enabled_operand_indices
                        need_to_flip = idx == flip_candidate_idx
                        if previously_enabled != need_to_flip:
                            new_candidate.append(('create_operand_temporary', op.name, idx))
                    new_path_candidate = current_path[:start_idx] + new_candidate + current_path[end_idx+1:]
                    assert new_path_candidate != current_path  # we flip individual items, so the candidate should be different
                    candidates.append(new_path_candidate)
        assert all(candidates)  # all candidates should be non-empty
    return candidates


def path_has_transform(path, transform_name):
    for tname, *targs in reversed(path):
        if tname == transform_name:
            return True
    return False


class HeuristicCandidate:
    def __init__(self):
        # we need to cache for search methods that query the same path multiple times
        self.cached_candidates = {}
        
    def query(self, base_program, path):
        # print('Current path:', path)
        path_tuple = tuple(path)
        if path_tuple in self.cached_candidates:
            return self.cached_candidates[path_tuple]

        candidates = []
        current_path = path[:]  # copy to avoid modifying the path supplied from caller

        if current_path == []:
            # path is not initialized, we initialize it with the first set of candidates
            # sometimes they can be empty, meaning no transformations of this type are possible
            init_candidates = find_scope_tiling_candidates(base_program, current_path)
            if init_candidates:
                current_path = init_candidates[0]
            init_candidates = find_swap_nested_scopes_candidates(base_program, current_path, is_initial=True)
            if init_candidates:
                current_path = init_candidates[0]
            init_candidates = find_create_temporary_candidates(base_program, current_path)
            if init_candidates:
                current_path = init_candidates[0]
            candidates.append(current_path)
        
        # path is initialized, we try to find candidates "close" to the current path
        new_scope_tiling_candidates = find_scope_tiling_candidates(base_program, current_path)
        print('num new_scope_tiling_candidates:', len(new_scope_tiling_candidates))
        
        new_swap_nested_scopes_candidates = find_swap_nested_scopes_candidates(base_program, current_path, is_initial=False)
        print('num new_swap_nested_scopes_candidates:', len(new_swap_nested_scopes_candidates))
        
        new_create_temporary_candidates = find_create_temporary_candidates(base_program, current_path)
        print('num new_create_temporary_candidates:', len(new_create_temporary_candidates))
        
        candidates.extend(new_scope_tiling_candidates)
        candidates.extend(new_swap_nested_scopes_candidates)
        candidates.extend(new_create_temporary_candidates)

        # make sure to not include candidate that matches the current path
        assert all(c != path for c in candidates)
        
        self.cached_candidates[path_tuple] = candidates
        return candidates


class RuntimeCost:
    def __init__(self, evaluator):
        self.evaluator = evaluator
        self.best_cost = float('inf')
        self.seen_programs = {}
    
    def eval(self, base_program, path):
        program = program_for_path(base_program, path)
        # print(f"Fastest so far: {self.best_cost}")
        status, runtime_ms, runtime_std = self.evaluator.eval(program)
        if program.program_hash() not in self.seen_programs:
            # disable verbose output for already seen programs
            print("Transformations: ", end="")
            print("; ".join(f"{tname_path}(program, {args_as_str(targs_path)})" for tname_path, *targs_path in path))
            print(program.text())
            self.seen_programs[program.program_hash()] = True
            print(f"Runtime ({status}) [ms]: avg {runtime_ms:.6f} std {runtime_std:.6f}")
        if runtime_ms < self.best_cost:
            print(f"Cost improved. Old: {self.best_cost} -> New: {runtime_ms}")
            self.best_cost = runtime_ms
        return runtime_ms


def heuristic_parallelize_pass(program):
    parallelizable_scope_candidates_all = find_parallelizable_scopes(program)
    # filter out bad parallelization candidates
    parallelizable_scope_candidates = remove_bad_parallelization_candidates(program, parallelizable_scope_candidates_all)
    # now sort candidates by their depth in the scope nest
    candidates_with_depths = []
    for (scope_name,) in parallelizable_scope_candidates:
        parent, idx_in_parent = find_scope_by_name(program, scope_name)
        scope = parent.ops[idx_in_parent]    
        scope_depth = len(get_scopes_from_root_to_scope(program, scope))
        candidates_with_depths.append((scope_name, scope_depth))
    candidates_with_depths = sorted(candidates_with_depths, key=lambda x: x[1])
    # finally, apply candidates while they can be applied
    transform_chain = []
    for scope_name, _ in candidates_with_depths:
        if (scope_name,) not in find_parallelizable_scopes(program):
            continue
        parallelize_scope(program, scope_name)
        transform_chain.append(("parallelize_scope", scope_name))
    return transform_chain


def heuristic_swappable_buffer_dims(program):
    all_swappable_dims = find_swappable_buffer_dims(program)
    filtered_swappable_dims = []
    # allow swapping only if it moves dimensions associated with innermost scope to the more innermost position in all loops
    for buf_name, dim_outer in all_swappable_dims:
        dim_inner = dim_outer + 1
        reject = False
        # find references to the buffer
        for op in recursive_ops(program):
            for elem in op.all_elem:
                if not isinstance(elem, Array) or program.arr_to_buf[elem.name] != buf_name:
                    continue
                index_outer = elem.indices[dim_outer]
                index_inner = elem.indices[dim_inner]
                accessed_scopes_outer = index_outer.accessed_pos()
                accessed_scopes_inner = index_inner.accessed_pos()
                min_outer = min(accessed_scopes_outer)
                if not all(min_outer < p for p in accessed_scopes_inner):
                    reject = True
                    break
            if reject:
                break
        if not reject:
            filtered_swappable_dims.append((buf_name, dim_outer))
    return filtered_swappable_dims


def heuristic_pass(program):
    """
        Modifies the program in place. Returns the list of transformations applied.
    """
    
    transform_chain = apply_exhaustive_transform_chain(program, {
        "tile_buffer": (tile_buffer, find_tileable_buffers),
        "join_scopes": (join_scopes, find_joinable_scopes),
        "reuse_arr_dims": (reuse_arr_dims, find_reusable_arr_dims),
        "move_buf_to_stack": (move_buf_to_stack, find_bufs_movable_to_stack),
    })
    
    transform_chain += apply_exhaustive_transform_chain(program, {
        "swap_buffer_dims": (swap_buffer_dims, heuristic_swappable_buffer_dims),
    })
    
    transform_chain += apply_exhaustive_transform_chain(program, {
        "reuse_buffers": (reuse_buffers, find_reusable_buffers),
    })
    
    transform_chain += heuristic_parallelize_pass(program)
    
    return transform_chain


class HeurPassRuntimeCost:
    def __init__(self, evaluator):
        self.runtime_cost = RuntimeCost(evaluator)
    
    def eval(self, base_program, path):
        if not path:
            # this is needed to evaluate the baseline non-transformed program
            self.runtime_cost.eval(base_program, path)
        program = program_for_path(base_program, path)
        extended_path = heuristic_pass(program)
        return self.runtime_cost.eval(base_program, path + extended_path)


if __name__ == "__main__":
    # numactl -N 0 -m 0 -C 0-17 python heuristic_search_new.py --kernel matmul --size 768x1024x1024 --search no --candidate heur_norep
    if os.environ['OMP_PROC_BIND'] != 'close':
        raise Exception("Set OMP_PROC_BIND=close")
    
    argparser = argparse.ArgumentParser()
    argparser.add_argument("--kernel", type=str, required=True)
    argparser.add_argument("--size", type=str, required=True)
    argparser.add_argument("--min_repeats", type=int, default=5)
    argparser.add_argument("--min_seconds", type=float, default=0.1)
    argparser.add_argument("--search", type=str, default="greedy")
    argparser.add_argument("--candidate", type=str, default="all_edges")
    argparser.add_argument("--cost", type=str, default="heur_pass_runtime")
    argparser.add_argument("--timeout", type=int, default=500)
    argparser.add_argument("--cache", type=str, default="cached_runtime.db")
    argparser.add_argument("--no_timeout_adjust", action='store_true')
    argparser.add_argument("--no_verify", action='store_true')
    argparser.add_argument("--no_reinit_inputs", action='store_true')
    args = argparser.parse_args()

    random.seed(0)
    np.random.seed(0)
    torch.manual_seed(0)
    
    candidate = {
        'edges': EdgesCandidate,
        'heur': HeuristicCandidate,
    }[args.candidate]
    
    cost = {
        'runtime': RuntimeCost,
        'heur_pass_runtime': HeurPassRuntimeCost,
    }[args.cost]
    
    search = {
        'greedy': GreedySearch,
        'greedy_top': GreedyTopSearch,
        'mcts': MCTSearch,
        'annealing': SimulatedAnnealingSearch,
        'no': NoSearch,
    }[args.search]
    
    kernel = {
        'matmul': MatmulKernelCreator,
        'layernorm': LayerNormKernelCreator,
        'batchnorm': BatchNormKernelCreator,
        'conv': ConvKernelCreator,
        'relu': ReluKernelCreator,
        'reducemean': ReduceMeanKernelCreator,
        'softmax': SoftmaxKernelCreator,
        'rmsnorm': RMSNormKernelCreator,
        'bmm': BatchedMatmulKernelCreator,
        'mul': MulKernelCreator,
        'add': AddKernelCreator,
        'matmul_mul': MatmulMulKernelCreator,
        'add_rmsnorm': AddRMSNormKernelCreator,
        'matmul_silu_mul': MatmulSiLUMulKernelCreator,
        'mul_matmul': MulMatmulKernelCreator,
        'rmsnorm_matmul': RMSNormMatmulKernelCreator,
        'swiglu': SwiGLUKernelCreator,
        'relu_ffn': ReluFFNKernelCreator,
    }[args.kernel]
    
    run_kernel(
        kernel, args.size, args.min_repeats, args.min_seconds, args.timeout, search,
        cost, candidate, args.cache, not args.no_timeout_adjust, not args.no_verify, not args.no_reinit_inputs
    )
