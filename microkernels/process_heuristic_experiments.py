import re

values = {}

with open("logs/heuristic_experiments_daint_numact_18threads.log", 'r') as f:
    for line in f:
        name = re.findall(r"^Name: (?:func_)?(.*)$", line)
        if name:
            [current_name] = name
        if current_name not in values:
            values[current_name] = {}
        # Scipy sigmoid time [ms]: avg 379.076244 std 1.812715
        # Numpy sigmoid time [ms]: avg 178.061356 std 0.284747
        # JAX sigmoid time [ms]: avg 36.330268 std 0.117133
        # JAX JIT sigmoid time [ms]: avg 15.484110 std 0.124267
        # JAX builtin sigmoid time [ms]: avg 15.812558 std 0.018453
        # Pytorch sigmoid time [ms]: avg 31.956810 std 0.039137
        # Pytorch JIT sigmoid time [ms]: avg 25.090242 std 0.031334
        # Pytorch builtin sigmoid time [ms]: avg 7.465683 std 0.023219
        # My softmax [ms]: avg 205.085571 std 0.214421
        time_scipy = re.findall(r"^Scipy .* avg (.*) std (.*)$", line)
        if time_scipy:
            [[avg, std]] = time_scipy
            values[current_name]['scipy'] = (avg, std)
        time_numpy = re.findall(r"^Numpy .* avg (.*) std (.*)$", line)
        if time_numpy:
            [[avg, std]] = time_numpy
            values[current_name]['numpy'] = (avg, std)
        time_jax_jit = re.findall(r"^JAX JIT .* avg (.*) std (.*)$", line)
        if time_jax_jit:
            [[avg, std]] = time_jax_jit
            values[current_name]['jax_jit'] = (avg, std)
        time_jax_builtin = re.findall(r"^JAX builtin .* avg (.*) std (.*)$", line)
        if time_jax_builtin:
            [[avg, std]] = time_jax_builtin
            values[current_name]['jax_builtin'] = (avg, std)
        time_jax = re.findall(r"^JAX .* avg (.*) std (.*)$", line)
        if time_jax and not (time_jax_jit or time_jax_builtin):
            [[avg, std]] = time_jax
            values[current_name]['jax'] = (avg, std)
        time_pytorch_jit = re.findall(r"^Pytorch JIT .* avg (.*) std (.*)$", line)
        if time_pytorch_jit:
            [[avg, std]] = time_pytorch_jit
            values[current_name]['pytorch_jit'] = (avg, std)
        time_pytorch_builtin = re.findall(r"^Pytorch builtin .* avg (.*) std (.*)$", line)
        if time_pytorch_builtin:
            [[avg, std]] = time_pytorch_builtin
            values[current_name]['pytorch_builtin'] = (avg, std)
        time_pytorch = re.findall(r"^Pytorch .* avg (.*) std (.*)$", line)
        if time_pytorch and not (time_pytorch_jit or time_pytorch_builtin):
            [[avg, std]] = time_pytorch
            values[current_name]['pytorch'] = (avg, std)
        time_my = re.findall(r"^My .* avg (.*) std (.*)$", line)
        if time_my:
            [[avg, std]] = time_my
            values[current_name]['my'] = (avg, std)

common_keys = ['scipy', 'numpy', 'jax', 'jax_jit', 'jax_builtin', 'pytorch', 'pytorch_jit', 'pytorch_builtin', 'my']

print('name,', end='')
print(','.join(common_keys))

for name, value in values.items():
    print(name, end='')
    for key in common_keys:
        print(',', end='')
        if key not in value:
            print('', end='')
        else:
            print(value[key][0], end='')
    print('')
            