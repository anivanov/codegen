import copy

import torch
from torch.utils._contextlib import F

from .gen import *
from .manual_optimizations import *
import numpy as np
import itertools
import struct
import csv

# from .hgemm import hgemm


def write_results(kernel, size, results):
    headers = ["Kernel", "Size", "Cycles", "Reference", "Peak"]
    data = [[kernel, size, *results]]

    file_exists = os.path.isfile("results.csv")

    with open("results.csv", mode='a', newline='') as file:
        writer = csv.writer(file)
        if not file_exists:
            writer.writerow(headers)
        writer.writerows(data)



# ++++++++++++++++++++++++++ FUNCTION APPROXIMATIONS ++++++++++++++++++++++++++

def approx_sqrt(number):
    threehalfs = 1.5
    x2 = number * 0.5
    y = number
 
    i = struct.unpack('I', struct.pack('f', y))[0]
    i = 0x5f3759df - (i >> 1)
    y = struct.unpack('f', struct.pack('I', i))[0]
 
    # 1st iteration
    y = y * (threehalfs - (x2 * y * y))
 
    # 2nd iteration, this can be removed
    y = y * (threehalfs - (x2 * y * y))
    result_bits = struct.unpack('I', struct.pack('f', y))[0]
    size = struct.calcsize('I')
 
    if result_bits < 0 or result_bits >= (1 << (size * 8)):
        raise ValueError('result_bits out of range')
    
    v = struct.unpack('f', struct.pack('I', result_bits))[0] * number
    assert np.allclose(np.sqrt(number), v, rtol=1e-2, atol=1e-5)
    return v

def sleef_rsqrt_op(out_elem, inp_elem):
    out_elem = canonicalize_access(out_elem)
    inp_elem = canonicalize_access(inp_elem)
    no = out_elem.name
    io = out_elem.indices
    ni = inp_elem.name
    ii = inp_elem.indices
    if io != ii:
        raise ValueError("Not supported")
    return [
        Operation(Array(no, io), [Array(ni, ii)], OpDesc("f32", "Sleef_rsqrtf_u10({0})"))
    ]

def rsqrt_op(out_elem, inp_elem):
    out_elem = canonicalize_access(out_elem)
    inp_elem = canonicalize_access(inp_elem)
    no = out_elem.name
    io = out_elem.indices
    ni = inp_elem.name
    ii = inp_elem.indices
    if io != ii:
        raise ValueError("Not supported")
    return [
        Operation(Array(f"{no}_d2", io), [Array(ni, ii), (0.5)], "mul"),
        Operation(Array(f"{no}_f32", io), [Array(ni, ii)], "f32_from_f64"),
        Operation(Array(f"{no}_i32", io), [Array(f"{no}_f32", io)], "i32_copyfrom_f32"),
        Operation(Array(f"{no}_s1", io), [Array(f"{no}_i32", io), 1], "srli"),
        Operation(Array(f"{no}_s2", io), [0x5f3759df, Array(f"{no}_s1", io)], "isub"),
        Operation(Array(f"{no}_f32_2", io), [Array(f"{no}_s2", io)], "f32_copyfrom_i32"),
        Operation(Array(f"{no}_f64", io), [Array(f"{no}_f32_2", io)], "f64_from_f32"),
        Operation(Array(f"{no}_f64_2", io), [Array(f"{no}_f64", io), Array(f"{no}_f64", io)], "mul"),
        Operation(Array(f"{no}_th", io), [Array(f"{no}_f64_2", io), Array(f"{no}_d2", io), 1.5], "fnmsub"),
        Operation(Array(f"{no}_inv1", io), [Array(f"{no}_th", io), Array(f"{no}_f64", io)], "mul"),

        #the next few operations could be avoided trading speed for accuracy
        Operation(Array(f"{no}_f64_2_2", io), [Array(f"{no}_inv1", io), Array(f"{no}_inv1", io)], "mul"),
        Operation(Array(f"{no}_th_2", io), [Array(f"{no}_f64_2_2", io), Array(f"{no}_d2", io), 1.5], "fnmsub"),
        Operation(Array(f"{no}", io), [Array(f"{no}_th_2", io), Array(f"{no}_inv1", io)], "mul"),
    ]



def sqrt_op(out_elem, inp_elem):
    out_elem = canonicalize_access(out_elem)
    inp_elem = canonicalize_access(inp_elem)
    no = out_elem.name
    io = out_elem.indices
    ni = inp_elem.name
    ii = inp_elem.indices
    if io != ii:
        raise ValueError("Not supported")
    return [
        *rsqrt_op(Array(f"{no}_inv", io), Array(ni, ii)),
        Operation(Array(f"{no}", io), [Array(f"{no}_inv", io), Array(ni, ii)], "mul"),
    ]


def approx_div(n, d):
    sd = np.sign(d)
    
    threehalfs = 1.5
    x2 = abs(d) * 0.5
    y = abs(d)
 
    i = struct.unpack('I', struct.pack('f', y))[0]
    i = 0x5f3759df - (i >> 1)
    y = struct.unpack('f', struct.pack('I', i))[0]
 
    # 1st iteration
    y = y * (threehalfs - (x2 * y * y))
 
    # 2nd iteration, this can be removed
    y = y * (threehalfs - (x2 * y * y))
    result_bits = struct.unpack('I', struct.pack('f', y))[0]
    size = struct.calcsize('I')
 
    if result_bits < 0 or result_bits >= (1 << (size * 8)):
        raise ValueError('result_bits out of range')
    
    v = struct.unpack('f', struct.pack('I', result_bits))[0]

    v = n * v * v * sd

    assert np.allclose(n/d, v, rtol=1e-2, atol=1e-5)
    return v


def div_op(out_elem, inp_elem1, inp_elem2):
    out_elem = canonicalize_access(out_elem)
    inp_elem1 = canonicalize_access(inp_elem1)
    inp_elem2 = canonicalize_access(inp_elem2)
    no = out_elem.name
    io = out_elem.indices
    ni = inp_elem2.name
    ii = inp_elem2.indices
    ni1 = inp_elem1.name
    ii1 = inp_elem1.indices
    # if io != ii:
    #     raise ValueError("Not supported")
    return [
        *reciprocal_op(Array(f"{no}_div", io), Array(ni, ii)),
        Operation(Array(f"{no}", io), [Array(f"{no}_div", io), Array(ni1, ii1)], "mul"),
    ]

def reciprocal_op_f32(out_elem, inp_elem):
    out_elem = canonicalize_access(out_elem)
    inp_elem = canonicalize_access(inp_elem)
    no = out_elem.name
    io = out_elem.indices
    ni = inp_elem.name
    ii = inp_elem.indices
    # if io != ii:
    #     raise ValueError("Not supported")
    return [
        Operation(Array(f"{no}_abs", io), [Array(ni, ii)], OpDesc("f32", "fabs({0})")),
        *rsqrt_op(Array(f"{no}_inv", io), Array(f"{no}_abs", io)),
        Operation(Array(f"{no}_div_u", io), [Array(f"{no}_inv", io), Array(f"{no}_inv", io)], OpDesc("f32", "{0} * {1}")),
        Operation(Array(f"{no}", io), [Array(f"{no}_div_u", io), Array(ni, ii)], OpDesc("f32", "copysign({0}, {1})")),
    ]

def reciprocal_op(out_elem, inp_elem):
    out_elem = canonicalize_access(out_elem)
    inp_elem = canonicalize_access(inp_elem)
    no = out_elem.name
    io = out_elem.indices
    ni = inp_elem.name
    ii = inp_elem.indices
    # if io != ii:
    #     raise ValueError("Not supported")
    return [
        Operation(Array(f"{no}_abs", io), [Array(ni, ii)], "abs"),
        *rsqrt_op(Array(f"{no}_inv", io), Array(f"{no}_abs", io)),
        Operation(Array(f"{no}_div_u", io), [Array(f"{no}_inv", io), Array(f"{no}_inv", io)], "mul"),
        Operation(Array(f"{no}", io), [Array(f"{no}_div_u", io), Array(ni, ii)], "sgnj"),
    ]

def approx_exp(x):
    n = 13
    v = 1 + x / 2**n
    for _ in range(n):
        v *= v
    assert np.allclose(np.exp(x), v, rtol=1e-2, atol=1e-5)
    return v

def sleef_exp_op(out_elem, inp_elem):
    out_elem = canonicalize_access(out_elem)
    inp_elem = canonicalize_access(inp_elem)
    no = out_elem.name
    io = out_elem.indices
    ni = inp_elem.name
    ii = inp_elem.indices
    if io != ii:
        raise ValueError("Not supported")
    return [
        Operation(Array(no, io), [Array(ni, ii)], OpDesc("f32", "Sleef_expf_u10({0})"))
    ]

def sleef_exp8_op(out_elem, inp_elem):
    out_elem = canonicalize_access(out_elem)
    inp_elem = canonicalize_access(inp_elem)
    no = out_elem.name
    io = out_elem.indices
    ni = inp_elem.name
    ii = inp_elem.indices
    if io != ii:
        raise ValueError("Not supported")
    return [
        Operation(Array(no, io), [Array(ni, ii)], OpDesc("m256", "Sleef_expf8_u10({0})"))
    ]

def exp_op(out_elem, inp_elem):
    #adding more iteration may be necessary in the future
    out_elem = canonicalize_access(out_elem)
    inp_elem = canonicalize_access(inp_elem)
    no = out_elem.name
    io = out_elem.indices
    ni = inp_elem.name
    ii = inp_elem.indices
    if io != ii:
        raise ValueError("Not supported")
    return [
        Operation(Array(f"{no}_v1", io), [Array(ni, ii), (1.0/8192), 1.0], "fmadd"),
        Operation(Array(f"{no}_v2", io), [Array(f"{no}_v1", io), Array(f"{no}_v1", io)], "mul"),
        Operation(Array(f"{no}_v3", io), [Array(f"{no}_v2", io), Array(f"{no}_v2", io)], "mul"),
        Operation(Array(f"{no}_v4", io), [Array(f"{no}_v3", io), Array(f"{no}_v3", io)], "mul"),
        Operation(Array(f"{no}_v5", io), [Array(f"{no}_v4", io), Array(f"{no}_v4", io)], "mul"),
        Operation(Array(f"{no}_v6", io), [Array(f"{no}_v5", io), Array(f"{no}_v5", io)], "mul"),
        Operation(Array(f"{no}_v7", io), [Array(f"{no}_v6", io), Array(f"{no}_v6", io)], "mul"),
        Operation(Array(f"{no}_v8", io), [Array(f"{no}_v7", io), Array(f"{no}_v7", io)], "mul"),
        Operation(Array(f"{no}_v9", io), [Array(f"{no}_v8", io), Array(f"{no}_v8", io)], "mul"),
        Operation(Array(f"{no}_v10", io), [Array(f"{no}_v9", io), Array(f"{no}_v9", io)], "mul"),
        Operation(Array(f"{no}_v11", io), [Array(f"{no}_v10", io), Array(f"{no}_v10", io)], "mul"),
        Operation(Array(f"{no}_v12", io), [Array(f"{no}_v11", io), Array(f"{no}_v11", io)], "mul"),
        Operation(Array(f"{no}_v13", io), [Array(f"{no}_v12", io), Array(f"{no}_v12", io)], "mul"),
        Operation(Array(f"{no}", io), [Array(f"{no}_v13", io), Array(f"{no}_v13", io)], "mul"),
    ]


def approx_ln(x):
    # approximate log(x) according to https://math.stackexchange.com/a/1680815
    # z = np.vectorize(approx_div)(1, 7+32 * x**(1/4) + 12 * x**(1/2) + 32 * x**(3/4) + 7 * x)
    # return 90*(x-1) * z
    ret = 90*(x-1)/(7+32 * x**(1/4) + 12 * x**(1/2) + 32 * x**(3/4) + 7 * x)
    assert np.allclose(np.log(x), ret, rtol=1e-2, atol=1e-2)
    return ret

def ln_op(out_elem, inp_elem):
    """
        Implmentation of log(x) using approximation
        log(x) = 90*(x-1)/(7+32 * x**(1/4) + 12 * x**(1/2) + 32 * x**(3/4) + 7 * x)
    """
    out_elem = canonicalize_access(out_elem)
    inp_elem = canonicalize_access(inp_elem)
    no = out_elem.name
    io = out_elem.indices
    ni = inp_elem.name
    ii = inp_elem.indices
    if io != ii:
        raise ValueError("Not supported")
    return [
        *sqrt_op(Array(f"{no}_rt2x", io), Array(ni, ii)),
        *sqrt_op(Array(f"{no}_rt4x", io), Array(f"{no}_rt2x", io)),
        Operation(Array(f"{no}_rt3_4x", io), [Array(f"{no}_rt2x", io), Array(f"{no}_rt4x", io)], "mul"),
        Operation(Array(f"{no}_rt4x32", io), [Array(f"{no}_rt4x", io), 32.0], "mul"),
        Operation(Array(f"{no}_rt3_4x32", io), [Array(f"{no}_rt3_4x", io), 32.0], "mul"),
        Operation(Array(f"{no}_d1", io), [Array(ni, ii), 7.0, Array(f"{no}_rt3_4x32", io)], "fmadd"),
        Operation(Array(f"{no}_d2", io), [Array(f"{no}_rt2x", io), 12.0, Array(f"{no}_rt4x32", io)], "fmadd"),
        Operation(Array(f"{no}_d3", io), [7.0, Array(f"{no}_d1", io)], "add"),
        Operation(Array(f"{no}_d4", io), [Array(f"{no}_d3", io), Array(f"{no}_d2", io)], "add"),
        Operation(Array(f"{no}_x_1_90", io), [Array(ni, ii), 90.0, -90.0], "fmadd"),
        *div_op(Array(no, io), Array(f"{no}_x_1_90", io), Array(f"{no}_d4", io)),
    ]


def approx_tanh(x):
    ret = (approx_exp(2 * x) - 1) / (approx_exp(2 * x) + 1)
    assert np.allclose(np.tanh(x), ret, rtol=1e-2, atol=1e-5)
    return ret


def tanh_op(out_elem, inp_elem):
    out_elem = canonicalize_access(out_elem)
    inp_elem = canonicalize_access(inp_elem)
    no = out_elem.name
    io = out_elem.indices
    ni = inp_elem.name
    ii = inp_elem.indices
    if io != ii:
        raise ValueError("Not supported")
    return [
        Operation(Array(f"{no}_2x", io), [2.0, Array(ni, ii)], "mul"),
        *exp_op(Array(f"{no}_exp", io), Array(f"{no}_2x", io)),
        Operation(Array(f"{no}_exp_m1", io), [Array(f"{no}_exp", io), 1.0], "sub"),
        Operation(Array(f"{no}_exp_p1", io), [Array(f"{no}_exp", io), 1.0], "add"),
        *div_op(Array(no, io), Array(f"{no}_exp_m1", io), Array(f"{no}_exp_p1", io)),
    ]


def approx_pow(x1, x2):
    ret = approx_exp(approx_ln(x1) * x2)
    #assert np.allclose(x1**x2, ret, rtol=1e-1, atol=1e-5)
    return ret


def pow_op(out_elem, inp1_elem, inp2_elem):
    out_elem = canonicalize_access(out_elem)
    inp1_elem = canonicalize_access(inp1_elem)
    inp2_elem = canonicalize_access(inp2_elem)
    no = out_elem.name
    io = out_elem.indices
    ni1 = inp1_elem.name
    ii1 = inp1_elem.indices
    ni2 = inp2_elem.name
    ii2 = inp2_elem.indices
    if io != ii1 or io != ii2 or ii2 != ii1:
        raise ValueError("Not supported")
    return [
        *ln_op(Array(f"{no}_ln", io), Array(ni1, ii1)),
        Operation(Array(f"{no}_t", io), [Array(f"{no}_ln", io), Array(ni2, ii2)], "mul"),
        *exp_op(Array(no, io), Array(f"{no}_t", io)),
    ]


def approx_softplus(x):
    return approx_ln(1 + approx_exp(x))


def softplus_op(out_elem, inp_elem):
    # softplus(x) = ln(1 + e^{x})
    out_elem = canonicalize_access(out_elem)
    inp_elem = canonicalize_access(inp_elem)
    no = out_elem.name
    io = out_elem.indices
    ni = inp_elem.name
    ii = inp_elem.indices
    if io != ii:
        raise ValueError("Not supported")
    return [
        *exp_op(Array(f"{no}_exp", io), Array(ni, ii)),
        Operation(Array(f"{no}_exp_p1", io), [Array(f"{no}_exp", io), 1.0], "add"),
        *ln_op(Array(no, io), Array(f"{no}_exp_p1", io)),
    ]


def approx_mish(x):
    return x * approx_tanh(approx_softplus(x))


def mish_op(out_elem, inp_elem):
    # mish(x) =  x * tanh(softplus(x))
    out_elem = canonicalize_access(out_elem)
    inp_elem = canonicalize_access(inp_elem)
    no = out_elem.name
    io = out_elem.indices
    ni = inp_elem.name
    ii = inp_elem.indices
    if io != ii:
        raise ValueError("Not supported")
    return [
        *softplus_op(Array(f"{no}_softplus", io), Array(ni, ii)),
        *tanh_op(Array(f"{no}_tanh", io), Array(f"{no}_softplus", io)),
        Operation(Array(no, io), [Array(ni, ii), Array(f"{no}_tanh", io)], "mul"),
    ]


def approx_acos(src):
    # source: https://developer.download.nvidia.com/cg/acos.html
    # // Handbook of Mathematical Functions
    # // M. Abramowitz and I.A. Stegun, Ed.
    # // Absolute error <= 6.7e-5
    # float acos(float x) {
    # float negate = float(x < 0);
    # x = abs(x);
    # float ret = -0.0187293;
    # ret = ret * x;
    # ret = ret + 0.0742610;
    # ret = ret * x;
    # ret = ret - 0.2121144;
    # ret = ret * x;
    # ret = ret + 1.5707288;
    # ret = ret * sqrt(1.0-x);
    # ret = ret - 2 * negate * ret;
    # return negate * 3.14159265358979 + ret;
    # }
    sign = 2 * (src >= 0).astype(float) - 1 # -1 or 1
    nsign = - sign
    negate = np.maximum(nsign, 0) # 1 or 0
    x = np.abs(src)
    ret = -0.0187293 * x + 0.0742610
    ret = ret * x - 0.2121144
    ret = ret * x + 1.5707288
    ret = ret * np.sqrt(1.0-x)
    ret = sign * ret
    ret = negate * np.pi + ret
    # print("src", src)
    # print("my", ret)
    # print("np", np.arccos(src))
    assert np.allclose(np.arccos(src), ret, rtol=1e-2, atol=1e-5)
    return ret


def acos_op(out_elem, inp_elem):
    out_elem = canonicalize_access(out_elem)
    inp_elem = canonicalize_access(inp_elem)
    no = out_elem.name
    io = out_elem.indices
    ni = inp_elem.name
    ii = inp_elem.indices
    if io != ii:
        raise ValueError("Not supported")
    return [
        Operation(Array(f"{no}_nsign", io), [1.0, Array(ni, ii)], "sgnjn"),
        Operation(Array(f"{no}_negate", io), [Array(f"{no}_nsign", io), 0.0], "max"),
        Operation(Array(f"{no}_abs", io), [Array(ni, ii)], "abs"),
        Operation(Array(f"{no}_t1", io), [-0.0187293, Array(f"{no}_abs", io), 0.0742610], "fmadd"),
        Operation(Array(f"{no}_t2", io), [Array(f"{no}_t1", io), Array(f"{no}_abs", io), -0.2121144], "fmadd"),
        Operation(Array(f"{no}_t3", io), [Array(f"{no}_t2", io), Array(f"{no}_abs", io), 1.5707288], "fmadd"),
        Operation(Array(f"{no}_one_minus_abs", io), [1.0, Array(f"{no}_abs", io)], "sub"),
        *sqrt_op(Array(f"{no}_sqrt", io), Array(f"{no}_one_minus_abs", io)),
        Operation(Array(f"{no}_t4", io), [Array(f"{no}_t3", io), Array(f"{no}_sqrt", io)], "mul"),
        Operation(Array(f"{no}_t5", io), [Array(f"{no}_t4", io), Array(ni, ii)], "sgnj"),
        Operation(Array(no, io), [Array(f"{no}_negate", io), 3.14159265358979, Array(f"{no}_t5", io)], "fmadd"),
    ]


def approx_asin(src):
    # source: https://developer.download.nvidia.com/cg/asin.html
    # // Handbook of Mathematical Functions
    # // M. Abramowitz and I.A. Stegun, Ed.
    # float asin(float x) {
    #   float negate = float(x < 0);
    #   x = abs(x);
    #   float ret = -0.0187293;
    #   ret *= x;
    #   ret += 0.0742610;
    #   ret *= x;
    #   ret -= 0.2121144;
    #   ret *= x;
    #   ret += 1.5707288;
    #   ret = 3.14159265358979*0.5 - sqrt(1.0 - x)*ret;
    #   return ret - 2 * negate * ret;
    # }
    sign = 2 * (src >= 0).astype(float) - 1 # -1 or 1
    x = np.abs(src)
    ret = -0.0187293 * x + 0.0742610
    ret = ret * x - 0.2121144
    ret = ret * x + 1.5707288
    ret = np.pi / 2 - ret * np.sqrt(1.0-x)
    ret = sign * ret
    # print("src", src)
    # print("my", ret)
    # print("np", np.arcsin(src))
    assert np.allclose(np.arcsin(src), ret, rtol=1e-2, atol=1e-4)
    return ret


def asin_op(out_elem, inp_elem):
    out_elem = canonicalize_access(out_elem)
    inp_elem = canonicalize_access(inp_elem)
    no = out_elem.name
    io = out_elem.indices
    ni = inp_elem.name
    ii = inp_elem.indices
    if io != ii:
        raise ValueError("Not supported")
    return [
        Operation(Array(f"{no}_abs", io), [Array(ni, ii)], "abs"),
        Operation(Array(f"{no}_t1", io), [-0.0187293, Array(f"{no}_abs", io), 0.0742610], "fmadd"),
        Operation(Array(f"{no}_t2", io), [Array(f"{no}_t1", io), Array(f"{no}_abs", io), -0.2121144], "fmadd"),
        Operation(Array(f"{no}_t3", io), [Array(f"{no}_t2", io), Array(f"{no}_abs", io), 1.5707288], "fmadd"),
        Operation(Array(f"{no}_one_minus_abs", io), [1.0, Array(f"{no}_abs", io)], "sub"),
        *sqrt_op(Array(f"{no}_sqrt", io), Array(f"{no}_one_minus_abs", io)),
        Operation(Array(f"{no}_t4", io), [Array(f"{no}_t3", io), Array(f"{no}_sqrt", io), np.pi / 2], "fnmsub"),
        Operation(Array(no, io), [Array(f"{no}_t4", io), Array(ni, ii)], "sgnj"),
    ]


def acosh_op(out_elem, inp_elem):
    out_elem = canonicalize_access(out_elem)
    inp_elem = canonicalize_access(inp_elem)
    no = out_elem.name
    io = out_elem.indices
    ni = inp_elem.name
    ii = inp_elem.indices
    if io != ii:
        raise ValueError("Not supported")
    return [
        Operation(Array(f"{no}_t1", io), [Array(ni, ii), Array(ni, ii), 1.0], "fmsub"),
        *sqrt_op(Array(f"{no}_t2", io), Array(f"{no}_t1", io)),
        Operation(Array(f"{no}_t3", io), [Array(f"{no}_t2", io), Array(ni, ii)], "add"),
        *ln_op(Array(no, io), Array(f"{no}_t3", io)),
    ]


def approx_acosh(src):
    # ln (x + sqrt(x^2 - 1))
    ret = approx_ln(src + np.sqrt(src**2 - 1))
    assert np.allclose(np.arccosh(src), ret, rtol=1e-2, atol=1e-5)
    return ret


def asinh_op(out_elem, inp_elem):
    out_elem = canonicalize_access(out_elem)
    inp_elem = canonicalize_access(inp_elem)
    no = out_elem.name
    io = out_elem.indices
    ni = inp_elem.name
    ii = inp_elem.indices
    if io != ii:
        raise ValueError("Not supported")
    return [
        Operation(Array(f"{no}_t1", io), [Array(ni, ii), Array(ni, ii), 1.0], "fmadd"),
        *sqrt_op(Array(f"{no}_t2", io), Array(f"{no}_t1", io)),
        Operation(Array(f"{no}_t3", io), [Array(f"{no}_t2", io), Array(ni, ii)], "add"),
        *ln_op(Array(no, io), Array(f"{no}_t3", io)),
    ]


def approx_asinh(src):
    # ln (x + sqrt(x^2 + 1))
    ret = approx_ln(src + np.sqrt(src**2 + 1))
    assert np.allclose(np.arcsinh(src), ret, rtol=1e-2, atol=1e-5)
    return ret


def approx_atan2(y, x):
    # source: https://developer.download.nvidia.com/cg/atan2.html
    # float2 atan2(float2 y, float2 x)
    # {
    # float2 t0, t1, t2, t3, t4;
    # t3 = abs(x);
    # t1 = abs(y);
    # t0 = max(t3, t1);
    # t1 = min(t3, t1);
    # t3 = float(1) / t0;
    # t3 = t1 * t3;
    # t4 = t3 * t3;
    # t0 =         - float(0.013480470);
    # t0 = t0 * t4 + float(0.057477314);
    # t0 = t0 * t4 - float(0.121239071);
    # t0 = t0 * t4 + float(0.195635925);
    # t0 = t0 * t4 - float(0.332994597);
    # t0 = t0 * t4 + float(0.999995630);
    # t3 = t0 * t3;
    # t3 = (abs(y) > abs(x)) ? float(1.570796327) - t3 : t3;
    # t3 = (x < 0) ?  float(3.141592654) - t3 : t3;
    # t3 = (y < 0) ? -t3 : t3;
    # return t3;
    # }
    abs_x = np.abs(x)
    abs_y = np.abs(y)
    max_abs = np.maximum(abs_x, abs_y)
    min_abs = np.minimum(abs_x, abs_y)
    min_max = min_abs / max_abs
    mm = min_max * min_max
    t0 = -0.013480470 * mm + 0.057477314
    t1 = t0 * mm - 0.121239071
    t2 = t1 * mm + 0.195635925
    t3 = t2 * mm - 0.332994597
    t4 = t3 * mm + 0.999995630
    r0 = t4 * min_max
    dyx = abs_y - abs_x
    syx = np.sign(dyx) * np.pi/2
    pyx = np.maximum(0, syx)
    sr0 = r0 * np.sign(dyx)
    r1 = pyx - sr0
    sx = -np.sign(x) * np.pi
    px = np.maximum(0, sx)
    sr1 = r1 * np.sign(x)
    r2 = px + sr1
    ret = r2 * np.sign(y)
    # print("x", x)
    # print("y", y)
    # print("my", ret)
    # print("np", np.arctan2(y, x))
    assert np.allclose(np.arctan2(y, x), ret, rtol=1e-2, atol=1e-4)
    return ret


def approx_atan(src):
    return approx_atan2(src, 1.0)


def atan2_op(out_elem, y_elem, x_elem):
    out_elem = canonicalize_access(out_elem)
    y_elem = canonicalize_access(y_elem)
    x_elem = canonicalize_access(x_elem)
    no = out_elem.name
    io = out_elem.indices
    ny = y_elem.name
    iy = y_elem.indices
    if io != iy:
        raise ValueError("Not supported")
    return [
        Operation(Array(f"{no}_abs_x", io), [copy.deepcopy(x_elem)], "abs"),
        Operation(Array(f"{no}_abs_y", io), [Array(ny, iy)], "abs"),
        Operation(Array(f"{no}_max_abs", io), [Array(f"{no}_abs_x", io), Array(f"{no}_abs_y", io)], "max"),
        Operation(Array(f"{no}_min_abs", io), [Array(f"{no}_abs_x", io), Array(f"{no}_abs_y", io)], "min"),
        *div_op(Array(f"{no}_min_max", io), Array(f"{no}_min_abs", io), Array(f"{no}_max_abs", io)),
        Operation(Array(f"{no}_mm", io), [Array(f"{no}_min_max", io), Array(f"{no}_min_max", io)], "mul"),
        Operation(Array(f"{no}_t0", io), [-0.013480470, Array(f"{no}_mm", io), 0.057477314], "fmadd"),
        Operation(Array(f"{no}_t1", io), [Array(f"{no}_t0", io), Array(f"{no}_mm", io), -0.121239071], "fmadd"),
        Operation(Array(f"{no}_t2", io), [Array(f"{no}_t1", io), Array(f"{no}_mm", io), 0.195635925], "fmadd"),
        Operation(Array(f"{no}_t3", io), [Array(f"{no}_t2", io), Array(f"{no}_mm", io), -0.332994597], "fmadd"),
        Operation(Array(f"{no}_t4", io), [Array(f"{no}_t3", io), Array(f"{no}_mm", io), 0.999995630], "fmadd"),
        Operation(Array(f"{no}_r0", io), [Array(f"{no}_t4", io), Array(f"{no}_min_max", io)], "mul"),
        Operation(Array(f"{no}_dyx", io), [Array(f"{no}_abs_y", io), Array(f"{no}_abs_x", io)], "sub"),
        Operation(Array(f"{no}_syx", io), [np.pi/2, Array(f"{no}_dyx", io)], "sgnj"),
        Operation(Array(f"{no}_pyx", io), [Array(f"{no}_syx", io), 0.0], "max"),
        Operation(Array(f"{no}_sr0", io), [Array(f"{no}_r0", io), Array(f"{no}_dyx", io)], "sgnjx"),
        Operation(Array(f"{no}_r1", io), [Array(f"{no}_pyx", io), Array(f"{no}_sr0", io)], "sub"),
        Operation(Array(f"{no}_sx", io), [-np.pi, copy.deepcopy(x_elem)], "sgnjx"),
        Operation(Array(f"{no}_px", io), [Array(f"{no}_sx", io), 0.0], "max"),
        Operation(Array(f"{no}_sr1", io), [Array(f"{no}_r1", io), copy.deepcopy(x_elem)], "sgnjx"),
        Operation(Array(f"{no}_r2", io), [Array(f"{no}_px", io), Array(f"{no}_sr1", io)], "add"),
        Operation(Array(no, io), [Array(f"{no}_r2", io), Array(ny, iy)], "sgnjx"),
    ]


def atan_op(out_elem, inp_elem):
    out_elem = canonicalize_access(out_elem)
    inp_elem = canonicalize_access(inp_elem)
    no = out_elem.name
    io = out_elem.indices
    ni = inp_elem.name
    ii = inp_elem.indices
    if io != ii:
        raise ValueError("Not supported")
    return [
        Operation(Array(f"{no}_abs_y", io), [Array(ni, ii)], "abs"),
        Operation(Array(f"{no}_max_abs", io), [1.0, Array(f"{no}_abs_y", io)], "max"),
        Operation(Array(f"{no}_min_abs", io), [1.0, Array(f"{no}_abs_y", io)], "min"),
        *div_op(Array(f"{no}_min_max", io), Array(f"{no}_min_abs", io), Array(f"{no}_max_abs", io)),
        Operation(Array(f"{no}_mm", io), [Array(f"{no}_min_max", io), Array(f"{no}_min_max", io)], "mul"),
        Operation(Array(f"{no}_t0", io), [-0.013480470, Array(f"{no}_mm", io), 0.057477314], "fmadd"),
        Operation(Array(f"{no}_t1", io), [Array(f"{no}_t0", io), Array(f"{no}_mm", io), -0.121239071], "fmadd"),
        Operation(Array(f"{no}_t2", io), [Array(f"{no}_t1", io), Array(f"{no}_mm", io), 0.195635925], "fmadd"),
        Operation(Array(f"{no}_t3", io), [Array(f"{no}_t2", io), Array(f"{no}_mm", io), -0.332994597], "fmadd"),
        Operation(Array(f"{no}_t4", io), [Array(f"{no}_t3", io), Array(f"{no}_mm", io), 0.999995630], "fmadd"),
        Operation(Array(f"{no}_r0", io), [Array(f"{no}_t4", io), Array(f"{no}_min_max", io)], "mul"),
        Operation(Array(f"{no}_dyx", io), [Array(f"{no}_abs_y", io), 1.0], "sub"),
        Operation(Array(f"{no}_syx", io), [np.pi/2, Array(f"{no}_dyx", io)], "sgnj"),
        Operation(Array(f"{no}_pyx", io), [Array(f"{no}_syx", io), 0.0], "max"),
        Operation(Array(f"{no}_sr0", io), [Array(f"{no}_r0", io), Array(f"{no}_dyx", io)], "sgnjx"),
        Operation(Array(f"{no}_r1", io), [Array(f"{no}_pyx", io), Array(f"{no}_sr0", io)], "sub"),
        Operation(Array(f"{no}_sr1", io), [Array(f"{no}_r1", io), 1.0], "sgnjx"),
        Operation(Array(no, io), [Array(f"{no}_sr1", io), Array(ni, ii)], "sgnjx"),
    ]


def approx_atanh(src):
    # 0.5 * log((1 + x) / (1 - x))
    ret = 0.5 * approx_ln((1 + src) / (1 - src))
    assert np.allclose(np.arctanh(src), ret, rtol=1e-2, atol=1e-5)
    return ret


def atanh_op(out_elem, inp_elem):
    out_elem = canonicalize_access(out_elem)
    inp_elem = canonicalize_access(inp_elem)
    no = out_elem.name
    io = out_elem.indices
    ni = inp_elem.name
    ii = inp_elem.indices
    if io != ii:
        raise ValueError("Not supported")
    return [
        Operation(Array(f"{no}_t1", io), [1.0, Array(ni, ii)], "add"),
        Operation(Array(f"{no}_t2", io), [1.0, Array(ni, ii)], "sub"),
        *div_op(Array(f"{no}_t3", io), Array(f"{no}_t1", io), Array(f"{no}_t2", io)),
        *ln_op(Array(f"{no}_ln", io), Array(f"{no}_t3", io)),
        Operation(Array(no, io), [0.5, Array(f"{no}_ln", io)], "mul"),
    ]
    
    
def approx_cos(x):
    # adapted from https://developer.download.nvidia.com/cg/cos.html
    # /* C simulation gives a max absolute error of less than 1.8e-7 */
    # const float4 c0 = float4( 0.0,            0.5,
    #                             1.0,            0.0            );
    # const float4 c1 = float4( 0.25,          -9.0,
    #                             0.75,           0.159154943091 );
    # const float4 c2 = float4( 24.9808039603, -24.9808039603,
    #                         -60.1458091736,  60.1458091736  );
    # const float4 c3 = float4( 85.4537887573, -85.4537887573,
    #                         -64.9393539429,  64.9393539429  );
    # const float4 c4 = float4( 19.7392082214, -19.7392082214,
    #                         -1.0,            1.0            );
    # /* r0.x = cos(a) */
    # float3 r0, r1, r2;
    # r1.x  = c1.w * a;                       // normalize input
    # r1.y  = frac( r1.x );                   // and extract fraction
    # r2.x  = (float) ( r1.y < c1.x );        // range check: 0.0 to 0.25
    # r2.yz = (float2) ( r1.yy >= c1.yz );    // range check: 0.75 to 1.0
    # r2.y  = dot( r2, c4.zwz );              // range check: 0.25 to 0.75
    # r0    = c0.xyz - r1.yyy;                // range centering
    # r0    = r0 * r0;
    # r1    = c2.xyx * r0 + c2.zwz;           // start power series
    # r1    =     r1 * r0 + c3.xyx;
    # r1    =     r1 * r0 + c3.zwz;
    # r1    =     r1 * r0 + c4.xyx;
    # r1    =     r1 * r0 + c4.zwz;
    # r0.x  = dot( r1, -r2 );                 // range extract
    # return r0.x;
    r1x = 0.159154943091 * x
    r1y = r1x - np.floor(r1x)
    r0y = 0.5 - r1y
    diff_25 = np.abs(r0y) - 0.25
    on_edges = np.sign(diff_25)
    r0 = 0.25 - np.abs(diff_25)
    r00 = r0 * r0
    r1 = 24.9808039603 * r00 - 60.1458091736
    r1 = r1 * r00 + 85.4537887573
    r1 = r1 * r00 - 64.9393539429
    r1 = r1 * r00 + 19.7392082214
    r1 = r1 * r00 - 1.0
    #r0x = -r1 * on_edges  # alternative to the next line, r1 <= 0
    r0x = np.abs(r1) * on_edges
    # print("approx cos", r0x)
    # print("np cos", np.cos(x))
    assert np.allclose(np.cos(x), r0x, rtol=1e-7, atol=1e-7)
    return r0x    


def cos_op(out_elem, inp_elem):
    out_elem = canonicalize_access(out_elem)
    inp_elem = canonicalize_access(inp_elem)
    no = out_elem.name
    io = out_elem.indices
    ni = inp_elem.name
    ii = inp_elem.indices
    if io != ii:
        raise ValueError("Not supported")
    return [
        Operation(Array(f"{no}_r1x", io), [0.159154943091, Array(ni, ii)], "mul"),
        *frac_op(Array(f"{no}_r1y", io), Array(f"{no}_r1x", io)),
        Operation(Array(f"{no}_r0y", io), [0.5, Array(f"{no}_r1y", io)], "sub"),
        Operation(Array(f"{no}_abs_r0y", io), [Array(f"{no}_r0y", io)], "abs"),
        Operation(Array(f"{no}_diff_25", io), [Array(f"{no}_abs_r0y", io), 0.25], "sub"),
        Operation(Array(f"{no}_abs_25", io), [Array(f"{no}_diff_25", io)], "abs"),
        Operation(Array(f"{no}_r0", io), [0.25, Array(f"{no}_abs_25", io)], "sub"),
        Operation(Array(f"{no}_r00", io), [Array(f"{no}_r0", io), Array(f"{no}_r0", io)], "mul"),
        Operation(Array(f"{no}_r1", io), [24.9808039603, Array(f"{no}_r00", io), -60.1458091736], "fmadd"),
        Operation(Array(f"{no}_r2", io), [Array(f"{no}_r1", io), Array(f"{no}_r00", io), 85.4537887573], "fmadd"),
        Operation(Array(f"{no}_r3", io), [Array(f"{no}_r2", io), Array(f"{no}_r00", io), -64.9393539429], "fmadd"),
        Operation(Array(f"{no}_r4", io), [Array(f"{no}_r3", io), Array(f"{no}_r00", io), 19.7392082214], "fmadd"),
        Operation(Array(f"{no}_r5", io), [Array(f"{no}_r4", io), Array(f"{no}_r00", io), -1.0], "fmadd"),
        Operation(Array(no, io), [Array(f"{no}_r5", io), Array(f"{no}_diff_25", io)], "sgnj"),
    ]

def approx_cosh(src):
    # 0.5 * (exp(x) + exp(-x))
    ret = 0.5 * (approx_exp(src) + approx_exp(-src))
    assert np.allclose(np.cosh(src), ret, rtol=1e-2, atol=1e-5)
    return ret

def cosh_op(out_elem, inp_elem):
    out_elem = canonicalize_access(out_elem)
    inp_elem = canonicalize_access(inp_elem)
    no = out_elem.name
    io = out_elem.indices
    ni = inp_elem.name
    ii = inp_elem.indices
    if io != ii:
        raise ValueError("Not supported")
    return [
        *exp_op(Array(f"{no}_t1", io), Array(ni, ii)),
        Operation(Array(f"{no}_n", io), [Array(ni, ii), -1.0], "sgnjx"),
        *exp_op(Array(f"{no}_t2", io), Array(f"{no}_n", ii)),

        Operation(Array(f"{no}_t3", io), [Array(f"{no}_t1", io), Array(f"{no}_t2", io)], "add"),
        Operation(Array(no, io), [0.5, Array(f"{no}_t3", io)], "mul"),
    ]

def trunc_op(out_elem, inp_elem):
    out_elem = canonicalize_access(out_elem)
    inp_elem = canonicalize_access(inp_elem)
    no = out_elem.name
    io = out_elem.indices
    ni = inp_elem.name
    ii = inp_elem.indices
    if io != ii:
        raise ValueError("Not supported")
    return [
        Operation(Array(f"{no}_int", io), [copy.deepcopy(inp_elem)], "i32_from_f64"),
        Operation(copy.deepcopy(out_elem), [Array(f"{no}_int", io)], "f64_from_i32"),
    ]


def ceil_op(out_elem, inp_elem):
    out_elem = canonicalize_access(out_elem)
    inp_elem = canonicalize_access(inp_elem)
    no = out_elem.name
    io = out_elem.indices
    ni = inp_elem.name
    ii = inp_elem.indices
    if io != ii:
        raise ValueError("Not supported")
    return [
        *trunc_op(Array(f"{no}_trunc", io), copy.deepcopy(inp_elem)),
        Operation(Array(f"{no}_diff", io), [Array(f"{no}_trunc", io), copy.deepcopy(inp_elem)], "sub"),
        Operation(Array(f"{no}_sign", io), [1.0, Array(f"{no}_diff", io)], "sgnj"),
        Operation(Array(f"{no}_neg", io), [0.0, Array(f"{no}_sign", io)], "min"),
        Operation(copy.deepcopy(out_elem), [Array(f"{no}_trunc", io), Array(f"{no}_neg", io)], "sub"),
    ]


def floor_op(out_elem, inp_elem):
    out_elem = canonicalize_access(out_elem)
    inp_elem = canonicalize_access(inp_elem)
    no = out_elem.name
    io = out_elem.indices
    ni = inp_elem.name
    ii = inp_elem.indices
    if io != ii:
        raise ValueError("Not supported")
    return [
        *trunc_op(Array(f"{no}_trunc", io), copy.deepcopy(inp_elem)),
        Operation(Array(f"{no}_diff", io), [copy.deepcopy(inp_elem), Array(f"{no}_trunc", io)], "sub"),
        Operation(Array(f"{no}_sign", io), [1.0, Array(f"{no}_diff", io)], "sgnj"),
        Operation(Array(f"{no}_neg", io), [0.0, Array(f"{no}_sign", io)], "min"),
        Operation(copy.deepcopy(out_elem), [Array(f"{no}_trunc", io), Array(f"{no}_neg", io)], "add"),
    ]


def frac_op(out_elem, inp_elem):
    out_elem = canonicalize_access(out_elem)
    inp_elem = canonicalize_access(inp_elem)
    no = out_elem.name
    io = out_elem.indices
    ni = inp_elem.name
    ii = inp_elem.indices
    if io != ii:
        raise ValueError("Not supported")
    return [
        *floor_op(Array(f"{no}_floor", io), copy.deepcopy(inp_elem)),
        Operation(copy.deepcopy(out_elem), [copy.deepcopy(inp_elem), Array(f"{no}_floor", io)], "sub"),
    ]


# ++++++++++++++++++++++++++ KERNEL IMPLEMENTATIONS ++++++++++++++++++++++++++



def create_layernorm_program():
    program = RootScope(
        ops=[
            *reciprocal_op("IN", "N"),
            Operation("N1", ["N", 1.0], "sub"),
            *reciprocal_op("IN1", "N1"),
            loop_scope(
                dims=["B", "N"],
                ops=Operation("m[{1}]", ["src[{1}, {0}]"], "reduce_add"),
            ),
            loop_scope(
                dims=["B"],
                ops=Operation("mu[{0}]", ["m[{0}]", "IN"], "mul"),
            ),
            loop_scope(
                dims=["B", "N"],
                ops=Operation("diff[{1}, {0}]", ["src[{1}, {0}]", "mu[{1}]"], "sub"),
            ),
            loop_scope(
                dims=["B", "N"],
                ops=Operation("q[{1}]", ["diff[{1}, {0}]", "diff[{1}, {0}]"], "reduce_fmadd"),
            ),
            loop_scope(
                dims=["B", "N"],
                ops=Operation("tmp[{1}, {0}]", ["diff[{1}, {0}]", "gamma[{0}]"], "mul"),
            ),
            loop_scope(dims=["B"], ops=Operation("qeps[{0}]", ["q[{0}]", "IN1", 1e-5], "fmadd")),
            loop_scope(dims=["B"], ops=rsqrt_op("sigma[{0}]", "qeps[{0}]")),
            loop_scope(
                dims=["B", "N"],
                ops=Operation("dst[{1}, {0}]", ["sigma[{1}]", "tmp[{1}, {0}]", "beta[{0}]"], "fmadd"),
            ),
        ],
        name="layernorm",
        input_declarations={
            "B": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "src": Buffer(dims=["B", "N"], dtype="f64"),
            "gamma": Buffer(dims=["N"], dtype="f64"),
            "beta": Buffer(dims=["N"], dtype="f64"),
        },
        reusable_inputs={"src"},
        used_outputs={"dst"},
    )
    return program


def ref_layernorm(input_data):
    B = input_data["B"]
    N = input_data["N"]
    src = input_data["src"]
    gamma = input_data["gamma"]
    beta = input_data["beta"]
    # eps = input_data["eps"]
    eps = 1e-5

    dst = np.zeros_like(src)

    for b in range(B):
        m = 0
        for n in range(N):
            m += src[b, n]
        mu = m * (1/N)
        q = 0
        for n in range(N):
            d = src[b, n] - mu
            q += d * d
            dst[b, n] = d * gamma[n]
        sigma = 1 / np.sqrt(q * (1 / (N - 1)) + eps)
        for n in range(N):
            dst[b, n] = sigma * dst[b, n] + beta[n]

    return {
        "dst": dst
    }


def create_layernorm_inputs():
    B = 32
    N = 32
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes}
    return sizes, fixed_inputs


def test_layernorm():
    B = 64
    N = 64
    fixed_inputs = {
        "B": B,
        "N": N,
    }
    sizes = {
        "B": B,
        "N": N,
    }
    def opt(program):
        specialize_inputs(program, sizes)
        apply_manual_layernorm_transformations(program)
        # apply_to_exhaustion(program, [
        #     (join_scopes, find_joinable_scopes),
        #     (reuse_arr_dims, find_reusable_arr_dims),
        #     (reuse_buffers, find_reusable_buffers),
        #     (move_buf_to_stack, find_bufs_movable_to_stack),
        #     (enable_ssr, find_applicable_ssr),
        #     (increase_ssr_depth_by_idx, find_increasable_ssr_depth_by_idx),
        #     (merge_ssr_by_idx, find_mergeable_ssr_by_idx),
        #     (activate_ssr_by_id, find_activatable_ssr_by_idx),
        #     (enable_frep, find_applicable_frep),
        # ])
    cycles = run_kernel(create_layernorm_program, ref_layernorm, opt, lambda p, d: test_snitch_code(p, d, "rtl", False), fixed_inputs)
    print("cycles:", cycles)



def create_old_batchnorm_program():
    program = RootScope(
        ops=[
            Operation("NH", ["N", "H"], "mul"),
            Operation("NHW", ["NH", "W"], "mul"),
            Operation("NHW_1", ["NHW", 1.0], "sub"),
            Operation("INHW", [1.0, "NHW"], "div"),
            Operation("INHW_1", [1.0, "NHW_1"], "div"),
            # reciprocal_op("INHW", ["NHW"]),
            # reciprocal_op("INHW_1", ["NHW_1"]),

            Operation("one_momentum", [1.0, "momentum"], "sub"),
            loop_scope(
                dims=["N", "C", "H", "W"],
                ops=Operation("current_sum[{2}]", ["X[{3}, {2}, {1}, {0}]"], "reduce_add"),
            ),
            loop_scope(
                dims=["C"],
                ops=Operation("current_mean[{0}]", ["current_sum[{0}]", "INHW"], "mul"),
            ),
            loop_scope(
                dims=["N", "C", "H", "W"],
                ops=Operation("diff[{3}, {2}, {1}, {0}]", ["X[{3}, {2}, {1}, {0}]", "current_mean[{2}]"], "sub"),
            ),
            loop_scope(
                dims=["N", "C", "H", "W"],
                ops=Operation("diff2_sum[{2}]", ["diff[{3}, {2}, {1}, {0}]", "diff[{3}, {2}, {1}, {0}]"], "reduce_fmadd"),
            ),
            loop_scope(
                dims=["C"],
                ops=Operation("current_var_biased[{0}]", ["diff2_sum[{0}]", "INHW"], "mul"),
            ),
            loop_scope(
                dims=["C"],
                ops=Operation("current_var[{0}]", ["diff2_sum[{0}]", "INHW_1"], "mul"),
            ),
            loop_scope(
                dims=["C"],
                ops=Operation("running_mean_l[{0}]", ["momentum", "input_mean[{0}]"], "mul"),
            ),
            loop_scope(
                dims=["C"],
                ops=Operation("running_mean_r[{0}]", ["one_momentum", "current_mean[{0}]"], "mul"),
            ),
            loop_scope(
                dims=["C"],
                ops=Operation("running_mean[{0}]", ["running_mean_l[{0}]", "running_mean_r[{0}]"], "add"),
            ),
            loop_scope(
                dims=["C"],
                ops=Operation("running_var_l[{0}]", ["momentum", "input_var[{0}]"], "mul"),
            ),
            loop_scope(
                dims=["C"],
                ops=Operation("running_var_r[{0}]", ["one_momentum", "current_var[{0}]"], "mul"),
            ),
            loop_scope(
                dims=["C"],
                ops=Operation("running_var[{0}]", ["running_var_l[{0}]", "running_var_r[{0}]"], "add"),
            ),
            loop_scope(
                dims=["C"],
                ops=Operation("var_eps[{0}]", ["current_var_biased[{0}]", "epsilon"], "add"),
            ),
            loop_scope(
                dims=["C"],
                ops=rsqrt_op("sqrt_var_eps[{0}]", "var_eps[{0}]"),
            ),
            loop_scope(
                dims=["N", "C", "H", "W"],
                ops=Operation("Ysb[{3}, {2}, {1}, {0}]", ["diff[{3}, {2}, {1}, {0}]", "sqrt_var_eps[{2}]"], "mul"),
            ),
            loop_scope(
                dims=["N", "C", "H", "W"],
                ops=Operation("Y[{3}, {2}, {1}, {0}]", ["Ysb[{3}, {2}, {1}, {0}]", "scale[{2}]", "bias[{2}]"], "fmadd"),
            ),
        ],
        name="batchnorm2d",
        input_declarations={
            "N": Buffer(dims=[], dtype="i32"),
            "C": Buffer(dims=[], dtype="i32"),
            "H": Buffer(dims=[], dtype="i32"),
            "W": Buffer(dims=[], dtype="i32"),
            "epsilon": Buffer(dims=[], dtype="f64"),
            "momentum": Buffer(dims=[], dtype="f64"),
            "X": Buffer(dims=["N", "C", "H", "W"], dtype="f64"),
            "scale": Buffer(dims=["C"], dtype="f64"),
            "bias": Buffer(dims=["C"], dtype="f64"),
            "input_mean": Buffer(dims=["C"], dtype="f64"),
            "input_var": Buffer(dims=["C"], dtype="f64"),
            # "one": Const(value=1, dtype="f64"),
        },
        reusable_inputs={"input_mean", "input_var"},
        used_outputs={"Y", "running_mean", "running_var"},
    )
    return program

def create_batchnorm_program():
    program = RootScope(
        ops=[
            Operation("NH", ["N", "H"], "mul"),
            Operation("NHW", ["NH", "W"], "mul"),
            Operation("NHW_1", ["NHW", 1.0], "sub"),
            *reciprocal_op("INHW", "NHW"),
            *reciprocal_op("INHW_1", "NHW_1"),

            Operation("one_momentum", [1.0, "momentum"], "sub"),
            loop_scope(
                dims=["N", "C", "H", "W"],
                ops=Operation("current_sum[{2}]", ["X[{3}, {2}, {1}, {0}]"], "reduce_add"),
            ),
            loop_scope(
                dims=["C"],
                ops=Operation("current_mean[{0}]", ["current_sum[{0}]", "INHW"], "mul"),
            ),
            loop_scope(
                dims=["N", "C", "H", "W"],
                ops=Operation("diff[{3}, {2}, {1}, {0}]", ["X[{3}, {2}, {1}, {0}]", "current_mean[{2}]"], "sub"),
            ),
            loop_scope(
                dims=["N", "C", "H", "W"],
                ops=Operation("diff2_sum[{2}]", ["diff[{3}, {2}, {1}, {0}]", "diff[{3}, {2}, {1}, {0}]"], "reduce_fmadd"),
            ),
            loop_scope(
                dims=["C"],
                ops=Operation("current_var_biased[{0}]", ["diff2_sum[{0}]", "INHW"], "mul"),
            ),
            loop_scope(
                dims=["C"],
                ops=Operation("current_var[{0}]", ["diff2_sum[{0}]", "INHW_1"], "mul"),
            ),
            loop_scope(
                dims=["C"],
                ops=Operation("running_mean_l[{0}]", ["momentum", "input_mean[{0}]"], "mul"),
            ),
            loop_scope(
                dims=["C"],
                ops=Operation("running_mean_r[{0}]", ["one_momentum", "current_mean[{0}]"], "mul"),
            ),
            loop_scope(
                dims=["C"],
                ops=Operation("running_mean[{0}]", ["running_mean_l[{0}]", "running_mean_r[{0}]"], "add"),
            ),
            loop_scope(
                dims=["C"],
                ops=Operation("running_var_l[{0}]", ["momentum", "input_var[{0}]"], "mul"),
            ),
            loop_scope(
                dims=["C"],
                ops=Operation("running_var_r[{0}]", ["one_momentum", "current_var[{0}]"], "mul"),
            ),
            loop_scope(
                dims=["C"],
                ops=Operation("running_var[{0}]", ["running_var_l[{0}]", "running_var_r[{0}]"], "add"),
            ),
            loop_scope(
                dims=["C"],
                ops=Operation("var_eps[{0}]", ["current_var_biased[{0}]", "epsilon"], "add"),
            ),
            loop_scope(
                dims=["C"],
                ops=rsqrt_op("sqrt_var_eps[{0}]", "var_eps[{0}]"),
            ),
            loop_scope(
                dims=["C"],
                ops=Operation("alpha[{0}]", ["sqrt_var_eps[{0}]" ,"scale[{0}]"], "mul"),
            ),
            loop_scope(
                dims=["C"],
                ops=Operation("beta[{0}]", ["alpha[{0}]", "current_mean[{0}]", "bias[{0}]"], "fnmsub"),
            ),
            loop_scope(
                dims=["N", "C", "H", "W"],
                ops=Operation("Y[{3}, {2}, {1}, {0}]", ["X[{3}, {2}, {1}, {0}]", "alpha[{2}]", "beta[{2}]"], "fmadd"),
            ),
        ],
        name="batchnorm2d",
        input_declarations={
            "N": Buffer(dims=[], dtype="i32"),
            "C": Buffer(dims=[], dtype="i32"),
            "H": Buffer(dims=[], dtype="i32"),
            "W": Buffer(dims=[], dtype="i32"),
            "epsilon": Buffer(dims=[], dtype="f64"),
            "momentum": Buffer(dims=[], dtype="f64"),
            "X": Buffer(dims=["N", "C", "H", "W"], dtype="f64"),
            "scale": Buffer(dims=["C"], dtype="f64"),
            "bias": Buffer(dims=["C"], dtype="f64"),
            "input_mean": Buffer(dims=["C"], dtype="f64"),
            "input_var": Buffer(dims=["C"], dtype="f64"),
        },
        reusable_inputs={"input_mean", "input_var"},
        used_outputs={"Y", "running_mean", "running_var"},
    )
    return program


def ref_batchnorm(input_data):
    N = input_data["N"]
    C = input_data["C"]
    H = input_data["H"]
    W = input_data["W"]
    epsilon = input_data["epsilon"]
    momentum = input_data["momentum"]
    X = input_data["X"]
    scale = input_data["scale"]
    bias = input_data["bias"]
    input_mean = input_data["input_mean"]
    input_var = input_data["input_var"]

    Y = np.zeros_like(X)
    running_mean = np.zeros_like(input_mean)
    running_var = np.zeros_like(input_var)

    for c in range(C):
        current_mean = 0
        for n, h, w in np.ndindex((N, H, W)):
            current_mean += X[n, c, h, w]
        current_mean /= N * H * W
        current_var = 0
        for n, h, w in np.ndindex((N, H, W)):
            d = X[n, c, h, w] - current_mean
            Y[n, c, h, w] = d
            current_var += d ** 2
        current_var_biased = current_var / (N * H * W)
        rsqrt = 1 / np.sqrt(current_var_biased + epsilon)
        current_var /= N * H * W - 1
        running_mean[c] = momentum * input_mean[c] + (1 - momentum) * current_mean
        running_var[c] = momentum * input_var[c] + (1 - momentum) * current_var
        for n, h, w in np.ndindex((N, H, W)):
            Y[n, c, h, w] = Y[n, c, h, w] * rsqrt * scale[c] + bias[c]

    ref_running_mean = copy.deepcopy(input_mean)
    ref_running_var = copy.deepcopy(input_var)
    ref_Y = torch.nn.functional.batch_norm(
        torch.from_numpy(X),
        torch.from_numpy(ref_running_mean),
        torch.from_numpy(ref_running_var),
        weight=torch.from_numpy(scale),
        bias=torch.from_numpy(bias),
        training=True,
        momentum=1-float(momentum),
        eps=float(epsilon),
    ).numpy()
    assert np.allclose(ref_running_mean, running_mean, rtol=1e-2, atol=1e-5)
    assert np.allclose(ref_running_var, running_var, rtol=1e-2, atol=1e-5)
    assert np.allclose(ref_Y, Y, rtol=1e-2, atol=1e-5)

    return {
        "Y": Y,
        "running_mean": running_mean,
        "running_var": running_var,
    }


def create_batchnorm_inputs():
    N = 4
    C = 4
    H = 8
    W = 8
    sizes = {
        "N": N,
        "C": C,
        "H": H,
        "W": W,
    }
    fixed_inputs = {
       **sizes,
        "X": np.random.uniform(-10, 10, (N, C, H, W)),
    }
    return sizes, fixed_inputs


def test_batchnorm():
    N = 1
    C = 16
    H = 8
    W = 8
    fixed_inputs = {
        "N": N,
        "C": C,
        "H": H,
        "W": W,
        "X": np.random.uniform(-10, 10, (N, C, H , W)),
    }
    sizes = {
        "N": N,
        "C": C,
        "H": H,
        "W": W,
    }
    def opt(program):
        specialize_inputs(program, sizes)
        apply_to_exhaustion(program, [
            (join_scopes, find_joinable_scopes),
            (reuse_arr_dims, find_reusable_arr_dims),
            (reuse_buffers, find_reusable_buffers),
            (move_buf_to_stack, find_bufs_movable_to_stack),
            (enable_ssr, find_applicable_ssr),
            (increase_ssr_depth_by_idx, find_increasable_ssr_depth_by_idx),
            (merge_ssr_by_idx, find_mergeable_ssr_by_idx),
            (activate_ssr_by_id, find_activatable_ssr_by_idx),
            (enable_frep, find_applicable_frep),
        ])
        # apply_manual_batchnorm_transformations(program)
    # cycles = run_kernel(create_batchnorm_program, ref_batchnorm, opt, test_snitch_code_rtl,fixed_inputs, "batchnorm2d.c")
    def mytester(program, ref_data):
        return test_snitch_code(program, ref_data, simulator="rtl", check_correctness=False)
        # return test_snitch_code(program, ref_data, simulator="banshee", check_correctness=True)
    cycles = run_kernel(create_batchnorm_program, ref_batchnorm, opt, mytester, fixed_inputs, None)

    write_results("batchnorm", f"{N}x{C}x{H}x{W}", cycles)

    print("cycles:", cycles)


def create_gemm_program():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["M", "N", "K"],
                ops=Operation("acc[{2}, {1}]", ["A[{2}, {0}]", "B[{0}, {1}]"], "reduce_fmadd"),
            ),
            loop_scope(
                dims=["M", "N"],
                ops=Operation("alpha_acc[{1}, {0}]", ["alpha", "acc[{1}, {0}]"], "mul"),
            ),
            loop_scope(
                dims=["M", "N"],
                ops=Operation("D[{1}, {0}]", ["beta", "C[{1}, {0}]", "alpha_acc[{1}, {0}]"], "fmadd"),
            ),
        ],
        # ops=[
        #     loop_scope(
        #         dims=["M", "N", "K"],
        #         ops=Operation("acc[{2}, {1}]", ["A[{2}, {0}]", "B[{0}, {1}]"], "reduce_fmadd"),
        #     ),
        #     loop_scope(
        #         dims=["M", "N"],
        #         ops=Operation("alpha_acc[{1}, {0}]", ["alpha", "acc[{1}, {0}]"], "mul"),
        #     ),
        #     loop_scope(
        #         dims=["M", "N"],
        #         ops=Operation("beta_C[{1}, {0}]", ["beta", "C[{1}, {0}]"], "mul"),
        #     ),
        #     loop_scope(
        #         dims=["M", "N"],
        #         ops=Operation("D[{1}, {0}]", ["alpha_acc[{1}, {0}]", "beta_C[{1}, {0}]"], "add"),
        #     ),
        # ],
        name="gemm",
        input_declarations={
            "M": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "K": Buffer(dims=[], dtype="i32"),
            "alpha": Buffer(dims=[], dtype="f64"),
            "beta": Buffer(dims=[], dtype="f64"),
            "A": Buffer(dims=["M", "K"], dtype="f64"),
            "B": Buffer(dims=["K", "N"], dtype="f64"),
            "C": Buffer(dims=["M", "N"], dtype="f64"),
        },
        reusable_inputs={"C"},
        used_outputs={"D"},
    )
    return program


def create_gemm_program_f32():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["M", "N", "K"],
                ops=Operation("acc[{2}, {1}]", ["A[{2}, {0}]", "B[{0}, {1}]"], OpDesc("f32", "{0} * {1}", reduce="{y} += {x}")),
            ),
            loop_scope(
                dims=["M", "N"],
                ops=Operation("alpha_acc[{1}, {0}]", ["alpha", "acc[{1}, {0}]"], OpDesc("f32", "{0} * {1}")),
            ),
            loop_scope(
                dims=["M", "N"],
                ops=Operation("D[{1}, {0}]", ["beta", "C[{1}, {0}]", "alpha_acc[{1}, {0}]"], OpDesc("f32", '+ {0} * {1} + {2}')),
            ),
        ],
        name="gemm",
        input_declarations={
            "M": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "K": Buffer(dims=[], dtype="i32"),
            "alpha": Buffer(dims=[], dtype="f32"),
            "beta": Buffer(dims=[], dtype="f32"),
            "A": Buffer(dims=["M", "K"], dtype="f32"),
            "B": Buffer(dims=["K", "N"], dtype="f32"),
            "C": Buffer(dims=["M", "N"], dtype="f32"),
        },
        reusable_inputs={"C"},
        used_outputs={"D"},
    )
    return program


def create_gemm_program_f16():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["M", "N", "K"],
                ops=Operation("acc[{2}, {1}]", ["A[{2}, {0}]", "B[{0}, {1}]"], OpDesc("f16", "{0} * {1}", reduce="{y} += {x}")),
            ),
            loop_scope(
                dims=["M", "N"],
                ops=Operation("alpha_acc[{1}, {0}]", ["alpha", "acc[{1}, {0}]"], OpDesc("f16", "{0} * {1}")),
            ),
            loop_scope(
                dims=["M", "N"],
                ops=Operation("D[{1}, {0}]", ["beta", "C[{1}, {0}]", "alpha_acc[{1}, {0}]"], OpDesc("f16", '+ {0} * {1} + {2}')),
            ),
        ],
        name="gemm",
        input_declarations={
            "M": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "K": Buffer(dims=[], dtype="i32"),
            "alpha": Buffer(dims=[], dtype="f16"),
            "beta": Buffer(dims=[], dtype="f16"),
            "A": Buffer(dims=["M", "K"], dtype="f16"),
            "B": Buffer(dims=["K", "N"], dtype="f16"),
            "C": Buffer(dims=["M", "N"], dtype="f16"),
        },
        reusable_inputs={"C"},
        used_outputs={"D"},
    )
    return program



def ref_gemm(input_data):
    # M = input_data["M"]
    # N = input_data["N"]
    # K = input_data["K"]
    alpha = input_data["alpha"]
    beta = input_data["beta"]
    A = input_data["A"]
    B = input_data["B"]
    C = input_data["C"]

    D = np.zeros_like(C)

    # for m, n in np.ndindex((M, N)):
    #     acc = 0
    #     for k in range(K):
    #         acc += A[m, k] * B[k, n]
    #     D[m, n] = alpha * acc + beta * C[m, n]
    # D = alpha * np.einsum("mk,kn->mn", A, B) + beta * C

    if A.dtype == np.float64:
        ref_D = scipy.linalg.blas.dgemm(alpha=alpha, a=A, b=B, beta=beta, c=C, overwrite_c=False)
    elif A.dtype == np.float32:
        ref_D = scipy.linalg.blas.sgemm(alpha=alpha, a=A, b=B, beta=beta, c=C, overwrite_c=False)
    else:
        #TODO: hgemm does not exist in scipy
        ref_D = hgemm(alpha, A, B, beta, np.copy(C))
    # if A.dtype == np.float64:
    #     assert np.allclose(ref_D, D, rtol=1e-2, atol=1e-5)
    # else:
    #     assert np.allclose(ref_D, D, rtol=1e-2, atol=1e-3)

    return {
        "D": ref_D,
    }


def create_gemm_inputs():
    N = 1024
    M = 1024
    K = 1024
    sizes = {"N": N, "M": M, "K": K}
    fixed_inputs = {**sizes}
    return sizes, fixed_inputs


def test_gemm():
    #greedy 22571
    M = 8
    N = 16
    K = 8
    fixed_inputs = {
            "M": M,
            "N": N,
            "K": K,
            "A": np.random.uniform(-1, 1, (M, K)),
            "B": np.random.uniform(-1, 1, (K, N)),
            "C": np.random.uniform(-1, 1, (M, N)),
            "alpha": 3.121,
            "beta": 1.32,
        }
    size_dict = {"M": M, "N": N, "K": K}
    def opt(program):
        specialize_inputs(program, size_dict)
        apply_manual_gemm_transformations(program)
        
    cycles = run_kernel(create_gemm_program, ref_gemm, opt, test_snitch_code, fixed_inputs=fixed_inputs, hand_optimized_file="gemm.c")
    #write_results("gemm", f"{M}x{N}x{K}", cycles)
    print("cycles:", cycles)



def create_matmul_program():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["M", "N", "K"],
                ops=Operation("D[{2}, {1}]", ["A[{2}, {0}]", "B[{0}, {1}]"], "reduce_fmadd"),
            ),
        ],
        name="matmul",
        input_declarations={
            "M": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "K": Buffer(dims=[], dtype="i32"),
            "A": Buffer(dims=["M", "K"], dtype="f64"),
            "B": Buffer(dims=["K", "N"], dtype="f64"),
        },
        reusable_inputs={"C"},
        used_outputs={"D"},
    )
    return program
    

def ref_matmul(input_data):
    A = input_data["A"]
    B = input_data["B"]

    D = np.einsum("mk,kn->mn", A, B)

    ref_D = scipy.linalg.blas.dgemm(alpha=1, a=A, b=B)

    assert np.allclose(ref_D, D, rtol=1e-2, atol=1e-5)

    return {
        "D": D,
    }


def create_matmul_inputs():
    N = 16
    M = 16
    K = 16
    sizes = {"N": N, "M": M, "K": K}
    fixed_inputs = {**sizes}
    return sizes, fixed_inputs


def test_matmul():
    cycles = run_kernel(create_matmul_program, ref_matmul, greedy_opt, test_snitch_code)
    print("cycles:", cycles)



def create_mish_program():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["B", "N"],
                ops=mish_op("Y[{1}, {0}]", "X[{1}, {0}]")
            ),
        ],
        name="mish",
        input_declarations={
            "B": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "X": Buffer(dims=["B", "N"], dtype="f64"),
        },
        reusable_inputs={},
        used_outputs={"Y"},
    )
    return program


def create_mish_inputs():
    B = 8
    N = 8
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "X": np.random.uniform(-1, 1, (B, N))}
    return sizes, fixed_inputs


def ref_mish(input_data):
    # https://github.com/onnx/onnx/blob/main/docs/Operators.md#mish
    # mish(x) = x * tanh(softplus(x)) = x * tanh(ln(1 + e^{x}))
    # softplus(x) = ln(exp(x) + 1)
    X = input_data["X"]
    B = input_data["B"]
    N = input_data["N"]
    Y = np.zeros_like(X)
    for b, n in np.ndindex((B, N)):
        Y[b, n] = X[b, n] * approx_tanh(approx_softplus(X[b, n]))
    ref_Y = torch.nn.functional.mish(torch.from_numpy(X)).numpy()
    assert np.allclose(ref_Y, Y, rtol=1e-2, atol=1e-5)
    return {
        "Y": Y,
    }
    

def test_mish():
    cycles = run_kernel(create_mish_program, ref_mish, apply_manual_mish_transformations, test_snitch_code)
    print("cycles:", cycles)



def create_softplus_program():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["B", "N"],
                ops=softplus_op("Y[{1}, {0}]", "X[{1}, {0}]")
            ),
        ],
        name="softplus",
        input_declarations={
            "B": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "X": Buffer(dims=["B", "N"], dtype="f64"),
        },
        reusable_inputs={},
        used_outputs={"Y"},
    )
    return program


def ref_softplus(input_data):
    X = input_data["X"]
    Y = np.zeros_like(X)
    ref_Y = torch.nn.functional.softplus(torch.from_numpy(X)).numpy()
    B = input_data["B"]
    N = input_data["N"]
    for b, n in np.ndindex((B, N)):
        Y[b, n] = approx_ln(1 + approx_exp(X[b, n]))
    assert np.allclose(ref_Y, Y, rtol=1e-2, atol=1e-5)
    return {
        "Y": Y,
    }


def create_softplus_inputs():
    B = 16
    N = 16
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "X": np.random.uniform(-1, 1, (B, N))}
    return sizes, fixed_inputs


def test_softplus():
    #rtl     man 18880  greedy 33803
    #banshee man 6875   greedy 6221
    fixed_inputs = {
            "B": 16,
            "N": 16,
            "X": np.random.uniform(-1, 1, (16, 16)),
        }
    sizes = {
            "B": 16,
            "N": 16,
        }
    def opt(program):
        specialize_inputs(program, sizes)
        apply_manual_softplus_transformations(program)

    cycles = run_kernel(create_softplus_program, ref_softplus, opt, test_snitch_code, fixed_inputs)
    print("cycles:", cycles)


def create_rmsnorm_program_f32avx():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["B", "Ndiv8"],
                ops=Operation("src_vec[{1}, {0}]",
                              [
                                  "src[{1}, {0} * 8]",
                                  "src[{1}, {0} * 8 + 1]",
                                  "src[{1}, {0} * 8 + 2]",
                                  "src[{1}, {0} * 8 + 3]",
                                  "src[{1}, {0} * 8 + 4]",
                                  "src[{1}, {0} * 8 + 5]",
                                  "src[{1}, {0} * 8 + 6]",
                                  "src[{1}, {0} * 8 + 7]"
                              ], OpDesc("m256", "_mm256_set_ps({7},{6},{5},{4},{3},{2},{1},{0})")),
            ),
            loop_scope(
                dims=["B", "Ndiv8"],
                ops=Operation("squares[{1}, {0}]", ["src_vec[{1}, {0}]"], OpDesc("m256", "_mm256_mul_ps({0}, {0})")),
            ),
            loop_scope(
                dims=["B", "Ndiv8"],
                ops=Operation("sums[{1}]", ["squares[{1}, {0}]"], OpDesc("m256", reduce="{y} = _mm256_add_ps({y}, {x})")),
            ),
            Operation("fN",["N"], OpDesc("m256", "_mm256_set1_ps({0})")),
            loop_scope(
                dims=["B"],
                ops=Operation("means[{0}]", ["sums[{0}]", "fN"], OpDesc("m256", "_mm256_div_ps({0},{1})")),
            ),
            Operation("eps_vec", ["eps"], OpDesc("m256", "_mm256_set1_ps({0})")),
            loop_scope(
                dims=["B"],
                ops=Operation("means_plus_eps[{0}]", ["eps_vec","means[{0}]"], OpDesc("m256", "_mm256_add_ps({0},{1})")),
            ),
            loop_scope(dims=["B"], ops=Operation("rsqrts[{0}]", ["means_plus_eps[{0}]"], OpDesc("m256", "_mm256_rsqrt_ps({0})"))),
            loop_scope(
                dims=["B","Ndiv8"],
                ops=Operation("dst_vec[{1}, {0}]", ["rsqrts[{1}]", "src_vec[{1}, {0}]"], OpDesc("m256", "_mm256_mul_ps({0}, {1})"))
            ),
            loop_scope(
                dims=["B", "N"],
                ops=Operation("dst[{1}, {0}]", ["dst_vec[{1}, {0} / 8]"],
                              OpDesc("f32", "get_scalar_from_vec({0})"))
            )
        ],
        name="rmsnorm",
        input_declarations={
            "B": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "Ndiv8": Buffer(dims=[], dtype="i32"),
            "eps": Buffer(dims=[], dtype="f32"),
            "src": Buffer(dims=["B", "N"], dtype="f32"),
        },
        reusable_inputs={"src"},
        used_outputs={"dst"},
    )
    return program

def create_rmsnorm_program_f32():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["B", "N"],
                ops=Operation("squares[{1}, {0}]", ["src[{1}, {0}]"], OpDesc("f32", "{0} * {0}")),
            ),
            loop_scope(
                dims=["B", "N"],
                ops=Operation("sums[{1}]", ["squares[{1}, {0}]"], OpDesc("f32", reduce="{y} += {x}")),
            ),
            Operation("fN",["N"], OpDesc("f32", "(f32) {0}")),
            loop_scope(
                dims=["B"],
                ops=Operation("means[{0}]", ["sums[{0}]", "fN"], OpDesc("f32", "{0} / {1}")),
            ),
            loop_scope(
                dims=["B"],
                ops=Operation("means_plus_eps[{0}]", ["eps","means[{0}]"], OpDesc("f32", "{0} + {1}")),
            ),
            loop_scope(dims=["B"], ops=rsqrt_op("rsqrts[{0}]", "means_plus_eps[{0}]")),
            loop_scope(
                dims=["B","N"],
                ops=Operation("dst[{1}, {0}]", ["rsqrts[{1}]", "src[{1}, {0}]"], OpDesc("f32", "{0} * {1}"))
            )
        ],
        name="rmsnorm",
        input_declarations={
            "B": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "eps": Buffer(dims=[], dtype="f32"),
            "src": Buffer(dims=["B", "N"], dtype="f32"),
        },
        reusable_inputs={"src"},
        used_outputs={"dst"},
    )
    return program
def ref_rmsnorm(input_data):
    B = input_data["B"]
    eps = input_data["eps"]
    src = input_data["src"]

    dst = np.zeros_like(src, dtype=np.float32)
    def _norm(x):
        x = torch.tensor(x, dtype=torch.float32, device='cpu')
        return x * torch.rsqrt(x.pow(2).mean(-1, keepdim=True) + eps)

    for b in range(B):
        dst[b] = _norm(src[b])
    return {
        "dst": dst
    }

def create_softmax_program_f32():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["B", "N"],
                ops=Operation("max_val[{1}]", ["src[{1}, {0}]"], OpDesc("f32", reduce="{y} = fmax({y}, {x})")),
            ),
            loop_scope(
                dims=["B", "N"],
                ops=Operation("diff[{1}, {0}]", ["src[{1}, {0}]", "max_val[{1}]"], OpDesc("f32", "{0} - {1}")),
            ),
            loop_scope(
                dims=["B", "N"],
                ops=sleef_exp_op("exp_diff[{1}, {0}]", "diff[{1}, {0}]")),
            loop_scope(
                dims=["B", "N"],
                ops=Operation("sum_val[{1}]", ["exp_diff[{1}, {0}]"], OpDesc("f32", reduce="{y} += {x}")),
            ),
            loop_scope(
                dims=["B"],
                ops=reciprocal_op_f32("inv_sum_val[{0}]", "sum_val[{0}]"),
            ),
            loop_scope(
                dims=["B", "N"],
                ops=Operation("dst[{1}, {0}]", ["exp_diff[{1}, {0}]", "inv_sum_val[{1}]"], OpDesc("f32", "{0} * {1}")),
            ),
        ],
        name="softmax",
        input_declarations={
            "B": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "src": Buffer(dims=["B", "N"], dtype="f32"),
        },
        reusable_inputs={"src"},
        used_outputs={"dst"},
    )
    return program


def create_softmax_program_f32avx():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["B","Ndiv8"],
                ops=Operation("src_vec[{1}, {0}]",
                              [
                                  "src[{1}, {0} * 8]",
                                  "src[{1}, {0} * 8 + 1]",
                                  "src[{1}, {0} * 8 + 2]",
                                  "src[{1}, {0} * 8 + 3]",
                                  "src[{1}, {0} * 8 + 4]",
                                  "src[{1}, {0} * 8 + 5]",
                                  "src[{1}, {0} * 8 + 6]",
                                  "src[{1}, {0} * 8 + 7]"
                              ], OpDesc("m256", "_mm256_set_ps({7},{6},{5},{4},{3},{2},{1},{0})")),
            ),
            loop_scope(
                dims=["B", "Ndiv8"],
                ops=Operation("max_val[{1}]", ["src_vec[{1}, {0}]"], OpDesc("m256", reduce="{y} = _mm256_max_ps({y}, {x})")),
            ),
            loop_scope(
                dims=["B"],
                ops=Operation("max_val_vec[{0}]", ["max_val[{0}]"], OpDesc("m256", "allreduce_max({0})"))),
            loop_scope(
                dims=["B", "Ndiv8"],
                ops=Operation("diff[{1}, {0}]", ["src_vec[{1}, {0}]", "max_val_vec[{1}]"], OpDesc("m256", "_mm256_sub_ps({0}, {1})")),
            ),
            loop_scope(
                dims=["B", "Ndiv8"],
                ops=sleef_exp8_op("exp_diff[{1}, {0}]", "diff[{1}, {0}]")),
            loop_scope(
                dims=["B", "Ndiv8"],
                ops=Operation("sum_val[{1}]", ["exp_diff[{1}, {0}]"], OpDesc("m256", reduce="{y} = _mm256_add_ps({y},{x})")),
            ),
            loop_scope(
                dims=["B"],
                ops=Operation("sum_val_reduced[{0}]", ["sum_val[{0}]"],
                              OpDesc("f32", "reduce_add({0})")),
            ),
            loop_scope(
                dims=["B"],
                ops=reciprocal_op_f32("inv_sum_val[{0}]", "sum_val_reduced[{0}]"),
            ),
            loop_scope(
                dims=["B", "Ndiv8"],
                ops=Operation("dst_vec[{1}, {0}]", ["exp_diff[{1}, {0}]", "inv_sum_val[{1}]"], OpDesc("m256", "_mm256_mul_ps({0}, _mm256_set1_ps({1}))")),
            ),
            loop_scope(
                dims=["B", "N"],
                ops=Operation("dst[{1}, {0}]", ["dst_vec[{1}, {0} / 8]"],
                              OpDesc("f32", "get_scalar_from_vec({0})")),
            ),
        ],
        name="softmax",
        input_declarations={
            "B": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "Ndiv8": Buffer(dims=[], dtype="i32"),
            "src": Buffer(dims=["B", "N"], dtype="f32"),
        },
        reusable_inputs={"src"},
        used_outputs={"dst"},
    )
    return program

def create_add_program_f32avx():
    program = RootScope(
        ops=[
            # loop_scope(
            #     dims=["B", "Ndiv8"],
            #     ops=Operation("src1_vec[{1}, {0}]",
            #                   [
            #                       "src1[{1}, {0} * 8]",
            #                       "src1[{1}, {0} * 8 + 1]",
            #                       "src1[{1}, {0} * 8 + 2]",
            #                       "src1[{1}, {0} * 8 + 3]",
            #                       "src1[{1}, {0} * 8 + 4]",
            #                       "src1[{1}, {0} * 8 + 5]",
            #                       "src1[{1}, {0} * 8 + 6]",
            #                       "src1[{1}, {0} * 8 + 7]"
            #                   ], OpDesc("m256", "_mm256_set_ps({7},{6},{5},{4},{3},{2},{1},{0})")),
            # ),
            # loop_scope(
            #     dims=["B", "Ndiv8"],
            #     ops=Operation("src2_vec[{1}, {0}]",
            #                   [
            #                       "src2[{1}, {0} * 8]",
            #                       "src2[{1}, {0} * 8 + 1]",
            #                       "src2[{1}, {0} * 8 + 2]",
            #                       "src2[{1}, {0} * 8 + 3]",
            #                       "src2[{1}, {0} * 8 + 4]",
            #                       "src2[{1}, {0} * 8 + 5]",
            #                       "src2[{1}, {0} * 8 + 6]",
            #                       "src2[{1}, {0} * 8 + 7]"
            #                   ], OpDesc("m256", "_mm256_set_ps({7},{6},{5},{4},{3},{2},{1},{0})")),
            # ),
            loop_scope(
                dims=["B", "Ndiv8"],
                ops=[
                    Operation("src1mm",
                              [
                                  "src1[{1}, {0} * 8]",
                                  "src1[{1}, {0} * 8 + 1]",
                                  "src1[{1}, {0} * 8 + 2]",
                                  "src1[{1}, {0} * 8 + 3]",
                                  "src1[{1}, {0} * 8 + 4]",
                                  "src1[{1}, {0} * 8 + 5]",
                                  "src1[{1}, {0} * 8 + 6]",
                                  "src1[{1}, {0} * 8 + 7]"
                              ], OpDesc("m256", "_mm256_set_ps({7},{6},{5},{4},{3},{2},{1},{0})")),
                    Operation("src2mm",
                              [
                                  "src2[{1}, {0} * 8]",
                                  "src2[{1}, {0} * 8 + 1]",
                                  "src2[{1}, {0} * 8 + 2]",
                                  "src2[{1}, {0} * 8 + 3]",
                                  "src2[{1}, {0} * 8 + 4]",
                                  "src2[{1}, {0} * 8 + 5]",
                                  "src2[{1}, {0} * 8 + 6]",
                                  "src2[{1}, {0} * 8 + 7]"
                              ], OpDesc("m256", "_mm256_set_ps({7},{6},{5},{4},{3},{2},{1},{0})")),
                    Operation("add_vec[{1}, {0}]", ["src1mm", "src2mm"],
                              OpDesc("m256", "_mm256_add_ps({0}, {1})"))
                ],
            ),
            loop_scope(
                dims=["B", "Ndiv8", 8],
                ops=[
                    Operation("dst[{2}, {1}*8 + {0}]", ["add_vec[{2}, {1}]","{0}"],
                              OpDesc("f32", "vec_to_scalar({0}, {1})")),
                ]
            )

        ],
        name="add",
        input_declarations={
            "B": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "Ndiv8": Buffer(dims=[], dtype="i32"),
            "src1": Buffer(dims=["B", "N"], dtype="f32"),
            "src2": Buffer(dims=["B", "N"], dtype="f32"),
            "dst": Buffer(dims=["B", "N"], dtype="f32"),
        },
        reusable_inputs={"src1", "src2"},
        used_outputs={"dst"},
    )
    return program

def create_softmax_program():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["B", "N"],
                ops=Operation("max_val[{1}]", ["src[{1}, {0}]"], "reduce_max"),
            ),
            loop_scope(
                dims=["B", "N"],
                ops=Operation("diff[{1}, {0}]", ["src[{1}, {0}]", "max_val[{1}]"], "sub"),
            ),
            loop_scope( dims=["B", "N"], ops=exp_op("exp_diff[{1}, {0}]", "diff[{1}, {0}]") ),
            loop_scope(
                dims=["B", "N"],
                ops=Operation("sum_val[{1}]", ["exp_diff[{1}, {0}]"], "reduce_add"),
            ),
            loop_scope(
                dims=["B"],
                ops=reciprocal_op("inv_sum_val[{0}]", "sum_val[{0}]"),
            ),
            loop_scope(
                dims=["B", "N"],
                ops=Operation("dst[{1}, {0}]", ["exp_diff[{1}, {0}]", "inv_sum_val[{1}]"], "mul"),
            ),
        ],
        name="softmax",
        input_declarations={
            "B": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "src": Buffer(dims=["B", "N"], dtype="f64"),
        },
        reusable_inputs={"src"},
        used_outputs={"dst"},
    )
    return program


def ref_softmax(input_data):
    src = input_data["src"]

    dst = np.zeros_like(src)
    B = src.shape[0]
    N = src.shape[1]

    for b in range(B):
        max_val = float('-inf')
        for n in range(N):
            max_val = max(max_val, src[b, n])
        sum_val = 0
        for n in range(N):
            #exp_diff = math.exp(src[b, n] - max_val)
            diff = src[b, n] - max_val

            exp_diff = approx_exp(diff)
            # here exp_diff = math.exp(src[b, n] - max_val)
            sum_val += exp_diff
            dst[b, n] = exp_diff
        for n in range(N):
            dst[b, n] = dst[b, n] / sum_val

    assert np.allclose(scipy.special.softmax(src, axis=1), dst, rtol=1e-2, atol=1e-5)

    return {
        "dst": dst
    }


def create_softmax_inputs():
    B = 8
    N = 8
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "src": np.random.uniform(-1, 1, (B, N))}
    return sizes, fixed_inputs


def test_softmax():
    B = 48
    N = 48
    fixed_inputs = {
            "B": B,
            "N": N,
            "src": np.random.uniform(-1, 1, (B, N)),
        }
    sizes = {
            "B": B,
            "N": N,
        }
    def opt(program):
        specialize_inputs(program, sizes)
        apply_manual_softmax_transformations(program)

    cycles = run_kernel(create_softmax_program, ref_softmax, opt, test_snitch_code_rtl, fixed_inputs, "softmax.c")
    write_results("softmax", f"{B}x{N}", cycles)
    print("cycles:", cycles)



def create_log_softmax_program():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["B", "N"],
                ops=Operation("max_val[{1}]", ["src[{1}, {0}]"], "reduce_max"),
            ),
            loop_scope(
                dims=["B", "N"],
                ops=Operation("diff[{1}, {0}]", ["src[{1}, {0}]", "max_val[{1}]"], "sub"),
            ),
            loop_scope(dims=["B", "N"], ops=exp_op("exp_diff[{1}, {0}]", "diff[{1}, {0}]")),
            loop_scope(
                dims=["B", "N"],
                ops=Operation("sum_val[{1}]", ["exp_diff[{1}, {0}]"], "reduce_add"),
            ),
            loop_scope(dims=["B"], ops=ln_op("ln_sum[{0}]", "sum_val[{0}]")),
            loop_scope(
                dims=["B", "N"],
                ops=Operation("dst[{1}, {0}]", ["diff[{1}, {0}]", "ln_sum[{1}]"], "sub"),
            ),
        ],
        name="log_softmax",
        input_declarations={
            "B": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "src": Buffer(dims=["B", "N"], dtype="f64"),
        },
        reusable_inputs={"src"},
        used_outputs={"dst"},
    )
    return program


def ref_log_softmax(input_data):
    B = input_data["B"]
    N = input_data["N"]
    src = input_data["src"]

    dst = np.zeros_like(src)

    for b in range(B):
        max_val = float('-inf')
        for n in range(N):
            max_val = max(max_val, src[b, n])
        sum_val = 0
        for n in range(N):
            #exp_diff = math.exp(src[b, n] - max_val)
            diff = src[b, n] - max_val
            # use approximation e^x=(1 + x/n)^n with n=2 ** 10
            exp_diff = approx_exp(diff)
            # here exp_diff = math.exp(src[b, n] - max_val)
            sum_val += exp_diff
            dst[b, n] = diff
        sum_ln = approx_ln(sum_val)
        for n in range(N):
            dst[b, n] = dst[b, n] - sum_ln

    assert np.allclose(scipy.special.log_softmax(src, axis=1), dst, rtol=1e-2, atol=1e-3) 

    return {
        "dst": dst
    }


def create_log_softmax_inputs():
    B = 8
    N = 8
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "src": np.random.uniform(-10, 10, (B, N))}
    return sizes, fixed_inputs


def test_log_softmax():
    cycles = run_kernel(create_log_softmax_program, ref_log_softmax, greedy_opt, test_snitch_code)
    print("cycles:", cycles)



def create_abs_program():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["B", "N"],
                ops=Operation("dst[{1}, {0}]", ["src[{1}, {0}]"], "abs"),
            ),
        ],
        name="func_abs",
        input_declarations={
            "B": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "src": Buffer(dims=["B", "N"], dtype="f64"),
            "dst": Buffer(dims=["B", "N"], dtype="f64"),
        },
        reusable_inputs={"src"},
        used_outputs={"dst"},
    )
    return program


def ref_abs_program(input_data):
    B = input_data["B"]
    N = input_data["N"]
    src = input_data["src"]
    dst = np.abs(src)
    return {"dst": dst}


def test_abs():
    B = 16
    N = 16
    fixed_inputs = {
        "B": B,
        "N": N,
        "src": np.random.uniform(-10, 10, (B, N)),
    }
    sizes = {
            "B": B,
            "N": N,
        }

    def opt(program):
        specialize_inputs(program, sizes)
        apply_manual_elem_by_elem_uop_transformations(program)

    cycles = run_kernel(create_abs_program, ref_abs_program, opt, test_snitch_code, fixed_inputs)
    print("cycles:", cycles)



def create_acos_program():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["B", "N"],
                ops=acos_op("dst[{1}, {0}]", "src[{1}, {0}]"),
            ),
        ],
        name="func_acos",
        input_declarations={
            "B": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "src": Buffer(dims=["B", "N"], dtype="f64"),
            "dst": Buffer(dims=["B", "N"], dtype="f64"),
        },
        reusable_inputs={"src"},
        used_outputs={"dst"},
    )
    return program


def ref_acos_program(input_data):
    B = input_data["B"]
    N = input_data["N"]
    src = input_data["src"]
    dst = approx_acos(src)
    return {"dst": dst}


def test_acos():
    B = 16
    N = 16
    fixed_inputs = {
        "B": B,
        "N": N,
        "src": np.random.uniform(-1, 1, (B, N)),
    }
    sizes = {
            "B": B,
            "N": N,
        }

    def opt(program):
        specialize_inputs(program, sizes)
        apply_manual_elem_by_elem_fun_transformations(program)

    cycles = run_kernel(
        create_acos_program,
        ref_acos_program,
        greedy_opt,
        test_snitch_code,
        fixed_inputs)
    print("cycles:", cycles)



def create_acosh_program():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["B", "N"],
                ops=acosh_op("dst[{1}, {0}]", "src[{1}, {0}]"),
            ),
        ],
        name="func_acosh",
        input_declarations={
            "B": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "src": Buffer(dims=["B", "N"], dtype="f64"),
            "dst": Buffer(dims=["B", "N"], dtype="f64"),
            # "one": Const(value=1, dtype="f64"),
        },
        reusable_inputs={"src"},
        used_outputs={"dst"},
    )
    return program


def ref_acosh_program(input_data):
    B = input_data["B"]
    N = input_data["N"]
    src = input_data["src"]
    dst = approx_acosh(src)
    return {"dst": dst}


def test_acosh():
    cycles = run_kernel(
        create_acosh_program,
        ref_acosh_program,
        greedy_opt,
        test_snitch_code,
        fixed_inputs={
            "B": 10,
            "N": 4,
            "src": np.random.uniform(1, 10, (10, 4)),
        }
    )
    print("cycles:", cycles)


def create_test_create_dimension_program():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["N"],
                ops=Operation("x", ["y[{0}]"], "reduce_add"),
            ),
            Operation("z", ["x"], "move"),
        ],
        name="func_test_create_dimension",
        input_declarations={
            "N": Buffer(dims=[], dtype="i32"),
            "y": Buffer(dims=["N"], dtype="f64"),
        },
        reusable_inputs={},
        used_outputs={"z"},
    )
    return program


def ref_test_create_dimension(input_data):
    return {"z": np.sum(input_data["y"])}


def create_test_create_dimension_inputs():
    N = 16
    sizes = {
        "N": N,
    }
    fixed_inputs = {
        "N": N,
        "y": np.random.uniform(-10, 10, (N,)),
    }
    return sizes, fixed_inputs


def create_add_program():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["B", "N"],
                ops=Operation("dst[{1}, {0}]", ["src1[{1}, {0}]", "src2[{1}, {0}]"], "add"),
            ),
        ],
        name="func_add",
        input_declarations={
            "B": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "src1": Buffer(dims=["B", "N"], dtype="f64"),
            "src2": Buffer(dims=["B", "N"], dtype="f64"),
            "dst": Buffer(dims=["B", "N"], dtype="f64"),
        },
        reusable_inputs={},
        used_outputs={"dst"},
    )
    return program


def ref_add_program(input_data):
    src1 = input_data["src1"]
    src2 = input_data["src2"]
    return {"dst": src1 + src2}


def create_add_inputs():
    B = 16
    N = 16
    sizes = {
        "B": B,
        "N": N,
    }
    fixed_inputs = {
        "B": B,
        "N": N,
        "src1": np.random.uniform(-10, 10, (B, N)),
        "src2": np.random.uniform(-10, 10, (B, N)),
    }
    return sizes, fixed_inputs


def test_add():
    B = 16
    N = 16
    fixed_inputs = {
        "B": B,
        "N": N,
        "src1": np.random.uniform(-10, 10, (B, N)),
        "src2": np.random.uniform(-10, 10, (B, N)),
    }
    sizes = {
            "B": B,
            "N": N,
    }
    def opt(program):
        specialize_inputs(program, sizes)
        apply_manual_elem_by_elem_bop_transformations(program)

    cycles = run_kernel(create_add_program, ref_add_program, opt, test_snitch_code, fixed_inputs)
    print("cycles:", cycles)



def create_affine_grid_2d_program():
    program = RootScope(
        ops=[
            *reciprocal_op("invW", "W"),
            *reciprocal_op("invH", "H"),
            loop_scope(
                dims=["N", "H", "W"],
                ops=[
                    # x = (2 * w + 1) / W - 1
                    # y = (2 * h + 1) / H - 1
                    # grid[n, h, w, 0] = x * theta[n, 0, 0] + y * theta[n, 0, 1] + theta[n, 0, 2]
                    # grid[n, h, w, 1] = x * theta[n, 1, 0] + y * theta[n, 1, 1] + theta[n, 1, 2]
                    Operation("w21[{2}, {1}, {0}]", [2, "{0}", 1], "fmadd"),
                    Operation("x[{2}, {1}, {0}]", ["w21[{2}, {1}, {0}]", "invW", 1], "fmsub"),
                    Operation("h21[{2}, {1}, {0}]", [2, "{1}", 1], "fmadd"),
                    Operation("y[{2}, {1}, {0}]", ["h21[{2}, {1}, {0}]", "invH", 1], "fmsub"),
                    Operation("t1[{2}, {1}, {0}]", ["x[{2}, {1}, {0}]", "theta[{2}, 0, 0]", "theta[{2}, 0, 2]"], "fmadd"),
                    Operation("grid0[{2}, {1}, {0}]", ["y[{2}, {1}, {0}]", "theta[{2}, 0, 1]", "t1[{2}, {1}, {0}]"], "fmadd"),
                    Operation("t2[{2}, {1}, {0}]", ["x[{2}, {1}, {0}]", "theta[{2}, 1, 0]", "theta[{2}, 1, 2]"], "fmadd"),
                    Operation("grid1[{2}, {1}, {0}]", ["y[{2}, {1}, {0}]", "theta[{2}, 1, 1]", "t2[{2}, {1}, {0}]"], "fmadd"),
                ]
            ),
        ],
        name="affine_grid_2d",
        input_declarations={
            "N": Buffer(dims=[], dtype="i32"),
            "H": Buffer(dims=[], dtype="i32"),
            "W": Buffer(dims=[], dtype="i32"),
            "theta": Buffer(dims=["N", 2, 3], dtype="f64"),
        },
        reusable_inputs={},
        used_outputs={"grid0", "grid1"},
    )
    return program


def ref_affine_grid_2d(input_data):
    """Example:

    ref_affine_grid_2d(
        {'N': 4, 'H': 4, 'W': 5, 'theta': np.array([
            [[1, 0, 3], 
             [0, 1, 4]],
            [[np.cos(np.pi/6), -np.sin(np.pi/6), -1], 
             [np.sin(np.pi/6), np.cos(np.pi/6), -2]],
            [[1, 2, 0], 
             [3, 1, 0]],
            [[1, 0, 0], 
             [0, 1, 0]],
        ], dtype=np.float64)})
    """
    
    N = input_data["N"]
    H = input_data["H"]
    W = input_data["W"]
    theta = input_data["theta"]
    grid = np.zeros((N, H, W, 2))
    for n, h, w in np.ndindex((N, H, W)):
        x = (2 * w + 1) / W - 1
        y = (2 * h + 1) / H - 1
        grid[n, h, w, 0] = x * theta[n, 0, 0] + y * theta[n, 0, 1] + theta[n, 0, 2]
        grid[n, h, w, 1] = x * theta[n, 1, 0] + y * theta[n, 1, 1] + theta[n, 1, 2]
    ref_grid = torch.nn.functional.affine_grid(torch.from_numpy(theta), (N, 777, H, W), align_corners=False).numpy()
    assert np.allclose(ref_grid, grid, rtol=1e-2, atol=1e-5)
    return {"grid0": grid[:,:,:,0], "grid1": grid[:,:,:,1]}


def create_affine_grid_2d_inputs():
    sizes = {'N': 4, 'H': 16, 'W': 16}
    fixed_inputs = {
        **sizes, 'theta': np.array([
            [[1, 0, 3], 
             [0, 1, 4]],
            [[np.cos(np.pi/6), -np.sin(np.pi/6), -1], 
             [np.sin(np.pi/6), np.cos(np.pi/6), -2]],
            [[1, 2, 0], 
             [3, 1, 0]],
            [[1, 0, 0], 
             [0, 1, 0]],
        ], dtype=np.float64)
    }
    return sizes, fixed_inputs


def test_affine_grid_2d():
    fixed_inputs = {
        'N': 4, 'H': 4, 'W': 5, 'theta': np.array([
            [[1, 0, 3], 
             [0, 1, 4]],
            [[np.cos(np.pi/6), -np.sin(np.pi/6), -1], 
             [np.sin(np.pi/6), np.cos(np.pi/6), -2]],
            [[1, 2, 0], 
             [3, 1, 0]],
            [[1, 0, 0], 
             [0, 1, 0]],
        ], dtype=np.float64)
    }
    cycles = run_kernel(create_affine_grid_2d_program, ref_affine_grid_2d, greedy_opt, test_snitch_code, fixed_inputs)
    print("cycles:", cycles)



def create_and_program():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["B", "N"],
                ops=Operation("dst[{1}, {0}]", ["src1[{1}, {0}]", "src2[{1}, {0}]"], "and"),
            ),
        ],
        name="func_add",
        input_declarations={
            "B": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "src1": Buffer(dims=["B", "N"], dtype="i32"),
            "src2": Buffer(dims=["B", "N"], dtype="i32"),
            "dst": Buffer(dims=["B", "N"], dtype="i32"),
        },
        reusable_inputs={},
        used_outputs={"dst"},
    )
    return program


def ref_and(input_data):
    src1 = input_data["src1"]
    src2 = input_data["src2"]
    return {"dst": np.logical_and(src1, src2).astype(np.int32)}


def create_and_inputs():
    B, N = 10, 4
    sizes = {"B": B, "N": N}
    fixed_inputs = {
        **sizes,
        "src1": np.random.randint(0, 2, (10, 4)),
        "src2": np.random.randint(0, 2, (10, 4)),
    }
    return sizes, fixed_inputs


def test_and():
    fixed_inputs = {
        "B": 10,
        "N": 4,
        "src1": np.random.randint(0, 2, (10, 4)),
        "src2": np.random.randint(0, 2, (10, 4)),
    }
    cycles = run_kernel(create_and_program, ref_and, greedy_opt, test_snitch_code, fixed_inputs)
    print("cycles:", cycles)



def create_asin_program():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["B", "N"],
                ops=asin_op("dst[{1}, {0}]", "src[{1}, {0}]"),
            ),
        ],
        name="func_asin",
        input_declarations={
            "B": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "src": Buffer(dims=["B", "N"], dtype="f64"),
            "dst": Buffer(dims=["B", "N"], dtype="f64"),
        },
        reusable_inputs={"src"},
        used_outputs={"dst"},
    )
    return program


def ref_asin_program(input_data):
    src = input_data["src"]
    dst = approx_asin(src)
    return {"dst": dst}


def create_asin_inputs():
    B = 16
    N = 16
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "src": np.sin(np.random.uniform(-1, 1, (B, N)))}
    return sizes, fixed_inputs


def test_asin():
    cycles = run_kernel(
        create_asin_program,
        ref_asin_program,
        greedy_opt,
        test_snitch_code,
        fixed_inputs={
            "B": 10,
            "N": 4,
            "src": np.random.uniform(-1, 1, (10, 4)),
        }
    )
    print("cycles:", cycles)



def create_asinh_program():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["B", "N"],
                ops=asinh_op("dst[{1}, {0}]", "src[{1}, {0}]"),
            ),
        ],
        name="func_asinh",
        input_declarations={
            "B": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "src": Buffer(dims=["B", "N"], dtype="f64"),
            "dst": Buffer(dims=["B", "N"], dtype="f64"),
        },
        reusable_inputs={"src"},
        used_outputs={"dst"},
    )
    return program


def ref_asinh_program(input_data):
    src = input_data["src"]
    dst = approx_asinh(src)
    return {"dst": dst}


def create_asinh_inputs():
    B = 8
    N = 8
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes}
    return sizes, fixed_inputs


def test_asinh():
    cycles = run_kernel(
        create_asinh_program,
        ref_asinh_program,
        greedy_opt,
        test_snitch_code,
        fixed_inputs={
            "B": 10,
            "N": 4,
        }
    )
    print("cycles:", cycles)



def create_atan_program():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["B", "N"],
                ops=atan_op("dst[{1}, {0}]", "src[{1}, {0}]"),
            ),
        ],
        name="func_atan",
        input_declarations={
            "B": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "src": Buffer(dims=["B", "N"], dtype="f64"),
            "dst": Buffer(dims=["B", "N"], dtype="f64"),
        },
        reusable_inputs={"src"},
        used_outputs={"dst"},
    )
    return program


def ref_atan_program(input_data):
    src = input_data["src"]
    dst = approx_atan(src)
    return {"dst": dst}


def create_atan_inputs():
    B = 16
    N = 16
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "src": np.tan(np.random.uniform(-np.pi, np.pi, (B, N)))}
    return sizes, fixed_inputs


def test_atan():
    fixed_inputs = {
        "B": 10,
        "N": 4,
        "src": np.tan(np.random.uniform(-np.pi, np.pi, (10, 4))),
    }
    cycles = run_kernel(
        create_atan_program,
        ref_atan_program,
        greedy_opt,
        test_snitch_code,
        fixed_inputs,
    )
    print("cycles:", cycles)



def create_atanh_program():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["B", "N"],
                ops=atanh_op("dst[{1}, {0}]", "src[{1}, {0}]"),
            ),
        ],
        name="func_atanh",
        input_declarations={
            "B": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "src": Buffer(dims=["B", "N"], dtype="f64"),
            "dst": Buffer(dims=["B", "N"], dtype="f64"),
        },
        reusable_inputs={"src"},
        used_outputs={"dst"},
    )
    return program


def ref_atanh_program(input_data):
    src = input_data["src"]
    dst = approx_atanh(src)
    return {"dst": dst}


def create_atanh_inputs():
    B = 8
    N = 8
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "src": np.random.uniform(-0.9, 0.9, (B, N))}
    return sizes, fixed_inputs


def test_atanh():
    fixed_inputs = {
        "B": 10,
        "N": 4,
        "src": np.random.uniform(-1, 1, (10, 4)),
    }
    cycles = run_kernel(
        create_atanh_program,
        ref_atanh_program,
        greedy_opt,
        test_snitch_code,
        fixed_inputs,
    )
    print("cycles:", cycles)


def create_averagepool2d_nopad_program():
    program = RootScope(
        ops=[
            Operation("k2", ["kernel", "kernel"], "mul"),
            *reciprocal_op("inv_k2", "k2"),
            loop_scope(
                dims=["N", "C", "H_out", "W_out", "kernel", "kernel"],
                ops=[
                    Operation("output[{5}, {4}, {3}, {2}]", ["input[{5}, {4}, {3} * stride + {1} * dilation, {2} * stride + {0} * dilation]", "inv_k2"], "reduce_fmadd"),
                ]
            ),
        ],
        name="func_averagepool",
        input_declarations={
            "kernel": Buffer(dims=[], dtype="i32"),
            "stride": Buffer(dims=[], dtype="i32"),
            "dilation": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "C": Buffer(dims=[], dtype="i32"),
            "H_in": Buffer(dims=[], dtype="i32"),
            "W_in": Buffer(dims=[], dtype="i32"),
            "H_out": Buffer(dims=[], dtype="i32"),
            "W_out": Buffer(dims=[], dtype="i32"),
            "input": Buffer(dims=["N", "C", "H_in", "W_in"], dtype="f64"),
            "output": Buffer(dims=["N", "C", "H_out", "W_out"], dtype="f64"),
        },
        reusable_inputs=set(),
        used_outputs={"output"},
    )
    return program


def ref_averagepool2d(input_data):
    kernel = input_data["kernel"]
    stride = input_data["stride"]
    pad = input_data["pad"]
    dilation = input_data["dilation"]
    N = input_data["N"]
    C = input_data["C"]
    H_in = input_data["H_in"]
    W_in = input_data["W_in"]
    H_out = input_data["H_out"]
    W_out = input_data["W_out"]
    input = input_data["input"]
    
    N, C, H_in, W_in = input.shape
    output = np.zeros((N, C, H_out, W_out), dtype=input.dtype)
    for n, c, ho, wo in np.ndindex((N, C, H_out, W_out)):
        # this would be the index of the first accessed element in the input (which may be out of bounds)
        h_offset = ho * stride - pad
        # first h in "h * dilation" that when added to offset actually give value inside the bounds
        h_first = max(0, (- h_offset + dilation - 1) // dilation)
        # last h in "h * dilation" that when added to offset actually give value inside the bounds
        h_last = min(kernel - 1, (H_in - 1 - h_offset) // dilation)

        # this is the actual index of the first accessed element in the input
        # hstart = h_offset + h_first * dilation
        # this is the index of the last accessed element in the input
        # hend = h_offset + h_last * dilation
        # assert (hend - hstart) % dilation == 0

        # same for width dimension
        w_offset = wo * stride - pad
        w_first = max(0, (- w_offset + dilation - 1) // dilation)
        w_last = min(kernel - 1, (W_in - 1 - w_offset) // dilation)

        # wstart = wo * stride - pad + w_first * dilation
        # wend = w_offset + w_last * dilation
        # assert (wend - wstart) % dilation == 0

        out_acc = 0
        for hk_idx in range(h_last - h_first + 1):
            for hw_idx in range(w_last - w_first + 1):
                out_acc += input[n, c, h_offset + (hk_idx + h_first) * dilation, w_offset + (hw_idx + w_first) * dilation]
        output[n, c, ho, wo] = out_acc / ((h_last - h_first + 1) * (w_last - w_first + 1))
    
    return {"output": output}


def ref_averagepool2d_nopad(input_data):
    kernel = input_data["kernel"]
    stride = input_data["stride"]
    dilation = input_data["dilation"]
    H_out = input_data["H_out"]
    W_out = input_data["W_out"]
    input = input_data["input"]
    
    N, C, H_in, W_in = input.shape
    output = np.zeros((N, C, H_out, W_out), dtype=input.dtype)
    inv_k2 = 1 / (kernel * kernel)
    for n, c, ho, wo, hk_idx, hw_idx in np.ndindex((N, C, H_out, W_out, kernel, kernel)):
        output[n, c, ho, wo] += input[n, c, ho * stride + hk_idx * dilation, wo * stride + hw_idx * dilation] * inv_k2
    
    return {"output": output}


def pooling_out_dim(input, kernel, stride, pad, dilation):
    return (input + 2 * pad - (dilation * (kernel - 1) + 1)) // stride + 1


def test_averagepool2d_ref():
    Hs = [15, 13, 20]
    Ws = [14, 12, 19]
    Ks = [2, 3, 4, 5]
    Ss = [1, 2, 3]
    Ps = [0]  # [0, 1, 2, 3]
    Ds = [1, 2, 3]
    N = 4
    C = 3
    for H, W, K, S, P, D in itertools.product(Hs, Ws, Ks, Ss, Ps, Ds):
        H_out = pooling_out_dim(H, K, S, P, D)
        W_out = pooling_out_dim(W, K, S, P, D)
        fixed_inputs = {
            "kernel": K,
            "stride": S,
            "pad": P,
            "dilation": D,
            "N": N,
            "C": C,
            "H_in": H,
            "W_in": W,
            "H_out": H_out,
            "W_out": W_out,
            "input": np.random.uniform(-1, 1, (N, C, H, W)),
        }
        assert np.allclose(ref_averagepool2d(fixed_inputs)["output"], ref_averagepool2d_nopad(fixed_inputs)["output"])


def create_averagepool2d_nopad_inputs():
    sizes = {
        "N": 4,
        "C": 4,
        "H_in": 10,
        "W_in": 10,
        "kernel": 3,
        "stride": 1,
        "dilation": 1,
    }
    sizes["H_out"] = pooling_out_dim(sizes["H_in"], sizes["kernel"], sizes["stride"], 0, sizes["dilation"])
    sizes["W_out"] = pooling_out_dim(sizes["W_in"], sizes["kernel"], sizes["stride"], 0, sizes["dilation"])
    fixed_inputs = {**sizes}
    return sizes, fixed_inputs


def test_averagepool2d_nopad():
    fixed_inputs = {
        "N": 4,
        "C": 3,
        "H_in": 15,
        "W_in": 14,
        "kernel": 3,
        "stride": 2,
        "dilation": 3
    }
    fixed_inputs["H_out"] = pooling_out_dim(fixed_inputs["H_in"], fixed_inputs["kernel"], fixed_inputs["stride"], 0, fixed_inputs["dilation"])
    fixed_inputs["W_out"] = pooling_out_dim(fixed_inputs["W_in"], fixed_inputs["kernel"], fixed_inputs["stride"], 0, fixed_inputs["dilation"])
    cycles = run_kernel(
        create_averagepool2d_nopad_program,
        ref_averagepool2d_nopad,
        greedy_opt,
        test_snitch_code,
        fixed_inputs,
    )
    print("cycles:", cycles)



def create_bitshift_left_program():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["B", "N"],
                ops=Operation("z[{1}, {0}]", ["x[{1}, {0}]", "y[{1}, {0}]"], "slli"),
            ),
        ],
        name="func_shift_left",
        input_declarations={
            "B": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "x": Buffer(dims=["B", "N"], dtype="i32"),
            "y": Buffer(dims=["B", "N"], dtype="i32"),
        },
        reusable_inputs={},
        used_outputs={"z"},
    )
    return program


def ref_bitshift_left(input_data):
    x = input_data["x"]
    y = input_data["y"]
    return {"z": np.left_shift(x, y)}


def create_bitshift_left_inputs():
    B = 64
    N = 64
    sizes = {"B": B, "N": N}
    fixed_inputs = {
        **sizes,
        "x": np.random.randint(0, 1000, (B, N), dtype=np.int32),
        "y": np.random.randint(0, 5, (B, N), dtype=np.int32),
    }
    return sizes, fixed_inputs


def test_bitshift_left():
    B = 3
    N = 4
    fixed_inputs = {
        "B": B, "N": N,
        "x": np.random.randint(0, 1000, (B, N), dtype=np.int32),
        "y": np.random.randint(0, 5, (B, N), dtype=np.int32),
    }
    cycles = run_kernel(create_bitshift_left_program, ref_bitshift_left, greedy_opt, test_snitch_code, fixed_inputs)
    print("cycles:", cycles)



def create_bitwise_and_program():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["B", "N"],
                ops=Operation("z[{1}, {0}]", ["x[{1}, {0}]", "y[{1}, {0}]"], "bitwise_and"),
            ),
        ],
        name="func_bitwise_and",
        input_declarations={
            "B": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "x": Buffer(dims=["B", "N"], dtype="i32"),
            "y": Buffer(dims=["B", "N"], dtype="i32"),
        },
        reusable_inputs={},
        used_outputs={"z"},
    )
    return program


def ref_bitwise_and(input_data):
    x = input_data["x"]
    y = input_data["y"]
    return {"z": np.bitwise_and(x, y)}


def create_bitwise_and_inputs():
    B = 64
    N = 64
    sizes = {"B": B, "N": N}
    fixed_inputs = {
        **sizes,
        "x": np.random.randint(0, 1000, (B, N), dtype=np.int32),
        "y": np.random.randint(0, 1000, (B, N), dtype=np.int32),
    }
    return sizes, fixed_inputs


def test_bitwise_and():
    B = 3
    N = 4
    fixed_inputs = {
        "B": B, "N": N,
        "x": np.random.randint(0, 1000, (B, N), dtype=np.int32),
        "y": np.random.randint(0, 1000, (B, N), dtype=np.int32),
    }
    cycles = run_kernel(create_bitwise_and_program, ref_bitwise_and, greedy_opt, test_snitch_code, fixed_inputs)
    print("cycles:", cycles)



def create_bitwise_not_program():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["B", "N"],
                ops=Operation("z[{1}, {0}]", ["x[{1}, {0}]"], "bitwise_not"),
            ),
        ],
        name="func_bitwise_not",
        input_declarations={
            "B": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "x": Buffer(dims=["B", "N"], dtype="i32"),
            "z": Buffer(dims=["B", "N"], dtype="i32"),
        },
        reusable_inputs={"x"},
        used_outputs={"z"},
    )
    return program


def ref_bitwise_not(input_data):
    x = input_data["x"]
    return {"z": np.bitwise_not(x)}


def create_bitwise_not_inputs():
    B = 64
    N = 64
    sizes = {"B": B, "N": N}
    fixed_inputs = {
        **sizes,
        "x": np.random.randint(0, 1000, (B, N), dtype=np.int32),
    }
    return sizes, fixed_inputs


def test_bitwise_not():
    B = 3
    N = 4
    fixed_inputs = {
        "B": B, "N": N,
        "x": np.random.randint(0, 1000, (B, N), dtype=np.int32),
    }
    cycles = run_kernel(create_bitwise_not_program, ref_bitwise_not, greedy_opt, test_snitch_code, fixed_inputs)
    print("cycles:", cycles)



def create_bitwise_or_program():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["B", "N"],
                ops=Operation("z[{1}, {0}]", ["x[{1}, {0}]", "y[{1}, {0}]"], "bitwise_or"),
            ),
        ],
        name="func_bitwise_or",
        input_declarations={
            "B": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "x": Buffer(dims=["B", "N"], dtype="i32"),
            "y": Buffer(dims=["B", "N"], dtype="i32"),
            "z": Buffer(dims=["B", "N"], dtype="i32"),
        },
        reusable_inputs={},
        used_outputs={"z"},
    )
    return program


def ref_bitwise_or(input_data):
    x = input_data["x"]
    y = input_data["y"]
    return {"z": np.bitwise_or(x, y)}


def create_bitwise_or_inputs():
    B = 64
    N = 64
    sizes = {"B": B, "N": N}
    fixed_inputs = {
        **sizes,
        "x": np.random.randint(0, 1000, (B, N), dtype=np.int32),
        "y": np.random.randint(0, 1000, (B, N), dtype=np.int32),
    }
    return sizes, fixed_inputs


def test_bitwise_or():
    B = 3
    N = 4
    fixed_inputs = {
        "B": B, "N": N,
        "x": np.random.randint(0, 1000, (B, N), dtype=np.int32),
        "y": np.random.randint(0, 1000, (B, N), dtype=np.int32),
    }
    cycles = run_kernel(create_bitwise_or_program, ref_bitwise_or, greedy_opt, test_snitch_code, fixed_inputs)
    print("cycles:", cycles)



def create_bitwise_xor_program():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["B", "N"],
                ops=Operation("z[{1}, {0}]", ["x[{1}, {0}]", "y[{1}, {0}]"], "bitwise_xor"),
            ),
        ],
        name="func_bitwise_xor",
        input_declarations={
            "B": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "x": Buffer(dims=["B", "N"], dtype="i32"),
            "y": Buffer(dims=["B", "N"], dtype="i32"),
            "z": Buffer(dims=["B", "N"], dtype="i32"),
        },
        reusable_inputs={},
        used_outputs={"z"},
    )
    return program


def ref_bitwise_xor(input_data):
    x = input_data["x"]
    y = input_data["y"]
    return {"z": np.bitwise_xor(x, y)}


def create_bitwise_xor_inputs():
    B = 64
    N = 64
    sizes = {"B": B, "N": N}
    fixed_inputs = {
        **sizes,
        "x": np.random.randint(0, 1000, (B, N), dtype=np.int32),
        "y": np.random.randint(0, 1000, (B, N), dtype=np.int32),
    }
    return sizes, fixed_inputs


def test_bitwise_xor():
    B = 3
    N = 4
    fixed_inputs = {
        "B": B, "N": N,
        "x": np.random.randint(0, 1000, (B, N), dtype=np.int32),
        "y": np.random.randint(0, 1000, (B, N), dtype=np.int32),
    }
    cycles = run_kernel(create_bitwise_xor_program, ref_bitwise_xor, greedy_opt, test_snitch_code, fixed_inputs)
    print("cycles:", cycles)



def create_cos_program():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["B", "N"],
                ops=cos_op("z[{1}, {0}]", "x[{1}, {0}]"),
            ),
        ],
        name="func_cos",
        input_declarations={
            "B": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "x": Buffer(dims=["B", "N"], dtype="f64"),
            "z": Buffer(dims=["B", "N"], dtype="f64"),
        },
        reusable_inputs={},
        used_outputs={"z"},
    )
    return program


def ref_cos(input_data):
    res = approx_cos(input_data["x"])
    return {"z": res}


def create_cos_inputs():
    B = 16
    N = 16
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "x": np.random.uniform(-100, 100, (B, N))}
    return sizes, fixed_inputs


def test_cos():
    B = 4
    N = 3
    fixed_inputs = {
        "B": B,
        "N": N,
        "x": np.random.uniform(-100, 100, (B, N)),
    }
    cycles = run_kernel(create_cos_program, ref_cos, greedy_opt, test_snitch_code, fixed_inputs)
    print("cycles:", cycles)



def create_blackman_window_program():
    program = RootScope(
        ops=[
            Operation("N1", ["N", 1], "sub"),
            *reciprocal_op("N1_reciprocal", "N1"),
            Operation("TWOPI_N", [2.0 * np.pi, "N1_reciprocal"], "mul"),
            Operation("FOURPI_N", [2.0, "TWOPI_N"], "mul"),
            loop_scope(dims=["N"], ops=[
                Operation("arg1[{0}]", ["TWOPI_N", "{0}"], "mul"),
                *cos_op("cos1[{0}]", "arg1[{0}]"),
                Operation("arg2[{0}]", ["FOURPI_N", "{0}"], "mul"),
                *cos_op("cos2[{0}]", "arg2[{0}]"),
                Operation("tmp[{0}]", [0.08, "cos2[{0}]", "0.42"], "fmadd"),
                Operation("w[{0}]", ["cos1[{0}]", -0.5, "tmp[{0}]"], "fmadd"),
            ]),
        ],
        name="func_blackman_window",
        input_declarations={
            "N": Buffer(dims=[], dtype="i32"),
        },
        reusable_inputs={},
        used_outputs={"w"},
    )
    return program


def create_blackman_window_inputs():
    N = 100
    sizes = {"N": N}
    fixed_inputs = {**sizes}
    return sizes, fixed_inputs


def ref_blackman_window(input_data):
    N = input_data["N"]
    # w[n] = 0.42 - 0.5 cos(2 pi n / (N - 1)) + 0.08 cos(4 pi n / (N - 1))
    x = np.arange(N)
    my = 0.42 - 0.5 * np.cos(2 * np.pi * x / (N - 1)) + 0.08 * np.cos(4 * np.pi * x / (N - 1))
    # print("x:", x)
    # print("my:", my)
    # print("reference:", np.blackman(N))
    assert np.allclose(np.blackman(N), my)
    return {"w": np.blackman(N)}


def test_blackman_window():
    N = 11
    fixed_inputs = {"N": N,}
    cycles = run_kernel(create_blackman_window_program, ref_blackman_window, greedy_opt, test_snitch_code, fixed_inputs)
    print("cycles:", cycles)



def create_ceil_program():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["B", "N"],
                ops=ceil_op("z[{1}, {0}]", "x[{1}, {0}]")
            ),
        ],
        name="func_ceil",
        input_declarations={
            "B": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "x": Buffer(dims=["B", "N"], dtype="f64"),
            "z": Buffer(dims=["B", "N"], dtype="f64"),
        },
        reusable_inputs={},
        used_outputs={"z"},
    )
    return program


def ref_ceil(input_data):
    x = input_data["x"]
    return {"z": np.ceil(x)}


def create_ceil_inputs():
    B = 32
    N = 32
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "x": np.random.uniform(-100, 100, (B, N))}
    return sizes, fixed_inputs


def test_ceil():
    B = 3
    N = 4
    fixed_inputs = {
        "B": B, "N": N,
        "x": np.random.uniform(-1000, 1000, (B, N)),
    }
    fixed_inputs["x"][0,0] = -10.0
    fixed_inputs["x"][0,1] = 15.0
    
    cycles = run_kernel(create_ceil_program, ref_ceil, greedy_opt, test_snitch_code, fixed_inputs)
    print("cycles:", cycles)



def create_floor_program():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["B", "N"],
                ops=floor_op("z[{1}, {0}]", "x[{1}, {0}]")
            ),
        ],
        name="func_floor",
        input_declarations={
            "B": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "x": Buffer(dims=["B", "N"], dtype="f64"),
            "z": Buffer(dims=["B", "N"], dtype="f64"),
        },
        reusable_inputs={},
        used_outputs={"z"},
    )
    return program


def ref_floor(input_data):
    x = input_data["x"]
    return {"z": np.floor(x)}


def create_floor_inputs():
    B = 32
    N = 32
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "x": np.random.uniform(-100, 100, (B, N))}
    return sizes, fixed_inputs


def test_floor():
    B = 3
    N = 4
    fixed_inputs = {
        "B": B, "N": N,
        "x": np.random.uniform(-1000, 1000, (B, N)),
    }
    fixed_inputs["x"][0,0] = -10.0
    fixed_inputs["x"][0,1] = 15.0
    cycles = run_kernel(create_floor_program, ref_floor, greedy_opt, test_snitch_code, fixed_inputs)
    print("cycles:", cycles)



def create_clip_program():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["B", "N"],
                ops=[
                    Operation("t[{1}, {0}]", ["x[{1}, {0}]", "a"], "max"),
                    Operation("z[{1}, {0}]", ["t[{1}, {0}]", "b"], "min"),
                ],
            ),
        ],
        name="func_clip",
        input_declarations={
            "B": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "x": Buffer(dims=["B", "N"], dtype="f64"),
            "a": Buffer(dims=[], dtype="f64"),
            "b": Buffer(dims=[], dtype="f64"),
            "z": Buffer(dims=["B", "N"], dtype="f64"),
        },
        reusable_inputs={},
        used_outputs={"z"},
    )
    return program


def ref_clip(input_data):
    x = input_data["x"]
    a = input_data["a"]
    b = input_data["b"]
    return {"z": np.clip(x, a, b)}


def create_clip_inputs():
    B = 64
    N = 64
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "x": np.random.uniform(-10, 10, (B, N)), "a": -5.0, "b": 5.0}
    return sizes, fixed_inputs


def test_clip():
    B = 3
    N = 4
    fixed_inputs = {
        "B": B, "N": N,
        "x": np.random.uniform(-10, 10, (B, N)),
        "a": -5.0,
        "b": 5.0,
    }
    cycles = run_kernel(create_clip_program, ref_clip, greedy_opt, test_snitch_code, fixed_inputs)
    print("cycles:", cycles)



def ref_conv2d(input_data):
    kernel = input_data["kernel"]
    stride = input_data["stride"]
    pad = input_data["pad"]
    dilation = input_data["dilation"]
    M = input_data["M"]
    N = input_data["N"]
    C = input_data["C"]
    G = input_data["G"]
    H_in = input_data["H_in"]
    W_in = input_data["W_in"]
    H_out = input_data["H_out"]
    W_out = input_data["W_out"]
    input = input_data["input"]
    weight = input_data["weight"]
    
    assert input.shape == (N, C, H_in, W_in)
    assert weight.shape == (M, C // G, kernel, kernel)
    output = np.zeros((N, M, H_out, W_out), dtype=input.dtype)
    for g, m, n, c, ho, wo in np.ndindex((G, M // G, N, C // G, H_out, W_out)):
        # see averagepool implementation for explanation
        h_offset = ho * stride - pad
        h_first = max(0, (- h_offset + dilation - 1) // dilation)
        h_last = min(kernel - 1, (H_in - 1 - h_offset) // dilation)

        w_offset = wo * stride - pad
        w_first = max(0, (- w_offset + dilation - 1) // dilation)
        w_last = min(kernel - 1, (W_in - 1 - w_offset) // dilation)

        mm = g * (M // G) + m
        cc = g * (C // G) + c

        for hk_idx in range(h_last - h_first + 1):
            for hw_idx in range(w_last - w_first + 1):
                
                output[n, mm, ho, wo] \
                    += input[n, cc, h_offset + (hk_idx + h_first) * dilation, w_offset + (hw_idx + w_first) * dilation] \
                    * weight[mm, c, h_first + hk_idx, w_first + hw_idx]
    
    return {"output": output}


def ref_conv2d_nopad(input_data):
    kernel = input_data["kernel"]
    stride = input_data["stride"]
    dilation = input_data["dilation"]
    M = input_data["M"]
    N = input_data["N"]
    C = input_data["C"]
    G = input_data.get("G", 1)
    H_in = input_data["H_in"]
    W_in = input_data["W_in"]
    H_out = input_data["H_out"]
    W_out = input_data["W_out"]
    input = input_data["input"]
    weight = input_data["weight"]
    
    assert input.shape == (N, C, H_in, W_in)
    assert weight.shape == (M, C // G, kernel, kernel)
    output = np.zeros((N, M, H_out, W_out), dtype=input.dtype)
    for g, m, n, c, ho, wo in np.ndindex((G, M // G, N, C // G, H_out, W_out)):
        mm = g * (M // G) + m
        cc = g * (C // G) + c
        for hk_idx, hw_idx in np.ndindex((kernel, kernel)):
            output[n, mm, ho, wo] \
                += input[n, cc, ho * stride + hk_idx * dilation, wo * stride + hw_idx * dilation] \
                * weight[mm, c, hk_idx, hw_idx]
    
    return {"output": output}


def create_conv2d_nopad_inputs():
    M = 1
    C = 4
    H = 10
    W = 10
    K = 3
    sizes = {
        "N": 4, #batches
        "M": M, #feature maps (or output channels)
        "C": C, #input channels
        "H_in": H, #input height
        "W_in": W, #inpuy width
        "kernel": K,
        "stride": 1,
        "dilation": 1,
    }
    sizes["H_out"] = pooling_out_dim(sizes["H_in"], sizes["kernel"], sizes["stride"], 0, sizes["dilation"])
    sizes["W_out"] = pooling_out_dim(sizes["W_in"], sizes["kernel"], sizes["stride"], 0, sizes["dilation"])
    sizes["H_out"] = pooling_out_dim(sizes["H_in"], sizes["kernel"], sizes["stride"], 0, sizes["dilation"])
    sizes["W_out"] = pooling_out_dim(sizes["W_in"], sizes["kernel"], sizes["stride"], 0, sizes["dilation"])
    fixed_inputs = {**sizes}
    return sizes, fixed_inputs


def test_conv2d_ref():
    Hs = [15, 20]
    Ws = [14, 19]
    Ks = [2, 3, 4]
    Ss = [1, 2, 3]
    Ps = [0, 1, 2, 3]
    Ds = [1, 2, 3]
    Gs = [1, 2]
    M = 2  # output channels
    N = 3  # minibatch
    C = 4  # input channels
    for H, W, K, S, P, D, G in itertools.product(Hs, Ws, Ks, Ss, Ps, Ds, Gs):
        assert C % G == 0 and M % G == 0
        H_out = pooling_out_dim(H, K, S, P, D)
        W_out = pooling_out_dim(W, K, S, P, D)
        d = {
            "kernel": K,
            "stride": S,
            "pad": P,
            "dilation": D,
            "N": N,
            "M": M,
            "C": C,
            "G": G,
            "H_in": H,
            "W_in": W,
            "H_out": H_out,
            "W_out": W_out,
            "input": np.random.uniform(-1, 1, (N, C, H, W)),
            "weight": np.random.uniform(-1, 1, (M, C // G, K, K)),
        }
        torch_res = torch.nn.functional.conv2d(
            torch.from_numpy(d["input"]),
            torch.from_numpy(d["weight"]),
            stride=d["stride"],
            padding=d["pad"],
            dilation=d["dilation"],
            groups=d["G"],
        ).numpy()
        my_res = ref_conv2d(d)["output"]
        assert np.allclose(my_res, torch_res)
        if P == 0:
            my_res_nopad = ref_conv2d_nopad(d)["output"]
            assert np.allclose(my_res_nopad, torch_res)


def create_conv2d_nopad_program_notmp():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["N", "M", "C", "H_out", "W_out", "kernel", "kernel"],
                ops=[
                    Operation("output[{6}, {5}, {3}, {2}]", ["input[{6}, {4}, {3} * stride + {1} * dilation, {2} * stride + {0} * dilation]", "weight[{5}, {4}, {1}, {0}]"], "reduce_fmadd"),
                ]
            ),
        ],
        name="func_conv2d",
        input_declarations={
            "kernel": Buffer(dims=[], dtype="i32"),
            "stride": Buffer(dims=[], dtype="i32"),
            "dilation": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "M": Buffer(dims=[], dtype="i32"),
            "C": Buffer(dims=[], dtype="i32"),
            "H_in": Buffer(dims=[], dtype="i32"),
            "W_in": Buffer(dims=[], dtype="i32"),
            "H_out": Buffer(dims=[], dtype="i32"),
            "W_out": Buffer(dims=[], dtype="i32"),
            "input": Buffer(dims=["N", "C", "H_in", "W_in"], dtype="f64"),
            "weight": Buffer(dims=["M", "C", "kernel", "kernel"], dtype="f64"),
            "output": Buffer(dims=["N", "M", "H_out", "W_out"], dtype="f64"),
        },
        reusable_inputs={},
        used_outputs={"output"},
    )
    return program


def create_conv2d_nopad_program():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["N", "M", "C", "H_out", "W_out", "kernel", "kernel"],
                ops=[
                    Operation("tmp[{6}, {5}, {3}, {2}]", ["input[{6}, {4}, {3} * stride + {1} * dilation, {2} * stride + {0} * dilation]", "weight[{5}, {4}, {1}, {0}]"], "reduce_fmadd"),
                ]
            ),
            loop_scope(
                dims=["N", "M", "H_out", "W_out"],
                ops=[Operation("output[{3}, {2}, {1}, {0}]", ["tmp[{3}, {2}, {1}, {0}]"], "move"),]
            )
        ],
        name="func_conv2d",
        input_declarations={
            "kernel": Buffer(dims=[], dtype="i32"),
            "stride": Buffer(dims=[], dtype="i32"),
            "dilation": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "M": Buffer(dims=[], dtype="i32"),
            "C": Buffer(dims=[], dtype="i32"),
            "H_in": Buffer(dims=[], dtype="i32"),
            "W_in": Buffer(dims=[], dtype="i32"),
            "H_out": Buffer(dims=[], dtype="i32"),
            "W_out": Buffer(dims=[], dtype="i32"),
            "input": Buffer(dims=["N", "C", "H_in", "W_in"], dtype="f64"),
            "weight": Buffer(dims=["M", "C", "kernel", "kernel"], dtype="f64"),
            "output": Buffer(dims=["N", "M", "H_out", "W_out"], dtype="f64"),
        },
        reusable_inputs={},
        used_outputs={"output"},
    )
    return program


def test_conv2d():
    M = 4
    C = 4
    H = 10
    W = 10
    K = 3

    d = {
        "N": 1, #batches
        "M": M, #feature maps (or output channels)
        "C": C, #input channels
        "H_in": H, #input height
        "W_in": W, #input width
        "kernel": K,
        "stride": 1,
        "dilation": 1,
    }
    sizes = {
        "N": 1, #batches
        "M": M, #feature maps (or output channels)
        "C": C, #input channels
        "H_in": H, #input height
        "W_in": W, #inpuy width
        "kernel": K,
        # "stride": 1,
        # "dilation": 0,
    }
    d["H_out"] = pooling_out_dim(d["H_in"], d["kernel"], d["stride"], 0, d["dilation"])
    d["W_out"] = pooling_out_dim(d["W_in"], d["kernel"], d["stride"], 0, d["dilation"])
    sizes["H_out"] = pooling_out_dim(d["H_in"], d["kernel"], d["stride"], 0, d["dilation"])
    sizes["W_out"] = pooling_out_dim(d["W_in"], d["kernel"], d["stride"], 0, d["dilation"])

    print(d["H_out"],  d["W_out"])

    def opt(program):
        specialize_inputs(program, sizes)
        apply_manual_conv2d_transformations(program)

    cycles = run_kernel(create_conv2d_nopad_program, lambda x: ref_conv2d_nopad({**x, 'G': 1}), opt, test_snitch_code_rtl, fixed_inputs=d, hand_optimized_file="func_conv2d.c")
    write_results("conv", f"{M}x{C}x{H}x{W}x{K}", cycles)
    print("cycles:", cycles)



def create_argmax_program():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["B", "N"],
                ops=Operation("max_vals[{1}]", ["x[{1}, {0}]"], "reduce_max"),
            ),
            loop_scope(
                dims=["B", "N"],
                ops=Operation("max_mask[{1}, {0}]", ["x[{1}, {0}]", "max_vals[{1}]"], "sub"),
            ),
            loop_scope(
                dims=["B", "N"],
                ops=Operation("idx[{1}, {0}]", ["N", "{0}"], "sub"),
            ),
            loop_scope(
                dims=["B", "N"],
                ops=Operation("inv_idx[{1}, {0}]", ["idx[{1}, {0}]", "max_mask[{1}, {0}]"], "sgnj"),
            ),
            loop_scope(
                dims=["B", "N"],
                ops=Operation("max_idx[{1}]", ["inv_idx[{1}, {0}]"], "reduce_max"),
            ),
            loop_scope(
                dims=["B"],
                ops=Operation("max_idx_i[{0}]", ["max_idx[{0}]"], "i32_from_f64"),
            ),
            loop_scope(
                dims=["B"],
                ops=Operation("z[{0}]", ["N", "max_idx_i[{0}]"], "sub"),
            ),
        ],
        name="func_argmax",
        input_declarations={
            "B": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "x": Buffer(dims=["B", "N"], dtype="f64"),
            "z": Buffer(dims=["B"], dtype="i32"),
        },
        reusable_inputs={"x"},
        used_outputs={"z"},
    )
    return program


def ref_argmax(input_data):
    x = input_data["x"]
    B = input_data["B"]
    N = input_data["N"]
    z = np.zeros(shape=B, dtype=np.int32)
    max_vals = np.full(shape=B, fill_value=-np.inf, dtype=np.float64)
    for b in range(B):
        for n in range(N):
            max_vals[b] = max(max_vals[b], x[b, n])
        for n in range(N):
            max_mask = float(np.sign(x[b,n] - max_vals[b]) >= 0)
            max_idx = max_mask * (N - n)
            z[b] = max(z[b], max_idx)
        z[b] = N - z[b]
    np_z = np.argmax(x, axis=1)
    assert np.allclose(z, np_z)
    return {"z": z}


def create_argmax_inputs():
    B = 32
    N = 32
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "x": np.random.uniform(-10, 10, (B, N))}
    return sizes, fixed_inputs


def test_argmax():
    B = 3 
    N = 4
    fixed_inputs = {
        "B": B, "N": N,
        "x": np.array([
            [-1, 2, -3, 4],
            [4, -3, 2, -1],
            [-3, 2, 2, -3],
        ], dtype=np.float64),
    }
    cycles = run_kernel(create_argmax_program, ref_argmax, greedy_opt, test_snitch_code, fixed_inputs)
    print("cycles:", cycles)



def create_argmin_program():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["B", "N"],
                ops=Operation("min_vals[{1}]", ["x[{1}, {0}]"], "reduce_min"),
            ),
            loop_scope(
                dims=["B", "N"],
                ops=Operation("min_mask[{1}, {0}]", ["min_vals[{1}]", "x[{1}, {0}]"], "sub"),
            ),
            loop_scope(
                dims=["B", "N"],
                ops=Operation("idx[{1}, {0}]", ["N", "{0}"], "sub"),
            ),
            loop_scope(
                dims=["B", "N"],
                ops=Operation("inv_idx[{1}, {0}]", ["idx[{1}, {0}]", "min_mask[{1}, {0}]"], "sgnj"),
            ),
            loop_scope(
                dims=["B", "N"],
                ops=Operation("min_idx[{1}]", ["inv_idx[{1}, {0}]"], "reduce_max"),
            ),
            loop_scope(
                dims=["B"],
                ops=Operation("min_idx_i[{0}]", ["min_idx[{0}]"], "i32_from_f64"),
            ),
            loop_scope(
                dims=["B"],
                ops=Operation("z[{0}]", ["N", "min_idx_i[{0}]"], "sub"),
            ),
        ],
        name="func_argmin",
        input_declarations={
            "B": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "x": Buffer(dims=["B", "N"], dtype="f64"),
            "z": Buffer(dims=["B"], dtype="i32"),
        },
        reusable_inputs={"x"},
        used_outputs={"z"},
    )
    return program


def ref_argmin(input_data):
    x = input_data["x"]
    B = input_data["B"]
    N = input_data["N"]
    z = np.zeros(shape=B, dtype=np.int32)
    min_vals = np.full(shape=B, fill_value=np.inf, dtype=np.float64)
    for b in range(B):
        for n in range(N):
            min_vals[b] = min(min_vals[b], x[b, n])
        for n in range(N):
            min_mask = float(np.sign(min_vals[b] - x[b,n]) >= 0)
            min_idx = min_mask * (N - n)
            z[b] = max(z[b], min_idx)
        z[b] = N - z[b]
    np_z = np.argmin(x, axis=1)
    assert np.allclose(z, np_z)
    return {"z": z}


def create_argmin_inputs():
    B = 32
    N = 32
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "x": np.random.uniform(-10, 10, (B, N))}
    return sizes, fixed_inputs


def test_argmin():
    B = 3 
    N = 4
    fixed_inputs = {
        "B": B, "N": N,
        "x": np.array([
            [-1, 2, -3, 4],
            [4, -3, 2, -1],
            [-3, 2, 2, -3],
        ], dtype=np.float64),
    }
    cycles = run_kernel(create_argmin_program, ref_argmin, greedy_opt, test_snitch_code, fixed_inputs)
    print("cycles:", cycles)


def create_reducemean_program():
    program = RootScope(
        ops=[
            Operation(copy.deepcopy("fN"), "N", "f64_from_i32"),
            *reciprocal_op("invN", "fN"),
            loop_scope(
                dims=["B", "N"],
                ops=Operation("sum_vals[{1}]", ["x[{1}, {0}]"], "reduce_add"),
            ),
            loop_scope(
                dims=["B"],
                ops=Operation("z[{0}]", ["sum_vals[{0}]", "invN"], "mul"),
            ),
        ],
        name="func_reducemean",
        input_declarations={
            "B": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "x": Buffer(dims=["B", "N"], dtype="f64"),
            "z": Buffer(dims=["B"], dtype="f64"),
        },
        reusable_inputs={"x"},
        used_outputs={"z"},
    )
    return program


def ref_reducemean(input_data):
    x = input_data["x"]
    B = input_data["B"]
    N = input_data["N"]
    z = np.mean(x, axis=1)
    return {"z": z}


def create_reducemean_inputs():
    B = 64
    N = 64
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "x": np.random.uniform(-10, 10, (B, N))}
    return sizes, fixed_inputs


def test_reducemean():
    B = 3 
    N = 4
    fixed_inputs = {
        "B": B, "N": N,
        "x": np.array([
            [-1, 2, -3, 4],
            [1, -3, 2, -1],
            [-3, 2, 1, -3],
        ], dtype=np.float64),
    }
    cycles = run_kernel(create_reducemean_program, ref_reducemean, greedy_opt, test_snitch_code, fixed_inputs)
    print("cycles:", cycles)



def create_cosh_program():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["B", "N"],
                ops=cosh_op("z[{1}, {0}]", "x[{1}, {0}]"),
            ),
        ],
        name="func_cosh",
        input_declarations={
            "B": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "x": Buffer(dims=["B", "N"], dtype="f64"),
            "z": Buffer(dims=["B", "N"], dtype="f64"),
        },
        reusable_inputs={},
        used_outputs={"z"},
    )
    return program


def create_cosh_inputs():
    B = 16
    N = 16
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "x": np.random.uniform(-2, 2, (B, N))}
    return sizes, fixed_inputs


def ref_cosh(input_data):
    res = approx_cosh(input_data["x"])
    return {"z": res}


def test_cosh():
    B = 4
    N = 3
    fixed_inputs = {
        "B": B,
        "N": N,
        "x": np.random.uniform(-2, 2, (B, N)),
    }
    cycles = run_kernel(create_cosh_program, ref_cosh, greedy_opt, test_snitch_code, fixed_inputs)
    print("cycles:", cycles)



def create_exp_program():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["B", "N"],
                ops=exp_op("z[{1}, {0}]", "x[{1}, {0}]"),
            ),
        ],
        name="func_exp",
        input_declarations={
            "B": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "x": Buffer(dims=["B", "N"], dtype="f64"),
            "z": Buffer(dims=["B", "N"], dtype="f64"),
        },
        reusable_inputs={},
        used_outputs={"z"},
    )
    return program


def ref_exp(input_data):
    res = approx_exp(input_data["x"])
    return {"z": res}


def create_exp_inputs():
    B = 16
    N = 16
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "x": np.random.uniform(-10, 10, (B, N))}
    return sizes, fixed_inputs


def test_exp():
    B = 8
    N = 8
    fixed_inputs = {
        "B": B,
        "N": N,
        "x": np.random.uniform(-10, 10, (B, N)),
    }
    sizes = {
            "B": B,
            "N": N,
        }

    def opt(program):
        specialize_inputs(program, sizes)
        apply_manual_exp_transformations(program)
    cycles = run_kernel(create_exp_program, ref_exp, opt, test_snitch_code, fixed_inputs)
    print("cycles:", cycles)



def create_sum_program():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["B", "N"],
                ops=Operation("dst[{1}, {0}]", ["src1[{1}, {0}]", "src2[{1}, {0}]"], "add"),
            ),
        ],
        name="func_sum",
        input_declarations={
            "B": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "src1": Buffer(dims=["B", "N"], dtype="f64"),
            "src2": Buffer(dims=["B", "N"], dtype="f64"),
            "dst": Buffer(dims=["B", "N"], dtype="f64"),
        },
        reusable_inputs={},
        used_outputs={"dst"},
    )
    return program


def ref_sum_program(input_data):
    src1 = input_data["src1"]
    src2 = input_data["src2"]
    return {"dst": src1 + src2}


def create_sum_inputs():
    B = 48
    N = 48
    sizes = {
        "B": B,
        "N": N,
    }
    fixed_inputs = {
        "B": B,
        "N": N,
        "src1": np.random.uniform(-10, 10, (B, N)),
        "src2": np.random.uniform(-10, 10, (B, N)),
    }
    return sizes, fixed_inputs


def test_sum():
    B = 48
    N = 48
    fixed_inputs = {
        "B": B,
        "N": N,
        "src1": np.random.uniform(-10, 10, (B, N)),
        "src2": np.random.uniform(-10, 10, (B, N)),
    }
    sizes = {
            "B": B,
            "N": N,
    }
    def opt(program):
        specialize_inputs(program, sizes)
        apply_manual_elem_by_elem_bop_transformations(program)
    cycles = run_kernel(create_sum_program, ref_sum_program, opt, test_snitch_code_rtl, fixed_inputs, "func_sum.c")
    write_results("sum", f"{B}x{N}", cycles)
    print("cycles:", cycles)



def create_sub_program():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["B", "N"],
                ops=Operation("dst[{1}, {0}]", ["src1[{1}, {0}]", "src2[{1}, {0}]"], "sub"),
            ),
        ],
        name="func_sub",
        input_declarations={
            "B": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "src1": Buffer(dims=["B", "N"], dtype="f64"),
            "src2": Buffer(dims=["B", "N"], dtype="f64"),
            "dst": Buffer(dims=["B", "N"], dtype="f64"),
        },
        reusable_inputs={},
        used_outputs={"dst"},
    )
    return program


def ref_sub_program(input_data):
    src1 = input_data["src1"]
    src2 = input_data["src2"]
    return {"dst": src1 - src2}


def create_sub_inputs():
    B = 64
    N = 32
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "src1": np.random.uniform(-10, 10, (B, N)), "src2": np.random.uniform(-10, 10, (B, N))}
    return sizes, fixed_inputs


def test_sub():
    B = 16
    N = 16
    fixed_inputs = {
        "B": B,
        "N": N,
        "src1": np.random.uniform(-10, 10, (B, N)),
        "src2": np.random.uniform(-10, 10, (B, N)),
    }
    sizes = {
            "B": B,
            "N": N,
    }
    def opt(program):
        #specialize_inputs(program, sizes)
        apply_manual_elem_by_elem_bop_transformations(program)
    cycles = run_kernel(create_sub_program, ref_sub_program, opt, test_snitch_code)
    print("cycles:", cycles)



def create_mul_program():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["B", "N"],
                ops=Operation("dst[{1}, {0}]", ["src1[{1}, {0}]", "src2[{1}, {0}]"], "mul"),
            ),
        ],
        name="func_mul",
        input_declarations={
            "B": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "src1": Buffer(dims=["B", "N"], dtype="f64"),
            "src2": Buffer(dims=["B", "N"], dtype="f64"),
            "dst": Buffer(dims=["B", "N"], dtype="f64"),
        },
        reusable_inputs={},
        used_outputs={"dst"},
    )
    return program


def ref_mul_program(input_data):
    src1 = input_data["src1"]
    src2 = input_data["src2"]
    return {"dst": src1 * src2}


def create_mul_inputs():
    B = 64
    N = 32
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "src1": np.random.uniform(-10, 10, (B, N)), "src2": np.random.uniform(-10, 10, (B, N))}
    return sizes, fixed_inputs


def test_mul():
    #greedy 3405
    #none 8446
    #manual 1221
    B = 16
    N = 16
    fixed_inputs = {
        "B": B,
        "N": N,
        "src1": np.random.uniform(-10, 10, (B, N)),
        "src2": np.random.uniform(-10, 10, (B, N)),
    }
    sizes = {
            "B": B,
            "N": N,
        }

    def opt(program):
        #specialize_inputs(program, sizes)
        apply_manual_elem_by_elem_bop_transformations(program)


    cycles = run_kernel(create_mul_program, ref_mul_program, opt, test_snitch_code, fixed_inputs)
    print("cycles:", cycles)



def create_div_program():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["B", "N"],
                ops=div_op("dst[{1}, {0}]", "src1[{1}, {0}]", "src2[{1}, {0}]"),
            ),
        ],
        name="func_div",
        input_declarations={
            "B": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "src1": Buffer(dims=["B", "N"], dtype="f64"),
            "src2": Buffer(dims=["B", "N"], dtype="f64"),
            "dst": Buffer(dims=["B", "N"], dtype="f64"),
        },
        reusable_inputs={},
        used_outputs={"dst"},
    )
    return program


def ref_div_program(input_data):
    src1 = input_data["src1"]
    src2 = input_data["src2"]
    return {"dst": np.vectorize(approx_div)(src1, src2)}


def create_div_inputs():
    B = 16
    N = 16
    sizes = {"B": B, "N": N}
    fixed_inputs = {
        **sizes,
        "src1": np.random.uniform(-10, 10, (B, N)),
        "src2": np.random.uniform(0.5, 2.0, (B, N)) * np.random.choice([1, -1], (B, N))
    }
    
    return sizes, fixed_inputs


def test_div():
    B = 16
    N = 16
    fixed_inputs = {
        "B": B,
        "N": N,
        "src1": np.random.uniform(-10, 10, (B, N)),
        "src2": np.random.uniform(-10, 10, (B, N)),
    }
    sizes = {
            "B": B,
            "N": N,
        }

    def opt(program):
        specialize_inputs(program, sizes)
        apply_manual_div_transformations(program)


    cycles = run_kernel(create_div_program, ref_div_program, opt, test_snitch_code)
    print("cycles:", cycles)



def create_log_program():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["B", "N"],
                ops=ln_op("z[{1}, {0}]", "x[{1}, {0}]"),
            ),
        ],
        name="func_log",
        input_declarations={
            "B": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "x": Buffer(dims=["B", "N"], dtype="f64"),
            "z": Buffer(dims=["B", "N"], dtype="f64"),
        },
        reusable_inputs={},
        used_outputs={"z"},
    )
    return program


def ref_log(input_data):
    res = approx_ln(input_data["x"])
    return {"z": res}


def create_log_inputs():
    B = 16
    N = 8
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "x": np.random.uniform(0.001, 100, (B, N))}
    return sizes, fixed_inputs


def test_log():
    B = 8
    N = 8
    fixed_inputs = {
        "B": B,
        "N": N,
        "x": np.random.uniform(0.001, 100, (B, N)),
    }
    sizes = {
            "B": B,
            "N": N,
        }

    def opt(program):
        specialize_inputs(program, sizes)
        apply_manual_log_transformations(program)
    cycles = run_kernel(create_log_program, ref_log, opt, test_snitch_code, fixed_inputs)
    
    print("cycles:", cycles)



def create_tanh_program():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["B", "N"],
                ops=tanh_op("z[{1}, {0}]", "x[{1}, {0}]"),
            ),
        ],
        name="func_tanh",
        input_declarations={
            "B": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "x": Buffer(dims=["B", "N"], dtype="f64"),
            "z": Buffer(dims=["B", "N"], dtype="f64"),
        },
        reusable_inputs={},
        used_outputs={"z"},
    )
    return program


def ref_tanh(input_data):
    res = approx_tanh(input_data["x"])
    assert np.allclose(np.tanh(input_data["x"]), res, rtol=1e-2, atol=1e-5)
    return {"z": res}


def create_tanh_inputs():
    B = 16
    N = 16
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "x": np.random.uniform(-1, 1, (B, N))}
    return sizes, fixed_inputs


def test_tanh():
    B = 4
    N = 3
    fixed_inputs = {
        "B": B,
        "N": N,
        "x": np.random.uniform(-1, 1, (B, N)),
    }
    cycles = run_kernel(create_tanh_program, ref_tanh, greedy_opt, test_snitch_code, fixed_inputs)
    print("cycles:", cycles)



def create_sqrt_program():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["B", "N"],
                ops=sqrt_op("dst[{1}, {0}]", "src[{1}, {0}]"),
            ),
        ],
        name="func_sqrt",
        input_declarations={
            "B": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "src": Buffer(dims=["B", "N"], dtype="f64"),
            "dst": Buffer(dims=["B", "N"], dtype="f64"),
        },
        reusable_inputs={},
        used_outputs={"dst"},
    )
    return program


def ref_sqrt_program(input_data):
    src = input_data["src"]
    return {"dst": np.vectorize(approx_sqrt)(src)}


def create_sqrt_inputs():
    B = 16
    N = 16
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "src": np.random.uniform(0, 10, (B, N))}
    return sizes, fixed_inputs


def test_sqrt():
    B = 8
    N = 8
    fixed_inputs = {
        "B": B,
        "N": N,
        "src": np.random.uniform(0, 10, (B, N)),
    }
    sizes = {
            "B": B,
            "N": N,
        }

    def opt(program):
        specialize_inputs(program, sizes)
        apply_manual_sqrt_transformations(program)

    cycles = run_kernel(create_sqrt_program, ref_sqrt_program, opt, test_snitch_code_rtl, fixed_inputs)
    print("cycles:", cycles)



def create_pow_program():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["B", "N"],
                ops=pow_op("z[{1}, {0}]", "x1[{1}, {0}]", "x2[{1}, {0}]"),
            ),
        ],
        name="func_pow",
        input_declarations={
            "B": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "x1": Buffer(dims=["B", "N"], dtype="f64"),
            "x2": Buffer(dims=["B", "N"], dtype="f64"),
            "z": Buffer(dims=["B", "N"], dtype="f64"),
        },
        reusable_inputs={},
        used_outputs={"z"},
    )
    return program


def ref_pow(input_data):
    res = approx_pow(input_data["x1"], input_data["x2"])
    assert np.allclose(pow(input_data["x1"], input_data["x2"]), res, rtol=1e-2, atol=1e-5)
    return {"z": res}


def create_pow_inputs():
    B = 8
    N = 8
    sizes = {"B": B, "N": N}
    fixed_inputs = {
        **sizes,
        "x1": np.random.uniform(0.1, 10, (B, N)),
        "x2": np.random.uniform(-4, 4, (B, N)),
    }
    return sizes, fixed_inputs



def test_pow():
    B = 4
    N = 3
    fixed_inputs = {
        "B": B,
        "N": N,
        "x1": np.random.uniform(0.1, 10, (B, N)),
        "x2": np.random.uniform(-10, 10, (B, N)),
    }
    cycles = run_kernel(create_pow_program, ref_pow, greedy_opt, test_snitch_code, fixed_inputs)
    print("cycles:", cycles)



def create_relu_program():
    program = RootScope(
        # max(0,x)
        ops=[
            loop_scope(
                dims=["B", "N"],
                ops=[
                    Operation("dst[{1}, {0}]", ["x[{1}, {0}]", 0.0], "max"),
                ],
            ),
        ],
        name="func_relu",
        input_declarations={
            "B": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "x": Buffer(dims=["B", "N"], dtype="f64"),
        },
        reusable_inputs={},
        used_outputs={"dst"},
    )
    return program


def ref_relu(input_data):
    x = input_data["x"]
    res = torch.nn.functional.relu(torch.from_numpy(x), inplace=False).numpy()
    return {"dst": res}


def create_relu_inputs():
    B = 64
    N = 64
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "x": np.random.uniform(-10, 10, (B, N))}
    return sizes, fixed_inputs


def test_relu():
    B = 16
    N = 16
    fixed_inputs = {
        "B": B,
        "N": N,
        "x": np.random.uniform(-10, 10, (B, N)),
    }
    sizes = {
            "B": B,
            "N": N,
        }

    def opt(program):
        specialize_inputs(program, sizes)
        apply_manual_elem_by_elem_uop_transformations(program)

    cycles = run_kernel(create_relu_program, ref_relu, opt, test_snitch_code, fixed_inputs)
    print("cycles:", cycles)



def create_celu_program():
    program = RootScope(
        # max(0,x) + min(0,alpha*(exp(x/alpha)-1))
        ops=[
            *reciprocal_op("invA", "alpha"),
            loop_scope(
                dims=["B", "N"],
                ops=[
                    Operation("x_a[{1}, {0}]", ["x[{1}, {0}]", "invA"], "mul"),
                    *exp_op("exp_x_a[{1}, {0}]", "x_a[{1}, {0}]"),
                    Operation("e_m1[{1}, {0}]", ["exp_x_a[{1}, {0}]", 1.0], "sub"),
                    Operation("x1[{1}, {0}]", ["alpha", "e_m1[{1}, {0}]"], "mul"),
                    Operation("t1[{1}, {0}]", ["x[{1}, {0}]", 0.0], "max"),
                    Operation("t2[{1}, {0}]", ["x1[{1}, {0}]", 0.0], "min"),
                    Operation("z[{1}, {0}]", ["t1[{1}, {0}]", "t2[{1}, {0}]"], "add"),
                ],
            ),
        ],
        name="func_celu",
        input_declarations={
            "B": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "alpha": Buffer(dims=[], dtype="f64"),
            "x": Buffer(dims=["B", "N"], dtype="f64"),
        },
        reusable_inputs={},
        used_outputs={"z"},
    )
    return program


def ref_celu(input_data):
    x = input_data["x"]
    alpha = input_data["alpha"]
    res = torch.nn.functional.celu(torch.from_numpy(x), alpha=torch.from_numpy(alpha), inplace=False).numpy()
    return {"z": res}


def create_celu_inputs():
    B = 16
    N = 16
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "x": np.random.uniform(-10, 10, (B, N)), "alpha": 0.4}
    return sizes, fixed_inputs


def test_celu():
    B = 3
    N = 4
    fixed_inputs = {
        "B": B, "N": N,
        "x": np.random.uniform(-10, 10, (B, N)),
        "alpha": 0.4,
    }
    cycles = run_kernel(create_celu_program, ref_celu, greedy_opt, test_snitch_code, fixed_inputs)
    print("cycles:", cycles)



def create_leakyrelu_program():
    program = RootScope(
        # f(x) = alpha * x for x < 0, f(x) = x for x >= 0
        # https://pytorch.org/docs/stable/generated/torch.nn.LeakyReLU.html
        # f(x) = max(0,x) + alpha ∗ min(0,x)
        ops=[
            loop_scope(
                dims=["B", "N"],
                ops=[
                    Operation("max[{1}, {0}]", [0.0, "x[{1}, {0}]"], "max"),
                    Operation("min[{1}, {0}]", [0.0, "x[{1}, {0}]"], "min"),
                    Operation("dst[{1}, {0}]", ["alpha", "min[{1}, {0}]", "max[{1}, {0}]"], "fmadd"),
                ],
            ),
        ],
        name="func_leakyrelu",
        input_declarations={
            "B": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "alpha": Buffer(dims=[], dtype="f64"),
            "x": Buffer(dims=["B", "N"], dtype="f64"),
        },
        reusable_inputs={},
        used_outputs={"dst"},
    )
    return program


def ref_leakyrelu(input_data):
    x = input_data["x"]
    alpha = input_data["alpha"]
    res = torch.nn.functional.leaky_relu(torch.from_numpy(x), inplace=False, negative_slope=torch.from_numpy(alpha)).numpy()
    return {"dst": res}


def create_leakyrelu_inputs():
    B = 16
    N = 16
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "x": np.random.uniform(-10, 10, (B, N))}
    return sizes, fixed_inputs


def test_leakyrelu():
    B = 32
    N = 32
    fixed_inputs = {
        "B": B,
        "N": N,
        "dst": np.random.uniform(-10, 10, (B, N)),
    }
    sizes = {
            "B": B,
            "N": N,
        }

    def opt(program):
        specialize_inputs(program, sizes)
        apply_manual_leakyrelu_transformations(program)
    cycles = run_kernel(create_leakyrelu_program, ref_leakyrelu, opt, test_snitch_code_rtl, fixed_inputs)#, "func_leakyrelu.c")
    #write_results("leakyrelu", f"{B}x{N}", cycles)
    print("cycles:", cycles)



def create_sigmoid_program():
    program = RootScope(
        # y = 1 / (1 + exp(-x))
        ops=[
            loop_scope(
                dims=["B", "N"],
                ops=[
                    Operation("x_n[{1}, {0}]", ["x[{1}, {0}]", -1.0], "sgnjx"),
                    *exp_op("exp_x[{1}, {0}]", "x_n[{1}, {0}]"),
                    Operation("d[{1}, {0}]", ["exp_x[{1}, {0}]", 1.0], "add"),
                    *reciprocal_op("z[{1}, {0}]", "d[{1}, {0}]"),
                ],
            ),
        ],
        name="func_sigmoid",
        input_declarations={
            "B": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "x": Buffer(dims=["B", "N"], dtype="f64"),
        },
        reusable_inputs={},
        used_outputs={"z"},
    )
    return program


def ref_sigmoid(input_data):
    x = input_data["x"]
    res = torch.nn.functional.sigmoid(torch.from_numpy(x)).numpy()
    return {"z": res}


def create_sigmoid_inputs():
    B = 16
    N = 16
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "x": np.random.uniform(-10, 10, (B, N))}
    return sizes, fixed_inputs


def test_sigmoid():
    B = 3
    N = 4
    fixed_inputs = {
        "B": B, "N": N,
        "x": np.random.uniform(-10, 10, (B, N)),
    }
    cycles = run_kernel(create_sigmoid_program, ref_sigmoid, greedy_opt, test_snitch_code, fixed_inputs)
    print("cycles:", cycles)


def ref_llama3_feedforward(input_data):
    x = input_data["x"]
    w1 = input_data["w1"]
    w2 = input_data["w2"]
    w3 = input_data["w3"]

    a = np.einsum("mk,nk->mn", x, w1)
    b = np.einsum("mk,nk->mn", x, w3)
    e = a / (1 + np.exp(-a))  # silu
    c = e * b
    y = np.einsum("mn,kn->mk", c, w2)

    return {"y": y}

def precompute_freqs_cis(dim: int, end: int, theta: float = 10000.0):
    freqs = 1.0 / (theta ** (torch.arange(0, dim, 2)[: (dim // 2)].float() / dim))
    t = torch.arange(end, device=freqs.device, dtype=torch.float32)
    freqs = torch.outer(t, freqs)
    freqs_cis = torch.polar(torch.ones_like(freqs), freqs)  # complex64
    return freqs_cis


def reshape_for_broadcast(freqs_cis: torch.Tensor, x: torch.Tensor):
    ndim = x.ndim
    assert 0 <= 1 < ndim
    assert freqs_cis.shape == (x.shape[1], x.shape[-1])
    shape = [d if i == 1 or i == ndim - 1 else 1 for i, d in enumerate(x.shape)]
    return freqs_cis.view(*shape)


def apply_rotary_emb(
    xq: torch.Tensor,
    xk: torch.Tensor,
    freqs_cis: torch.Tensor,
) -> Tuple[torch.Tensor, torch.Tensor]:
    xq_ = torch.view_as_complex(xq.float().reshape(*xq.shape[:-1], -1, 2))
    xk_ = torch.view_as_complex(xk.float().reshape(*xk.shape[:-1], -1, 2))
    freqs_cis = reshape_for_broadcast(freqs_cis, xq_)
    xq_out = torch.view_as_real(xq_ * freqs_cis).flatten(3)
    xk_out = torch.view_as_real(xk_ * freqs_cis).flatten(3)
    return xq_out.type_as(xq), xk_out.type_as(xk)


def repeat_kv(x: torch.Tensor, n_rep: int) -> torch.Tensor:
    """torch.repeat_interleave(x, dim=2, repeats=n_rep)"""
    bs, slen, n_kv_heads, head_dim = x.shape
    if n_rep == 1:
        return x
    return (
        x[:, :, :, None, :]
        .expand(bs, slen, n_kv_heads, n_rep, head_dim)
        .reshape(bs, slen, n_kv_heads * n_rep, head_dim)
    )



def linear_layer(w,x):
    return np.einsum("mk,nk->mn", x, w)
def ref_llama3_attention(input_data):
    x = input_data["x"]
    wq = input_data["wq"]
    wk = input_data["wk"]
    wv = input_data["wv"]
    wo = input_data["wo"]
    n_local_heads = input_data["n_local_heads"]
    n_local_kv_heads = input_data["n_local_kv_heads"]
    head_dim = input_data["head_dim"]
    freqs_cis = input_data["freqs_cis"]
    n_rep = input_data["n_rep"]

    bsz, seqlen, _ = x.shape
    xq, xk, xv = linear_layer(wq, x), linear_layer(wk,x), linear_layer(wv, x)
    xq = xq.reshape(bsz, seqlen, n_local_heads, head_dim)
    xk = xk.reshape(bsz, seqlen, n_local_kv_heads, head_dim)
    xv = xv.reshape(bsz, seqlen, n_local_kv_heads, head_dim)

    xq, xk = apply_rotary_emb(xq, xk, freqs_cis=freqs_cis)

    self.cache_k = self.cache_k.to(xq)
    self.cache_v = self.cache_v.to(xq)

    self.cache_k[:bsz, start_pos: start_pos + seqlen] = xk
    self.cache_v[:bsz, start_pos: start_pos + seqlen] = xv

    keys = self.cache_k[:bsz, : start_pos + seqlen]
    values = self.cache_v[:bsz, : start_pos + seqlen]

    # repeat k/v heads if n_kv_heads < n_heads
    keys = repeat_kv(
        keys, n_rep
    )  # (bs, cache_len + seqlen, n_local_heads, head_dim)
    values = repeat_kv(
        values, n_rep
    )  # (bs, cache_len + seqlen, n_local_heads, head_dim)

    xq = xq.transpose(1, 2)  # (bs, n_local_heads, seqlen, head_dim)
    keys = keys.transpose(1, 2)  # (bs, n_local_heads, cache_len + seqlen, head_dim)
    values = values.transpose(
        1, 2
    )  # (bs, n_local_heads, cache_len + seqlen, head_dim)
    scores = torch.matmul(xq, keys.transpose(2, 3)) / math.sqrt(head_dim)
    scores = F.softmax(scores.float(), dim=-1).type_as(xq)
    output = torch.matmul(scores, values)  # (bs, n_local_heads, seqlen, head_dim)
    output = output.transpose(1, 2).contiguous().view(bsz, seqlen, -1)
    return linear_layer(wo, output)

def create_llama3_feedforward_program():
    reduce_fma = OpDesc("f32", "{0} * {1}", reduce="{y} += {x}")
    program = RootScope(
        ops=[
            loop_scope(dims=["M", "K", "N"], ops=Operation("a[{2}, {0}]", ["x[{2}, {1}]", "w1[{0}, {1}]"], copy.deepcopy(reduce_fma))),
            loop_scope(dims=["M", "K", "N"], ops=Operation("b[{2}, {0}]", ["x[{2}, {1}]", "w3[{0}, {1}]"], copy.deepcopy(reduce_fma))),
            loop_scope(dims=["M", "N"], ops=Operation("na[{1}, {0}]", ["a[{1}, {0}]"], OpDesc("f32", "- {0}"))),
            loop_scope(dims=["M", "N"], ops=Operation("ea[{1}, {0}]", ["na[{1}, {0}]"], OpDesc("f32", "expf({0})"))),
            loop_scope(dims=["M", "N"], ops=Operation("a1[{1}, {0}]", ["ea[{1}, {0}]", 1.0], OpDesc("f32", "{0} + {1}"))),
            loop_scope(dims=["M", "N"], ops=Operation("e[{1}, {0}]", ["a[{1}, {0}]", "a1[{1}, {0}]"], OpDesc("f32", "{0} / {1}"))),
            loop_scope(dims=["M", "N"], ops=Operation("c[{1}, {0}]", ["e[{1}, {0}]", "b[{1}, {0}]"], OpDesc("f32", "{0} * {1}"))),
            loop_scope(dims=["M", "N", "K"], ops=Operation("y[{2}, {0}]", ["c[{2}, {1}]", "w2[{0}, {1}]"], copy.deepcopy(reduce_fma))),
        ],
        name="func_llama3_feedforward",
        input_declarations={
            "M": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "K": Buffer(dims=[], dtype="i32"),
            "x": Buffer(dims=["M", "K"], dtype="f32"),
            "w1": Buffer(dims=["N", "K"], dtype="f32"),
            "w2": Buffer(dims=["K", "N"], dtype="f32"),
            "w3": Buffer(dims=["N", "K"], dtype="f32"),
            "y": Buffer(dims=["M", "K"], dtype="f32"),
        },
        reusable_inputs={},
        used_outputs={"y"},
    )
    return program


def create_llama3_feedforward_inputs(size="test"):
    if size == "test":
        M = 4
        N = 14
        K = 10
    else:
        M = 4
        N = 14336
        K = 4096
    sizes = {"M": M, "N": N, "K": K}
    fixed_inputs = {
        **sizes,
        "x": np.random.uniform(-0.2, 0.2, (M, K)),
        "w1": np.random.uniform(-0.2, 0.2, (N, K)),
        "w2": np.random.uniform(-0.2, 0.2, (K, N)),
        "w3": np.random.uniform(-0.2, 0.2, (N, K)),
    }
    return sizes, fixed_inputs


def ref_llama3_matmul(input_data):
    x = input_data["x"]
    w = input_data["w"]
    y = np.einsum("mk,nk->mn", x, w)
    return {"y": y}


def create_llama3_matmul_program():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["M", "K", "N"],
                ops=Operation(
                    "y[{2}, {0}]",
                    ["x[{2}, {1}]", "w[{0}, {1}]"],
                    OpDesc("f32", "{0} * {1}", reduce="{y} += {x}")
                )
            ),
        ],
        name="func_llama3_matmul",
        input_declarations={
            "M": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "K": Buffer(dims=[], dtype="i32"),
            "x": Buffer(dims=["M", "K"], dtype="f32"),
            "w": Buffer(dims=["N", "K"], dtype="f32"),
        },
        reusable_inputs={},
        used_outputs={"y"},
    )
    return program


def create_llama3_matmul_inputs(sizes={"M": 4, "N": 14, "K": 10}):
    fixed_inputs = {
        **sizes,
        "x": np.random.uniform(-0.2, 0.2, (sizes['M'], sizes['K'])),
        "w": np.random.uniform(-0.2, 0.2, (sizes['N'], sizes['K'])),
    }
    return sizes, fixed_inputs


TEST_KERNEL_DICT = {
    "llama3_feedforward": (create_llama3_feedforward_program, create_llama3_feedforward_inputs, ref_llama3_feedforward),
    "test_create_dimension": (create_test_create_dimension_program, create_test_create_dimension_inputs, ref_test_create_dimension),
}


ONNX_KERNEL_DICT = {
    "add": (create_add_program, create_add_inputs, ref_add_program),
    "affine_grid_2d": (create_affine_grid_2d_program, create_affine_grid_2d_inputs, ref_affine_grid_2d),
    "and": (create_and_program, create_and_inputs, ref_and),
    "argmax": (create_argmax_program, create_argmax_inputs, ref_argmax),
    "argmin": (create_argmin_program, create_argmin_inputs, ref_argmin),
    "asin": (create_asin_program, create_asin_inputs, ref_asin_program),
    "asinh": (create_asinh_program, create_asinh_inputs, ref_asinh_program),
    "atan": (create_atan_program, create_atan_inputs, ref_atan_program),
    "atanh": (create_atanh_program, create_atanh_inputs, ref_atanh_program),
    "averagepool2d_nopad": (create_averagepool2d_nopad_program, create_averagepool2d_nopad_inputs, ref_averagepool2d_nopad),
    "batchnorm": (create_batchnorm_program, create_batchnorm_inputs, ref_batchnorm),
    "bitshift_left": (create_bitshift_left_program, create_bitshift_left_inputs, ref_bitshift_left),
    "bitwise_and": (create_bitwise_and_program, create_bitwise_and_inputs, ref_bitwise_and),
    "bitwise_not": (create_bitwise_not_program, create_bitwise_not_inputs, ref_bitwise_not),
    "bitwise_or": (create_bitwise_or_program, create_bitwise_or_inputs, ref_bitwise_or),
    "bitwise_xor": (create_bitwise_xor_program, create_bitwise_xor_inputs, ref_bitwise_xor),
    "blackman_window": (create_blackman_window_program, create_blackman_window_inputs, ref_blackman_window),
    "ceil": (create_ceil_program, create_ceil_inputs, ref_ceil),
    "celu": (create_celu_program, create_celu_inputs, ref_celu),
    "clip": (create_clip_program, create_clip_inputs, ref_clip),
    "conv2d": (create_conv2d_nopad_program, create_conv2d_nopad_inputs, ref_conv2d_nopad),
    "cos": (create_cos_program, create_cos_inputs, ref_cos),
    "cosh": (create_cosh_program, create_cosh_inputs, ref_cosh),
    "div": (create_div_program, create_div_inputs, ref_div_program),
    "exp": (create_exp_program, create_exp_inputs, ref_exp),
    "floor": (create_floor_program, create_floor_inputs, ref_floor),
    "gemm": (create_gemm_program, create_gemm_inputs, ref_gemm),
    "layernorm": (create_layernorm_program, create_layernorm_inputs, ref_layernorm),
    "leakyrelu": (create_leakyrelu_program, create_leakyrelu_inputs, ref_leakyrelu),
    "log_softmax": (create_log_softmax_program, create_log_softmax_inputs, ref_log_softmax),
    "log": (create_log_program, create_log_inputs, ref_log),
    "matmul": (create_matmul_program, create_matmul_inputs, ref_matmul),
    "mish": (create_mish_program, create_mish_inputs, ref_mish),
    "mul": (create_mul_program, create_mul_inputs, ref_mul_program),
    "pow": (create_pow_program, create_pow_inputs, ref_pow),
    "reducemean": (create_reducemean_program, create_reducemean_inputs, ref_reducemean),
    "relu": (create_relu_program, create_relu_inputs, ref_relu),
    "softmax": (create_softmax_program, create_softmax_inputs, ref_softmax),
    "softplus": (create_softplus_program, create_softplus_inputs, ref_softplus),
    "sqrt": (create_sqrt_program, create_sqrt_inputs, ref_sqrt_program),
    "sub": (create_sub_program, create_sub_inputs, ref_sub_program),
    "sum": (create_sum_program, create_sum_inputs, ref_sum_program),
    "tanh": (create_tanh_program, create_tanh_inputs, ref_tanh),
}

# last number is the coverage of the group from 0.0 to 1.0
# (approximate fraction of taken transforms among the available)
RANDOM_GROUPS = [
    (tile_scope, find_tileable_scopes, 0.1),
    (tile_buffer, find_tileable_buffers, 0.1),
    (create_dimension, find_creatable_dimensions, 0.1),
    (delete_dimension, find_deletable_dimensions, 0.1),
    (swap_nested_scopes, find_swappable_nested_scopes, 0.3),
    (create_temporary, find_creatable_temporaries, 0.1),
    (swap_ops, find_swappable_ops, 0.3),
    (swap_buffer_dims, find_swappable_buffer_dims, 0.3),
    (untile_buffer, find_untileable_buffers, 0.1),
    (delete_temporary, find_deletable_temporaries, 0.9),
    (untile_scope, find_untileable_scopes, 0.9),
    (join_scopes, find_joinable_scopes, 0.9),
    (split_scopes, find_splittable_scopes, 0.1),
    (reuse_arr_dims, find_reusable_arr_dims, 0.9),
    (unroll_scope, find_unrollable_scopes, 0.2),
    (reuse_buffers, find_reusable_buffers, 0.9),
    (move_buf_to_stack, find_bufs_movable_to_stack, 0.9),
    (parallelize_scope, find_parallelizable_scopes, 0.9),
]



if __name__ == '__main__':
    test_abs()
    test_acos()
    test_acosh()
    test_add()
    test_affine_grid_2d()
    test_and()
    test_argmax()
    test_argmin()
    test_asin()
    test_asinh()
    test_atan()
    test_atanh()
    test_averagepool2d_nopad()
    test_averagepool2d_ref()
    test_batchnorm()
    test_bitshift_left()
    test_bitwise_and()
    test_bitwise_not()
    test_bitwise_or()
    test_bitwise_xor()
    test_blackman_window()
    test_ceil()
    test_celu()
    test_clip()
    test_conv2d()
    test_cos()
    test_cosh()
    test_div()
    test_exp()
    test_floor()
    test_gemm()
    test_layernorm()
    test_leakyrelu()
    test_log_softmax()
    test_log()
    test_matmul()
    test_mish()
    test_mul()
    test_pow()
    test_reducemean()
    test_relu()
    test_softmax()
    test_softplus()
    test_sqrt()
    test_sub()
    test_sum()
    test_tanh()

