from kernels import *
import timeit
import torch
import numpy as np
import re


def run_feedforward():
    for seed in range(0, 1000):
    # for seed in range(1):
        print("Running with seed", seed)
        kernel = "llama3_feedforward"
        create_program, create_inputs, reference = ONNX_KERNEL_DICT[kernel]
        random.seed(seed)
        np.random.seed(seed)
        sizes, fixed_inputs = create_inputs("big")
        def opt(program):
            specialize_inputs(program, sizes)
            random_opt(program, seed=seed, silent=False)
            # random_opt(program, seed=seed, silent=True)
        warmup_reps = 0
        min_reps = warmup_reps + 1
        def runner(p, d):
            return test_native_code(p, d, check_correctness=False, min_reps=min_reps)
        time, _, instr = run_kernel(create_program, reference, opt, runner, fixed_inputs)
        no_warmup = time[warmup_reps:]


        print(f"runtime: {np.mean(no_warmup):.2f}s, instructions: {instr}")


def create_tc_matmul_program():
    text = """
        Name: matmul
        In: a, b
        Out: c
        Declarations:
            a f16 [4096, 4096] heap
            b f16 [4096, 4096] heap
            c f32 [4096, 4096] heap
            as f16 [32:N, 32:N,128, 64:N,64] cache
            bs f16 [32:N, 32:N,128, 64:N,64] cache
            cs f32 [32:N,128, 32:N,128] cache
            ar f16 [32:N,2:N, 32:N,4:N,2,16, 64:N,4:N,16] fragment_a_16_16_16_row
            br f16 [32:N,4:N, 32:N,2:N,4,16, 64:N,4:N,16] fragment_b_16_16_16_col
            cr f32 [32:N,4:N,2, 32:N,2:N,4, 16,16] fragment_c_16_16_16_row
        Code:
        #  0    1    2    3    4    5    6    7    8    9   10   11
        #  M    N    K    M    M    K    M    K
        32:g 32:g   64  8:b  4:w  8:w    4    8                      as[{1}, {0},{6}*32+{3}*4+{4}, {2},{5}*8+{7}] = a[{0}*128+{6}*32+{3}*4+{4}, {2}*64+{5}*8+{7}]
        #  M    N    K    N    N    K    N    K
           |    |    |    |    |    |    4    8                      bs[{0}, {1},{6}*32+{3}*4+{4}, {2},{5}*8+{7}] = b[{1}*128+{6}*32+{3}*4+{4}, {2}*64+{5}*8+{7}]
        #  M    N    K    M    N    K    M    M    K
           |    |    |  4:b  2:b    4    2 16:f 16:f                 ar[{1},{4}, {0},{3},{6},{7}, {2},{5},{8}] = as[{1}, {0},{3}*32+{6}*16+{7}, {2},{5}*16+{8}]
        #  M    N    K    K    M    N    N    N    K
           |    |    |    |    |    |    4 16:f 16:f                 br[{0},{3}, {1},{4},{6},{7}, {2},{5},{8}] = bs[{0}, {1},{4}*64+{6}*16+{7}, {2},{5}*16+{8}]
        #  M    N    K    K    M    N    N    M    M    N    K
           |    |    |    |    |    |    |    2 16:f 16:f 16:f       cr[{0},{3},{7}, {1},{4},{6}, {8},{9}] += (float) ar[{1},{4}, {0},{3},{7},{8}, {2},{5},{10}] * (float) br[{0},{3}, {1},{4},{6},{9}, {2},{5},{10}]
        #  M    N    M    N    M    N    M    N
           |    |  4:b  2:b    2    4 16:f 16:f                      cs[{0},{2}*32+{4}*16+{6}, {1},{3}*64+{5}*16+{7}] = cr[{0},{2},{4}, {1},{3},{5}, {6},{7}]
        #  M    N    M    N    M    N
           |    |  8:b 32:w   16    4                                c[{0}*128+{2}*16+{4}, {1}*128+{3}*4+{5}] = cs[{0},{2}*16+{4}, {1},{3}*4+{5}]
    """
    # text = """
    #     Name: matmul
    #     In: a, b
    #     Out: c
    #     Declarations:
    #         a f16 [256, 4096]
    #         b f16 [256, 4096]
    #         c f32 [256, 256]
    #     Code:
    #     256 256 4096 c[{0}, {1}] += (float) a[{0}, {2}] * (float) b[{1}, {2}]
    # """
    program = parse_program(text)
    print(program.text())
    return program


def create_tc_matmul_inputs():
    # M, N, K = 4096, 4096, 4096
    M, N, K = 256, 256, 4096
    fixed_inputs = {M: M, N: N, K: K}
    a = np.random.randn(M, K).astype(np.float16)
    b = np.random.randn(N, K).astype(np.float16)
    return {"a": a, "b": b, **fixed_inputs}, fixed_inputs


def ref_tc_matmul(input_data):
    a = input_data["a"]
    b = input_data["b"]
    c = np.matmul(a.astype(np.float32), b.T.astype(np.float32))
    return {"c": c}


def run_matmul():
    sizes, fixed_inputs = create_tc_matmul_inputs()
    warmup_reps = 3
    min_reps = warmup_reps + 10
    def runner(p, d, timeout=10):
        return test_cuda_code(p, d, check_correctness=True, min_reps=min_reps, min_seconds=1)
        # return test_native_code(p, d, check_correctness=True, min_reps=min_reps, min_seconds=1, remove_tmp_files=False, silent=False)
    run_result = run_kernel(create_tc_matmul_program, ref_tc_matmul, lambda *args: [], runner, fixed_inputs)
    if run_result["error"]:
        print("Error:", run_result["error"])
    else:
        no_warmup = run_result['measured'][warmup_reps:]
        time_ms = np.mean(no_warmup) * 1e3
        print(f"runtime [ms]: {time_ms:.3f}, instructions: {run_result['peak']}")


def repeat(code, repeats, warmup, locals):
    return timeit.repeat(code, repeat=repeats + warmup, number=1, globals={**locals, **globals()})[warmup:]


def reference_llama3_matmul_performance():   
    repeats = 3
    repeats_inner = 100
    warmup = 10
    
    # M, N, K
    linear_layer_sizes = [
        (4096, 4096, 4096),  # FLOP reference that expected to be well optimized
        (32, 14336, 4096),
        (32, 4096, 14336),
        (32, 4096, 4096),
        (32, 1024, 4096),
        (32, 128256, 4096),
        (4, 14336, 4096),
        (4, 4096, 14336),
        (4, 4096, 4096),
        (4, 1024, 4096),
        (4, 128256, 4096),
    ]

    for M, N, K in linear_layer_sizes:
        weight = torch.randn(N, K).cuda().to(dtype=torch.bfloat16)
        input = torch.randn(M, K).cuda().to(dtype=torch.bfloat16)
        times = repeat('torch.nn.functional.linear(weight, input);' * repeats_inner + 'torch.cuda.synchronize()', repeats, warmup, locals())
        times = np.array(times) / repeats_inner
        mean = np.mean(times)
        print(f"M={M} N={N} K={K} time [ms]: mean {1e3*mean:.3f} std {1e3*np.std(times):.3f} tflop/s {M*N*K/1e12/mean:.3f}")
    # B, M, N, K
    bmm_sizes = [
        (128, 8, 64, 128),
        (128, 8, 128, 64),
    ]
    for B, M, N, K in bmm_sizes:
        X = torch.randn(B, M, K).cuda().to(dtype=torch.bfloat16)
        Y = torch.randn(B, K, N).cuda().to(dtype=torch.bfloat16)
        times = repeat('torch.matmul(X, Y);' * repeats_inner + 'torch.cuda.synchronize()', repeats, warmup, locals())
        times = np.array(times) / repeats_inner
        mean = np.mean(times)
        print(f"B={B} M={M} N={N} K={K} time [ms]: mean {1e3*mean:.3f} std {1e3*np.std(times):.3f} tflop/s {B*M*N*K/1e12/mean:.3f}")


if __name__ == "__main__":
    # reference_llama3_matmul_performance()
    run_matmul()
    # run_feedforward()
