from gen import *
from kernels import *
import numpy as np
import ray
import os

if os.getenv("RAY_HEAD") is not None:
    ray.init(address=f'ray://{os.getenv("RAY_HEAD")}:10001', runtime_env={"working_dir": "./", 'env_vars': {'LD_LIBRARY_PATH': '/users/mchrapek/miniforge3/lib/'}})

def test_transform(program):
    print(program)

def test_transform_applicability():
    simulator = "banshee"
    #optimized_softmax(simulator, test_transform)
    for seed in range(0, 1000):
        print("Running with seed", seed)
        opt = lambda program: greedy_opt(program, seed=seed, remaining_random=20)
        print("Sum")
        optimized_sum(simulator, opt)
        print("Leakyrelu")
        optimized_leakyrelu(simulator, opt)
        print("Gemm")
        optimized_gemm(simulator, opt)
        print("Softmax")
        optimized_softmax(simulator, opt)
        print("Conv")
        optimized_batchnorm(simulator, opt)
        optimized_conv2d(simulator, opt)
        

def optimized_sum(simulator, transform, check_correctness=True, size=[8, 8]):
    size_dict = {"B": size[0], "N": size[1]}
    program = create_sum_program()
    specialize_inputs(program, size_dict)
    input_data = generate_input_for_program(program, {})
    transform(program)
    output_data = ref_sum_program(input_data)
    cycles = test_snitch_code(program, {**input_data, **output_data}, simulator, check_correctness)
    print("cycles:", cycles)

def optimized_leakyrelu(simulator, transform, check_correctness=True, size=[8, 8]):
    size_dict = {"B": size[0], "N": size[1]}
    program = create_leakyrelu_program()
    specialize_inputs(program, size_dict)
    input_data = generate_input_for_program(program, {})
    transform(program)
    output_data = ref_leakyrelu(input_data)
    cycles = test_snitch_code(program, {**input_data, **output_data}, simulator, check_correctness)
    print("cycles:", cycles)

def optimized_gemm(simulator, transform, check_correctness=True, size=[8, 8, 8]):
    size_dict = {"M": size[0], "N": size[1], "K": size[2]}
    program = create_gemm_program()
    specialize_inputs(program, size_dict)
    input_data = generate_input_for_program(program, {
        "alpha": 0.9, "beta": 0.1,
    })
    transform(program)
    output_data = ref_gemm(input_data)
    cycles = test_snitch_code(program, {**input_data, **output_data}, simulator, check_correctness)
    print("cycles:", cycles)

def optimized_conv2d(simulator, transform, check_correctness=True, size=[4, 4, 10, 10, 3]):
    M = 4
    C = 4
    H = 10
    W = 10
    K = 3

    d = {
        "N": 1, #batches
        "M": M, #feature maps (or output channels)
        "C": C, #input channels
        "H_in": H, #input height
        "W_in": W, #input width
        "kernel": K,
        "stride": 1,
        "dilation": 1,
    }
    sizes = {
        "N": 1, #batches
        "M": M, #feature maps (or output channels)
        "C": C, #input channels
        "H_in": H, #input height
        "W_in": W, #inpuy width
        "kernel": K,
        # "stride": 1,
        # "dilation": 0,
    }
    d["H_out"] = pooling_out_dim(d["H_in"], d["kernel"], d["stride"], 0, d["dilation"])
    d["W_out"] = pooling_out_dim(d["W_in"], d["kernel"], d["stride"], 0, d["dilation"])
    sizes["H_out"] = pooling_out_dim(d["H_in"], d["kernel"], d["stride"], 0, d["dilation"])
    sizes["W_out"] = pooling_out_dim(d["W_in"], d["kernel"], d["stride"], 0, d["dilation"])

    def opt(program):
        specialize_inputs(program, sizes)
        transform(program)


    cycles = run_kernel(create_conv2d_nopad_program, lambda x: ref_conv2d_nopad({**x, 'G': 1}), opt, test_snitch_code, fixed_inputs=d)
    print("cycles:", cycles)

def optimized_softmax(simulator, transform, check_correctness=True, size=[2, 16]):
    fixed_inputs = {
        "B": size[0],
        "N": size[1],
        "src": np.random.uniform(-1, 1, (size[0], size[1])),
    }
    program = create_softmax_program()
    input_data = generate_input_for_program(program, fixed_inputs)
    specialize_inputs(program, {'B': size[0], 'N': size[1]})
    output_data = ref_softmax(input_data)
    transform(program)
    cycles = test_snitch_code(program, {**input_data, **output_data}, simulator, check_correctness)
    print("cycles:", cycles)


def optimized_batchnorm(simulator, transform, check_correctness=True, size=[2, 4, 4, 8]):
    program = create_batchnorm_program()
    input_data = generate_input_for_program(program, {
        'N': size[0], 'C': size[1], 'H': size[2], 'W': size[3], "epsilon": 1e-5, "momentum": 0.9,
        'scale': np.random.randn(size[1]), 'bias': np.random.randn(size[1]),
    })
    specialize_inputs(program, {'N': size[0], 'C': size[1], 'H': size[2], 'W': size[3]})
    transform(program)
    output_data = ref_batchnorm(input_data)
    cycles = test_snitch_code(program, {**input_data, **output_data}, simulator, check_correctness)
    print("cycles:", cycles)


def test_portable_transform_applicability():
    # last number is the coverage of the group from 0.0 to 1.0
    # (approximate fraction of taken transforms among the available)
    random_groups = [
        (tile_scope, find_tileable_scopes, 0.1),
        (tile_buffer, find_tileable_buffers, 0.1),
        (create_dimension, find_creatable_dimensions, 0.1),
        (delete_dimension, find_deletable_dimensions, 0.1),
        (swap_nested_scopes, find_swappable_nested_scopes, 0.3),
        (create_temporary, find_creatable_temporaries, 0.1),
        (swap_ops, find_swappable_ops, 0.3),
        (swap_buffer_dims, find_swappable_buffer_dims, 0.3),
        (untile_buffer, find_untileable_buffers, 0.1),
        (delete_temporary, find_deletable_temporaries, 0.9),
        (untile_scope, find_untileable_scopes, 0.9),
        (join_scopes, find_joinable_scopes, 0.9),
        (split_scopes, find_splittable_scopes, 0.1),
        (reuse_arr_dims, find_reusable_arr_dims, 0.9),
        (unroll_scope, find_unrollable_scopes, 0.2),
        (reuse_buffers, find_reusable_buffers, 0.9),
        (move_buf_to_stack, find_bufs_movable_to_stack, 0.9),
        (parallelize_scope, find_parallelizable_scopes, 0.9),
    ]
    for seed in range(0, 1000):
    # for seed in [6]:
        print("Running with seed", seed)
        for kernel, (create_program, create_inputs, reference) in ONNX_KERNEL_DICT.items():
            # if kernel != "affine_grid_2d":
            #     continue
            print("Running", kernel)
            random.seed(seed)
            np.random.seed(seed)
            sizes, fixed_inputs = create_inputs()
            
            def opt(program):
                specialize_inputs(program, sizes)
                # random_opt(program, seed=seed, silent=False)
                random_opt(program, random_groups, seed=seed, silent=True)
            def test_native_code1(*args, **kwargs):
                return test_native_code(*args, **kwargs, silent=False, remove_tmp_files=False)
            run_result = run_kernel(create_program, reference, opt, test_native_code1, fixed_inputs)
            if run_result["error"]:
                print("Error:", run_result["error"])
                return
            else:
                time = run_result['measured']
                print(f"Time [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")


@ray.remote(num_cpus=1, scheduling_strategy="SPREAD")
def test_distributed_portable_transform_applicability(seed, curr_min_time):
    # last number is the coverage of the group from 0.0 to 1.0
    # (approximate fraction of taken transforms among the available)
    random_groups = [
        (tile_scope, find_tileable_scopes, 0.1),
        (tile_buffer, find_tileable_buffers, 0.1),
        (create_dimension, find_creatable_dimensions, 0.1),
        (delete_dimension, find_deletable_dimensions, 0.1),
        (swap_nested_scopes, find_swappable_nested_scopes, 0.3),
        (create_temporary, find_creatable_temporaries, 0.1),
        (swap_ops, find_swappable_ops, 0.3),
        (swap_buffer_dims, find_swappable_buffer_dims, 0.3),
        (untile_buffer, find_untileable_buffers, 0.1),
        (delete_temporary, find_deletable_temporaries, 0.9),
        (untile_scope, find_untileable_scopes, 0.9),
        (join_scopes, find_joinable_scopes, 0.9),
        (split_scopes, find_splittable_scopes, 0.1),
        (reuse_arr_dims, find_reusable_arr_dims, 0.9),
        (unroll_scope, find_unrollable_scopes, 0.2),
        (reuse_buffers, find_reusable_buffers, 0.9),
        (move_buf_to_stack, find_bufs_movable_to_stack, 0.9),
        (parallelize_scope, find_parallelizable_scopes, 0.9),
    ]
    kernel = "gemm"
    create_program, create_inputs, reference = ONNX_KERNEL_DICT[kernel]
    print(f"Running {kernel} with seed {seed}, current min time {curr_min_time}")
    random.seed(seed)
    np.random.seed(seed)
    sizes, fixed_inputs = create_inputs()

    def opt(program):
        specialize_inputs(program, sizes)
        return uniform_random_opt(program, random_groups, seed=seed, silent=True)
    
    path = []
    try:
        run_result = run_kernel(create_program, reference, opt, test_native_code, fixed_inputs,
                                           timeout=curr_min_time)
        time = run_result['measured']
        path = run_result['path']
        if run_result["error"]:
            print("Error:", run_result["error"])
            return -1, path
        else:
            time = run_result['measured']
            print(f"Time [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")
            return time, path
    except OSError as e:
        if e.errno == 26:
            return -2, path
        return -1, path
    except Exception as e:
        return -1, path

def run_ray_optimization():
    random.seed(42)
    number_of_workers = 32
    number_of_iters_between_updates = 1
    iterations = 0
    random_ints = [random.randint(0, 10000000) for _ in range(number_of_workers)]
    print(f"[DEBUG] Random ints: {random_ints}")
    minimum_time = np.inf
    futures = [test_distributed_portable_transform_applicability.remote(random_ints[i], minimum_time) for i in range(number_of_workers)]
    minimum_path = []
    with open("errors.txt", "w") as errors:
        with open("results.csv", "w") as results:
            results_writer = csv.writer(results)
            while True:
                future, futures = ray.wait(futures)
                current_time, current_path = ray.get(future[0])
                iterations += 1
                if current_time == -1:
                    errors.write(str(current_path) + "\n")
                    errors.flush()
                elif np.median(current_time) < minimum_time:
                    minimum_time = np.median(current_time)
                    minimum_path = current_path
                results_writer.writerow([minimum_time])
                results.flush()
                if iterations == number_of_iters_between_updates:
                    print("Iterations: ", iterations)
                    iterations = 0
                    print("Minimum time: ", minimum_time)
                    print("Minimum path: ", minimum_path)
                futures.append(test_distributed_portable_transform_applicability.remote(random.randint(0, 10000000), minimum_time * 1.5))

if __name__ == "__main__":
    # test_transform_applicability()
    test_portable_transform_applicability()
    # run_ray_optimization()
    