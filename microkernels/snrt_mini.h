#pragma once
#include <snrt.h>
#include <stdint.h>

#define f32 float
#define f64 double

#define i8 int8_t
#define i16 int16_t
#define i32 int32_t
#define i64 int64_t

#define u8 uint8_t
#define u16 uint16_t
#define u32 uint32_t
#define u64 uint64_t

// see https://stackoverflow.com/questions/71354999/how-to-print-the-register-number-with-gcc-style-inline-assembly
#define REG_CONST(reg_name, reg_idx) asm(".equ .L__reg_const__" #reg_name ", " #reg_idx);
REG_CONST(zero, 0)
REG_CONST(ra, 1)
REG_CONST(sp, 2)
REG_CONST(gp, 3)
REG_CONST(tp, 4)
REG_CONST(t0, 5)
REG_CONST(t1, 6)
REG_CONST(t2, 7)
REG_CONST(s0, 8) REG_CONST(fp, 8)
REG_CONST(s1, 9)
REG_CONST(a0, 10)
REG_CONST(a1, 11)
REG_CONST(a2, 12)
REG_CONST(a3, 13)
REG_CONST(a4, 14)
REG_CONST(a5, 15)
REG_CONST(a6, 16)
REG_CONST(a7, 17)
REG_CONST(s2, 18)
REG_CONST(s3, 19)
REG_CONST(s4, 20)
REG_CONST(s5, 21)
REG_CONST(s6, 22)
REG_CONST(s7, 23)
REG_CONST(s8, 24)
REG_CONST(s9, 25)
REG_CONST(s10, 26)
REG_CONST(s11, 27)
REG_CONST(t3, 28)
REG_CONST(t4, 29)
REG_CONST(t5, 30)
REG_CONST(t6, 31)

/// The SSR configuration registers.
enum {
    SSR_REG_STATUS = 0,
    SSR_REG_REPEAT = 1,
    SSR_REG_BOUNDS = 2,   // + loop index
    SSR_REG_STRIDES = 6,  // + loop index
    SSR_REG_RPTR = 24,    // + snrt_ssr_dim
    SSR_REG_WPTR = 28,    // + snrt_ssr_dim
};

// https://pulp-platform.github.io/snitch_cluster/rm/custom_instructions.html#configuration-register-operations
inline void my_write_ssr_cfg(uint32_t reg, uint32_t dm, uint32_t value) {
    uint32_t inp0 = reg << 5 | dm;
    uint32_t inp1 = value;
    asm volatile(
        ".word (0b0000000 << 25) |"
        "(.L__reg_const__%0 << 20) |"
        "(.L__reg_const__%1 << 15) |"
        "(0b010 << 12) |"
        "(0b00001 << 7) |"
        "(0b0101011 << 0)" 
        :
        : "r"(inp0), "r"(inp1)
    );
}

inline uint32_t my_read_ssr_cfg(uint32_t reg, uint32_t dm) {
    // scfgr out, inp
    uint32_t inp = reg << 5 | dm;
    uint32_t out;
    asm volatile(
        ".word (0b0000000 << 25) |"
        "(.L__reg_const__%1 << 20) |"
        "(0b00001 << 15) |"
        "(0b001 << 12) |"
        "(.L__reg_const__%0 << 7) |"
        "(0b0101011 << 0)"
        : "=r"(out) : "r"(inp));
    return out;
}

// https://pulp-platform.github.io/snitch_cluster/rm/custom_instructions.html#xfrep-extension-for-floating-point-repetition
#define FREP(ops, rep) \
    ".word (%[" #ops "] << 20) | (.L__reg_const__%[" #rep "] << 15) | (0 << 12) | (0 << 8) | (1 << 7) | (0b0001011 << 0);"

inline void snrt_ssr_barrier(uint32_t dm) {
    while (my_read_ssr_cfg(SSR_REG_STATUS, dm) >> 31 == 0) {}
    // snrt_fpu_fence();
}

        
inline double my_div(double a, double b) {
    double c;
    asm ("fdiv.d %0, %1, %2" : "=f"(c) : "f"(a), "f"(b));
    return c;
}

inline double my_sqrt(double a) {
    double c;
    asm ("fsqrt.d %0, %1" : "=f"(c) : "f"(a));
    return c;
}

inline double my_sgnjx(double a, double b) {
    double c;
    //c = (b < 0) ? -a : a;
    asm ("fsgnjx.d %0, %1, %2" : "=f"(c) : "f"(a), "f"(b));
    return c;
}

inline float reinterpret_f32_from_i32(int32_t i) {
    union {
        int32_t i;
        float f;
    } u;
    u.i = i;
    return u.f;
}

inline int32_t reinterpret_i32_from_f32(float f) {
    union {
        int32_t i;
        float f;
    } u;
    u.f = f;
    return u.i;
}


extern __thread struct snrt_team *_snrt_team_current;
extern __thread uint32_t _snrt_core_idx;
extern const uint32_t _snrt_team_size;

struct snrt_team {
    /// Pointer to the root team description of this cluster.
    struct snrt_team_root *root;
};

struct snrt_allocator_inst {
    // Base address from where allocation starts
    uint32_t base;
    // Number of bytes alloctable
    uint32_t size;
    // Address of the next allocated block
    uint32_t next;
};
struct snrt_allocator {
    struct snrt_allocator_inst l1;
    struct snrt_allocator_inst l3;
};

// This struct is placed at the end of each clusters TCDM
struct snrt_team_root {
    struct snrt_team base;
    const void *bootdata;
    uint32_t global_core_base_hartid;
    uint32_t global_core_num;
    uint32_t cluster_idx;
    uint32_t cluster_num;
    uint32_t cluster_core_base_hartid;
    uint32_t cluster_core_num;
    snrt_slice_t global_mem;
    snrt_slice_t cluster_mem;
    snrt_slice_t zero_mem;
    struct snrt_allocator allocator;
    struct snrt_barrier cluster_barrier;
    uint32_t barrier_reg_ptr;
    struct snrt_peripherals peripherals;
};


inline void *snrt_l1_next() {
    struct snrt_allocator_inst *alloc = &snrt_current_team()->allocator.l1;
    return (void*)alloc->next;
}

inline void *snrt_l3_next() { 
    struct snrt_allocator_inst *alloc = &snrt_current_team()->allocator.l3;
    return (void*)alloc->next;
}

inline void my_snrt_cluster_hw_barrier() {
    asm volatile("csrr x0, 0x7C2" ::: "memory");
}