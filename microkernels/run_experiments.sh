#!/bin/bash


source ~/.bashrc
conda activate main
export CC=gcc
export LDFLAGS="-lm -L$(dirname $(which clang))/../lib/"
export CFLAGS="-O3 -I . -fopenmp -march=native -I$(dirname $(which clang))/../include/"
export LD_LIBRARY_PATH="$(dirname $(which clang))/../lib/"
export OMP_PROC_BIND=close


numactl -N 0 -m 0 -C 0-17 python batchnorm_fp32_avx2.py | grep -e '[ms]'
numactl -N 0 -m 0 -C 0-17 python layernorm_fp32_avx2.py | grep -e '[ms]'
numactl -N 0 -m 0 -C 0-17 python matmul_fp32_avx2.py | grep -e '[ms]'
numactl -N 0 -m 0 -C 0-17 python relu_fp32_avx2.py | grep -e '[ms]'
numactl -N 0 -m 0 -C 0-17 python reducemean_fp32_avx2.py | grep -e '[ms]'
numactl -N 0 -m 0 -C 0-17 python conv_fp32_avx2.py | grep -e '[ms]'
