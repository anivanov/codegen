# This file contains the code for the distributed search algorithm built
# on top of MPI.
# The basic idea of this approach is that we spawn a number of ranks
# and the master rank is responsible for aggregating the data from all
# the other ranks. Each rank performs the search for a given number of
# iterations and sends the time and the optimization path to the master
# rank. The master rank updates the minimum time and other relevant
# information and sends the updated information back to the rank that
# sent the data. The worker rank then uses the updated information to
# perform the next iteration of the search.

import argparse
import os
import logging
from mpi4py import MPI
from typing import List, Tuple, Dict
from scipy.special import softmax
import numpy as np
import heapq
import shutil

from gen import *
from kernels import *
from utils import *

MASTER_RANK = 0
TIMEOUT_TOLERANCE = 2


def num_avail_workers(worker_pool: List) -> int:
    """
    A util function that returns the number of available workers
    in the worker pool.
    """
    return sum([1 for w in worker_pool if w])


def search(seed: int, kernel: str, curr_min_time: float,
           transformations: List, logger: logging.Logger) \
    -> Tuple[float, List]:
    """
    A function executed by the worker ranks to perform the search.
    Performs one iteration of the search algorithm.
    """
    create_program, create_inputs, reference = ONNX_KERNEL_DICT[kernel]
    random.seed(seed)
    np.random.seed(seed)

    sizes, fixed_inputs = create_inputs()

    def opt(program: RootScope) -> List:
        # specialize_inputs(program, sizes)
        return uniform_random_opt(program, RANDOM_GROUPS, seed=seed, silent=True)
    
    
    def create_prog_with_specialized_input():
        program = create_program()
        specialize_inputs(program, sizes)
        return program

    start = MPI.Wtime()

    # FIXME (sishen): questionable design pattern
    res = run_kernel(create_prog_with_specialized_input,
                                        reference, opt, test_native_code, fixed_inputs,
                                        transformations=transformations,
                                        timeout=curr_min_time)
    logger.info(f"Time taken for the search: {MPI.Wtime() - start:.4f} s")
    if len(res) == 2:
        exception, path = res
        logger.error(f"Exception: {exception}")
        err_code = -1 if isinstance(exception, subprocess.TimeoutExpired) else -2
        return err_code, path
    else:
        runtime, ref, peak, path = res
        logger.info(f"Kernel Runtime [s]: avg {np.mean(runtime):.3f} std {np.std(runtime):.3f}")
    return runtime, path


def evaluate(kernel: str, current_level: int, curr_min_time: float,
           transformations: List, logger: logging.Logger) \
    -> Tuple[float, List, int]:
    """
    A function executed by the beam search workers to evaluate each kernel.
    """
    create_program, create_inputs, reference = ONNX_KERNEL_DICT[kernel]
    sizes, fixed_inputs = create_inputs()

    def opt(program: RootScope) -> List:
        return []

    def create_prog_with_specialized_input():
        program = create_program()
        specialize_inputs(program, sizes)
        return program

    start = MPI.Wtime()
    res = run_kernel(create_prog_with_specialized_input,
                    reference, opt, test_native_code, fixed_inputs,
                    transformations=transformations,
                    timeout=curr_min_time)
    logger.info(f"Time taken for the evaluation: {MPI.Wtime() - start:.4f} s")
    if len(res) == 2:
        exception, path = res
        logger.error(f"Exception: {exception}")
        err_code = -1 if isinstance(exception, subprocess.TimeoutExpired) else -2
        return err_code, path, current_level
    else:
        runtime, ref, peak, path = res
        logger.info(f"Kernel Runtime [s]: avg {np.mean(runtime):.3f} std {np.std(runtime):.3f}")
    return runtime, path, current_level


def worker_rank_loop(comm: MPI.Comm, rank: int, kernel: str,
                     strategy: str, logger: logging.Logger) -> None:
    """
    The main loop for the worker rank in the distributed search algorithm.
    @param rank: The rank of the worker
    @param num_iters: The number of iterations to run the search
    @param kernel: The name of the kernel to optimize
    """
    min_time = np.inf
    seed = None
    min_time = None
    transformations = []
    while True:
        data = comm.recv(source=MASTER_RANK, tag=0)
        if data is None:
            # If the master rank sends None, it means that the search
            # has been completed
            break
        
        # Unpacks the data received from the master rank as per the
        # search strategy
        if strategy == "random":
            seed, min_time = data
            func = search
            args = (seed, kernel, min_time * TIMEOUT_TOLERANCE, transformations, logger)
        elif strategy == "heuristic":
            seed, (min_time, transformations) = data
            func = search
            args = (seed, kernel, min_time * TIMEOUT_TOLERANCE, transformations, logger)
        elif strategy == "beam":
            seed = 0
            min_time, transformations, current_level = data
            func = evaluate
            args = (kernel, current_level, min_time * TIMEOUT_TOLERANCE, transformations, logger)
        else:
            raise NotImplementedError(f"Strategy {strategy} is not implemented yet")
        
        logger.info(f"Received data from the master rank: seed {seed}, min_time {min_time}, transformations {transformations}")

        data = func(*args)
        # Sends the time and path to the master rank
        comm.send(data, dest=MASTER_RANK, tag=0)
        logger.info("Sent data to the master rank")

    logger.info("Search completed")

def fill_task_queue_for_beam_search(task_queue: List, curr_min_time: float, best_paths: list,
                                    num_beams: int, sample: bool, kernel: str, logger: logging.Logger) \
    -> int:
    """
    Fills the task queue with the tasks for the worker ranks
    for the beam search strategy.

    The tasks for the beam search have the following structure:
    (curr_min_time: float, transformations: list, current_level: int)

    The results coming from the search should have the following structure:
    (runtime: float, transformations: list, current_level: int, is_final: bool)
    is_final == True represents the node that does not conduct any transformation and is by default added to the best_paths.
    """
    # If best_paths is empty, then we initialize with the original program 
    if len(best_paths) == 0:
        # Evaluate current program for time
        task_queue.append((curr_min_time, [], 0))
    else:
        # Choose up to num_beams best paths either by sampling or by taking the best ones
        beams_to_consider = []
        if sample:
            probabilities = softmax([-np.log(x[0]) for x in best_paths])
            indexes = random.choices(range(len(best_paths)), k=min(num_beams, len(best_paths)), weights=probabilities)
            beams_to_consider = [best_paths[i] for i in indexes]
            heapq.heapify(beams_to_consider)
        else:
            for _ in range(num_beams):
                if len(best_paths) == 0:
                    break
                beams_to_consider.append(heapq.heappop(best_paths))
        # Clear best paths
        best_paths.clear()
    
        for beam in beams_to_consider:
            # If the beam is a final beam and its level is final we log it
            if beam[3] and beam[2] == len(RANDOM_GROUPS):
                logger.info(f"Found final beam with time {beam[0]} and path {beam[1]}")
                continue

            # Append to the best paths the current beam with the final flag
            # Representing the node that does not conduct any transformation and skips a level
            best_time = beam[0]
            transformations = beam[1]
            current_level = beam[2]
            is_final = beam[3]
            heapq.heappush(best_paths, (best_time, transformations, current_level + 1, True))
            
            # Evaluate possible next paths to be taken
            # Intialize the program with appropriate transformations
            create_program, create_inputs, _ = ONNX_KERNEL_DICT[kernel]
            sizes, _ = create_inputs()
            program = create_program()
            specialize_inputs(program, sizes)
            try:
                if transformations is not None:
                    # Canonicalizes the program before running the optimizer.
                    apply_to_exhaustion(program, [
                        (split_scopes, find_splittable_scopes),
                    ], silent=True)

                    for transform_name, loc in transformations:
                        assert transform_name in CPU_TRANSFORMATIONS
                        apply_func, _ = CPU_TRANSFORMATIONS[transform_name]
                        apply_func(program, *loc)
            except Exception as e:
                raise Exception(f"Error while searching for transformations {transformations}: {e}")

            # Find the options
            applicable_list = []
            while len(applicable_list) == 0 and current_level < len(RANDOM_GROUPS):
                transformation, options, _ = RANDOM_GROUPS[current_level]
                applicable_list = options(program)
                if applicable_list == []:
                    current_level += 1
                for chosen_location in applicable_list:
                    temp_transformations = copy.deepcopy(transformations)
                    temp_transformations.append((transformation.__name__, chosen_location))
                    # Assign possible paths to tasks
                    task_queue.append((best_time, temp_transformations, current_level))
            
    return len(task_queue)
    
def fill_task_queue_for_random_search(task_queue: List,
                                      avail_workers: int, rem_tasks: int,
                                      curr_min_time: float) \
    -> int:
    """
    Fills the task queue with the tasks for the worker ranks
    for the random search strategy.
    """
    for _ in range(min(avail_workers, rem_tasks)):
        # Generates a random seed for each task
        task_queue.append((np.random.randint(0, 1e6), curr_min_time))
        rem_tasks -= 1
    
    return rem_tasks

def fill_task_queue_for_heuristic_search(task_queue: List,
                                         avail_workers: int, rem_tasks: int,
                                         curr_min_time: float,
                                         best_paths: List,
                                         alpha: float, N: int) \
        -> int:
        """
        Fills the task queue with the tasks for the worker ranks
        for the heuristic search strategy.
        @param best_paths: A list of the best K paths found so far
        @param alpha: The parameter that controls the exploration vs exploitation
        tradeoff in the heuristic search strategy.
        @param N: The number of transforms to exploit from one of the best paths.
        """
        for _ in range(min(avail_workers, rem_tasks)):
            r = np.random.rand()
            if r < alpha or len(best_paths) == 0:
                # If r is less than the exploration parameter alpha,
                # it means that we will explore a random path
                task_queue.append((np.random.randint(0, 1e6), (curr_min_time, [])))
            else:
                # Otherwise, we will exploit the first N transforms
                # from one of the best path

                # Chooses the path to exploit according to their quality
                # Essentially, the shorter the runtime, the better the path
                # and the more likely it is to be chosen
                weights = [1 / (-x[0]) for x in best_paths] ## FIXME (mchrapek): I suggest doing softmax or log_softmax here
                weights = np.array(weights) / np.sum(weights)
                chosen = best_paths[np.random.choice(len(best_paths), p=weights)]
                # FIXME (sishen): This is a temporary variable that
                # can be potentially turned into a parameter
                N = int(0.5 * len(chosen[1]))
                seed = np.random.randint(0, 1e6)
                task_queue.append((seed, (curr_min_time, chosen[1][:N])))
            rem_tasks -= 1
        return rem_tasks


# ================= MAIN LOOP FOR THE MASTER RANK =================

def master_rank_main_loop(comm: MPI.Comm, strategy: str,
                          num_iters: int, size: int,
                          logger: logging.Logger,
                          res_file_path: str, kernel: str, 
                          beam_num: int, sample: bool) -> None:
    """
    FIXME (sishen): probably not the best way to implement the main loop.

    The main loop for the master rank in the distributed search algorithm.
    @param num_iters: The number of iterations to run the search
    for each worker rank.
    @param size: The number of ranks in the MPI communicator
    """
    task_queue = []

    # Initializes a file named "result.csv" to store the results
    res_file = open(res_file_path, "w")
    res_file.write("runtime,paths\n")
    res_file.flush()
    logger.info(f"Logging results to {res_file_path}")

    # A list to keep track of the worker ranks that are currently
    # available to perform the search
    worker_pool = [True for _ in range(size)]
    worker_pool[MASTER_RANK] = False
    rem_tasks = num_iters * (size - 1)

    min_time = np.inf
    best_path = []

    # Parameters used in the heuristic search strategy
    K = 10
    best_paths = []
    alpha = 0.3
    N = 30

    # Parameters used in the beam search strategy
    checked_transformations = 0
    
    while True:
        if strategy == "random":
            rem_tasks = fill_task_queue_for_random_search(task_queue,
                                                          num_avail_workers(worker_pool),
                                                          rem_tasks, min_time)
        elif strategy == "heuristic":
            rem_tasks = fill_task_queue_for_heuristic_search(task_queue,
                                                             num_avail_workers(worker_pool),
                                                             rem_tasks, min_time,
                                                             best_paths,
                                                             alpha, N)
        elif strategy == "beam":
            if len(task_queue) == 0 and num_avail_workers(worker_pool) == size - 1:
                logger.info(f"Current best paths: {best_paths}")
                rem_tasks = fill_task_queue_for_beam_search(task_queue, min_time, best_paths, beam_num, sample, kernel, logger)
                logger.info(f"Selected beams: {best_paths}")
                checked_transformations = 0
                logger.info(f"Filled task queue with {len(task_queue)} tasks: {task_queue}")
        else:
            raise NotImplementedError(f"Strategy {strategy} is not implemented yet")

        if len(task_queue) == 0 and num_avail_workers(worker_pool) == size - 1:
            # If the task queue is empty, then it means that the search
            # has been completed
            logger.info("Search completed")
            assert num_avail_workers(worker_pool) == size - 1, "All worker ranks should be available"
            for worker_rank in range(1, size):
                comm.send(None, dest=worker_rank, tag=0)
            break

        # Sends the tasks to the worker ranks
        for worker_rank in range(1, size):
            if worker_pool[worker_rank] and len(task_queue) > 0:
                task_data = task_queue.pop(0)
                comm.send(task_data, dest=worker_rank, tag=0)
                logger.info(f"Sent task to rank {worker_rank}, task data: {task_data}")
                worker_pool[worker_rank] = False

        # Receives data from the worker ranks
        status = MPI.Status()
        worker_data = comm.recv(source=MPI.ANY_SOURCE, tag=0, status=status)

        # Updates the worker pool to mark the worker rank as available
        worker_pool[status.Get_source()] = True
        if strategy == "beam":
            runtime, path, current_level = worker_data
        else:
            runtime, path = worker_data
        logger.info(f"Received data from rank {status.Get_source()}, worker data: {worker_data}")
        
        # Writes the results to the result file
        res_file.write(f"{np.median(runtime) if isinstance(runtime, list) else runtime},{path}\n")
        res_file.flush()

        if not isinstance(runtime, list):
            logger.error(f"An error occurred in rank {status.Get_source()}")
            if strategy == "beam":
                checked_transformations += 1
                logger.info(f"Checked transformation [{checked_transformations}/{rem_tasks}]")
            continue
            
        worker_time = np.median(runtime)
        if worker_time < min_time:
            min_time = worker_time
            best_path = path
            logger.info(f"Updated min time {min_time} and best path {best_path}")
        
        # Aggregates the data from the worker ranks as per the search strategy
        if strategy == "random":
            pass
        elif strategy == "heuristic":
            # If the strategy is heuristic, then the master rank will maintain
            # a heap of the best K paths
            if len(best_paths) < K:
                heapq.heappush(best_paths, (-worker_time, path))
            else:
                if np.median(runtime) < -best_paths[0][0]:
                    heapq.heappop(best_paths)
                    heapq.heappush(best_paths, (-worker_time, path))
            current_best_times = [float(-x[0]) for x in best_paths]
            best_path_lengths = [len(x[1]) for x in best_paths]
            logger.info(f"Current best times: {current_best_times}, best paths length {best_path_lengths}")
        elif strategy == "beam":
            heapq.heappush(best_paths, (worker_time, path, current_level, False))
            checked_transformations += 1
            logger.info(f"Checked transformation [{checked_transformations}/{rem_tasks}]")
        else:
            raise NotImplementedError(f"Strategy {strategy} is not implemented yet")
        
    res_file.flush()
    res_file.close()
        


def main(strategy: str, kernel: str, num_iters: int,
         enable_logging: bool, log_dir: str, res_file: str,
         beams: int, sample: bool) -> None:
    """
    Main function for the distributed search algorithm.
    """
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    size = comm.Get_size()

    # Makes the log directory if it does not exist
    if rank == MASTER_RANK:
        if not os.path.exists(log_dir):
            os.makedirs(log_dir)
        else:
            # If the log directory already exists, then remove the existing
            # log files
            for f in os.listdir(log_dir):
                os.remove(os.path.join(log_dir, f))
        for directory in glob.glob("generated*"):
            shutil.rmtree(directory)
    comm.barrier()

    # Initializes the logger for each rank
    logger = logging.getLogger(f"rank[{rank}]")
    level = logging.DEBUG
    logger.setLevel(level)
    formatter = logging.Formatter(f"[{'MASTER' if rank == 0 else f'WORKER {rank}':^10}]" + "[{levelname:^8s}] [{asctime}]: {message}", style="{")
    
    file_handler = logging.FileHandler(f"{log_dir}/rank_{rank}.log")
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)

    stream_handler = logging.StreamHandler()
    stream_handler.setLevel(level)
    stream_handler.setFormatter(formatter)
    logger.addHandler(stream_handler)

    logger.disabled = not enable_logging
    logger.info(f"Rank {rank} started")

    assert size >= 1, "The distributed search algorithm requires at least 2 ranks"

    # Seed for the random number generator
    if rank == MASTER_RANK:
        if enable_logging:
            logger.info(f"[INFO] Logging enabled, logs will be stored in '{log_dir}'")
        logger.info(f"Running distributed search with {size} ranks")
        logger.info(f"[MASTER] [INFO] ================== Search Strategy: {strategy} ==================")
        logger.info(f"Search Strategy: {strategy}")
        master_rank_main_loop(comm, strategy, num_iters, size, logger, res_file, kernel, beams, sample)
    else:
        worker_rank_loop(comm, rank, kernel, strategy, logger)
        

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Distributed search for kernel optimizations")
    parser.add_argument("-n", "--num_iters", type=int, default=100, dest="num_iters",
                        help="Number of iterations of the search performed by each rank")
    parser.add_argument("-k", "--kernel", type=str, default="gemm", dest="kernel",
                        help="Name of the kernel to optimize")
    parser.add_argument("-s", "--strategy", type=str, default="random", dest="strategy",
                        help="Optimization strategy to use in the search algorithm, options are ['random', 'greedy', 'heuristic]")
    parser.add_argument("-l", "--log", dest="enable_logging",
                        action="store_true", help="Whether to enable logging or not")
    parser.add_argument("--log-dir", type=str, default="logs", dest="log_dir",
                        help="Directory to store the logs")
    parser.add_argument("-r", "--res-file", type=str, default="result.csv", dest="res_file",
                        help="File to store the results")
    parser.add_argument("--beams", type=int, default=3, dest="beams",
                        help="Number of beams to consider in the beam search strategy")
    parser.add_argument("--sample", dest="sample", action="store_true",
                        help="Whether to sample the beams or not")
    args = parser.parse_args()
    
    main(args.strategy, args.kernel, args.num_iters,
         args.enable_logging, args.log_dir, args.res_file,
         args.beams, args.sample)
