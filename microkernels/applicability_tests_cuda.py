from gen import *
from kernels import *
import numpy as np


def test_cuda_transform_applicability():
    # last number is the coverage of the group from 0.0 to 1.0
    # (approximate fraction of taken transforms among the available)
    random_groups = [
        (tile_scope, find_tileable_scopes, 0.1),
        (tile_buffer, find_tileable_buffers, 0.1),
        (create_dimension, find_creatable_dimensions, 0.1),
        (delete_dimension, find_deletable_dimensions, 0.1),
        (swap_nested_scopes, find_swappable_nested_scopes, 0.3),
        (create_temporary, find_creatable_temporaries, 0.1),
        (swap_ops, find_swappable_ops, 0.3),
        (swap_buffer_dims, find_swappable_buffer_dims, 0.3),
        (untile_buffer, find_untileable_buffers, 0.1),
        (delete_temporary, find_deletable_temporaries, 0.9),
        (untile_scope, find_untileable_scopes, 0.9),
        (join_scopes, find_joinable_scopes, 0.9),
        (split_scopes, find_splittable_scopes, 0.1),
        (reuse_arr_dims, find_reusable_arr_dims, 0.9),
        (unroll_scope, find_unrollable_scopes, 0.2),
        (reuse_buffers, find_reusable_buffers, 0.9),
        (parallelize_cuda_warp_scope, find_cuda_warp_scopes, 0.9),
        (parallelize_cuda_block_scope, find_cuda_block_scopes, 0.9),
        (parallelize_cuda_grid_scope, find_cuda_grid_scopes, 0.9),
        (move_buf_to_stack, find_bufs_movable_to_stack_cuda, 0.95),
        (move_buf_to_cache, find_bufs_movable_to_cache_cuda, 0.95),
    ]
    for seed in range(0, 1000):
    # for seed in [19]:
        print("Running with seed", seed)
        for kernel, (create_program, create_inputs, reference) in {**TEST_KERNEL_DICT, **ONNX_KERNEL_DICT}.items():
            # if kernel != "argmax":
            #     continue
            print("Running", kernel)
            random.seed(seed)
            np.random.seed(seed)
            sizes, fixed_inputs = create_inputs()
            def opt(program):
                specialize_inputs(program, sizes)
                random_opt(program, random_groups, seed=seed, silent=False)
                print(program.text())
                # random_opt(program, random_groups, seed=seed, silent=True)
            time = run_kernel(create_program, reference, opt, test_cuda_code, fixed_inputs)


if __name__ == "__main__":
    test_cuda_transform_applicability()