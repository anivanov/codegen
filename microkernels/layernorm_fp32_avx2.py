from gen import *
import torch


def layernorm_handoptimized(M, N):
    
    src = np.random.uniform(-1, 1, (M, N)).astype(np.float32)
    gamma = np.random.uniform(-1, 1, (N,)).astype(np.float32)
    beta = np.random.uniform(-1, 1, (N,)).astype(np.float32)
    
    def init_src():
        src[:] = np.random.uniform(-1, 1, (M, N)).astype(np.float32)
        gamma[:] = np.random.uniform(-1, 1, (N,)).astype(np.float32)
        beta[:] = np.random.uniform(-1, 1, (N,)).astype(np.float32)
        
    tsrc = torch.from_numpy(src)
    tgamma  = torch.from_numpy(gamma)
    tbeta = torch.from_numpy(beta)
        
    pt_mean, pt_std, _ = time_prof(10, lambda: torch.nn.functional.layer_norm(tsrc, [N], weight=tgamma, bias=tbeta), init_src)
    print(f"Pytorch layernorm time [ms]: avg {pt_mean * 1e3:.6f} std {pt_std * 1e3:.6f}")
    
    dst = torch.nn.functional.layer_norm(tsrc, [N], weight=tgamma, bias=tbeta, eps=1e-5).numpy()    
    
    def eval_func(program, ignore=False):
        if ignore:
            return
        print(program.text())
        time = test_native_code(
            program,
            {'src': src, 'gamma': gamma, 'beta': beta, 'dst': dst},
            min_reps=10,
            remove_tmp_files=False,
            silent=False,
            timeout=60,
        )
        time = time[max(len(time) // 3, 1):]
        print(f"My layernorm [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")
    
    program_text = f"""
        Name: layernorm
        In: src, gamma, beta
        Out: dst
        Declarations:
            src f32 [{M}, {N}] heap
            gamma f32 [{N}] heap
            beta f32 [{N}] heap
            dst f32 [{M}, {N}] heap
            m f32 [{M}] heap
            mu f32 [{M}] heap
            diff f32 [{M}, {N}] heap
            q f32 [{M}] heap
            tmp f32 [{M}, {N}] heap
            qeps f32 [{M}] heap
            sigma f32 [{M}] heap
        Code:
        {M} {N} m[{{0}}] += src[{{0}}, {{1}}]
        {M} mu[{{0}}] = m[{{0}}] / {N}
        {M} {N} diff[{{0}}, {{1}}] = src[{{0}}, {{1}}] - mu[{{0}}]
        {M} {N} q[{{0}}] += diff[{{0}}, {{1}}] * diff[{{0}}, {{1}}]
        {M} {N} tmp[{{0}}, {{1}}] = diff[{{0}}, {{1}}] * gamma[{{1}}]
        {M} qeps[{{0}}] = q[{{0}}] * {1./(N - 1.)} + 0.00001
        {M} sigma[{{0}}] = 1 / sqrtf(qeps[{{0}}])
        {M} {N} dst[{{0}}, {{1}}] = sigma[{{0}}] * tmp[{{0}}, {{1}}] + beta[{{1}}]
    """
    
    program = parse_program(program_text); eval_func(program, ignore=False)
    
    apply_to_exhaustion(program, [(join_scopes, find_joinable_scopes)], silent=False, eval_func=eval_func)
    expected_cores = 16
    tile_scope(program, 'm#1', expected_cores); eval_func(program)
    swap_nested_scopes(program, 'm#2'); eval_func(program)

    reuse_buffers(program, 'tmp', 'dst'); eval_func(program)
    apply_to_exhaustion(program, [(tile_buffer, find_tileable_buffers)], silent=False, eval_func=eval_func)
    apply_to_exhaustion(program, [(reuse_arr_dims, find_reusable_arr_dims)], silent=False, eval_func=eval_func)

    apply_to_exhaustion(program, [(move_buf_to_stack, lambda x: find_bufs_movable_to_stack(x, 4096))], silent=False, eval_func=eval_func)
    parallelize_scope(program, 'm#2'); eval_func(program, ignore=False)
    

if __name__ == "__main__":
    layernorm_handoptimized(4096, 4096)