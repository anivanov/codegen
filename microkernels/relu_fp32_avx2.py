from gen import *
import torch

def run_relu_handtuned(M, N):
    
    nx = np.random.uniform(-10, 10, (M, N)).astype(np.float32)
    tx = torch.from_numpy(nx)
    
    def init_src():
        nx[:] = np.random.uniform(-10, 10, (M, N)).astype(np.float32)
    
    pt_mean, pt_std, pt_builtin_runtime = time_prof(10, lambda: torch.nn.functional.relu(tx), init_src)
    print(f"Pytorch builtin relu time [ms]: avg {pt_mean * 1e3:.6f} std {pt_std * 1e3:.6f} reps {len(pt_builtin_runtime)}")

    tz = torch.nn.functional.relu(tx)
    nz = tz.numpy()

    def eval_func(program, ignore=True):
        if ignore:
            return
        print(program.text())
        time = test_native_code(
            program,
            {'x': nx, 'z': nz},
            min_reps=10,
            remove_tmp_files=False,
            silent=False,
        )[3:]
        print(f"My relu [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")

    program_text = f"""
        Name: relu
        In: x
        Out: z
        Declarations:
            x f32 [{M}, {N}] heap
            z f32 [{M}, {N}] heap
        Code:
        {M} {N} z[{{0}}, {{1}}] = fmaxf(x[{{0}}, {{1}}], 0)
    """
    
    program = parse_program(program_text); eval_func(program, ignore=False)
    parallelize_scope(program, 'z#1'); eval_func(program, ignore=False)


if __name__ == '__main__':
    run_relu_handtuned(4096, 4096)