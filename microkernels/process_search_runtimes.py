import re
import argparse
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import math
import pathlib
from cycler import cycler



parser = argparse.ArgumentParser(description='Process search runtimes')
parser.add_argument('output', type=str, help='Output file')
parser.add_argument('files', type=str, nargs='+', help='Files to process')

args = parser.parse_args()

data = {}

for file_name in args.files:
    # name without suffix
    path = pathlib.Path(file_name)
    name = path.stem
    with open(path, "r", encoding="utf8", errors='ignore') as f:
        runtimes = []
        for line in f:
            # Runtime (measured) [ms]: avg 0.978630 std 0.230425
            match = re.search(r'Runtime.* \[ms\]: avg ([0-9.]+) std ([0-9.]+)$', line)
            if match:
                avg, std = match.groups()
                runtimes.append(float(avg))

        runtimes = np.array(runtimes)

        best_runtimes = np.minimum.accumulate(runtimes)
        
        data[name] = (runtimes, best_runtimes)


# indices = [str(idx) for idx in all_indices]
fig, ax = plt.subplots(layout='constrained', figsize=(10, 5))
ax.set_prop_cycle( cycler('linestyle', ['-','--',':','-.']) * cycler('color', list('rbgykcm')))  # https://stackoverflow.com/a/7799661
for name, (_, best_runtimes) in data.items():
    indices = 1 + np.arange(len(best_runtimes))
    ax.step(indices, best_runtimes, label=name)
ax.set_yscale('log')
ax.set_xscale('log')
ax.set_xlabel('Iteration')
ax.set_ylabel('Runtime [ms]')
ax.legend()
ax.set_title('Search Runtimes')
fig.savefig(args.output)
