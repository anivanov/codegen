import numpy as np
import timeit
import onnxruntime as ort
import torch
import onnx
import os
from kernels import pooling_out_dim
from gen import time_prof
import copy


def test_relu(M, N):
    input = onnx.helper.make_tensor_value_info('input', onnx.TensorProto.FLOAT, [M, N])
    output = onnx.helper.make_tensor_value_info('output', onnx.TensorProto.FLOAT, [M, N])
    eps = 1e-5
    node = onnx.helper.make_node(
        'Relu',
        inputs=['input'],
        outputs=['output'],
    )
    graph_def = onnx.helper.make_graph(
        [node],
        'model',
        [input],
        [output],
    )
    model_def = onnx.helper.make_model(graph_def)
    
    sess_opt = ort.SessionOptions()
    num_threads = int(os.environ.get('OMP_NUM_THREADS', 1))
    sess_opt.intra_op_num_threads = num_threads
    if num_threads > 1:
        # onnxruntime does not need the main thread affinity because it is already the thread that is running this code
        sess_opt.add_session_config_entry('session.intra_op_thread_affinities', ';'.join(str(x) for x in range(1, num_threads)))
        
    ort_session = ort.InferenceSession(
        model_def.SerializeToString(),
        providers=["CPUExecutionProvider"],
        sess_options=sess_opt
    )
    
    input_data = np.random.randn(M, N).astype(np.float32)
    inputs = {'input': input_data}
    [output_data] = ort_session.run(None, inputs)
    
    torch_output = torch.nn.functional.relu(torch.from_numpy(input_data))
    
    assert torch.allclose(torch_output, torch.from_numpy(output_data), rtol=1e-3, atol=1e-4)

    def init():
        inputs['input'][:] = copy.deepcopy(input_data)
    
    mean, std, runtime = time_prof(10, lambda: ort_session.run(None, inputs), init, time_budget=1)
    
    runtime = timeit.repeat(lambda: ort_session.run(None, inputs), repeat=100, number=1)[30:]
    print(f"Relu {M}x{N} time [ms]: avg {mean * 1e3:.6f} std {std * 1e3:.6f} repeats {len(runtime)}")


def test_layernorm(M, N):

    input = onnx.helper.make_tensor_value_info('input', onnx.TensorProto.FLOAT, [M, N])
    scale = onnx.helper.make_tensor_value_info('scale', onnx.TensorProto.FLOAT, [N])
    output = onnx.helper.make_tensor_value_info('output', onnx.TensorProto.FLOAT, [M, N])
    eps = 1e-5
    node = onnx.helper.make_node(
        'LayerNormalization',
        inputs=['input', 'scale'],
        outputs=['output'],
        epsilon=eps
    )
    graph_def = onnx.helper.make_graph(
        [node],
        'model',
        [input, scale],
        [output],
    )
    model_def = onnx.helper.make_model(graph_def)
    
    sess_opt = ort.SessionOptions()
    num_threads = int(os.environ.get('OMP_NUM_THREADS', 1))
    sess_opt.intra_op_num_threads = num_threads
    if num_threads > 1:
        # onnxruntime does not need the main thread affinity because it is already the thread that is running this code
        sess_opt.add_session_config_entry('session.intra_op_thread_affinities', ';'.join(str(x) for x in range(1, num_threads)))
        
    ort_session = ort.InferenceSession(
        model_def.SerializeToString(),
        providers=["CPUExecutionProvider"],
        sess_options=sess_opt
    )
    
    input_data = np.random.randn(M, N).astype(np.float32)
    scale_data = np.random.randn(N).astype(np.float32)
    inputs = {'input': input_data, 'scale': scale_data}
    [output_data] = ort_session.run(None, inputs)
    
    torch_output = torch.nn.functional.layer_norm(torch.tensor(input_data), [N], torch.tensor(scale_data), eps=eps)
    
    assert torch.allclose(torch_output, torch.from_numpy(output_data), rtol=1e-3, atol=1e-4)
    
    # runtime = timeit.repeat(lambda: ort_session.run(None, inputs), repeat=100, number=1)[30:]
    # print(f"LayerNormalization {M}x{N} time [ms]: avg {np.mean(runtime) * 1e3:.6f} std {np.std(runtime) * 1e3:.6f}")
    
    def init():
        inputs['input'][:] = copy.deepcopy(input_data)
        inputs['scale'][:] = copy.deepcopy(scale_data)
        
    mean, std, runtime = time_prof(10, lambda: ort_session.run(None, inputs), init, time_budget=1)
    print(f"LayerNormalization {M}x{N} time [ms]: avg {mean * 1e3:.6f} std {std * 1e3:.6f} repeats {len(runtime)}")


def test_batchnorm(N, C, H, W):
    eps = 1e-5
    input = onnx.helper.make_tensor_value_info('input', onnx.TensorProto.FLOAT, [N, C, H, W])
    scale = onnx.helper.make_tensor_value_info('scale', onnx.TensorProto.FLOAT, [C])
    bias = onnx.helper.make_tensor_value_info('bias', onnx.TensorProto.FLOAT, [C])
    mean = onnx.helper.make_tensor_value_info('mean', onnx.TensorProto.FLOAT, [C])
    var = onnx.helper.make_tensor_value_info('var', onnx.TensorProto.FLOAT, [C])
    output = onnx.helper.make_tensor_value_info('output', onnx.TensorProto.FLOAT, [N, C, H, W])
    node = onnx.helper.make_node(
        'BatchNormalization',
        inputs=['input', 'scale', 'bias', 'mean', 'var'],
        outputs=['output'],
        epsilon=eps,
        training_mode=0,
    )
    graph_def = onnx.helper.make_graph(
        [node],
        'model',
        [input, scale, bias, mean, var],
        [output],
    )
    model_def = onnx.helper.make_model(graph_def)
    
    sess_opt = ort.SessionOptions()
    num_threads = int(os.environ.get('OMP_NUM_THREADS', 1))
    sess_opt.intra_op_num_threads = num_threads
    if num_threads > 1:
        # onnxruntime does not need the main thread affinity because it is already the thread that is running this code
        sess_opt.add_session_config_entry('session.intra_op_thread_affinities', ';'.join(str(x) for x in range(1, num_threads)))
        
    ort_session = ort.InferenceSession(
        model_def.SerializeToString(),
        providers=["CPUExecutionProvider"],
        sess_options=sess_opt
    )
    
    input_data = np.random.uniform(-1, 1, (N, C, H, W)).astype(np.float32)
    scale_data = np.random.uniform(-1, 1, (C,)).astype(np.float32)
    bias_data = np.random.uniform(-1, 1, (C,)).astype(np.float32)
    mean_data = np.random.uniform(-1, 1, (C,)).astype(np.float32)
    var_data = np.random.uniform(0, 1, (C,)).astype(np.float32)
    
    inputs = {'input': input_data, 'scale': scale_data, 'bias': bias_data, 'mean': mean_data, 'var': var_data}
    [output_data] = ort_session.run(None, inputs)
    
    tinput = torch.from_numpy(input_data)
    tscale = torch.from_numpy(scale_data)
    tbias = torch.from_numpy(bias_data)
    tmean = torch.from_numpy(mean_data)
    tvar = torch.from_numpy(var_data)
    
    ty = torch.nn.functional.batch_norm(tinput, tmean, tvar, weight=tscale, bias=tbias, training=False, eps=eps)
    assert torch.allclose(ty, torch.from_numpy(output_data), rtol=1e-3, atol=1e-4)
    
    # runtime = timeit.repeat(lambda: ort_session.run(None, inputs), repeat=100, number=1)[30:]
    # print(f"BatchNorm {N}x{C}x{H}x{W} time [ms]: avg {np.mean(runtime) * 1e3:.6f} std {np.std(runtime) * 1e3:.6f}")
    
    def init():
        inputs['input'][:] = copy.deepcopy(input_data)
        inputs['scale'][:] = copy.deepcopy(scale_data)
        inputs['bias'][:] = copy.deepcopy(bias_data)
        inputs['mean'][:] = copy.deepcopy(mean_data)
        inputs['var'][:] = copy.deepcopy(var_data)
        
    mean, std, runtime = time_prof(10, lambda: ort_session.run(None, inputs), init, time_budget=1)
    print(f"BatchNorm {N}x{C}x{H}x{W} time [ms]: avg {mean * 1e3:.6f} std {std * 1e3:.6f} repeats {len(runtime)}")


def test_conv(N, M, C, H_in, W_in, K):
    H = pooling_out_dim(input=H_in, kernel=K, stride=1, pad=0, dilation=1)
    W = pooling_out_dim(input=W_in, kernel=K, stride=1, pad=0, dilation=1)
    
    input = onnx.helper.make_tensor_value_info('input', onnx.TensorProto.FLOAT, [N, C, H_in, W_in])
    weight = onnx.helper.make_tensor_value_info('weight', onnx.TensorProto.FLOAT, [M, C, K, K])
    output = onnx.helper.make_tensor_value_info('output', onnx.TensorProto.FLOAT, [N, M, H, W])
    node = onnx.helper.make_node(
        'Conv',
        inputs=['input', 'weight'],
        outputs=['output'],
    )
    graph_def = onnx.helper.make_graph(
        [node],
        'model',
        [input, weight],
        [output],
    )
    model_def = onnx.helper.make_model(graph_def)
    
    sess_opt = ort.SessionOptions()
    num_threads = int(os.environ.get('OMP_NUM_THREADS', 1))
    sess_opt.intra_op_num_threads = num_threads
    if num_threads > 1:
        # onnxruntime does not need the main thread affinity because it is already the thread that is running this code
        sess_opt.add_session_config_entry('session.intra_op_thread_affinities', ';'.join(str(x) for x in range(1, num_threads)))
        
    ort_session = ort.InferenceSession(
        model_def.SerializeToString(),
        providers=["CPUExecutionProvider"],
        sess_options=sess_opt
    )
    
    input_data = np.random.randn(N, C, H_in, W_in).astype(np.float32)
    weight_data = np.random.randn(M, C, K, K).astype(np.float32)
    inputs = {'input': input_data, 'weight': weight_data}
    [output_data] = ort_session.run(None, inputs)
    
    tinput = torch.from_numpy(input_data)
    tweight = torch.from_numpy(weight_data)
    toutput = torch.nn.functional.conv2d(tinput, tweight)
        
    assert torch.allclose(toutput, torch.from_numpy(output_data), rtol=1e-3, atol=1e-4)
    
    # runtime = timeit.repeat(lambda: ort_session.run(None, inputs), repeat=100, number=1)[30:]
    # print(f"Conv {N}x{M}x{C}x{H_in}x{W_in}x{K} time [ms]: avg {np.mean(runtime) * 1e3:.6f} std {np.std(runtime) * 1e3:.6f}")
    
    def init():
        inputs['input'][:] = copy.deepcopy(input_data)
        inputs['weight'][:] = copy.deepcopy(weight_data)
        
    mean, std, runtime = time_prof(10, lambda: ort_session.run(None, inputs), init, time_budget=1)
    print(f"Conv {N}x{M}x{C}x{H_in}x{W_in}x{K} time [ms]: avg {mean * 1e3:.6f} std {std * 1e3:.6f} repeats {len(runtime)}")


def test_reducemean(M, N):
    input = onnx.helper.make_tensor_value_info('input', onnx.TensorProto.FLOAT, [M, N])
    output = onnx.helper.make_tensor_value_info('output', onnx.TensorProto.FLOAT, [M])
    axes = onnx.helper.make_tensor_value_info('axes', onnx.TensorProto.INT64, [1])
    eps = 1e-5
    node = onnx.helper.make_node(
        'ReduceMean',
        inputs=['input', 'axes'],
        outputs=['output'],
        keepdims=0,
    )
    graph_def = onnx.helper.make_graph(
        [node],
        'model',
        [input, axes],
        [output],
    )
    model_def = onnx.helper.make_model(graph_def, opset_imports=[onnx.helper.make_operatorsetid("", 18)])
    
    sess_opt = ort.SessionOptions()
    num_threads = int(os.environ.get('OMP_NUM_THREADS', 1))
    sess_opt.intra_op_num_threads = num_threads
    if num_threads > 1:
        # onnxruntime does not need the main thread affinity because it is already the thread that is running this code
        sess_opt.add_session_config_entry('session.intra_op_thread_affinities', ';'.join(str(x) for x in range(1, num_threads)))
        
    ort_session = ort.InferenceSession(
        model_def.SerializeToString(),
        providers=["CPUExecutionProvider"],
        sess_options=sess_opt
    )
    
    input_data = np.random.randn(M, N).astype(np.float32)
    axes_data = np.array([1], dtype=np.int64)
    inputs = {'input': input_data, 'axes': axes_data}
    [output_data] = ort_session.run(None, inputs)
    
    torch_output = torch.mean(torch.from_numpy(input_data), dim=1)
    
    assert torch.allclose(torch_output, torch.from_numpy(output_data), rtol=1e-3, atol=1e-4)
    
    # runtime = timeit.repeat(lambda: ort_session.run(None, inputs), repeat=100, number=10)[30:]
    # runtime = np.array(runtime) / 10
    # print(f"Reducemean {M}x{N} time [ms]: avg {np.mean(runtime) * 1e3:.6f} std {np.std(runtime) * 1e3:.6f}")
    
    def init():
        inputs['input'][:] = copy.deepcopy(input_data)
        inputs['axes'][:] = copy.deepcopy(axes_data)
        
    mean, std, runtime = time_prof(10, lambda: ort_session.run(None, inputs), init, time_budget=1)
    print(f"Reducemean {M}x{N} time [ms]: avg {mean * 1e3:.6f} std {std * 1e3:.6f} repeats {len(runtime)}")


def test_matmul(M, K, N):
    A = onnx.helper.make_tensor_value_info('A', onnx.TensorProto.FLOAT, [M, K])
    B = onnx.helper.make_tensor_value_info('B', onnx.TensorProto.FLOAT, [K, N])
    C = onnx.helper.make_tensor_value_info('C', onnx.TensorProto.FLOAT, [M, N])

    node = onnx.helper.make_node(
        'MatMul',
        inputs=['A', 'B'],
        outputs=['C'],
    )
    graph_def = onnx.helper.make_graph(
        [node],
        'model',
        [A, B],
        [C],
    )
    model_def = onnx.helper.make_model(graph_def)
    
    sess_opt = ort.SessionOptions()
    num_threads = int(os.environ.get('OMP_NUM_THREADS', 1))
    sess_opt.intra_op_num_threads = num_threads
    if num_threads > 1:
        # onnxruntime does not need the main thread affinity because it is already the thread that is running this code
        sess_opt.add_session_config_entry('session.intra_op_thread_affinities', ';'.join(str(x) for x in range(1, num_threads)))
        
    ort_session = ort.InferenceSession(
        model_def.SerializeToString(),
        providers=["CPUExecutionProvider"],
        sess_options=sess_opt
    )
    
    A_data = np.asfortranarray(np.random.randn(M, K).astype(np.float32))
    B_data = np.random.randn(K, N).astype(np.float32)
    inputs = {'A': A_data, 'B': B_data}
    [output_data] = ort_session.run(None, inputs)
    
    torch_output = torch.matmul(torch.from_numpy(A_data), torch.from_numpy(B_data))
    
    assert torch.allclose(torch_output, torch.from_numpy(output_data), rtol=1e-3, atol=1e-4)
    
    # runtime = timeit.repeat(lambda: ort_session.run(None, inputs), repeat=100, number=10)[30:]
    # runtime = np.array(runtime)
    # print(f"MatMul {M}x{K}x{N} time [ms]: avg {np.mean(runtime) * 1e3:.6f} std {np.std(runtime) * 1e3:.6f}")
    
    def init():
        inputs['A'][:] = copy.deepcopy(A_data)
        inputs['B'][:] = copy.deepcopy(B_data)
        
    mean, std, runtime = time_prof(10, lambda: ort_session.run(None, inputs), init, time_budget=1)
    print(f"MatMul {M}x{K}x{N} time [ms]: avg {mean * 1e3:.6f} std {std * 1e3:.6f} repeats {len(runtime)}")


if __name__ == "__main__":
    test_matmul(768, 1024, 1024)
    test_reducemean(4096, 4096)
    test_conv(8, 10, 3, 512, 512, 5)
    test_batchnorm(N=8, C=3, H=2048, W=2048)
    test_relu(4096, 4096)
    test_layernorm(4096, 4096)