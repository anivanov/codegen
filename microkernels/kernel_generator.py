from gen import *
import random
import itertools
from multiset import Multiset
import copy


class KernelGenerator:
    def __init__(self, seed):
        self.seed = seed
        self.rng = random.Random(seed)
        
        # initialize parameters
        self.max_scope_depth = 5
        self.max_dims_per_array = 4
        self.max_reduce_dims = 2
        self.num_op_budget = [1, 10]
        self.reduction_prob = 0.4
        self.max_loop_nest_volume = 2**24
        self.const_input_prob = 0.1
        self.reuse_input_prob = 0.5
        self.bcast_prob = 0.3
        self.reuse_dim_prob = 0.7
        
        literals = [chr(c) for c in range(ord('a'), ord('z'))]
        self.valid_names = list(''.join(x) for x in itertools.product(literals, repeat=2))
        self.rng.shuffle(self.valid_names)        
        self.num_used_names = 0
    
    def existing_dims(self, existing_operands):
        "Return all dimensions that are used by existing operands"
        all_dims = Multiset()
        for operand in existing_operands:
            if isinstance(operand, ConstElem):
                continue
            all_dims = Multiset([d.name for d in self.buffers[operand.name].dims]) | all_dims
        return all_dims
    
    def new_dim_constant(self):
        buf_name = self.get_new_name()
        self.buffers[buf_name] = Buffer(dims=[], dtype="i32")
        return Array(buf_name, indices=[])
    
    def create_new_output_dims(self, num_new_dims):
        dims = []
        for _ in range(num_new_dims):
            dim = self.new_dim_constant()
            dims.append(dim)
        return dims

    def get_new_name(self):
        name = self.valid_names[self.num_used_names]
        self.num_used_names += 1
        return name

    def get_current_outputs(self):
        return 'out'

    def get_current_inputs(self):
        accessed_inputs = set()
        initialized_inputs = set()
        for loop_nest_with_op in self.ops:
            op = loop_nest_with_op
            while not isinstance(op, Operation):
                op = op.ops[0]
            initialized_inputs.add(op.out_elem.name)
            for a in op.inp_elem:
                if isinstance(a, Array):
                    accessed_inputs.add(a.name)
        return accessed_inputs - initialized_inputs
    
    def get_current_dimensions(self):
        all_dimensions = set()
        for buf_name, buf in self.buffers.items():
            if buf.dtype == 'i32' and not buf.dims:
                all_dimensions.add(buf_name)
        return all_dimensions
        
        
    def generate_loops_with_operation(self):
        # select operator
        op_desc = self.generate_operator()
        
        # generate output element
        out_elem = self.generate_output_operand(op_desc)

        # generate input elements
        inp_elems = self.generate_input_operands(op_desc, out_elem)

        # generate loop nest
        loop_nest_dims = self.generate_loop_nest(op_desc, out_elem, inp_elems)

        operation = Operation(out_elem, inp_elems, op_desc)
        loop_nest_with_op = loop_scope(operation, loop_nest_dims)
        return loop_nest_with_op


    def generate_operator(self):
        # first pick function
        func_list = {
            '{0}': 5, '{0} + {1}': 5, '{0} - {1}': 5, '{0} * {1}': 10, '{0} / {1}': 3,
            'max({0}, {1})': 4, 'min({0}, {1})': 4, 'abs({0})': 4, 'sqrt({0})': 3,
            'sin({0})': 1, 'cos({0})': 1,
        }
        [op_func] = self.rng.choices(list(func_list.keys()), weights=list(func_list.values()))

        # we can attempt to use reduction only if the operator compatible with reduction
        reduction_compatibility = op_func in ('{0}', '{0} * {1}')
        with_reduction = reduction_compatibility and (self.rng.random() < self.reduction_prob)
        
        if with_reduction:
            if op_func == '{0} * {1}':
                reduce_list = ['{y} += {x}']
            elif op_func == '{0}':
                reduce_list = ['{y} += {x}', '{y} = max({y}, {x})', '{y} = min({y}, {x})']
            else:
                raise ValueError(f'Unexpected function {op_func}')
            op_reduce = self.rng.choice(reduce_list)
        else:
            op_reduce = ''

        return OpDesc(out_dtype='f32', func=op_func, reduce=op_reduce)

    def generate_loop_nest(self, op_desc, out_elem, inp_elems):
        # generate loop nest
        all_elem = [out_elem, *inp_elems]
        all_dims = sorted(self.existing_dims(all_elem))
        
        loop_nest = [Array(name=d, indices=[]) for d in all_dims]
        # adjust operand indices to point to correct loops
        for operand in all_elem:
            if isinstance(operand, ConstElem):
                continue
            self.buffers[operand.name].dims = sorted(self.buffers[operand.name].dims, key=lambda d: d.name)
            elem_dims = [d.name for d in self.buffers[operand.name].dims]
            assert Multiset(elem_dims) <= Multiset(all_dims)
            taken_dims = []
            taken_indices = []
            next_index = 0
            # next loop relies on assumption that both loop nest dims and buffer dims are sorted
            for d in elem_dims:
                while loop_nest[next_index].name != d:
                    next_index += 1
                taken_dims.append(loop_nest[next_index].name)
                taken_indices.append(next_index)
                next_index += 1
            operand.indices = [Index(len(loop_nest) - i - 1) for i in taken_indices]
        
        return loop_nest
        

    def generate_output_operand(self, op_desc):
        # pick input to be initialized
        if len(self.ops) == 0:
            name = 'out'
            if op_desc.reduce:
                # we need to add 1-2 extra dimensions for reduction
                num_reduce_dims = self.rng.randint(1, self.max_reduce_dims)
            else:
                num_reduce_dims = 0
            # we can select any shape for the output
            num_dims = self.rng.randint(1, self.max_dims_per_array - num_reduce_dims)
            dims = self.create_new_output_dims(num_dims)
            self.buffers[name] = Buffer(dims=dims, dtype='f32')
        else:
            name = self.rng.choice(sorted(self.get_current_inputs()))
        out_shape = self.buffers[name].dims
        out_elem = Array(name, [Index(i) for i in range(len(out_shape))])
        return out_elem
        

    def generate_input_operand(self, op_desc, out_elem, inp_elems, num_total_input):
        """
        inp_elems is a list of input elements that have been already generated for this operation.
        num_total_input is the total number of inputs that will be generated for this operation.
        """
        # for reduction, we need to make sure that the set of input dimensions is larger than the set of output dimensions
        is_last_input = len(inp_elems) == num_total_input - 1
        force_new_dims = op_desc.reduce and is_last_input
        # we want to make sure that at least one input is not a constant
        force_non_const = force_new_dims or (is_last_input and all(isinstance(a, ConstElem) for a in inp_elems))
        # decide if we want to generate a constant input
        if (not force_non_const) and self.rng.random() < self.const_input_prob:
            # generate constant input
            inp_elem = ConstElem(self.rng.random() * 2 - 1, dtype='f32')
            return inp_elem
        else:
            # generate non-constant input

            # filter out existing inputs that are compatible
            all_inputs = sorted(self.get_current_inputs() - {out_elem.name})
            compatible_inputs = []
            # contains all dimensions used so far by current operator
            base_dims = self.existing_dims([out_elem, *inp_elems])
            for name in all_inputs:
                # if operation is not a reduction, we can only use inputs that have
                # dimensions that are a strict subset of output dimensions
                if not op_desc.reduce:
                    buffer_dims = Multiset([d.name for d in self.buffers[name].dims])
                    if not (buffer_dims <= base_dims):
                        continue
                # if total number of dimensions becomes too large, skip this input
                candidate_buf = self.buffers[name]
                candidate_dims = set(d.name for d in candidate_buf.dims)
                # if force_new_dims is set, only allow inputs that have at least one new dimension
                if force_new_dims and not (Multiset(candidate_dims) - base_dims):
                    continue
                if len(base_dims | candidate_dims) > self.max_scope_depth:
                    continue
                compatible_inputs.append(name)
                
            
            # decide if we want to reuse existing input
            reuse_existing_input = compatible_inputs and self.rng.random() < self.reuse_input_prob
            if reuse_existing_input:
                # reuse existing input
                all_inputs = sorted(self.get_current_inputs() - {out_elem.name})
                name = self.rng.choice(compatible_inputs)
                
            else:
                # creating new input
                name = self.get_new_name()
                
                inp_shape = []
                
                # contains dimensions used by previous operators
                # may not include newly created dimensions for current operator
                existing_dims = self.get_current_dimensions()
                # existing dimensions, that were not used by the current operator
                existing_unused_dims = Multiset(existing_dims) - base_dims
                
                allow_new_dims = bool(op_desc.reduce)
                min_new_dims = 1 if force_new_dims else 0
                num_dims = self.rng.randint(1, min(self.max_dims_per_array, len(out_elem.indices)))
                untaken_base_dims = Multiset(base_dims)
                num_new_dims = 0
                for _ in range(num_dims):
                    if not allow_new_dims:
                        create_new_dim = False
                    elif num_new_dims < min_new_dims:
                        create_new_dim = True
                        num_new_dims += 1
                    else:
                        create_new_dim = self.rng.random() < self.bcast_prob
                    if create_new_dim:
                        # here we may use completely new dimensions or 
                        # reuse existing ones that were not used by the current operator
                        if existing_unused_dims and self.rng.random() < self.reuse_dim_prob:
                            # existing dimension that was not used by the current operator
                            inp_shape.append(self.rng.choice(list(existing_unused_dims)))
                        else:
                            # completely new dimension
                            inp_shape.append(self.new_dim_constant())
                    else:
                        # reuse dimension that was used by the current operator
                        dim = self.rng.choice(list(untaken_base_dims))
                        untaken_base_dims -= Multiset([dim])
                        inp_shape.append(dim)
                self.buffers[name] = Buffer(dims=inp_shape, dtype='f32')
                
            inp_elem = Array(name, [Index(i) for i in range(len(self.buffers[name].dims))])
            return inp_elem
    
    
    def generate_input_operands(self, op_desc, out_elem):
        num_inputs = 2 if '{1}' in op_desc.func else 1
        inp_elems = []
        for _ in range(num_inputs):
            inp_elems.append(self.generate_input_operand(op_desc, out_elem, inp_elems, num_inputs))
        return inp_elems


    def get_kernel(self):
        # generate code
        self.ops = []
        self.buffers = {}
        num_ops = self.rng.randint(*self.num_op_budget)
        for _ in range(num_ops):
            loop_nest_with_op = self.generate_loops_with_operation()
            self.ops.insert(0, loop_nest_with_op)
        return RootScope(self.ops, f'gen{self.seed:05d}', input_declarations=self.buffers, reusable_inputs={}, used_outputs={'out'})
    
    def gen_constant_dims(self, kernel):
        loop_nest_sizes = []
        for op in kernel.ops:
            loop_size = []
            while isinstance(op, LoopScope):
                loop_size.append(op.dim)
                op = op.ops[0]
            loop_nest_sizes.append(loop_size)
        # process nests from the largest to the smallest
        loop_nest_sizes.sort(key=lambda x: -len(x))
        
        loop_size_map = {}
        for loop_nest in loop_nest_sizes:
            max_loop_size = self.max_loop_nest_volume ** (1 / len(loop_nest))
            max_pow2 = math.floor(math.log2(max_loop_size))
            for loop_dim in loop_nest:
                if isinstance(loop_dim, Array):
                    if loop_dim.name not in loop_size_map:
                        pow2 = self.rng.randint(1, max_pow2)
                        max_factor = max(1, max_loop_size // 2**pow2)
                        factor = self.rng.randint(1, max_factor)
                        loop_size = factor * 2**pow2
                        loop_size_map[loop_dim.name] = loop_size

        kernel_copy = copy.deepcopy(kernel)
        specialize_inputs(kernel_copy, loop_size_map)
        return kernel_copy
        

if __name__ == '__main__':
    min_cycles = 1e9
    max_cycles = 0
    for i in range(1000):
        print(f'Generating kernel {i}')
        random.seed(i)
        kernel_gen = KernelGenerator(seed=random.randint(0, 1000000-1))
        program = kernel_gen.get_kernel()
        print(program.text())
        const_program = kernel_gen.gen_constant_dims(program)
        print(const_program.text())
        expected_cycles = const_program.peak_performance_cycles()
        perc_expected = 100 * expected_cycles / 1e9
        print(f'Generated cycles: {expected_cycles} relative to expectation: {perc_expected:.2f} %')
        if perc_expected > 100:
            print('Warning: expected cycles are too high')
            raise ValueError('Too many cycles')
