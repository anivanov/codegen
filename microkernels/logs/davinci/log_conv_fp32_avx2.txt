Pytorch builtin conv2d time [ms]: avg 198.873321 std 4.096849
Name: conv2d
In: input, weight
Out: output
Declarations:
    input f32 [8, 3, 512, 512] heap
    output f32 [8, 10, 508, 508] heap
    weight f32 [10, 3, 5, 5] heap
Code:
8 10 508 508 3 5 5 output[{6},{5},{4},{3}] += input[{6},{2},({4})+({1}),({3})+({0})] * weight[{5},{2},{1},{0}]
Compiling (/home/aivanov/codegen/microkernels/codegen/generated/b75405/)...
Compiling (/home/aivanov/codegen/microkernels/codegen/generated/b75405/)... DONE (1.009 s)
Running (native)...
Running (native)... DONE (11.112 s)
My time [ms]: avg 998.595429 std 0.232899
Name: conv2d
In: input, weight
Out: output
Declarations:
    input f32 [8, 3, 512, 512] heap
    output f32 [8, 10, 508, 508] heap
    weight f32 [10, 3, 5, 5] heap
Code:
8 10 127 4 508 3 5 5 output[{7},{6},(({5})*(4))+({4}),{3}] += input[{7},{2},((({5})*(4))+({4}))+({1}),({3})+({0})] * weight[{6},{2},{1},{0}]
Compiling (/home/aivanov/codegen/microkernels/codegen/generated/73efa9/)...
Compiling (/home/aivanov/codegen/microkernels/codegen/generated/73efa9/)... DONE (0.714 s)
Running (native)...
Running (native)... DONE (11.114 s)
My time [ms]: avg 998.571714 std 0.352987
Name: conv2d
In: input, weight
Out: output
Declarations:
    input f32 [8, 3, 512, 512] heap
    output f32 [8, 10, 508, 508] heap
    weight f32 [10, 3, 5, 5] heap
Code:
8 127 10 4 508 3 5 5 output[{7},{5},(({6})*(4))+({4}),{3}] += input[{7},{2},((({6})*(4))+({4}))+({1}),({3})+({0})] * weight[{5},{2},{1},{0}]
Compiling (/home/aivanov/codegen/microkernels/codegen/generated/034c69/)...
Compiling (/home/aivanov/codegen/microkernels/codegen/generated/034c69/)... DONE (0.698 s)
Running (native)...
Running (native)... DONE (11.078 s)
My time [ms]: avg 994.572429 std 0.209383
Name: conv2d
In: input, weight
Out: output
Declarations:
    input f32 [8, 3, 512, 512] heap
    output f32 [8, 10, 508, 508] heap
    weight f32 [10, 3, 5, 5] heap
Code:
127 8 10 4 508 3 5 5 output[{6},{5},(({7})*(4))+({4}),{3}] += input[{6},{2},((({7})*(4))+({4}))+({1}),({3})+({0})] * weight[{5},{2},{1},{0}]
Compiling (/home/aivanov/codegen/microkernels/codegen/generated/f2af1e/)...
Compiling (/home/aivanov/codegen/microkernels/codegen/generated/f2af1e/)... DONE (0.255 s)
Running (native)...
Running (native)... DONE (11.077 s)
My time [ms]: avg 995.083857 std 0.234648
Name: conv2d
In: input, weight
Out: output
Declarations:
    input f32 [8, 3, 512, 512] heap
    output f32 [8, 10, 508, 508] heap
    weight f32 [10, 3, 5, 5] heap
Code:
127:p 8 10 4 508 3 5 5 output[{6},{5},(({7})*(4))+({4}),{3}] += input[{6},{2},((({7})*(4))+({4}))+({1}),({3})+({0})] * weight[{5},{2},{1},{0}]
Compiling (/home/aivanov/codegen/microkernels/codegen/generated/b919a7/)...
Compiling (/home/aivanov/codegen/microkernels/codegen/generated/b919a7/)... DONE (0.656 s)
Running (native)...
Running (native)... DONE (1.597 s)
My time [ms]: avg 130.072571 std 10.586931
