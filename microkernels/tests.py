import os

import numpy as np
import torch
import torchtune.modules
# from memory_profiler import profile

import gen
from kernels import *


def make_transformations_graph():
    program = create_layernorm_program()
    input_data = generate_input_for_program(program, {'B': 3, 'N': 4})
    output_data = ref_layernorm(input_data)
    ref_data = {**input_data, **output_data}

    # ref_program = copy.deepcopy(program)
    # ref_program.name = "ref_" + ref_program.name

    # reuse_buffers(program, 'diff', 'tmp2')
    # reuse_buffers(program, 'dst', 'src')
    # reuse_buffers(program, 'q1', 'qeps')
    # reuse_buffers(program, 'IN1', 'N1')
    # join_scopes(program, 'm#0')
    # join_scopes(program, 'q1')
    # join_scopes(program, 'm#0')
    
    # enable_ssr(program, 'm', 1) 
    # enable_ssr(program, 'diff', 1)
    # enable_ssr(program, 'dst', 1)
    # enable_ssr(program, 'd2', 1)
    # increase_ssr_depth(program, 'm', 1)
    # increase_ssr_depth(program, 'dst', 1)
    # increase_ssr_depth(program, 'd2', 1)
    # increase_ssr_depth(program, 'diff', 1)
    # merge_ssr(program, 'm', 1, 'd2', 1)
    # merge_ssr(program, 'diff', 1, 'dst', 1)
    # merge_ssr(program, 'm', 1, 'diff', 1)
    # activate_ssr(program, 'm', 1, 0)

    # banshee_snitch_code(program, ref_program)

    # return

    def run_transformation_group(transform_list, transform_chain, current_program):
        while True:
            transforms = []
            for transform_func, transform_check in transform_list:
                transforms += [(transform_func, tr_args) for tr_args in transform_check(current_program)]
            transforms += ["stop"]
            tr = random.choice(transforms)
            if tr == "stop":
                break
            # apply transform
            transform_func, transform_args = tr
            print(
                transform_func.__name__
                + "(program, "
                + ", ".join((f"'{x}'" if isinstance(x, str) else str(x)) for x in transform_args)
                + ")"
            )
            transform_func(current_program, *transform_args)
            transform_chain.append((len(transforms) - 1, tr))


    transforms_per_depth = []
    for _ in range(300):
        print('----------------------------------------------------')
        current_program = copy.deepcopy(program)
        current_program.name = "transformed_" + current_program.name
        transform_chain = []
        # high-level transformations
        run_transformation_group([
            (join_scopes, find_joinable_scopes),
            (reuse_arr_dims, find_reusable_arr_dims),
            (reuse_buffers, find_reusable_buffers),
        ], transform_chain, current_program)
        # SSR exploration
        run_transformation_group([
            (enable_ssr, find_applicable_ssr),
            (increase_ssr_depth, find_increasable_ssr_depth),
            (merge_ssr, find_mergeable_ssr),
        ], transform_chain, current_program)
        # SSR activation
        run_transformation_group([
            (activate_ssr, find_activatable_ssr),
        ], transform_chain, current_program)
        # FREP activation
        run_transformation_group([
            (enable_frep, find_applicable_frep),
        ], transform_chain, current_program)
        #print(transform_chain)
        # chain_str = "root"
        for i, (available_transforms, chosen_transform) in enumerate(transform_chain):
            if len(transforms_per_depth) <= i:
                transforms_per_depth.append([])
            transforms_per_depth[i].append(available_transforms)
            # chain_str += "->" + chosen_transform[0].__name__ + str(chosen_transform[1:])
        # chain_str += "->stop"
        cycles = test_snitch_code(current_program, ref_data)
        print("cycles:", cycles)

    avg_tpd = np.array(
        [round(mean(transform), 2) for transform in transforms_per_depth]
    )
    std_tpd = np.array(
        [
            round(stdev(transform) if len(transform) > 1 else 0, 2)
            for transform in transforms_per_depth
        ]
    )
    print("avg_tpd:", avg_tpd)
    print("std_tpd:", std_tpd)

    x = np.arange(len(avg_tpd))
    plt.plot(x, avg_tpd)
    plt.fill_between(x, avg_tpd - std_tpd, avg_tpd + std_tpd, alpha=0.2)
    plt.xlabel("# applied transformations")
    plt.ylabel("# available transformations")
    plt.savefig("avg_tpd.png")

    # while True:
    #     js = find_joinable_scopes(program)
    #     if not js:
    #         break
    #     x = js.pop()
    #     print(f"joining scopes {x}")
    #     join_scopes(program, x)

    # while True:
    #     js = find_reusable_arr_dims(program)
    #     if not js:
    #         break
    #     x, y = js.pop()
    #     print(f"reusing array dims arr={x} dim={y}")
    #     reuse_arr_dims(program, x, y)

    # while True:
    #     rb = find_reusable_buffers(program)
    #     if not rb:
    #         break
    #     x, y = rb.pop()
    #     print(f"reusing buffers {x}, {y}")
    #     reuse_buffers(program, x, y)

    # ref_program = copy.deepcopy(program)
    # ref_program.name = "ref_" + ref_program.name
    # print(program)
    # banshee_snitch_code(program, ref_program)



def apply_manual_logsoftmax_transformations(program):
    tile_buffer(program, "src", 0, 4)
    swap_nested_scopes(program, "max_val#1")
    swap_nested_scopes(program, "diff#1")
    swap_nested_scopes(program, "exp_diff_v0#1")
    swap_nested_scopes(program, "sum_val#1")
    swap_nested_scopes(program, "x#1")
    join_scopes(program, "diff#2")
    join_scopes(program, "diff#1")
    join_scopes(program, "diff#2")
    join_scopes(program, "diff#1")
    join_scopes(program, "diff#0")
    join_scopes(program, "diff#0")
    join_scopes(program, "x#2")
    swap_nested_scopes(program, "dst_rt2x#1")
    join_scopes(program, "x#1")
    
    split_scopes(program, "exp_diff_v0")
    split_scopes(program, "exp_diff_v1")
    split_scopes(program, "exp_diff_v2")
    split_scopes(program, "exp_diff_v3")
    split_scopes(program, "exp_diff_v4")
    split_scopes(program, "exp_diff_v5")
    split_scopes(program, "exp_diff_v6")
    split_scopes(program, "exp_diff_v7")
    split_scopes(program, "exp_diff_v8")
    split_scopes(program, "exp_diff_v9")
    split_scopes(program, "exp_diff_v10")
    split_scopes(program, "exp_diff")
    split_scopes(program, "sum_val")
    
    split_scopes(program, "dst_rt4x")
    split_scopes(program, "dst_rt3_4x")
    split_scopes(program, "dst_x_1")
    split_scopes(program, "dst_rt4x32")
    split_scopes(program, "dst_rt2x12")
    split_scopes(program, "dst_rt3_4x32")
    split_scopes(program, "dst_x7")
    split_scopes(program, "dst_d1")
    split_scopes(program, "dst_d2")
    split_scopes(program, "dst_d3")
    split_scopes(program, "dst_d4")
    split_scopes(program, "dst_x_1_90")
    split_scopes(program, "dst")
    apply_to_exhaustion(program, [
        (reuse_arr_dims, find_reusable_arr_dims),
        (reuse_buffers, find_reusable_buffers),
        (move_buf_to_stack, find_bufs_movable_to_stack),
    ])
    unroll_scope(program, 'max_val#0')
    enable_ssr(program, "max_val", 1)
    enable_ssr(program, "diff", 1)
    enable_ssr(program, "x", 1)
    enable_ssr(program, "dst", 0)
    apply_to_exhaustion(program, [
        (increase_ssr_depth, find_increasable_ssr_depth),
        (merge_ssr, find_mergeable_ssr),
        (activate_ssr, find_activatable_ssr),
        (enable_frep, find_applicable_frep),
    ])


def apply_manual_layernorm_transformations(program):
    tile_buffer(program, 'src', 0, 4)
    join_scopes(program, 'm#2',)
    join_scopes(program, 'm#2',)
    join_scopes(program, 'd2#2',)
    join_scopes(program, 'd2#2',)
    join_scopes(program, 'd2#2',)
    join_scopes(program, 'd2#2',)
    join_scopes(program, 'd2#2',)
    join_scopes(program, 'd2#2',)
    join_scopes(program, 'd2#2',)
    join_scopes(program, 'd2#2',)
    swap_nested_scopes(program, 'm#1',)
    swap_nested_scopes(program, 'diff#1',)
    swap_nested_scopes(program, 'd2#1',)
    swap_nested_scopes(program, 'q#1',)
    swap_nested_scopes(program, 'tmp#1',)
    swap_nested_scopes(program, 'tmp2#1',)
    swap_nested_scopes(program, 'dst#1',)
    swap_ops(program, 'tmp#1',)
    swap_ops(program, 'tmp#1',)
    swap_ops(program, 'tmp#1',)
    swap_ops(program, 'tmp#1',)
    join_scopes(program, 'd2#1',)
    join_scopes(program, 'q1#0',)
    join_scopes(program, 'q1#0',)
    join_scopes(program, 'q1#0',)
    join_scopes(program, 'tmp#1',)
    join_scopes(program, 'tmp#1',)
    reuse_buffers(program, 'diff', 'dst')
    reuse_buffers(program, 'src', 'dst')
    # apply_to_exhaustion(program, [
    #     (reuse_buffers, find_reusable_buffers),
    #     (reuse_arr_dims, find_reusable_arr_dims),
    #     (move_buf_to_stack, find_bufs_movable_to_stack),
    #     (unroll_scope, find_unrollable_scopes),
    # ])
    reuse_buffers(program, 'tmp', 'tmp2')
    reuse_arr_dims(program, 'tmp2', 2)
    unroll_scope(program, 'tmp2#0',)
    reuse_buffers(program, 'sigma', 'sqrtq')
    reuse_arr_dims(program, 'tmp2', 0)
    unroll_scope(program, 'tmp#0',)
    reuse_buffers(program, 'qeps', 'sqrtq')
    reuse_arr_dims(program, 'sqrtq', 0)
    unroll_scope(program, 'q1#0',)
    reuse_buffers(program, 'q', 'q1')
    reuse_arr_dims(program, 'q1', 0)
    unroll_scope(program, 'q#0',)
    reuse_buffers(program, 'q1', 'sqrtq')
    reuse_arr_dims(program, 'mu', 0)
    unroll_scope(program, 'mu#0',)
    reuse_buffers(program, 'mu', 'sqrtq')
    reuse_arr_dims(program, 'm', 0)
    unroll_scope(program, 'm#0',)
    reuse_buffers(program, 'm', 'sqrtq')
    reuse_arr_dims(program, 'd2', 2)
    unroll_scope(program, 'dst#0',)
    reuse_buffers(program, 'IN1', 'N1')
    reuse_arr_dims(program, 'd2', 0)
    unroll_scope(program, 'diff#0',)
    reuse_buffers(program, 'd2', 'tmp2')
    unroll_scope(program, 'd2#0',)
    apply_to_exhaustion(program, [ (move_buf_to_stack, find_bufs_movable_to_stack), ])
    apply_to_exhaustion(program, [ (enable_ssr, find_applicable_ssr), ])
    apply_to_exhaustion(program, [ (increase_ssr_depth, find_increasable_ssr_depth), ])
    apply_to_exhaustion(program, [ (merge_ssr, find_mergeable_ssr), ])
    # apply_to_exhaustion(program, [ (activate_ssr, find_activatable_ssr), ])
    activate_ssr(program, 'diff', 0, 2)
    activate_ssr(program, 'm', 1, 0)
    activate_ssr(program, 'dst', 0, 1)
    activate_ssr(program, 'dst', 2, 2)
    activate_ssr(program, 'd2', 1, 0)
    apply_to_exhaustion(program, [ (enable_frep, find_applicable_frep), ])



def apply_manual_softmax_transformations(program):    
    tile_buffer(program, "src", 0, 4)
    swap_nested_scopes(program, "max_val#1")
    swap_nested_scopes(program, "diff#1")
    swap_nested_scopes(program, "exp_diff_v0#1")
    swap_nested_scopes(program, "sum_val#1")
    swap_nested_scopes(program, "dst#1")
    join_scopes(program, "inv_sum_val#1")
    join_scopes(program, "diff#2")
    join_scopes(program, "diff#1")
    join_scopes(program, "diff#2")
    join_scopes(program, "diff#1")
    join_scopes(program, "diff#0")
    join_scopes(program, "diff#0")
    split_scopes(program, "exp_diff_v0")
    split_scopes(program, "exp_diff_v1")
    split_scopes(program, "exp_diff_v2")
    split_scopes(program, "exp_diff_v3")
    split_scopes(program, "exp_diff_v4")
    split_scopes(program, "exp_diff_v5")
    split_scopes(program, "exp_diff_v6")
    split_scopes(program, "exp_diff_v7")
    split_scopes(program, "exp_diff_v8")
    split_scopes(program, "exp_diff_v9")
    split_scopes(program, "exp_diff_v10")
    split_scopes(program, "exp_diff")
    split_scopes(program, "sum_val")
    apply_to_exhaustion(program, [
        (reuse_arr_dims, find_reusable_arr_dims),
        (reuse_buffers, find_reusable_buffers),
    ])
    move_buf_to_stack(program, 'exp_diff_v9')
    move_buf_to_stack(program, 'inv_sum_val')
    unroll_scope(program, 'max_val#0')
    unroll_scope(program, 'dst#0')
    enable_ssr(program, "max_val", 1)
    enable_ssr(program, "diff", 1)
    enable_ssr(program, "dst", 1)
    enable_ssr(program, "dst", 0)
    apply_to_exhaustion(program, [
        (increase_ssr_depth, find_increasable_ssr_depth),
        (merge_ssr, find_mergeable_ssr),
        (activate_ssr, find_activatable_ssr),
        (enable_frep, find_applicable_frep),
    ])


def apply_manual_mish_transformations(program):
    tile_buffer(program, 'X', 1, 4)
    splittable_scopes = find_splittable_scopes(program)
    for s in splittable_scopes:
        print(f"split_scopes(program, {args_as_str(s)})")
        split_scopes(program, *s)
    apply_to_exhaustion(program, [
        (reuse_arr_dims, find_reusable_arr_dims),
        (reuse_buffers, find_reusable_buffers),
        (move_buf_to_stack, find_bufs_movable_to_stack),
    ])
    apply_to_exhaustion(program, [(unroll_scope, find_unrollable_scopes)])
    # unroll_scope(program, 'Y_softplus_exp_v0#0')
    # apply_to_exhaustion(program, [ (enable_ssr, find_applicable_ssr), ])
    enable_ssr(program, 'Y', 0)
    enable_ssr(program, 'Y', 1)
    enable_ssr(program, 'Y_softplus_exp_v0', 1)
    apply_to_exhaustion(program, [ (increase_ssr_depth, find_increasable_ssr_depth), ])
    apply_to_exhaustion(program, [ (merge_ssr, find_mergeable_ssr), ])
    apply_to_exhaustion(program, [ (activate_ssr, find_activatable_ssr), ])


def apply_manual_batchnorm_transformations(program):
    # 70163
    # apply_to_exhaustion(program, [
    #     (join_scopes, find_joinable_scopes),
    #     (reuse_arr_dims, find_reusable_arr_dims),
    #     (reuse_buffers, find_reusable_buffers),
    #     (move_buf_to_stack, find_bufs_movable_to_stack),
    # ])
    # return
    
    tile_buffer(program, 'X', 1, 4)
    swap_nested_scopes(program, 'current_sum#2',)
    swap_nested_scopes(program, 'current_sum#1',)
    swap_nested_scopes(program, 'diff#2',)
    swap_nested_scopes(program, 'diff#1',)
    swap_nested_scopes(program, 'Ysb#2',)
    swap_nested_scopes(program, 'Ysb#1',)
    # swap_nested_scopes(program, 'diff2#2',)
    # swap_nested_scopes(program, 'diff2#1',)
    swap_nested_scopes(program, 'diff2_sum#2',)
    swap_nested_scopes(program, 'diff2_sum#1',)
    # swap_nested_scopes(program, 'Yb#2',)
    # swap_nested_scopes(program, 'Yb#1',)
    swap_nested_scopes(program, 'Y#2',)
    swap_nested_scopes(program, 'Y#1',)
    
    # moving channel dimension outside
    swap_nested_scopes(program, 'current_sum#4',)
    swap_nested_scopes(program, 'diff#4',)
    # swap_nested_scopes(program, 'diff2#4',)
    swap_nested_scopes(program, 'diff2_sum#4',)
    swap_nested_scopes(program, 'Ysb#4',)
    # swap_nested_scopes(program, 'Yb#4',)
    swap_nested_scopes(program, 'Y#4',)
        
    apply_to_exhaustion(program, [ (join_scopes, find_joinable_scopes), ])
    split_scopes(program, 'diff2_sum')
    split_scopes(program, 'diff2_sum#0')
    split_scopes(program, 'diff2_sum#1')
    split_scopes(program, 'diff2_sum#2')
    
    split_scopes(program, 'Y')
    split_scopes(program, 'Y#0')
    split_scopes(program, 'Y#1')
    split_scopes(program, 'Y#2')
    # swap_nested_scopes(program, 'current_sum#3',)
    # swap_nested_scopes(program, 'diff#3',)
    # swap_nested_scopes(program, 'Ysb#3',)
    reuse_buffers(program, "Ysb", "diff")
    reuse_buffers(program, "Y", "diff")
    apply_to_exhaustion(program, [
        (reuse_arr_dims, find_reusable_arr_dims),
        (reuse_buffers, find_reusable_buffers),
        (move_buf_to_stack, find_bufs_movable_to_stack),
        # (unroll_scope, find_unrollable_scopes),
    ])
    
    unroll_scope(program, 'current_sum#0')
    enable_ssr(program, 'current_sum', 1)
    unroll_scope(program, 'diff#0')
    enable_ssr(program, 'diff', 0)
    enable_ssr(program, 'diff', 1)
    unroll_scope(program, 'diff2_sum#0')
    enable_ssr(program, 'diff2_sum', 1)
    enable_ssr(program, 'diff2_sum', 2)
    # unroll_scope(program, 'Ysb#0')
    unroll_scope(program, 'Ysb#0')
    enable_ssr(program, 'Ysb', 1)
    enable_ssr(program, 'Ysb', 0)

    unroll_scope(program, 'Y#0')
    enable_ssr(program, 'Y', 1)
    enable_ssr(program, 'Y', 0)
    
    apply_to_exhaustion(program, [
        (increase_ssr_depth, find_increasable_ssr_depth),
        (merge_ssr, find_mergeable_ssr),
        (activate_ssr, find_activatable_ssr),
    ])
    apply_to_exhaustion(program, [
        (enable_frep, find_applicable_frep),
    ])


def apply_manual_softplus_transformations(program):
    tile_buffer(program, 'X', 1, 4)
    splittable_scopes = find_splittable_scopes(program)
    for s in splittable_scopes:
        print(f"split_scopes(program, {args_as_str(s)})")
        split_scopes(program, *s)
    apply_to_exhaustion(program, [
        (reuse_arr_dims, find_reusable_arr_dims),
        (reuse_buffers, find_reusable_buffers),
        (move_buf_to_stack, find_bufs_movable_to_stack),
    ])
    enable_ssr(program, 'Y_exp_v0', 1)
    enable_ssr(program, 'Y', 0)
    apply_to_exhaustion(program, [ (increase_ssr_depth, find_increasable_ssr_depth), ])
    apply_to_exhaustion(program, [ (merge_ssr, find_mergeable_ssr), ])
    apply_to_exhaustion(program, [ (activate_ssr, find_activatable_ssr), ])



def run_correctness_tests():
    # testing correctness
    for simulator in ["banshee", "rtl"]:
    # for simulator in ["rtl"]:
        optimized_gemm(simulator, apply_manual_gemm_transformations, check_correctness=True, size=[2, 8, 3])
        optimized_mish(simulator, greedy_opt)
        optimized_softplus(simulator, greedy_opt)
        optimized_gemm(simulator, greedy_opt)
        optimized_batchnorm(simulator, greedy_opt)
        # optimized_layernorm(simulator, apply_manual_layernorm_transformations)
        optimized_layernorm(simulator, greedy_opt)
        optimized_softmax(simulator, greedy_opt)
        # optimized_softmax(simulator, apply_manual_softmax_transformations)
        optimized_logsoftmax(simulator, greedy_opt)
        # optimized_logsoftmax(simulator, apply_manual_logsoftmax_transformations)


def test_transform_applicability():
    simulator = "rtl"
    for seed in range(0, 1000):
        print("Running with seed", seed)
        opt = lambda program: greedy_opt(program, seed=seed, remaining_random=20)
        # optimized_mish(simulator, opt)
        # optimized_softplus(simulator, opt)
        optimized_gemm(simulator, opt)
        optimized_batchnorm(simulator, opt)
        # optimized_layernorm(simulator, opt)
        optimized_softmax(simulator, opt)
        optimized_logsoftmax(simulator, opt)
    # optimized_layernorm("rtl", apply_manual_layernorm_transformations)


def run_performance_tests():
    # first, run correctness tests in banshee, then run performance tests in verilator
    # for simulator in ["rtl"]:
    for simulator in ["banshee", "rtl"]:
        check_correctness = simulator == "banshee"
        print("Mish (-snitch) 64x64")
        optimized_mish(simulator, greedy_opt_no_snitch_silent, check_correctness, size=[64, 64])
        print("Mish (+snitch) 64x64")
        optimized_mish(simulator, greedy_opt_snitch_silent, check_correctness, size=[64, 64])
        print("Softplus (-snitch) 64x64")
        optimized_softplus(simulator, greedy_opt_no_snitch_silent, check_correctness, size=[64, 64])
        print("Softplus (+snitch) 64x64")
        optimized_softplus(simulator, greedy_opt_snitch_silent, check_correctness, size=[64, 64])
        print("GEMM (-snitch) 16x16x16")
        optimized_gemm(simulator, greedy_opt_no_snitch_silent, check_correctness, size=[16, 16, 16])
        print("GEMM (+snitch) 16x16x16")
        optimized_gemm(simulator, greedy_opt_snitch_silent, check_correctness, size=[16, 16, 16])
        print("GEMM (manual) 16x16x16")
        optimized_gemm(simulator, apply_manual_gemm_transformations, check_correctness, size=[16, 16, 16])
        print("BatchNorm (-snitch) 8x8x8x8")
        optimized_batchnorm(simulator, greedy_opt_no_snitch_silent, check_correctness, size=[8, 8, 8, 8])
        print("BatchNorm (+snitch) 8x8x8x8")
        optimized_batchnorm(simulator, greedy_opt_snitch_silent, check_correctness, size=[8, 8, 8, 8])
        print("LayerNorm (-snitch) 64x64")
        optimized_layernorm(simulator, greedy_opt_no_snitch_silent, check_correctness, size=[64, 64])
        print("LayerNorm (+snitch) 64x64")
        optimized_layernorm(simulator, greedy_opt_snitch_silent, check_correctness, size=[64, 64])
        print("LayerNorm (manual) 64x64")
        optimized_layernorm(simulator, apply_manual_layernorm_transformations, check_correctness, size=[64, 64])
        print("Softmax (-snitch) 64x64")
        optimized_softmax(simulator, greedy_opt_no_snitch_silent, check_correctness, size=[64, 64])
        print("Softmax (+snitch) 64x64")
        optimized_softmax(simulator, greedy_opt_snitch_silent, check_correctness, size=[64, 64])
        print("LogSoftmax (-snitch) 64x64")
        optimized_logsoftmax(simulator, greedy_opt_no_snitch_silent, check_correctness, size=[64, 64])
        print("LogSoftmax (+snitch) 64x64")
        optimized_logsoftmax(simulator, greedy_opt_snitch_silent, check_correctness, size=[64, 64])




def run_performance_manual_tests():
    for simulator in ["banshee", "rtl"]:
        check_correctness = simulator == "banshee"
        
        # Mish 16x16
        sizes = {'B': 16, 'N': 16}
        print("Mish 16x16")
        def opt(program):
            specialize_inputs(program, sizes)
            apply_manual_mish_transformations(program)
        optimized_mish(simulator, opt, check_correctness, size=list(sizes.values()))
        
        # # Mish 64x64
        sizes = {'B': 64, 'N': 64}
        print("Mish 64x64")
        def opt(program):
            specialize_inputs(program, sizes)
            apply_manual_mish_transformations(program)
        optimized_mish(simulator, opt, check_correctness, size=list(sizes.values()))
        
        # GEMM
        sizes = {'M': 16, 'N': 16, 'K': 16}
        print(f"GEMM {sizes['M']}x{sizes['N']}x{sizes['K']}")
        optimized_gemm(simulator, apply_manual_gemm_transformations, check_correctness=True, size=list(sizes.values()))

        # LayerNorm 16x16
        sizes = {'B': 16, 'N': 16}
        print(f"LayerNorm {sizes['B']}x{sizes['N']}")
        def opt(program):
            specialize_inputs(program, sizes)
            apply_manual_layernorm_transformations(program)
        optimized_layernorm(simulator, opt, check_correctness, size=list(sizes.values()))

        # LayerNorm 64x64
        sizes = {'B': 64, 'N': 64}
        print(f"LayerNorm {sizes['B']}x{sizes['N']}")
        def opt(program):
            specialize_inputs(program, sizes)
            apply_manual_layernorm_transformations(program)
        optimized_layernorm(simulator, opt, check_correctness, size=list(sizes.values()))
        
        # BatchNorm 8x8x8x8
        sizes = {'N': 8, 'C': 8, 'H': 8, 'W': 8}
        print("BatchNorm 8x8x8x8")
        def opt(program):
            specialize_inputs(program, sizes)
            apply_manual_batchnorm_transformations(program)
        optimized_batchnorm(simulator, opt, check_correctness, size=list(sizes.values()))
        
        # Softmax 16x16
        sizes = {'B': 16, 'N': 16}
        print("Softmax 16x16")
        def opt(program):
            specialize_inputs(program, sizes)
            apply_manual_softmax_transformations(program)
        optimized_softmax(simulator, opt, check_correctness, size=list(sizes.values()))
        
        # Softplus 16x16
        sizes = {'B': 16, 'N': 16}
        print("Softplus 16x16")
        def opt(program):
            specialize_inputs(program, sizes)
            apply_manual_softplus_transformations(program)
        optimized_softplus(simulator, opt, check_correctness, size=list(sizes.values()))
        
        # LogSoftmax 16x16
        sizes = {'B': 16, 'N': 16}
        print("LogSoftmax 16x16")
        def opt(program):
            specialize_inputs(program, sizes)
            apply_manual_logsoftmax_transformations(program)
        optimized_logsoftmax(simulator, opt, check_correctness, size=list(sizes.values()))


def measure_optimized_gemm_program():
    program_text = """
    Name: gemm
    In: A, B, C, alpha, beta
    Out: D
    Declarations:
        A f64 [1000, 1000] heap
        B f64 [1000, 960] heap
        C f64 [1000, 960] heap
        D f64 [1000, 960] heap
        alpha_acc f64 [250:N, 4:N, 8:N, 10:N, 3:N, 4:N] stack
        acc f64 [250, 4, 8, 10, 3, 4] heap
        alpha f64 [] heap
        beta f64 [] heap
    Code:
      5 8 250 10 200 4 3 4 acc[{5},{2},{6},{4},{1},{0}] += A[(({5})*(4))+({2}),(({7})*(200))+({3})] * B[(({7})*(200))+({3}),(({6})*(120))+((({4})*(12))+((({1})*(4))+({0})))]
    250 4   8 10   3 4     alpha_acc[{5},{4},{3},{2},{1},{0}] = alpha * acc[{5},{4},{3},{2},{1},{0}]
      | |   |  |   | |     D[(({5})*(4))+({4}),(({3})*(120))+((({2})*(12))+((({1})*(4))+({0})))] = + beta * C[(({5})*(4))+({4}),(({3})*(120))+((({2})*(12))+((({1})*(4))+({0})))] + alpha_acc[{5},{4},{3},{2},{1},{0}]
    """ # 58%
    program_text = """
    Name: gemm
    In: A, B, C, alpha, beta
    Out: D
    Declarations:
        A f64 [1000, 1000] heap
        B f64 [1000, 960] heap
        C f64 [1000, 960] heap
        D f64 [1000, 960] heap
        alpha_acc f64 [250:N, 4:N, 8:N, 10:N, 3:N, 4:N] stack
        acc f64 [250, 4, 8, 10, 3, 4] heap
        alpha f64 [] heap
        beta f64 [] heap
    Code:
    #                   L3_total    L3_per_core         L2          L1
    # total     24M          10M           4.2M       200K       25.6K
    # A          8M         1.6M           1.6M       6.4K        6.4K    
    # B          8M         1.6M           1.6M       192K       19.2K
    # C          8M        7.68M          0.96M      3.84K        384B
    # 1000x960x1000 1000x960x200   1000x120x200  4x120x200    4x12x200 4x12x1 1x12x1 1x4x1
    #             K            N              M          N           K      M      N     N
                  5            8            250         10         200      4      3     4 acc[{5},{2},{6},{4},{1},{0}] += A[{5}*4+{2},{7}*200+{3}] * B[{7}*200+{3},{6}*120+{4}*12+{1}*4+{0}]
                250            4              8         10           3      4              alpha_acc[{5},{4},{3},{2},{1},{0}] = alpha * acc[{5},{4},{3},{2},{1},{0}]
                  |            |              |          |           |      |              D[{5}*4+{4},{3}*120+{2}*12+{1}*4+{0}] = + beta * C[{5}*4+{4},{3}*120+{2}*12+{1}*4+{0}] + alpha_acc[{5},{4},{3},{2},{1},{0}]
    """ # 58%
    # program_text = """
    # Name: gemm
    # In: A, B, C, alpha, beta
    # Out: D
    # Declarations:
    #     A f64 [1000, 1000] heap
    #     B f64 [1000, 960] heap
    #     C f64 [1000, 960] heap
    #     D f64 [1000, 960] heap
    #     alpha_acc f64 [250:N, 4:N, 8:N, 10:N, 3:N, 4:N] stack
    #     acc1 f64 [250:N, 8:N, 5:N, 10:N, 4, 3, 4] stack
    #     acc f64 [250:N, 8:N, 10, 4, 3, 4] heap
    #     alpha f64 [] heap
    #     beta f64 [] heap
    # Code:
    # #       M      N      K      N      K      M      N      N
    #       250      8      5     10    200      4      3      4  acc1[{7},{6},{5},{4},{2},{1},{0}] += A[{7}*4+{2},{5}*200+{3}] * B[{5}*200+{3},{6}*120+{4}*12+{1}*4+{0}]
    # #       M      N      K      N      M      N      N
    #         |      |      |      |      4      3      4         acc[{6},{5},{3},{2},{1},{0}] += acc1[{6},{5},{4},{3},{2},{1},{0}]
    # #       M      N      M      N      N      N
    #         |      |      4     10      3      4                alpha_acc[{5},{3},{4},{2},{1},{0}] = alpha * acc[{5},{4},{2},{3},{1},{0}]
    #         |      |      |      |      |      |                D[{5}*4+{3},{4}*120+{2}*12+{1}*4+{0}] = + beta * C[{5}*4+{3},{4}*120+{2}*12+{1}*4+{0}] + alpha_acc[{5},{3},{4},{2},{1},{0}]
    # """ # 36%
    # program_text = """
    # Name: gemm
    # In: A, B, C, alpha, beta
    # Out: D
    # Declarations:
    #     A f64 [1000, 1000] heap
    #     B f64 [1000, 960] heap
    #     C f64 [1000, 960] heap
    #     D f64 [1000, 960] heap
    #     alpha_acc f64 [250:N, 4:N, 8:N, 10:N, 3:N, 4:N] stack
    #     acc f64 [250, 8, 4, 10, 3, 4] heap
    #     alpha f64 [] heap
    #     beta f64 [] heap
    # Code:
    # #       M      N      K      N      K      M      N      N
    #       250      8      5     10    200      4      3      4  acc[{7},{6},{2},{4},{1},{0}] += A[{7}*4+{2},{5}*200+{3}] * B[{5}*200+{3},{6}*120+{4}*12+{1}*4+{0}]
    # #       M      N      M      N      N      N
    #       250      8      4     10      3      4                alpha_acc[{5},{3},{4},{2},{1},{0}] = alpha * acc[{5},{4},{3},{2},{1},{0}]
    #         |      |      |      |      |      |                D[{5}*4+{3},{4}*120+{2}*12+{1}*4+{0}] = + beta * C[{5}*4+{3},{4}*120+{2}*12+{1}*4+{0}] + alpha_acc[{5},{3},{4},{2},{1},{0}]
    # """ # 18%
    program_text = """
    Name: gemm
    In: A, B, C, alpha, beta
    Out: D
    Declarations:
        A f64 [1000, 1000] heap
        B f64 [1000, 960] heap
        C f64 [1000, 960] heap
        D f64 [1000, 960] heap
        alpha_acc f64 [250:N, 4:N, 8:N, 10:N, 3:N, 4:N] stack
        acc1 f64 [5:N, 250, 4, 8:N, 10:N, 3, 4] stack
        acc f64 [250, 4, 8, 10, 3, 4] heap
        alpha f64 [] heap
        beta f64 [] heap
    Code:
    #             K            N              M          N           K      M      N     N
                  5            8          250:p         10         200      4      3     4 acc1[{7},{5},{2},{6},{4},{1},{0}] += A[{5}*4+{2},{7}*200+{3}] * B[{7}*200+{3},{6}*120+{4}*12+{1}*4+{0}]
                  |            |              |          |           1      4      3     4 acc[{5},{2},{6},{4},{1},{0}] += acc1[{7},{5},{2},{6},{4},{1},{0}]
              250:p            4              8         10           3      4              alpha_acc[{5},{4},{3},{2},{1},{0}] = alpha * acc[{5},{4},{3},{2},{1},{0}]
                  |            |              |          |           |      |              D[{5}*4+{4},{3}*120+{2}*12+{1}*4+{0}] = + beta * C[{5}*4+{4},{3}*120+{2}*12+{1}*4+{0}] + alpha_acc[{5},{4},{3},{2},{1},{0}]
    """ # 58%

    program = parse_program(program_text, invert_indices=False)
    print(program.text())
    input_data = generate_input_for_program(program, {
        "alpha": 0.9, "beta": 0.1,
    })
    output_data = ref_gemm(input_data)
    seconds = test_native_code(program, {**input_data, **output_data}, check_correctness=True, min_reps=10, silent=False, remove_tmp_files=False)
    return seconds


def run_native_correctness_tests():
    sizes = {'M': 1000, 'N': 960, 'K': 1000}
    # sizes = {'M': 1024, 'N': 900, 'K': 1024}
    print(f"GEMM {sizes['M']}x{sizes['N']}x{sizes['K']}")
    def opt(program):
        # print(program.text())

        join_scopes(program, 'alpha_acc#1')
        join_scopes(program, 'alpha_acc#0')
        
        tile_K = 200
        tile_M = 4
        in_vec_N = 4  # avx256 / fp64
        tile_N = 3 * in_vec_N
        out_tile_N = tile_N * 10
        assert tile_K * out_tile_N * 8 < 512 * 1024  # tile of B matrix tile_K * out_tile_N should fit L2 (~512K)
        assert (tile_M * tile_K + tile_M * out_tile_N) * 8 < 32 * 1024  # tile of A and C matrix should fit L1 (~32K)
        assert tile_M * (tile_N // in_vec_N) < 16  # num of avx256 regs

        tile_scope(program, "acc#2", tile_M)
        tile_scope(program, "alpha_acc#1", tile_M)

        tile_scope(program, "acc#1", out_tile_N)

        tile_scope(program, "alpha_acc#0", out_tile_N)

        tile_scope(program, "acc#1", tile_N)
        tile_scope(program, "alpha_acc#0", tile_N)

        tile_scope(program, "acc#1", in_vec_N)
        tile_scope(program, "alpha_acc#0", in_vec_N)

        tile_scope(program, "acc#0", tile_K)

        # tile_buffer(program, "A", 0, tile_M)
        # tile_buffer(program, "B", 1, out_tile_N)
        # tile_buffer(program, "B", 2, tile_N)
        # tile_buffer(program, "B", 3, in_vec_N)
        # tile_buffer(program, "A", 2, tile_K)
        apply_to_exhaustion(program, [(tile_buffer, find_tileable_buffers)])  
        
        # now loop nest is:
        # M M N N N N K K
        # we want to turn it into:
        # K N M N K M N N
        # 7 6 5 4 3 2 1 0
        # swaps: 
        # 2 1 :     M M N N N K K N
        # 3 2 :     M M N N K K N N
        # 6 5 4 3 : M N N K K M N N
        # 5 :       M N K N K M N N
        # 7 6 :     N K M N K M N N
        # 7 :       K N M N K M N N

        swap_nested_scopes(program, 'acc#2')
        swap_nested_scopes(program, 'acc#1')
        swap_nested_scopes(program, 'acc#3')
        swap_nested_scopes(program, 'acc#2')
        swap_nested_scopes(program, 'acc#6')
        swap_nested_scopes(program, 'acc#5')
        swap_nested_scopes(program, 'acc#4')
        swap_nested_scopes(program, 'acc#3')
        swap_nested_scopes(program, 'acc#5')
        swap_nested_scopes(program, 'acc#7')
        swap_nested_scopes(program, 'acc#6')
        swap_nested_scopes(program, 'acc#7')
                     
        apply_to_exhaustion(program, [
            (reuse_arr_dims, find_reusable_arr_dims),
            (reuse_buffers, find_reusable_buffers),
            (move_buf_to_stack, lambda r: find_bufs_movable_to_stack(r, 1024)),
        ])
        print(program.text())
    seconds_list = optimized_gemm_native(opt, check_correctness=False, size=list(sizes.values()))
    # seconds_list = measure_optimized_gemm_program()

    A = np.random.randn(sizes['M'], sizes['K'])
    B = np.random.randn(sizes['K'], sizes['N'])
    C = np.random.randn(sizes['M'], sizes['N'])
    alpha = 0.2
    beta = 0.8
    ref_seconds_list = timeit.repeat('D = scipy.linalg.blas.dgemm(alpha, A, B, beta, C)', globals={**globals(), **locals()}, number=1, repeat=10)
    seconds_list = [round(x, 6) for x in seconds_list]
    ref_seconds_list = [round(x, 6) for x in ref_seconds_list]
    print(f"Reference seconds: {ref_seconds_list}")
    print(f"Codegen   seconds: {seconds_list}")
    warmup = 3
    ref_seconds_list = ref_seconds_list[warmup:]
    seconds_list = seconds_list[warmup:]
    ref_seconds = np.mean(ref_seconds_list)
    seconds = np.mean(seconds_list)
    print(f"Reference seconds (avg): {ref_seconds:.6f}")
    print(f"Codegen   seconds (avg): {seconds:.6f}")
    print(f"% of reference: {100 * ref_seconds / seconds:.2f}")


def llamacpp_gemm(size, num_threads=1):
    result = subprocess.run(
        ["./benchmark-matmult", "-x", str(size[2]), "-y", str(size[0]), "-z", str(size[1]), "-t", str(num_threads)],
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
        universal_newlines=True
    )
    seconds = re.findall(r"uSec\s(\d+)", result.stdout)
    if not seconds:
        raise Exception("Could not find elapsed seconds")
    return np.array([float(s) for s in seconds]) * 1e-6


def optimized_rmsnorm_x86_f32(transform, check_correctness=True, size=[2, 3]):
    program = create_rmsnorm_program_f32()
    transform(program)
    input_data = generate_input_for_program(program, {})
    output_data = ref_rmsnorm(input_data)
    seconds = test_native_code(program, {**input_data, **output_data}, check_correctness, min_reps=1000000)
    return seconds


def optimized_rmsnorm_x86_f32avx(transform, check_correctness=True, size=[2, 3, 4]):
    program = create_rmsnorm_program_f32avx()
    input_data = generate_input_for_program(program, {'B': size[0], 'N': size[1], 'Ndiv8': size[2], 'eps': 1e-6})
    transform(program)
    output_data = ref_rmsnorm(input_data)
    seconds = test_native_code(program, {**input_data, **output_data}, check_correctness, min_reps=100)
    return seconds


def torchtune_rms_norm(x: torch.Tensor, eps: float = 1e-6):
    """
    Implements Root Mean Square Normalization introduced in
    https://arxiv.org/pdf/1910.07467.pdf.

    Reference implementation (used for correctness verfication)
    can be found here:
    https://github.com/facebookresearch/llama/blob/main/llama/model.py

    Args:
        x (torch.Tensor): input tensor
        eps (float): small value to avoid division by zero. Default: 1e-6
    """
    x_fp32 = x.float()
    x_normed = (
            x_fp32 * torch.rsqrt(x_fp32.pow(2).mean(-1, keepdim=True) + eps)
    ).type_as(x)
    return x_normed

def test_f32_rmsnorm():
    sizes = {'B': 1, 'N': 4096}

    def opt(program):
        specialize_inputs(program, sizes)
        apply_to_exhaustion(program, [
            (split_scopes, find_splittable_scopes),])
        vectorize_program(program, 256)
        apply_to_exhaustion(program, [
            (join_scopes, find_joinable_scopes),
            (reuse_arr_dims, find_reusable_arr_dims),
            (reuse_buffers, find_reusable_buffers),
            (move_buf_to_stack, find_bufs_movable_to_stack),
        ])
        print(program.text())
        # join_scopes(program,'means#0')
        # parallelize_scope(program, 'sums#1')
        # print(find_reusable_buffers(program))

    seconds_list = optimized_rmsnorm_x86_f32(opt, check_correctness=True, size=list(sizes.values()))
    A = torch.tensor(np.random.randn(sizes['B'], sizes['N']), dtype=torch.float32, device='cpu')
    ref_seconds_list = timeit.repeat('D = torchtune_rms_norm(A)',
                                     globals={**globals(), **locals()}, number=1000, repeat=1000)
    seconds_list = [round(x, 6) for x in seconds_list]
    ref_seconds_list = [round(x, 6) for x in ref_seconds_list]
    print(f"Reference seconds: {ref_seconds_list}")
    print(f"Codegen   seconds: {seconds_list}")
    warmup = 3
    ref_seconds_list = ref_seconds_list[warmup:]
    ref_seconds_list = np.array(ref_seconds_list) / 1000
    ref_us_list = ref_seconds_list * 1000000
    seconds_list = seconds_list[warmup:]
    us_list = np.array(seconds_list) * 1000000
    ref_us = np.mean(ref_us_list)
    us = np.mean(us_list)
    ref_std = np.std(ref_us_list)
    codegen_std = np.std(us_list)
    print(f"Reference uS (avg): {ref_us:.6f}")
    print(f"Reference uS (std): {ref_std:.6f}")
    print(f"Codegen   uS (avg): {us:.6f}")
    print(f"Codegen   uS (std): {codegen_std:.6f}")
    print(f"% of reference: {100 * ref_us / us:.2f}")

def test_f32avx_rmsnorm():
    n = 4096
    sizes = {'B': 1, 'N': n, 'Ndiv8': n // 8}

    def opt(program):
        specialize_inputs(program, sizes)
        apply_to_exhaustion(program, [
            (reuse_arr_dims, find_reusable_arr_dims),
            (reuse_buffers, find_reusable_buffers),
            (move_buf_to_stack, find_bufs_movable_to_stack),
        ])

    seconds_list = optimized_rmsnorm_x86_f32avx(opt, check_correctness=False, size=list(sizes.values()))
    A = torch.tensor(np.random.randn(sizes['B'], sizes['N']), dtype=torch.float32, device='cpu')
    ref_seconds_list = timeit.repeat('D = torchtune_rms_norm(A)',
                                     globals={**globals(), **locals()}, number=1000, repeat=1000)
    seconds_list = [round(x, 6) for x in seconds_list]
    ref_seconds_list = [round(x, 6) for x in ref_seconds_list]
    print(f"Reference seconds: {ref_seconds_list}")
    print(f"Codegen   seconds: {seconds_list}")
    warmup = 3
    ref_seconds_list = ref_seconds_list[warmup:]
    ref_seconds_list = np.array(ref_seconds_list) / 1000
    ref_us_list = ref_seconds_list * 1000000
    seconds_list = seconds_list[warmup:]
    us_list = np.array(seconds_list) * 1000000
    ref_us = np.mean(ref_us_list)
    us = np.mean(us_list)
    ref_std = np.std(ref_us_list)
    codegen_std = np.std(us_list)
    print(f"Reference uS (avg): {ref_us:.6f}")
    print(f"Reference uS (std): {ref_std:.6f}")
    print(f"Codegen   uS (avg): {us:.6f}")
    print(f"Codegen   uS (std): {codegen_std:.6f}")
    print(f"% of reference: {100 * ref_us / us:.2f}")


def test_f32_softmax():
    sizes = {'B': 64, 'N': 32}
    def opt(program):
        specialize_inputs(program, sizes)
        apply_to_exhaustion(program, [
            (split_scopes, find_splittable_scopes),])
        vectorize_program(program, 256)
        apply_to_exhaustion(program, [
            (join_scopes, find_joinable_scopes),
            (reuse_arr_dims, find_reusable_arr_dims),
            (reuse_buffers, find_reusable_buffers),
            (move_buf_to_stack, find_bufs_movable_to_stack),
        ])
        # join_scopes(program, 'max_val#1')
        # join_scopes(program, 'max_val#1')
        # join_scopes(program, 'max_val#1')
        # join_scopes(program, 'max_val#1')
        # join_scopes(program, 'max_val#1')
        # parallelize_scope(program, 'max_val#1')

    seconds_list = optimized_softmax_x86_f32(opt, check_correctness=True, size=list(sizes.values()))
    A = torch.tensor(np.random.randn(sizes['B'], sizes['N']), dtype=torch.float32, device='cpu')
    ref_seconds_list = timeit.repeat('D = torch.softmax(A, dim=1)',
                                     globals={**globals(), **locals()}, number=1000, repeat=1000)
    seconds_list = [round(x, 6) for x in seconds_list]
    ref_seconds_list = [round(x, 6) for x in ref_seconds_list]
    print(f"Reference seconds: {ref_seconds_list}")
    print(f"Codegen   seconds: {seconds_list}")
    warmup = 3
    ref_seconds_list = ref_seconds_list[warmup:]
    ref_seconds_list = np.array(ref_seconds_list) / 1000
    ref_us_list = ref_seconds_list * 1000000
    seconds_list = seconds_list[warmup:]
    us_list = np.array(seconds_list) * 1000000
    ref_us = np.mean(ref_us_list)
    us = np.mean(us_list)
    ref_std = np.std(ref_us_list)
    codegen_std = np.std(us_list)
    print(f"Reference uS (avg): {ref_us:.6f}")
    print(f"Reference uS (std): {ref_std:.6f}")
    print(f"Codegen   uS (avg): {us:.6f}")
    print(f"Codegen   uS (std): {codegen_std:.6f}")
    print(f"% of reference: {100 * ref_us / us:.2f}")


def test_f32avx_softmax():
    n = 32
    sizes = {'B': 64, 'N': n, 'Ndiv8': n // 8}

    def opt(program):
        specialize_inputs(program, sizes)
        swap_ops(program, 'inv_sum_val_inv_d2')
        swap_ops(program, 'inv_sum_val_inv_d2')
        swap_ops(program, 'inv_sum_val_inv_d2')
        swap_ops(program, 'inv_sum_val_inv_d2')
        swap_ops(program, 'inv_sum_val_inv_d2')
        swap_ops(program, 'inv_sum_val_inv_d2')
        split_scopes(program, 'inv_sum_val_inv_d2')

        apply_to_exhaustion(program, [
            (reuse_arr_dims, find_reusable_arr_dims),
            (reuse_buffers, find_reusable_buffers),
            (move_buf_to_stack, find_bufs_movable_to_stack),
        ])

        parallelize_scope(program, "src_vec#0")
        parallelize_scope(program, "diff#0")
        parallelize_scope(program, "exp_diff#0")
        parallelize_scope(program, "max_val_vec#0")
        parallelize_scope(program, "sum_val_reduced#0")
        parallelize_scope(program, "dst_vec#0")
        parallelize_scope(program, "dst#0")
        print(find_parallelizable_scopes(program))


    seconds_list = optimized_softmax_x86_f32avx(opt, check_correctness=False, size=list(sizes.values()))
    A = torch.tensor(np.random.randn(sizes['B'], sizes['N']), dtype=torch.float32, device='cpu')
    ref_seconds_list = timeit.repeat('D = torch.softmax(A, dim=1)',
                                     globals={**globals(), **locals()}, number=1000, repeat=1000)
    seconds_list = [round(x, 6) for x in seconds_list]
    ref_seconds_list = [round(x, 6) for x in ref_seconds_list]
    print(f"Reference seconds: {ref_seconds_list}")
    print(f"Codegen   seconds: {seconds_list}")
    warmup = 3
    ref_seconds_list = ref_seconds_list[warmup:]
    ref_seconds_list = np.array(ref_seconds_list) / 1000
    ref_us_list = ref_seconds_list * 1000000
    seconds_list = seconds_list[warmup:]
    us_list = np.array(seconds_list) * 1000000
    ref_us = np.mean(ref_us_list)
    us = np.mean(us_list)
    ref_std = np.std(ref_us_list)
    codegen_std = np.std(us_list)
    print(f"Reference uS (avg): {ref_us:.6f}")
    print(f"Reference uS (std): {ref_std:.6f}")
    print(f"Codegen   uS (avg): {us:.6f}")
    print(f"Codegen   uS (std): {codegen_std:.6f}")
    print(f"% of reference: {100 * ref_us / us:.2f}")


def optimized_add_x86_f32avx(transform, check_correctness, size):
    program = create_add_program_f32avx()
    input_data = generate_input_for_program(program, {'B': size[0], 'N': size[1], 'Ndiv8': size[2]})
    transform(program)
    output_data = ref_add_program(input_data)
    seconds = test_native_code(program, {**input_data, **output_data}, check_correctness, min_reps=1000, min_seconds=5)
    return seconds


def test_f32avx_sum():
    n = 128256
    sizes = {'B': 64, 'N': n, 'Ndiv8': n // 8}

    def opt(program):
        specialize_inputs(program, sizes)

        pass


    seconds_list = optimized_add_x86_f32avx(opt, check_correctness=False, size=list(sizes.values()))
    A = np.random.randn(sizes['B'], sizes['N']).astype(np.float32)
    B = np.random.randn(sizes['B'], sizes['N']).astype(np.float32)
    ref_seconds_list = timeit.repeat('D = A + B',
                                     globals={**globals(), **locals()}, number=10, repeat=100)
    seconds_list = [round(x, 6) for x in seconds_list]
    ref_seconds_list = [round(x, 6) for x in ref_seconds_list]
    print(f"Reference seconds: {ref_seconds_list}")
    print(f"Codegen   seconds: {seconds_list}")
    warmup = 3
    ref_seconds_list = ref_seconds_list[warmup:]
    ref_seconds_list = np.array(ref_seconds_list) / 10
    ref_us_list = ref_seconds_list * 1000000
    seconds_list = seconds_list[warmup:]
    us_list = np.array(seconds_list) * 1000000
    ref_us = np.mean(ref_us_list)
    us = np.mean(us_list)
    ref_std = np.std(ref_us_list)
    codegen_std = np.std(us_list)
    print(f"Reference uS (avg): {ref_us:.6f}")
    print(f"Reference uS (std): {ref_std:.6f}")
    print(f"Codegen   uS (avg): {us:.6f}")
    print(f"Codegen   uS (std): {codegen_std:.6f}")
    print(f"% of reference: {100 * ref_us / us:.2f}")

def run_x86_f32_correctness_tests():
    # test_f32_gemm()
    # test_f16_gemm()
    # test_f16_gemm_transformations()
    # test_f32_softmax()
    test_f32_rmsnorm()
    # test_f32avx_sum()
    # test_f32avx_softmax()
    # test_f32avx_rmsnorm()

def test_f32_gemm():
    # sizes = {'M': 1, 'N': 1024, 'K': 512}
    sizes = {'M': 512, 'N': 11008, 'K': 4096}
    print(f"GEMM {sizes['M']}x{sizes['N']}x{sizes['K']}")

    def opt(program):
        tile_size = 64

        tile_scope(program, 'acc#1', tile_size)
        tile_scope(program, 'alpha_acc#0', tile_size)
        tile_scope(program, 'D#0', tile_size)
        swap_nested_scopes(program, 'acc#1')
        join_scopes(program, 'acc#3')
        join_scopes(program, 'acc#3')
        # parallelize_scope(program, 'acc#2')
        # parallelize_scope(program, 'D#1')
        # parallelize_scope(program, 'alpha_acc#1')
        print(find_parallelizable_scopes(program))

    seconds_list = optimized_gemm_x86_f32(opt, check_correctness=False, size=list(sizes.values()))

    # llamacpp_seconds_list = llamacpp_gemm(size=list(sizes.values()), num_threads=3)
    A = np.random.randn(sizes['M'], sizes['K']).astype(np.float32, order='F')
    B = np.random.randn(sizes['K'], sizes['N']).astype(np.float32, order='F')
    C = np.random.randn(sizes['M'], sizes['N']).astype(np.float32, order='F')
    alpha = np.float32(0.2)
    beta = np.float32(0.8)
    ref_seconds_list = timeit.repeat('D = scipy.linalg.blas.sgemm(alpha, A, B, beta, C)',
                                     globals={**globals(), **locals()}, number=1, repeat=10)
    seconds_list = [round(x, 6) for x in seconds_list]
    ref_seconds_list = [round(x, 6) for x in ref_seconds_list]
    print(f"Reference seconds: {ref_seconds_list}")
    print(f"Codegen   seconds: {seconds_list}")
    # print(f"Llama.cpp seconds: {llamacpp_seconds_list}")
    warmup = 3
    # llamacpp_seconds_list = llamacpp_seconds_list[warmup:]
    ref_seconds_list = ref_seconds_list[warmup:]
    seconds_list = seconds_list[warmup:]
    ref_seconds = np.mean(ref_seconds_list)
    # llamacpp_sec = np.mean(llamacpp_seconds_list)
    seconds = np.mean(seconds_list)
    print(f"Reference seconds (avg): {ref_seconds:.6f}")
    print(f"Codegen   seconds (avg): {seconds:.6f}")
    # print(f"Llama.cpp seconds (avg): {llamacpp_sec:.6f}")
    print(f"% of reference: {100 * ref_seconds / seconds:.2f}")
    # print(f"% of llama.cpp: {100 * llamacpp_sec / seconds:.2f}")


def optimized_gemm_x86_f16():
    program_text = """
        Name: gemm
        In: A, B, C, alpha, beta
        Out: D
        Declarations:
            A f16 [512, 4096] heap
            B f16 [4096, 14336] heap
            C f16 [512, 14336] heap
            D f16 [512, 14336] heap
            a_packed f16 [56:N, 16:N, 2:N, 65536] heap
            b_packed f16 [56:N, 16:N, 65536] heap
            avec h512 [56:N, 16:N, 2:N, 16:N, 8:N, 16:N] stack
            bvec h512 [56:N, 16:N, 2:N, 16:N, 8:N] stack
            alpha_vec h512 [56:N, 16:N, 2:N, 16:N, 8:N, 16:N] stack
            C_block_vec h512 [56:N, 16:N, 2:N, 16:N, 8:N, 256:N, 16, 1] stack
            Cb_alpha_vec h512 [56:N, 16:N, 2:N, 16:N, 8:N, 256:N, 16, 1] stack
            Cb_alpha_s f16 [56:N, 16:N, 2:N, 16:N, 8:N, 256, 16, 32] stack
            alpha f16 [] heap
            beta f16 [] heap
        Code:
        56    16  16:p   256    16                   b_packed[{4}, {3}, {2} * 16 * 256 + {1} * 16 + {0}] = B[{3} * 256 + {1}, {4} * 256 + {0} + {2} * 16]
         |     |     2   8:p   256     32            a_packed[{5}, {4}, {3}, {2}*32*256 + {1}*32 + {0}] = A[{3}*256+{2}*32+{0},{4}*256 + {1}]
         |     |     |  16:p     8    256  16        bvec[{6}, {5}, {4}, {3}, {2}, {1}, {0}] = _mm512_set1_ph(b_packed[{6}, {5}, {3} * 16 * 256 + {1} * 16 + {0}])
         |     |     |     |     |      |   |    1   avec[{7}, {6}, {5}, {4}, {3}, {2}, {1}, {0}] = _mm512_set_ph(a_packed[{7}, {6}, {5}, {3}*32*256 + {1}*32 + {0}*32],a_packed[{7}, {6}, {5}, {3}*32*256 + {1}*32 + {0}*32+1],a_packed[{7}, {6}, {5}, {3}*32*256 + {1}*32 + {0}*32+2],a_packed[{7}, {6}, {5}, {3}*32*256 + {1}*32 + {0}*32+3],a_packed[{7}, {6}, {5}, {3}*32*256 + {1}*32 + {0}*32+4],a_packed[{7}, {6}, {5}, {3}*32*256 + {1}*32 + {0}*32+5],a_packed[{7}, {6}, {5}, {3}*32*256 + {1}*32 + {0}*32+6],a_packed[{7}, {6}, {5}, {3}*32*256 + {1}*32 + {0}*32+7],a_packed[{7}, {6}, {5}, {3}*32*256 + {1}*32 + {0}*32+8],a_packed[{7}, {6}, {5}, {3}*32*256 + {1}*32 + {0}*32+9],a_packed[{7}, {6}, {5}, {3}*32*256 + {1}*32 + {0}*32+10],a_packed[{7}, {6}, {5}, {3}*32*256 + {1}*32 + {0}*32+11],a_packed[{7}, {6}, {5}, {3}*32*256 + {1}*32 + {0}*32+12],a_packed[{7}, {6}, {5}, {3}*32*256 + {1}*32 + {0}*32+13],a_packed[{7}, {6}, {5}, {3}*32*256 + {1}*32 + {0}*32+14],a_packed[{7}, {6}, {5}, {3}*32*256 + {1}*32 + {0}*32+15],a_packed[{7}, {6}, {5}, {3}*32*256 + {1}*32 + {0}*32+16],a_packed[{7}, {6}, {5}, {3}*32*256 + {1}*32 + {0}*32+17],a_packed[{7}, {6}, {5}, {3}*32*256 + {1}*32 + {0}*32+18],a_packed[{7}, {6}, {5}, {3}*32*256 + {1}*32 + {0}*32+19],a_packed[{7}, {6}, {5}, {3}*32*256 + {1}*32 + {0}*32+20],a_packed[{7}, {6}, {5}, {3}*32*256 + {1}*32 + {0}*32+21],a_packed[{7}, {6}, {5}, {3}*32*256 + {1}*32 + {0}*32+22],a_packed[{7}, {6}, {5}, {3}*32*256 + {1}*32 + {0}*32+23],a_packed[{7}, {6}, {5}, {3}*32*256 + {1}*32 + {0}*32+24],a_packed[{7}, {6}, {5}, {3}*32*256 + {1}*32 + {0}*32+25],a_packed[{7}, {6}, {5}, {3}*32*256 + {1}*32 + {0}*32+26],a_packed[{7}, {6}, {5}, {3}*32*256 + {1}*32 + {0}*32+27],a_packed[{7}, {6}, {5}, {3}*32*256 + {1}*32 + {0}*32+28],a_packed[{7}, {6}, {5}, {3}*32*256 + {1}*32 + {0}*32+29],a_packed[{7}, {6}, {5}, {3}*32*256 + {1}*32 + {0}*32+30],a_packed[{7}, {6}, {5}, {3}*32*256 + {1}*32 + {0}*32+31])
         |     |     |     |     |      |   |    |   C_block_vec[{7}, {6}, {5}, {4}, {3}, {2}, {1}, {0}] fma_avx= avec[{7}, {6}, {5}, {4}, {3}, {2}, {1}, {0}], bvec[{6}, {5}, {4}, {3}, {2}, {1}, {0}]
         |     |     |     |     |      |            alpha_vec[{5}, {4}, {3}, {2}, {1}, {0}] =  _mm512_set1_ph(alpha)
         |     |     |     |     |      |  16    1   Cb_alpha_vec[{7}, {6}, {5}, {4}, {3}, {2}, {1}, {0}] = _mm512_mul_ph(C_block_vec[{7}, {6}, {5}, {4}, {3}, {2}, {1}, {0}], alpha_vec[{7}, {6}, {5}, {4}, {3}, {2}, {1}, {0}])
         |     |     |     |     |      |  16   32   Cb_alpha_s[{7}, {6}, {5}, {4}, {3}, {2}, {1}, {0}] = vec_to_scalar_h512(Cb_alpha_vec[{7}, {6}, {5}, {4}, {3}, {2}, {1}, {0}/32], {0})
         |     |     |     |     |      1  32   16   D[{5}*256 + {3}*32 + {2}*32+{1},{7}*256+{4}*16+{0}] += Cb_alpha_s[{7}, {6}, {5}, {4}, {3}, {0}, {2}, {1}] + beta*C[{5}*256 + {3}*32 + {2}*32+{1},{7}*256+{4}*16+{0}]
        """

    program = parse_program(program_text, invert_indices=False)
    print(program.text())
    input_data = generate_input_for_program(program, {
        "alpha": 0.9, "beta": 0.1,
    })
    output_data = ref_gemm(input_data)
    seconds = test_native_code(program, {**input_data, **output_data}, check_correctness=False, min_reps=10,
                               silent=False, remove_tmp_files=False)
    return seconds


def test_f16_gemm():
    # sizes = {'M': 1, 'N': 1024, 'K': 512}
    sizes = {'M': 512, 'N': 14336, 'K': 4096}
    print(f"GEMM {sizes['M']}x{sizes['N']}x{sizes['K']}")

    def opt(program):
        pass

    seconds_list = optimized_gemm_x86_f16()

    A = np.random.randn(sizes['M'], sizes['K']).astype(np.float16)
    B = np.random.randn(sizes['K'], sizes['N']).astype(np.float16)
    C = np.random.randn(sizes['M'], sizes['N']).astype(np.float16)
    alpha = np.float16(0.2)
    beta = np.float16(0.8)
    ref_seconds_list = timeit.repeat('hgemm(alpha, A, B, beta, C)',
                                     globals={**globals(), **locals()}, number=3, repeat=20)
    seconds_list = [round(x, 6) for x in seconds_list]
    ref_seconds_list = np.array([round(x, 6) for x in ref_seconds_list]) / 3
    print(f"Reference seconds: {ref_seconds_list}")
    print(f"Codegen   seconds: {seconds_list}")
    warmup = 3
    ref_seconds_list = ref_seconds_list[warmup:]
    seconds_list = seconds_list[warmup:]
    ref_seconds = np.mean(ref_seconds_list)
    ref_seconds_std = np.std(ref_seconds_list)
    seconds = np.mean(seconds_list)
    seconds_std = np.std(seconds_list)
    print(f"Reference seconds (avg): {ref_seconds:.6f}")
    print(f"Reference seconds (std): {ref_seconds_std:.6f}")
    print(f"Codegen   seconds (avg): {seconds:.6f}")
    print(f"Codegen   seconds (std): {seconds_std:.6f}")
    print(f"% of reference: {100 * ref_seconds / seconds:.2f}")

def naive_gemm_x86_f16(transform, sizes, check=False):
    M = sizes["M"]
    N = sizes["N"]
    K = sizes["K"]

    program_text = f"""
      Name: gemm
In: A, B
Out: C
Declarations:
    A f16 [{K}, {M}] heap
    B f16 [{K}, {N}] heap
    C f16 [{N}, {M}] heap
Code:
{M} {N} {K} C[{{1}}, {{2}}] += A[{{0}}, {{2}}] * B[{{0}}, {{1}}]
    """

    program = parse_program(program_text, invert_indices=False)
    transform(program)
    print(program.text())
    input_data = generate_input_for_program(program, {})
    output_data = {'C': np.array(torch.einsum("km,kn->nm", torch.tensor(input_data["A"]), torch.tensor(input_data["B"])))} if check else {'C': np.zeros((N,M)).astype(np.float16)}
    seconds = test_native_code(program, {**input_data, **output_data}, check_correctness=check, min_reps=20,
                               silent=False, remove_tmp_files=False)
    return seconds


def test_f16_gemm_transformations():
    # sizes = {'M': 1, 'N': 1024, 'K': 512}
    sizes = {'M': 512, 'N': 14336, 'K': 4096}
    print(f"GEMM {sizes['M']}x{sizes['N']}x{sizes['K']}")

    def opt(program):
        specialize_inputs(program, {})

        nc = 256
        kc = 512
        mc = 256
        nr = 16 # was 16
        mr = 32 # vector size
        

        create_temporary(program, 'C')
        tile_scope(program,'C0#2', mc)
        tile_scope(program,'C0#2', mr)
        tile_scope(program,'C0#1', nc)
        tile_scope(program,'C0#1', nr)
        tile_scope(program,'C0#0', kc)

        swap_nested_scopes(program, 'C0#1')
        create_dimension(program, 'C', 1)
        swap_nested_scopes(program, 'C0#1')
        
        tile_scope(program,'C#2', nc)
        tile_scope(program,'C#2', nr)
        tile_scope(program,'C#1', mc)
        tile_scope(program,'C#1', mr)

        reorder_loops(program, 'C0', before=['Mo', 'Mc', 'Mr', 'No', 'Nc', 'Nr','Ko', 'Kc'], after=['Ko', 'No', 'Mo', 'Nc', 'Mc', 'Kc', 'Nr', 'Mr'], silent=False)
        reorder_loops(program, 'C' , before=['No', 'Nc', 'Nr', 'Mo', 'Mc', 'Mr','Ko'],       after=['Ko', 'No', 'Mo', 'Nc', 'Mc', 'Nr', 'Mr'], silent=False)

        create_temporary(program, "A")
        create_temporary(program, "B")
        # tile B
        tile_scope(program,'B0#1', kc)
        tile_scope(program,'B0#0', nc)
        tile_scope(program,'B0#0', nr)
        reorder_loops(program, 'B0', before=['Ko', 'Kc', 'No', 'Nc', 'Nr'], after=['Ko', 'No','Nc', 'Kc', 'Nr'], silent=False)

        # tile A
        tile_scope(program,'A0#1', kc)
        tile_scope(program,'A0#0', mc)
        tile_scope(program,'A0#0', mr)
        shift_scope_to_pos(program, 'C0#6', 0)
        create_dimension(program, 'A0', 0)
        shift_scope_to_pos(program, 'C0#0', 6)
        shift_scope_to_pos(program, 'A0#0', 4)
        reorder_loops(program, 'A0', before=['Ko', 'Kc', 'Mo', 'Mc', 'Mr'], after=['Ko', 'Mo', 'Mc', 'Kc', 'Mr'], silent=False)
        
        # buffer transformations only allowed AFTER create dimension
        apply_to_exhaustion(program, [(tile_buffer, find_tileable_buffers)], silent=False)


        swap_buffer_dims(program, 'A0', 4)
        swap_buffer_dims(program, 'A0', 1)
        swap_buffer_dims(program, 'A0', 2)
        swap_buffer_dims(program, 'A0', 3)
        swap_buffer_dims(program, 'A0', 2)
        swap_buffer_dims(program, 'A0', 1)

        swap_buffer_dims(program, 'B0', 1)
        swap_buffer_dims(program, 'B0', 2)

        swap_buffer_dims(program, 'C0', 5)
        swap_buffer_dims(program, 'C0', 2)
        swap_buffer_dims(program, 'C0', 3)
        swap_buffer_dims(program, 'C0', 4)
        swap_buffer_dims(program, 'C0', 3)
        swap_buffer_dims(program, 'C0', 1)
        swap_buffer_dims(program, 'C0', 2)
        swap_buffer_dims(program, 'C0', 1)
        swap_buffer_dims(program, 'C0', 0)

        # parallelize_scope(program, 'B0#2')
        # parallelize_scope(program, 'B0#1')
        # parallelize_scope(program, 'A0#2')
        # parallelize_scope(program, 'A0#1')
        # parallelize_scope(program, 'C0#4')

        vectorize_program(program, 512)
        # swap_ops(program, 'A00#5')
        # swap_ops(program, 'A0#5')
        # unroll_scope(program, 'C0#0', 32)
        apply_to_exhaustion(program, [(join_scopes, find_joinable_scopes)], silent=False)
        apply_to_exhaustion(program, [(reuse_arr_dims, find_reusable_arr_dims)], silent=False)
        apply_to_exhaustion(program, [(move_buf_to_stack, lambda x: find_bufs_movable_to_stack(x, 4096))], silent=False)

        # print(program.text())
        # exit()

       


    seconds_list = naive_gemm_x86_f16(opt, sizes, check=False)

    A = np.random.randn(sizes['M'], sizes['K']).astype(np.float16)
    B = np.random.randn(sizes['K'], sizes['N']).astype(np.float16)
    C = np.random.randn(sizes['M'], sizes['N']).astype(np.float16)
    alpha = np.float16(1.0)
    beta = np.float16(0.0)
    ref_seconds_list = timeit.repeat('hgemm(alpha, A, B, beta, C)',
                                     globals={**globals(), **locals()}, number=3, repeat=20)
    seconds_list = [round(x, 6) for x in seconds_list]
    ref_seconds_list = np.array([round(x, 6) for x in ref_seconds_list]) / 3
    print(f"Reference seconds: {ref_seconds_list}")
    print(f"Codegen   seconds: {seconds_list}")
    warmup = 10
    ref_seconds_list = ref_seconds_list[warmup:]
    seconds_list = seconds_list[warmup:]
    ref_seconds = np.mean(ref_seconds_list)
    ref_seconds_std = np.std(ref_seconds_list)
    seconds = np.mean(seconds_list)
    seconds_std = np.std(seconds_list)
    print(f"Reference seconds (avg): {ref_seconds:.6f}")
    print(f"Reference seconds (std): {ref_seconds_std:.6f}")
    print(f"Codegen   seconds (avg): {seconds:.6f}")
    print(f"Codegen   seconds (std): {seconds_std:.6f}")
    print(f"% of reference: {100 * ref_seconds / seconds:.2f}")

def optimized_gemm(simulator, transform, check_correctness=True, size=[2, 3, 4]):
    size_dict = {"M": size[0], "N": size[1], "K": size[2]}
    program = create_gemm_program()
    specialize_inputs(program, size_dict)
    input_data = generate_input_for_program(program, {
        "alpha": 0.9, "beta": 0.1,
    })
    transform(program)
    output_data = ref_gemm(input_data)
    cycles = test_snitch_code(program, {**input_data, **output_data}, simulator, check_correctness)
    print("cycles:", cycles)
    
    
def optimized_gemm_native(transform, check_correctness=True, size=[2,3,4]):
    size_dict = {"M": size[0], "N": size[1], "K": size[2]}
    program = create_gemm_program()
    specialize_inputs(program, size_dict)
    print(program.text())
    input_data = generate_input_for_program(program, {
        "alpha": 0.9, "beta": 0.1,
    })
    transform(program)
    output_data = ref_gemm(input_data)
    seconds = test_native_code(program, {**input_data, **output_data}, check_correctness, min_reps=10, remove_tmp_files=False, silent=False)
    return seconds


def optimized_gemm_x86_f32(transform, check_correctness=True, size=[2,3,4]):
    size_dict = {"M": size[0], "N": size[1], "K": size[2]}
    program = create_gemm_program_f32()
    specialize_inputs(program, size_dict)
    input_data = generate_input_for_program(program, {
        "alpha": 0.9, "beta": 0.1,
    })
    transform(program)
    output_data = ref_gemm(input_data)
    seconds = test_native_code(program, {**input_data, **output_data}, check_correctness, min_reps=10)
    return seconds


def optimized_softmax_x86_f32avx(transform, check_correctness=True, size=[2,3, 4]):
    program = create_softmax_program_f32avx()
    input_data = generate_input_for_program(program, {'B': size[0], 'N': size[1], 'Ndiv8': size[2]})
    transform(program)
    output_data = ref_softmax(input_data)
    seconds = test_native_code(program, {**input_data, **output_data}, check_correctness, min_reps=100)
    return seconds

def optimized_softmax_x86_f32(transform, check_correctness=True, size=[2,3, 4]):
    program = create_softmax_program_f32()
    transform(program)
    input_data = generate_input_for_program(program, {})
    print(program.text())
    output_data = ref_softmax(input_data)
    seconds = test_native_code(program, {**input_data, **output_data}, check_correctness, min_reps=1000, silent=False)
    return seconds


def optimized_mish(simulator, transform, check_correctness=True, size=[2, 3]):
    program = create_mish_program()
    input_data = generate_input_for_program(program, {'B': size[0], 'N': size[1]})
    output_data = ref_mish(input_data)
    transform(program)
    cycles = test_snitch_code(program, {**input_data, **output_data}, simulator, check_correctness)
    print("cycles:", cycles)



def optimized_layernorm(simulator, transform, check_correctness=True, size=[2, 3]):
    program = create_layernorm_program()
    input_data = generate_input_for_program(program, {'B': size[0], 'N': size[1]})
    output_data = ref_layernorm(input_data)
    transform(program)
    cycles = test_snitch_code(program, {**input_data, **output_data}, simulator, check_correctness)
    print("cycles:", cycles)


def optimized_softmax(simulator, transform, check_correctness=True, size=[2, 16]):
    program = create_softmax_program()
    input_data = generate_input_for_program(program, {'B': size[0], 'N': size[1]})
    output_data = ref_softmax(input_data)
    transform(program)
    cycles = test_snitch_code(program, {**input_data, **output_data}, simulator, check_correctness)
    print("cycles:", cycles)


def optimized_logsoftmax(simulator, transform, check_correctness=True, size=[2, 16]):
    program = create_log_softmax_program()
    input_data = generate_input_for_program(program, {'B': size[0], 'N': size[1]})
    transform(program)
    output_data = ref_log_softmax(input_data)
    cycles = test_snitch_code(program, {**input_data, **output_data}, simulator, check_correctness)
    print("cycles:", cycles)


def optimized_batchnorm(simulator, transform, check_correctness=True, size=[2, 3, 4, 5]):
    program = create_batchnorm_program()
    input_data = generate_input_for_program(program, {
        'N': size[0], 'C': size[1], 'H': size[2], 'W': size[3], "epsilon": 1e-5, "momentum": 0.9,
        'scale': np.random.randn(size[1]), 'bias': np.random.randn(size[1]),
    })
    transform(program)
    output_data = ref_batchnorm(input_data)
    cycles = test_snitch_code(program, {**input_data, **output_data}, simulator, check_correctness)
    print("cycles:", cycles)



def optimized_softplus(simulator, transform, check_correctness=True, size=[2, 3]):
    program = create_softplus_program()
    input_data = generate_input_for_program(program, {'B': size[0], 'N': size[1]})
    output_data = ref_softplus(input_data)
    transform(program)
    cycles = test_snitch_code(program, {**input_data, **output_data}, simulator, check_correctness)
    print("cycles:", cycles)


if __name__ == "__main__":
    # make_transformations_graph()
    # run_correctness_tests()
    # run_performance_tests()
    
    # run_performance_manual_tests()
    # run_native_correctness_tests()
    # test_transform_applicability()
    # gen.change_precision("f32")
    run_x86_f32_correctness_tests()
    # run_x86_correctness_tests()
    # test_transform_applicability()
