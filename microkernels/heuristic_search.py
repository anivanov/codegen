from kernels import *

import torch
import sqlite3
import argparse


TRANSFORMATIONS_LIST = {
    "tile_scope": (tile_scope, find_tileable_scopes),
    "untile_scope": (untile_scope, find_untileable_scopes),
    "tile_buffer": (tile_buffer, find_tileable_buffers),
    "untile_buffer": (untile_buffer, find_untileable_buffers),
    "create_dimension": (create_dimension, find_creatable_dimensions),
    "delete_dimension": (delete_dimension, find_deletable_dimensions),
    "swap_nested_scopes": (swap_nested_scopes, find_swappable_nested_scopes),
    "create_temporary": (create_temporary, find_creatable_temporaries),
    "delete_temporary": (delete_temporary, find_deletable_temporaries),
    "swap_ops": (swap_ops, find_swappable_ops),
    "swap_buffer_dims": (swap_buffer_dims, find_swappable_buffer_dims),
    "join_scopes": (join_scopes, find_joinable_scopes),
    "split_scopes": (split_scopes, find_splittable_scopes),
    "reuse_arr_dims": (reuse_arr_dims, find_reusable_arr_dims),
    "unroll_scope": (unroll_scope, find_unrollable_scopes),
    "reuse_buffers": (reuse_buffers, find_reusable_buffers),
    "move_buf_to_stack": (move_buf_to_stack, find_bufs_movable_to_stack),
    "parallelize_scope": (parallelize_scope, find_parallelizable_scopes),
}


class SearchNode:
    def __init__(self, runtime, edges):
        self.runtime = runtime
        self.edges = edges  # edge = (tname, *targs)
        self.subtree = {}  # edge -> SearchNode


class SearchStateGreedy:
    def __init__(self, program, runtime):
        self.program = copy.deepcopy(program)
        self.explored_paths = {tuple(): runtime}  # path -> runtime
        self.seen_hashes = set()  # prevent looping
        self.tree_root = SearchNode(runtime, self.find_edges_for_path([]))
        self.seen_hashes.add(self.program.program_hash())
    
    def find_edges_for_path(self, path):
        program_copy = copy.deepcopy(self.program)
        for tname, *targs in path:
            tapply, _ = TRANSFORMATIONS_LIST[tname]
            tapply(program_copy, *targs)
        if program_copy.program_hash() in self.seen_hashes:
            return tuple()
        edges = tuple(
            (tname, *targs)
            for tname, (tapply, tcheck) in TRANSFORMATIONS_LIST.items()
            for targs in tcheck(program_copy)
        )
        return edges
    
    def update(self, path, runtime, program_hash):
        assert len(path) > 0
        # find where to insert
        node = self.tree_root
        for tname_with_targs in path[:-1]:
            node = node.subtree[tname_with_targs]
        # insert
        edges = self.find_edges_for_path(path)
        self.seen_hashes.add(program_hash)
        node.subtree[path[-1]] = SearchNode(runtime, edges)
        # insert to the explored paths
        self.explored_paths[path] = runtime

    def query(self):
        # find math with minimum runtime that is still unexplored
        for path, cost in sorted(self.explored_paths.items(), key=lambda x: x[1]):
            node = self.tree_root
            for tname, *targs in path:
                node = node.subtree[(tname, *targs)]
            for edge in node.edges:
                if edge not in node.subtree:
                    return (*path, edge)
        return None
    
    
class SearchStateShortAndGreedy:
    def __init__(self, program, runtime):
        self.program = copy.deepcopy(program)
        self.explored_paths = {tuple(): runtime}  # path -> runtime
        self.seen_hashes = set()  # prevent looping
        self.tree_root = SearchNode(runtime, self.find_edges_for_path([]))
        self.seen_hashes.add(self.program.program_hash())
    
    def find_edges_for_path(self, path):
        program_copy = copy.deepcopy(self.program)
        for tname, *targs in path:
            tapply, _ = TRANSFORMATIONS_LIST[tname]
            tapply(program_copy, *targs)
        if program_copy.program_hash() in self.seen_hashes:
            return tuple()
        edges = tuple(
            (tname, *targs)
            for tname, (tapply, tcheck) in TRANSFORMATIONS_LIST.items()
            for targs in tcheck(program_copy)
        )
        return edges
    
    def update(self, path, runtime, program_hash):
        assert len(path) > 0
        # find where to insert
        node = self.tree_root
        for tname_with_targs in path[:-1]:
            node = node.subtree[tname_with_targs]
        # insert
        edges = self.find_edges_for_path(path)
        self.seen_hashes.add(program_hash)
        node.subtree[path[-1]] = SearchNode(runtime, edges)
        # insert to the explored paths
        self.explored_paths[path] = runtime

    def query(self):
        # find math with minimum runtime that is still unexplored
        for path, cost in sorted(self.explored_paths.items(), key=lambda x: (1 + 0.05 * math.log(1+len(x[0]))) * x[1]):
            node = self.tree_root
            for tname, *targs in path:
                node = node.subtree[(tname, *targs)]
            for edge in node.edges:
                if edge not in node.subtree:
                    return (*path, edge)
        return None


class SearchNodeLeastExploredPaths:
    def __init__(self, runtime, edges):
        self.runtime = runtime
        self.edges = edges  # edge = (tname, *targs)
        self.subtree = {}  # edge -> SearchNode
        self.num_explored_subpath = 0


class SearchLeastExploredPaths:
    def __init__(self, program, runtime):
        self.program = copy.deepcopy(program)
        self.explored_paths = {tuple(): runtime}  # path -> runtime
        self.seen_hashes = set()  # prevent looping
        self.tree_root = SearchNodeLeastExploredPaths(runtime, self.find_edges_for_path([]))
        self.seen_hashes.add(self.program.program_hash())
    
    def find_edges_for_path(self, path):
        program_copy = copy.deepcopy(self.program)
        for tname, *targs in path:
            tapply, _ = TRANSFORMATIONS_LIST[tname]
            tapply(program_copy, *targs)
        if program_copy.program_hash() in self.seen_hashes:
            return tuple()
        edges = tuple(
            (tname, *targs)
            for tname, (tapply, tcheck) in TRANSFORMATIONS_LIST.items()
            for targs in tcheck(program_copy)
        )
        return edges
    
    def update(self, path, runtime, program_hash):
        assert len(path) > 0
        # find where to insert
        node = self.tree_root
        node.num_explored_subpath += 1
        for tname_with_targs in path[:-1]:
            node = node.subtree[tname_with_targs]
            node.num_explored_subpath += 1
        # insert
        edges = self.find_edges_for_path(path)
        self.seen_hashes.add(program_hash)
        node.subtree[path[-1]] = SearchNodeLeastExploredPaths(runtime, edges)
        # insert to the explored paths
        self.explored_paths[path] = runtime

    def get_num_subpath_list(self, path):
        result = [1]
        node = self.tree_root
        parent_paths = node.num_explored_subpath + 1
        for tname_with_targs in path:
            node = node.subtree[tname_with_targs]
            result.append(node.num_explored_subpath / parent_paths)
        return result

    def query(self):

        for path, cost in sorted(self.explored_paths.items(), key=lambda x: np.sum(self.get_num_subpath_list(x[0])) * x[1]):
            node = self.tree_root
            for tname, *targs in path:
                node = node.subtree[(tname, *targs)]
            for edge in node.edges:
                if edge not in node.subtree:
                    return (*path, edge)
        return None
    

class SearchNodeLeastExploredOrFastest:
    def __init__(self, runtime, edges):
        self.runtime = runtime
        self.edges = edges  # edge = (tname, *targs)
        self.subtree = {}  # edge -> SearchNode
        self.is_exhausted = False
        self.min_runtime = runtime


class SearchLeastExploredOrFastest:
    def __init__(self, program, runtime):
        self.program = copy.deepcopy(program)
        self.seen_hashes = set()  # prevent looping
        self.tree_root = SearchNodeLeastExploredOrFastest(runtime, self.find_edges_for_path([]))
        self.seen_hashes.add(self.program.program_hash())
    
    def find_edges_for_path(self, path):
        program_copy = copy.deepcopy(self.program)
        for tname, *targs in path:
            tapply, _ = TRANSFORMATIONS_LIST[tname]
            tapply(program_copy, *targs)
        if program_copy.program_hash() in self.seen_hashes:
            return tuple()
        edges = tuple(
            (tname, *targs)
            for tname, (tapply, tcheck) in TRANSFORMATIONS_LIST.items()
            for targs in tcheck(program_copy)
        )
        return edges
    
    def update(self, path, runtime, program_hash):
        assert len(path) > 0
        # find where to insert
        node = self.tree_root
        node.min_runtime = min(node.min_runtime, runtime)
        for tname_with_targs in path[:-1]:
            node = node.subtree[tname_with_targs]
            node.min_runtime = min(node.min_runtime, runtime)
        # insert
        edges = self.find_edges_for_path(path)
        self.seen_hashes.add(program_hash)
        node.subtree[path[-1]] = SearchNodeLeastExploredOrFastest(runtime, edges)

    def query(self):
        node = self.tree_root
        path = []
        while not self.tree_root.is_exhausted:
            # for every edge, assign cost
            costs = {}
            # first, process only edges that were already taken
            for edge, nested_node in node.subtree.items():
                if len(nested_node.edges) == 0:
                    continue  # there is nothing to explore and node was already considred
                if nested_node.is_exhausted:
                    continue  # do not consider fully explored nodes
                explored_fraction = len(nested_node.subtree) / len(nested_node.edges)
                costs[edge] = (nested_node.min_runtime, explored_fraction)
            average_min_runtime = np.mean([r for r, e in costs.values()]) if costs else 0.0
            # second add edges that were never taken
            for edge in node.edges:
                if edge in node.subtree:
                    continue  # already considered
                costs[edge] = (average_min_runtime, 0.0)
            if not costs:
                # we end up in fully explored node, mark the node as fully explored and restart
                node.is_exhausted = True
                node = self.tree_root
                path = []
                continue
            # find the least explored path candidates
            least_explored_val = min([e for r, e in costs.values()])
            least_runtime_val = min([r for r, e in costs.values()])
            fastest_candidates = [k for k, (r, e) in costs.items() if r == least_runtime_val]
            least_explored_candidates = [k for k, (r, e) in costs.items() if e == least_explored_val]
            # randomly choose among candidates
            if random.random() < 0.3:
                candidates = least_explored_candidates
            else:
                candidates = fastest_candidates
            edge = random.choice(candidates)
            path.append(edge)
            if edge in node.subtree:
                node = node.subtree[edge]
            else:
                return tuple(path)
        return None


class SearchNodeProbabilistic:
    def __init__(self, runtime, edges):
        self.runtime = runtime
        self.edges = edges  # edge = (tname, *targs)
        self.subtree = {}  # edge -> SearchNode
        self.is_exhausted = False
        self.min_runtime = runtime


class SearchProbabilistic:
    def __init__(self, program, runtime):
        self.program = copy.deepcopy(program)
        self.seen_hashes = set()  # prevent looping
        self.tree_root = SearchNodeProbabilistic(runtime, self.find_edges_for_path([]))
        self.seen_hashes.add(self.program.program_hash())
    
    def find_edges_for_path(self, path):
        program_copy = copy.deepcopy(self.program)
        for tname, *targs in path:
            tapply, _ = TRANSFORMATIONS_LIST[tname]
            tapply(program_copy, *targs)
        if program_copy.program_hash() in self.seen_hashes:
            return tuple()
        edges = tuple(
            (tname, *targs)
            for tname, (tapply, tcheck) in TRANSFORMATIONS_LIST.items()
            for targs in tcheck(program_copy)
        )
        return edges
    
    def update(self, path, runtime, program_hash):
        assert len(path) > 0
        # find where to insert
        node = self.tree_root
        node.min_runtime = min(node.min_runtime, runtime)
        for tname_with_targs in path[:-1]:
            node = node.subtree[tname_with_targs]
            node.min_runtime = min(node.min_runtime, runtime)
        # insert
        edges = self.find_edges_for_path(path)
        self.seen_hashes.add(program_hash)
        node.subtree[path[-1]] = SearchNodeProbabilistic(runtime, edges)

    def query(self):
        node = self.tree_root
        path = []
        while not self.tree_root.is_exhausted:
            # for every edge, assign cost
            costs = {}
            # first, process only edges that were already taken
            for edge, nested_node in node.subtree.items():
                if len(nested_node.edges) == 0:
                    continue  # there is nothing to explore and node was already considred
                if nested_node.is_exhausted:
                    continue  # do not consider fully explored nodes
                costs[edge] = nested_node.min_runtime
            average_min_runtime = np.mean(list(costs.values())) if costs else 1.0  # 1.0 is chosen as any constant except 0 is allowed
            # second add edges that were never taken
            for edge in node.edges:
                if edge in node.subtree:
                    continue  # already considered
                costs[edge] = average_min_runtime
            if not costs:
                # we end up in fully explored node, mark the node as fully explored and restart
                node.is_exhausted = True
                node = self.tree_root
                path = []
                continue
            edges = list(costs.keys())
            cost_vals = list(costs.values())
            profits = np.array([1.0 / c for c in cost_vals])
            normalized_profits = profits / np.sum(profits)
            # randomly choose among candidates
            sampled_edge_idx = np.random.choice(len(edges), p=normalized_profits)
            edge = edges[sampled_edge_idx]
            path.append(edge)
            if edge in node.subtree:
                node = node.subtree[edge]
            else:
                return tuple(path)
        return None


class SearchProbabilisticLimitedMin:
    def __init__(self, program, runtime):
        self.program = copy.deepcopy(program)
        self.seen_hashes = set()  # prevent looping
        self.tree_root = SearchNodeProbabilistic(runtime, self.find_edges_for_path([]))
        self.seen_hashes.add(self.program.program_hash())
    
    def find_edges_for_path(self, path):
        program_copy = copy.deepcopy(self.program)
        for tname, *targs in path:
            tapply, _ = TRANSFORMATIONS_LIST[tname]
            tapply(program_copy, *targs)
        if program_copy.program_hash() in self.seen_hashes:
            return tuple()
        edges = tuple(
            (tname, *targs)
            for tname, (tapply, tcheck) in TRANSFORMATIONS_LIST.items()
            for targs in tcheck(program_copy)
        )
        return edges
    
    def update(self, path, runtime, program_hash):
        assert len(path) > 0
        # find where to insert
        node = self.tree_root
        node.min_runtime = min(node.min_runtime, runtime)
        for i, tname_with_targs in enumerate(path[:-1]):
            node = node.subtree[tname_with_targs]
            if i > len(path) - 3:
                # only update the last 3 nodes
                node.min_runtime = min(node.min_runtime, runtime)
        # insert
        edges = self.find_edges_for_path(path)
        self.seen_hashes.add(program_hash)
        node.subtree[path[-1]] = SearchNodeProbabilistic(runtime, edges)

    def query(self):
        node = self.tree_root
        path = []
        while not self.tree_root.is_exhausted:
            # for every edge, assign cost
            costs = {}
            # first, process only edges that were already taken
            for edge, nested_node in node.subtree.items():
                if len(nested_node.edges) == 0:
                    continue  # there is nothing to explore and node was already considred
                if nested_node.is_exhausted:
                    continue  # do not consider fully explored nodes
                costs[edge] = nested_node.min_runtime
            average_min_runtime = np.mean(list(costs.values())) if costs else 1.0  # 1.0 is chosen as any constant except 0 is allowed
            # second add edges that were never taken
            for edge in node.edges:
                if edge in node.subtree:
                    continue  # already considered
                costs[edge] = average_min_runtime
            if not costs:
                # we end up in fully explored node, mark the node as fully explored and restart
                node.is_exhausted = True
                node = self.tree_root
                path = []
                continue
            edges = list(costs.keys())
            cost_vals = list(costs.values())
            profits = np.array([1.0 / c for c in cost_vals])
            normalized_profits = profits / np.sum(profits)
            # randomly choose among candidates
            sampled_edge_idx = np.random.choice(len(edges), p=normalized_profits)
            edge = edges[sampled_edge_idx]
            path.append(edge)
            if edge in node.subtree:
                node = node.subtree[edge]
            else:
                return tuple(path)
        return None


class SearchNodeProbGeneric:
    def __init__(self, runtime, edges):
        self.runtime = runtime
        self.edges = edges  # edge = (tname, *targs)
        self.subtree = {}  # edge -> SearchNode
        self.is_exhausted = False
        self.runtimes = [runtime]


class SearchProbabilisticLimitedAvg:
    def __init__(self, program, runtime):
        self.program = copy.deepcopy(program)
        self.seen_hashes = set()  # prevent looping
        self.tree_root = SearchNodeProbGeneric(runtime, self.find_edges_for_path([]))
        self.seen_hashes.add(self.program.program_hash())
    
    def find_edges_for_path(self, path):
        program_copy = copy.deepcopy(self.program)
        for tname, *targs in path:
            tapply, _ = TRANSFORMATIONS_LIST[tname]
            tapply(program_copy, *targs)
        if program_copy.program_hash() in self.seen_hashes:
            return tuple()
        edges = tuple(
            (tname, *targs)
            for tname, (tapply, tcheck) in TRANSFORMATIONS_LIST.items()
            for targs in tcheck(program_copy)
        )
        return edges
    
    def update(self, path, runtime, program_hash):
        assert len(path) > 0
        # find where to insert
        node = self.tree_root
        for i, tname_with_targs in enumerate(path[:-1]):
            node = node.subtree[tname_with_targs]
            if i > len(path) - 3:
                # only update the last 3 nodes
                node.runtimes.append(runtime)
        # insert
        edges = self.find_edges_for_path(path)
        self.seen_hashes.add(program_hash)
        node.subtree[path[-1]] = SearchNodeProbGeneric(runtime, edges)

    def query(self):
        node = self.tree_root
        path = []
        while not self.tree_root.is_exhausted:
            # for every edge, assign cost
            costs = {}
            # first, process only edges that were already taken
            for edge, nested_node in node.subtree.items():
                if len(nested_node.edges) == 0:
                    continue  # there is nothing to explore and node was already considred
                if nested_node.is_exhausted:
                    continue  # do not consider fully explored nodes
                costs[edge] = np.mean(nested_node.runtimes)
            average_min_runtime = np.mean(list(costs.values())) if costs else 1.0  # 1.0 is chosen as any constant except 0 is allowed
            # second add edges that were never taken
            for edge in node.edges:
                if edge in node.subtree:
                    continue  # already considered
                costs[edge] = average_min_runtime
            if not costs:
                # we end up in fully explored node, mark the node as fully explored and restart
                node.is_exhausted = True
                node = self.tree_root
                path = []
                continue
            edges = list(costs.keys())
            cost_vals = list(costs.values())
            profits = np.array([1.0 / c for c in cost_vals])
            normalized_profits = profits / np.sum(profits)
            # randomly choose among candidates
            sampled_edge_idx = np.random.choice(len(edges), p=normalized_profits)
            edge = edges[sampled_edge_idx]
            path.append(edge)
            if edge in node.subtree:
                node = node.subtree[edge]
            else:
                return tuple(path)
        return None


class SearchProbabilisticLimitedAvg2:
    def __init__(self, program, runtime):
        self.program = copy.deepcopy(program)
        self.seen_hashes = set()  # prevent looping
        self.tree_root = SearchNodeProbGeneric(runtime, self.find_edges_for_path([]))
        self.seen_hashes.add(self.program.program_hash())
    
    def find_edges_for_path(self, path):
        program_copy = copy.deepcopy(self.program)
        for tname, *targs in path:
            tapply, _ = TRANSFORMATIONS_LIST[tname]
            tapply(program_copy, *targs)
        if program_copy.program_hash() in self.seen_hashes:
            return tuple()
        edges = tuple(
            (tname, *targs)
            for tname, (tapply, tcheck) in TRANSFORMATIONS_LIST.items()
            for targs in tcheck(program_copy)
        )
        return edges
    
    def update(self, path, runtime, program_hash):
        assert len(path) > 0
        # find where to insert
        node = self.tree_root
        for i, tname_with_targs in enumerate(path[:-1]):
            node = node.subtree[tname_with_targs]
            if i > len(path) - 2:
                # only update the last 2 nodes
                node.runtimes.append(runtime)
        # insert
        edges = self.find_edges_for_path(path)
        self.seen_hashes.add(program_hash)
        node.subtree[path[-1]] = SearchNodeProbGeneric(runtime, edges)

    def query(self):
        node = self.tree_root
        path = []
        while not self.tree_root.is_exhausted:
            # for every edge, assign cost
            costs = {}
            # first, process only edges that were already taken
            for edge, nested_node in node.subtree.items():
                if len(nested_node.edges) == 0:
                    continue  # there is nothing to explore and node was already considred
                if nested_node.is_exhausted:
                    continue  # do not consider fully explored nodes
                costs[edge] = np.mean(nested_node.runtimes)
            average_min_runtime = np.mean(list(costs.values())) if costs else 1.0  # 1.0 is chosen as any constant except 0 is allowed
            # second add edges that were never taken
            for edge in node.edges:
                if edge in node.subtree:
                    continue  # already considered
                costs[edge] = average_min_runtime
            if not costs:
                # we end up in fully explored node, mark the node as fully explored and restart
                node.is_exhausted = True
                node = self.tree_root
                path = []
                continue
            edges = list(costs.keys())
            cost_vals = list(costs.values())
            profits = np.array([1.0 / c for c in cost_vals])
            normalized_profits = profits / np.sum(profits)
            # randomly choose among candidates
            sampled_edge_idx = np.random.choice(len(edges), p=normalized_profits)
            edge = edges[sampled_edge_idx]
            path.append(edge)
            if edge in node.subtree:
                node = node.subtree[edge]
            else:
                return tuple(path)
        return None


class SearchProbabilisticAvg:
    def __init__(self, program, runtime):
        self.program = copy.deepcopy(program)
        self.seen_hashes = set()  # prevent looping
        self.tree_root = SearchNodeProbGeneric(runtime, self.find_edges_for_path([]))
        self.seen_hashes.add(self.program.program_hash())
    
    def find_edges_for_path(self, path):
        program_copy = copy.deepcopy(self.program)
        for tname, *targs in path:
            tapply, _ = TRANSFORMATIONS_LIST[tname]
            tapply(program_copy, *targs)
        if program_copy.program_hash() in self.seen_hashes:
            return tuple()
        edges = tuple(
            (tname, *targs)
            for tname, (tapply, tcheck) in TRANSFORMATIONS_LIST.items()
            for targs in tcheck(program_copy)
        )
        return edges
    
    def update(self, path, runtime, program_hash):
        assert len(path) > 0
        # find where to insert
        node = self.tree_root
        for i, tname_with_targs in enumerate(path[:-1]):
            node = node.subtree[tname_with_targs]
            node.runtimes.append(runtime)
        # insert
        edges = self.find_edges_for_path(path)
        self.seen_hashes.add(program_hash)
        node.subtree[path[-1]] = SearchNodeProbGeneric(runtime, edges)

    def query(self):
        node = self.tree_root
        path = []
        while not self.tree_root.is_exhausted:
            # for every edge, assign cost
            costs = {}
            # first, process only edges that were already taken
            for edge, nested_node in node.subtree.items():
                if len(nested_node.edges) == 0:
                    continue  # there is nothing to explore and node was already considred
                if nested_node.is_exhausted:
                    continue  # do not consider fully explored nodes
                costs[edge] = np.mean(nested_node.runtimes)
            average_min_runtime = np.mean(list(costs.values())) if costs else 1.0  # 1.0 is chosen as any constant except 0 is allowed
            # second add edges that were never taken
            for edge in node.edges:
                if edge in node.subtree:
                    continue  # already considered
                costs[edge] = average_min_runtime
            if not costs:
                # we end up in fully explored node, mark the node as fully explored and restart
                node.is_exhausted = True
                node = self.tree_root
                path = []
                continue
            edges = list(costs.keys())
            cost_vals = list(costs.values())
            profits = np.array([1.0 / c for c in cost_vals])
            normalized_profits = profits / np.sum(profits)
            # randomly choose among candidates
            sampled_edge_idx = np.random.choice(len(edges), p=normalized_profits)
            edge = edges[sampled_edge_idx]
            path.append(edge)
            if edge in node.subtree:
                node = node.subtree[edge]
            else:
                return tuple(path)
        return None





def compute_normalized_profits(profits, importance):
    # copy profits
    profits = {k: list(v) for k, v in profits.items()}
    # normalize importance
    importance = np.array(importance) / np.sum(importance)
    # for each compontent of profits, normalize it across the same component
    profit_sum = np.zeros_like(importance)
    for k, v in profits.items():
        for i, c in enumerate(v):
            profit_sum[i] += c
    for i in range(len(profit_sum)):
        if profit_sum[i] < 1e-4:
            profit_sum[i] = len(profits)
            for k in profits:
                profits[k][i] = 1.0
    # compute normalized profits
    normalized_profits = {}
    for k, v in profits.items():
        normalized_profits[k] = [c / profit_sum[i] for i, c in enumerate(v)]
    # finally, combine profits vector into scalar using importance
    combined_profits = {k: float(np.dot(np.array(v), importance)) for k, v in normalized_profits.items()}
    return combined_profits


class SearchNodeNoEval:
    def __init__(self, edges):
        self.edges = edges  # edge = (tname, *targs)
        self.subtree = {}  # edge -> SearchNode
        self.is_exhausted = False
        self.runtimes = []


class SearchProbRareEval:
    def __init__(self, program, runtime):
        self.program = copy.deepcopy(program)
        self.seen_hashes = set()  # prevent looping
        self.tree_root = SearchNodeNoEval(self.find_edges_for_path([]))
        self.seen_hashes.add(self.program.program_hash())
    
    def find_edges_for_path(self, path):
        program_copy = program_for_path(self.program, path)
        if program_copy.program_hash() in self.seen_hashes:
            return tuple()
        edges = tuple(
            (tname, *targs)
            for tname, (tapply, tcheck) in TRANSFORMATIONS_LIST.items()
            for targs in tcheck(program_copy)
        )
        return edges
    
    def fill(self, path):
        assert len(path) > 0
        # find where to insert
        node = self.tree_root
        for i, tname_with_targs in enumerate(path[:-1]):
            node = node.subtree[tname_with_targs]
        # insert
        edges = self.find_edges_for_path(path)
        node.subtree[path[-1]] = SearchNodeNoEval(edges)
    
    def update(self, path, runtime, program_hash):
        assert len(path) > 0
        # find where to insert
        node = self.tree_root
        for i, tname_with_targs in enumerate(path[:-1]):
            node = node.subtree[tname_with_targs]
            node.runtimes.append(runtime)
        # insert
        edges = self.find_edges_for_path(path)
        self.seen_hashes.add(program_hash)
        if path[-1] in node.subtree:
            node.subtree[path[-1]].runtimes.append(runtime)
        else:
            node.subtree[path[-1]] = SearchNodeProbGeneric(runtime, edges)

    def query(self):
        node = self.tree_root
        path = []
        while not self.tree_root.is_exhausted:
            # for every edge, assign cost
            costs = {}
            # first, process only edges that were already taken
            for edge, nested_node in node.subtree.items():
                if len(nested_node.edges) == 0:
                    continue  # there is nothing to explore and node was already considered
                if nested_node.is_exhausted:
                    continue  # do not consider fully explored nodes
                if not nested_node.runtimes:
                    continue  # do not consider nodes without any evaluations (we will guess their runtime later)
                costs[edge] = np.mean(nested_node.runtimes)
            average_min_runtime = np.mean(list(costs.values())) if costs else 1.0  # 1.0 is chosen as any constant except 0 is allowed
            # second add edges that were never taken
            for edge in node.edges:
                if edge in node.subtree:
                    continue  # already considered
                costs[edge] = average_min_runtime
            if not costs:
                # we end up in fully explored node, mark the node as fully explored and restart
                node.is_exhausted = True
                node = self.tree_root
                path = []
                continue
            # now, for all candidates collected so far, assign costs based on the program assessment
            profits = {}
            
            min_num_scopes = float('inf')
            max_num_scopes = 0
            for edge in costs:
                candidate_path = path + [edge]
                program_copy = program_for_path(self.program, candidate_path)
                runtime = costs[edge]
                num_scopes = num_scopes_in_program(program_copy)
                buf_size = bufsize_in_program(program_copy)
                parallel_depth = parallelism_depth(program_copy)
                profits[edge] = (runtime, num_scopes, buf_size, parallel_depth)
                
                min_num_scopes = min(min_num_scopes, num_scopes)
                max_num_scopes = max(max_num_scopes, num_scopes)
            if max_num_scopes-min_num_scopes > 0:
                for edge, (r, s, b, p) in profits.items():
                    profits[edge] = (1/r, (max_num_scopes-s)/(max_num_scopes-min_num_scopes), 1/b, 1-p)
            
            normalized_profits = compute_normalized_profits(profits, importance=(0.1, 0.5, 0.3, 0.1))            
            # randomly choose among candidates
            edges = list(normalized_profits.keys())
            sampled_edge_idx = np.random.choice(len(normalized_profits), p=list(normalized_profits.values()))
            edge = edges[sampled_edge_idx]
            path.append(edge)
            if edge not in node.subtree:
                self.fill(path)
            do_measurement = random.random() < 0.05
            no_more_transforms = not node.subtree[edge].edges
            if do_measurement or no_more_transforms:
                return tuple(path)
            else:
                node = node.subtree[edge]
        return None


def program_for_path(program, path):
    new_program = copy.deepcopy(program)
    for tname, *targs in path:
        tapply, _ = TRANSFORMATIONS_LIST[tname]
        tapply(new_program, *targs)
    return new_program


def node_for_path(node, path):
    for tname_with_targs in path:
        if tname_with_targs not in node.subtree:
            return None
        node = node.subtree[tname_with_targs]
    return node


def num_scopes_in_program(program):
    return len(list(recursive_scopes(program)))


def bufsize_in_program(program):
    return sum(
        buf.size_bytes()
        for buf in program.buffers.values()
    )


def parallelism_depth(program):
    deepest_parallelism = 0
    for scopes, op in get_flattened_ops_with_scopes_textual(program):
        first_parallel_scope = next((i for i, s in enumerate(scopes) if s.options.get('parallel', False)), 0)
        current_parallelism_depth = 0 if len(scopes) <= 1 else first_parallel_scope / (len(scopes) - 1)
        deepest_parallelism = max(deepest_parallelism, current_parallelism_depth)
    return deepest_parallelism


class SearchProbabilisticRS:
    def __init__(self, program, runtime):
        self.program = copy.deepcopy(program)
        self.seen_hashes = set()  # prevent looping
        self.tree_root = SearchNodeProbabilistic(runtime, self.find_edges_for_path([]))
        self.seen_hashes.add(self.program.program_hash())
    
    def find_edges_for_path(self, path):
        program_copy = program_for_path(self.program, path)
        if program_copy.program_hash() in self.seen_hashes:
            return tuple()
        edges = tuple(
            (tname, *targs)
            for tname, (tapply, tcheck) in TRANSFORMATIONS_LIST.items()
            for targs in tcheck(program_copy)
        )
        return edges
    
    def update(self, path, runtime, program_hash):
        assert len(path) > 0
        # find where to insert
        node = self.tree_root
        node.min_runtime = min(node.min_runtime, runtime)
        for tname_with_targs in path[:-1]:
            node = node.subtree[tname_with_targs]
            node.min_runtime = min(node.min_runtime, runtime)
        # insert
        edges = self.find_edges_for_path(path)
        self.seen_hashes.add(program_hash)
        node.subtree[path[-1]] = SearchNodeProbabilistic(runtime, edges)

    def query(self):
        node = self.tree_root
        path = []
        while not self.tree_root.is_exhausted:
            # for every edge, assign cost
            costs = {}
            # first, process only edges that were already taken
            for edge, nested_node in node.subtree.items():
                if len(nested_node.edges) == 0:
                    continue  # there is nothing to explore and node was already considred
                if nested_node.is_exhausted:
                    continue  # do not consider fully explored nodes
                program_copy = program_for_path(self.program, path + [edge])
                num_scopes = num_scopes_in_program(program_copy)
                costs[edge] = (nested_node.min_runtime, num_scopes)
            average_min_runtime = np.mean([r for r, s in costs.values()]) if costs else 1.0  # 1.0 is chosen as any constant except 0 is allowed
            # second add edges that were never taken
            for edge in node.edges:
                if edge in node.subtree:
                    continue  # already considered
                program_copy = program_for_path(self.program, path + [edge])
                num_scopes = num_scopes_in_program(program_copy)
                costs[edge] = (average_min_runtime, num_scopes)
            if not costs:
                # we end up in fully explored node, mark the node as fully explored and restart
                node.is_exhausted = True
                node = self.tree_root
                path = []
                continue            
            edges = list(costs.keys())
            profit_runtimes = np.array(list((1 / r) for r, s in costs.values()))
            profit_scopes = np.array(list((1 / s) for r, s in costs.values()))
            # normalize profits
            profit_runtimes = profit_runtimes / np.sum(profit_runtimes)
            profit_scopes = profit_scopes / np.sum(profit_scopes)
            profits = 0.5 * profit_runtimes + 0.5 * profit_scopes
            # randomly choose among candidates
            sampled_edge_idx = np.random.choice(len(edges), p=profits)
            edge = edges[sampled_edge_idx]
            path.append(edge)
            if edge in node.subtree:
                node = node.subtree[edge]
            else:
                return tuple(path)
        return None


class SearchProbabilisticRSB:
    def __init__(self, program, runtime):
        self.program = copy.deepcopy(program)
        self.seen_hashes = set()  # prevent looping
        self.tree_root = SearchNodeProbabilistic(runtime, self.find_edges_for_path([]))
        self.seen_hashes.add(self.program.program_hash())
    
    def find_edges_for_path(self, path):
        program_copy = program_for_path(self.program, path)
        if program_copy.program_hash() in self.seen_hashes:
            return tuple()
        edges = tuple(
            (tname, *targs)
            for tname, (tapply, tcheck) in TRANSFORMATIONS_LIST.items()
            for targs in tcheck(program_copy)
        )
        return edges
    
    def update(self, path, runtime, program_hash):
        assert len(path) > 0
        # find where to insert
        node = self.tree_root
        node.min_runtime = min(node.min_runtime, runtime)
        for tname_with_targs in path[:-1]:
            node = node.subtree[tname_with_targs]
            node.min_runtime = min(node.min_runtime, runtime)
        # insert
        edges = self.find_edges_for_path(path)
        self.seen_hashes.add(program_hash)
        node.subtree[path[-1]] = SearchNodeProbabilistic(runtime, edges)

    def query(self):
        node = self.tree_root
        path = []
        while not self.tree_root.is_exhausted:
            # for every edge, assign cost
            costs = {}
            # first, process only edges that were already taken
            for edge, nested_node in node.subtree.items():
                if len(nested_node.edges) == 0:
                    continue  # there is nothing to explore and node was already considred
                if nested_node.is_exhausted:
                    continue  # do not consider fully explored nodes
                program_copy = program_for_path(self.program, path + [edge])
                num_scopes = num_scopes_in_program(program_copy)
                buf_sizes = bufsize_in_program(program_copy)
                costs[edge] = (nested_node.min_runtime, num_scopes, buf_sizes)
            average_min_runtime = np.mean([r for r, s, b in costs.values()]) if costs else 1.0  # 1.0 is chosen as any constant except 0 is allowed
            # second add edges that were never taken
            for edge in node.edges:
                if edge in node.subtree:
                    continue  # already considered
                program_copy = program_for_path(self.program, path + [edge])
                num_scopes = num_scopes_in_program(program_copy)
                buf_sizes = bufsize_in_program(program_copy)
                costs[edge] = (average_min_runtime, num_scopes, buf_sizes)
            if not costs:
                # we end up in fully explored node, mark the node as fully explored and restart
                node.is_exhausted = True
                node = self.tree_root
                path = []
                continue            
            edges = list(costs.keys())
            profit_runtimes = np.array(list((1 / r) for r, s, b in costs.values()))
            profit_scopes = np.array(list((1 / s) for r, s, b in costs.values()))
            profit_bufsizes = np.array(list((1 / b) for r, s, b in costs.values()))
            # normalize profits
            profit_runtimes = profit_runtimes / np.sum(profit_runtimes)
            profit_scopes = profit_scopes / np.sum(profit_scopes)
            profit_bufsizes = profit_bufsizes / np.sum(profit_bufsizes)
            profits = 0.33 * profit_runtimes + 0.33 * profit_scopes + 0.34 * profit_bufsizes
            # randomly choose among candidates
            sampled_edge_idx = np.random.choice(len(edges), p=profits)
            edge = edges[sampled_edge_idx]
            path.append(edge)
            if edge in node.subtree:
                node = node.subtree[edge]
            else:
                return tuple(path)
        return None


class SearchProbabilisticRSBP:
    def __init__(self, program, runtime):
        self.program = copy.deepcopy(program)
        self.seen_hashes = set()  # prevent looping
        self.tree_root = SearchNodeProbabilistic(runtime, self.find_edges_for_path([]))
        self.seen_hashes.add(self.program.program_hash())
    
    def find_edges_for_path(self, path):
        program_copy = program_for_path(self.program, path)
        if program_copy.program_hash() in self.seen_hashes:
            return tuple()
        edges = tuple(
            (tname, *targs)
            for tname, (tapply, tcheck) in TRANSFORMATIONS_LIST.items()
            for targs in tcheck(program_copy)
        )
        return edges
    
    def update(self, path, runtime, program_hash):
        assert len(path) > 0
        # find where to insert
        node = self.tree_root
        node.min_runtime = min(node.min_runtime, runtime)
        for tname_with_targs in path[:-1]:
            node = node.subtree[tname_with_targs]
            node.min_runtime = min(node.min_runtime, runtime)
        # insert
        edges = self.find_edges_for_path(path)
        self.seen_hashes.add(program_hash)
        node.subtree[path[-1]] = SearchNodeProbabilistic(runtime, edges)

    def query(self):
        node = self.tree_root
        path = []
        while not self.tree_root.is_exhausted:
            # for every edge, assign cost
            costs = {}
            # first, process only edges that were already taken
            for edge, nested_node in node.subtree.items():
                if len(nested_node.edges) == 0:
                    continue  # there is nothing to explore and node was already considred
                if nested_node.is_exhausted:
                    continue  # do not consider fully explored nodes
                program_copy = program_for_path(self.program, path + [edge])
                num_scopes = num_scopes_in_program(program_copy)
                buf_sizes = bufsize_in_program(program_copy)
                parallelism_depth_val = parallelism_depth(program_copy)
                costs[edge] = (nested_node.min_runtime, num_scopes, buf_sizes, parallelism_depth_val)
            average_min_runtime = np.mean([r for r, s, b, p in costs.values()]) if costs else 1.0  # 1.0 is chosen as any constant except 0 is allowed
            # second add edges that were never taken
            for edge in node.edges:
                if edge in node.subtree:
                    continue  # already considered
                program_copy = program_for_path(self.program, path + [edge])
                num_scopes = num_scopes_in_program(program_copy)
                buf_sizes = bufsize_in_program(program_copy)
                parallelism_depth_val = parallelism_depth(program_copy)
                costs[edge] = (average_min_runtime, num_scopes, buf_sizes, parallelism_depth_val)
            if not costs:
                # we end up in fully explored node, mark the node as fully explored and restart
                node.is_exhausted = True
                node = self.tree_root
                path = []
                continue            
            edges = list(costs.keys())
            profit_runtimes = np.array(list((1 / r) for r, s, b, p in costs.values()))
            profit_scopes = np.array(list((1 / s) for r, s, b, p in costs.values()))
            profit_bufsizes = np.array(list((1 / b) for r, s, b, p in costs.values()))
            profit_par_depth = np.array(list(1 - p for r, s, b, p in costs.values()))
            # normalize profits
            profit_runtimes = profit_runtimes / np.sum(profit_runtimes)
            profit_scopes = profit_scopes / np.sum(profit_scopes)
            profit_bufsizes = profit_bufsizes / np.sum(profit_bufsizes)
            profit_par_depth = profit_par_depth / np.sum(profit_par_depth) if np.sum(profit_par_depth) else 1 / len(profit_par_depth)
            profits = 0.7 * profit_runtimes + 0.1 * profit_scopes + 0.1 * profit_bufsizes + 0.1 * profit_par_depth
            # randomly choose among candidates
            sampled_edge_idx = np.random.choice(len(edges), p=profits)
            edge = edges[sampled_edge_idx]
            path.append(edge)
            if edge in node.subtree:
                node = node.subtree[edge]
            else:
                return tuple(path)
        return None


class SearchProbabilisticRSBP:
    def __init__(self, program, runtime):
        self.program = copy.deepcopy(program)
        self.seen_hashes = set()  # prevent looping
        self.tree_root = SearchNodeProbabilistic(runtime, self.find_edges_for_path([]))
        self.seen_hashes.add(self.program.program_hash())
    
    def find_edges_for_path(self, path):
        program_copy = program_for_path(self.program, path)
        if program_copy.program_hash() in self.seen_hashes:
            return tuple()
        edges = tuple(
            (tname, *targs)
            for tname, (tapply, tcheck) in TRANSFORMATIONS_LIST.items()
            for targs in tcheck(program_copy)
        )
        return edges
    
    def update(self, path, runtime, program_hash):
        assert len(path) > 0
        # find where to insert
        node = self.tree_root
        node.min_runtime = min(node.min_runtime, runtime)
        for tname_with_targs in path[:-1]:
            node = node.subtree[tname_with_targs]
            node.min_runtime = min(node.min_runtime, runtime)
        # insert
        edges = self.find_edges_for_path(path)
        self.seen_hashes.add(program_hash)
        node.subtree[path[-1]] = SearchNodeProbabilistic(runtime, edges)

    def query(self):
        node = self.tree_root
        path = []
        while not self.tree_root.is_exhausted:
            # for every edge, assign cost
            costs = {}
            # first, process only edges that were already taken
            for edge, nested_node in node.subtree.items():
                if len(nested_node.edges) == 0:
                    continue  # there is nothing to explore and node was already considred
                if nested_node.is_exhausted:
                    continue  # do not consider fully explored nodes
                program_copy = program_for_path(self.program, path + [edge])
                num_scopes = num_scopes_in_program(program_copy)
                buf_sizes = bufsize_in_program(program_copy)
                parallelism_depth_val = parallelism_depth(program_copy)
                costs[edge] = (nested_node.min_runtime, num_scopes, buf_sizes, parallelism_depth_val)
            average_min_runtime = np.mean([r for r, s, b, p in costs.values()]) if costs else 1.0  # 1.0 is chosen as any constant except 0 is allowed
            # second add edges that were never taken
            for edge in node.edges:
                if edge in node.subtree:
                    continue  # already considered
                program_copy = program_for_path(self.program, path + [edge])
                num_scopes = num_scopes_in_program(program_copy)
                buf_sizes = bufsize_in_program(program_copy)
                parallelism_depth_val = parallelism_depth(program_copy)
                costs[edge] = (average_min_runtime, num_scopes, buf_sizes, parallelism_depth_val)
            if not costs:
                # we end up in fully explored node, mark the node as fully explored and restart
                node.is_exhausted = True
                node = self.tree_root
                path = []
                continue            
            edges = list(costs.keys())
            profit_runtimes = np.array(list((1 / r) for r, s, b, p in costs.values()))
            profit_scopes = np.array(list((1 / s) for r, s, b, p in costs.values()))
            profit_bufsizes = np.array(list((1 / b) for r, s, b, p in costs.values()))
            profit_par_depth = np.array(list(1 - p for r, s, b, p in costs.values()))
            # normalize profits
            profit_runtimes = profit_runtimes / np.sum(profit_runtimes)
            profit_scopes = profit_scopes / np.sum(profit_scopes)
            profit_bufsizes = profit_bufsizes / np.sum(profit_bufsizes)
            profit_par_depth = profit_par_depth / np.sum(profit_par_depth) if np.sum(profit_par_depth) else 1 / len(profit_par_depth)
            profits = 0.7 * profit_runtimes + 0.1 * profit_scopes + 0.1 * profit_bufsizes + 0.1 * profit_par_depth
            # randomly choose among candidates
            sampled_edge_idx = np.random.choice(len(edges), p=profits)
            edge = edges[sampled_edge_idx]
            path.append(edge)
            if edge in node.subtree:
                node = node.subtree[edge]
            else:
                return tuple(path)
        return None


class SearchNode:
    def __init__(self, runtime, edges):
        self.runtime = runtime
        self.edges = edges  # edge = (tname, *targs)
        self.subtree = {}  # edge -> SearchNode


class SearchSimulatedAnnealing:
    def __init__(self, program, runtime):
        self.program = copy.deepcopy(program)
        self.current_path = []
        self.candidate_path = []
        # self.seen_hashes = set()  # prevent looping
        self.tree_root = SearchNode(runtime, self.find_edges_for_path([]))
        # self.seen_hashes.add(self.program.program_hash())
        self.temperature = 10.0
        self.total_steps = 100
        self.steps_left = self.total_steps
        self.cooling_factor = 0.99
    
    def find_edges_for_path(self, path):
        program_copy = program_for_path(self.program, path)
        # if program_copy.program_hash() in self.seen_hashes:
        #     return tuple()
        edges = tuple(
            (tname, *targs)
            for tname, (tapply, tcheck) in TRANSFORMATIONS_LIST.items()
            for targs in tcheck(program_copy)
        )
        return edges
    
    def update(self, path, runtime, program_hash):
        # sometimes there can be backtracking and the same paths may be inserted multiple times
        if len(path) == 0:
            return  # if it is root again, just ignore it
        # find where to insert
        node = node_for_path(self.tree_root, path[:-1])
        if path[-1] in node.subtree:
            return  # skip if already considered
        edges = self.find_edges_for_path(path)
        # self.seen_hashes.add(program_hash)
        node.subtree[path[-1]] = SearchNode(runtime, edges)

    def query(self):
        if self.steps_left <= 0:
            # restart search
            self.steps_left = self.total_steps
            self.current_path = []
            self.candidate_path = []
        if self.current_path == self.candidate_path:
            # we need to generate new candidate
            current_node = node_for_path(self.tree_root, self.current_path)
            if self.current_path:
                current_edges = [None, *current_node.edges]  # None means to go back to the root
            else:
                current_edges = current_node.edges
            # uniformly pick direction
            # print("Selecting from the list of available choices: ", current_edges)
            edge = random.choice(current_edges)
            # print("Current path:", self.current_path)
            # print("Selected candidate:", edge)
            if edge is None:
                candidate_path = self.current_path[:-1]
            else:
                candidate_path = self.current_path + [edge]
            self.candidate_path = candidate_path
        else:
            # we already have a candidate
            # we decide to pick it or reject it
            current_node = node_for_path(self.tree_root, self.current_path)
            candidate_node = node_for_path(self.tree_root, self.candidate_path)
            current_energy = current_node.runtime
            candidate_energy = candidate_node.runtime
            # accept or reject
            if candidate_energy < current_energy:
                # print("Candidate accepted (lower energy)")
                # accept lower energy unconditionally
                self.current_path = self.candidate_path
            elif random.random() < np.exp((current_energy - candidate_energy) / self.temperature):
                # print("Candidate accepted (higher energy)")
                # accept higher energy
                self.current_path = self.candidate_path
            else:
                # print("Candidate rejected")
                # reject higher energy
                self.candidate_path = self.current_path
            # update temperature and steps
            self.temperature *= self.cooling_factor
            self.steps_left -= 1
        print('steps left:', self.steps_left)
        return self.candidate_path


class RuntimeCache:
    def __init__(self, filename):
        self.db = sqlite3.connect(filename, timeout=300)
        self.db.execute('PRAGMA journal_mode=WAL')
        self.db.execute('''
            CREATE TABLE IF NOT EXISTS cache (
                key TEXT PRIMARY KEY,
                value REAL,
                std REAL,
                timeout BOOLEAN
            )
        ''')
        self.db.commit()
        self.local_cache = {}

    def __setitem__(self, key, value_std_timeout):
        value, std, timeout = value_std_timeout
        if key in self.local_cache:
            old_value, old_std, old_timeout = self.local_cache[key]
            # we overwrite only if old is timed out and new is not
            overwrite = old_timeout and not timeout
            if not overwrite:
                return
        self.db.execute('''
            REPLACE INTO cache (key, value, std, timeout) VALUES (?, ?, ?, ?)
        ''', (key, value, std, timeout))
        self.db.commit()
        self.local_cache[key] = (value, std, timeout)


    def __getitem__(self, key):
        if key in self.local_cache:
            return self.local_cache[key]
        cursor = self.db.execute('''
            SELECT value, std, timeout FROM cache WHERE key = ?
        ''', (key,))
        row = cursor.fetchone()
        if row is None:
            # if not found assume timed out with 0 threshold
            value = (0.0, 0.0, True)
            # put in local cache to minimize db queries
            self.local_cache[key] = value
            return value
        return row

    def __contains__(self, key):
        return self[key] is not None


def eval_program(new_program, ref_data, min_repeats, timeout_ms, cache):
    warmup = math.ceil(0.3 * min_repeats)
    new_program_hash = new_program.program_hash()
    cached_ms, cached_std_ms, cached_timeout = cache[new_program_hash]
    if (not cached_timeout) or (cached_timeout and timeout_ms <= cached_ms):
        # use cached result
        runtime_ms = cached_ms
        runtime_std = cached_std_ms
        print(f"Runtime (from cache) [ms]: avg {runtime_ms:.6f} std {runtime_std:.6f}")
    else:
        try:
            time = test_native_code(
                new_program,
                ref_data,
                min_reps=warmup+min_repeats,
                min_seconds=0.1,
                remove_tmp_files=False,
                silent=False,
                timeout=timeout_ms / 1e3,
            )[warmup:]
            runtime_ms = np.mean(time) * 1e3
            runtime_std = np.std(time) * 1e3
            cache[new_program_hash] = runtime_ms, runtime_std, False
            print(f"Runtime (measured) [ms]: avg {runtime_ms:.6f} std {runtime_std:.6f}")
        except subprocess.TimeoutExpired as e:
            runtime_ms = timeout_ms
            runtime_std = 0.0
            cache[new_program_hash] = runtime_ms, runtime_std, True
            print(f"Runtime (timed out) [ms]: avg {runtime_ms:.6f} std 0.0")
    return runtime_ms, runtime_std


def run_search(program, ref_data, min_repeats, searcher):
    
    cache = RuntimeCache('cached_runtime.db')
    
    runtime_ms, runtime_std = eval_program(program, ref_data, min_repeats, float('inf'), cache)
    ss = searcher(program=program, runtime=runtime_ms)
    print(f"Initial runtime [ms]: avg {runtime_ms:.6f} std {runtime_std:.6f}")
    best_runtime_ms = runtime_ms
        
    while True:
        transformation_sequence = ss.query()
        if transformation_sequence is None:
            break
        new_program = copy.deepcopy(program)
        for tname, *targs in transformation_sequence:
            tapply, _ = TRANSFORMATIONS_LIST[tname]
            tapply(new_program, *targs)
        
        new_program_hash = new_program.program_hash()
        
        print("Transformations: ", end="")
        print("; ".join(f"{tname_path}(program, {args_as_str(targs_path)})" for tname_path, *targs_path in transformation_sequence))
        print(new_program.text())

        # timeout_ms = 2 * best_runtime_ms        
        #timeout_ms = float('inf')
        timeout_ms = 10000
        runtime_ms, runtime_std = eval_program(new_program, ref_data, min_repeats, timeout_ms, cache)

        ss.update(transformation_sequence, runtime_ms, new_program_hash)        

        if runtime_ms < best_runtime_ms:
            print(f"Cost improved. Old: {best_runtime_ms} -> New: {runtime_ms}")
            best_runtime_ms = runtime_ms
    
    print("Beam search exhausted.")


def run_matmul(M, N, K, min_repeats, searcher):
    program_text = f"""
        Name: func_matmul
        In: x, y
        Out: z
        Declarations:
            x f32 [{M}, {K}] heap
            y f32 [{K}, {N}] heap
            z f32 [{M}, {N}] heap
        Code:
        {M} {N} {K} z[{{0}}, {{1}}] += x[{{0}}, {{2}}] * y[{{2}}, {{1}}]
    """
    
    program = parse_program(program_text)
    print(program.text())
    
    x = np.random.uniform(-1, 1, (M, K)).astype(np.float32)
    y = np.random.uniform(-1, 1, (K, N)).astype(np.float32)
    tx = torch.from_numpy(x)
    ty = torch.from_numpy(y)
    tz = torch.matmul(tx, ty)
    z = tz.numpy()
    
    warmup = math.ceil(0.3 * min_repeats)

    pt_runtime = timeit.repeat(lambda: torch.matmul(tx, ty), repeat=warmup+min_repeats, number=1)[warmup:]
    print(f"Pytorch matmul time [ms]: avg {np.mean(pt_runtime) * 1e3:.6f} std {np.std(pt_runtime) * 1e3:.6f}")

    ref_data = {'x': x, 'y': y, 'z': z}
    run_search(program, ref_data, min_repeats=min_repeats, searcher=searcher)

    
def run_layernorm(M, N, min_repeats, searcher):
    program_text = f"""
        Name: layernorm
        In: src, gamma, beta
        Out: dst
        Declarations:
            src f32 [{M}, {N}] heap
            gamma f32 [{N}] heap
            beta f32 [{N}] heap
            dst f32 [{M}, {N}] heap
            m f32 [{M}] heap
            mu f32 [{M}] heap
            diff f32 [{M}, {N}] heap
            q f32 [{M}] heap
            tmp f32 [{M}, {N}] heap
            qeps f32 [{M}] heap
            sigma f32 [{M}] heap
        Code:
        {M} {N} m[{{0}}] += src[{{0}}, {{1}}]
        {M} mu[{{0}}] = m[{{0}}] / {N}
        {M} {N} diff[{{0}}, {{1}}] = src[{{0}}, {{1}}] - mu[{{0}}]
        {M} {N} q[{{0}}] += diff[{{0}}, {{1}}] * diff[{{0}}, {{1}}]
        {M} {N} tmp[{{0}}, {{1}}] = diff[{{0}}, {{1}}] * gamma[{{1}}]
        {M} qeps[{{0}}] = q[{{0}}] * {1./(N - 1.)} + 0.00001
        {M} sigma[{{0}}] = 1 / sqrtf(qeps[{{0}}])
        {M} {N} dst[{{0}}, {{1}}] = sigma[{{0}}] * tmp[{{0}}, {{1}}] + beta[{{1}}]
    """
    
    program = parse_program(program_text)
    print(program.text())
    
    src = np.random.uniform(-1, 1, (M, N)).astype(np.float32)
    gamma = np.random.uniform(-1, 1, (N,)).astype(np.float32)
    beta = np.random.uniform(-1, 1, (N,)).astype(np.float32)
    
    tsrc = torch.from_numpy(src)
    tgamma  = torch.from_numpy(gamma)
    tbeta = torch.from_numpy(beta)
    
    warmup = math.ceil(0.3 * min_repeats)
    pt_runtime = timeit.repeat(lambda: torch.nn.functional.layer_norm(tsrc, [N], weight=tgamma, bias=tbeta, eps=1e-5), repeat=warmup+min_repeats, number=1)[warmup:]
    print(f"Pytorch layernorm time [ms]: avg {np.mean(pt_runtime) * 1e3:.6f} std {np.std(pt_runtime) * 1e3:.6f}")
    
    
    tdst = torch.nn.functional.layer_norm(tsrc, [N], weight=tgamma, bias=tbeta, eps=1e-5)
    dst = tdst.numpy()
    
    ref_data = {'src': src, 'gamma': gamma, 'beta': beta, 'dst': dst}
    
    run_search(program, ref_data, min_repeats=min_repeats, searcher=searcher)


def run_conv(N, M, C, H_in, W_in, K, min_repeats, searcher):
    H = pooling_out_dim(input=H_in, kernel=K, stride=1, pad=0, dilation=1)
    W = pooling_out_dim(input=W_in, kernel=K, stride=1, pad=0, dilation=1)
    
    program_text = f"""
        Name: conv2d
        In: input, weight
        Out: output
        Declarations:
            input f32 [{N}, {C}, {H_in}, {W_in}] heap
            weight f32 [{M}, {C}, {K}, {K}] heap
            output f32 [{N}, {M}, {H}, {W}] heap
        Code:
        {N} {M} {H} {W} {C} {K} {K} output[{{0}}, {{1}}, {{2}}, {{3}}] += input[{{0}}, {{4}}, {{2}} + {{5}}, {{3}} + {{6}}] * weight[{{1}}, {{4}}, {{5}}, {{6}}]
    """
    
    program = parse_program(program_text)
    
    print(program.text())
    
    input = np.random.uniform(-10, 10, (N, C, H_in, W_in)).astype(np.float32)
    weight = np.random.uniform(-10, 10, (M, C, K, K)).astype(np.float32)
    tinput = torch.from_numpy(input)
    tweight = torch.from_numpy(weight)
    toutput = torch.nn.functional.conv2d(tinput, tweight)
    output = toutput.numpy()
        
    pt_builtin_runtime = timeit.repeat(lambda: torch.nn.functional.conv2d(tinput, tweight), repeat=10, number=1)[3:]
    print(f"Pytorch builtin conv2d time [ms]: avg {np.mean(pt_builtin_runtime) * 1e3:.6f} std {np.std(pt_builtin_runtime) * 1e3:.6f}")
    
    ref_data = {'input': input, 'weight': weight, 'output': output}
    
    run_search(program, ref_data, min_repeats=min_repeats, searcher=searcher)


def run_log_softmax(M, N, min_repeats, searcher):
    program_text = f"""
        Name: func_log_softmax
        In: x
        Out: z
        Declarations:
            x f32 [{M}, {N}] heap
            z f32 [{M}, {N}] heap
            max_val f32 [{M}] heap
            diff f32 [{M}, {N}] heap
            exp_diff f32 [{M}, {N}] heap
            sum_val f32 [{M}] heap
            ln_sum f32 [{M}] heap
        Code:
        {M} {N} max_val[{{0}}] fmaxf= (x[{{0}}, {{1}}])
        {M} {N} diff[{{0}}, {{1}}] = x[{{0}}, {{1}}] - max_val[{{0}}]
        {M} {N} exp_diff[{{0}}, {{1}}] = expf(diff[{{0}}, {{1}}])
        {M} {N} sum_val[{{0}}] += exp_diff[{{0}}, {{1}}]
        {M} ln_sum[{{0}}] = logf(sum_val[{{0}}])
        {M} {N} z[{{0}}, {{1}}] = diff[{{0}}, {{1}}] - ln_sum[{{0}}]
    """
    
    program = parse_program(program_text)
    print(program.text())
    
    x = np.random.uniform(-10, 10, (M, N)).astype(np.float32)
    tx = torch.from_numpy(x)
    tz = torch.nn.functional.log_softmax(tx, dim=1)
    z = tz.numpy()
    
    warmup = math.ceil(0.3 * min_repeats)
    pt_builtin_runtime = timeit.repeat(lambda: torch.nn.functional.log_softmax(tx, dim=1), repeat=warmup+min_repeats, number=1)[warmup:]
    print(f"Pytorch log_softmax time [ms]: avg {np.mean(pt_builtin_runtime) * 1e3:.6f} std {np.std(pt_builtin_runtime) * 1e3:.6f}")
    
    run_search(program, {'x': x, 'z': z}, min_repeats=min_repeats, searcher=searcher)


def run_reducemean(M, N, min_repeats, searcher):
    program_text = f"""
        Name: reducemean
        In: x
        Out: z
        Declarations:
            x f32 [{M}, {N}] heap
            t f32 [{M}] heap
            z f32 [{M}] heap
        Code:
        {M} {N} t[{{0}}] += x[{{0}}, {{1}}]
        {M} z[{{0}}] = t[{{0}}] / {N}
    """
    
    program = parse_program(program_text)
    print(program.text())
    
    x = np.random.uniform(-10, 10, (M, N)).astype(np.float32)
    tx = torch.from_numpy(x)
    z = np.mean(x, axis=1)
    
    warmup = math.ceil(0.3 * min_repeats)
    pt_runtime = timeit.repeat(lambda: torch.mean(tx, dim=1), repeat=warmup+min_repeats, number=1)[warmup:]
    print(f"Pytorch mean time [ms]: avg {np.mean(pt_runtime) * 1e3:.6f} std {np.std(pt_runtime) * 1e3:.6f}")

    ref_data = {'x': x, 'z': z}
    run_search(program, ref_data, min_repeats=min_repeats, searcher=searcher)


if __name__ == "__main__":    
    argparser = argparse.ArgumentParser()
    argparser.add_argument("--kernel", type=str, required=True)
    argparser.add_argument("--min_repeats", type=int, default=5)
    argparser.add_argument("--search", type=str, default="greedy")
    args = argparser.parse_args()

    random.seed(0)
    np.random.seed(0)
    torch.manual_seed(0)

    searcher = {
        'greedy': SearchStateGreedy,
        'short_and_greedy': SearchStateShortAndGreedy,
        'least_explored_paths': SearchLeastExploredPaths,
        'least_explored_or_fastest': SearchLeastExploredOrFastest,
        'probabilistic': SearchProbabilistic,
        'prob_rs': SearchProbabilisticRS,
        'prob_rsb': SearchProbabilisticRSB,
        'prob_rsbp': SearchProbabilisticRSBP,
        'prob_limited_min': SearchProbabilisticLimitedMin,
        'prob_limited_avg': SearchProbabilisticLimitedAvg,
        'prob_limited_avg2': SearchProbabilisticLimitedAvg2,
        'prob_avg': SearchProbabilisticAvg,
        'prob_rare_eval': SearchProbRareEval,
        'simulated_annealing': SearchSimulatedAnnealing,
    }[args.search]
    
    if args.kernel == "matmul":
        run_matmul(1024, 1024, 1024, min_repeats=args.min_repeats, searcher=searcher)
    elif args.kernel == "layernorm":
        run_layernorm(4096, 4096, min_repeats=args.min_repeats, searcher=searcher)
    elif args.kernel == "conv":
        run_conv(N=8, M=10, C=3, H_in=512, W_in=512, K=5, min_repeats=args.min_repeats, searcher=searcher)
    elif args.kernel == "log_softmax":
        run_log_softmax(4096, 4096, min_repeats=args.min_repeats, searcher=searcher)
    elif args.kernel == "reducemean":
        run_reducemean(4096, 4096, min_repeats=args.min_repeats, searcher=searcher)
    else:
        raise ValueError("Unknown kernel")
