from kernels import *


def make_heuristic_opt(sizes):
    def heuristic_opt(program):
        specialize_inputs(program, sizes)
        apply_to_exhaustion(program, [
            (join_scopes, find_joinable_scopes),
            (reuse_arr_dims, find_reusable_arr_dims),
            (reuse_buffers, find_reusable_buffers),
            (move_buf_to_stack, find_bufs_movable_to_stack),
            (enable_ssr, find_applicable_ssr),
            (increase_ssr_depth_by_idx, find_increasable_ssr_depth_by_idx),
            (merge_ssr_by_idx, find_mergeable_ssr_by_idx),
            (activate_ssr_by_id, find_activatable_ssr_by_idx),
            (enable_frep, find_applicable_frep),
        ])
        print(program)
    def better_heuristic_opt(program):
        specialize_inputs(program, sizes)
        flags = [0]*len(program.ops)
        for loop, f in zip(program.ops, range(len(flags))):
            if isinstance(loop, LoopScope):
                try:
                    tile_scope(program, loop.name, 4)
                    for d in range(int(loop.name[-1]) - 1, 0, -1):
                        scope_name = loop.name[:-1] + str(d)
                        swap_nested_scopes(program, scope_name)
                except:
                    flags[f] = 1

        apply_to_exhaustion(program, [
            (join_scopes, find_joinable_scopes),
            (reuse_arr_dims, find_reusable_arr_dims),
            (reuse_buffers, find_reusable_buffers),
            (move_buf_to_stack, find_bufs_movable_to_stack),
            (enable_ssr, find_applicable_ssr),
            (increase_ssr_depth_by_idx, find_increasable_ssr_depth_by_idx),
            (merge_ssr_by_idx, find_mergeable_ssr_by_idx),
            (activate_ssr_by_id, find_activatable_ssr_by_idx),
        ])
        print(program)
        
        for loop, f in zip(program.ops, flags):
            if isinstance(loop, LoopScope) and flags != 1:
                last_scope = loop
                for d in range(int(loop.name[-1]), 0, -1):
                    last_scope = last_scope.ops[0]
                for op in last_scope.ops[1:]:
                    #print(op)
                    split_scopes(program, op.out_elem.name)
                    unroll_scope(program, op.out_elem.name  + "#0")
                unroll_scope(program, last_scope.ops[0].out_elem.name  + "#0")

        apply_to_exhaustion(program, [(enable_frep, find_applicable_frep),
        ])
        print(program)
    return heuristic_opt


def rtl_tester(program, ref_data):
    #return test_snitch_code(program, ref_data, simulator="banshee", check_correctness=True)
    return test_snitch_code(program, ref_data, simulator="rtl", check_correctness=False)


def greedy_opt_sigmoid():
    B = 16
    N = 16
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "x": np.random.uniform(-10, 10, (B, N))}
    cycles = run_kernel(create_sigmoid_program, ref_sigmoid, make_heuristic_opt(sizes), rtl_tester, fixed_inputs)
    f = open("naive_results.txt",  'a')
    print("Sigmoid:", cycles, file=f)
    print("Sigmoid:", cycles)


def greedy_opt_leakyrelu():
    B = 16
    N = 16
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "x": np.random.uniform(-10, 10, (B, N))}
    cycles = run_kernel(create_leakyrelu_program, ref_leakyrelu, make_heuristic_opt(sizes), rtl_tester, fixed_inputs)
    f = open("naive_results.txt",  'a')
    print("LeakyRelu:", cycles, file=f)
    print("LeakyRelu:", cycles)


def greedy_opt_celu():
    B = 16
    N = 16
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "x": np.random.uniform(-10, 10, (B, N)), "alpha": 0.4}
    cycles = run_kernel(create_celu_program, ref_celu, make_heuristic_opt(sizes), rtl_tester, fixed_inputs)
    f = open("naive_results.txt",  'a')
    print('Celu:', cycles, file=f)
    print('Celu:', cycles)


def greedy_opt_relu():
    B = 64
    N = 64
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "x": np.random.uniform(-10, 10, (B, N))}
    cycles = run_kernel(create_relu_program, ref_relu, make_heuristic_opt(sizes), rtl_tester, fixed_inputs)
    f = open("naive_results.txt",  'a')
    print("Relu:", cycles, file=f)
    print("Relu:", cycles)


def greedy_opt_pow():
    B = 8
    N = 8
    sizes = {"B": B, "N": N}
    fixed_inputs = {
        **sizes,
        "x1": np.random.uniform(0.1, 10, (B, N)),
        "x2": np.random.uniform(-4, 4, (B, N)),
    }
    cycles = run_kernel(create_pow_program, ref_pow, make_heuristic_opt(sizes), rtl_tester, fixed_inputs)
    f = open("naive_results.txt",  'a')
    print("Pow:", cycles, file=f)
    print("Pow:", cycles)


def greedy_opt_sqrt():
    B = 16
    N = 16
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "src": np.random.uniform(0, 10, (B, N))}
    cycles = run_kernel(create_sqrt_program, ref_sqrt_program, make_heuristic_opt(sizes), rtl_tester, fixed_inputs)
    f = open("naive_results.txt",  'a')
    print("Sqrt:", cycles, file=f)
    print("Sqrt:", cycles)


def greedy_opt_tanh():
    B = 16
    N = 16
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "x": np.random.uniform(-1, 1, (B, N))}
    cycles = run_kernel(create_tanh_program, ref_tanh, make_heuristic_opt(sizes), rtl_tester, fixed_inputs)
    f = open("naive_results.txt",  'a')
    print("Tanh:", cycles, file=f)
    print("Tanh:", cycles)


def greedy_opt_log():
    B = 16
    N = 8
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "x": np.random.uniform(0.001, 100, (B, N))}
    cycles = run_kernel(create_log_program, ref_log, make_heuristic_opt(sizes), rtl_tester, fixed_inputs)
    f = open("naive_results.txt",  'a')
    print("Log:", cycles, file=f)
    print("Log:", cycles)


def greedy_opt_div():
    B = 16
    N = 16
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "src1": np.random.uniform(-10, 10, (B, N)), "src2": np.random.uniform(0.5, 2.0, (B, N)) * np.random.choice([1, -1], (B, N))}
    cycles = run_kernel(create_div_program, ref_div_program, make_heuristic_opt(sizes), rtl_tester, fixed_inputs)
    f = open("naive_results.txt",  'a')
    print("Div:", cycles, file=f)
    print("Div:", cycles)


def greedy_opt_mul():
    B = 64
    N = 32
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "src1": np.random.uniform(-10, 10, (B, N)), "src2": np.random.uniform(-10, 10, (B, N))}
    cycles = run_kernel(create_mul_program, ref_mul_program, make_heuristic_opt(sizes), rtl_tester, fixed_inputs)
    f = open("naive_results.txt",  'a')
    print("Mul:", cycles, file=f)
    print("Mul:", cycles)


def greedy_opt_sub():
    B = 64
    N = 32
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "src1": np.random.uniform(-10, 10, (B, N)), "src2": np.random.uniform(-10, 10, (B, N))}
    cycles = run_kernel(create_sub_program, ref_sub_program, make_heuristic_opt(sizes), rtl_tester, fixed_inputs)
    f = open("naive_results.txt",  'a')
    print("Sub:", cycles, file=f)
    print("Sub:", cycles)


def greedy_opt_sum():
    B = 16
    N = 16
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "src1": np.random.uniform(-10, 10, (B, N)), "src2": np.random.uniform(-10, 10, (B, N))}
    cycles = run_kernel(create_sum_program, ref_sum_program, make_heuristic_opt(sizes), rtl_tester, fixed_inputs)
    f = open("naive_results.txt",  'a')
    print("Sum:", cycles, file=f)
    print("Sum:", cycles)


def greedy_opt_cosh():
    B = 16
    N = 16
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "x": np.random.uniform(-2, 2, (B, N))}
    cycles = run_kernel(create_cosh_program, ref_cosh, make_heuristic_opt(sizes), rtl_tester, fixed_inputs)
    f = open("naive_results.txt",  'a')
    print("Cosh:", cycles, file=f)
    print("Cosh:", cycles)


def greedy_opt_exp():
    B = 16
    N = 16
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "x": np.random.uniform(-10, 10, (B, N))}
    cycles = run_kernel(create_exp_program, ref_exp, make_heuristic_opt(sizes), rtl_tester, fixed_inputs)
    f = open("naive_results.txt",  'a')
    print("Exp:", cycles, file=f)
    print("Exp:", cycles)


def greedy_opt_reducemean():
    B = 64
    N = 64
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "x": np.random.uniform(-10, 10, (B, N))}
    cycles = run_kernel(create_reducemean_program, ref_reducemean, make_heuristic_opt(sizes), rtl_tester, fixed_inputs)
    f = open("naive_results.txt",  'a')
    print("ReduceMean:", cycles, file=f)
    print("ReduceMean:", cycles)


def greedy_opt_argmin():
    B = 32
    N = 32
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "x": np.random.uniform(-10, 10, (B, N))}
    cycles = run_kernel(create_argmin_program, ref_argmin, make_heuristic_opt(sizes), rtl_tester, fixed_inputs)
    f = open("naive_results.txt",  'a')
    print("ArgMin:", cycles, file=f)
    print("ArgMin:", cycles)


def greedy_opt_argmax():
    B = 32
    N = 32
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "x": np.random.uniform(-10, 10, (B, N))}
    cycles = run_kernel(create_argmax_program, ref_argmax, make_heuristic_opt(sizes), rtl_tester, fixed_inputs)
    f = open("naive_results.txt",  'a')
    print("ArgMax:", cycles, file=f)
    print("ArgMax:", cycles)


def greedy_opt_conv():
    M = 1
    C = 4
    H = 10
    W = 10
    K = 3
    sizes = {
        "N": 4, #batches
        "M": M, #feature maps (or output channels)
        "C": C, #input channels
        "H_in": H, #input height
        "W_in": W, #inpuy width
        "kernel": K,
        "stride": 1,
        "dilation": 1,
    }
    sizes["H_out"] = pooling_out_dim(sizes["H_in"], sizes["kernel"], sizes["stride"], 0, sizes["dilation"])
    sizes["W_out"] = pooling_out_dim(sizes["W_in"], sizes["kernel"], sizes["stride"], 0, sizes["dilation"])
    sizes["H_out"] = pooling_out_dim(sizes["H_in"], sizes["kernel"], sizes["stride"], 0, sizes["dilation"])
    sizes["W_out"] = pooling_out_dim(sizes["W_in"], sizes["kernel"], sizes["stride"], 0, sizes["dilation"])
    cycles = run_kernel(create_conv2d_nopad_program_notmp, lambda x: ref_conv2d_nopad({**x, 'G': 1}), make_heuristic_opt(sizes), rtl_tester, fixed_inputs=sizes)
    f = open("naive_results.txt",  'a')
    print("Conv:", cycles, file=f)
    print("Conv:", cycles)


def greedy_opt_clip():
    B = 64
    N = 64
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "x": np.random.uniform(-10, 10, (B, N)), "a": -5.0, "b": 5.0}
    cycles = run_kernel(create_clip_program, ref_clip, make_heuristic_opt(sizes), rtl_tester, fixed_inputs=fixed_inputs)
    print("Clip:", cycles, file=f)
    print("Clip:", cycles)


def greedy_opt_blackman_window():
    N = 100
    sizes = {"N": N}
    fixed_inputs = {**sizes}
    cycles = run_kernel(create_blackman_window_program, ref_blackman_window, make_heuristic_opt(sizes), rtl_tester, fixed_inputs=fixed_inputs)
    f = open("naive_results.txt",  'a')
    print("BlackmanWindow:", cycles, file=f)
    print("BlackmanWindow:", cycles)


def greedy_opt_cos():
    B = 16
    N = 16
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "x": np.random.uniform(-100, 100, (B, N))}
    cycles = run_kernel(create_cos_program, ref_cos, make_heuristic_opt(sizes), rtl_tester, fixed_inputs=fixed_inputs)
    f = open("naive_results.txt",  'a')
    print("Cos:", cycles, file=f)
    print("Cos:", cycles)
    
    
def greedy_opt_floor():
    B = 32
    N = 32
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "x": np.random.uniform(-100, 100, (B, N))}
    cycles = run_kernel(create_floor_program, ref_floor, make_heuristic_opt(sizes), rtl_tester, fixed_inputs=fixed_inputs)
    f = open("naive_results.txt",  'a')
    print("Floor:", cycles, file=f)
    print("Floor:", cycles)


def greedy_opt_ceil():
    B = 32
    N = 32
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "x": np.random.uniform(-100, 100, (B, N))}
    cycles = run_kernel(create_ceil_program, ref_ceil, make_heuristic_opt(sizes), rtl_tester, fixed_inputs=fixed_inputs)
    f = open("naive_results.txt",  'a')
    print("Ceil:", cycles, file=f)
    print("Ceil:", cycles)


def greedy_opt_bitwise_xor():
    B = 64
    N = 64
    sizes = {"B": B, "N": N}
    fixed_inputs = {
        **sizes,
        "x": np.random.randint(0, 1000, (B, N), dtype=np.int32),
        "y": np.random.randint(0, 1000, (B, N), dtype=np.int32),
    }
    cycles = run_kernel(create_bitwise_xor_program, ref_bitwise_xor, make_heuristic_opt(sizes), rtl_tester, fixed_inputs=fixed_inputs)
    f = open("naive_results.txt",  'a')
    print("BitwiseXor:", cycles, file=f)
    print("BitwiseXor:", cycles)


def greedy_opt_bitwise_or():
    B = 64
    N = 64
    sizes = {"B": B, "N": N}
    fixed_inputs = {
        **sizes,
        "x": np.random.randint(0, 1000, (B, N), dtype=np.int32),
        "y": np.random.randint(0, 1000, (B, N), dtype=np.int32),
    }
    cycles = run_kernel(create_bitwise_or_program, ref_bitwise_or, make_heuristic_opt(sizes), rtl_tester, fixed_inputs=fixed_inputs)
    f = open("naive_results.txt",  'a')
    print("BitwiseOr:", cycles, file=f)
    print("BitwiseOr:", cycles)
    

def greedy_opt_bitwise_and():
    B = 64
    N = 64
    sizes = {"B": B, "N": N}
    fixed_inputs = {
        **sizes,
        "x": np.random.randint(0, 1000, (B, N), dtype=np.int32),
        "y": np.random.randint(0, 1000, (B, N), dtype=np.int32),
    }
    cycles = run_kernel(create_bitwise_and_program, ref_bitwise_and, make_heuristic_opt(sizes), rtl_tester, fixed_inputs=fixed_inputs)
    f = open("naive_results.txt",  'a')
    print("BitwiseAnd:", cycles, file=f)
    print("BitwiseAnd:", cycles)
    
    
def greedy_opt_bitwise_not():
    B = 64
    N = 64
    sizes = {"B": B, "N": N}
    fixed_inputs = {
        **sizes,
        "x": np.random.randint(0, 1000, (B, N), dtype=np.int32),
    }
    cycles = run_kernel(create_bitwise_not_program, ref_bitwise_not, make_heuristic_opt(sizes), rtl_tester, fixed_inputs=fixed_inputs)
    f = open("naive_results.txt",  'a')
    print("BitwiseNot:", cycles, file=f)
    print("BitwiseNot:", cycles)


def greedy_opt_bitshift():
    B = 64
    N = 64
    sizes = {"B": B, "N": N}
    fixed_inputs = {
        **sizes,
        "x": np.random.randint(0, 1000, (B, N), dtype=np.int32),
        "y": np.random.randint(0, 5, (B, N), dtype=np.int32),
    }
    cycles = run_kernel(create_bitshift_left_program, ref_bitshift_left, make_heuristic_opt(sizes), rtl_tester, fixed_inputs=fixed_inputs)
    f = open("naive_results.txt",  'a')
    print("BitShift:", cycles, file=f)
    print("BitShift:", cycles)
    

def greedy_opt_averagepool():
    sizes = {
        "N": 4,
        "C": 4,
        "H_in": 10,
        "W_in": 10,
        "kernel": 3,
        "stride": 1,
        "dilation": 1,
    }
    sizes["H_out"] = pooling_out_dim(sizes["H_in"], sizes["kernel"], sizes["stride"], 0, sizes["dilation"])
    sizes["W_out"] = pooling_out_dim(sizes["W_in"], sizes["kernel"], sizes["stride"], 0, sizes["dilation"])
    fixed_inptus = {**sizes}
    cycles = run_kernel(
        create_averagepool2d_nopad_program,
        ref_averagepool2d_nopad,
        make_heuristic_opt(sizes),
        rtl_tester,
        fixed_inputs=fixed_inptus
    )
    f = open("naive_results.txt",  'a')
    print("AveragePool:", cycles, file=f)
    print("AveragePool:", cycles)


def greedy_opt_atanh():
    B = 8
    N = 8
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "src": np.random.uniform(-0.9, 0.9, (B, N))}
    cycles = run_kernel(create_atanh_program, ref_atanh_program, make_heuristic_opt(sizes), rtl_tester, fixed_inputs)
    f = open("naive_results.txt",  'a')
    print("Atanh:", cycles, file=f)
    print("Atanh:", cycles)


def greedy_opt_atan():
    B = 16
    N = 16
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "src": np.tan(np.random.uniform(-np.pi, np.pi, (B, N)))}
    cycles = run_kernel(create_atan_program, ref_atan_program, make_heuristic_opt(sizes), rtl_tester, fixed_inputs)
    f = open("naive_results.txt",  'a')
    print("Atan:", cycles, file=f)
    print("Atan:", cycles)


def greedy_opt_asinh():
    B = 8
    N = 8
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes}
    cycles = run_kernel(create_asinh_program, ref_asinh_program, make_heuristic_opt(sizes), rtl_tester, fixed_inputs)
    f = open("naive_results.txt",  'a')
    print("Asinh:", cycles, file=f)
    print("Asinh:", cycles)
    
    
def greedy_opt_asin():
    B = 16
    N = 16
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "src": np.sin(np.random.uniform(-1, 1, (B, N)))}
    cycles = run_kernel(create_asin_program, ref_asin_program, make_heuristic_opt(sizes), rtl_tester, fixed_inputs)
    f = open("naive_results.txt",  'a')
    print("Asin:", cycles, file=f)
    print("Asin:", cycles)


def greedy_opt_and():
    B = 32
    N = 32
    sizes = {"B": B, "N": N}
    fixed_inputs = {
        **sizes,
        "src1": np.random.randint(0, 2, (B, N)),
        "src2": np.random.randint(0, 2, (B, N)),
    }
    cycles = run_kernel(create_and_program, ref_and, make_heuristic_opt(sizes), rtl_tester, fixed_inputs)
    print("And:", cycles, file=f)
    print("And:", cycles)


def greedy_opt_affine_grid():
    sizes = {'N': 4, 'H': 16, 'W': 16}
    fixed_inputs = {
        **sizes, 'theta': np.array([
            [[1, 0, 3], 
             [0, 1, 4]],
            [[np.cos(np.pi/6), -np.sin(np.pi/6), -1], 
             [np.sin(np.pi/6), np.cos(np.pi/6), -2]],
            [[1, 2, 0], 
             [3, 1, 0]],
            [[1, 0, 0], 
             [0, 1, 0]],
        ], dtype=np.float64)
    }
    cycles = run_kernel(create_affine_grid_2d_program, ref_affine_grid_2d, make_heuristic_opt(sizes), rtl_tester, fixed_inputs)
    f = open("naive_results.txt",  'a')
    print("AffineGrid:", cycles, file=f)
    print("AffineGrid:", cycles)


def greedy_opt_add():
    B = 64
    N = 32
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "src1": np.random.uniform(-10, 10, (B, N)), "src2": np.random.uniform(-10, 10, (B, N))}
    cycles = run_kernel(create_add_program, ref_add_program, make_heuristic_opt(sizes), rtl_tester, fixed_inputs)
    f = open("naive_results.txt",  'a')
    print("Add:", cycles, file=f)
    print("Add:", cycles)



def greedy_opt_acosh():
    B = 8
    N = 8
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "src": np.random.uniform(1, 10, (B, N))}
    cycles = run_kernel(create_acosh_program, ref_acosh_program, make_heuristic_opt(sizes), rtl_tester, fixed_inputs)
    f = open("naive_results.txt",  'a')
    print("Acosh:", cycles, file=f)
    print("Acosh:", cycles)


def greedy_opt_acos():
    B = 16
    N = 16
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "src": np.random.uniform(-1, 1, (B, N))}
    cycles = run_kernel(create_acos_program, ref_acos_program, make_heuristic_opt(sizes), rtl_tester, fixed_inputs)
    f = open("naive_results.txt",  'a')
    print("Acos:", cycles, file=f)
    print("Acos:", cycles)


def greedy_opt_abs():
    B = 64
    N = 64
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "src": np.random.uniform(-10, 10, (B, N))}
    cycles = run_kernel(create_abs_program, ref_abs_program, make_heuristic_opt(sizes), rtl_tester, fixed_inputs)
    f = open("naive_results.txt",  'a')
    print("Abs:", cycles, file=f)
    print("Abs:", cycles)


def greedy_opt_log_softmax():
    B = 8
    N = 8
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "src": np.random.uniform(-10, 10, (B, N))}
    cycles = run_kernel(create_log_softmax_program, ref_log_softmax, make_heuristic_opt(sizes), rtl_tester, fixed_inputs)
    f = open("naive_results.txt",  'a')
    print("LogSoftmax:", cycles, file=f)
    print("LogSoftmax:", cycles)


def greedy_opt_softmax():
    B = 8
    N = 8
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "src": np.random.uniform(-1, 1, (B, N))}
    cycles = run_kernel(create_softmax_program, ref_softmax, make_heuristic_opt(sizes), rtl_tester, fixed_inputs)
    f = open("naive_results.txt",  'a')
    print("Softmax:", cycles, file=f)
    print("Softmax:", cycles)


def greedy_opt_softplus():
    B = 16
    N = 16
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "X": np.random.uniform(-1, 1, (B, N))}
    cycles = run_kernel(create_softplus_program, ref_softplus, make_heuristic_opt(sizes), rtl_tester, fixed_inputs)
    f = open("naive_results.txt",  'a')
    print("Softplus:", cycles, file=f)
    print("Softplus:", cycles)


def greedy_opt_mish():
    B = 8
    N = 8
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes, "X": np.random.uniform(-1, 1, (B, N))}
    cycles = run_kernel(create_mish_program, ref_mish, make_heuristic_opt(sizes), rtl_tester, fixed_inputs)
    f = open("naive_results.txt",  'a')
    print("Mish:", cycles, file=f)
    print("Mish:", cycles)
    
    
def greedy_opt_matmul():
    N = 16
    M = 16
    K = 16
    sizes = {"N": N, "M": M, "K": K}
    fixed_inputs = {**sizes}
    cycles = run_kernel(create_matmul_program, ref_matmul, make_heuristic_opt(sizes), rtl_tester, fixed_inputs)
    f = open("naive_results.txt",  'a')
    print("Matmul:", cycles, file=f)
    print("Matmul:", cycles)


def greedy_opt_gemm():
    N = 16
    M = 16
    K = 16
    sizes = {"N": N, "M": M, "K": K}
    fixed_inputs = {**sizes}
    cycles = run_kernel(create_gemm_program, ref_gemm, make_heuristic_opt(sizes), rtl_tester, fixed_inputs)
    f = open("naive_results.txt",  'a')
    print("Gemm:", cycles, file=f)
    print("Gemm:", cycles)


def greedy_opt_batchnorm():
    N = 4
    C = 4
    H = 8
    W = 8
    sizes = {
        "N": N,
        "C": C,
        "H": H,
        "W": W,
    }
    fixed_inputs = {
       **sizes,
        "X": np.random.uniform(-10, 10, (N, C, H, W)),
    }
    cycles = run_kernel(create_batchnorm_program, ref_batchnorm, make_heuristic_opt(sizes), rtl_tester, fixed_inputs)
    f = open("naive_results.txt",  'a')
    print("BatchNorm:", cycles, file=f)
    print("BatchNorm:", cycles)


def greedy_opt_layernorm():
    B = 32
    N = 32
    sizes = {"B": B, "N": N}
    fixed_inputs = {**sizes}
    cycles = run_kernel(create_layernorm_program, ref_layernorm, make_heuristic_opt(sizes), rtl_tester, fixed_inputs)
    f = open("naive_results.txt",  'a')
    print("LayerNorm:", cycles, file=f)
    print("LayerNorm:", cycles)


if __name__ == '__main__':
    f = open("naive_results.txt",  'a')
    # greedy_opt_sigmoid()
    #greedy_opt_leakyrelu()
    # greedy_opt_celu()
    # greedy_opt_relu()
    # greedy_opt_pow()
    # greedy_opt_sqrt()
    # greedy_opt_tanh()
    # greedy_opt_log()
    # greedy_opt_div()
    # greedy_opt_mul()
    # greedy_opt_sub()
    # greedy_opt_sum()
    # greedy_opt_cosh()
    # greedy_opt_exp()
    # greedy_opt_reducemean()
    # greedy_opt_argmin() ### todo:check correctness    
    # greedy_opt_argmax() ### todo:check correctness    
    greedy_opt_conv() ###
    # greedy_opt_clip()
    # # greedy_opt_blackman_window()
    # greedy_opt_cos()
    # greedy_opt_floor()
    # greedy_opt_ceil()
    # greedy_opt_bitwise_xor()
    # greedy_opt_bitwise_or()
    # greedy_opt_bitwise_and()
    # greedy_opt_bitwise_not()
    # greedy_opt_bitshift()
    # greedy_opt_averagepool()
    # # greedy_opt_atanh() #doesn't work
    # greedy_opt_atan()
    # # # greedy_opt_asinh()
    # greedy_opt_asin()
    # greedy_opt_and()
    # #greedy_opt_affine_grid()
    # greedy_opt_add()
    # # # greedy_opt_acosh()
    # greedy_opt_acos()
    # greedy_opt_abs()
    #greedy_opt_log_softmax()
    #greedy_opt_softmax()
    # greedy_opt_softplus()
    # greedy_opt_mish()
    # greedy_opt_matmul()
    # greedy_opt_gemm()
    # greedy_opt_batchnorm()
    # greedy_opt_layernorm()