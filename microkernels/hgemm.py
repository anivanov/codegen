import ctypes
import numpy as np

libmkl = ctypes.cdll.LoadLibrary('libmkl_rt.so')

CblasRowMajor = 101
CblasColMajor = 102
CblasNoTrans = 111
CblasTrans = 112
CblasConjTrans = 113

libmkl.cblas_hgemm.argtypes = [ctypes.c_int, ctypes.c_int, ctypes.c_int,
                               ctypes.c_int, ctypes.c_int, ctypes.c_int,
                               ctypes.c_void_p, np.ctypeslib.ndpointer(dtype=np.float16, flags="C_CONTIGUOUS"), ctypes.c_int,
                               np.ctypeslib.ndpointer(dtype=np.float16, flags="C_CONTIGUOUS"), ctypes.c_int, ctypes.c_void_p,
                               np.ctypeslib.ndpointer(dtype=np.float16, flags="C_CONTIGUOUS"), ctypes.c_int]

# Wrapper function for cblas_hgemm
def hgemm(alpha, A, B, beta, C):
    A = np.asarray(A, dtype=np.float16)
    B = np.asarray(B, dtype=np.float16)
    C = np.asarray(C, dtype=np.float16)
    M = A.shape[0]
    K = A.shape[1]
    N = C.shape[1]

    alpha = np.float16(alpha)
    beta = np.float16(beta)

    alpha_p = np.ctypeslib.as_ctypes(np.array([alpha], dtype=np.int16))
    beta_p = np.ctypeslib.as_ctypes(np.array([beta], dtype=np.int16))

    lda = K
    ldb = N
    ldc = N

    libmkl.cblas_hgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, M, N, K,
                       alpha_p,
                       A, lda,
                       B, ldb,
                       beta_p,
                       C, ldc)

    return C