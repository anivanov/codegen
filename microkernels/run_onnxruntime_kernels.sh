#!/bin/bash

source ~/.bashrc
conda activate main

unset OMP_PROC_BIND  # onnxruntime manages affinity itself, this will interfere
export OMP_NUM_THREADS=18
numactl -N 0 -m 0 -C 0-17 python onnxruntime_kernels.py