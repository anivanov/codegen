from kelgen.gen import parse_program, time_prof
from kelgen.evaluator import eval_once
import numpy as np
import torch
import copy
from .rmsnorm import torch_rms_norm

class RMSNormMatmulKernelCreator:
    @staticmethod
    def parse_params(size_str):
        M, K, N = [int(v) for v in size_str.split('x')]
        return {'M': M, 'K': K, 'N': N}
    
    @staticmethod
    def create_kernel(params):
        M, K, N = params.values()
        
        eps = 1e-5
        
        program_text = f"""
            Name: rmsnorm_matmul
            In: x, y, gamma
            Out: z
            Declarations:
                x f32 [{M}, {K}] heap
                y f32 [{K}, {N}] heap
                gamma f32 [{K}] heap
                z f32 [{M}, {N}] heap
                sum_squares f32 [{M}] heap
                mean_squares f32 [{M}] heap
                mean_eps f32 [{M}] heap
                sqrt_mean f32 [{M}] heap
                rsqrt_mean f32 [{M}] heap
                t1 f32 [{K}, {M}] heap
                t2 f32 [{K}, {M}] heap
            Code:
            {M} {K} sum_squares[{{0}}] += x[{{0}}, {{1}}] * x[{{0}}, {{1}}]
            {M} mean_squares[{{0}}] = sum_squares[{{0}}] / {K}
            {M} mean_eps[{{0}}] = mean_squares[{{0}}] + {eps:.6f}
            {M} sqrt_mean[{{0}}] = sqrtf(mean_eps[{{0}}])
            {M} rsqrt_mean[{{0}}] = 1 / sqrt_mean[{{0}}]
            {M} {K} t1[{{1}}, {{0}}] = x[{{0}}, {{1}}] * rsqrt_mean[{{0}}]
            {M} {K} t2[{{1}}, {{0}}] = t1[{{1}}, {{0}}] * gamma[{{1}}]
            {M} {N} {K} z[{{0}}, {{1}}] += t2[{{2}}, {{0}}] * y[{{2}}, {{1}}]
        """
        
        return parse_program(program_text)
    
    @staticmethod
    def create_reference_data(params):
        M, K, N = params.values()
        x = np.random.uniform(-1, 1, (M, K)).astype(np.float32)
        y = np.random.uniform(-1, 1, (K, N)).astype(np.float32)
        gamma = np.random.uniform(-1, 1, (K,)).astype(np.float32)
        t = torch_rms_norm(torch.from_numpy(x), torch.from_numpy(gamma))
        z = torch.matmul(t, torch.from_numpy(y)).numpy()
        return {'x': x, 'y': y, 'gamma': gamma, 'z': z}
    
    @staticmethod
    def evaluate_baseline(ref_data, min_repeats, target='native'):
        x, y, gamma, z = ref_data.values()
        tx = torch.from_numpy(x)
        ty = torch.from_numpy(y)
        tgamma = torch.from_numpy(gamma)
        
        def init():
            x[:] = copy.deepcopy(x)
            y[:] = copy.deepcopy(y)
            gamma[:] = copy.deepcopy(gamma)
        
        def torch_func(x, y, gamma):
            return torch.matmul(torch_rms_norm(x, gamma), y)
        
        torch_func_jit = torch.jit.trace(torch_func, (tx, ty, tgamma))
        
        pt_mean, pt_std, pt_runtime = time_prof(min_repeats, lambda: torch_func_jit(tx, ty, tgamma), init)
        return pt_mean, pt_std, pt_runtime
    
if __name__ == '__main__':
    eval_once(RMSNormMatmulKernelCreator, '768x512x1024', target='native')