from kelgen.gen import parse_program, time_prof
from kelgen.evaluator import eval_once
import numpy as np
import torch
import copy

class BatchedMatmulKernelCreator:
    @staticmethod
    def parse_params(size_str):
        B, M, K, N = [int(v) for v in size_str.split('x')]
        return {'B': B, 'M': M, 'K': K, 'N': N}
    
    @staticmethod
    def create_kernel(params):
        B, M, K, N = params.values()
        
        program_text = f"""
            Name: batched_matmul
            In: x, y
            Out: z
            Declarations:
                x f32 [{B}, {K}, {M}] heap
                y f32 [{B}, {K}, {N}] heap
                z f32 [{B}, {M}, {N}] heap
            Code:
            {B} {M} {N} {K} z[{{0}}, {{1}}, {{2}}] += x[{{0}}, {{3}}, {{1}}] * y[{{0}}, {{3}}, {{2}}]
        """
        
        return parse_program(program_text)
    
    @staticmethod
    def create_reference_data(params):
        B, M, K, N = params.values()
        
        x = np.random.uniform(-1, 1, (B, K, M)).astype(np.float32)
        y = np.random.uniform(-1, 1, (B, K, N)).astype(np.float32)
        z = np.matmul(x.transpose(0, 2, 1), y)
        
        return {'x': x, 'y': y, 'z': z}
    
    @staticmethod
    def evaluate_baseline(ref_data, min_repeats, target='native'):
        x = ref_data['x']
        y = ref_data['y']
        
        tx = torch.from_numpy(x)
        ty = torch.from_numpy(y)
        
        def init():
            x[:] = copy.deepcopy(x)
            y[:] = copy.deepcopy(y)
        
        pt_mean, pt_std, pt_runtime = time_prof(min_repeats, lambda: torch.einsum("bkm,bkn->bmn", tx, ty), init)
        return pt_mean, pt_std, pt_runtime

if __name__ == '__main__':
    eval_once(BatchedMatmulKernelCreator, '192x512x128x512')
    