from kelgen.gen import parse_program, time_prof
from kelgen.evaluator import eval_once
from kelgen import features
import numpy as np
import torch
import copy
import functools
import operator

class AddKernelCreator:
    @staticmethod
    def parse_params(size_str):
        N = functools.reduce(operator.mul, map(int, size_str.split('x')), 1)
        return {'N': N}
    
    @staticmethod
    def create_kernel(params):
        [N] = params.values()
        
        program_text = f"""
            Name: add
            In: x, y
            Out: z
            Declarations:
                x f32 [{N}] heap
                y f32 [{N}] heap
                z f32 [{N}] heap
            Code:
            {N} z[{{0}}] = x[{{0}}] + y[{{0}}]
        """
        
        return parse_program(program_text)
    
    @staticmethod
    def create_reference_data(params):
        [N] = params.values()
        x = np.random.uniform(-10, 10, [N]).astype(np.float32)
        y = np.random.uniform(-10, 10, [N]).astype(np.float32)
        z = x + y
        return {'x': x, 'y': y, 'z': z}
    
    @staticmethod
    def evaluate_baseline(ref_data, min_repeats=3, target='native'):
        x = torch.from_numpy(ref_data['x'])
        y = torch.from_numpy(ref_data['y'])
        
        if target == 'cuda':
            x = x.cuda()
            y = y.cuda()
        
        tx = torch.empty_like(x)
        ty = torch.empty_like(y)
        
        def init():
            tx.copy_(x)
            ty.copy_(y)
            if target == 'cuda':
                torch.cuda.synchronize()
        
        def finalize():
            if target == 'cuda':
                torch.cuda.synchronize()
                
        pt_mean, pt_std, pt_runtime = time_prof(
            min_repeats,
            lambda: tx + ty,
            init=init,
            finalize=finalize
        )
        return pt_mean, pt_std, pt_runtime

def test_add():
    eval_once(AddKernelCreator, '4096x4096')
    
def test_add_cuda():
    if not features.cuda_available():
        return
    eval_once(AddKernelCreator, '4096x4096', target='cuda')

if __name__ == '__main__':
    test_add()
    test_add_cuda()
