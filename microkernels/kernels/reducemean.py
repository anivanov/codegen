from kelgen.gen import parse_program, time_prof
from kelgen.evaluator import eval_once
import numpy as np
import torch
import copy

class ReduceMeanKernelCreator:
    @staticmethod
    def parse_params(size_str):
        M, N = [int(v) for v in size_str.split('x')]
        return {'M': M, 'N': N}
    
    @staticmethod
    def create_kernel(params):
        M, N = params.values()
        
        program_text = f"""
            Name: reducemean
            In: x
            Out: z
            Declarations:
                x f32 [{M}, {N}] heap
                t f32 [{M}] heap
                z f32 [{M}] heap
            Code:
            {M} {N} t[{{0}}] += x[{{0}}, {{1}}]
            {M} z[{{0}}] = t[{{0}}] / {N}
        """
        
        return parse_program(program_text)
    
    @staticmethod
    def create_reference_data(params):
        M, N = params.values()
        x = np.random.uniform(-10, 10, (M, N)).astype(np.float32)
        z = np.mean(x, axis=1)
        return {'x': x, 'z': z}
    
    @staticmethod
    def evaluate_baseline(ref_data, min_repeats, target='native'):
        x = ref_data['x']
        tx = torch.from_numpy(x)
        
        def init():
            x[:] = copy.deepcopy(x)
        
        pt_mean, pt_std, pt_runtime = time_prof(min_repeats, lambda: torch.mean(tx, dim=1), init)
        return pt_mean, pt_std, pt_runtime

def test_reducemean():
    eval_once(ReduceMeanKernelCreator, '4096x4096', target='native')

if __name__ == "__main__":
    test_reducemean()