from kelgen.kernels.add_rmsnorm import AddRMSNormKernelCreator
from kelgen.kernels.add import AddKernelCreator
from kelgen.kernels.batchnorm import BatchNormKernelCreator
from kelgen.kernels.bmm import BatchedMatmulKernelCreator
from kelgen.kernels.conv import ConvKernelCreator
from kelgen.kernels.layernorm import LayerNormKernelCreator
from kelgen.kernels.matmul_mul import MatmulMulKernelCreator
from kelgen.kernels.matmul_silu_mul import MatmulSiLUMulKernelCreator
from kelgen.kernels.matmul import MatmulKernelCreator
from kelgen.kernels.mul_matmul import MulMatmulKernelCreator
from kelgen.kernels.mul import MulKernelCreator
from kelgen.kernels.reducemean import ReduceMeanKernelCreator
from kelgen.kernels.relu_ffn import ReluFFNKernelCreator
from kelgen.kernels.relu import ReluKernelCreator
from kelgen.kernels.rmsnorm_matmul import RMSNormMatmulKernelCreator
from kelgen.kernels.rmsnorm import RMSNormKernelCreator
from kelgen.kernels.softmax import SoftmaxKernelCreator
from kelgen.kernels.swiglu import SwiGLUKernelCreator

KERNEL_DICT = {
    'add_rmsnorm': AddRMSNormKernelCreator,
    'add': AddKernelCreator,
    'batchnorm': BatchNormKernelCreator,
    'bmm': BatchedMatmulKernelCreator,
    'conv': ConvKernelCreator,
    'layernorm': LayerNormKernelCreator,
    'matmul_mul': MatmulMulKernelCreator,
    'matmul_silu_mul': MatmulSiLUMulKernelCreator,
    'matmul': MatmulKernelCreator,
    'mul_matmul': MulMatmulKernelCreator,
    'mul': MulKernelCreator,
    'reducemean': ReduceMeanKernelCreator,
    'relu_ffn': ReluFFNKernelCreator,
    'relu': ReluKernelCreator,
    'rmsnorm_matmul': RMSNormMatmulKernelCreator,
    'rmsnorm': RMSNormKernelCreator,
    'softmax': SoftmaxKernelCreator,
    'swiglu': SwiGLUKernelCreator,
}
