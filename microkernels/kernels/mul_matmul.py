from kelgen.gen import parse_program, time_prof
from kelgen.evaluator import eval_once
import numpy as np
import torch
import copy

class MulMatmulKernelCreator:
    @staticmethod
    def parse_params(size_str):
        M, K, N = [int(v) for v in size_str.split('x')]
        return {'M': M, 'K': K, 'N': N}
    
    @staticmethod
    def create_kernel(params):
        M, K, N = params.values()
        
        program_text = f"""
            Name: mul_matmul
            In: m, x, y
            Out: z
            Declarations:
                m f32 [{K}, {M}] heap
                x f32 [{K}, {M}] heap
                y f32 [{K}, {N}] heap
                t f32 [{K}, {M}] heap
                z f32 [{M}, {N}] heap
            Code:
            {K} {M} t[{{0}}, {{1}}] = x[{{0}}, {{1}}] * m[{{0}}, {{1}}]
            {M} {N} {K} z[{{0}}, {{1}}] += t[{{2}}, {{0}}] * y[{{2}}, {{1}}]
        """
        
        return parse_program(program_text)
    
    @staticmethod
    def create_reference_data(params):
        M, K, N = params.values()
        x = np.random.uniform(-1, 1, (K, M)).astype(np.float32)
        y = np.random.uniform(-1, 1, (K, N)).astype(np.float32)
        m = np.random.uniform(-1, 1, (K, M)).astype(np.float32)
        t = x * m
        z = np.matmul(t.T, y)
        return {'m': m, 'x': x, 'y': y, 'z': z}
    
    @staticmethod
    def evaluate_baseline(ref_data, min_repeats, target='native'):
        m, x, y, z = ref_data.values()
        tm = torch.from_numpy(m)
        tx = torch.from_numpy(x)
        ty = torch.from_numpy(y)
        
        def init():
            m[:] = copy.deepcopy(m)
            x[:] = copy.deepcopy(x)
            y[:] = copy.deepcopy(y)
        
        def torch_func(m, x, y):
            return torch.matmul((x * m).T, y)
        
        torch_func_jit = torch.jit.trace(torch_func, (tm, tx, ty))
        
        pt_mean, pt_std, pt_runtime = time_prof(min_repeats, lambda: torch_func_jit(tm, tx, ty), init)
        return pt_mean, pt_std, pt_runtime

if __name__ == "__main__":
    eval_once(MulMatmulKernelCreator, '768x512x1024', target='native')