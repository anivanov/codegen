from kelgen.gen import parse_program, time_prof
from kelgen.evaluator import eval_once
import numpy as np
import torch
import copy

@torch.jit.script
def torch_rms_norm(x, gamma):
    eps = 1e-5
    sum_squares = torch.sum(x**2, dim=1)
    mean_squares = sum_squares / x.shape[1]
    mean_eps = mean_squares + eps
    sqrt_mean = torch.sqrt(mean_eps)
    rsqrt_mean = 1 / sqrt_mean
    tmp = x * rsqrt_mean[:, None]
    y = tmp * gamma[None, :]
    return y


class RMSNormKernelCreator:
    @staticmethod
    def parse_params(size_str):
        M, N = [int(v) for v in size_str.split('x')]
        return {'M': M, 'N': N}
    
    @staticmethod
    def create_kernel(params):
        M, N = params.values()
        
        eps = 1e-5
        
        program_text = f"""
            Name: rmsnorm
            In: x, gamma
            Out: y
            Declarations:
                x f32 [{M}, {N}] heap
                y f32 [{M}, {N}] heap
                gamma f32 [{N}] heap
                sum_squares f32 [{M}] heap
                mean_squares f32 [{M}] heap
                mean_eps f32 [{M}] heap
                sqrt_mean f32 [{M}] heap
                rsqrt_mean f32 [{M}] heap
                tmp f32 [{M}, {N}] heap
            Code:
            {M} {N} sum_squares[{{0}}] += x[{{0}}, {{1}}] * x[{{0}}, {{1}}]
            {M} mean_squares[{{0}}] = sum_squares[{{0}}] / {N}
            {M} mean_eps[{{0}}] = mean_squares[{{0}}] + {eps:.6f}
            {M} sqrt_mean[{{0}}] = sqrtf(mean_eps[{{0}}])
            {M} rsqrt_mean[{{0}}] = 1 / sqrt_mean[{{0}}]
            {M} {N} tmp[{{0}}, {{1}}] = x[{{0}}, {{1}}] * rsqrt_mean[{{0}}]
            {M} {N} y[{{0}}, {{1}}] = tmp[{{0}}, {{1}}] * gamma[{{1}}]
        """
                
        return parse_program(program_text)
    
    @staticmethod
    def create_reference_data(params):
        M, N = params.values()
        x = np.random.uniform(-10, 10, (M, N)).astype(np.float32)
        gamma = np.random.uniform(-10, 10, (N,)).astype(np.float32)
        y = x / np.sqrt(np.mean(x**2, axis=1) + 1e-5)[:, None] * gamma[None, :]
        return {'x': x, 'gamma': gamma, 'y': y}
    
    @staticmethod
    def evaluate_baseline(ref_data, min_repeats, target='native'):
        M, N = ref_data['x'].shape        
        x = ref_data['x']
        gamma = ref_data['gamma']
        tx = torch.from_numpy(x)
        tgamma = torch.from_numpy(gamma)
        
        def init():
            x[:] = copy.deepcopy(x)
            gamma[:] = copy.deepcopy(gamma)
                
        pt_mean, pt_std, pt_runtime = time_prof(min_repeats, lambda: torch_rms_norm(tx, tgamma), init)
        return pt_mean, pt_std, pt_runtime
    
if __name__ == "__main__":
    eval_once(RMSNormKernelCreator, '4096x4096', target='native')