from kelgen.gen import parse_program, time_prof
from kelgen.evaluator import eval_once
import numpy as np
import torch
import copy
import functools
import operator

class ReluKernelCreator:
    @staticmethod
    def parse_params(size_str):
        N = functools.reduce(operator.mul, map(int, size_str.split('x')), 1)
        return {'N': N}
    
    @staticmethod
    def create_kernel(params):
        [N] = params.values()
        
        program_text = f"""
            Name: relu
            In: x
            Out: z
            Declarations:
                x f32 [{N}] heap
                z f32 [{N}] heap
            Code:
            {N} z[{{0}}] = fmaxf(x[{{0}}], 0)
        """
        
        return parse_program(program_text)
    
    @staticmethod
    def create_reference_data(params):
        [N] = params.values()
        nx = np.random.uniform(-10, 10, [N]).astype(np.float32)
        tx = torch.from_numpy(nx)
        tz = torch.nn.functional.relu(tx)
        nz = tz.numpy()
        return {'x': nx, 'z': nz}
    
    @staticmethod
    def evaluate_baseline(ref_data, min_repeats, target='native'):
        x = ref_data['x']
        tx = torch.from_numpy(x)
        
        def init():
            x[:] = copy.deepcopy(x)
        
        pt_mean, pt_std, pt_runtime = time_prof(min_repeats, lambda: torch.nn.functional.relu(tx), init)
        return pt_mean, pt_std, pt_runtime


def test_relu():
    eval_once(ReluKernelCreator, '4096x4096', target='native')

if __name__ == "__main__":
    test_relu()
