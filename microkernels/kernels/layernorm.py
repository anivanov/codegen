from kelgen.gen import parse_program, time_prof
from kelgen.evaluator import eval_once
import numpy as np
import torch
import copy

class LayerNormKernelCreator:
    @staticmethod
    def parse_params(size_str):
        M, N = [int(v) for v in size_str.split('x')]
        return {'M': M, 'N': N}
    
    @staticmethod
    def create_kernel(params):
        M = params['M']
        N = params['N']
        
        program_text = f"""
            Name: layernorm
            In: src, gamma, beta
            Out: dst
            Declarations:
                src f32 [{M}, {N}] heap
                gamma f32 [{N}] heap
                beta f32 [{N}] heap
                dst f32 [{M}, {N}] heap
                m f32 [{M}] heap
                mu f32 [{M}] heap
                diff f32 [{M}, {N}] heap
                q f32 [{M}] heap
                tmp f32 [{M}, {N}] heap
                qeps f32 [{M}] heap
                sigma f32 [{M}] heap
            Code:
            {M} {N} m[{{0}}] += src[{{0}}, {{1}}]
            {M} mu[{{0}}] = m[{{0}}] / {N}
            {M} {N} diff[{{0}}, {{1}}] = src[{{0}}, {{1}}] - mu[{{0}}]
            {M} {N} q[{{0}}] += diff[{{0}}, {{1}}] * diff[{{0}}, {{1}}]
            {M} {N} tmp[{{0}}, {{1}}] = diff[{{0}}, {{1}}] * gamma[{{1}}]
            {M} qeps[{{0}}] = q[{{0}}] * {1./(N - 1.)} + 0.00001
            {M} sigma[{{0}}] = rsqrtf(qeps[{{0}}])
            {M} {N} dst[{{0}}, {{1}}] = sigma[{{0}}] * tmp[{{0}}, {{1}}] + beta[{{1}}]
        """
        
        return parse_program(program_text)
    
    @staticmethod
    def create_reference_data(params):
        M = params['M']
        N = params['N']
       
        src = np.random.uniform(-1, 1, (M, N)).astype(np.float32)
        gamma = np.random.uniform(-1, 1, (N,)).astype(np.float32)
        beta = np.random.uniform(-1, 1, (N,)).astype(np.float32)
        tsrc = torch.from_numpy(src)
        tgamma  = torch.from_numpy(gamma)
        tbeta = torch.from_numpy(beta)
        
        tdst = torch.nn.functional.layer_norm(tsrc, [N], weight=tgamma, bias=tbeta, eps=1e-5)
        dst = tdst.numpy()
        
        ref_data = {'src': src, 'gamma': gamma, 'beta': beta, 'dst': dst}
        return ref_data
    
    @staticmethod
    def evaluate_baseline(ref_data, min_repeats, target='native'):
        src = ref_data['src']
        gamma = ref_data['gamma']
        beta = ref_data['beta']
        
        tsrc = torch.from_numpy(src)
        tgamma  = torch.from_numpy(gamma)
        tbeta = torch.from_numpy(beta)
        
        def init():
            src[:] = copy.deepcopy(src)
            gamma[:] = copy.deepcopy(gamma)
            beta[:] = copy.deepcopy(beta)
        
        N = tsrc.shape[1]
        pt_mean, pt_std, pt_runtime = time_prof(min_repeats, lambda: torch.nn.functional.layer_norm(tsrc, [N], weight=tgamma, bias=tbeta, eps=1e-5), init)
        return pt_mean, pt_std, pt_runtime

def test_layernorm():
    eval_once(LayerNormKernelCreator, '4096x4096', target='native')


if __name__ == "__main__":
    test_layernorm()
