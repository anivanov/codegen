from kelgen.gen import parse_program, time_prof
from kelgen.evaluator import eval_once
import numpy as np
import torch
import copy

def pooling_out_dim(input, kernel, stride, pad, dilation):
    return (input + 2 * pad - (dilation * (kernel - 1) + 1)) // stride + 1


class ConvKernelCreator:
    @staticmethod
    def parse_params(size_str):
        N, M, C, H_in, W_in, K = [int(v) for v in size_str.split('x')]
        return {'N': N, 'M': M, 'C': C, 'H_in': H_in, 'W_in': W_in, 'K': K}
    
    @staticmethod
    def create_kernel(params):
        N, M, C, H_in, W_in, K = params.values()
        
        H = pooling_out_dim(input=H_in, kernel=K, stride=1, pad=0, dilation=1)
        W = pooling_out_dim(input=W_in, kernel=K, stride=1, pad=0, dilation=1)
        
        program_text = f"""
            Name: conv2d
            In: input, weight
            Out: output
            Declarations:
                input f32 [{N}, {C}, {H_in}, {W_in}] heap
                weight f32 [{M}, {C}, {K}, {K}] heap
                output f32 [{N}, {M}, {H}, {W}] heap
            Code:
            {N} {M} {H} {W} {C} {K} {K} output[{{0}}, {{1}}, {{2}}, {{3}}] += input[{{0}}, {{4}}, {{2}} + {{5}}, {{3}} + {{6}}] * weight[{{1}}, {{4}}, {{5}}, {{6}}]
        """
        
        return parse_program(program_text)
    
    @staticmethod
    def create_reference_data(params):
        N, M, C, H_in, W_in, K = params.values()
        input = np.random.uniform(-1, 1, (N, C, H_in, W_in)).astype(np.float32)
        weight = np.random.uniform(-1, 1, (M, C, K, K)).astype(np.float32)
        tinput = torch.from_numpy(input)
        tweight = torch.from_numpy(weight)
        toutput = torch.nn.functional.conv2d(tinput, tweight)
        output = toutput.numpy()
        return {'input': input, 'weight': weight, 'output': output}
    
    @staticmethod
    def evaluate_baseline(ref_data, min_repeats, target='native'):
        input, weight, output = ref_data.values()
        tinput = torch.from_numpy(input)
        tweight = torch.from_numpy(weight)
        
        def init():
            input[:] = copy.deepcopy(input)
            weight[:] = copy.deepcopy(weight)
        
        pt_mean, pt_std, pt_runtime = time_prof(min_repeats, lambda: torch.nn.functional.conv2d(tinput, tweight), init)
        return pt_mean, pt_std, pt_runtime

if __name__ == "__main__":
    eval_once(ConvKernelCreator, "8x64x64x56x56x3")