from kelgen.gen import parse_program, time_prof
from kelgen.evaluator import eval_once
from kelgen import features
import numpy as np
import torch
import copy
from kelgen.kernels.rmsnorm import torch_rms_norm


class AddRMSNormKernelCreator:
    @staticmethod
    def parse_params(size_str):
        M, N = [int(v) for v in size_str.split('x')]
        return {'M': M, 'N': N}
    
    @staticmethod
    def create_kernel(params):
        M, N = params.values()
        
        eps = 1e-5
        
        program_text = f"""
            Name: add_rmsnorm
            In: a, x, gamma
            Out: y
            Declarations:
                a f32 [{M}, {N}] heap
                x f32 [{M}, {N}] heap
                gamma f32 [{N}] heap
                y f32 [{M}, {N}] heap
                ax f32 [{M}, {N}] heap
                sum_squares f32 [{M}] heap
                mean_squares f32 [{M}] heap
                mean_eps f32 [{M}] heap
                sqrt_mean f32 [{M}] heap
                rsqrt_mean f32 [{M}] heap
                tmp f32 [{M}, {N}] heap
            Code:
            {M} {N} ax[{{0}}, {{1}}] = a[{{0}}, {{1}}] + x[{{0}}, {{1}}]
            {M} {N} sum_squares[{{0}}] += ax[{{0}}, {{1}}] * ax[{{0}}, {{1}}]
            {M} mean_squares[{{0}}] = sum_squares[{{0}}] / {N}
            {M} mean_eps[{{0}}] = mean_squares[{{0}}] + {eps:.6f}
            {M} sqrt_mean[{{0}}] = sqrtf(mean_eps[{{0}}])
            {M} rsqrt_mean[{{0}}] = 1 / sqrt_mean[{{0}}]
            {M} {N} tmp[{{0}}, {{1}}] = ax[{{0}}, {{1}}] * rsqrt_mean[{{0}}]
            {M} {N} y[{{0}}, {{1}}] = tmp[{{0}}, {{1}}] * gamma[{{1}}]
        """
        
        return parse_program(program_text)
    
    @staticmethod
    def create_reference_data(params):
        M, N = params.values()
        a = np.random.uniform(-10, 10, (M, N)).astype(np.float32)
        x = np.random.uniform(-10, 10, (M, N)).astype(np.float32)
        y = np.random.uniform(-10, 10, (M, N)).astype(np.float32)
        gamma = np.random.uniform(-10, 10, (N,)).astype(np.float32)
        ax = a + x
        y = ax / np.sqrt(np.mean(ax**2, axis=1) + 1e-5)[:, None] * gamma[None, :]
        
        return {'a': a, 'x': x, 'gamma': gamma, 'y': y}
    
    @staticmethod
    def evaluate_baseline(ref_data, min_repeats, target='native'):
        a, x, gamma, y = ref_data.values()
        ta = torch.from_numpy(a)
        tx = torch.from_numpy(x)
        tgamma = torch.from_numpy(gamma)
        
        def init():
            a[:] = copy.deepcopy(a)
            x[:] = copy.deepcopy(x)
            gamma[:] = copy.deepcopy(gamma)
        
        @torch.jit.script
        def torch_func(a, x, gamma):
            ax = a + x
            y = torch_rms_norm(ax, gamma)
            return y
                
        pt_mean, pt_std, pt_runtime = time_prof(min_repeats, lambda: torch_func(ta, tx, tgamma), init)
        return pt_mean, pt_std, pt_runtime


def test_addrmsnorm():
    eval_once(AddRMSNormKernelCreator, '4096x4096')

def test_addrmsnorm_cuda():
    if not features.cuda_available():
        return
    eval_once(AddRMSNormKernelCreator, '4096x4096', target='cuda')

if __name__ == "__main__":
    test_addrmsnorm()