from kelgen.gen import parse_program, time_prof
from kelgen.evaluator import eval_once
import numpy as np
import torch
import copy
import functools
import operator

class MulKernelCreator:
    @staticmethod
    def parse_params(size_str):
        N = functools.reduce(operator.mul, map(int, size_str.split('x')), 1)
        return {'N': N}
    
    @staticmethod
    def create_kernel(params):
        [N] = params.values()
        
        program_text = f"""
            Name: mul
            In: x, y
            Out: z
            Declarations:
                x f32 [{N}] heap
                y f32 [{N}] heap
                z f32 [{N}] heap
            Code:
            {N} z[{{0}}] = x[{{0}}] * y[{{0}}]
        """
        
        return parse_program(program_text)
    
    @staticmethod
    def create_reference_data(params):
        [N] = params.values()
        x = np.random.uniform(-10, 10, [N]).astype(np.float32)
        y = np.random.uniform(-10, 10, [N]).astype(np.float32)
        z = x * y
        return {'x': x, 'y': y, 'z': z}
    
    @staticmethod
    def evaluate_baseline(ref_data, min_repeats, target='native'):
        x = ref_data['x']
        y = ref_data['y']
        tx = torch.from_numpy(x)
        ty = torch.from_numpy(y)
        
        def init():
            x[:] = copy.deepcopy(x)
            y[:] = copy.deepcopy(y)
        
        pt_mean, pt_std, pt_runtime = time_prof(min_repeats, lambda: tx * ty, init)
        return pt_mean, pt_std, pt_runtime
    
if __name__ == "__main__":
    eval_once(MulKernelCreator, '4096x4096', target='native')