from kelgen.gen import parse_program, time_prof
from kelgen.evaluator import eval_once
import numpy as np
import torch
import copy


class ReluFFNKernelCreator:
    @staticmethod
    def parse_params(size_str):
        # size_str is of format: batch x sequence x embedding x hidden
        B, S, E, H = list(map(int, size_str.split('x')))
        return {'B': B, 'S': S, 'E': E, 'H': H}
    
    @staticmethod
    def create_kernel(params):
        B, S, E, H = params.values()
        
        program_text = f"""
            Name: relu_ffn
            In: x, w1, b1, w2, b2
            Out: y
            Declarations:
                x f32 [{B}, {S}, {E}] heap
                w1 f32 [{E}, {H}] heap
                b1 f32 [{H}] heap
                w2 f32 [{H}, {E}] heap
                b2 f32 [{E}] heap
                t1 f32 [{B}, {S}, {H}] heap
                t2 f32 [{B}, {S}, {H}] heap
                t3 f32 [{B}, {S}, {H}] heap
                t4 f32 [{B}, {S}, {E}] heap
                y f32 [{B}, {S}, {E}] heap
            Code:
            {B} {S} {E} {H} t1[{{0}}, {{1}}, {{3}}] += x[{{0}}, {{1}}, {{2}}] * w1[{{2}}, {{3}}]
            {B} {S} {H} t2[{{0}}, {{1}}, {{2}}] = t1[{{0}}, {{1}}, {{2}}] + b1[{{2}}]
            {B} {S} {H} t3[{{0}}, {{1}}, {{2}}] = fmaxf(t2[{{0}}, {{1}}, {{2}}], 0)
            {B} {S} {H} {E} t4[{{0}}, {{1}}, {{3}}] += t3[{{0}}, {{1}}, {{2}}] * w2[{{2}}, {{3}}]
            {B} {S} {E} y[{{0}}, {{1}}, {{2}}] = t4[{{0}}, {{1}}, {{2}}] + b2[{{2}}]
        """
        
        return parse_program(program_text)
    
    @staticmethod
    def create_reference_data(params):
        B, S, E, H = params.values()
        x = np.random.uniform(-1, 1, (B, S, E)).astype(np.float32)
        std = np.sqrt(2 / (E + H))
        w1 = np.random.normal(0, std, (E, H)).astype(np.float32)
        b1 = np.random.normal(0, std, (H,)).astype(np.float32)
        w2 = np.random.normal(0, std, (H, E)).astype(np.float32)
        b2 = np.random.normal(0, std, (E,)).astype(np.float32)
        
        t1 = np.einsum("bse,eh->bsh", x, w1)
        t2 = np.maximum(t1 + b1, 0)
        t3 = np.einsum("bsh,he->bse", t2, w2)
        y = t3 + b2
        
        return {'x': x, 'w1': w1, 'b1': b1, 'w2': w2, 'b2': b2, 'y': y}
    
    @staticmethod
    def evaluate_baseline(ref_data, min_repeats, target='native'):
        x, w1, b1, w2, b2, y = ref_data.values()
        tx = torch.from_numpy(x)
        tw1 = torch.from_numpy(w1)
        tb1 = torch.from_numpy(b1)
        tw2 = torch.from_numpy(w2)
        tb2 = torch.from_numpy(b2)
        
        def init():
            x[:] = copy.deepcopy(x)
            w1[:] = copy.deepcopy(w1)
            b1[:] = copy.deepcopy(b1)
            w2[:] = copy.deepcopy(w2)
            b2[:] = copy.deepcopy(b2)
        
        def torch_func(x, w1, b1, w2, b2):
            return torch.nn.functional.relu(torch.einsum("bse,eh->bsh", x, w1) + b1) @ w2 + b2
        
        torch_func_jit = torch.jit.script(torch_func)
        
        pt_mean, pt_std, pt_runtime = time_prof(min_repeats, lambda: torch_func_jit(tx, tw1, tb1, tw2, tb2), init)
        return pt_mean, pt_std, pt_runtime
    
if __name__ == '__main__':
    eval_once(ReluFFNKernelCreator, '8x64x112x112', target='native')