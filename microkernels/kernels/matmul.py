from kelgen.gen import parse_program, time_prof
from kelgen.evaluator import eval_once
import numpy as np
import torch
import copy

class MatmulKernelCreator:
    @staticmethod
    def parse_params(size_str):
        M, K, N = [int(v) for v in size_str.split('x')]
        return {'M': M, 'K': K, 'N': N}
    
    @staticmethod
    def create_kernel(params):
        M = params['M']
        N = params['N']
        K = params['K']
        
        program_text = f"""
            Name: matmul
            In: x, y
            Out: z
            Declarations:
                x f32 [{K}, {M}] heap
                y f32 [{K}, {N}] heap
                z f32 [{M}, {N}] heap
            Code:
            {M} {N} {K} z[{{0}}, {{1}}] += x[{{2}}, {{0}}] * y[{{2}}, {{1}}]
        """
        
        return parse_program(program_text)
    
    @staticmethod
    def create_reference_data(params):
        M = params['M']
        N = params['N']
        K = params['K']
        
        x = np.random.uniform(-1, 1, (K, M)).astype(np.float32)
        y = np.random.uniform(-1, 1, (K, N)).astype(np.float32)
        z = np.matmul(x.T, y)
        
        return {'x': x, 'y': y, 'z': z}
    
    @staticmethod
    def evaluate_baseline(ref_data, min_repeats, target='native'):
        x = ref_data['x']
        y = ref_data['y']
        
        tx = torch.from_numpy(x)
        ty = torch.from_numpy(y)
        
        def init():
            x[:] = copy.deepcopy(x)
            y[:] = copy.deepcopy(y)
        
        pt_mean, pt_std, pt_runtime = time_prof(min_repeats, lambda: torch.einsum("km,kn->mn", tx, ty), init)
        return pt_mean, pt_std, pt_runtime
    
if __name__ == "__main__":
    eval_once(MatmulKernelCreator, '768x512x1024', target='native')