from kelgen.gen import parse_program, time_prof
from kelgen.evaluator import eval_once
import numpy as np
import torch
import copy

class SwiGLUKernelCreator:
    @staticmethod
    def parse_params(size_str):
        # size_str is of format: batch x sequence x embedding x hidden
        B, S, E, H = list(map(int, size_str.split('x')))
        return {'B': B, 'S': S, 'E': E, 'H': H}
    
    @staticmethod
    def create_kernel(params):
        B, S, E, H = params.values()
        
        program_text = f"""
            Name: swiglu
            In: x, w1, w2, w3
            Out: y
            Declarations:
                x f32 [{B}, {S}, {E}] heap
                w1 f32 [{E}, {H}] heap
                w2 f32 [{H}, {E}] heap
                w3 f32 [{E}, {H}] heap
                a f32 [{B}, {S}, {H}] heap
                ma f32 [{B}, {S}, {H}] heap
                b f32 [{B}, {S}, {H}] heap
                ea f32 [{B}, {S}, {H}] heap
                ea1 f32 [{B}, {S}, {H}] heap
                silu f32 [{B}, {S}, {H}] heap
                c f32 [{B}, {S}, {H}] heap
                y f32 [{B}, {S}, {E}] heap
            Code:
            {B} {S} {E} {H} a[{{0}}, {{1}}, {{3}}] += x[{{0}}, {{1}}, {{2}}] * w1[{{2}}, {{3}}]
            {B} {S} {E} {H} b[{{0}}, {{1}}, {{3}}] += x[{{0}}, {{1}}, {{2}}] * w3[{{2}}, {{3}}]
            {B} {S} {H} ma[{{0}}, {{1}}, {{2}}] = - a[{{0}}, {{1}}, {{2}}]
            {B} {S} {H} ea[{{0}}, {{1}}, {{2}}] = expf(ma[{{0}}, {{1}}, {{2}}])
            {B} {S} {H} ea1[{{0}}, {{1}}, {{2}}] = ea[{{0}}, {{1}}, {{2}}] + 1
            {B} {S} {H} silu[{{0}}, {{1}}, {{2}}] = a[{{0}}, {{1}}, {{2}}] / ea1[{{0}}, {{1}}, {{2}}]
            {B} {S} {H} c[{{0}}, {{1}}, {{2}}] = silu[{{0}}, {{1}}, {{2}}] * b[{{0}}, {{1}}, {{2}}]
            {B} {S} {H} {E} y[{{0}}, {{1}}, {{3}}] += c[{{0}}, {{1}}, {{2}}] * w2[{{2}}, {{3}}]
        """
        
        return parse_program(program_text)
    
    @staticmethod
    def create_reference_data(params):
        B, S, E, H = params.values()
        x = np.random.uniform(-1, 1, (B, S, E)).astype(np.float32)
        std = np.sqrt(2 / (E + H)) # xavier initialization
        w1 = np.random.normal(0, std, (E, H)).astype(np.float32)
        w2 = np.random.normal(0, std, (H, E)).astype(np.float32)
        w3 = np.random.normal(0, std, (E, H)).astype(np.float32)
        
        T1 = np.einsum("bse,eh->bsh", x, w1)
        T2 = torch.nn.functional.silu(torch.from_numpy(T1)).numpy()
        T3 = np.einsum("bse,eh->bsh", x, w3)
        T4 = T2 * T3
        y = np.einsum("bsh,he->bse", T4, w2)
                
        return {'x': x, 'w1': w1, 'w2': w2, 'w3': w3, 'y': y}

    @staticmethod
    def evaluate_baseline(ref_data, min_repeats, target='native'):
        x, w1, w2, w3, y = ref_data.values()
        tx = torch.from_numpy(x)
        tw1 = torch.from_numpy(w1)
        tw2 = torch.from_numpy(w2)
        tw3 = torch.from_numpy(w3)
        
        def init():
            x[:] = copy.deepcopy(x)
            w1[:] = copy.deepcopy(w1)
            w2[:] = copy.deepcopy(w2)
            w3[:] = copy.deepcopy(w3)
        
        def torch_func(x, w1, w2, w3):
            return torch.einsum("bsh,he->bse", torch.nn.functional.silu(torch.einsum("bse,eh->bsh", x, w1)) * torch.einsum("bse,eh->bsh", x, w3), w2)
        
        torch_func_jit = torch.jit.trace(torch_func, (tx, tw1, tw2, tw3))
        
        pt_mean, pt_std, pt_runtime = time_prof(min_repeats, lambda: torch_func_jit(tx, tw1, tw2, tw3), init)
        return pt_mean, pt_std, pt_runtime

if __name__ == '__main__':
    eval_once(SwiGLUKernelCreator, '2x256x2048x448')