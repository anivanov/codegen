from kelgen.gen import parse_program, time_prof
from kelgen.evaluator import eval_once
import numpy as np
import torch
import copy

class SoftmaxKernelCreator:
    @staticmethod
    def parse_params(size_str):
        M, N = [int(v) for v in size_str.split('x')]
        return {'M': M, 'N': N}
    
    @staticmethod
    def create_kernel(params):
        M, N = params.values()
        
        program_text = f"""
            Name: softmax
            In: x
            Out: z
            Declarations:
                x f32 [{M}, {N}] heap
                z f32 [{M}, {N}] heap
                max_val f32 [{M}] heap
                diff f32 [{M}, {N}] heap
                exp_diff f32 [{M}, {N}] heap
                sum_val f32 [{M}] heap
            Code:
            {M} {N} max_val[{{0}}] fmaxf= (x[{{0}}, {{1}}])
            {M} {N} diff[{{0}}, {{1}}] = x[{{0}}, {{1}}] - max_val[{{0}}]
            {M} {N} exp_diff[{{0}}, {{1}}] = expf(diff[{{0}}, {{1}}])
            {M} {N} sum_val[{{0}}] += exp_diff[{{0}}, {{1}}]
            {M} {N} z[{{0}}, {{1}}] = exp_diff[{{0}}, {{1}}] / sum_val[{{0}}]
        """
        
        return parse_program(program_text)
    
    @staticmethod
    def create_reference_data(params):
        M, N = params.values()
        x = np.random.uniform(-10, 10, (M, N)).astype(np.float32)
        z = torch.nn.functional.softmax(torch.from_numpy(x), dim=-1).numpy()
        return {'x': x, 'z': z}
    
    @staticmethod
    def evaluate_baseline(ref_data, min_repeats, target='native'):
        x = ref_data['x']
        tx = torch.from_numpy(x)
        
        def init():
            x[:] = copy.deepcopy(x)
        
        pt_mean, pt_std, pt_runtime = time_prof(min_repeats, lambda: torch.nn.functional.softmax(tx, dim=-1), init)
        return pt_mean, pt_std, pt_runtime

if __name__ == "__main__":
    eval_once(SoftmaxKernelCreator, '4096x4096', target='native')