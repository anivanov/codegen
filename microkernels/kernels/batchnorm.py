from kelgen.gen import parse_program, time_prof
from kelgen.evaluator import eval_once
from kelgen import features
import numpy as np
import torch
import copy

class BatchNormKernelCreator:
    @staticmethod
    def parse_params(size_str):
        N, C, H, W = [int(v) for v in size_str.split('x')]
        return {'N': N, 'C': C, 'H': H, 'W': W}
    
    @staticmethod
    def create_kernel(params):
        N = params['N']
        C = params['C']
        H = params['H']
        W = params['W']
        
        epsilon = 1e-4
        program_text = f"""
            Name: batchnorm_inference
            In: x, mean, var, scale, bias
            Out: y
            Declarations:
                x f32 [{N}, {C}, {H}, {W}] heap
                y f32 [{N}, {C}, {H}, {W}] heap
                mean f32 [{C}] heap
                var f32 [{C}] heap
                scale f32 [{C}] heap
                bias f32 [{C}] heap
                var_eps f32 [{C}] heap
                sqrt_var_eps f32 [{C}] heap
                alpha f32 [{C}] heap
                beta f32 [{C}] heap
            Code:
            {C} var_eps[{{0}}] = var[{{0}}] + {epsilon:.6f}
            {C} sqrt_var_eps[{{0}}] = sqrtf(var_eps[{{0}}])
            {C} alpha[{{0}}] = scale[{{0}}] / sqrt_var_eps[{{0}}]
            {C} beta[{{0}}] = alpha[{{0}}] * mean[{{0}}] - bias[{{0}}]
            {N} {C} {H} {W} y[{{0}}, {{1}}, {{2}}, {{3}}] = alpha[{{1}}] * x[{{0}}, {{1}}, {{2}}, {{3}}] - beta[{{1}}]
        """
        
        return parse_program(program_text)
    
    @staticmethod
    def create_reference_data(params):
        N = params['N']
        C = params['C']
        H = params['H']
        W = params['W']
        
        x = np.random.uniform(-1, 1, (N, C, H, W)).astype(np.float32)
        scale = np.random.uniform(-1, 1, (C,)).astype(np.float32)
        bias = np.random.uniform(-1, 1, (C,)).astype(np.float32)
        mean = np.random.uniform(-1, 1, (C,)).astype(np.float32)
        var = np.random.uniform(-1, 1, (C,)).astype(np.float32)
        
        tx = torch.from_numpy(x)
        tscale = torch.from_numpy(scale)
        tbias = torch.from_numpy(bias)
        tmean = torch.from_numpy(mean)
        tvar = torch.from_numpy(var)
    
        epsilon = 1e-4
        momentum = 0.9  # irrelevant if training=False
        ty = torch.nn.functional.batch_norm(tx, tmean, tvar, weight=tscale, bias=tbias, training=False, momentum=1-momentum, eps=epsilon)
        y = ty.numpy()
        
        return {'x': x, 'mean': mean, 'var': var, 'scale': scale, 'bias': bias, 'y': y}
    
    @staticmethod
    def evaluate_baseline(ref_data, min_repeats, target='native'):
        epsilon = 1e-4
        momentum = 0.9  # irrelevant for inference (training=False)
        x = torch.from_numpy(ref_data['x'])
        scale = torch.from_numpy(ref_data['scale'])
        bias = torch.from_numpy(ref_data['bias'])
        mean = torch.from_numpy(ref_data['mean'])
        var = torch.from_numpy(ref_data['var'])
        
        if target == 'cuda':
            x = x.cuda()
            scale = scale.cuda()
            bias = bias.cuda()
            mean = mean.cuda()
            var = var.cuda()
        
        tx = torch.empty_like(x)
        tscale = torch.empty_like(scale)
        tbias = torch.empty_like(bias)
        tmean = torch.empty_like(mean)
        tvar = torch.empty_like(var)
        
        def init():
            tx.copy_(x)
            tscale.copy_(scale)
            tbias.copy_(bias)
            tmean.copy_(mean)
            tvar.copy_(var)
            if target == 'cuda':
                torch.cuda.synchronize()
                
        def finalize():
            if target == 'cuda':
                torch.cuda.synchronize()
        
        pt_mean, pt_std, pt_runtime = time_prof(
            min_repeats,
            lambda: torch.nn.functional.batch_norm(tx, tmean, tvar, weight=tscale, bias=tbias, training=False, momentum=1-momentum, eps=epsilon),
            init=init,
            finalize=finalize
        )
        return pt_mean, pt_std, pt_runtime


def test_batchnorm():
    eval_once(BatchNormKernelCreator, '8x64x300x300', target='native')
    
def test_batchnorm_cuda():
    if not features.cuda_available():
        return
    eval_once(BatchNormKernelCreator, '8x64x300x300', target='cuda')

if __name__ == "__main__":
    test_batchnorm()
    test_batchnorm_cuda()
