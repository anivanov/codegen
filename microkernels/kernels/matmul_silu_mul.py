from kelgen.gen import parse_program, time_prof
from kelgen.evaluator import eval_once
import numpy as np
import torch
import copy

class MatmulSiLUMulKernelCreator:
    @staticmethod
    def parse_params(size_str):
        M, K, N = [int(v) for v in size_str.split('x')]
        return {'M': M, 'K': K, 'N': N}
    
    @staticmethod
    def create_kernel(params):
        M, K, N = params.values()
        
        program_text = f"""
            Name: matmul_silu_mul
            In: x, y, m
            Out: z
            Declarations:
                x f32 [{K}, {M}] heap
                y f32 [{K}, {N}] heap
                m f32 [{M}, {N}] heap
                t f32 [{M}, {N}] heap
                et f32 [{M}, {N}] heap
                et1 f32 [{M}, {N}] heap
                sigma f32 [{M}, {N}] heap
                silu f32 [{M}, {N}] heap
                z f32 [{M}, {N}] heap
            Code:
            {M} {N} {K} t[{{0}}, {{1}}] += x[{{2}}, {{0}}] * y[{{2}}, {{1}}]
            {M} {N} et[{{0}}, {{1}}] = expf(t[{{0}}, {{1}}])
            {M} {N} et1[{{0}}, {{1}}] = et[{{0}}, {{1}}] + 1
            {M} {N} sigma[{{0}}, {{1}}] = et[{{0}}, {{1}}] / et1[{{0}}, {{1}}]
            {M} {N} silu[{{0}}, {{1}}] = t[{{0}}, {{1}}] * sigma[{{0}}, {{1}}]
            {M} {N} z[{{0}}, {{1}}] = silu[{{0}}, {{1}}] * m[{{0}}, {{1}}]
        """
        
        return parse_program(program_text)
    
    @staticmethod
    def create_reference_data(params):
        M, K, N = params.values()
        x = np.random.uniform(-1, 1, (K, M)).astype(np.float32)
        y = np.random.uniform(-1, 1, (K, N)).astype(np.float32)
        m = np.random.uniform(-1, 1, (M, N)).astype(np.float32)
        t = np.matmul(x.T, y)
        z = (torch.nn.functional.silu(torch.from_numpy(t)) * m).numpy()
        return {'x': x, 'y': y, 'm': m, 'z': z}
    
    @staticmethod
    def evaluate_baseline(ref_data, min_repeats, target='native'):
        x, y, m, z = ref_data.values()
        tx = torch.from_numpy(x)
        ty = torch.from_numpy(y)
        tm = torch.from_numpy(m)
        
        def init():
            x[:] = copy.deepcopy(x)
            y[:] = copy.deepcopy(y)
            m[:] = copy.deepcopy(m)
        
        def torch_func(x, y, m):
            return torch.nn.functional.silu(torch.matmul(x.T, y)) * m
        
        torch_func_jit = torch.jit.trace(torch_func, (tx, ty, tm))
        
        pt_mean, pt_std, pt_runtime = time_prof(min_repeats, lambda: torch_func_jit(tx, ty, tm), init)
        return pt_mean, pt_std, pt_runtime

if __name__ == "__main__":
    eval_once(MatmulSiLUMulKernelCreator, "768x512x1024", target='native')