#!/usr/bin/env python


from enum import Enum
from textwrap import dedent, indent
from collections.abc import Sequence
import re
import copy
import subprocess
from itertools import combinations, count
import random
import numpy as np
import shutil
from pathlib import Path
import math
import os
from time import time
import glob
import timeit
from typing import Dict, Iterable, List, Set, Tuple, Union
import ast
import warnings
import pyparsing as pp
import hashlib
import functools
import resource
import tempfile
import psutil
from kelgen import features


MAX_SSR_DEPTH = 4
MAX_SSR_REGS = 3


class DataType:
    def __init__(self, dtype_literal: str, dtype_size: int, num_scalars: int):
        self.literal = dtype_literal
        self.size = dtype_size
        self.scalars = num_scalars
    
    def total_bits(self):
        return self.size * self.scalars
    
    @staticmethod
    def from_string(s: str):
        ss = s.split('x')
        if len(ss) == 1:
            base = ss[0]
            num_scalars = 1
        elif len(ss) == 2:
            base = ss[0]
            num_scalars = int(ss[1])
        else:
            raise ValueError(f"Invalid data type string: {s}")
        dtype_literal = base[0]
        dtype_size = int(base[1:])
        return DataType(dtype_literal, dtype_size, num_scalars)
    
    @staticmethod
    def canonicalize(dtype):
        if isinstance(dtype, DataType):
            return dtype
        else:
            return DataType.from_string(dtype)
        
    def is_vector(self):
        return self.scalars > 1
    
    def scalar(self):
        return DataType(self.literal, self.size, 1)
    
    def __repr__(self):
        return f"DataType({self})"
    
    def __str__(self):
        if self.is_vector():
            return f"{self.literal}{self.size}x{self.scalars}"
        else:
            return f"{self.literal}{self.size}"
        
    def __eq__(self, other):
        return self.literal == other.literal and self.size == other.size and self.scalars == other.scalars
    
    def __hash__(self):
        return hash((self.literal, self.size, self.scalars))


class Buffer:
    def __init__(self, dims, dtype):
        self.dims = [canonicalize_access(d) for d in dims]
        self.dtype = DataType.canonicalize(dtype)
        self.used_dims = [True for _ in self.dims]
        self.allocation = "heap"

    def __getitem__(self, indices):
        if not isinstance(indices, Sequence):
            indices = [indices]
        assert len(indices) == len(self.dims)
        return Array(self, indices)

    def as_scalar(self):
        assert not self.dims
        return Array(self, [])

    def __repr__(self):
        return (
            f"Buffer(dims={self.dims}, used_dims={self.used_dims}, dtype={self.dtype})"
        )

    def __eq__(self, other):
        return (
            self.dims == other.dims
            and self.dtype == other.dtype
            and self.used_dims == other.used_dims
        )

    def __hash__(self):
        return hash((tuple(self.dims), self.dtype, tuple(self.used_dims)))

    def size_elems(self):
        materialized_dims = [dim.value for dim, used in zip(self.dims, self.used_dims) if used]
        if self.dtype.scalars > 1:
            # last dimension represents vector dimension and should not be counted
            materialized_dims = materialized_dims[:-1]
        return functools.reduce(lambda x, y: x * y, materialized_dims, 1)

    def size_bytes(self):
        elems = self.size_elems()
        return elems * self.dtype_bytes()

    def dtype_bytes(self):
        dtype_bits = self.dtype.total_bits()
        assert dtype_bits % 8 == 0
        return dtype_bits // 8

    def to_dict(self) -> Dict:
        return {
            "obj": self.__class__.__name__,
            "dims": [dim.to_dict() for dim in self.dims],
            "dtype": self.dtype,
            "used_dims": self.used_dims,
            "allocation": self.allocation,
            "transforms": self.transform_annotations,
        }
        


NP_TO_GEN_DTYPE = {
    np.float16: DataType.from_string("f16"),
    np.float32: DataType.from_string("f32"),
    np.float64: DataType.from_string("f64"),
    np.int32: DataType.from_string("i32"),
}


GEN_TO_NP_DTYPE = {v: k for k, v in NP_TO_GEN_DTYPE.items()}


class Array:
    def __init__(self, name, indices):
        assert isinstance(name, str)
        assert isinstance(indices, Sequence)
        self._name = name
        self._indices = copy.deepcopy(indices)  # now this class is owning its copy of indices
        assert None not in self.indices
        self.options = {}

    @property
    def name(self):
        return self._name
    
    @name.setter
    def name(self, n):
        self._name = n
    
    @property
    def indices(self):
        return self._indices
    
    @indices.setter
    def indices(self, i):
        self._indices = copy.deepcopy(i)
    
    def swap_index(self, access_depth):
        res = Array(self._name, [idx.swap_index(access_depth) for idx in self.indices])
        res.options = copy.deepcopy(self.options)
        return res
    
    def swap_buf_dims(self, arr_names, dim_idx):
        res = Array(self._name, [idx.swap_buf_dims(arr_names, dim_idx) for idx in self.indices])
        if self._name in arr_names:
            res.indices[dim_idx], res.indices[dim_idx+1] = res.indices[dim_idx+1], res.indices[dim_idx]
        return res
    
    def tile_index(self, access_depth, tile_size):
        res = Array(self._name, [idx.tile_index(access_depth, tile_size) for idx in self.indices])
        res.options = copy.deepcopy(self.options)
        return res
    
    def untile_index(self, access_depth):
        res = Array(self._name, [idx.untile_index(access_depth) for idx in self.indices])
        res.options = copy.deepcopy(self.options)
        return res
    
    def find_tile_expr(self, access_depth, tile_size):
        if tile_size == 0:
            return 0
        for idx in self.indices:
            tile_size = idx.find_tile_expr(access_depth, tile_size)
        return tile_size
    
    def to_index_list(self):
        return []
    
    def replace_index(self, new_index, access_depth):
        res = Array(self._name, [idx.replace_index(new_index, access_depth) for idx in self.indices])
        res.options = copy.deepcopy(self.options)
        return res
    
    def shift_index(self, value):
        res = Array(self._name, [idx.shift_index(value) for idx in self.indices])
        res.options = copy.deepcopy(self.options)
        return res

    def remove_index(self, access_depth, with_shift):
        new_indices = [idx.remove_index(access_depth, with_shift) for idx in self.indices]
        res = Array(self._name, [idx for idx in new_indices if idx is not None])
        res.options = copy.deepcopy(self.options)
        return res

    def __repr__(self):
        indices_str = ', '.join([str(idx) for idx in self.indices])
        if self.options:
            opts_str = ' | ' + ', '.join(f"{k}={v}" for k, v in self.options.items())
        else:
            opts_str = ''
        if not indices_str and not opts_str:
            return self.name
        return f"{self.name}[{indices_str}{opts_str}]"

    def text(self, root_scope, print_annotations: bool = False):
        indices_text = [idx.text(root_scope) for idx in self.indices]
        buffer_name = root_scope.arr_to_buf[self.name]
        buffer = root_scope.buffers[buffer_name]
        # indices_with_use = []
        # for idx, used in zip(indices_text, buffer.used_dims):
        #     indices_with_use.append(idx + ('' if used else ':N'))
    
        # Annotations for transformations such as
        # 'untile_buffer', 'tile_buffer', 'reuse_arr_dims', etc.
        # Assuming that the dim_idx is sorted in ascending order
        # Annotation for each index
        # if print_annotations and \
        #     len(buffer.transform_annotations) > 0 and \
        #     len(buffer.transform_annotations[0]) == 2 and \
        #     type(buffer.transform_annotations[0][0]) == int:
        #     for dim_idx, annotation in buffer.transform_annotations:
        #         assert dim_idx < len(indices_with_use)
        #         indices_with_use[dim_idx] = f"{indices_with_use[dim_idx]} {annotation}"
        
        # indices_str = ','.join(indices_with_use)
        indices_str = ','.join(indices_text)
        if 'ssr' in self.options:
            ssr_id = self.options['ssr']
            ssr_depth = root_scope.options['ssr'][ssr_id]['depth']
            opts_str = f'|s:{ssr_id},d:{ssr_depth}'
            if 'hw_idx' in root_scope.options['ssr'][ssr_id]:
                ssr_hw_idx = root_scope.options['ssr'][ssr_id]['hw_idx']
                opts_str += f',h:{ssr_hw_idx}'
        else:
            opts_str = ''
        # related_arrays = root_scope.buf_to_arr(buffer_name)
        access_str = ''
        if indices_str:
            access_str = f'[{indices_str}{opts_str}]'
        name_postfix = ''
        # if len(related_arrays) > 1 and buffer_name != self.name:
        #     name_postfix += f':{buffer_name}'
        # if buffer.allocation == "stack":
        #     name_postfix += "|s"
        
        front_annotations = ""
        back_annotations = ""
        if print_annotations and \
            len(self.transform_annotations) > 0 and \
            len(self.transform_annotations[0]) == 2 and \
            type(self.transform_annotations[0][0]) == bool:
            front_annotations = " ".join([x for f, x in self.transform_annotations if f])
            back_annotations = " ".join([x for f, x in self.transform_annotations if not f])
        
        return f"{front_annotations} {self.name}{name_postfix}{access_str} {back_annotations}".strip()

    def gen_address(self, ctx, addr_depth):
        buf_name = ctx.root.arr_to_buf[self.name]
        buf = ctx.root.buffers[buf_name]
        dims_access = [
            buf.dims[pos].gen_access(ctx)
            for pos, used in enumerate(buf.used_dims) if used
        ]
        indices = [
            self.indices[pos].pos_access(ctx, addr_depth=addr_depth)
            for pos, used in enumerate(buf.used_dims) if used
        ]
        if self.get_dtype(ctx.root).is_vector():
            # last dimension represents vector dimension and should not be counted
            dims_access = dims_access[:-1]
            indices = indices[:-1]
        if not indices:
            return f"(*{buf_name})"
        strided_access = [
            " * ".join([indices[i], *dims_access[i + 1 :]])
            for i in range(len(dims_access))
        ]
        access_str = " + ".join(strided_access)
        return buf_name + "[" + access_str + "]"

    def gen_access(self, ctx):
        buf_name = ctx.root.arr_to_buf[self.name]
        buf = ctx.root.buffers[buf_name]
        dims_access = [
            buf.dims[pos].gen_access(ctx)
            for pos, used in enumerate(buf.used_dims) if used
        ]
        indices = [
            self.indices[pos].pos_access(ctx)
            for pos, used in enumerate(buf.used_dims) if used
        ]
        if self.get_dtype(ctx.root).is_vector():
            # last dimension represents vector dimension and should not be counted
            dims_access = dims_access[:-1]
            indices = indices[:-1]
        if (
            "ssr" in self.options
            and "hw_idx" in ctx.root.options["ssr"][self.options["ssr"]]
        ):
            hw_idx = ctx.root.options["ssr"][self.options["ssr"]]["hw_idx"]
            return f"__ft{hw_idx}"
        if not indices:
            # scalar access
            return f"(*{buf_name})"
        else:
            strided_access = [
                " * ".join([indices[i], *dims_access[i + 1 :]])
                for i in range(len(dims_access))
            ]
            access_str = " + ".join(strided_access)
            # return f"/*{self.name}*/" + buf_name + "[" + access_str + "]"
            return buf_name + "[" + access_str + "]"

    def gen_vectorized_load(self, ctx, simd, op_vtype):
        dtype = self.get_dtype(ctx.root)
        array_vtype = get_vectorized_dtype(simd, dtype)
        access = self.gen_address(ctx, addr_depth=ctx.depth-2)
        if 0 in self.accessed_pos():
            if dtype.is_vector():
                # load from vector source
                return self.gen_address(ctx, addr_depth=ctx.depth-2)
            else:
                if array_vtype != op_vtype:
                    raise ValueError(f"Cannot load from scalar storage of type {array_vtype} into vector of {op_vtype}. Input program should contain explicit cast.")
                # load from scalar source
                return f"vector_load_{op_vtype}(&{access})"
        else:
            return f"vector_bcast_{op_vtype}({access})"
    
    def gen_vectorized_store(self, ctx, simd):
        dtype = self.get_dtype(ctx.root)
        vtype = get_vectorized_dtype(simd, dtype)
        access = self.gen_address(ctx, addr_depth=ctx.depth-2)
        if dtype.is_vector():
            # storing to a vectorized destination
            return f"{access} = {{0}}"
        else:
            # storing to a scalar destination
            return f"vector_store_{vtype}(&{access}, {{0}})"
        
    
    def canonicalize_strings(self, arr_rename):
        self.name = arr_rename[self.name]
        for idx_pos, idx in enumerate(self.indices):
            idx.canonicalize_strings(arr_rename)
            
    def is_same(self, program, other):
        # access is the same if both arrays point to the same buffer and indices are the same
        if self.__class__ != other.__class__:
            return False
        buf1 = program.arr_to_buf[self.name]
        buf2 = program.arr_to_buf[other.name]
        if buf1 != buf2:
            return False
        if self.indices != other.indices:
            return False
        return True
    
    def get_dtype(self, program):
        return program.buffers[program.arr_to_buf[self.name]].dtype
    
    def __eq__(self, other):
        return (
            self.__class__ == other.__class__
            and self.name == other.name
            and self.indices == other.indices
        )
        
    def __hash__(self):
        return hash((self.name, tuple(self.indices)))

    def to_dict(self) -> Dict:
        res = {
            "obj": self.__class__.__name__,
            "array": self.name,
            "indices": [index.to_dict() for index in self.indices],
            "transforms": self.transform_annotations,
        }
        assert not (self.options.keys() - {'ssr'})  # make sure I didn't miss any fields
        if self.options.get("ssr"):
            res["ssr"] = self.options["ssr"]
        return res
    
    def accessed_pos(self):
        # pass empty set to avoid error when list is empty
        return set.union(set(), *[idx.accessed_pos() for idx in self.indices])

    def used_arrs(self):
        return set.union({self.name}, *[idx.used_arrs() for idx in self.indices])

    def replace_buf_access(self, root, buf_name, replacement):
        if root.arr_to_buf[self.name] == buf_name:
            return copy.deepcopy(replacement)
        new_indices = []
        for idx in self.indices:
            new_indices.append(idx.replace_buf_access(root, buf_name, replacement))
        return Array(self.name, new_indices)

    def replace_arr_name(self, old_name, new_name):
        res = copy.deepcopy(self)
        res.indices = [idx.replace_arr_name(old_name, new_name) for idx in res.indices]
        if res.name == old_name:
            res.name = new_name
        return res


def myindent(x, s4):
    is_first = [True]

    def pred(line):
        if is_first[0]:
            is_first[0] = False
            return False
        return not line.isspace()

    return indent(x, s4 * 4 * " ", pred)


class ConstElem:
    def __init__(self, value, dtype):
        self.dtype = DataType.canonicalize(dtype)
        if self.dtype.literal == "f":
            self.value = float(value)
        elif self.dtype.literal == "i":
            self.value = int(value)
        else:
            raise ValueError(f"Unknown dtype: {dtype}")

    @property
    def dims(self):
        return []

    def gen_access(self, ctx):
        return str(self.value)

    def pos_access(self, ctx, addr_depth=None):
        return str(self.value)

    def gen_vectorized_load(self, ctx, simd, op_vtype):
        return f"vector_bcast_{op_vtype}({self.value})"

    def __repr__(self):
        return f"<{self.value}>"

    def text(self, root_scope, print_annotations: bool = False):
        return f"{self.value}"

    def to_dict(self) -> Dict:
        return {
            "obj": self.__class__.__name__,
            "dtype": self.dtype,
            "value": self.value,
        }
    
    def is_same(self, program, other):
        return (
            self.__class__ == other.__class__
            and self.value == other.value
            and self.dtype == other.dtype
        )
    
    def __eq__(self, other):
        return (
            self.__class__ == other.__class__
            and self.value == other.value
            and self.dtype == other.dtype
        )
    
    def __hash__(self):
        return hash((self.value, self.dtype))
    
    def get_dtype(self, program):
        return self.dtype
    
    def accessed_pos(self):
        return set()
    
    def used_arrs(self):
        return set()
    
    def swap_index(self, access_depth):
        return ConstElem(self.value, self.dtype)
    
    def swap_buf_dims(self, arr_names, dim_idx):
        return copy.deepcopy(self)
    
    def tile_index(self, access_depth, tile_size):
        return copy.deepcopy(self)

    def untile_index(self, access_depth):
        return copy.deepcopy(self)
    
    def find_tile_expr(self, access_depth, tile_size):
        return tile_size

    def to_index_list(self):
        return []
    
    def replace_index(self, new_index, access_depth):
        return copy.deepcopy(self)
    
    def shift_index(self, value):
        return copy.deepcopy(self)
    
    def only_pos(self, pos):
        """This IndexExpr contains either constants or pos='pos' but not other values"""
        return True
    
    def remove_index(self, access_depth, with_shift):
        return copy.deepcopy(self)

    def replace_buf_access(self, root, buf_name, replacement):
        return copy.deepcopy(self)
    
    def replace_arr_name(self, old_name, new_name):
        return copy.deepcopy(self)


class Index:
    def __init__(self, pos):
        self._pos = pos

    @property
    def pos(self):
        return self._pos

    def accessed_pos(self):
        return {self.pos}
    
    def gen_vectorized_load(self, ctx, simd, op_vtype):
        return f"vector_bcast_{op_vtype}({self.gen_access(ctx)})"

    def __repr__(self):
        return "{" + str(self.pos) + "}"
    
    def text(self, root_scope, print_annotations: bool = False):
        return "{" + str(self.pos) + "}"

    def only_pos(self, pos):
        """This IndexExpr contains either constants or pos='pos' but not other values"""
        return self.pos == pos

    def get_dtype(self, program):
        return 'i32'

    def gen_access(self, ctx):
        return self.pos_access(ctx)

    def pos_access(self, ctx, addr_depth=None):
        """
        Loop variables are reffered by their distance relative to the location of operation.

        Example:
            root_scope <-- depth 0 (=addr_depth, a[0, 0], address)
                for (i0 = ...) <-- depth 1 (=addr_depth, a[0, i0], address)
                    for (i1 = ...) <-- depth 2 (=addr_depth, a[i1, i0], address)
                        operation_scope: a[{0}, {1}] <-- depth 3 (=ctx.depth, a[i1, i0], value)
            Here {0} refers to i1 and {1} refers to i0!

        Scope depth is required to instantiate correct accesses to loop variables.
        Here, to generate the access to a[i1, i0], supplied scope depth (in ctx.depth) should be equal to 3.
        For any operation, it is equal to the number of loops from root +1.

        During the code generation, the loop variables are substituted with the following expressions:
            {0} should be substituted with i[scope_depth - 2 - {0}] = i1
            {1} should be substituted with i[scope_depth - 2 - {1}] = i0

        Sometimes, we may need to generate address of the first element that will be processed per the loop level.
        For example, before the loop i0 starts (addr_depth=0), the first accessed elelement is a[0, 0].
        Inside the loop over i0, before the i1 starts (addr_depth=1), the first accessed element is a[0, i0].
        Inside the loop over i1 (addr_depth=2), address and the accessed value are fully instantiated and match each other (a[i1, i0]).
        """

        # offset by 2 to account the depth introduced by the "root scope" and "operation"
        absolute_idx = ctx.depth - 2 - self.pos
        if addr_depth is not None:
            if absolute_idx >= addr_depth:
                return "0"
        if absolute_idx < 0:
            raise ValueError("Index can't be negative")
        return f"i{absolute_idx}"

    def canonicalize_strings(self, arr_rename):
        pass  # nothing to do
    
    def __eq__(self, other):
        return self.__class__ == other.__class__ and self.pos == other.pos
    
    def __hash__(self):
        return hash(self.pos)

    def to_dict(self) -> Dict:
        return {
            "obj": self.__class__.__name__,
            "pos": self.pos,
        }
        
    def used_arrs(self):
        return set()
    
    def swap_index(self, access_depth):
        if self.pos == access_depth:
            return Index(access_depth + 1)
        elif self.pos == access_depth + 1:
            return Index(access_depth)
        else:
            return Index(self._pos)

    def swap_buf_dims(self, arr_names, dim_idx):
        return copy.deepcopy(self)

    def tile_index(self, access_depth, tile_size):
        if self.pos == access_depth:
            return IndexExpr(IndexExpr(Index(access_depth + 1), '*', tile_size), '+', Index(access_depth))
        elif self.pos > access_depth:
            return Index(self.pos + 1)
        else:
            return Index(self._pos)
        
    def untile_index(self, access_depth):
        assert self.pos not in (access_depth, access_depth - 1)
        if self.pos > access_depth:
            return Index(self.pos - 1)
        else:
            return Index(self.pos)

    def find_tile_expr(self, access_depth, tile_size):
        # access_depth refers to the depth of the outer loop
        if tile_size == 0:
            return 0
        # access_depth refers to the outer scope, access_depth-1 to the inner scope
        if access_depth in {self.pos + 1, self.pos}:
            return 0  # standalone use of the index, meaning it is not always participating in tile expression
        return tile_size

    def to_index_list(self):
        return [self]
    
    def replace_index(self, new_index, access_depth):
        if self.pos == access_depth:
            return copy.deepcopy(new_index)
        else:
            return Index(self.pos)
        
    def shift_index(self, value):
        assert self.pos + value >= 0
        return Index(self.pos + value)
        
    def remove_index(self, access_depth, with_shift):
        if self.pos < access_depth:
            return Index(self.pos)
        elif self.pos > access_depth:
            return Index(self.pos-1 if with_shift else self.pos)
        else:
            return None

    def replace_buf_access(self, root, buf_name, replacement):
        return copy.deepcopy(self)
    
    def replace_arr_name(self, old_name, new_name):
        return copy.deepcopy(self)


class IndexExpr:
    def __init__(self, lhs, op, rhs):
        self.lhs = lhs
        self.op = op
        self.rhs = rhs

    def __repr__(self):
        return f"({self.lhs}) {self.op} ({self.rhs})"
    
    def __eq__(self, other):
        # required for some transformations to work correctly
        # for example, reuse_buffers
        return self.__class__ == other.__class__ and self.lhs == other.lhs and self.op == other.op and self.rhs == other.rhs

    def __hash__(self):
        # required because we defined __eq__
        return hash((self.lhs, self.op, self.rhs))

    def gen_vectorized_load(self, ctx, simd, op_vtype):
        return f"vector_bcast_{op_vtype}({gen_access(ctx, self)})"

    def text(self, root_scope, print_annotations: bool = False):
        return f"({self.lhs.text(root_scope)}){self.op}({self.rhs.text(root_scope)})"

    def only_pos(self, pos):
        """This IndexExpr contains either constants or pos='pos' but not other values"""
        return self.lhs.only_pos(pos) and self.rhs.only_pos(pos)

    def pos_access(self, ctx, addr_depth=None):
        lhs_access = (
            self.lhs.pos_access(ctx, addr_depth=addr_depth)
            if hasattr(self.lhs, "pos_access")
            else self.lhs
        )
        rhs_access = (
            self.rhs.pos_access(ctx, addr_depth=addr_depth)
            if hasattr(self.rhs, "pos_access")
            else self.rhs
        )
        return f"({lhs_access} {self.op} {rhs_access})"
    
    def gen_access(self, ctx):
        return self.pos_access(ctx)

    def get_dtype(self, program):
        return 'i32'

    def to_dict(self) -> Dict:
        return {
            "obj": self.__class__.__name__,
            "lhs": self.lhs.to_dict(),
            "op": self.op,
            "rhs": self.rhs.to_dict()
        }
    
    def is_same(self, program, other):
        if not isinstance(other, IndexExpr):
            return False
        return self.lhs.is_same(program, other.lhs) and self.op == other.op and self.rhs.is_same(program, other.rhs)

    
    def accessed_pos(self):
        return self.lhs.accessed_pos() | self.rhs.accessed_pos()
    
    def used_arrs(self):
        return self.lhs.used_arrs() | self.rhs.used_arrs()
    
    def swap_index(self, access_depth):
        return IndexExpr(self.lhs.swap_index(access_depth), self.op, self.rhs.swap_index(access_depth))
    
    def swap_buf_dims(self, arr_names, dim_idx):
        return IndexExpr(self.lhs.swap_buf_dims(arr_names, dim_idx), self.op, self.rhs.swap_buf_dims(arr_names, dim_idx))
    
    def tile_index(self, access_depth, tile_size):
        return IndexExpr(self.lhs.tile_index(access_depth, tile_size), self.op, self.rhs.tile_index(access_depth, tile_size))

    def untile_index(self, access_depth): 
        if is_untiling_exp(self, access_depth):
            return Index(access_depth - 1)
        else:
            return IndexExpr(self.lhs.untile_index(access_depth), self.op, self.rhs.untile_index(access_depth))

    def find_tile_expr(self, access_depth, tile_size):
        if tile_size == 0:
            return 0
        expr_tile_size = is_untiling_exp(self, access_depth, tile_size)
        if expr_tile_size != 0:
            return expr_tile_size  # exact match of tiling expression
        tile_size = self.lhs.find_tile_expr(access_depth, tile_size)
        tile_size = self.rhs.find_tile_expr(access_depth, tile_size)
        return tile_size

    def to_index_list(self):
        return self.lhs.to_index_list() + self.rhs.to_index_list()
    
    def replace_index(self, new_index, access_depth):
        return IndexExpr(self.lhs.replace_index(new_index, access_depth), self.op, self.rhs.replace_index(new_index, access_depth))
    
    def shift_index(self, value):
        return IndexExpr(self.lhs.shift_index(value), self.op, self.rhs.shift_index(value))

    def remove_index(self, access_depth, with_shift):
        l = self.lhs.remove_index(access_depth, with_shift)
        r = self.rhs.remove_index(access_depth, with_shift)
        if l is None or r is None:
            raise ValueError("Removal of index from expression is undefined")
        return IndexExpr(l, self.op, r)

    def replace_buf_access(self, root, buf_name, replacement):
        return IndexExpr(
            self.lhs.replace_buf_access(root, buf_name, replacement),
            self.op,
            self.rhs.replace_buf_access(root, buf_name, replacement),
        )
    
    def replace_arr_name(self, old_name, new_name):
        return IndexExpr(
            self.lhs.replace_arr_name(old_name, new_name),
            self.op,
            self.rhs.replace_arr_name(old_name, new_name),
        )


class OpDesc:
    def __init__(self, out_dtype, func="{0}", *, reduce="", asm="", asm_dtypes=[]):
        self.out_dtype = out_dtype
        self.func = func
        self.reduce = reduce
        if bool(asm) != bool(asm_dtypes):
            raise ValueError("asm_dtypes must be provided if asm is provided")
        self.asm = asm
        self.asm_dtypes = asm_dtypes
        
    def __repr__(self):
        return f"OpDesc(out_dtype={self.out_dtype}, func='{self.func}', reduce='{self.reduce}', asm='{self.asm}', asm_dtypes={self.asm_dtypes})"

    def __str__(self):
        num_inputs = 0
        while f"{{{num_inputs}}}" in self.func:
            num_inputs += 1
        func_string = self.func.format(*[f'{{{i}}}' for i in range(num_inputs)])
        reduce_template = self.reduce or "{y} = {x}"
        template = reduce_template.format(x=func_string, y=f'{{{num_inputs}}}')
        return template

    def is_empty_func(self):
        return self.func == "{0}"

    def is_move(self):
        return self.is_empty_func() and not self.reduce

    def reduce_identity(self):
        if not self.reduce:
            raise ValueError("Operation is not a reduction")
        
        if "max" in self.reduce:
            return np.finfo(GEN_TO_NP_DTYPE[self.out_dtype]).min
        elif "min" in self.reduce:
            return np.finfo(GEN_TO_NP_DTYPE[self.out_dtype]).max
        elif "+=" in self.reduce:
            return 0
        elif "*=" in self.reduce:
            return 1
        else:
            raise ValueError(f"Unknown reduce operation: {self.reduce}")


OP_DESC_TEMPLATES = {
    "abs": OpDesc("f64", "fabs({0})", asm="fabs.d {0}, {1}", asm_dtypes=["f64", "f64"]),
    "acos": OpDesc("f64", "acos({0})"),
    "acosh": OpDesc("f64", "acosh({0})"),
    "add": OpDesc("f64", "{0} + {1}", asm="fadd.d {0}, {1}, {2}", asm_dtypes=["f64", "f64", "f64"]),
    "and": OpDesc("i32", "{0} && {1}"),
    "asin": OpDesc("f64", "asin({0})"),
    "asinh": OpDesc("f64", "asinh({0})"),
    "atan": OpDesc("f64", "atan({0})"),
    "atanh": OpDesc("f64", "atanh({0})"),
    "bitwise_and": OpDesc("i32", "{0} & {1}"),
    "bitwise_not": OpDesc("i32", "~{0}"),
    "bitwise_or": OpDesc("i32", "{0} | {1}"),
    "bitwise_xor": OpDesc("i32", "{0} ^ {1}"),
    "div": OpDesc("f64", "{0} / {1}", asm="fdiv.d {0}, {1}, {2}", asm_dtypes=["f64", "f64", "f64"]),
    "f32_copyfrom_i32": OpDesc("f32", "reinterpret_f32_from_i32({0})", asm="fmv.w.x {0}, {1}", asm_dtypes=["f32", "i32"]),
    "f32_from_f64": OpDesc("f32", "(f32) {0}", asm="fcvt.s.d {0}, {1}", asm_dtypes=["f32", "f64"]),
    "f64_from_f32": OpDesc("f64", "(f64) {0}", asm="fcvt.d.s {0}, {1}", asm_dtypes=["f64", "f32"]),
    "f64_from_i32": OpDesc("f64", "(f64) {0}", asm="fcvt.d.w {0}, {1}", asm_dtypes=["f64", "i32"]),
    "fmadd": OpDesc("f64", '+ {0} * {1} + {2}', asm="fmadd.d {0}, {1}, {2}, {3}", asm_dtypes=["f64", "f64", "f64", "f64"]),
    "fmsub": OpDesc("f64", '+ {0} * {1} - {2}', asm="fmsub.d {0}, {1}, {2}, {3}", asm_dtypes=["f64", "f64", "f64", "f64"]),
    "fnmadd": OpDesc("f64", '- {0} * {1} - {2}', asm="fnmadd.d {0}, {1}, {2}, {3}", asm_dtypes=["f64", "f64", "f64", "f64"]),
    "fnmsub": OpDesc("f64", '- {0} * {1} + {2}', asm="fnmsub.d {0}, {1}, {2}, {3}", asm_dtypes=["f64", "f64", "f64", "f64"]),
    "i32_copyfrom_f32": OpDesc("i32", "reinterpret_i32_from_f32({0})", asm="fmv.x.w {0}, {1}", asm_dtypes=["i32", "f32"]),
    "i32_from_f64": OpDesc("i32", "(i32) {0}", asm="fcvt.w.d {0}, {1}", asm_dtypes=["i32", "f64"]),
    "iadd": OpDesc("i32", "{0} + {1}", asm="add {0}, {1}, {2}", asm_dtypes=["i32", "i32", "i32"]),
    "idiv": OpDesc("i32", "{0} / {1}"),
    "imul": OpDesc("i32", "{0} * {1}"),
    "isub": OpDesc("i32", "{0} - {1}", asm="sub {0}, {1}, {2}", asm_dtypes=["i32", "i32", "i32"]),
    "max": OpDesc("f64", "fmax({0}, {1})", asm="fmax.d {0}, {1}, {2}", asm_dtypes=["f64", "f64", "f64"]),
    "min": OpDesc("f64", "fmin({0}, {1})", asm="fmin.d {0}, {1}, {2}", asm_dtypes=["f64", "f64", "f64"]),
    "move": OpDesc("f64", asm="fmv.d {0}, {1}", asm_dtypes=["f64", "f64"]),
    "mul": OpDesc("f64", "{0} * {1}", asm="fmul.d {0}, {1}, {2}", asm_dtypes=["f64", "f64", "f64"]),
    "not": OpDesc("i32", "! {0}"),
    "or": OpDesc("i32", "{0} || {1}"),
    "reduce_add": OpDesc("f64", reduce="{y} += {x}", asm="fadd.d {0}, {0}, {1}", asm_dtypes=["f64", "f64"]),
    "reduce_fmadd": OpDesc("f64", "{0} * {1}", reduce="{y} += {x}", asm="fmadd.d {0}, {1}, {2}, {0}", asm_dtypes=["f64", "f64", "f64"]),
    "reduce_max": OpDesc("f64", reduce="{y} = fmax({y}, {x})", asm="fmax.d {0}, {0}, {1}", asm_dtypes=["f64", "f64"]),
    "reduce_min": OpDesc("f64", reduce="{y} = fmin({y}, {x})", asm="fmin.d {0}, {0}, {1}", asm_dtypes=["f64", "f64"]),
    "sgnj": OpDesc("f64", "({1} >= 0 ? fabs({0}) : -fabs({0}))", asm="fsgnj.d {0}, {1}, {2}", asm_dtypes=["f64", "f64", "f64"]),
    "sgnjn": OpDesc("f64", "({1} >= 0 ? -fabs({0}) : fabs({0}))", asm="fsgnjn.d {0}, {1}, {2}", asm_dtypes=["f64", "f64", "f64"]),
    "sgnjx": OpDesc("f64", "({1} >= 0 ? {0} : -{0})", asm="fsgnjx.d {0}, {1}, {2}", asm_dtypes=["f64", "f64", "f64"]),
    "slli": OpDesc("i32", "{0} << {1}"),
    "sqrt": OpDesc("f64", "sqrt({0})", asm="fsqrt.d {0}, {1}", asm_dtypes=["f64", "f64"]),
    "srli": OpDesc("i32", "{0} >> {1}"),
    "sub": OpDesc("f64", "{0} - {1}", asm="fsub.d {0}, {1}, {2}", asm_dtypes=["f64", "f64", "f64"]),
}


def canonicalize_op(op):

    if isinstance(op, OpDesc):
        return op
    return copy.deepcopy(OP_DESC_TEMPLATES[op])


def num_inputs_in_op_str(op_str):
    num_inputs = 0
    while f"{{{num_inputs}}}" in op_str:
        num_inputs += 1
    return num_inputs


def get_vectorized_operator(scalar_func: str, vtype: DataType):
    if scalar_func == "{0}":
        return "{0}"
    if m := re.match(r"^({\d+}) \+ ({\d+})$", scalar_func):
        op_str = "add"
        op_args = ", ".join(m.groups())
    elif m := re.match(r"^({\d+}) - ({\d+})$", scalar_func):
        op_str = "sub"
        op_args = ", ".join(m.groups())
    elif m := re.match(r"^({\d+}) \* ({\d+})$", scalar_func):
        op_str = "mul"
        op_args = ", ".join(m.groups())
    elif m := re.match(r"^({\d+}) / ({\d+})$", scalar_func):
        op_str = "div"
        op_args = ", ".join(m.groups())
    elif m := re.match(r"^({\d+}) \* ({\d+}) \+ ({\d+})$", scalar_func):
        op_str = "fmadd"
        op_args = ", ".join(m.groups())
    elif m := re.match(r"^({\d+}) \* ({\d+}) \- ({\d+})$", scalar_func):
        op_str = "fmsub"
        op_args = ", ".join(m.groups())
    elif m := re.match(r"^(\w+)\((.*)\)$", scalar_func):
        op_str = m.group(1)
        op_args = m.group(2)
    else:
        raise ValueError(f"Can't determine operation string for '{scalar_func}'")
    vectorized_operator = f"vector_{op_str}_{vtype}({op_args})"    
    return vectorized_operator

def get_vec_to_scalar_reduction(scalar_reduction: str, vtype: DataType):
    return {
        '{y} += {x}': f"reduce_add_{vtype}({{0}})",
        '{y} = fmaxf({y}, {x})': f"reduce_max_{vtype}({{0}})",
        '{y} = fminf({y}, {x})': f"reduce_min_{vtype}({{0}})",
        '{y} = {x}': "{0}"
    }[scalar_reduction]

def get_vec_to_vec_reduction(scalar_reduction: str, vtype: DataType):
    return {
        '{y} += {x}': f"vector_add_{vtype}({{y}}, {{x}})",
        '{y} = fmaxf({y}, {x})': f"vector_max_{vtype}({{y}}, {{x}})",
        '{y} = fminf({y}, {x})': f"vector_min_{vtype}({{y}}, {{x}})",
        '{y} = {x}': "{y} = {x}"
    }[scalar_reduction]

class Operation:
    def __init__(self, out_elem, inp_elem, op):
        self.out_elem = canonicalize_access(out_elem)
        self.inp_elem = [canonicalize_access(a) for a in inp_elem]
        self.op = canonicalize_op(op)

    @property
    def name(self):
        # indices can include constants to index across certain dimensions
        # they should be added to the name to make it unique
        const_indices = [str(idx.value) for idx in self.out_elem.indices if isinstance(idx, ConstElem)]
        unique_id = [self.out_elem.name, *const_indices]
        return ".".join(str(x) for x in unique_id)

    def __str__(self):
        return str(self.op).format(*self.all_elem)

    def __repr__(self):
        return f"Operation(out_elem={repr(self.out_elem)}, inp_elem={repr(self.inp_elem)}, op={repr(self.op)})"

    def text(self, root_scope, print_annotations: bool = False):
        inp_elem_text = [e.text(root_scope, print_annotations) for e in self.inp_elem]
        out_elem_text = self.out_elem.text(root_scope, print_annotations)

        # Transformations annotations
        if print_annotations:
            front_annotations = " ".join([a for f, a in self.transform_annotations if f])
            back_annotations = " ".join([a for f, a in self.transform_annotations if not f])
        else:
            front_annotations = ""
            back_annotations = ""

        func = self.op.func.format(*inp_elem_text)
        assign_template = self.op.reduce or '{y} = {x}'
        assign = assign_template.format(y=out_elem_text, x=func)

        return [[f"{front_annotations} {assign} {back_annotations}".strip()]]

    def get_inp_out(self):
        outputs = {self.out_elem.name}
        inputs = set.union(set(), *[e.used_arrs() for e in self.all_elem])
        inputs -= outputs
        return inputs, outputs

    def get_inp_tmp_out(self):
        inputs, outputs = self.get_inp_out()
        return inputs, set(), outputs

    def compute_inp_tmp_out(self, used_outputs):
        inp, out = self.get_inp_out()
        unused = out - used_outputs
        if unused:
            raise Exception(f"Output of {self} is not used")
        return self.get_inp_tmp_out()

    def propagate_used_outputs(self, used_outputs):
        inp, out = self.get_inp_out()
        assert out <= used_outputs

    def infer_decls(self, root_scope, declarations):
        # if self.op.reduce:
        #     [inp_elem] = self.inp_elem
        #     inp_arr = declarations[inp_elem.name]
        #     index_mapping = {}
        #     for out_loc, out_index in enumerate(self.out_elem.indices):
        #         for inp_loc, inp_index in enumerate(inp_elem.indices):
        #             if same_index(inp_index, out_index):
        #                 index_mapping[out_loc] = inp_loc
        #     index_mapping = dict(sorted(index_mapping.items()))
        #     out_dims = [inp_arr.dims[i] for i in index_mapping.values()]
        # else:
        if self.out_elem.name not in declarations:
            out_dtype = self.op.out_dtype
            out_dims = infer_output_dims(root_scope, self)
            return {self.out_elem.name: Buffer(out_dims, out_dtype)}
        else:
            return {}
        

    def gen_asm_body(self, ctx, op_idx):
        """return (op_str, access_dict {name: (accessor, access_type, op_dtype, arg_dtype)})
        there are different access types: '+' in/out, '=' out, '' in

        Operators may expect different data types which derived from data types of inputs.
        This may create the need to convert data types of inputs to the expected type.
        """
        
        pi = op_idx  # index of operator in parent scope (after unrolling)

        a = [ae.gen_access(ctx) for ae in self.all_elem]
        dt = [ae.get_dtype(ctx.root) for ae in self.all_elem]
                
        if not self.op.asm:
            if self.op.is_move() and ctx.target == "snitch":
                # special case when we know how assembly should look
                self.op.asm = "fmv.d {0}, {1}"
                self.op.asm_dtypes = ["f64", "f64"]
            else:
                raise ValueError(f"Operation {self.op} is not supplied with assembly code")

        op_str = '"' + self.op.asm.format(*[f"%[{symbol}{pi}]" for access, symbol in zip(a, 'cabd')]) + ';"'
        def find_access_type(symbol):
            if symbol == 'c':
                return '+' if self.op.reduce else "="
            return ""
        access_dict = {
            f"{symbol}{pi}": (access, find_access_type(symbol), asm_dtype, dtype)
            for access, symbol, asm_dtype, dtype in zip(a, 'cabd', self.op.asm_dtypes, dt)
        }

        for name, (accessor, access_type, op_dtype, arg_dtype) in access_dict.items():
            if accessor.startswith("__ft"):
                hw_idx = accessor[2:]
                op_str = op_str.replace(f"%[{name}]", hw_idx)
        access_dict = {
            name: (accessor, access_type, op_dtype, arg_dtype)
            for name, (accessor, access_type, op_dtype, arg_dtype) in access_dict.items()
            if not accessor.startswith("__ft")
        }

        return (op_str, access_dict)

    def gen_vectorized_body(self, ctx, simd):
        ctx1 = ctx.at_depth(ctx.depth + 1)
        dtype = self.out_elem.get_dtype(ctx1.root)
        vtype = get_vectorized_dtype(simd, dtype)
        reduce_over_vector_dim = 0 not in self.out_elem.accessed_pos()
        reduce_over_other_dims = self.out_elem.accessed_pos() - {0} < set(range(1, ctx.depth))
        input_loads = [elem.gen_vectorized_load(ctx1, simd, vtype) for elem in self.inp_elem]
        output_store = self.out_elem.gen_vectorized_store(ctx1, simd)  # "y = {0}" or "store(&y, {0})"
        output_load = self.out_elem.gen_vectorized_load(ctx1, simd, vtype)  # "x" or "load(&x)"
        output_scalar = self.out_elem.gen_address(ctx1, ctx1.depth-2)  # "y"
        scalar_operator = self.op.func
        assignment = "{y} = {x}"
        scalar_reduction = self.op.reduce or assignment
        vectorized_operator = get_vectorized_operator(scalar_operator, vtype)  # "vector_add_f32x4({0}, {1})"
        vec_to_scalar_reduction = get_vec_to_scalar_reduction(scalar_reduction, vtype)  # "reduce_add_f32x4({0})"
        vec_to_vec_reduction = get_vec_to_vec_reduction(scalar_reduction, vtype)  # "vector_add_f32x4({y}, {x})"
        if reduce_over_vector_dim:
            if reduce_over_other_dims:
                # case A: reduction over vectorized and non-vectorized dimensions
                # scalar: y[] = scalar_reduce(y[], operator(x[n,v]))
                # vector: y[] = scalar_reduce(y[], vec_to_scalar_reduction(vectorized_operator(x[n,v]))))
                vectorized_operator_str = vectorized_operator.format(*input_loads)
                vec_to_scalar_reduction_str = vec_to_scalar_reduction.format(vectorized_operator_str)
                op_compute = scalar_reduction.format(y=output_scalar, x=vec_to_scalar_reduction_str)
            else:
                # case B: reduction only over vectorized dimension
                # scalar: y[n] = scalar_reduce(y[n], operator(x[n,v]))
                # vector: y[n] = vec_to_scalar_reduction(vectorized_operator(x[n,v]))
                vectorized_operator_str = vectorized_operator.format(*input_loads)
                vec_to_scalar_reduction_str = vec_to_scalar_reduction.format(vectorized_operator_str)
                op_compute = assignment.format(y=output_scalar, x=vec_to_scalar_reduction_str)
        else:
            if reduce_over_other_dims:
                # case C: reduction over non-vectorized dimension
                # scalar: y[v] = scalar_reduce(operator(x[n,v]))
                # vector: y[v] = vec_to_vec_reduction(y[v], vectorized_operator(...))
                if vtype.literal == 'f' and scalar_reduction == '{y} += {x}' and scalar_operator == '{0} * {1}':
                    # special case for fused multiply-add
                    # y[v] = vector_fmadd_vtype(a[n, v], b[n, v], y[v])
                    op_compute = output_store.format(f'vector_fmadd_{vtype}({input_loads[0]}, {input_loads[1]}, {output_load})')
                else:
                    # general case
                    vectorized_operator_str = vectorized_operator.format(*input_loads)
                    vec_to_vec_reduction_str = vec_to_vec_reduction.format(y=output_load, x=vectorized_operator_str)
                    op_compute = output_store.format(vec_to_vec_reduction_str)
            else:
                # case D: no reduction
                # scalar: y[n, v] = operator(x[n,v])  
                # vector: y[n, v] = vectorized_operator(x[n,v])
                vectorized_operator_str = vectorized_operator.format(*input_loads)
                op_compute = output_store.format(vectorized_operator_str)
        return ([], [op_compute + ";"])

    def gen_body(self, ctx):
        output_acc = self.out_elem.gen_access(ctx)
        input_acc = [elem.gen_access(ctx) for elem in self.inp_elem]
        op_func = self.op.func.format(*input_acc)
        assign_template = self.op.reduce or "{y} = {x}"
        op_assign = assign_template.format(y=output_acc, x=op_func)
        return ([], [op_assign + ";"])

    @property
    def all_elem(self):
        return [self.out_elem, *self.inp_elem]

    @all_elem.setter
    def all_elem(self, elems):
        [self.out_elem, *self.inp_elem] = copy.deepcopy(elems)

    def canonicalize_strings(self, arr_rename):
        for elem in self.all_elem:
            elem.canonicalize_strings(arr_rename)

    def to_dict(self) -> Dict:
        return {
            "obj": self.__class__.__name__,
            "out": self.out_elem.to_dict(),
            "inp": [elem.to_dict() for elem in self.inp_elem],
            "op": self.op,
            "transforms": self.transform_annotations,
        }
    
    def peak_performance_cycles(self):
        return 1


def get_reduce_indices(operation):
    """
    Analyzes indices present in input and output dimensions of the reduce operation
    Returns: (reduce_indices, non_reduce_indices) as sets of integers representing the depth of the loop index
    """
    assert operation.op.reduce
    inp_elems = operation.inp_elem
    out_elem = operation.out_elem
    out_indices = set()
    out_indices |= out_elem.accessed_pos()
    inp_indices = set()
    for inp_elem in inp_elems:
        inp_indices |= inp_elem.accessed_pos()
    reduce_indices = inp_indices - out_indices
    non_reduce_indices = inp_indices & out_indices
    return reduce_indices, non_reduce_indices


def increment_pos_accesses(index, pos):
    if isinstance(index, IndexExpr):
        return IndexExpr(
            increment_pos_accesses(index.lhs, pos),
            index.op,
            increment_pos_accesses(index.rhs, pos),
        )
    elif isinstance(index, Index):
        if index.pos == pos:
            return IndexExpr(index, "+", 1)
        else:
            return copy.deepcopy(index)
    elif isinstance(index, ConstElem):
        return copy.deepcopy(index)
    elif isinstance(index, Array):
        nested_indices = [increment_pos_accesses(idx, pos) for idx in index.indices]
        return Array(index.name, nested_indices)
    else:
        raise ValueError(f"Unknown index type: {index}")


class Scope:
    def __init__(self, ops):
        if not isinstance(ops, Sequence):
            ops = [ops]
        self.ops = ops[:]

        self.used_outputs = None
        self.options = {}

    def get_inp_out(self):
        inputs = set()
        outputs = set()
        if isinstance(self.dim, Array):
            inputs.add(self.dim.name)
        for op in self.ops:
            op_inps, op_outs = op.get_inp_out()
            inputs |= op_inps
            outputs |= op_outs
        return inputs - outputs, outputs

    def compute_inp_tmp_out(self, used_outputs):
        inp, tmp_out = self.get_inp_out()
        tmp = tmp_out - used_outputs
        out = tmp_out & used_outputs
        return inp, tmp, out

    def propagate_used_outputs(self, used_outputs):
        self.used_outputs = used_outputs
        all_used = set(used_outputs)
        for op in reversed(self.ops):
            inp, tmp, out = op.compute_inp_tmp_out(all_used)
            op.propagate_used_outputs(all_used & out)
            all_used |= inp

    def get_inp_tmp_out(self):
        assert self.used_outputs is not None
        return self.compute_inp_tmp_out(self.used_outputs)

    def infer_decls(self, root_scope, declarations):
        """
        This method returns declarations from all nested scopes, even if they are not visible in the current scope.
        """
        inferred_decls = {}
        for op in self.ops:
            op_decls = op.infer_decls(root_scope, {**declarations, **inferred_decls})
            inferred_decls.update(op_decls)
        return inferred_decls
    
    def peak_performance_cycles(self):
        cycles = 0
        for op in self.ops:
            nested_peak = op.peak_performance_cycles()
            if nested_peak is None:
                return None
            cycles += nested_peak
        return cycles

    def gen_scope_ops(self, ctx, unrolled_ops):        
        # finally generate body of loop

        ssr_conf = find_ssr_config(self)

        body = []
        decls = []

        # group continuous operations for asm generation
        op_groups = {}  # {start_idx: scope or [operators]}
        last_start_idx = None
        for idx, op in enumerate(unrolled_ops):
            if isinstance(op, Scope):
                op_groups[idx] = op
                last_start_idx = None
            elif isinstance(op, Operation):
                if last_start_idx is None:
                    last_start_idx = idx
                op_groups.setdefault(last_start_idx, []).append(op)
            else:
                raise ValueError("Unknown operation type")

        for idx, op_group in op_groups.items():
            # ssr allocation start
            for ssr_idx, val in ssr_conf.items():
                depth = ctx.root.options["ssr"][ssr_idx]["depth"]
                if (
                    # len(val["dims"]) gives the number of dimensions from the most nested to 'self'
                    # if it exactly matches number of dimensions discovered in SSR cofiguration, it is time to configure SSR
                    depth == len(val["dims"])
                    and idx == val["first_op"][depth]
                    and "hw_idx" in ctx.root.options["ssr"][ssr_idx]
                ):
                    ssr_hw_idx = ctx.root.options["ssr"][ssr_idx]["hw_idx"]
                    body.append(f"// ssr {ssr_hw_idx} use start")
                    ssr_cfg_strides = []
                    ssr_cfg_sizes = []
                    ssr_cfg_repeat = None
                    ssr_depth = len(val["dims"])
                    op_depth = 1
                    start_access = val["elem_start"].gen_address(
                        ctx.at_depth(ctx.depth + ssr_depth + op_depth),
                        addr_depth=ctx.depth,
                    )
                    for d in range(
                        2 * len(val["dims"]) + 1
                    ):  # repeats surround each dim
                        dim_idx = d // 2
                        if d % 2 == 0:
                            # processing repeat
                            if val["repeats"][dim_idx] > 1:
                                end_access = val["rep_offsets"][dim_idx].gen_address(
                                    ctx.at_depth(ctx.depth + ssr_depth + op_depth),
                                    addr_depth=ctx.depth,
                                )
                                if start_access != end_access or ssr_cfg_repeat or ssr_cfg_sizes or val["repeats"][dim_idx] == 1:
                                    ssr_cfg_sizes.append(str(val["repeats"][dim_idx]))
                                    ssr_cfg_strides.append(
                                        f"(&{end_access} - &{start_access})"
                                    )
                                else:
                                    # ssr repeat call can be applied only if the 0-stride is in the first dimension and the number of repeats is > 1
                                    ssr_cfg_repeat = val["repeats"][dim_idx]
                        else:
                            # processing dim
                            end_elem = copy.deepcopy(val["elem_start"])
                            for end_elem_i, end_elem_idx in enumerate(end_elem.indices):
                                end_elem.indices[end_elem_i] = increment_pos_accesses(end_elem_idx, dim_idx)
                            end_access = end_elem.gen_address(
                                ctx.at_depth(ctx.depth + ssr_depth + op_depth),
                                addr_depth=ctx.depth,
                            )
                            if start_access != end_access or ssr_cfg_repeat is not None or ssr_cfg_sizes:
                                ssr_cfg_strides.append(f"(&{end_access} - &{start_access})")
                                ssr_cfg_sizes.append(
                                    val["dims"][dim_idx].gen_access(
                                        ctx.at_depth(ctx.depth + ssr_depth),
                                    )
                                )
                            else:
                                #ssr repeat call can be applied only if we haven't applied it to this stream yet and the 0-stride is in the first dimension 
                                ssr_cfg_repeat = val["dims"][dim_idx].gen_access(
                                    ctx.at_depth(ctx.depth + ssr_depth)
                                )


                    num_dims = len(ssr_cfg_sizes)
                    dtype = ctx.root.buffers[
                        ctx.root.arr_to_buf[val["elem_start"].name]
                    ].dtype
                    ssr_cfg_sizes_str = ", ".join(ssr_cfg_sizes)
                    ssr_cfg_strides_str = ", ".join(
                        f"({x}) * sizeof({dtype})" for x in ssr_cfg_strides
                    )
                    ssr_rw = "write" if val["is_write"] else "read"
                    if not val["is_write"]:
                        body.append("snrt_fpu_fence();")
                    if num_dims!=0:
                        body.append(
                            f"snrt_ssr_loop_{num_dims}d(SNRT_SSR_DM{ssr_hw_idx}, {ssr_cfg_sizes_str}, {ssr_cfg_strides_str});"
                        )
                    else:
                        body.append(
                            f"snrt_ssr_loop_1d(SNRT_SSR_DM{ssr_hw_idx}, 1, 0);"
                        )

                    if ssr_cfg_repeat is not None:
                        body.append(
                            f"snrt_ssr_repeat(SNRT_SSR_DM{ssr_hw_idx}, {ssr_cfg_repeat});"
                        )
                    else:
                        body.append(
                            f"snrt_ssr_repeat(SNRT_SSR_DM{ssr_hw_idx}, 1);"
                        )
                    if num_dims!=0:
                        body.append(
                            f"snrt_ssr_{ssr_rw}(SNRT_SSR_DM{ssr_hw_idx}, SNRT_SSR_{num_dims}D, &{start_access});"
                        )
                    else:
                        body.append(
                            f"snrt_ssr_{ssr_rw}(SNRT_SSR_DM{ssr_hw_idx}, SNRT_SSR_1D, &{start_access});"
                        )


                    # body.append(f"snrt_ssr_enable();")
            # ssr allocation end

            if isinstance(op_group, Scope):
                nested_decls, nested_body = op_group.gen_body(ctx.at_depth(ctx.depth+1))
                decls += nested_decls
                body += nested_body
            else:
                for op in op_group:
                    gen_assembly = any(hasattr(elem, 'options') and "ssr" in elem.options and "hw_idx" in ctx.root.options["ssr"][elem.options["ssr"]] for elem in op.all_elem)
                    if gen_assembly:
                        nested_decls = []
                        nested_body = generate_asm_op_range(
                            ctx,
                            self,
                            [op],
                        )
                    else:
                        nested_decls, nested_body = op.gen_body(
                            ctx.at_depth(ctx.depth + 1),
                        )
                    decls += nested_decls
                    body += nested_body

            for ssr_idx, val in ssr_conf.items():
                depth = ctx.root.options["ssr"][ssr_idx]["depth"]
                if (
                    depth == len(val["dims"])
                    and idx == val["last_op"][depth]
                    and "hw_idx" in ctx.root.options["ssr"][ssr_idx]
                ):
                    ssr_hw_idx = ctx.root.options["ssr"][ssr_idx]["hw_idx"]
                    # body.append(f"snrt_ssr_disable(); // ssr {ssr_hw_idx} use end")
                    if val["is_write"]:
                        # body.append(f"__builtin_ssr_barrier(SNRT_SSR_DM{ssr_hw_idx});")
                        body.append(f"snrt_ssr_barrier(SNRT_SSR_DM{ssr_hw_idx});")
                    body.append(f"// ssr {ssr_hw_idx} use end")
        assert all(isinstance(s, str) for s in body)
        return (decls, body)


def find_ssr_config(scope):
    """
    Returns dict of dicts:
    {
        'ssr_idx': {
            'dims': [dim1, dim2],
            'repeats': [num_rep1, num_rep2, num_rep3],
            'elem_start': elem,
            'rep_offsets': [offset_elem1, offset_elem2, offset_elem3],
            'first_op': [idx, idx, idx],
            'last_op': [idx, idx, idx],
            'is_write': True/False,
        }
    }
    """

    current_conf = {}
    for op_idx, op in enumerate(scope.ops):
        if isinstance(op, Scope):
            nested_conf = find_ssr_config(op)
            for k, v in nested_conf.items():
                if k not in current_conf:
                    current_conf[k] = {
                        "dims": v["dims"] + [op.dim],
                        "repeats": v["repeats"] + [1],
                        "elem_start": v["elem_start"],
                        "rep_offsets": v["rep_offsets"] + [v["elem_start"]],
                        "first_op": v["first_op"] + [op_idx],
                        "last_op": v["last_op"] + [op_idx],
                        "is_write": v["is_write"],
                    }
                else:
                    if current_conf[k]["repeats"][-1] == 1:
                        current_conf[k]["rep_offsets"][-1] = v["elem_start"]
                    current_conf[k]["repeats"][-1] += 1
                    current_conf[k]["last_op"][-1] = op_idx
        elif isinstance(op, Operation):
            for elem_idx, elem in enumerate(op.all_elem):
                if not isinstance(elem, Array):
                    continue
                if "ssr" in elem.options:
                    ssr_idx = elem.options["ssr"]
                    if ssr_idx not in current_conf:
                        # first occurence
                        current_conf[ssr_idx] = {
                            "repeats": [1],
                            "dims": [],
                            "elem_start": elem,
                            "rep_offsets": [elem],
                            "is_write": elem_idx == 0,
                            "first_op": [op_idx],
                            "last_op": [op_idx],
                        }
                    else:
                        # repeated occurence
                        if current_conf[ssr_idx]["last_op"][-1] == op_idx:
                            # in the special case when repeat happened at the same op
                            # do not count it as a repeat
                            continue
                        # include pair of accesses to represent repeat offset
                        if current_conf[ssr_idx]["repeats"][-1] == 1:
                            current_conf[ssr_idx]["rep_offsets"][-1] = elem
                        current_conf[ssr_idx]["repeats"][-1] += 1
                        current_conf[ssr_idx]["last_op"][-1] = op_idx
    return current_conf


def get_reduce_allocation(root, scope):
    """
    Returns arrays that have to be initialized for reduce operations before entering the target 'scope'.
    """
    scope_allocs = get_reduce_allocation_info(root, scope)
    result_arrays = {}
    for k, v in scope_allocs.items():
        if all(a == 0 for a in v["indices"]):
            non_reduce_scope_indices = []
            for x in v['out_elem'].indices:
                pos_l = [pos for idx in x.to_index_list() for pos in [idx.pos]]
                non_reduce_scope_indices = non_reduce_scope_indices + pos_l
                # it is [-idx-2] because [-1] refers to the operator, while idx=0 refers to the last scope
            init_scopes = [(v["scope_nest"][-idx-2], idx) for idx in non_reduce_scope_indices if idx < v["elem_depth"]]
            result_arrays[k] = {
                "out_elem": v["out_elem"],
                "elem_depth": v["elem_depth"],
                "op_str": v["op_str"],
                "init_scopes": init_scopes,
            }
    return result_arrays


def get_reduce_allocation_info(root, scope):
    """
    Returns dict: {
        (reduce_array_name_str, reduce_array_indices): {
            'indices': (set[int]) reduce_indices,
            'out_elem': (Array) out_elem,
            'elem_depth': (int) depth of elem nesting relative to the current scope,
            'op_str': (str) operation string,
            'scope_nest': (list[Scope]) list of scopes that contain this reduce operation
        }
    }
    reduce_dim_indices contain indices relative to the depth of the current scope
    """
    allocation_info = {}
    for op in scope.ops:
        if isinstance(op, Operation) and op.op.reduce:
            reduce_idx, non_reduce_idx = get_reduce_indices(op)
            reduce_array_name = op.out_elem.name
            reduce_array_idx = op.out_elem.indices
            allocation_info[(reduce_array_name, tuple(reduce_array_idx))] = {
                "indices": reduce_idx,
                "out_elem": op.out_elem,
                "elem_depth": 1,  # current 'scope' already introduces one level of nesting
                "op_str": op.op,
                "scope_nest": get_scopes_from_root_to_scope(root, op),
            }
        elif isinstance(op, Scope):
            nested_info = get_reduce_allocation_info(root, op)
            for k, v in nested_info.items():
                cur_alloc_indices = {x - 1 for x in v["indices"] if x > 0}
                if cur_alloc_indices:
                    allocation_info[k] = {
                        "indices": cur_alloc_indices,
                        "out_elem": v["out_elem"],
                        "elem_depth": v["elem_depth"] + 1,
                        "op_str": v["op_str"],
                        "scope_nest": v["scope_nest"],
                    }
    return allocation_info


def find_canonical_array_order(scope, seen_arrays=[]):
    """
    Returns list of arrays in the order of their appearance in the code
    """
    # TODO: consider indirect accesses and getting references to arrays from outside
    arrs = seen_arrays[:]
    for op in scope.ops:
        if isinstance(op, Operation):
            for elem in op.all_elem:
                if elem.name not in arrs:
                    arrs.append(elem.name)
        elif isinstance(op, Scope):
            if op.dim.name not in arrs:
                arrs.append(op.dim.name)
            arrs += find_canonical_array_order(op, arrs)
        else:
            assert 0
    assert len(set(arrs)) == len(arrs)
    return arrs[len(seen_arrays) :]


class RootScope(Scope):
    
    def __init__(self, ops, name, input_declarations, reusable_inputs, used_outputs):
        super().__init__(ops)
        self.name = name
        
        # maps buffer names to declarations that represent these buffers
        self.buffers = {**input_declarations, **self.infer_decls(self, input_declarations)}

        # this should be called after delcarations are inferred because root scope inputs include buffer dimensions
        self.propagate_used_outputs(used_outputs)

        # maps single assignment 'array' variable names to 'buffer' names where they are stored
        self.arr_to_buf = {x: x for x in self.buffers}

        # set of input array names that can be reused to perform computation
        self.reusable_inputs = reusable_inputs

        # record originally requested input and output buffer sizes
        inp, tmp, out = self.get_inp_tmp_out()
        inp_out = inp | out
        self.frozen_dims = {}
        for arr_name, arr in self.buffers.items():
            if arr_name in inp_out:
                self.frozen_dims[arr_name] = arr.dims

        self.transform_group = "schedule"
        
        self.concepts = set()

        self.options = {}
    
        
    def reused_out_as_inp(self):
        res = {}
        inp, tmp, out = self.get_inp_tmp_out()
        for i in inp:
            for o in out:
                if self.arr_to_buf[i] == self.arr_to_buf[o]:
                    res[o] = i
        return res
        
    def canonicalize_strings(self):
        # order all arrays by the order of their appearance in the code
        ordered_arrays = find_canonical_array_order(self)
        ordered_bufs = []
        for arr_name in ordered_arrays:
            buf_name = self.arr_to_buf[arr_name]
            if buf_name not in ordered_bufs:
                ordered_bufs.append(buf_name)
        # rename arrays and buffers to be in the canonical order
        arr_rename = {}
        for idx, arr in enumerate(ordered_arrays):
            arr_rename[arr] = f"arr{idx}"
        buf_rename = {}
        for idx, buf in enumerate(ordered_bufs):
            buf_rename[buf] = f"buf{idx}"
        # self.arr_to_buf
        self.arr_to_buf = {
            arr_rename[arr_name]: buf_rename[buf_name]
            for arr_name, buf_name in self.arr_to_buf.items()
        }
        # self.used_outputs
        self.used_outputs = {arr_rename[x] for x in self.used_outputs}
        # self.reusable_inputs
        self.reusable_inputs = {arr_rename[x] for x in self.reusable_inputs}
        # self.buffers
        self.buffers = {
            buf_rename[buf_name]: buf for buf_name, buf in self.buffers.items()
        }
        # self.ops
        for op in self.ops:
            op.canonicalize_strings(arr_rename)

    def buf_to_arr(self, buf_name):
        arr_names = set()
        for a, b in self.arr_to_buf.items():
            if buf_name == b:
                arr_names.add(a)
        if not arr_names:
            raise ValueError(f"Buffer {buf_name} is not found!")
        return arr_names
    
    def buf_of_arr(self, arr_name):
        return self.buffers[self.arr_to_buf[arr_name]]

    @property
    def external_bufs(self):
        """
        Returns a dictionary {buf_name: buf_instance}
        of buffers allocated outside for either input or output
        """
        inp, tmp, out = self.get_inp_tmp_out()
        result = {}
        for arr_name in inp | out:
            buf_name = self.arr_to_buf[arr_name]
            buf = self.buffers[buf_name]
            if not isinstance(buf, Buffer):
                continue  # ignore Const bufs
            result[buf_name] = buf
        return result

    @property
    def internal_bufs(self):
        """
        Returns a disctionary {buf_name: buf_instance}
        of buffers allocated inside the scope for temporary storage
        """
        bufs = self.buffers
        ext = self.external_bufs
        return {k: v for k, v in bufs.items() if k not in ext}

    def get_inp_out(self):
        inputs = set()
        outputs = set()
        for op in self.ops:
            op_inps, op_outs = op.get_inp_out()
            inputs |= op_inps
            outputs |= op_outs
        # some inputs may define dimensions, but not be used in computation
        for buf in self.buffers.values():
            for dim in buf.dims:
                inputs |= dim.used_arrs()
        return inputs - outputs, outputs

    def __repr__(self):
        return f"RootScope({self.name})"
        # res = f"Root({self.name}, [\n"
        # for idx, op in enumerate(self.ops):
        #     delim = "\n" if idx < len(self.ops) - 1 else "],\n"
        #     res += indent(f"{op}{delim}", 2 * " ")
        # for idx, (k, v) in enumerate(self.buffers.items()):
        #     delim = "\n" if idx < len(self.buffers) - 1 else ")\n"
        #     bufs = sorted(self.buf_to_arr(k))
        #     res += f"{bufs} -> {v}{delim}"
        # res += str(self.options.get("ssr", ""))
        # return res

    def program_hash(self):
        val = str(hashlib.sha1(self.text().encode("utf-8")).hexdigest())
        return val
    
    def text(self, print_annotations: bool = False):
        # Name: sigmoid
        # In: x
        # Out: z
        # Declarations:
        #     x f32 [{M}, {N}] heap
        #     z f32 [{M}, {N}] heap
        #     a f32 [{M}, {N}] heap
        #     b f32 [{M}, {N}] heap
        #     c f32 [{M}, {N}] heap
        # Code:
        res = []
        res.append(f"Name: {self.name}")

        # Prints the input and output arrays
        inputs, _, outputs = self.get_inp_tmp_out()
        res.append("In: " + ", ".join(sorted(inputs)))

        # # Adds transform annotations to the input arrays
        # # Applicable to 'reuse_buffers'
        # for arr_name in sorted(inputs):
        #     buf_name = self.arr_to_buf[arr_name]
        #     assert buf_name in self.buffers
        #     buffer = self.buffers[buf_name]
        #     front_annotations = ""
        #     back_annotations = ""
        #     if print_annotations:
        #         # If there are annotations
        #         # annotations = " ".join([x[0] for x in buffer.transform_annotations])
        #         front_annotations = " ".join([x for f, x in buffer.transform_annotations if f])
        #         back_annotations = " ".join([x for f, x in buffer.transform_annotations if not f])
        #     op_lines.append([f"{front_annotations} {buf_name} {back_annotations}".strip()])
        #     # op_lines.append([f"{buf_name} {annotations}".strip()])

        res.append("Out: " + ", ".join(sorted(outputs)))
        # res.extend([[f"{self.arr_to_buf[arr_name]}"] for arr_name in sorted(outputs)])

        res.append("Declarations:")
        for buf_name, buf in sorted(self.buffers.items()):
            arr_names = self.buf_to_arr(buf_name)
            dim_string = ", ".join(
                (str(dim.value) if isinstance(dim, ConstElem) else dim.name) + ("" if used else ":N")
                for dim, used
                in zip(buf.dims, buf.used_dims)
            )
            res.append(f"    {buf_name} {buf.dtype} [{dim_string}] {buf.allocation}")
            if arr_names != {buf_name}:
                array_mapping = ', '.join(sorted(arr_names))
                res[-1] += ' -> ' + array_mapping

        res.append("Code:")

        op_lines = []
        op_lines.extend([line for op in self.ops for line in op.text(self, print_annotations)])
        # find width of all dimension descriptions to select proper spacing
        dim_descs = []
        for line in op_lines:
            scope_nest = line[:-1]
            for idx, dim in enumerate(scope_nest):
                if idx >= len(dim_descs):
                    dim_descs.append(len(dim))
                else:
                    dim_descs[idx] = max(dim_descs[idx], len(dim))
        # pad to the same operator start alignment
        max_scope_depth = max(len(line) for line in op_lines)
        for line in op_lines:
            if line[0] == "In:" or line[0] == "Out:" or line[0] == "Program:":
                continue
            line[-1:-1] = [""] * (max_scope_depth - len(line))
        # generate text
        for line in op_lines:
            scope_nest = line[:-1]
            operation = line[-1]
            prefix = " ".join(f"{dim:>{desc_len}}" for dim, desc_len in zip(scope_nest, dim_descs))
            if prefix:
                prefix += " " # add space before operation
            res.append(prefix + operation)
        return '\n'.join(res)

    def gen_body(self, ctx):
        return self.gen_scope_ops(ctx, unrolled_ops=self.ops)

    def to_dict(self) -> Dict:
        res = {
            "obj": self.__class__.__name__,
            "name": self.name,
            "buffers": {key: buff.to_dict() for key, buff in self.buffers.items()},
            "arr_to_buf": self.arr_to_buf,
            "reusable_inputs": sorted(self.reusable_inputs),
            "used_outptus": sorted(self.used_outputs),
            "ops": [op.to_dict() for op in self.ops],
            "transform_group": self.transform_group,
            "transforms": self.transform_annotations,
        }
        assert not (self.options.keys() - {'max_ssr_idx', 'ssr'})  # make sure I didn't miss any fields
        if self.options.get("ssr"):
            res["ssr"] = self.options["ssr"]
        return res


def loop_scope(ops, dims):
    if len(dims) == 0:
        return ops
    else:
        return LoopScope(loop_scope(ops, dims[1:]), dims[0])


def generate_asm_op_range(
    ctx, target_scope, op_range
):
    code = []
    code.append("asm volatile(")

    gen_acc = (
        {}
    )  # generated_accesses = {access str: (access asm name, access type, op_dtype, arg_dtype)}

    prefix = []

    for op_idx, op in enumerate(op_range):
        op_str, acc_map = op.gen_asm_body(
            ctx.at_depth(ctx.depth + 1),
            op_idx=op_idx,
        )

        for acc_idx, (access_name, (access_str, acc_type, op_dtype, arg_dtype)) in enumerate(
            acc_map.items()
        ):
            is_write = acc_type == "+" or acc_type == "="
            if access_str in gen_acc:
                if is_write and gen_acc[access_str][1] == "":
                    gen_acc[access_str][1] = "+"
                if not is_write and gen_acc[access_str][1] == "=":
                    gen_acc[access_str][1] = "+"
                reused_acc_name = gen_acc[access_str][0]
                op_str = op_str.replace(access_name, reused_acc_name)
            else:
                gen_acc[access_str] = [access_name, acc_type, op_dtype, arg_dtype]
        code.append(op_str)

    rw_accesses = {x: y for x, y in gen_acc.items() if y[1].startswith("+")}
    w_accesses = {x: y for x, y in gen_acc.items() if y[1].startswith("=")}
    r_accesses = {
        x: y for x, y in gen_acc.items() if x not in rw_accesses and x not in w_accesses
    }

    out_asm_operands = []
    for access_str, (access_name, acc_type, op_dtype, arg_dtype) in {
        **rw_accesses,
        **w_accesses,
    }.items():
        dtype_literal = {
            "f64": "f",
            "f32": "f",
            "i32": "r",
        }[op_dtype]
        if op_dtype != arg_dtype:
            raise NotImplementedError()
        # outputs can overwrite inputs, so explicitly early-clobber all outputs
        # (TODO) except if output happens only by the last instruction
        if acc_type in ("=", "+"):
            acc_type = acc_type + "&" # early clobber
        out_asm_operands.append(
            f'[{access_name}] "{acc_type}{dtype_literal}"({access_str})'
        )

    inp_asm_operands = []
    for access_str, (access_name, acc_type, op_dtype, arg_dtype) in r_accesses.items():
        dtype_literal = {
            "f64": "f",
            "f32": "f",
            "i32": "r",
        }[op_dtype]
        if op_dtype != arg_dtype:
            full_access_str = f"({op_dtype})({access_str})"
        else:
            full_access_str = access_str
        inp_asm_operands.append(
            f'[{access_name}] "{acc_type}{dtype_literal}"({full_access_str})'
        )

    code.append(": " + ", ".join(out_asm_operands))
    code.append(": " + ", ".join(inp_asm_operands))
    code.append(': "ft0", "ft1", "ft2", "memory"')
    code.append(");")
    return prefix + code


def generate_asm_scope(ctx, frep_scope, unrolled_ops):
    return generate_asm_op_range(
        ctx, frep_scope, unrolled_ops
    )


def generate_frep_scope(ctx, frep_scope, unrolled_ops):
    scope_body = generate_asm_scope(ctx, frep_scope, unrolled_ops)
    asm_start_idx = scope_body.index("asm volatile(")
    frep_rep = frep_scope.dim.gen_access(ctx)
    frep_ops = len(unrolled_ops)
        
    # scope_body.insert(asm_start_idx + 1, '"frep.o %[frep_rep], %[frep_ops], 0, 0;"')
    scope_body.insert(asm_start_idx + 1, 'FREP(frep_ops, frep_rep)')

    inp_clobber_idx = scope_body.index(': "ft0", "ft1", "ft2", "memory"') - 1
    assert scope_body[inp_clobber_idx].startswith(": ")
    if scope_body[inp_clobber_idx] != ": ":
        scope_body[inp_clobber_idx] += ", "
    scope_body[
        inp_clobber_idx
    ] += f'[frep_rep] "r"({frep_rep} - 1), [frep_ops] "i"({frep_ops} - 1)'

    return scope_body


def find_omp_collapsable_loops(ctx, scope):
    """
    Return the number of loops that can be collapsed into the current OpenMP loop.
    Return 0 if this loop is part of the outer loop that should be collapsed instead.
    Return 1 if no collapsing is possible, only current scope is using OpenMP.
    """
    # first, check if outer collases current
    scopes_to_target = get_scopes_from_root_to_scope(ctx.root, scope, with_root=True, with_scope=False)
    if scopes_to_target[-1].options.get("parallel", False) and len(scopes_to_target[-1].ops) == 1:
        return 0  # outer loop already collapsed with current
    num_collapsable = 1
    while len(scope.ops) == 1 and isinstance(scope.ops[0], LoopScope) and scope.ops[0].options.get("parallel", False):
        num_collapsable += 1
        scope = scope.ops[0]
    return num_collapsable


class LoopScope(Scope):
    
    def __init__(self, ops, dim):
        super().__init__(ops)
        self.dim = canonicalize_access(dim)

    def __repr__(self):
        return f"LoopScope({self.name}, dim={self.dim})"
    
    def text(self, root_scope, print_annotations: bool = False):
        ops_lines = [line for op in self.ops for line in op.text(root_scope, print_annotations)]
        options = ""
        if self.options.get("unroll", False):
            options += ":u"
        if self.options.get("frep", False):
            options += ":f"
        if self.options.get("vectorize", False):
            options += ":v"
        if self.options.get("parallel", False):
            if self.options.get("parallel_level") == "cuda_grid":
                options += ":g"
            elif self.options.get("parallel_level") == "cuda_block":
                options += ":b"
            elif self.options.get("parallel_level") == "cuda_warp":
                options += ":w"
            elif self.options.get("parallel_level") == "cuda_fragment":
                options += ":t"  # t = Tensor
            else:
                options += ":p"
        # Ignores the 'is_front' annotation for the loop scope
        annotations = ' '.join([a[1] for a in self.transform_annotations]) if print_annotations else ""
        scope_prefix = f"{annotations} {self.dim.text(root_scope)}{options}".strip()
        res_text = [[scope_prefix if idx == 0 else '|', *op_line] for idx, op_line in enumerate(ops_lines)]
        return res_text
    
    def peak_performance_cycles(self):
        cycles = 0
        for op in self.ops:
            nested_peak = op.peak_performance_cycles()
            if nested_peak is None:
                return None
            cycles += nested_peak
        if not isinstance(self.dim, ConstElem):
            # Loop dimension must be constant to estimate peak
            return None
        return cycles * self.dim.value
        

    @property
    def name(self):
        nested_op = self.ops[0]
        if isinstance(nested_op, Operation):
            return nested_op.name + "#0"
        elif isinstance(nested_op, Scope):
            nested_name = nested_op.name
            assert "#" in nested_name
            split_name = nested_op.name.split("#")
            nesting_depth = str(int(split_name[-1]) + 1)
            return "#".join(split_name[:-1] + [nesting_depth])
        assert 0
        
    def gen_parallel_cuda(self, ctx):
        """
            Return the body of the loop mapped to CUDA grid-block structure.
        """
        
        # find configuration for kernel call
        cur_scope = self
        grid_dims = []
        block_dims = []
        warp_dims = []
        while isinstance(cur_scope, LoopScope):
            if cur_scope.options.get("parallel_level") == "cuda_grid":
                grid_dims.append(cur_scope.dim)
            elif cur_scope.options.get("parallel_level") == "cuda_block":
                block_dims.append(cur_scope.dim)
            elif cur_scope.options.get("parallel_level") ==  "cuda_warp":
                warp_dims.append(cur_scope.dim)
            cur_scope = cur_scope.ops[0]
        
        # now we can generate code of the cuda kernel
        # arguments: all indices initialized so far
        outer_scopes = get_scopes_from_root_to_scope(ctx.root, self, with_root=False, with_scope=False)
        passed_indices = [f"i{i}" for i, _ in enumerate(outer_scopes)]
        received_indices = [f"i32 i{i}" for i, _ in enumerate(outer_scopes)]
        
        # arguments: buffers that need to be passed inside the kernel
        inp, tmp, out = self.get_inp_tmp_out()
        buf_names = sorted({ctx.root.arr_to_buf[arr_name] for arr_name in inp | tmp | out})
        # remove non-heap buffers as they do not need to be passed
        buf_names = [buf_name for buf_name in buf_names if ctx.root.buffers[buf_name].allocation == "heap"]
        bufs = {buf_name: ctx.root.buffers[buf_name] for buf_name in buf_names}
        passed_buffers = [buf_name for buf_name in bufs]
        received_buffers = [f"{bufs[buf_name].dtype} *{buf_name}" for buf_name in bufs]
        
        passed_args = ", ".join(passed_indices + passed_buffers)
        received_args = ", ".join(received_indices + received_buffers)
        
        # k_idx = len(outer_scopes)
        # indices_list = []
        # for (idx, grid_dim), dim_letter in zip(enumerate(grid_dims), "xyz"):
        #     indices_list.append(f"i32 i{k_idx+idx} = blockIdx.{dim_letter};")
        # k_idx += len(grid_dims)
        # warp_size = 32
        # for (idx, block_dim), dim_letter in zip(enumerate(block_dims), "xyz"):
        #     if dim_letter == "x" and warp_dims:
        #         indices_list.append(f"i32 i{k_idx+idx} = threadIdx.{dim_letter} / {warp_size};")
        #     else:
        #         indices_list.append(f"i32 i{k_idx+idx} = threadIdx.{dim_letter};")
        # k_idx += len(block_dims)
        # if warp_dims:
        #     indices_list.append(f"i32 i{k_idx} = threadIdx.x % {warp_size};")
        
        func_decls = []
        scope_code = []
        # block_split_occured = False
        # warp_split_occured = False
        # for depth, scope in recursive_scopes_with_depth(self, depth=0):
        #     if depth == len(grid_dims):
        #         if block_split_occured:
        #             scope_code.append("__syncthreads();")
        #         else:
        #             block_split_occured = True
        #     if depth == len(grid_dims) + len(block_dims):
        #         if warp_split_occured:
        #             scope_code.append("__syncwarp();")
        #         else:
        #             warp_split_occured = True
        #     if depth == len(grid_dims) + len(block_dims) + len(warp_dims) - 1:
        #         nested_func_decls, nested_scope_code = scope.gen_scope_ops(
        #             ctx.at_depth(ctx.depth + depth),
        #             unrolled_ops=scope.ops,
        #         )
        #         func_decls += nested_func_decls
        #         scope_code += nested_scope_code
        
        func_name = self.name.replace("#", "_hash_")
        

        stack_allocs = gen_cuda_allocs(ctx, self)
        
        function_declarations = func_decls + [
            f"__global__ void {func_name}({received_args}) {{",
        # ] + indices_list + stack_allocs + scope_code + [
        ] + stack_allocs + scope_code
        
        grid_size = [d.gen_access(ctx) for d in grid_dims]
        block_size = [d.gen_access(ctx) for d in block_dims]
        warp_size = [d.gen_access(ctx) for d in warp_dims]
        total_warp_size = math.prod(int(x) for x in warp_size)
        assert 32 == total_warp_size
        # if warp_dims:
        #     if block_size:
        #         block_size[0] = f"{block_size[0]} * {warp_size}"
        #     else:
        #         block_size.append(str(warp_size))
        grid_size_str = " * ".join(grid_size)
        block_size_str = " * ".join(block_size)
        
        # determine shared memory size
        inp, tmp, out = self.get_inp_tmp_out()
        shared_mem_size = ctx.alloc_info.cache_sizes.get(self, "0")
        
        scope_code = [
            f"checkCudaErrors(cudaFuncSetAttribute({func_name}, cudaFuncAttributeMaxDynamicSharedMemorySize, {shared_mem_size}));",
            f"{func_name}<<<{grid_size_str}, ({block_size_str}) * {total_warp_size}, {shared_mem_size}>>>({passed_args});",
            f'checkCudaErrors(cudaGetLastError());',
            f'checkCudaErrors(cudaDeviceSynchronize());',
        ]
        assert all(isinstance(s, str) for s in scope_code)
        return (function_declarations, scope_code)


    def generate_vectorized(self, ctx):
        stack_allocs = gen_stack_allocs(ctx, self)
        heap_allocs, heap_frees = gen_scoped_heap_allocs(ctx, self)
        # loop should contain exactly one operation
        assert len(self.ops) == 1
        assert isinstance(self.ops[0], Operation)
        operation = self.ops[0]
        # now process all inputs and outputs to bring them into vectorized form
        simd = self.options["simd"]
        op_decls, operation_code = operation.gen_vectorized_body(ctx, simd)
        scope_code = [
            "{",
            *stack_allocs,
            *heap_allocs,
            *operation_code,
            *heap_frees,
            "}"
        ]
        function_declarations = op_decls
        return (function_declarations, scope_code)

    def gen_body(self, ctx):
        scope_nest = get_scopes_from_root_to_scope(ctx.root, self, with_root=False, with_scope=False)
        inside_kernel = any((scope.options.get("parallel_level") == "cuda_grid") for scope in scope_nest)
        
        function_declarations = []
        scope_code = []

        dim = self.dim

        loop_prefix = []

        if self.options.get("vectorize", False):
            return self.generate_vectorized(ctx)

        # reduce initialization start
        reduce_allocs = get_reduce_allocation(ctx.root, self)
        for name_indices, val in reduce_allocs.items():
            # val["elem_depth"] is the number of scopes from self to op not including op
            # scope_depth is the number of scopes from root to self not including self
            # full_elem_depth is the number of scopes from root to op not including op
            #                 is also the depth of operation
            full_elem_depth = ctx.depth + val["elem_depth"] 
            
            arr = val["out_elem"]
            buf_name = ctx.root.arr_to_buf[arr.name]
            buf = ctx.root.buffers[buf_name]
                
            if buf.allocation.startswith("fragment"):
                # special handling for fragment buffers
                assert ctx.target == "cuda"
            
            addr_depth = full_elem_depth - 1
            if buf.allocation.startswith("fragment"):
                # reduction over fragment is specified with last 3 dimensions M N K
                # reduction initialization infers from there, so it needs to remove these 3 dimensions
                addr_depth -= 3
            
            # elem_write_str = val["out_elem"].gen_access(
            #     ctx.at_depth(full_elem_depth)
            # )
            elem_write_str = val["out_elem"].gen_address(
                ctx.at_depth(full_elem_depth), addr_depth
            )

            init_val = val['op_str'].reduce_identity()
            out_type = val["out_elem"].get_dtype(ctx.root)
            if out_type.is_vector():
                init_val = f"vector_bcast_{out_type}({init_val})"
            
            init_scopes = val["init_scopes"]
            if buf.allocation.startswith("fragment"):
                # skip last two scopes since they refer to the shape of the fragment
                assert len(init_scopes) >= 2
                init_scopes = init_scopes[:-2]
            
            for init_scope, rdim_idx in init_scopes:
                rdim = init_scope.dim
                rdim_dt = rdim.get_dtype(ctx.root)
                # rdim_idx is from [0, val["elem_depth"]) 
                rinit_scope_depth = full_elem_depth - rdim_idx - 1  # -1 becaue of operator
                rdim_acc = rdim.gen_access(ctx.at_depth(rinit_scope_depth))
                ridx = rinit_scope_depth - 1
                # if ridx refers to last dim of a vector buffer, loop should be removed by setting rdim_acc to 0
                if buf.dtype.is_vector() and ridx == len(buf.dims):
                    rdim_acc = "1"
                loop_prefix.append(f"for ({rdim_dt} i{ridx} = 0; i{ridx} < {rdim_acc}; i{ridx}++) {{")
            if buf.allocation.startswith("fragment"):
                # elem_write_str is 'foo[scalar_address]'
                # it should be converted to 'foo[(scalar_address) / (fragment_size)]' by dividing by fragment size
                frag_size = ' * '.join([s.dim.gen_access(ctx) for s, i in val["init_scopes"][-2:]])
                fixed_access = re.sub(r'\[(.*?)\]', f'[(\\1) / ({frag_size})]', elem_write_str)
                loop_prefix.append(f"wmma::fill_fragment({fixed_access}, {init_val});  // init reduce")
            else:
                # default implementation
                loop_prefix.append(f"{elem_write_str} = {init_val};  // init reduce")
            # loop_prefix.append(f'asm volatile("fmv.d %0, %1;" : "=f"({elem_write_str}) : "f"((double){init_val}) : "ft0", "ft1", "ft2", "memory");  // init reduce')
            for init_scope, rdim_idx in init_scopes:
                loop_prefix.append("}")

        if self.options.get("parallel", False) and ctx.target == "native":
            # try to collapse inner loops when possible
            num_collapsed = find_omp_collapsable_loops(ctx, self)
            if num_collapsed == 0:
                # this loop is collapsed into the outer loop, nothing to do
                pass
            elif num_collapsed == 1:
                # default non-collapsed case
                loop_prefix.append("#pragma omp parallel for")
            else:
                loop_prefix.append(f"#pragma omp parallel for collapse({num_collapsed})")

        unrolled_ops = self.ops
        is_unrolled = self.options.get("unroll", False)
        if is_unrolled:
            unrolled_ops = get_unrolled_body(ctx, self)
        elif self.options.get("frep", False):
            # in the FREP case, we need to unroll all nested scopes before applying FREP
            scope_code = get_frep_body(self, ctx)
            return ([], loop_prefix + scope_code)      

        idx = ctx.depth - 1  # account for the loopless root scope

        dtype = dim.get_dtype(ctx.root)

        # if current scope unrolled, scope_depth should be reduced to generate nested code
        if is_unrolled:
            ctx = ctx.at_depth(ctx.depth - 1)
        
        if self.options.get("parallel_level") == "cuda_fragment":
            # depending on the operation and direction of movement , insert the appropriate operation
            tail_scope_nest = [self]
            while isinstance(tail_scope_nest[-1], LoopScope):
                tail_scope_nest.append(tail_scope_nest[-1].ops[0])
            if len(tail_scope_nest) == 3:
                # move fragment operation

                # find direction of movement
                op = tail_scope_nest[-1]
                dst_buf_name = ctx.root.arr_to_buf[op.out_elem.name]
                dst_buf = ctx.root.buffers[dst_buf_name]
                [inp_elem] = op.inp_elem
                src_buf_name = ctx.root.arr_to_buf[inp_elem.name]
                src_buf = ctx.root.buffers[src_buf_name]
                is_copy_in = dst_buf.allocation.startswith("fragment")

                if is_copy_in:
                    output_acc = op.out_elem.gen_address(ctx.at_depth(ctx.depth+2), ctx.depth-1)
                    input_acc = inp_elem.gen_address(ctx.at_depth(ctx.depth+2), ctx.depth-1)
                    inp_buf =  ctx.root.buffers[ctx.root.arr_to_buf[inp_elem.name]]
                    strided_input_dim = [d for d, u in zip(inp_buf.dims, inp_buf.used_dims) if u][-1]
                    strided_input_dim_access = strided_input_dim.gen_access(ctx)
                    
                    frag_size = ' * '.join(s.dim.gen_access(ctx) for s in tail_scope_nest[:-1])
                    output_acc_frag = re.sub(r'\[(.*?)\]', f'[(\\1) / ({frag_size})]', output_acc)
                    
                    op_assign = f"wmma::load_matrix_sync({output_acc_frag}, &{input_acc}, {strided_input_dim_access});"
                    return ([], [op_assign])
                else:
                    output_acc = op.out_elem.gen_address(ctx.at_depth(ctx.depth+2), ctx.depth-1)
                    input_acc = inp_elem.gen_address(ctx.at_depth(ctx.depth+2), ctx.depth-1)
                    out_elem = op.out_elem
                    out_buf =  ctx.root.buffers[ctx.root.arr_to_buf[out_elem.name]]
                    
                    # 1 refers to the second to last innermost index, while innermost is 0
                    [strided_idx_pos] = [p for p, i in enumerate(out_elem.indices) if 1 in i.accessed_pos()]
                    output_stride_dims = [d for i, (d, u) in enumerate(zip(out_buf.dims, out_buf.used_dims)) if (u and i > strided_idx_pos)]
                    output_stride_dims_access = ' * '.join([d.gen_access(ctx) for d in output_stride_dims])

                    frag_size = ' * '.join(s.dim.gen_access(ctx) for s in tail_scope_nest[:-1])
                    input_acc_frag = re.sub(r'\[(.*?)\]', f'[(\\1) / ({frag_size})]', input_acc)

                    layout = src_buf.allocation.split('_')[-1]
                    op_assign = f"wmma::store_matrix_sync(&{output_acc}, {input_acc_frag}, {output_stride_dims_access}, wmma::mem_{layout}_major);"
                    return ([], [op_assign])

            elif len(tail_scope_nest) == 4:
                op = tail_scope_nest[-1]
                output_acc = op.out_elem.gen_address(ctx.at_depth(ctx.depth+3), ctx.depth-1)
                inp_a_acc, inp_b_acc = [elem.gen_address(ctx.at_depth(ctx.depth+3), ctx.depth-1) for elem in op.inp_elem]
                
                M, N, K = [s.dim.gen_access(ctx) for s in tail_scope_nest[:-1]]
                a_frag = ' * '.join([M, K])
                b_frag = ' * '.join([K, N])
                c_frag = ' * '.join([M, N])
                inp_a_frag = re.sub(r'\[(.*?)\]', f'[(\\1) / ({a_frag})]', inp_a_acc)
                inp_b_frag = re.sub(r'\[(.*?)\]', f'[(\\1) / ({b_frag})]', inp_b_acc)
                output_frag = re.sub(r'\[(.*?)\]', f'[(\\1) / ({c_frag})]', output_acc)
                
                op_assign = f"wmma::mma_sync({output_frag}, {inp_a_frag}, {inp_b_frag}, {output_frag});"
                return ([], [op_assign])
            else:
                raise ValueError("Invalid fragment operation")
        
        
        nested_decls, nested_code = self.gen_scope_ops(
            ctx,
            unrolled_ops=unrolled_ops,
        )

        if is_unrolled:
            # if loop unrolled, there is no need to generate loop start and end
            loop_start = ["{"]
            loop_end = ["}"]
        else:
            loop_start = [f"for ({dtype} i{idx} = 0; i{idx} < {dim.gen_access(ctx)}; i{idx}++) {{"]
            loop_end = ["}"]
        
        cur_scope = self
        grid_dims = []
        block_dims = []
        warp_dims = []
        while isinstance(cur_scope, LoopScope):
            if cur_scope.options.get("parallel_level") == "cuda_grid":
                grid_dims.append(cur_scope.dim)
            elif cur_scope.options.get("parallel_level") == "cuda_block":
                block_dims.append(cur_scope.dim)
            elif cur_scope.options.get("parallel_level") ==  "cuda_warp":
                warp_dims.append(cur_scope.dim)
            cur_scope = cur_scope.ops[0]

        if self.options.get("parallel_level") == "cuda_grid":
            inner_grid_size = " * ".join([d.gen_access(ctx) for d in grid_dims[1:]])
            inner_grid_size = f" / ({inner_grid_size})" if inner_grid_size else ""
            inner_grid_size += f" % ({self.dim.gen_access(ctx)})"
            loop_start = ["{", f"{dtype} i{idx} = blockIdx.x{inner_grid_size};"]
            loop_end = ["}"]
        if self.options.get("parallel_level") == "cuda_block":
            inner_block_size = " * ".join([d.gen_access(ctx) for d in block_dims[1:]])
            inner_block_size = f" / ({inner_block_size})" if inner_block_size else ""
            inner_block_size += f" % ({self.dim.gen_access(ctx)})"
            loop_start = ["{", f"{dtype} i{idx} = (threadIdx.x / 32){inner_block_size};"]
            inside_block = any((scope.options.get("parallel_level") == "cuda_block") for scope in scope_nest)
            parent_is_grid = scope_nest[-1].options.get("parallel_level") == "cuda_grid"
            self_is_last = scope_nest[-1].ops[-1] == self
            skip_sync = (parent_is_grid and self_is_last) or inside_block
            loop_end = ["}"] if skip_sync else ["__syncthreads();", "}"]
        if self.options.get("parallel_level") == "cuda_warp":
            inner_warp_size = " * ".join([d.gen_access(ctx) for d in warp_dims[1:]])
            inner_warp_size = f" / ({inner_warp_size})" if inner_warp_size else ""
            inner_warp_size += f" % ({self.dim.gen_access(ctx)})"
            loop_start = ["{", f"{dtype} i{idx} = (threadIdx.x % 32){inner_warp_size};"]
            inside_warp = any((scope.options.get("parallel_level") == "cuda_warp") for scope in scope_nest)
            parent_is_block = scope_nest[-1].options.get("parallel_level") == "cuda_block"
            self_is_last = scope_nest[-1].ops[-1] == self
            skip_sync = (parent_is_block and self_is_last) or inside_warp
            loop_end = ["}"] if skip_sync else ["__syncwarp();", "}"]

        stack_allocs = gen_stack_allocs(ctx, self)
        heap_allocs, heap_frees = gen_scoped_heap_allocs(ctx, self)
        loop_start = [*loop_start, *stack_allocs, *heap_allocs]
        loop_end = [*heap_frees, *loop_end]

        function_declarations = nested_decls
        scope_code = [*loop_prefix, *loop_start, *nested_code, *loop_end]
        assert all(isinstance(s, str) for s in scope_code)

        if self.options.get("parallel_level") == "cuda_grid" and not inside_kernel:
            # wrap scope code into a CUDA kernel
            cuda_kernel_decl, cuda_kernel_code = self.gen_parallel_cuda(ctx)
            function_declarations = nested_decls + cuda_kernel_decl + scope_code + ["}"]
            scope_code = cuda_kernel_code
            
        return (function_declarations, scope_code)


    def canonicalize_strings(self, arr_rename):
        self.dim.canonicalize_strings(arr_rename)
        for op in self.ops:
            op.canonicalize_strings(arr_rename)

    def to_dict(self) -> Dict:
        res = {
            "obj": self.__class__.__name__,
            "dim": self.dim.to_dict(),
            "ops": [op.to_dict() for op in self.ops],
            "transforms": self.transform_annotations,
        }
        assert not (self.options.keys() - {'unroll', 'frep'})  # make sure I didn't miss any fields
        if self.options.get("unroll"):
            res["unroll"] = self.options["unroll"]
        if self.options.get("frep"):
            res["frep"] = self.options["frep"]
        return res
    
def get_frep_body(frep_scope, ctx):
    unrolled_ops = []
    ops = frep_scope.ops
    is_unrolled = isinstance(ops[0], Operation) or ops[0].options.get("unroll", False)
    for op in ops:
        if isinstance(op, LoopScope) and len(ops) == 1 and not is_unrolled:
            if isinstance(frep_scope.dim, ConstElem) and isinstance(ops[0].dim, ConstElem):
                new_scope = LoopScope(ops[0].ops, frep_scope.dim.value * ops[0].dim.value)
            else:
                new_scope = LoopScope(ops[0].ops, IndexExpr(frep_scope.dim, "*", ops[0].dim))
            return get_frep_body(new_scope, ctx.at_depth(ctx.depth + 1))
        elif isinstance(op, Scope):
            unrolled_ops.extend(get_unrolled_body(ctx, op))
        else:
            unrolled_ops.append(op)
    return generate_frep_scope(ctx, frep_scope, unrolled_ops)


def make_signature(ctx):
    items = []
    inp, tmp, out = ctx.root.get_inp_tmp_out()
    sorted_inputs = sorted(inp)
    sorted_outputs = sorted(out)
    used_names = {}  # name: number of uses
    for arr_name in [*sorted_inputs, *sorted_outputs]:
        buf_name = ctx.root.arr_to_buf[arr_name]
        buf = ctx.root.buffers[buf_name]
        if not isinstance(buf, Buffer):
            continue
        unused_suffix = ""
        if buf_name in used_names:
            unused_suffix = f"_unused{used_names[buf_name]}"
            used_names[buf_name] += 1
        else:
            used_names[buf_name] = 1
        items.append(f"{buf.dtype}* __restrict__ {buf_name}{unused_suffix}")
    items.append("void* __restrict__ _tmp")
    return ", ".join(items)


def compute_used_memory_size(root_scope: RootScope, memory_type: str, buf_override: Dict[str, Buffer] = {}):
    assert memory_type in ("stack", "cache", "heap")
    used_memory_size = 0
    for buf_name, buf in root_scope.internal_bufs.items():
        if buf.allocation != memory_type:
            continue
        if buf_name in buf_override:
            used_memory_size += buf_override[buf_name].size_bytes()
        else:
            used_memory_size += buf.size_bytes()
    return used_memory_size


def find_bufs_movable_to_memory(root_scope, max_size, target):
    """
        Returns buffer names that can be placed on memory indicated with target limited by the max_size.
        - their size should be constant
        - their size should be less than supplied max_size (total size for entire kernel)
    """
    
    used_stack_size = compute_used_memory_size(root_scope, target)
    
    result = set()
    for buf_name, buf in root_scope.internal_bufs.items():
        if not isinstance(buf, Buffer):
            continue
        if buf.allocation == target:
            continue  # already on stack
        materialized_shape = [d for d, u in zip(buf.dims, buf.used_dims) if u]
        const_shape = True
        for dim in materialized_shape:
            if not isinstance(dim, ConstElem):
                const_shape = False
        if not const_shape:
            continue
        added_stack_size = buf.size_bytes()
        if used_stack_size + added_stack_size > max_size:
            continue  # not enough space
        result.add((buf_name,))
        
    result = sorted(result)
    return result


def find_bufs_movable_to_memory_cuda(root_scope, max_size, memory):
    """
    Only temporary buffers inside the loop nests can be moved to kernel-local memory on CUDA.
    """
    cuda_temporaries = set()
    for scope in recursive_scopes(root_scope):
        if not scope.options.get("parallel", False):
            continue
        if not isinstance(scope.ops[0], LoopScope):
            continue
        if scope.ops[0].options.get("parallel", False):
            continue
        _, tmp, _ = scope.get_inp_tmp_out()
        cuda_temporaries |= tmp        
    
    result = []
    candidates = find_bufs_movable_to_memory(root_scope, max_size, memory)
    for (buf_name,) in candidates:
        arrays = root_scope.buf_to_arr(buf_name)
        if arrays <= cuda_temporaries:
            result.append((buf_name,))
    return result


def find_bufs_movable_to_stack_cuda(root_scope, max_size=1024):
    return find_bufs_movable_to_memory_cuda(root_scope, max_size, "stack")


def find_bufs_movable_to_cache_cuda(root_scope, max_size=64*1024):
    return find_bufs_movable_to_memory_cuda(root_scope, max_size, "cache")


def move_buf_to_cache(root_scope, buf_name):
    root_scope.concepts.add("data_locality")
    root_scope.internal_bufs[buf_name].allocation = "cache"


def gen_cuda_allocs(ctx, scope):
    inp, tmp, out = scope.get_inp_tmp_out()
    fragment_allocs = {}
    for arr_name in tmp:
        buf_name = ctx.root.arr_to_buf[arr_name]
        buf = ctx.root.buffers[buf_name]
        if buf.allocation.startswith("fragment"):
            fragment_allocs[buf_name] = buf
    code = []

    code.extend(gen_stack_allocs(ctx, scope))

    # cache allocs
    if ctx.target == "cuda":
        code.append("extern __shared__ i32 cache_ptr[];")  # data type is irrelevant, we cast to appropriate anyway
        for buf_name, buf in sorted(ctx.root.buffers.items()):
            if buf.allocation != "cache":
                continue
            offset = ctx.alloc_info.cache_offsets[buf_name]
            code.append(f"{buf.dtype}* {buf_name} = ({buf.dtype}*) ((u8*)cache_ptr + {offset});  // size {buf.size_elems()}")

    if fragment_allocs:
        if ctx.target != "cuda":
            raise NotImplementedError("Fragment allocations are only implemented in CUDA")
        for buf_name, buf in sorted(fragment_allocs.items()):
            frag_key, frag_type, frag_m, frag_n, frag_k, layout = buf.allocation.split('_')  # fragment_a_16_16_16_row
            
            dims_access = [
                canonicalize_access(dim).gen_access(ctx)
                for dim, used in zip(buf.dims, buf.used_dims)
                if used
            ]
            # last two dimensions related to the fragment size, they are ignored
            assert len(dims_access) >= 2
            frag_size = dims_access[:-2]
            arr_size = " * ".join(frag_size) if frag_size else "1"
            if frag_type in ('a', 'b'):
                code.append(f"wmma::fragment<wmma::matrix_{frag_type}, {frag_m}, {frag_n}, {frag_k}, {buf.dtype}, wmma::{layout}_major> {buf_name}[{arr_size}];")
            else:
                code.append(f"wmma::fragment<wmma::accumulator, {frag_m}, {frag_n}, {frag_k}, {buf.dtype}> {buf_name}[{arr_size}];")
    return code


def gen_stack_allocs(ctx, scope):
    # local allocations in the scope
    code = []
    buf_names = ctx.alloc_info.stack_scopes.get(scope, {})
    for buf_name in buf_names:
        buf = ctx.root.buffers[buf_name]
        buf_size = buf.size_elems()
        code.append(f"{buf.dtype} {buf_name}[{buf_size}];")
    return code


def gen_scoped_heap_allocs(ctx, scope):
    # local allocations in the scope
    alloc_code = []
    free_code = []
    buf_names = ctx.alloc_info.heap_scopes.get(scope, {})
    for buf_name in buf_names:
        buf = ctx.root.buffers[buf_name]
        buf_size = buf.size_elems()
        alloc_code.append(f"{buf.dtype}* {buf_name} = ({buf.dtype}*) aligned_alloc(4096, {buf_size} * sizeof({buf.dtype}));")
        free_code.append(f"free({buf_name});")
    return alloc_code, list(reversed(free_code))


def gen_heap_allocs(ctx):
    # global allocations in the root of the kernel
    code = []
    for buf_name, buf_offset in ctx.alloc_info.heap_offsets.items():
        buf = ctx.root.buffers[buf_name]
        buf_size = buf.size_elems()
        code.append(
            f"{buf.dtype}* {buf_name} = ({buf.dtype}*)((u8*)_tmp + {buf_offset});  // size = {buf_size}"
        )
    return code


def gen_microkernel_allocs(root_scope):
    allocs = {k: v for k, v in root_scope.external_bufs.items() if v.allocation == "heap"}
    allocs = dict(sorted(allocs.items()))
    return gen_preallocated_allocs_from_list(root_scope, allocs, "local_")


def gen_preallocated_allocs_from_list(ctx, buffers, name_prefix):
    code = []
    last_alloc = "_tmp"
    last_size = 0
    # make sure to process buffers in the decreasing order of their data type size
    # to guarantee that there are no accesses without at least type size alignment
    # when sizes match, order by name
    sorted_bufers = sorted(buffers.items(), key=lambda x: (-int(x[1].dtype[1:]), x[0]))
    for buf_name, buf in sorted_bufers:
        dims_access = [
            canonicalize_access(dim).gen_access(ctx)
            for dim, used in zip(buf.dims, buf.used_dims)
            if used
        ]
        arr_size = " * ".join([*dims_access, f"sizeof({buf.dtype})"])
        code.append(
            f"{buf.dtype}* {name_prefix}{buf_name} = ({buf.dtype}*)((u8*){last_alloc} + {last_size});  // size = {arr_size}"
        )
        last_alloc = name_prefix + buf_name
        last_size = arr_size
    return code


def get_tmp_buf_size(ctx):
    return ctx.alloc_info.heap_size


def bracket_info(s):
    opened = 0
    is_close_open = False
    for c in s:
        opened += c in '([{'
        is_close_open |= opened < 0
        opened -= c in ')]}'
    return opened, is_close_open and opened == 0


def autoindent(lines, start_indent=0):
    indent = start_indent
    indented_lines = []
    for l in lines:
        change, close_open = bracket_info(l)
        if change < 0:
            indent -= 1
        indented_lines.append((indent - close_open) * 4 * " " + l)
        if change > 0:
            indent += 1
    return indented_lines


class CodeGenContext:
    def __init__(self, root, target, depth=0, alloc_info=None):
        self.root = root
        self.target = target
        self.depth = depth
        self.alloc_info = alloc_info or find_global_allocation_ranges(root)

    def at_depth(self, new_depth):
        return CodeGenContext(self.root, self.target, new_depth, self.alloc_info)


def gencode(ctx):
    # unroll_recursive(root_scope)
    
    # all_decls = {**root_scope.input_decls, **root_scope.loc, **root_scope.output_decls}

    # inputs, tmps, outputs = root_scope.get_inp_tmp_out()
    # inp_decls = {
    #     name: arr
    #     for name, arr in all_decls.items()
    #     if name in inputs and not isinstance(arr, Const)
    # }
    # out_decls = {name: arr for name, arr in all_decls.items() if name in outputs}
    # input_bufs = {arr.buffer_name for arr in inp_decls.values()}
    # unique_out_decls = {
    #     name: arr
    #     for name, arr in out_decls.items()
    #     if arr.buffer_name not in input_bufs
    # }
    signature = make_signature(ctx)

    # check if there are missing inputs
    # for i in inputs:
    #     if i not in inp_decls and not isinstance(all_decls[i], Const):
    #         raise Exception(f"Missing input {i}")

    ssr_used = any("hw_idx" in v for v in ctx.root.options.get("ssr", {}).values())

    scope_body = []
    scope_body += gen_stack_allocs(ctx, ctx.root)
    scope_body += gen_heap_allocs(ctx)
    if ssr_used:
        # if at least one SSR register is in use, enable SSR
        scope_body.append("snrt_ssr_enable();")
    func_decls, scope_content = ctx.root.gen_body(ctx)
    scope_body += scope_content
    if ssr_used:
        scope_body.append("snrt_ssr_disable();")

    func_decls = autoindent(func_decls, start_indent=0)
    scope_body = autoindent(scope_body, start_indent=1)

    code = []
    
    code += func_decls
                
    code += dedent(
        f"""
        void {ctx.root.name}({signature}) {{
        """
    ).split("\n")
    code += scope_body
    code += "}\n"

    return "\n".join(code)


def nested_arr_split(inp_str):
    result = []
    current_level = 0
    current_str = ""

    for c in inp_str:
        if c == ',' and current_level == 0:
            result.append(current_str)
            current_str = ""
        else:
            current_str += c
            if c == '[':
                current_level += 1
            elif c == ']':
                current_level = current_level - 1
                assert current_level >= 0

    result.append(current_str)
    return result


def parse_expr(expr):
    if isinstance(expr, ast.Subscript):
        name = expr.value.id
        assert isinstance(name, str)
        if isinstance(expr.slice, ast.Set):
            # single element m[{1}]
            indices = [parse_expr(expr.slice)]
        elif isinstance(expr.slice, ast.Tuple):
            # multiple elements x[{1}, {0}]
            indices = [parse_expr(idx) for idx in expr.slice.elts]
        else:
            raise ValueError(f"Unexpected expression {ast.dump(expr.slice)}")
        return Array(name, indices)
    elif isinstance(expr, ast.Expr):
        assert isinstance(expr.value, ast.Name)
        return Array(expr.value.id, [])
    elif isinstance(expr, ast.Set):
        [idx] = expr.elts
        pos = idx.value
        assert isinstance(pos, int)
        return Index(pos)
    elif isinstance(expr, ast.BinOp):
        lhs = parse_expr(expr.left)
        rhs = parse_expr(expr.right)
        if isinstance(expr.op, ast.Add):
            return IndexExpr(lhs, "+", rhs)
        elif isinstance(expr.op, ast.Mult):
            return IndexExpr(lhs, "*", rhs)
        elif isinstance(expr.op, ast.Div):
            return IndexExpr(lhs, "/", rhs)
    elif isinstance(expr, ast.Name):
        return Array(expr.id, [])
    elif isinstance(expr, ast.Constant):
        val = expr.value
        if isinstance(val, int):
            return ConstElem(val, dtype="i32")
        elif isinstance(val, float):
            return ConstElem(val, dtype="f64")
    raise ValueError(f"Unexpected expression {ast.dump(expr)}")


class IRParser:
    def __init__(self):
        variable = pp.Word(pp.alphas, pp.alphanums+'_')
        
        real = pp.Optional("-") + pp.Combine(pp.Word(pp.nums) + "." + pp.Word(pp.nums))
        real.addParseAction(lambda toks: ConstElem(float(toks[0]), dtype="f64"))
        
        integer = pp.Optional("-") + pp.Word(pp.nums)
        integer.addParseAction(lambda toks: ConstElem(int(toks[0]), dtype="i32"))
        
        index = pp.Suppress("{") + integer + pp.Suppress("}")
        index.addParseAction(IRParser.parseIndex)
        
        array = pp.Forward()
        array.addParseAction(lambda toks: Array(toks[0], toks[1:]))
        expr = pp.Forward()
        expr.addParseAction(IRParser.parseExpr)
        operand = index | real | integer | array
        factor = operand | (pp.Suppress('(') + expr + pp.Suppress(')'))
        term = factor + pp.ZeroOrMore(pp.oneOf('* / %') + factor)
        term.addParseAction(IRParser.parseExpr)

        expr << (term + pp.ZeroOrMore(pp.oneOf('+ -') + term))
        reused_postfix = pp.Suppress(pp.Optional(":N"))
        array << (variable + pp.Optional(pp.Suppress("[") + pp.delimitedList(expr + reused_postfix) + pp.Suppress("]")))

        self.expr = expr

    @staticmethod
    def parseIndex(toks):
        [idx] = toks
        assert isinstance(idx, ConstElem)
        return Index(idx.value)

    @staticmethod
    def parseExpr(toks):
        if len(toks) == 1:
            assert isinstance(toks[0], (Array, ConstElem, Index, IndexExpr))
            return toks[0]
        elif len(toks) == 3:
            assert isinstance(toks[0], (Array, ConstElem, Index, IndexExpr))
            assert isinstance(toks[1], str)
            assert isinstance(toks[2], (Array, ConstElem, Index, IndexExpr))
            return IndexExpr(toks[0], toks[1], toks[2])
        else:
            new_toks = toks[:]
            while len(new_toks) != 1:
                new_toks = [IRParser.parseExpr(new_toks[:3])] + new_toks[3:]
            return new_toks[0]
    
    _instance = None

    @staticmethod
    def parse_expr(access_str):
        IRParser._instance = IRParser._instance or IRParser()  # lazy initialize
        [res] = IRParser._instance.expr.parseString(access_str, parseAll=True)
        return res


def canonicalize_access(access):
    if isinstance(access, (Array, ConstElem, Index, IndexExpr)):
        return copy.deepcopy(access)
    elif isinstance(access, str):
        return IRParser.parse_expr(access)
    elif isinstance(access, int):
        return ConstElem(access, dtype="i32")
    elif isinstance(access, float):
        return ConstElem(access, dtype="f64")
    else:
        raise ValueError(f"Can not parse access: {access}")


def common_output_type(inputs):
    assert inputs
    highest_float = None
    highest_int = None
    for i in inputs:
        assert i is not None
        if i.startswith("f"):
            if not highest_float or highest_float > int(i[1:]):
                highest_float = int(i[1:])
        if i.startswith("i"):
            if not highest_int or highest_int > int(i[1:]):
                highest_int = int(i[1:])
    if highest_float:
        return f"f{highest_float}"
    assert highest_int
    return f"i{highest_int}"


def infer_output_dims(root_scope, operator):
    scope_dims = get_dims_from_root_to_scope(root_scope, operator)
    indices = operator.out_elem.indices
    out_dims = []
    for idx in indices:
        if not isinstance(idx, Index):
            raise Exception("Not implemented")
        out_dims.append(scope_dims[-idx.pos-1])
    return out_dims

def any_vector_operands_present(root_scope, scope):
    for op in scope.ops:
        if isinstance(op, Operation):
            for elem in op.inp_elem:
                if not isinstance(elem, Array):
                    continue
                elem_buf_name = root_scope.arr_to_buf[elem.name]
                elem_buf = root_scope.buffers[elem_buf_name]
                if elem_buf.dtype.is_vector():
                    return True
            if op.op.out_dtype.is_vector():
                return True
    return False


def find_anything_by_name(root_scope, target_name):
    for idx, op in enumerate(root_scope.ops):
        if op.name == target_name:
            return root_scope, idx
        elif isinstance(op, Scope):
            res = find_anything_by_name(op, target_name)
            if res is not None:
                return res
    return None


def find_scope_by_name(root_scope, target_scope_name):
    """Returns parent scope and index of the target scope in the parent scope"""
    scope, idx = find_anything_by_name(root_scope, target_scope_name)
    assert isinstance(scope.ops[idx], Scope)
    return scope, idx


def find_op_by_name(root_scope, target_op_name):
    """Returns parent scope and index of the target op in the parent scope"""
    scope, idx = find_anything_by_name(root_scope, target_op_name)
    assert isinstance(scope.ops[idx], Operation)
    return scope, idx


def find_elem_by_location(program, op_name, access_idx):
    scope, idx = find_op_by_name(program, op_name)
    op = scope.ops[idx]
    return op.all_elem[access_idx]


def lower_loc(root_scope, loc_name):
    if loc_name in root_scope.loc:
        # find nested scope that uses loc_name as tmp and move it there
        for op in root_scope.ops:
            if not isinstance(op, Scope):
                continue
            inp, tmp, out = op.get_inp_tmp_out()
            if loc_name in tmp:
                # perform the move
                op.loc[loc_name] = root_scope.loc[loc_name]
                root_scope.loc.pop(loc_name)
                return True
        return False
    for op in root_scope.ops:
        if isinstance(op, Scope) and lower_loc(op, loc_name):
            return True
    return False


def elevate_loc(root_scope, loc_name):
    for op in root_scope.ops:
        if not isinstance(op, Scope):
            continue

        if loc_name in op.loc:
            # make sure that dimensions defined in the root scope
            arr = op.loc[loc_name]
            inp, tmp, out = op.get_inp_tmp_out()
            for dim in arr.dims:
                if dim.name not in inp:
                    # array dimensions are locally defined
                    return False
            # perform the move
            root_scope.loc[loc_name] = op.loc[loc_name]
            op.loc.pop(loc_name)
            return True
        elif elevate_loc(op, loc_name):
            return True
    return False

def get_scopes(root_scope):
    """ Traverse the tree recursively and return only scopes """
    scopes = []
    for op in root_scope.ops:
        if isinstance(op, Scope):
            scopes.append(op)
            scopes += get_scopes(op)
    return scopes


def analyze_deletable_dimension_candidates(root_scope, array_name):
    flat_list = get_flattened_ops_with_scopes_textual(root_scope)
    [(write_scopes, write_op)] = [(scopes, op) for scopes, op in flat_list if op.out_elem.name == array_name]
    read_scopes_op = [(scopes, op) for scopes, op in flat_list if any(isinstance(e, Array) and e.name == array_name for e in op.inp_elem)]

    discovered_rtw = None
    discovered_wtr = None
    for write_loc, idx in reversed(list(enumerate(write_op.out_elem.indices))):
        if not isinstance(idx, Index):
            continue
        candidate_write_scope = len(write_scopes) - idx.pos - 1
        candidate_dim = write_scopes[candidate_write_scope].dim
        candidate_rejected = False
        read_scope_indices = []  # for each read op, maintains the index of the found scope
        for scopes, op in read_scopes_op:
            candidate_read_scope = None
            for elem in op.inp_elem:
                if not isinstance(elem, Array) or elem.name != array_name:
                    continue
                for read_loc, read_idx in reversed(list(enumerate(elem.indices))):
                    if idx.pos not in read_idx.accessed_pos():
                        continue
                    if not isinstance(read_idx, Index):
                        candidate_rejected = True
                        break
                    if read_loc != write_loc:
                        candidate_rejected = True  # read and write refer to different array dimensions
                        break
                    current_read_scope = len(scopes) - read_idx.pos - 1
                    if candidate_read_scope is None:
                        candidate_read_scope = current_read_scope
                        read_scope_indices.append(candidate_read_scope)
                    elif candidate_read_scope != current_read_scope:
                        candidate_rejected = True  # multiple reads refer to different scopes
                        break
                    read_dim = scopes[candidate_read_scope].dim
                    if read_dim != candidate_dim:
                        candidate_rejected = True  # read and write dimensions mismatch
                        break
            if candidate_read_scope is None:
                candidate_rejected = True  # no read found

        if candidate_rejected:
            continue
        # finally, check that the candidate dimension adheres
        # to read-to-write or write-to-read case

        # in read-to-write case, all inputs should not access the candidate dimension (bcast)
        if discovered_rtw is None:
            rtw_rejected = False
            for inp in write_op.inp_elem:
                write_scope_pos = len(write_scopes) - candidate_write_scope - 1
                if write_scope_pos in inp.accessed_pos():
                    rtw_rejected = True
            if not rtw_rejected:
                discovered_rtw = [candidate_write_scope] + read_scope_indices

        # in write-to-read case, all outputs (and other inputs) should not access the candidate dimension (reduce)
        if discovered_wtr is None:
            wtr_rejected = False
            for (scopes, op), scope_idx in zip(read_scopes_op, read_scope_indices):
                read_scope_pos = len(scopes) - scope_idx - 1
                for elem in op.all_elem:
                    if isinstance(elem, Array) and elem.name == array_name:
                        continue  # skip the array itself (it needs the dimension accessing 'read_scope_pos' to be deleted)
                    if read_scope_pos in elem.accessed_pos():
                        wtr_rejected = True
            # check that involved operations do not contain any functions except plain assignments
            if write_op.op.func or any(op.op.func for scopes, op in read_scopes_op):
                wtr_rejected = True
            # check that reduction types match across reads
            common_read_reduce = None
            for scopes, op in read_scopes_op:
                if op.op.reduce:
                    if common_read_reduce is None:
                        common_read_reduce = op.op
                    elif common_read_reduce != op.op:
                        wtr_rejected = True
            # check that reduction types match with write
            if not write_op.op.is_move():
                if common_read_reduce != write_op.op:
                    wtr_rejected = True
            # if we get so far, then we can add the candidate
            if not wtr_rejected:
                discovered_wtr = [candidate_write_scope] + read_scope_indices
        
    return discovered_rtw, discovered_wtr


def recursive_ops(scope):
    for op in scope.ops:
        if isinstance(op, Operation):
            yield op
        elif isinstance(op, Scope):
            yield from recursive_ops(op)


def recursive_scopes(scope, skip_condition=lambda x: False):
    for op in scope.ops:
        if isinstance(op, Scope):
            yield op
            if not skip_condition(op):
                yield from recursive_scopes(op, skip_condition)


def recursive_scopes_with_depth(scope, depth):
    yield (depth, scope)
    for op in scope.ops:
        if isinstance(op, Scope):
            yield from recursive_scopes_with_depth(op, depth+1)


def recursive_scopes_with_ops(scope):
    for op in scope.ops:
        if isinstance(op, Operation):
            yield ([], op)
        elif isinstance(op, Scope):
            for scopes, operation in recursive_scopes_with_ops(op):
                yield ([op, *scopes], operation)


def recursive_indexed_scopes_with_ops(scope):
    for idx, op in enumerate(scope.ops):
        if isinstance(op, Operation):
            yield ([idx], [scope], op)
        elif isinstance(op, Scope):
            for indices, scopes, operation in recursive_indexed_scopes_with_ops(op):
                yield ([idx, *indices], [op, *scopes], operation)


def recursive_indexed_scopes_or_ops(root):
    # returns (indices_per_ancestors, ancestors, scope_or_op, is_entrance_if_scope=[None, True, False])
    for idx, op in enumerate(root.ops):
        if isinstance(op, Operation):
            yield ([idx], [root], op, None)
        elif isinstance(op, Scope):
            yield ([idx], [root], op, True)
            for indices, scopes, operation, entrance in recursive_indexed_scopes_or_ops(op):
                yield ([idx, *indices], [root, *scopes], operation, entrance)
            yield ([idx], [root], op, False)


def check_all_scopes_split(root_scope):
    for scope in recursive_scopes(root_scope):
        if len(scope.ops) > 1:
            return False
    return True


def check_no_reused_buffers(root_scope):
    for buf_name in root_scope.buffers:
        array_names = root_scope.buf_to_arr(buf_name)
        if len(array_names) > 1:
            return False
    return True


def check_no_unrolled_scopes(root_scope):
    for scope in recursive_scopes(root_scope):
        if scope.options.get("unroll", False):
            return False
    return True


def generate_new_array_name(root_scope, base_name):
    # generate new name that does not already exist using given name as a base
    new_name_suffix = 0
    while f"{base_name}{new_name_suffix}" in root_scope.arr_to_buf:
        new_name_suffix += 1
    new_arr_name = f"{base_name}{new_name_suffix}"
    return new_arr_name


def find_creatable_local_temporaries(root_scope):
    """ Requires all scopes to be split """
    
    if not check_all_scopes_split(root_scope):
        return []  # only one operation per scope is allowed

    result = []

    for op in recursive_ops(root_scope):
        for idx, elem in enumerate(op.all_elem):
            if isinstance(elem, Array):
                result.append((op.name, idx))

    # no need to sort, order already deterministic
    return result


def create_local_temporary(root_scope, op_name, operand_idx, check=True):
    """ Requires all scopes to be split """
    
    if check:
        assert (op_name, operand_idx) in find_creatable_local_temporaries(root_scope)
    
    parent_op, op_idx = find_op_by_name(root_scope, op_name)
    op = parent_op.ops[op_idx]

    scope_nest = get_scopes_from_root_to_scope(root_scope, parent_op)
    current_scope_pos = 0
    new_indices = []
    new_dims = []

    for scope in scope_nest:
        new_dims.append(copy.deepcopy(scope.dim))
        new_indices.append(Index(current_scope_pos))
        current_scope_pos += 1
    new_indices = list(reversed(new_indices))


    arr = op.all_elem[operand_idx]
    arr_name = arr.name
    new_arr_name = generate_new_array_name(root_scope, arr_name)
    # add temporary buffer
    orig_buf_name = root_scope.arr_to_buf[arr_name]
    orig_buf = root_scope.buffers[orig_buf_name]
    tmp_buf = Buffer(new_dims, orig_buf.dtype)
    new_buf_name = new_arr_name[:]  # for temporary array we create buffer with the same name
    root_scope.arr_to_buf[new_buf_name] = new_arr_name
    root_scope.buffers[new_buf_name] = tmp_buf  
    
    # create operation that will perform elementwise temporary assignment
    new_arr = copy.deepcopy(arr)
    new_arr.indices = new_indices
    new_op = Operation(
        new_arr,
        [copy.deepcopy(arr)],
        OpDesc(tmp_buf.dtype),
    )   
    
    
    # now iteratively wrap scopes into the new operation
    move_scope_nest = new_op
    current_scope_pos = 0
    for scope in reversed(scope_nest):
        move_scope_nest = LoopScope([move_scope_nest], copy.deepcopy(scope.dim))
        current_scope_pos += 1
    
    scope_nest_with_op = scope_nest + [op]  # correctly handle empty scope nests
    
    index_in_root = root_scope.ops.index(scope_nest_with_op[0])

    op.all_elem[operand_idx].name = new_arr_name
    op.all_elem[operand_idx].indices = new_indices
    # different behavior depending on the target operand position (read or write)
    if operand_idx == 0:
        # output operand: create temporary initialization right after the operation
        new_op.inp_elem[0].name = new_arr_name
        root_scope.ops.insert(index_in_root + 1, move_scope_nest)
    else:
        # input operand: create temporary initialization right before the operation
        new_op.out_elem.name = new_arr_name
        root_scope.ops.insert(index_in_root, move_scope_nest)
        
    # propagate used outputs
    root_scope.propagate_used_outputs(root_scope.used_outputs)


def tile_array_accesses(scope, tile_size, access_depth=0):
    # adjusts all array accesses to account for newly introduced tile dimension
    # access_depth equal to the positional index of tiled dimension
    # all accesses < access_depth are not affected
    # all accesses > access_depth are incremented by 1
    # all accesses == access_depth are replaced with an Index expression
    for op in scope.ops:
        if isinstance(op, Operation):
            new_elems = []
            for elem in op.all_elem:
                new_elems.append(elem.tile_index(access_depth, tile_size))
            [op.out_elem, *op.inp_elem] = new_elems
        elif isinstance(op, Scope):
            tile_array_accesses(op, tile_size, access_depth + 1)


def is_tiling_exp(i, tile_size=None):
    if not isinstance(i, IndexExpr):
        return 0
    if not isinstance(i.lhs, IndexExpr):
        return 0
    if not isinstance(i.rhs, Index) and not isinstance(i.rhs, IndexExpr):
        return 0
    if i.lhs.op != "*":
        return 0
    if i.op != "+":
        return 0
    if not isinstance(i.lhs.rhs, ConstElem):
        return 0
    if tile_size is not None:
        if i.lhs.rhs.value != tile_size:
            return 0
    return i.lhs.rhs.value


def check_tiling_propagation(root_scope, scope, buffer_name, dim_idx):
    tile_size = None
    new_tile_size = None
    arr_names = root_scope.buf_to_arr(buffer_name)
    for i, op in enumerate(scope.ops):
        if isinstance(op, LoopScope):
            new_tile_size = check_tiling_propagation(root_scope, op, buffer_name, dim_idx)
        elif isinstance(op, Operation):
            for el in op.all_elem:
                if isinstance(el, Array) and el.indices and (el.name in arr_names):
                    new_tile_size = is_tiling_exp(el.indices[dim_idx], new_tile_size)
        if not tile_size is None and not new_tile_size is None and new_tile_size != tile_size:
            return 0
        if not new_tile_size is None:
            tile_size = new_tile_size
    return tile_size


def propagate_tiling(root_scope, scope, buffer_name, dim_idx):
    arr_names = root_scope.buf_to_arr(buffer_name)
    for i, op in enumerate(scope.ops):
        if isinstance(op, LoopScope) or isinstance(op, Scope):
            propagate_tiling(root_scope, op, buffer_name, dim_idx)
        elif isinstance(op, Operation):
            new_elems = []
            for el in op.all_elem:
                if isinstance(el, Array) and el.indices and (el.name in arr_names) and is_tiling_exp(el.indices[dim_idx]):
                    i1 = el.indices[dim_idx].lhs.lhs
                    i2 = el.indices[dim_idx].rhs
                    el.indices[dim_idx] = i1
                    el.indices.insert(dim_idx + 1, i2)
                    new_elems.append(el)
                else:
                    new_elems.append(el)
            [op.out_elem, *op.inp_elem] = new_elems
            scope.ops[i] = op


def tile_array_dim(orig_array, dim_idx, out_dim, in_dim):
    # return new array that has dimension dim_idx split into two: outer and inner
    tiled_array = Buffer(
        dims=orig_array.dims[:dim_idx] + [out_dim, in_dim] + orig_array.dims[dim_idx + 1:],
        dtype=orig_array.dtype,
    )
    orig_dim_usage = orig_array.used_dims[dim_idx]
    tiled_array.used_dims = orig_array.used_dims[:dim_idx] + [orig_dim_usage] + orig_array.used_dims[dim_idx:]
    return tiled_array


def tile_dim(dim, tile_size):
    if isinstance(dim, ConstElem):
        out_dim = ConstElem(dim.value // tile_size, "i32")
        in_dim = ConstElem(tile_size, "i32")
    else:
        raise NotImplementedError("Symbolic tiling of expressions i*N+j to be implemented.")
        out_dim = IndexExpr(dim, "/", ConstElem(tile_size, "i32"))
        in_dim = ConstElem(tile_size, "i32")
    return out_dim, in_dim


def propagate_untiling(root_scope, scope, buffer_name, dim_idx, dim, access_depth=0):
    arr_names = root_scope.buf_to_arr(buffer_name)
    for i, op in enumerate(scope.ops):
        if isinstance(op, LoopScope) or isinstance(op, Scope):
            propagate_untiling(root_scope, op, buffer_name, dim_idx, dim, access_depth+1)
        elif isinstance(op, Operation):
            new_elems = []
            for el in op.all_elem:
                if isinstance(el, Array) and (el.name in arr_names):
                    tile_size = dim
                    new_indices = copy.deepcopy(el.indices)
                    new_indices[dim_idx] = IndexExpr(IndexExpr(el.indices[dim_idx], '*', tile_size), '+', el.indices[dim_idx+1])
                    new_indices.pop(dim_idx+1)
                    new_elem = Array(el.name, new_indices)
                    new_elem.options = copy.deepcopy(el.options)
                    new_elems.append(new_elem)
                else:
                    new_elems.append(el)
            
            [op.out_elem, *op.inp_elem] = new_elems
            scope.ops[i] = op


def untile_buffer_dim(orig_buffer, dim_idx, new_dim):
    # return new array without the dimension after dim_idx
    untiled_buffer = copy.deepcopy(orig_buffer)
    untiled_buffer.dims = orig_buffer.dims[:dim_idx] + [new_dim] + orig_buffer.dims[dim_idx + 2:]

    # pick smaller dim_order from the inner and outer dimensions
    assert orig_buffer.used_dims[dim_idx] == orig_buffer.used_dims[dim_idx + 1]  # should be checked in find_untilable_buffers
    untiled_buffer.used_dims = orig_buffer.dims[:dim_idx] + orig_buffer.dims[dim_idx + 1:]

    return untiled_buffer


def is_untiling_exp(i, access_depth, tile_size=None):
    """
    Returns the tile size if it is a tiling expression of access_depth and tile_size, 0 otherwise
    """
    if not isinstance(i, IndexExpr):
        return 0
    if not isinstance(i.lhs, IndexExpr) or not isinstance(i.lhs.lhs, Index) or i.lhs.lhs.pos != access_depth:
        return 0
    if not isinstance(i.rhs, Index) or i.rhs.pos != access_depth-1:
        return 0
    if i.lhs.op != "*":
        return 0
    if i.op != "+":
        return 0
    if not isinstance(i.lhs.rhs, ConstElem):
        return 0
    if tile_size is not None:
        if i.lhs.rhs.value != tile_size:
            return 0
    return i.lhs.rhs.value


def untile_array_accesses(scope, access_depth):
    for ii, op in enumerate(scope.ops):
        if isinstance(op, Operation):
            [op.out_elem, *op.inp_elem] = [el.untile_index(access_depth) for el in op.all_elem]
            scope.ops[ii] = op
        elif isinstance(op, Scope):
            untile_array_accesses(op, access_depth+1)


def is_index_in(index, pos):
    if isinstance(index, Index):
        return index.pos == pos
    elif isinstance(index, IndexExpr):
        return is_index_in(index.rhs, pos) or is_index_in(index.lhs, pos)
    else:
        return False

def search_untiling_exp(index, access_depth, tile_size=None):
    """
    Check that, if an untiled index appears, it will appear in a untiling expression with always the same tile size
    If tile_size is not None, all tile sizes should be equal to it
    Returns 0 if these properties are not respected, None if there is no untiled index or the value of the tile size if it was found
    """
    if is_index_in(index, access_depth) or is_index_in(index, access_depth-1):
        new_tile_size = is_untiling_exp(index, access_depth)
        if new_tile_size:
            if tile_size is not None and tile_size != new_tile_size:
                return 0
            else:
                return new_tile_size
        elif isinstance(index, IndexExpr):
            #check all the tiles in the same index expression are the same
            tile_l = search_untiling_exp(index.lhs, access_depth, tile_size)
            tile_r = search_untiling_exp(index.rhs, access_depth, tile_size)
            if tile_l is None:
                return tile_r
            elif tile_r is None:
                return tile_l
            elif tile_l == tile_r:
                return tile_r
            else:
                return 0
        #isn't a untiling expression nor an index expression
        else:
            return 0
    #doesn't contain the untiled indices
    else:
        return None


def check_untiled_operation(distance_to_outer, op, tile_size):
    """
    Check that all indices in the operation have the same tile size
    If tile_size is not None, all tile sizes should be equal to it
    Returns 0 if these properties are not respected, None if no tile_size is found or the value of tle_size if it was found
    """
    if tile_size == 0:
        return 0
    for el in op.all_elem:
        tile_size = el.find_tile_expr(distance_to_outer, tile_size)
    return tile_size


def check_untiling_propagation(distance_to_outer, scope, tile_size):
    """
    Check that all indices in the scope have the same tile size
    If tile_size is not None, all tile sizes should be equal to it
    Returns 0 if these properties are not respected, None if no tile_size is found or the value of tle_size if it was found
    """
    if tile_size == 0:
        return 0
    for op in scope.ops:
        if isinstance(op, Operation):
            tile_size = check_untiled_operation(distance_to_outer, op, tile_size)
        elif isinstance(op, Scope):
            tile_size = check_untiling_propagation(distance_to_outer + 1, op, tile_size)
        else:
            raise ValueError(f"Unexpected operation {op}")

    return tile_size


def find_scope_with_loc(program, loc_name):
    for op in program.ops:
        if isinstance(op, Scope):
            if loc_name in op.loc:
                return op
            else:
                res = find_scope_with_loc(op, loc_name)
                if res:
                    return res
    return None


def get_scopes_from_root_to_scope(root, scope, with_root=False, with_scope=True):
    """
    Returns list of scopes from root to scope (including 'scope' but not including 'root')
    root may be any scope (including non-root) and scope may be operation.
    """
    if root == scope:
        return [root] if (with_root or with_scope) else []
    head = [root] if with_root else []
    for s in root.ops:
        if s == scope:
            tail = [s] if with_scope else []
            return head + tail
        elif isinstance(s, Scope):
            tail = get_scopes_from_root_to_scope(s, scope, with_root=True, with_scope=with_scope)
            if tail is not None:
                return head + tail
    return None


def get_dims_from_root_to_scope(root, scope):
    scopes = get_scopes_from_root_to_scope(root, scope)
    if not scopes:
        return None
    dims = []
    for s in scopes:
        if isinstance(s, Scope):
            dims.append(s.dim)
    return dims


def get_accessing_operators(scope, arr_name):
    res = []
    for op in scope.ops:
        if isinstance(op, Operation):
            all_arr_names = [x.name for x in op.all_elem if isinstance(x, Array)]
            if arr_name in all_arr_names:
                res.append(op)
        elif isinstance(op, Scope):
            res.extend(get_accessing_operators(op, arr_name))
        else:
            assert 0
    return res


def get_flattened_ops(scope):
    """Returns flat list of operations [op1, op2, ...]"""
    ops_with_scopes = get_flattened_ops_with_scopes(scope)
    return [op for op, _ in ops_with_scopes]


def get_flattened_ops_with_dims(scope):
    """Returns the list with each element being a tuple (op, [dim1, dim2, ...])"""
    ops_with_scopes = get_flattened_ops_with_scopes(scope)
    return [(op, [s.dim for s in scopes]) for op, scopes in ops_with_scopes]    


def get_flattened_ops_with_scopes(scope):
    """
    Returns the list with each element being a tuple (op, [scope1, scope2, ...])
    outermost scope is the last, innermost scope is the first element in the list
    """
    result = []
    for op in scope.ops:
        if isinstance(op, Operation):
            result.append((op, []))
        elif isinstance(op, Scope):
            nested_res = get_flattened_ops_with_scopes(op)
            nested_res = [
                (nested_op, [*scopes, op]) for nested_op, scopes in nested_res
            ]
            result.extend(nested_res)
        else:
            assert 0
    return result


def get_flattened_ops_with_scopes_textual(scope):
    """
    Reverses the order of op and scopes returned by get_flattened_ops_with_scopes
    to make it appear in their order in the textual notaiton:
    [ ([scope_outer, ..., scope_inner], op), ...]
    """
    return [(list(reversed(scopes)), op) for op, scopes in get_flattened_ops_with_scopes(scope)]


def get_accesses_with_dims(scope):
    """
        Returns flast list of all accesses in the scope together with
        the dimensions of nested loops that contain such access:
        [(Array, [dim1, dim2, ...], is_write)]
    """
    flat_ops = get_flattened_ops_with_dims(scope)
    result = []
    for op, dims in flat_ops:
        op_accesses = []
        for elem_pos, elem in enumerate(op.all_elem):
            if not isinstance(elem, Array):
                continue
            is_write = elem_pos == 0
            # find loop indices used in the elem and construct list of used dimensions in them
            used_indices = sorted(elem.accessed_pos())
            access_dims = []
            for idx in used_indices:
                access_dims.append(dims[idx])
            op_accesses.append((elem, access_dims, is_write))
        result.append(op_accesses)
    return result


def get_accesses_with_scopes(scope):
    """
        Returns flast list of all accesses in the scope together with
        the scopes of nested loops that contain such access:
        [
            [(Array, [scope1, scope2, ...], is_write)] # list of accesses in the operation
        ]
    """
    flat_ops = get_flattened_ops_with_scopes(scope)
    result = []
    for op, scopes in flat_ops:
        op_accesses = []
        for elem_pos, elem in enumerate(op.all_elem):
            if not isinstance(elem, Array):
                continue
            is_write = elem_pos == 0
            op_accesses.append((elem, scopes, is_write))
        result.append(op_accesses)
    return result


def get_flat_scope_range(flat_accesses_with_scopes, scope):
    """
        Returns first and last indices of occurence of scope in the flat_accesses_with_scopes list.
    """
    first_idx = None
    last_idx = None
    for idx, op_entry in enumerate(flat_accesses_with_scopes):
        for array_elem, scopes, is_write in op_entry:
            for target in scopes:
                if target == scope:
                    if first_idx is None:
                        first_idx = idx
                    last_idx = idx
    return first_idx, last_idx


def find_reused_scope_pos(root_scope, op):
    arr = op.out_elem
    arr_name = arr.name
    buf_name = root_scope.arr_to_buf[arr_name]
    buf = root_scope.buffers[buf_name]
    reused_scope_pos = set()
    used_scope_pos = set()
    for arr_idx, dim_used in zip(arr.indices, buf.used_dims):
        if dim_used:
            used_scope_pos = used_scope_pos | arr_idx.accessed_pos()
        else:
            reused_scope_pos = reused_scope_pos | arr_idx.accessed_pos()
    # make sure to not return scope indices that are actually used
    result_reused_scope_pos = reused_scope_pos - used_scope_pos
    return result_reused_scope_pos


def find_array_liveness_ranges(root_scope):
    allocation_scopes = {}

    root_inp, root_tmp, root_out = root_scope.get_inp_tmp_out()

    for scopes, op in recursive_scopes_with_ops(root_scope):
        arr = op.out_elem
        arr_name = arr.name
        buf_name = root_scope.arr_to_buf[arr_name]
        buf = root_scope.buffers[buf_name]
        related_arr_names = root_scope.buf_to_arr(buf_name)
        if related_arr_names & (root_inp | root_out):
            continue  # input and output arrays are not subject to allocation
        # find first scope that is not reused (if it is not present then it is reduced over and therefore in use)
        # for this, we identify all reused scopes
        reused_scope_pos = find_reused_scope_pos(root_scope, op)
        # next, we find innermost scope which is still reused
        # allocation and deallocation should happen inside it
        innermost_reused_scope = root_scope
        for scope_idx, scope in enumerate(scopes):
            scope_pos = len(scopes) - scope_idx - 1
            if scope_pos in reused_scope_pos:
                innermost_reused_scope = scope
            else:
                break
        allocation_scopes[arr_name] = innermost_reused_scope

    # now we look inside each allocation to find the first and last occurence of the array reference
    # this will define the array liveness range
    alloc_indices = {}
    free_indices = {}
    for arr_name, alloc_scope in allocation_scopes.items():
        alloc_idx = None
        free_idx = None
        for idx, op_or_scope in enumerate(alloc_scope.ops):
            inp, tmp, out = op_or_scope.get_inp_tmp_out()
            if arr_name in tmp:
                alloc_idx = idx
                free_idx = idx
            if arr_name in out:
                alloc_idx = idx
            if arr_name in inp:
                free_idx = idx  # last occurence will make the last update as intended
        assert alloc_idx is not None and free_idx is not None
        alloc_indices[arr_name] = alloc_idx
        free_indices[arr_name] = free_idx

    liveness_ranges = {}
    for arr_name, alloc_scope in allocation_scopes.items():
        liveness_ranges[arr_name] = (alloc_scope, alloc_indices[arr_name], free_indices[arr_name])
    return liveness_ranges


def find_scope_nest_lca(root_scope, scope1, scope2):
    # determine LCA of the scopes (using simple inefficient method)
    scopes1 = get_scopes_from_root_to_scope(root_scope, scope1)
    scopes2 = get_scopes_from_root_to_scope(root_scope, scope2)
    common = root_scope
    for s in scopes1:
        if s in scopes2:
            common = s
    return common


def find_buffer_liveness_ranges(root_scope):
    arr_liveness_ranges = find_array_liveness_ranges(root_scope)
    # here we need to merge array liveness ranges that share the same buffer
    buf_liveness_ranges = {}
    for arr_name, (alloc_scope, alloc_idx, free_idx) in arr_liveness_ranges.items():
        buf_name = root_scope.arr_to_buf[arr_name]
        if buf_name not in buf_liveness_ranges:
            buf_liveness_ranges[buf_name] = (alloc_scope, alloc_idx, free_idx)
        else:
            prev_scope, prev_alloc_idx, prev_free_idx = buf_liveness_ranges[buf_name]
            if prev_scope == alloc_scope:
                # simply merge the ranges
                buf_liveness_ranges[buf_name] = (alloc_scope, min(prev_alloc_idx, alloc_idx), max(prev_free_idx, free_idx))
            else:
                common_scope = find_scope_nest_lca(root_scope, prev_scope, alloc_scope)
                # build the range by checking first write and last read relative to the common scope
                alloc_idx = None
                free_idx = None
                for idx, op_or_scope in enumerate(common_scope.ops):
                    inp, tmp, out = op_or_scope.get_inp_tmp_out()
                    inp_buf = [root_scope.arr_to_buf[x] for x in (inp | tmp)]
                    out_buf = [root_scope.arr_to_buf[x] for x in (out | tmp)]
                    if buf_name in out_buf:
                        alloc_idx = idx
                    if buf_name in inp_buf:
                        free_idx = idx  # last occurence will make the last update as intended
                assert alloc_idx is not None and free_idx is not None
                buf_liveness_ranges[buf_name] = (common_scope, alloc_idx, free_idx)
    return buf_liveness_ranges


ALLOCATION_TYPES = ["heap", "cache", "stack", "fragment"]


class AllocationInfo:
    def __init__(self, heap_offsets, heap_size, stack_scopes, cache_offsets, cache_sizes, heap_scopes):
        self.heap_offsets = heap_offsets
        self.heap_size = heap_size
        self.stack_scopes = stack_scopes

        self.cache_offsets = cache_offsets  # {buf_name: cache_offset}
        self.cache_sizes = cache_sizes  # {scope: cache_size}
        
        self.heap_scopes = heap_scopes


def find_cache_allocation_ranges(root_scope, buf_liveness_ranges):
    cache_buf_liveness_ranges = {k: v for k, v in buf_liveness_ranges.items() if root_scope.buffers[k].allocation == "cache"}

    # build mapping:
    # ranges_per_scope = {scope: (starts, ends)}
    # starts = {idx_in_scope: {buf_names}}
    # ends = {idx_in_scope: {buf_name}}
    ranges_per_scope = {}
    for buf_name, (alloc_scope, alloc_idx, free_idx) in cache_buf_liveness_ranges.items():
        scope_starts, scope_ends = ranges_per_scope.get(alloc_scope, ({}, {}))
        scope_starts.setdefault(alloc_idx, set()).add(buf_name)
        scope_ends.setdefault(free_idx, set()).add(buf_name)
        ranges_per_scope[alloc_scope] = (scope_starts, scope_ends)

    # data structure to keep currently allocated buffers
    # list sorted by start_byte [(buf_name, start_byte, end_byte), (buf_name, start_byte, end_byte), ...]
    allocation_state = []

    # now, we recursively process each scope and emulate allocation and deallocation of buffers
    # in the process, we fill cache_offsets and cache_sizes
    # WARNING: currently, alignment is not considered
    cache_offsets = {}
    cache_sizes = {}

    for indices, ancestors, op_or_scope, entrance in recursive_indexed_scopes_or_ops(root_scope):
        is_scope = isinstance(op_or_scope, Scope)
        all_scopes = ancestors + ([op_or_scope] if is_scope else [])
        grid_scopes = [s for s in all_scopes if s.options.get("parallel_level") == "cuda_grid"]
        if not grid_scopes:
            continue
        cuda_kernel_scope, *_ = grid_scopes
        parent = ancestors[-1]
        idx_in_parent = indices[-1]
        scope_starts, scope_ends = ranges_per_scope.get(parent, ({}, {}))
        if (not is_scope) or (is_scope and entrance):
            for buf_name in scope_starts.get(idx_in_parent, set()):
                # allocate buffer
                buf = root_scope.buffers[buf_name]
                buf_bytes = buf.size_bytes()
                # find spot that fits the buffer
                offset = None
                for i, (b1, s1, e1), (b2, s2, e2) in enumerate(zip(allocation_state[:-1], allocation_state[1:])):
                    if s2 - e1 >= buf_bytes:
                        # spot found, insert buffer here
                        allocation_state.insert(i+1, (buf_name, e1, e1 + buf_bytes))
                        offset = e1
                        break
                if offset is None:
                    # spot not found, insert at the end
                    if allocation_state:
                        _, _, offset = allocation_state[-1]
                    else:
                        offset = 0
                    allocation_state.append((buf_name, offset, offset + buf_bytes))
                cache_offsets[buf_name] = offset
                cache_sizes[cuda_kernel_scope] = max(cache_sizes.get(cuda_kernel_scope, 0), offset + buf_bytes)
        if (not is_scope) or (is_scope and not entrance):
            for buf_name in scope_ends.get(idx_in_parent, set()):
                deallocated = False
                for i, (b, s, e) in enumerate(allocation_state):
                    if b == buf_name:
                        allocation_state.pop(i)
                        deallocated = True
                        break
                assert deallocated

    return cache_offsets, cache_sizes


def find_global_allocation_ranges(root_scope):
    buf_liveness_ranges = find_buffer_liveness_ranges(root_scope)

    # Using liveness ranges per buffer, determine global buffer allocations and deallocations that need to happen.

    heap_offsets = {}  # {buf_name: heap_offset}
    heap_size = 0

    # handle heap allocations
    # usually happen in the root scope to achieve better reuse
    # exception is when parallelism is involved, then it is allocated in the appropriate parallel scope
    heap_scopes = {}  # {alloc_scope: {buf_name}}
    bufs_sorted_by_dtype = sorted(buf_liveness_ranges.keys(), key=lambda x: -root_scope.buffers[x].dtype.size)
    for buf_name in bufs_sorted_by_dtype:
        alloc_scope, alloc_idx, free_idx = buf_liveness_ranges[buf_name]
        buf = root_scope.buffers[buf_name]
        if buf.allocation != "heap":
            continue
        # if allocation scope is inside parallel scope, allocation should be done inside the parallel scope
        root_to_alloc = get_scopes_from_root_to_scope(root_scope, alloc_scope)
        if root_to_alloc is not None:
            alloc_scope = None
            for scope in root_to_alloc:
                if scope.options.get("parallel", False):
                    alloc_scope = scope
                    break
            if alloc_scope is not None:
                heap_scopes.setdefault(alloc_scope, set()).add(buf_name)
                continue
        buf_bytes = buf.size_bytes()
        heap_offsets[buf_name] = heap_size
        heap_size = buf_bytes + heap_size

    # handle stack allocations (always happen as deep as possible, ending up in respective alloc_scope)
    stack_scopes = {}  # {alloc_scope: {buf_name}}
    for buf_name, (alloc_scope, alloc_idx, free_idx) in buf_liveness_ranges.items():
        buf = root_scope.buffers[buf_name]
        if buf.allocation != "stack":
            continue
        stack_scopes.setdefault(alloc_scope, set()).add(buf_name)

    # handle cache allocations (currently, CUDA-specific)
    # they happen in the kernel scope
    cache_offsets, cache_sizes = find_cache_allocation_ranges(root_scope, buf_liveness_ranges)

    # TODO: handle fragment allocations here (CUDA-specific)

    return AllocationInfo(
        heap_offsets=heap_offsets,
        heap_size=heap_size,
        stack_scopes=stack_scopes,
        cache_offsets=cache_offsets,
        cache_sizes=cache_sizes,
        heap_scopes=heap_scopes,
    )


def find_live_ranges(root_scope):
    """
        Returns map: {buf_name: (first_write, last_read)}
        first_write and last_read are indices in the flat list of operations
    """
    accesses_with_scopes = get_accesses_with_scopes(root_scope)

    root_inp, root_tmp, root_out = root_scope.get_inp_tmp_out()

    buf_aliveness = {}
    # for each buffer, find pair of indices where they are used

    # first asign aliveness to buffers required as inputs or outputs to the root scope
    before_first_op = - 1
    after_last_op = len(accesses_with_scopes)
    for arr in root_inp:
        buf_name = root_scope.arr_to_buf[arr]
        last_read = buf_aliveness.get(buf_name, (None, None))[1]
        if arr not in root_scope.reusable_inputs:
            last_read = after_last_op
        buf_aliveness[buf_name] = (before_first_op, last_read)
    for arr in root_out:
        buf_name = root_scope.arr_to_buf[arr]
        first_write = buf_aliveness.get(buf_name, (None, None))[0]
        buf_aliveness[buf_name] = (first_write, after_last_op)

    for op_idx, elem_accesses in enumerate(accesses_with_scopes):
        for elem, access_scopes, is_write in elem_accesses:
            arr_name = elem.name
            buf_name = root_scope.arr_to_buf[arr_name]
            # find indices (reuse_indices) that are present in the scope nest but not in the access
            # example:
            #  N1 N2 N3 N4 a[n1, n3] = ... # here reuse indices are {n2, n4}
            reuse_indices = set(range(len(access_scopes)))  # start by assuming all indices are reused
            reuse_indices -= elem.accessed_pos()
            if reuse_indices:
                # now, find outermost reused index (i.e. n2 in the example above)
                outermost_reuse_index = max(reuse_indices)
                # access scopes contains scopes from inner to outer, to the first refers to innermost leaf scope
                # outermost_reuse_index refers to the innermost leaf scope (that wraps only operations) with value 0
                outermost_reuse_scope = access_scopes[outermost_reuse_index]
                # find first and last occurence of the outermost_reuse_scope
                range_first, range_last = get_flat_scope_range(accesses_with_scopes, outermost_reuse_scope)
            else:
                # all indices are in use, so we limit use range to the current operation
                range_first = op_idx
                range_last = op_idx
            default_range = (range_first, range_last)
            first_write, last_read = buf_aliveness.get(buf_name, default_range)
            if is_write:
                if first_write is None or first_write > range_first:
                    first_write = range_first
            else:
                if last_read is None or last_read < range_last:
                    last_read = range_last
            buf_aliveness[buf_name] = (first_write, last_read)

    return buf_aliveness


def swap_array_accesses(scope, access_depth=0):
    # this is a part of swap_nested_scopes transformation
    # it adjusts array accesses to refer to the swapped scopes
    # access_depth refers to the depth of the innermost dimension relative to the input scope
    # so inside the scope with access_depth == k, {k} is swapped with {k+1}
    for op in scope.ops:
        if isinstance(op, Operation):
            new_elems = []
            for inp in op.all_elem:
                new_elems.append(inp.swap_index(access_depth))
            [op.out_elem, *op.inp_elem] = new_elems
        elif isinstance(op, Scope):
            swap_array_accesses(op, access_depth + 1)
    

def shift_scope_to_pos(program, scope, new_position_idx):
    """Repeatedly swaps consecutive nested scopes until the scope parameter reaches the new position index"""
    
    [name, idx] = scope.split('#')
    idx = int(idx)
    if new_position_idx < idx:
        for i in range(idx, new_position_idx, -1):
            swap_nested_scopes(program, f"{name}#{i}")
    else:
        for i in range(idx + 1, new_position_idx + 1):
            swap_nested_scopes(program, f"{name}#{i}")


def get_unrolled_body(ctx, scope):
    num_iters = scope.dim.gen_access(ctx)
    unrolled_ops = []
    for i in range(int(num_iters)):
        for op in scope.ops:
            if not isinstance(op, Operation):
                raise ValueError("Unroll scope can only contain operations")
            new_op = copy.deepcopy(op)
            new_elems = []
            for elem in new_op.all_elem:
                new_elem = elem.replace_index(ConstElem(i, dtype='i32'), 0)
                new_elem = new_elem.shift_index(-1)
                new_elems.append(new_elem)
            [new_op.out_elem, *new_op.inp_elem] = new_elems
            unrolled_ops.append(new_op)
    return unrolled_ops
    
     
def perform_unroll(program, parent, idx):
    """
        Performs actual unrolling of the scope.
        This breaks single assignment property as it introduces multiple assignments to the same array.
        Therefore after this is applied, no further transformations are allowed
    """
    scope = parent.ops[idx]
    scope_depth = get_scopes_from_root_to_scope(program, scope)
    num_iters = scope.dim.gen_access(program, scope_depth)
    unrolled_ops = []
    for i in range(int(num_iters)):
        for op in scope.ops:
            if not isinstance(op, Operation):
                assert ValueError("Unroll scope can only contain operations")
            new_op = copy.deepcopy(op)
            for elem in new_op.all_elem:
                for ei_idx, ei in enumerate(elem.indices):
                    if ei.pos > 0:
                        ei.pos -= 1
                    else:
                        elem.indices[ei_idx] = ConstElem(i, 'i32')
            unrolled_ops.append(new_op)
    return unrolled_ops


def unroll_recursive(program, scope=None):
    if scope is None:
        scope = program
    unrolled_ops = []
    for idx, op in enumerate(scope.ops):
        if isinstance(op, Scope):
            if op.options.get("unroll", False):
                unrolled_ops.extend(perform_unroll(program, scope, idx))
            else:
                unroll_recursive(program, op)
                unrolled_ops.append(op)
        elif isinstance(op, Operation):
            unrolled_ops.append(op)
    scope.ops = unrolled_ops


def find_scopes_with_dim_reuse(program):
    buf_liveness = find_buffer_liveness_ranges(program)
    scopes_with_dim_reuse = set()
    for scopes, op in recursive_scopes_with_ops(program):
        for elem in op.all_elem:
            if not isinstance(elem, Array):
                continue
            buf_name = program.arr_to_buf[elem.name]
            buf = program.buffers[buf_name]
            alloc_scope, first_op_idx, last_op_idx = buf_liveness.get(buf_name, (program, 0, len(program.ops) - 1))
            for used, idx in zip(buf.used_dims, elem.indices):
                if used:
                    continue
                for pos in idx.accessed_pos():
                    scope_candidate = scopes[-pos-1]
                    # here we handle special case: when buffer allocated inside the candidate scope,
                    # the candidate scope is not considered to be reused
                    if buf.allocation == "stack" and get_scopes_from_root_to_scope(scope_candidate, alloc_scope) is not None:
                        continue
                    scopes_with_dim_reuse.add(scope_candidate.name)
    return scopes_with_dim_reuse


def undo_dim_reuse_for_scope(program, scope_name):
    # This method replicates behavior of find_scopes_with_dim_reuse
    buf_liveness = find_buffer_liveness_ranges(program)
    for scopes, op in recursive_scopes_with_ops(program):
        for elem in op.all_elem:
            if not isinstance(elem, Array):
                continue
            buf_name = program.arr_to_buf[elem.name]
            buf = program.buffers[buf_name]
            alloc_scope, first_op_idx, last_op_idx = buf_liveness.get(buf_name, (program, 0, len(program.ops) - 1))
            for dim_idx, (used, idx) in enumerate(zip(buf.used_dims, elem.indices)):
                if used:
                    continue
                for pos in idx.accessed_pos():
                    scope_candidate = scopes[-pos-1]
                    # here we handle special case: when buffer allocated inside the candidate scope,
                    # the candidate scope is not considered to be reused
                    if get_scopes_from_root_to_scope(scope_candidate, alloc_scope) is not None:
                        continue
                    if scope_candidate.name == scope_name:
                        buf.used_dims[dim_idx] = True


def find_scopes_with_reduction(program):
    scopes_with_reduction = set()
    for scopes, op in recursive_scopes_with_ops(program):
        if not op.op.reduce:
            continue
        r_idx, nr_idx = get_reduce_indices(op)
        for idx in r_idx:
            scopes_with_reduction.add(scopes[-idx-1].name)
    return scopes_with_reduction


def find_scopes_in_parallel_nests(program):
    scopes_in_parallel_nests = set()
    for scopes, op in recursive_scopes_with_ops(program):
        if any(s.options.get("parallel", False) for s in scopes):
            scopes_in_parallel_nests |= set(s.name for s in scopes)
    return scopes_in_parallel_nests


def find_parallelizable_cuda_scopes(program):
    """
        The main constraint for parallelization is dimensions reuse.
    """
    scopes_with_dim_reuse = find_scopes_with_dim_reuse(program)
    scopes_with_reduction = find_scopes_with_reduction(program)
    scopes_in_parallel_nests = find_scopes_in_parallel_nests(program)
    
    # now, return all scopes that can be reused
    result = []
    for scope in recursive_scopes(program):
        if scope.options.get("parallel", False):
            continue  # already parallelized
        if scope.options.get("unroll", False):
            continue  # unrolling and parallelization are mutually exclusive
        if scope.name in scopes_with_dim_reuse:
            continue
        if scope.name in scopes_with_reduction:
            continue
        if scope.name in scopes_in_parallel_nests:
            scopes_above = get_scopes_from_root_to_scope(program, scope)
            if len(scopes_above) < 2:
                continue  # no parent loop scope
            parent_scope = get_scopes_from_root_to_scope(program, scope)[-2]
            if not parent_scope.options.get("parallel", False):
                continue
            if len(parent_scope.ops) != 1:
                continue  # parent scope should have exactly one nested scope to extend cuda parallelization
            last_three = get_scopes_from_root_to_scope(program, parent_scope)[-3:]
            if len(last_three) == 3 and all(s.options.get("parallel", False) for s in last_three):
                continue  # no more than 3 dimensions are allowed to be mapped on CUDA kernel call
        result.append((scope.name,))

    return result


def find_cuda_warp_scopes(program):
    if {"parallel_cuda_block", "parallel_cuda_grid"} & program.concepts:
        warnings.warn("Attempting to consider warp parallelism after block or grid parallelism introduced")
        return []
    scopes_with_dim_reuse = find_scopes_with_dim_reuse(program)
    scopes_with_reduction = find_scopes_with_reduction(program)
    result = []
    skip_parallel = lambda s: s.options.get("parallel", False)
    for scope in recursive_scopes(program, skip_parallel):
        if scope.options.get("parallel", False):
            continue  # already parallelized
        if scope.options.get("unroll", False):
            continue  # unrolling and parallelization are mutually exclusive
        if scope.name in scopes_with_dim_reuse:
            continue
        if scope.name in scopes_with_reduction:
            continue  # we can enable it later, requires atomics or warp reduces
        warp_size = 32
        if not isinstance(scope.dim, ConstElem) or scope.dim.value != warp_size:
            continue
        if any(s.options.get("parallel", False) for s in recursive_scopes(scope)):
            continue  # check if there are any nested parallelized scopes, warp should not contain them
        result.append((scope.name,))
    return result


def find_cuda_block_scopes(program):
    if "parallel_cuda_grid" in program.concepts:
        warnings.warn("Attempting to consider block parallelism after grid parallelism introduced")
        return []
    scopes_with_dim_reuse = find_scopes_with_dim_reuse(program)
    scopes_with_reduction = find_scopes_with_reduction(program)
    result = []
    skip_parallel = lambda s: s.options.get("parallel", False)
    for scope in recursive_scopes(program, skip_parallel):
        if scope.options.get("parallel", False):
            continue  # already parallelized
        if scope.options.get("unroll", False):
            continue  # unrolling and parallelization are mutually exclusive
        if scope.name in scopes_with_dim_reuse:
            continue
        if scope.name in scopes_with_reduction:
            continue  # we can enable it later, requires atomics or warp reduces
        if not isinstance(scope.dim, ConstElem):
            continue
        # now, we allow multiple scenarious:
        # - no nested parallelized scopes
        # - all direct children of current scope are warp parallelized
        # - there is only one child and it is block parallelized
        all_direct_warps = all(s.options.get("parallel_level") == "cuda_warp" for s in scope.ops if isinstance(s, LoopScope))
        any_nested_parallelism = any(s.options.get("parallel", False) for s in recursive_scopes(scope))
        single_direct_block = len(scope.ops) == 1 and isinstance(scope.ops[0], LoopScope) and scope.ops[0].options.get("parallel_level") == "cuda_block"
        if all_direct_warps:
            warp_size = 32
            expected_block_size = warp_size * scope.dim.value
        elif single_direct_block:
            cur_scope = scope
            expected_block_size = cur_scope.dim.value
            num_block_scopes = 1
            while isinstance(cur_scope.ops[0], LoopScope) and cur_scope.ops[0].options.get("parallel"):
                cur_scope = cur_scope.ops[0]
                expected_block_size *= cur_scope.dim.value
                if cur_scope.options.get("parallel_level") == "cuda_block":
                    num_block_scopes += 1
            if num_block_scopes > 3:
                continue  # no more than 3 dimensions are allowed to be mapped on CUDA block
        elif not any_nested_parallelism:
            expected_block_size = scope.dim.value
        else:
            continue
        max_cuda_block = 1024
        if expected_block_size > max_cuda_block:
            continue  # above than allowed block size in cuda
        result.append((scope.name,))
    return result


def find_cuda_grid_scopes(program):
    scopes_with_dim_reuse = find_scopes_with_dim_reuse(program)
    scopes_with_reduction = find_scopes_with_reduction(program)
    result = []
    skip_parallel = lambda s: s.options.get("parallel", False)
    for scope in recursive_scopes(program, skip_parallel):
        if scope.options.get("parallel", False):
            continue  # already parallelized
        if scope.options.get("unroll", False):
            continue  # unrolling and parallelization are mutually exclusive
        if scope.name in scopes_with_dim_reuse:
            continue
        if scope.name in scopes_with_reduction:
            continue  # we can enable it later, requires atomics or warp reduces
        if not all(isinstance(s, LoopScope) for s in scope.ops):
            continue
        # now there are two options available
        # - all direct children of current scope has to be block parallelized with block nest having the same shape
        # - there is only one child and it is grid parallelized, with up to 3 levels of parallelism
        if all(s.options.get("parallel_level") == "cuda_block" for s in scope.ops):
            block_nest_shape = "unset"
            for s in scope.ops:
                current_nest_shape = []
                cur_scope = s
                while isinstance(cur_scope, LoopScope) and cur_scope.options.get("parallel_level") == "cuda_block":
                    current_nest_shape.append(cur_scope.dim.value)
                    cur_scope = cur_scope.ops[0]
                if block_nest_shape == "unset":
                    block_nest_shape = current_nest_shape
                elif block_nest_shape != current_nest_shape:
                    block_nest_shape = "mismatch"
                    break
            if block_nest_shape == "mismatch":
                continue
        elif len(scope.ops) == 1 and scope.ops[0].options.get("parallel_level") == "cuda_grid":
            cur_scope = scope.ops[0]
            num_grid_scopes = 1
            while cur_scope.ops[0].options.get("parallel_level") == "cuda_grid":
                cur_scope = cur_scope.ops[0]
                num_grid_scopes += 1
            if num_grid_scopes > 3:
                continue  # no more than 3 dimensions are allowed to be mapped on CUDA grid
        else:
            continue
        result.append((scope.name,))
    return result


def parallelize_cuda_warp_scope(program, scope_name):
    assert (scope_name,) in find_cuda_warp_scopes(program)
    parent_scope, scope_idx = find_scope_by_name(program, scope_name)
    scope = parent_scope.ops[scope_idx]
    scope.options["parallel"] = True
    scope.options["parallel_level"] = "cuda_warp"
    program.concepts.add("parallel_cuda_warp")


def parallelize_cuda_block_scope(program, scope_name):
    assert (scope_name,) in find_cuda_block_scopes(program)
    parent_scope, scope_idx = find_scope_by_name(program, scope_name)
    scope = parent_scope.ops[scope_idx]
    scope.options["parallel"] = True
    scope.options["parallel_level"] = "cuda_block"
    program.concepts.add("parallel_cuda_block")


def parallelize_cuda_grid_scope(program, scope_name):
    assert (scope_name,) in find_cuda_grid_scopes(program)
    parent_scope, scope_idx = find_scope_by_name(program, scope_name)
    scope = parent_scope.ops[scope_idx]
    scope.options["parallel"] = True
    scope.options["parallel_level"] = "cuda_grid"
    program.concepts.add("parallel_cuda_grid")


def find_cuda_scope_nest(program, scope_name):
    """
        Given the scope_name, this function returns the list of scopes that are in the same nest.
        This search is going in two directions: to the root and to the leaves.
    """
    parent_scope, scope_idx = find_scope_by_name(program, scope_name)
    scope = parent_scope.ops[scope_idx]
    if not scope.options.get("parallel", False):
        return []
    scopes_above = get_scopes_from_root_to_scope(program, scope)
    result = [s for s in scopes_above if s.options.get("parallel", False)]
    assert scope.name == result[-1].name
    while len(result[-1].ops) == 1 and isinstance(result[-1].ops[0], LoopScope) and result[-1].ops[0].options.get("parallel", False):
        result.append(result[-1].ops[0])
    assert 1 <= len(result) and len(result) <= 3
    return result


def find_incrementable_parallel_cuda_blocks(program):
    results = []
    for scope in recursive_scopes(program):
        if not scope.options.get("parallel", False):
            continue
        cuda_nest = find_cuda_scope_nest(program, scope.name)
        sizes_after = [s.options.get("parallel_block", 1) + int(s.name == scope.name) for s in cuda_nest]
        if math.prod(sizes_after) > 1024:
            continue  # above than allowed block size in cuda
        results.append((scope.name,))
    return results


def increment_parallel_block_size(program, scope_name):
    assert (scope_name,) in find_incrementable_parallel_cuda_blocks(program)
    parent_scope, scope_idx = find_scope_by_name(program, scope_name)
    scope = parent_scope.ops[scope_idx]
    scope.options["parallel_block"] = scope.options.get("parallel_block", 1) + 1


def find_doublable_parallel_cuda_blocks(program):
    results = []
    for scope in recursive_scopes(program):
        if not scope.options.get("parallel", False):
            continue
        cuda_nest = find_cuda_scope_nest(program, scope.name)
        sizes_after = [s.options.get("parallel_block", 1) * (1 + int(s.name == scope.name)) for s in cuda_nest]
        if math.prod(sizes_after) > 1024:
            continue  # above than allowed block size in cuda
        results.append((scope.name,))
    return results


def double_parallel_block_size(program, scope_name):
    assert (scope_name,) in find_doublable_parallel_cuda_blocks(program)
    parent_scope, scope_idx = find_scope_by_name(program, scope_name)
    scope = parent_scope.ops[scope_idx]
    scope.options["parallel_block"] = scope.options.get("parallel_block", 1) * 2


def is_index_safe(program, op, index):
    """
        Check if index safe to be used in FREP loop.
    """
    for acc in op.all_elem:
        # SSR accesses get quick pass
        if (
            "ssr" in getattr(acc, 'options', {})
            and "hw_idx" in program.options["ssr"][acc.options["ssr"]]
        ):
            continue
        # for plain memory accesses, check that the index of FREP loop is not used
        if isinstance(acc, ConstElem):
            continue
        elif isinstance(acc, Index):
            if acc.pos == index:
                return False
        elif isinstance(acc, Array):
            buf_name = program.arr_to_buf[acc.name]
            buf = program.buffers[buf_name]
            
            for idx, used in zip(acc.indices, buf.used_dims):
                if not used:
                    continue  # no need to check dims that are not materialized
                if index in idx.accessed_pos():
                    return False
        else:
            raise NotImplementedError("Unsupported access type")
    return True


def is_frep_applicable_to_ops(program, op_list):
    max_ops = 15  # HW limitation
    
    if not all(op.op.asm for op in op_list):
        return False  # Operations needs to have assembly implementation
    if any(("sqrt" in op.op.asm) or ("div" in op.op.asm) for op in op_list):
        return False  # FREP doesn't support sqrt and div
    if not all(all(dt.startswith('f') for dt in op.op.asm_dtypes) for op in op_list):
        return False  # FREP doesn't support integer operations

    if len(op_list) > max_ops:
        return False
    for op in op_list:
        if not is_index_safe(program, op, 0):
            return False
        
    return True


def is_frep_applicable_to_scope(program, scope):
    scope_with_ops_only = all(isinstance(op, Operation) for op in scope.ops)    
    if not scope_with_ops_only:
        return False

    if scope.options.get("frep", False):
        return False  # FREP is already enabled
    if scope.options.get("unroll", False):
        return False  # either FREP or unrolling is used

    if not is_frep_applicable_to_ops(program, scope.ops):
        return False

    return True


def ssr_in_scope(scope):
    ssr = set()
    if isinstance(scope, Operation):
        for el in scope.all_elem:
            if isinstance(el, Array) and "ssr" in el.options:
                ssr.add(el.options["ssr"])
    elif isinstance(scope, Scope):
        for op in scope.ops:
            ssr |= ssr_in_scope(op)
    return ssr


# def find_alive_ssr(scope):
#     """Return dict of SSR indices and their relative depth in the scope"""
#     result = {}
#     for op in scope.ops:
#         if isinstance(op, Operation):
#             for elem in op.all_elem:
#                 if "ssr" in elem.options:
#                     result[elem.options["ssr"]["idx"]] = elem.options["ssr"]["depth"]
#         elif isinstance(op, Scope):
#             nested_result = find_alive_ssr(op)
#             for k in nested_result:
#                 if nested_result[k] > 1:
#                     result[k] -= nested_result[k] - 1
#     return result


def has_accesses_to_same_buf(
    program,
    scopes,
    op,
    elem_idx,
    check_later=False,
    check_earlier=False,
    check_reads=False,
    check_writes=False,
):
    flat_ops = []
    for scope in scopes:
        if isinstance(scope, Operation):
            flat_ops.append(scope)
        else:
            ops = get_flattened_ops(scope)
            flat_ops.extend(ops)
    assert op in flat_ops
    elem = op.all_elem[elem_idx]
    if not isinstance(elem, Array):
        return False
    op_idx = None
    for i, op1 in enumerate(flat_ops):
        if op == op1:
            op_idx = i
    assert op_idx is not None
    for op1_idx, op1 in enumerate(flat_ops):
        assert isinstance(op1, Operation)
        for elem1_idx, elem1 in enumerate(op1.all_elem):
            if not isinstance(elem1, Array):
                continue
            if elem1_idx == 0 and not check_writes:
                continue
            if elem1_idx > 0 and not check_reads:
                continue
            if (op1_idx, elem1_idx) == (op_idx, elem_idx):
                continue  # skip self
            if (op1_idx > op_idx) and (not check_later):
                continue  # skip later accesses: subsequent ops
            if (op1_idx == op_idx) and (elem1_idx == 0) and (elem_idx > 0) and (not check_later):
                continue  # skip later accesses: same op, write (idx==0) happens after all reads (idx>0)
            if (op1_idx < op_idx) and (not check_earlier):
                continue  # skip earlier accesses: previous ops
            if (op1_idx == op_idx) and (elem1_idx > 0) and (elem_idx == 0) and (not check_earlier):
                continue  # skip earlier accesses: same op, write (idx==0) happens after all reads (idx>0)
            if program.arr_to_buf[elem1.name] == program.arr_to_buf[elem.name]:
                return True
    return False


def has_raw_or_waw(program, target_scopes, op, elem_idx):
    if elem_idx > 0:
        # this is read, check if there is write before it
        if has_accesses_to_same_buf(
            program,
            target_scopes,
            op,
            elem_idx,
            check_writes=True,
            check_earlier=True,
        ):
            return True
    else:
        # this is write
        # check if there is read after
        if has_accesses_to_same_buf(
            program,
            target_scopes,
            op,
            elem_idx,
            check_reads=True,
            check_later=True,
        ):
            return True
        # check if there is write before or after
        if has_accesses_to_same_buf(
            program,
            target_scopes,
            op,
            elem_idx,
            check_writes=True,
            check_earlier=True,
            check_later=True,
        ):
            return True
    return False


def has_raw_or_waw_ssr(program, ssr_idx, conf, depth):
    """
        Check if there is a read-after-write or write-after-write dependency
        happening with SSR accessed buffers at the specified depth.
    """
    target_scope = get_scope_at_ssr_depth(program, conf, depth)
    op_elem_idx_list = find_elems_with_ssr_idx(program, ssr_idx)
    for op, elem_idx in op_elem_idx_list:
        if has_raw_or_waw(program, [target_scope], op, elem_idx):
            return True
    return False


def has_raw_or_waw_ssr_range(program, ssr_idx1, conf1, ssr_idx2, conf2, depth):
    target_scope1 = get_scope_at_ssr_depth(program, conf1, depth)
    target_scope2 = get_scope_at_ssr_depth(program, conf2, depth)
    common_parent = [program, *get_scopes_from_root_to_scope(program, target_scope1)][-2]
    scope_idx1 = common_parent.ops.index(target_scope1)
    scope_idx2 = common_parent.ops.index(target_scope2)
    scope_idx1, scope_idx2 = sorted((scope_idx1, scope_idx2))
    target_scopes = common_parent.ops[scope_idx1:scope_idx2+1]
    op_elem_idx_list1 = find_elems_with_ssr_idx(program, ssr_idx1)
    op_elem_idx_list2 = find_elems_with_ssr_idx(program, ssr_idx2)
    op_elem_idx_list = op_elem_idx_list1 + op_elem_idx_list2
    for op, elem_idx in op_elem_idx_list:
        if has_raw_or_waw(program, target_scopes, op, elem_idx):
            return True
    return False


def get_scope_at_ssr_depth(program, conf, depth):
    """
        Get scope that contains the SSR at given depth
    """
    target_scope = program
    # depth is given from the operator to scope,
    # here will will iterate from root to this depth
    total_depth = len(conf['first_op'])
    for d in range(total_depth - depth):
        scope_start = conf['first_op'][-d-1]
        target_scope = target_scope.ops[scope_start]
    return target_scope


def find_elems_with_ssr_idx(scope, ssr_idx):
    results = []  # list of tuples (op, access_idx)
    for op in scope.ops:
        if isinstance(op, Operation):
            for elem_idx, elem in enumerate(op.all_elem):
                if not isinstance(elem, Array):
                    continue
                if elem.options.get("ssr") == ssr_idx:
                    results.append((op, elem_idx))
        elif isinstance(op, Scope):
            results.extend(find_elems_with_ssr_idx(op, ssr_idx))
        else:
            assert 0
    return results


debug_flag = [0]


DOCKER = os.environ.get("DOCKER", "podman")
DOCKER_OPTS = os.environ.get("DOCKER_OPTS", "--security-opt label=disable --rm --cap-add=SYS_PTRACE")
DOCKER_IMG = os.environ.get("DOCKER_IMG", "ghcr.io/pulp-platform/snitch@sha256:f43d2db7c97bdcd653deb567664032940e8d9051a80a52c22f239e19fe4c310b")
REPO_ROOT = Path(__file__).parent.parent.resolve()
CWD = Path.cwd()
IMAGE_CMD = f"{DOCKER} run {DOCKER_OPTS} --mount type=bind,source={REPO_ROOT},target=/repo --mount type=bind,source={CWD},target=/work -w /work {DOCKER_IMG}".split()


def run_and_check(cmd, failure="Command failed"):
    result = subprocess.run(
        cmd,
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
        universal_newlines=True,
        shell=True,
    )
    if result.returncode != 0:
        print(result.stdout)
        raise Exception(f"{failure}: {' '.join(cmd)}")
    return result


def compile_snitch_code(sources, binary_name, target):
    target_str = {
        "banshee": "banshee",
        "rtl": "cluster",
    }[target]
    
    toolchain = 'gcc'

    if toolchain == 'llvm':
        # clang toolchain for snitch (deprecated in code generation pipeline due to lack of SSR register protection)
        CC = "/tools/riscv-llvm/bin/clang"
        CFLAGS = ' '.join([
            "-g", "-O3",
            "-mcpu=snitch", "-mcmodel=medany", "-ffast-math",
            "-flto",
            "-fno-builtin-printf", "-fno-common", "-ffunction-sections",
            "-static", "-mllvm", "-enable-misched=false", "-mno-relax",
            "-fopenmp", "-menable-experimental-extensions",
            "-isystem", "/tools/riscv-llvm/riscv32-unknown-elf/include/",
            "-isystem", "/tools/riscv-llvm/lib/clang/12.0.1/include/",
            "-isystem", "/repo/snitch/sw/snRuntime/vendor/",
            "-isystem", "/repo/snitch/sw/snRuntime/include/",
            "-isystem", "/repo/snitch/sw/snRuntime/vendor/riscv-opcodes/",
            "-I", "/repo/microkernels/",  # snrt_mini.h
        ])
        LDFLAGS = ' '.join([
            "-flto",
            "-mcpu=snitch", "-nostartfiles", "-fuse-ld=lld", "-Wl,--image-base=0x80000000",
            "-nostdlib",
            "-static",
            "-Wl,-z,norelro",
            "-Wl,--gc-sections",
            "-Wl,--no-relax",
            "-nodefaultlibs",
            "-T", "/repo/snRuntime-build/common.ld",
            "/tools/riscv-llvm/riscv32-unknown-elf/lib/libc.a",
            "/tools/riscv-llvm/riscv32-unknown-elf/lib/libm.a",
            "/tools/riscv-llvm/lib/clang/12.0.1/lib/libclang_rt.builtins-riscv32.a",
            f"/repo/snRuntime-build/libsnRuntime-{target_str}.a",
        ])
    elif toolchain == 'gcc':
        CC = "/tools/riscv/bin/riscv32-unknown-elf-gcc"
        CFLAGS = ' '.join([
            "-g", "-O3",
            "-flto",
            "-march=rv32imafd", "-mabi=ilp32d", "-mcmodel=medany", "-ffast-math", "-fno-builtin-printf", "-fno-common",
            "-nostdinc",
            "-ffixed-ft0", "-ffixed-ft1", "-ffixed-ft2",
            "-isystem", "/tools/riscv/riscv64-unknown-elf/include/",
            "-isystem", "/tools/riscv/lib/gcc/riscv64-unknown-elf/8.3.0/include/",
            "-isystem", "/repo/snitch/sw/snRuntime/vendor/",
            "-isystem", "/repo/snitch/sw/snRuntime/include/",
            "-isystem", "/repo/snitch/sw/snRuntime/vendor/riscv-opcodes/",
            "-I", "/repo/microkernels/",  # snrt_mini.h
        ])
        LDFLAGS = ' '.join([
            "-flto",
            "-march=rv32imafd", "-mabi=ilp32d", "-nostartfiles", "-Wl,-Ttext-segment=0x80000000",
            "-nodefaultlibs", "-ffunction-sections",
            "-T", "/repo/snRuntime-gcc-build/common.ld",
            f"/repo/snRuntime-gcc-build/libsnRuntime-{target_str}.a",
            "/tools/riscv/riscv64-unknown-elf/lib/rv32imfd/ilp32d/libc.a",
            "/tools/riscv/riscv64-unknown-elf/lib/rv32imfd/ilp32d/libm.a",
            "/tools/riscv/lib/gcc/riscv64-unknown-elf/8.3.0/rv32imfd/ilp32d/libgcc.a",
            "-lc",
            "/repo/snrt_mini.a",
        ])
    else:
        assert 0
    
    # we use clang for objdump (irrespective of build toolchain) as it is able to decode snitch extensions
    OBJDUMP = "/tools/riscv-llvm/bin/llvm-objdump --mcpu=snitch"
    # BUILD_DIR = "/work/build"
    # AR = "/tools/riscv-llvm/bin/llvm-ar"
    # ARFLAGS = "rv"
    # RANLIB = "/tools/riscv-llvm/bin/llvm-ranlib"

    # object files:
    # $(CC) $(CFLAGS) -c -o foo.o foo.c
    # executable:
    # $(CC) $(LDFLAGS) foo.o -o foo
    # executable objdump
    # $(OBJDUMP) -dhS foo > foo.s
    # static library:
    # $(AR) $(ARFLAGS) libfoo.a foo.o

    for src in sources:
        # build object file
        obj_o = src.with_suffix('.o')
        src_cmd = f"{CC} {CFLAGS} -c -o {obj_o} {src}"
        run_and_check(IMAGE_CMD + src_cmd.split())

    objects = [str(src.with_suffix('.o')) for src in sources]
    all_obj_o = ' '.join(objects)

    # link into executable
    # LDFLAGS should come after object files as they may define symbols used in object files
    exe_cmd = f"{CC} {CFLAGS} {all_obj_o} {LDFLAGS} -o {binary_name}"
    cmd = IMAGE_CMD + exe_cmd.split()
    run_and_check(cmd)

    # objdump
    objdump_cmd = f"{OBJDUMP} -dhS {binary_name} > {binary_name}.s"
    cmd = IMAGE_CMD + ['/bin/bash', '-c', objdump_cmd]
    run_and_check(cmd)


def compile_code(sources, binary_name, target):
    CC = features.get_compiler()
    if CC is None:
        raise ValueError("No compiler found")
    NVCC = features.get_nvcc()
    if NVCC is None and target == "cuda":
        raise ValueError("No nvcc found")
    if target == "cuda":
        CC = NVCC
    
    default_cflags = f"-O3"
    if target == "native":
        default_cflags += " -fopenmp -march=native"
    if target == "cuda":
        default_cflags += f" -arch=sm_{features.get_cuda_cap()}"
    CFLAGS = os.getenv("CFLAGS", default_cflags)
    if ("-O3" not in CFLAGS) and ("-O2" not in CFLAGS):
        warnings.warn("Potentially compiling without optimizations")
    
    OBJDUMP = os.getenv("OBJDUMP", "objdump -M intel")
    
    default_ldflags = "-lm -lsleef"
    
    LDFLAGS = os.getenv("LDFLAGS", default_ldflags)
    
    # add mandatory include directory for generic_mini.h
    current_dir = Path(__file__).parent.resolve()
    CFLAGS += f' -I"{current_dir}"'
    # explicitly add library and include directories, i.e. when conda is used
    CFLAGS += f' -I"{features.get_include_dir()}"'
    LDFLAGS += f' -L"{features.get_library_dir()}"'

    for src in sources:
        # build object file
        obj_o = src.with_suffix('.o')
        src_cmd = f"{CC} {CFLAGS} -c -o {obj_o} {src}"
        run_and_check(src_cmd)

    objects = [str(src.with_suffix('.o')) for src in sources]
    all_obj_o = ' '.join(objects)

    # link into executable
    # LDFLAGS should come after object files as they may define symbols used in object files
    exe_cmd = f"{CC} {CFLAGS} {all_obj_o} {LDFLAGS} -o {binary_name}"
    run_and_check(exe_cmd)

    # objdump
    objdump_cmd = f"{OBJDUMP} -dhS {binary_name} > {binary_name}.s"
    run_and_check(objdump_cmd)


def compile_native_code(sources, binary_name):
    CC = os.getenv("CC", "clang")
    CFLAGS = os.getenv("CFLAGS", ' '.join([
        "-O3",
        "-g",
        # "-mstack-alignment=64",
        "-fopenmp",
        "-I ../sleef/include",
        "-L ../sleef/lib", # for sleef do not forget to set the LD_LIBRARY_PATH
        "-march=native",
    ]))  # "-g -mfma -mavx2"
    CFLAGS += f" -I{os.path.abspath(os.path.dirname(__file__))}"  # generic_mini.h
    if CFLAGS.find("-O3") == -1 and CFLAGS.find("-O2") == -1:
        warnings.warn("Potentially compiling without optimizations")
    OBJDUMP = os.getenv("OBJDUMP", "objdump -M intel")
    LDFLAGS = os.getenv("LDFLAGS", "-lsleef -lm")

    for src in sources:
        # build object file
        obj_o = src.with_suffix('.o')
        src_cmd = f"{CC} {CFLAGS} -c -o {obj_o} {src}"
        run_and_check(src_cmd.split())

    objects = [str(src.with_suffix('.o')) for src in sources]
    all_obj_o = ' '.join(objects)

    # link into executable
    # LDFLAGS should come after object files as they may define symbols used in object files
    exe_cmd = f"{CC} {CFLAGS} {all_obj_o} {LDFLAGS} -o {binary_name}"
    cmd = exe_cmd.split()
    run_and_check(cmd)

    # objdump
    objdump_cmd = f"{OBJDUMP} -dhS {binary_name} > {binary_name}.s"
    cmd = ['/bin/bash', '-c', objdump_cmd]
    run_and_check(cmd)
    
    
def compile_cuda_code(sources, binary_name):
    NVCC = os.getenv("NVCC", "nvcc")
    CC = os.getenv("CC", "clang")  # "clang-17"
    # CC = "gcc"
    CFLAGS = os.getenv("CFLAGS", ' '.join([
        "-O3",
    ]))  # "-g -mfma -mavx2"
    CFLAGS += f" -I{os.path.abspath(os.path.dirname(__file__))}"  # generic_mini.h
    # OBJDUMP = os.getenv("OBJDUMP", "objdump -M intel")
    CFLAGS += " -arch=sm_" + features.get_cuda_cap()
    LDFLAGS = os.getenv("LDFLAGS", "")
    nvcc_path = Path(shutil.which(NVCC)).absolute().parent / ".." / "lib64"

    LDFLAGS += f" -L{nvcc_path} -lcudart -lm"

    for src in sources:
        # build object file
        obj_o = src.with_suffix('.o')
        src_cmd = f"{NVCC} {CFLAGS} -c -o {obj_o} {src}"
        run_and_check(src_cmd.split())

    objects = [str(src.with_suffix('.o')) for src in sources]
    all_obj_o = ' '.join(objects)

    # link into executable
    # LDFLAGS should come after object files as they may define symbols used in object files
    exe_cmd = f"{CC} {CFLAGS} {all_obj_o} {LDFLAGS} -o {binary_name}"
    cmd = exe_cmd.split()
    run_and_check(cmd)

    # objdump
    # objdump_cmd = f"{OBJDUMP} -dhS {binary_name} > {binary_name}.s"
    # cmd = ['/bin/bash', '-c', objdump_cmd]
    # run_and_check(cmd)


def test_snitch_code_rtl(program, ref_data, check_correctness=True, reference_code=None):
    return test_snitch_code(program, ref_data, simulator="rtl", check_correctness=check_correctness, reference_code=reference_code)


# simulator either "banshee" or "rtl" (verilator)
def test_snitch_code(program, ref_data, simulator="banshee", check_correctness=True, reference_code=None):
    ctx = CodeGenContext(root=program, target="snitch")
    
    code = gencode(ctx)

    signature = make_signature(ctx)

    buf_size = ctx.alloc_info.heap_size
    
    inp, tmp, out = program.get_inp_tmp_out()

    reused_out_as_inp = {}
    for i in inp:
        for o in out:
            if program.arr_to_buf[i] == program.arr_to_buf[o]:
                reused_out_as_inp[o] = i

    ref_data_init_code = []
    ref_fast_init_code = []
    for k, v in ref_data.items():
        size = v.size
        dtype = NP_TO_GEN_DTYPE[v.dtype.type]
        values = ', '.join(str(s) for s in v.flatten())
        # include non-scalar data in the global memory
        if check_correctness:
            ref_data_init_code.append(f"{dtype} data_{k}[{size}] = {{ {values} }};")
        # make a copy in the fast memory for non-scalar data
        ref_fast_init_code.append(f"{dtype}* {k} = ({dtype}*) snrt_l1alloc({size} * sizeof({dtype}));")
        ref_fast_init_code.append(f'if (!{k}) {{ printf("ERROR: failed to allocate TCDM buffer {k} of size {size} * sizeof({dtype})\\n"); return 1; }}')
        if check_correctness:
            ref_fast_init_code.append(f"for (i32 i = 0; i < {size}; i++) {k}[i] = data_{k}[i];")
        # outputs require separate buffers as we don't want to modify reference data
        if k in out and k not in reused_out_as_inp:
            ref_fast_init_code.append(f"{dtype}* result_{k} = ({dtype}*) snrt_l1alloc({size} * sizeof({dtype}));")
            ref_fast_init_code.append(f'if (!result_{k}) {{ printf("ERROR: failed to allocate TCDM buffer result_{k} of size {size} * sizeof({dtype})\\n"); return 1; }}')
            if check_correctness:
                ref_fast_init_code.append(f"for (i32 i = 0; i < {size}; i++) result_{k}[i] = 0;")
        # when input is reused as output, we just alias such output to input
        # this will do in-place modifications of such input but since input is not needed anymore, it is ok to reuse memory
        if k in reused_out_as_inp:
            # output should point to input
            inp_name = reused_out_as_inp[k]
            ref_fast_init_code.append(f"{dtype}* result_{k} = {inp_name};")

    ref_data_init_code = "\n".join(ref_data_init_code)
    ref_fast_init_code = "\n".join(ref_fast_init_code)

    array_verify_code = []
    for k, v in ref_data.items():
        if k not in out:
            continue
        array_verify_code.append(f"for (int i = 0; i < {v.size}; i++) {{")
        array_verify_code.append(f"    if (fabs(result_{k}[i] - {k}[i]) > 1e-3 && fabs(result_{k}[i] - {k}[i]) / {k}[i] > 1e-3) {{")
        array_verify_code.append(f"        int x1 = (int)(result_{k}[i]);")
        array_verify_code.append(f"        int x2 = ((int)(fabs(result_{k}[i]) * 1000)) % 1000;")
        array_verify_code.append(f"        int y1 = (int)({k}[i]);")
        array_verify_code.append(f"        int y2 = ((int)(fabs({k}[i]) * 1000)) % 1000;")
        array_verify_code.append(f'        printf("Error: mismatch at {k}, %d, %d.%03d (computed) != %d.%03d (expected) \\n", (int)i, x1, x2, y1, y2);')
        array_verify_code.append(f"        ok = 0;")
        array_verify_code.append(f"        break;")
        array_verify_code.append(f"    }}")
        array_verify_code.append(f"}}")
    array_verify_code = "\n".join(array_verify_code)
        
    inp_arrays = [k for k in inp if isinstance(program.buffers[program.arr_to_buf[k]], Buffer)]

    call_args = sorted(inp_arrays) + [f"result_{k}" for k in sorted(out)]
    call_args = ", ".join(call_args)

    main_code = f"""
        #include <snrt.h>
        #include <stddef.h>
        #include <printf.h>
        #include <math.h>
        #include "snrt_mini.h"
        {ref_data_init_code}

        void {program.name}({signature});
        int main() {{
            unsigned tid = snrt_cluster_core_idx();
            if (tid != 0) {{
                return 0;
            }}
            {ref_fast_init_code}
            size_t buf_size = {buf_size};
            void* _tmp = snrt_l1alloc(buf_size);
            if (!_tmp) {{ printf("ERROR: failed to allocate TCDM buffer _tmp\\n"); return 1; }}
            unsigned long t1 = read_csr(mcycle);
            {program.name}({call_args}, _tmp);
            unsigned long t2 = read_csr(mcycle);
            printf("Cycles: %lu\\n", t2 - t1);
            i32 ok = 1;
            {array_verify_code if check_correctness else ""}
            if (ok) {{
                printf("success, exitting...\\n");
                return 0;
            }} else {{
                printf("FAILURE, exitting...\\n");
                return 1;
            }}
        }}
    """
    main_code_lines = [l.strip() for l in main_code.splitlines()]
    main_code_lines_indented = autoindent(main_code_lines)
    main_code = '\n'.join(main_code_lines_indented)

    if simulator not in ("banshee", "rtl"):
        raise ValueError("Unknown simulator")

    # supposed to be relative to cwd to work both in container and outside
    generated = Path(os.environ['SCRATCH'] + '/codegen/' + 'generated_' + str(hashlib.sha1(program.text().encode("utf-8")).hexdigest()))
    reference_kernels = Path("reference_kernels")
    generated.mkdir(parents=True, exist_ok=True)

    main_src = generated / "snitch_main.c"
    code_src = generated / "snitch_code.c"
    bin_src = generated / "snitch_bin"

    with open(main_src, "w") as f:
        f.write(main_code)
    with open(code_src, "w") as f:
        f.write("#include <snrt.h>\n")
        f.write('#include "snrt_mini.h"\n')
        f.write(code)

    if reference_code is None:
        return compile_and_run_snitch_code([code_src, main_src], bin_src, simulator=simulator)
    else:
        return compile_and_run_snitch_code([reference_kernels / reference_code, main_src], bin_src, simulator=simulator)


def compile_and_run_snitch_code(sources, bin_src, simulator="banshee"):
    
    print("Compiling...")
    t1 = time()
    compile_snitch_code(sources, bin_src, target=simulator)
    t2 = time()
    print(f"Compiling... DONE ({t2-t1:.3f} s)")

    if simulator == "banshee":
        run_cmd = f"RUST_MIN_STACK=134217728 SNITCH_LOG= banshee --configuration /repo/snitch/sw/banshee/config/snitch_cluster.yaml --latency {bin_src}"
        # warning: do not split run_cmd as it will break the command
        full_run_cmd = IMAGE_CMD + ['/bin/bash', '-c', run_cmd]
    elif simulator == "rtl":
        NUMACTL_PREFIX = os.getenv("NUMACTL_PREFIX", "") # numactl -m 0 -C 0-15
        run_cmd = f"/repo/snitch_cluster.vlt {bin_src}"
        # option 1: run in container (slight performance overhead, numactl not available)
        #full_run_cmd = IMAGE_CMD + ['/bin/bash', '-c', run_cmd]
        # option 2: run outside of container (potentially may not work on all systems)
        full_run_cmd = f"{NUMACTL_PREFIX} {REPO_ROOT}/snitch_cluster.vlt {bin_src}".split()

    print(f"Running ({simulator})...")
    t1 = time()
    result = subprocess.run(
        full_run_cmd,
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
        universal_newlines=True
    )
    t2 = time()
    print(f"Running ({simulator})... DONE ({t2-t1:.3f} s)")
    
    if result.returncode != 0:
        print(result.stdout)
    
    # process logs
    if simulator == "rtl":
        print(f"Postprocessing logs...")
        logs = glob.glob('logs/trace_hart_*.dasm')
        if not logs:
            raise Exception("Could not find trace logs")
        t1 = time()
        for dasm in logs:
            txt = dasm.replace('dasm', 'txt')
            trace = dasm.replace('dasm', 'trace')
            run_and_check(IMAGE_CMD + ['/bin/bash', '-c'] + [f"spike-dasm < {dasm} > {txt}"])
            run_and_check(IMAGE_CMD + ['/bin/bash', '-c'] + [f"/repo/snitch/util/gen_trace.py < {txt} > {trace}"])
        t2 = time()
        print(f"Postprocessing logs... DONE ({t2-t1:.3f} s)")
    if result.returncode != 0:
        if re.search("failed to allocate TCDM buffer _tmp", result.stdout):
            return 10**9
        raise Exception(f"Simulation failed: {' '.join(full_run_cmd)}")

    # find patter with regex: "Cycles: 1476"
    m = re.search(r"Cycles: (\d+)", result.stdout)
    if m is None:
        raise Exception("Could not find cycle count")
    cycles = int(m.group(1))
    
    return cycles


def set_memory_limit():
    available_memory = psutil.virtual_memory().available
    limit_fraction = 0.9
    soft_limit = 2 ** math.floor(math.log2(limit_fraction * available_memory))
    hard_limit = soft_limit
    resource.setrlimit(resource.RLIMIT_AS, (soft_limit, hard_limit))


def test_code(
    program, ref_data, check_correctness=True, min_reps=3, min_seconds=1,
    rep_number=1, timeout=5, remove_tmp_files=True, silent=True, reinit_input=True,
    target="native",
):
    ctx = CodeGenContext(root=program, target=target)
    
    for arr_name, val in ref_data.items():
        buf_name = program.arr_to_buf[arr_name]
        buf = program.buffers[buf_name]
        if buf.dtype != NP_TO_GEN_DTYPE[val.dtype.type]:
            raise ValueError(f"Supplied ref_data does not match the kernel data type for {arr_name} ({buf.dtype} != {val.dtype.type})")

    code = gencode(ctx)
    code = '#include "generic_mini.h"\n' + code

    # supposed to be relative to cwd to work both in container and outside
    workdir = os.environ.get("SCRATCH", os.getcwd())

    parent_dir = Path(workdir) / 'codegen' / 'generated'
    parent_dir.mkdir(parents=True, exist_ok=True)
    if remove_tmp_files:
        generated = Path(tempfile.mkdtemp(dir=parent_dir))
    else:
        generated = parent_dir / program.program_hash()[:6]
    generated.mkdir(parents=True, exist_ok=True)

    signature = make_signature(ctx)

    buf_size = get_tmp_buf_size(ctx)
    
    inp, tmp, out = program.get_inp_tmp_out()

    reused_out_as_inp = {}
    for i in inp:
        for o in out:
            if program.arr_to_buf[i] == program.arr_to_buf[o]:
                reused_out_as_inp[o] = i

    allocator = {
        "native": "{dtype}* {k} = ({dtype}*) aligned_alloc(4096, {size} * sizeof({dtype})); if (!{k}) {{ printf(\"ERROR: failed to allocate buffer {k} of size {size} * sizeof({dtype})\\n\"); exit(1); }}",
        "cuda": "{dtype}* {k}; if (cudaMallocManaged(&{k}, {size} * sizeof({dtype})) != cudaSuccess) {{ printf(\"ERROR: failed to allocate buffer {k} of size {size} * sizeof({dtype})\\n\"); exit(1); }}",
    }[target]

    ref_fast_init_code = []
    reinit_code = []
    for k, v in ref_data.items():
        size = v.size
        dtype = NP_TO_GEN_DTYPE[v.dtype.type]
        # save test data
        if check_correctness or reinit_input:
            v.tofile(generated / f"data_{k}.bin")
        # make a copy in the fast memory
        ref_fast_init_code.append(allocator.format(dtype=dtype, k=k, size=size))
        read_file_str = f'read_file({k}, {size} * sizeof({dtype}), "data_{k}.bin");'
        if check_correctness:
            ref_fast_init_code.append(read_file_str)
        else:
            ref_fast_init_code.append(f'memset({k}, 0, {size} * sizeof({dtype}));')
        # outputs require separate buffers as we don't want to modify reference data
        if k in out and k not in reused_out_as_inp:
            ref_fast_init_code.append(allocator.format(dtype=dtype, k=f"result_{k}", size=size))
            zero_output_str = f"memset(result_{k}, 0, {size} * sizeof({dtype}));"
            reinit_code.append(zero_output_str)
            if check_correctness:
                ref_fast_init_code.append(zero_output_str)
        else:
            reinit_code.append(read_file_str)        
        # when input is reused as output, we just alias such output to input
        # this will do in-place modifications of such input but since input is not needed anymore, it is ok to reuse memory
        if k in reused_out_as_inp:
            # output should point to input
            inp_name = reused_out_as_inp[k]
            ref_fast_init_code.append(f"{dtype}* result_{k} = {inp_name};")

    ref_fast_init_code = "\n".join(ref_fast_init_code)
    reinit_code = "\n".join(reinit_code) if reinit_input else ""

    array_verify_code = []
    for k, v in ref_data.items():
        if k not in out:
            continue
        array_verify_code.append(f"for (int i = 0; i < {v.size}; i++) {{")
        array_verify_code.append(f"    double eps = 1e-2;");
        array_verify_code.append(f"    double abs_err = fabs(result_{k}[i] - {k}[i]);");
        array_verify_code.append(f"    double rel_err = abs_err / (fabs({k}[i]) + eps);");        
        array_verify_code.append(f"    if (abs_err > eps && rel_err > eps) {{")
        array_verify_code.append(f'        printf("Error: mismatch at {k}, %d, %f (computed) != %f (expected) \\n", (int)i, (double)result_{k}[i], (double){k}[i]);')
        array_verify_code.append(f"        ok = 0;")
        array_verify_code.append(f"        break;")
        array_verify_code.append(f"    }}")
        array_verify_code.append(f"}}")
    array_verify_code = "\n".join(array_verify_code)
        
    inp_arrays = [k for k in inp if isinstance(program.buffers[program.arr_to_buf[k]], Buffer)]

    call_args = sorted(inp_arrays) + [f"result_{k}" for k in sorted(out)]
    call_args = ", ".join(call_args)

    main_correctness_check = f"""
        TIC(time_correctness);
        {program.name}({call_args}, _tmp);
        double correctness_elapsed = TOC(time_correctness);
        if (correctness_elapsed > {timeout}) {{
            printf("Seconds: %f\\n", correctness_elapsed);
            printf("success, exitting...\\n");
            return 0;
        }}
        {array_verify_code if check_correctness else ""}
        if (!ok) {{
            printf("FAILURE, exitting...\\n");
            return 1;
        }}
    """

    tmp_alloc = allocator.format(dtype="u8", k='_tmp', size=str(buf_size))

    main_code = f"""
        #include "generic_mini.h"

        void {program.name}({signature});
        int main() {{
            {ref_fast_init_code}
            size_t buf_size = {buf_size};
            i32 ok = 1;
            {tmp_alloc}
            {main_correctness_check}
            TIC(total);
            for (int i = 0; i < {min_reps} || TOC(total) < {min_seconds}; i++) {{
                {reinit_code}
                TIC(1);
                for (int j = 0; j < {rep_number}; j++) {{
                    {program.name}({call_args}, _tmp);
                }}
                double seconds = TOC(1) / {rep_number};
                printf("Seconds: %f\\n", seconds);
            }}
            printf("success, exitting...\\n");
            return 0;
        }}
    """
    main_code_lines = [l.strip() for l in main_code.splitlines()]
    main_code_lines_indented = autoindent(main_code_lines)
    main_code = '\n'.join(main_code_lines_indented)

    code_suffix = "c"
    if target == "cuda":
        code_suffix = "cu"

    main_src = generated / f"main.{code_suffix}"
    code_src = generated / f"code.{code_suffix}"
    bin_name = f"bin_{target}"
    bin_src = generated / bin_name

    with open(main_src, "w") as f:
        f.write(main_code)
    with open(code_src, "w") as f:
        f.write(code)

    if not silent:
        print(f"Compiling ({code_src})...")
    t1 = time()
    compile_code([main_src, code_src], bin_src, target)
    t2 = time()
    if not silent:
        print(f"Compiling ({code_src})... DONE ({t2-t1:.3f} s)")

    full_run_cmd = ["./" + bin_name]
    timeout_thresh = max(min_reps * timeout, min_seconds * 2.0)  # mul by 2 to allow for some overhead
    if not silent:
        print(f"Running ({target})...")
    t1 = time()
    try:
        result = subprocess.run(
            full_run_cmd,
            cwd=str(generated),
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            universal_newlines=True,
            timeout=timeout_thresh if timeout != float('inf') else None,
            preexec_fn=set_memory_limit,
        )
    except subprocess.TimeoutExpired:
        raise subprocess.TimeoutExpired(full_run_cmd, timeout_thresh)
    finally:
        if remove_tmp_files:
            shutil.rmtree(generated)
    t2 = time()
    if not silent:
        print(f"Running ({target})... DONE ({t2-t1:.3f} s)")
    
    if result.returncode != 0:
        print(result.stdout)

    if result.returncode == 1 and "FAILURE, exitting..." in result.stdout:
        print(result.stdout)
        raise ArithmeticError("FAILURE: Kernel computed incorrect results!")
    elif result.returncode != 0:
        raise RuntimeError(f"Run failed: {' '.join(full_run_cmd)}")

    # find pattern with regex: "Seconds: 1.23"
    seconds = re.findall(r"Seconds: ([0-9.]+)", result.stdout)
    if not seconds:
        raise RuntimeError("Could not find elapsed seconds")
    
    if len(seconds) == 1 and float(seconds[0]) > timeout:
        # fast timeout case
        raise subprocess.TimeoutExpired(full_run_cmd, timeout)
    
    return [float(s) for s in seconds]


def greedy_opt_no_snitch_silent(program):
    return greedy_opt(program, snitch=False, silent=True)


def greedy_opt_snitch_silent(program):
    return greedy_opt(program, snitch=True, silent=True)


def args_as_str(args):
    return str(args)[1:-1]  # expect list and cut away square brackets


def apply_to_exhaustion(program, greedy_transforms, silent=False, exceptions=[], eval_func=None):
    still_transforming = True
    while still_transforming:
        still_transforming = False 
        for transform_func, find_transforms_func in greedy_transforms:
            transforms = find_transforms_func(program)
            transforms = sorted(transforms)
            while transforms and transforms[-1] in exceptions:
                transforms.pop()
            if not transforms:
                continue
            args = transforms.pop()
            if not silent:
                print(f'{transform_func.__name__}(program, {args_as_str(args)})')
            transform_func(program, *args)
            if eval_func is not None:
                eval_func(program)
            still_transforming = True
            break


def apply_random_transform(program, random_transforms, silent=False):
    # randomly select transform type
    transforms_with_locations = []
    for apply_transform, find_transform in random_transforms:
        locations = find_transform(program)
        transforms_with_locations.extend([(apply_transform, loc) for loc in locations])
    if not transforms_with_locations:
        return False
    chosen_transform, chosen_location = random.choice(transforms_with_locations)
    if not silent:
        print(f'{chosen_transform.__name__}(program, {args_as_str(chosen_location)})')
    chosen_transform(program, *chosen_location)
    return True


def random_opt(program, random_groups, silent=False, seed=0):
    random.seed(seed)
    
    # greedyly canonicalize the program
    apply_to_exhaustion(program, [
        (split_scopes, find_splittable_scopes),
    ], silent=silent)

    for t, a, p in random_groups:
        if p == 0:
            continue
        already_appled = 0
        while True:
            applicable_list = a(program)
            num_applicable = len(applicable_list)
            if num_applicable == 0:
                break
            if p * (num_applicable + already_appled) < 1:
                # when there are few transforms available, interpret p as a probability to apply transformation at each step
                skip_probability = 1 - p
            else:
                # when there are many transforms available, aim for applying "p * num_applicable" transforms in average
                skip_probability = 1 / (p * (num_applicable + already_appled))
            # print(f'{t.__name__}: {num_applicable} applicable, skip probability: {skip_probability}')
            assert 0 <= skip_probability and skip_probability <= 1
            if random.random() < skip_probability:
                break
            chosen_location = random.choice(applicable_list)
            if not silent:
                print(f'{t.__name__}(program, {args_as_str(chosen_location)})')
            t(program, *chosen_location)
            already_appled += 1


def uniform_random_opt(program, random_groups, silent=False, seed=0, limit=1000):
    random.seed(seed)
    transformation_list = []
    number_transformations = 0
    
    # greedyly canonicalize the program
    apply_to_exhaustion(program, [
        (split_scopes, find_splittable_scopes),
    ], silent=silent)

    for t, a, _ in random_groups:
        while True:
            applicable_list = a(program)
            applicable_list.append('[done]')
            chosen_location = random.choice(applicable_list)
            if not silent:
                print(f'{t.__name__}(program, {args_as_str(chosen_location)})')
            number_transformations += 1
            if number_transformations >= limit:
                return transformation_list
            if chosen_location == '[done]':
                break
            t(program, *chosen_location)
            # print(f"[DEBUG] Program after transformation:")
            # print(program.text(), flush=True)
            transformation_list.append((t.__name__, chosen_location))
    return transformation_list


def heuristic_search_opt(program: RootScope, transform_info: Dict,
                         random_groups: Dict, alpha: float = 0.2,
                         silent: bool = False, seed: int = 42, limit: int = 1000):
    """
    A heuristic search optimization algorithm based on the
    exploration-exploitation strategy as well as information from
    the transformation_info dictionary which is accumulated from
    previous optimization runs.
    """
    random.seed(seed)
    transformation_list = []

    number_of_transformations = 0
    apply_to_exhaustion(program, [
        (split_scopes, find_splittable_scopes),
    ], silent=silent)

    applicable_list = []

    for apply_func, find_func, _ in random_groups:
        while True:
            r = random.random()
            if r < alpha:
                # If the random number is less than alpha,
                # it means that we choose to explore the search space
                # rather than exploit the information we have
                applicable_list = find_func(program)
                applicable_list.append('[done]')
                chosen_location = random.choice(applicable_list)
            else:
                # If the random number is greater than alpha,
                # it means that we choose to exploit the information
                # we have about the transformations.
                pass
    return transformation_list


def generate_input_for_program(program, fixed_inputs={}):
    np.random.seed(0)
    input_dict = {}
    # 1. assign inputs that are explicitly provided
    for arr_name, value in fixed_inputs.items():
        if arr_name not in program.arr_to_buf:
            continue
        buf_name = program.arr_to_buf[arr_name]
        buf = program.buffers[buf_name]
        input_dict[arr_name] = np.array(value, dtype=GEN_TO_NP_DTYPE[buf.dtype])
    # 2. generate constants
    inp, tmp, out = program.get_inp_tmp_out()
    # for arr_name in inp:
    #     buf_name = program.arr_to_buf[arr_name]
    #     buf = program.buffers[buf_name]
    #     if isinstance(buf, Const):
    #         input_dict[arr_name] = np.array(buf.value, dtype=GEN_TO_NP_DTYPE[buf.dtype])
    # 3. generate integers (array sizes) if they are not provided
    for arr_name in inp:
        buf_name = program.arr_to_buf[arr_name]
        buf = program.buffers[buf_name]
        if arr_name in fixed_inputs:
            continue
        if isinstance(buf, Buffer) and buf.dtype.startswith("i") and len(buf.dims) == 0:
            input_dict[arr_name] = (np.random.randint(3, 10, size=buf.dims)).astype(GEN_TO_NP_DTYPE[buf.dtype])
    # 4. generate arrays
    for arr_name in inp:
        buf_name = program.arr_to_buf[arr_name]
        buf = program.buffers[buf_name]
        if arr_name in input_dict:
            # already generated
            continue
        assert isinstance(buf, Buffer)
        int_dims = []
        for arr in program.frozen_dims[arr_name]:
            if isinstance(arr, ConstElem):
                int_dims.append(arr.value)
            else:
                int_dims.append(input_dict[arr.name])
        if buf.dtype.startswith('f'):
            input_dict[arr_name] = np.array(np.random.randn(*int_dims)).astype(GEN_TO_NP_DTYPE[buf.dtype])
        elif buf.dtype.startswith('i'):
            input_dict[arr_name] = np.array(np.random.randint(3, 10, size=int_dims)).astype(GEN_TO_NP_DTYPE[buf.dtype])
        else:
            raise Exception("Unexpected buffer type")
    return input_dict


def clear_transform_annotations(program):
    if isinstance(program, RootScope):
        for buf in program.buffers.values():
            buf.transform_annotations = []
            for d in buf.dims:
                d.transform_annotations = []
        for ssr_descr in program.options.setdefault("ssr", {}).values():
            ssr_descr["transform_annotations"] = []
    if isinstance(program, LoopScope):
        program.dim.transform_annotations = []
    if isinstance(program, Scope):
        program.transform_annotations = []
        for op in program.ops:
            clear_transform_annotations(op)
    elif isinstance(program, Operation):
        program.transform_annotations = []
        for elem in program.all_elem:
            elem.transform_annotations = []
    else:
        raise ValueError(f"Unknown scope type {type(program)}")


def add_transform_annotations(program, applicable_transforms):
    clear_transform_annotations(program)
    tid = 0
    for tid, (transform, loc) in enumerate(applicable_transforms):
        if transform in (swap_ops, swap_nested_scopes, join_scopes, unroll_scope, enable_frep):
            [scope_name] = loc
            parent, idx = find_anything_by_name(program, scope_name)
            parent.ops[idx].transform_annotations.append([tid, transform.__name__])
        elif transform in (enable_ssr,):
            [op_name, elem_idx] = loc
            elem = find_elem_by_location(program, op_name, elem_idx)
            elem.transform_annotations.append([tid, transform.__name__])
        elif transform in (reuse_arr_dims, reuse_buffers, move_buf_to_stack, tile_buffer):
            buf_name = loc[0]
            program.buffers[buf_name].transform_annotations.append([tid, transform.__name__, *loc[1:]])
        elif transform in (increase_ssr_depth_by_idx, merge_ssr_by_idx, activate_ssr_by_id):
            ssr_idx = loc[0]
            program.options["ssr"][ssr_idx]["transform_annotations"].append([tid, transform.__name__, *loc[1:]])
        elif transform in (lower_transform_group,):
            program.transform_annotations.append([tid, transform.__name__])
        else:
            raise NotImplementedError(f"Transform {transform} not supported")


def replace_buf_accesses(root, buf_name, replacement, scope=None):
    if scope is None:
        scope = root
        for buf in root.buffers.values():
            for dim_idx, dim in enumerate(buf.dims):
                if isinstance(dim, Array) and root.arr_to_buf[dim.name] == buf_name:
                    buf.dims[dim_idx] = copy.deepcopy(replacement)
    for op in scope.ops:
        if isinstance(op, Operation):
            for idx, elem in enumerate(op.inp_elem):
                op.inp_elem[idx] = elem.replace_buf_access(root, buf_name, replacement)
        elif isinstance(op, Scope):
            op.dim = op.dim.replace_buf_access(root, buf_name, replacement)
            replace_buf_accesses(root, buf_name, replacement, op)


def specialize_inputs(program, fixed_inputs):
    # specialize inputs
    for buf_name, const_value in fixed_inputs.items():
        buf = program.buffers[buf_name]
        if isinstance(buf, Buffer) and len(buf.dims) == 0:
            # program.buffers[buf_name] = Const(value=fixed_inputs[buf_name], dtype=buf.dtype)
            replace_buf_accesses(program, buf_name, ConstElem(value=const_value, dtype=buf.dtype))
            del program.buffers[buf_name]
            del program.arr_to_buf[buf_name]
        else:
            raise ValueError("Only scalar parameters can be specialized")

def has_one_op_in_scope(scope: Scope):
    return len(scope.ops) == 1 and isinstance(scope.ops[0], Operation)


def is_innermost_loop(scope: LoopScope | RootScope):
    for op in scope.ops:
        if isinstance(op, LoopScope):
            return False
        elif isinstance(op, Operation):
            pass
        else:
            raise NotImplementedError
    return True


OP_TO_AVX = {
    "+": "add_p",
    "*": "mul_p",
    "/": "div_p",
    "-": "sub_p"
}

REDUCE_OP_TO_AVX = {
    ("+=", "*"): "fmadd",
}

POSSIBLE_REDUCTIONS = [red_op for (red_op, _) in REDUCE_OP_TO_AVX.keys()]

GEN_TYPE_TO_AVX_SUFFIX = {
    DataType.from_string("f16"): "h",
    DataType.from_string("f32"): "s",
    DataType.from_string("f64"): "d"
}

SUPPORTED_SIMDLENS = {256, 512}

AVX_TO_GEN = {v: k for k, v in GEN_TYPE_TO_AVX_SUFFIX.items()}

def sleef_call_found(opdesc: OpDesc):
    if opdesc.out_dtype == 'f16':
        return False
    return "Sleef" in opdesc.func


def all_ops_are_convertible(operations: list[Operation]):
    for operation in operations:
        opdesc = operation.op

        if not simple_op_found(opdesc) and not sleef_call_found(opdesc) and not simple_reduction_found(opdesc):
            return False
    return True


def simple_op_found(opdesc):
    return len(list(get_func_ops(opdesc))) == 1

def get_reduce_ops(opdesc):
    for reduction_operator in POSSIBLE_REDUCTIONS:
        if reduction_operator in opdesc.reduce:
            yield reduction_operator

def get_func_ops(opdesc):
    for operator in OP_TO_AVX.keys():
        if operator in opdesc.func:
            yield operator

def get_reduce_op(opdesc):
    rops = list(get_reduce_ops(opdesc))
    if len(rops) != 1:
        raise ValueError(f"Expected exactly one reduction operator, got {rops}")
    return rops[0]

def get_func_op(opdesc):
    fops = list(get_func_ops(opdesc))
    if len(fops) != 1:
        raise ValueError(f"Expected exactly one function operator, got {fops}")
    return fops[0]

def get_reduce_func_tuple(opdesc):
    return (get_reduce_op(opdesc), get_func_op(opdesc))

def simple_reduction_found(opdesc):
    if not simple_op_found(opdesc):
        return False
    
    if len(list(get_reduce_ops(opdesc))) != 1:
        return False

    return get_reduce_func_tuple(opdesc) in REDUCE_OP_TO_AVX.keys()


def vec_to_scalar(out_elem, inp_elem, vector_size: int, vec_dtype: str, reduce: str):
    assert len(inp_elem) == 1
    vec_func = f"vec_to_scalar_{vec_dtype}"
    new_out: Array = copy.deepcopy(out_elem)
    [new_out] = add_one_to_indices([new_out])
    new_out.indices[-1] = canonicalize_access(IndexExpr(new_out.indices[-1], "+", Index(0)))
    inp_elem = add_one_to_indices(inp_elem)
    avx_suffix = vec_dtype[0]
    return LoopScope(dim=canonicalize_access(vector_size), ops=Operation(new_out, inp_elem + ['{0}'], OpDesc(AVX_TO_GEN[avx_suffix], vec_func + "({0}, {1})", reduce=reduce)))

def vectorize_sleef_call(func, vector_size: int):
    sleef_call = re.findall(r"^[^(]+", func)[0]
    return sleef_call[:10] + str(vector_size) + sleef_call[10:]


def add_one_to_indices(arrays: List[Array]):
    result = []
    for array in arrays:
        result.append(Array(array.name, [idx.shift_index(1) for idx in array.indices]))
    return result


def index_is_over_innermost_dim(index: Index | IndexExpr):
    if isinstance(index, Index):
        return index.pos == 0
    elif isinstance(index, IndexExpr):
        if index.op != "+":
            return False
        else:
            return index_is_over_innermost_dim(index.lhs) or index_is_over_innermost_dim(index.rhs)

def find_vectorizable_ops(program: RootScope) -> Set[str]:
    result = set()
    for operation, scopes in get_flattened_ops_with_scopes(program):
        if not is_innermost_loop(scopes[0]):
            continue
        if not index_is_over_innermost_dim(operation.out_elem.indices[-1]):
            continue
        # check if operation can be converted to vec op, fmadd and copying are
        # currently supported
        if operation.op.func != '{0}':
            reduce, func = get_reduce_func_tuple(operation.op)
            if (reduce, func) not in REDUCE_OP_TO_AVX:
                continue
        dtype_size = operation.out_elem.get_dtype(program).size
        for simdlen in SUPPORTED_SIMDLENS:
            if scopes[0].dim.value == simdlen // dtype_size:
                result.add((operation.out_elem.name, simdlen))
    return result


def vectorize_operation(program: RootScope, op_name: str, simdlen: int):
    assert simdlen in SUPPORTED_SIMDLENS
    assert (op_name, simdlen) in find_vectorizable_ops(program)
    parent_scope, idx = find_op_by_name(program, op_name)
    operation: Operation = parent_scope.ops[idx]
    vec_dtype = GEN_TYPE_TO_AVX_SUFFIX[operation.op.out_dtype] + str(simdlen)
    operation.op.out_dtype = vec_dtype
    program.buffers[operation.out_elem.name].dtype = vec_dtype
    

def get_simdlen(simd: str) -> int:
    return {
        'sse': 128,
        'avx': 256,
        'avx512': 512,
        'neon': 128,
    }[simd]

def get_vectorized_dtype(simd: str, base_type: DataType) -> DataType:
    simdlen = get_simdlen(simd)
    return DataType(base_type.literal, base_type.size, simdlen // base_type.size)

def get_supported_simd_dtypes(simd: str) -> Tuple[str]:
    return {
        'avx': (
            DataType.from_string('f32x8'),
            DataType.from_string('f16x16'),
        ),
        'avx512': (
            DataType.from_string('f16x32'),
        ),
        'neon': (
            DataType.from_string('f32x4'),
        ),
    }.get(simd, tuple())


def run_kernel(creator, reference, optimizer, runner, fixed_inputs={},
               hand_optimized_file=None,
               transformations=[],
               timeout=float('inf')):
    program = creator()
    input_data = generate_input_for_program(program, fixed_inputs)
    output_data = reference(input_data)

    # print(f"[DEBUG] Exploitation: {transformations}", flush=True)
    # If a list of transformations is provided, apply them to the program
    # before running the optimizer.
    try:
        if transformations:
            # Canonicalizes the program before running the optimizer.
            apply_to_exhaustion(program, [
                (split_scopes, find_splittable_scopes),
            ], silent=True)

            for transform_name, loc in transformations:
                assert transform_name in CPU_TRANSFORMATIONS
                apply_func, _ = CPU_TRANSFORMATIONS[transform_name]
                apply_func(program, *loc)
    except Exception as e:
        raise Exception(f"Error while applying transformations {transformations}: {e}")

    try:
        path = optimizer(program)
    except Exception as e:
        raise Exception(f"Error while running the optimizer: {e}")

    path = path or []
    path = transformations + path

    # try:
    cycles = runner(program, {**input_data, **output_data}, timeout=timeout)
    if hand_optimized_file is not None:
        ref = runner(program, {**input_data, **output_data},
                        reference_code=hand_optimized_file, timeout=timeout)
    else:
        ref = None
    error = None
    peak = program.peak_performance_cycles()
    # except subprocess.TimeoutExpired as e:
    #     return [6*timeout], None, None, path
    # except Exception as e:
    #     cycles = -1
    #     ref = -1
    #     import traceback
    #     print(traceback.format_exc())
    #     error = e

    peak = program.peak_performance_cycles()

    return {
        'measured': cycles,
        'reference': ref,
        'peak': peak,
        'path': path,
        'error': error,
    }


def parse_program(text, invert_indices=True):
    root_ops = []

    parsing_mode = "name" # "name", "in", "out", "decls", "body"

    declarations = {}
    input_names = []
    output_names = []

    for line in text.split("\n"):
        # remove whitespace and comments
        line = line.strip()
        if "#" in line:
            line = line[:line.index("#")]
        if not line:
            continue

        if parsing_mode == "name":
            assert line.startswith("Name:")
            kernel_name = line[len("Name:"):].strip()
            parsing_mode = "in"
            continue

        if parsing_mode == "in":
            assert line.startswith("In:")
            input_names = [i.strip() for i in line[len("In:"):].strip().split(",")]
            parsing_mode = "out"
            continue
        
        if parsing_mode == "out":
            assert line.startswith("Out:")
            # parse output
            output_names = [i.strip() for i in line[len("Out:"):].strip().split(",")]
            parsing_mode = "decls"
            continue
    
        if parsing_mode == "decls":
            if line.startswith("Declarations:"):
                continue
            if line.startswith("Code:"):
                parsing_mode = "body"
                continue

            # example "cr f32 [32:N,4:N,2,16, 32:N,2:N,4,16] heap"
            res = re.match(r"(\w+)\s+(\w+)\s+\[(.*)\]\s+(\w+)", line)
            buf_name, dtype, shape_str, allocation = res.groups()
            shape_list = [s.strip() for s in shape_str.split(',')] if shape_str else []
            shape_list_no_attr = [s.split(':')[0] for s in shape_list]
            buf = Buffer(dims=shape_list_no_attr, dtype=dtype)
            buf.used_dims = [not i.endswith(":N") for i in shape_list]
            buf.allocation = allocation
            declarations[buf_name] = buf

            continue
                

        if parsing_mode == "body":
            # parse line
            res = re.match(r'(?:([^[=\]]+)\s+)?(\S+(?:\[[^\]]+\])?\s+[^=]*=\s+.*)$', line)
            if res is None:
                raise ValueError(f"Unable to parse line: {line}")
            
            loops_str, op_str = res.groups()
            if loops_str is None:
                loops_str = ''
            loops_list = loops_str.split()
            non_fused_postfix = [i for i in loops_list if i != '|']
            non_fused_postfix_no_attr = [i.split(':')[0] for i in non_fused_postfix]
            non_fused_postfix_attr = [(i.split(':')[1] if ":" in i else None) for i in non_fused_postfix]
            num_fused_scopes = len(loops_list) - len(non_fused_postfix)
            
            op_str = op_str.strip()
            output_str, _, reduce_assign, input_str = re.match(r'^(\w+(\[.+\])?)\s+(.*=)\s+(.*)$', op_str).groups()

            if reduce_assign == "=":
                reduce_template = ''
            elif reduce_assign in "+=":
                reduce_template = '{y} += {x}'
            elif reduce_assign == "fmaxf=":
                reduce_template = '{y} = fmaxf({y}, {x})'
            elif reduce_assign == "fma_avx=":
                reduce_template = '{y} = _mm512_fmadd_ph({x}, {y})'
            elif reduce_assign == "fminf=":
                reduce_template = '{y} = fminf({y}, {x})'
            else:
                raise ValueError(f"Unknown reduce assign: {reduce_assign}")

            # parse input string and replace all patterns resembling inputs with template placeholders
            # 'func(a, b)' -> 'func({0}, {1})'
            # 'a + b' -> '{0} + {1}'
            func_list = re.findall(r'\w+\(.+\)', input_str)
            func_names = [func.split('(')[0] for func in func_list]
            arg_pattern = r'([a-zA-Z_]\w*(?:\[[^]]+\])?|\d+\.\d+|\{\d+\}|\d+)'
            args_list = re.findall(arg_pattern, input_str)
            op_template_list = re.sub(arg_pattern, '$$', input_str).split('$$')

            assert len(args_list) + 1 == len(op_template_list)

            input_list = list(dict.fromkeys(args_list))  # remove duplicates, maintain order
            input_list = [i for i in input_list if i not in func_names] # remove function names
            keywords = {'float', 'int', 'double', 'f32', 'f64', 'i32', 'i64', 'f16', 'h512', 'd512', 's512'}
            input_list = [i for i in input_list if i not in keywords]
            replacement_map = {i: k for k, i in enumerate(input_list)}

            template_input_op = ""
            for x, y in zip(args_list, op_template_list):
                template_input_op += y
                replacement_idx = replacement_map.get(x)
                if replacement_idx is not None:
                    template_input_op += "{" + str(replacement_idx) + "}"
                else:
                    template_input_op += x
            template_input_op += op_template_list[-1]

            # invert the order of scope indices
            inverted_input_list = []
            for input in input_list:
                inverted_input = input[:]
                if invert_indices:
                    for i in range(len(loops_list) // 2):
                        inverted_input = inverted_input.replace("{" + str(i) + "}", "{x}")
                        inverted_input = inverted_input.replace("{" + str(len(loops_list) - i - 1) + "}", "{" + str(i) + "}")
                        inverted_input = inverted_input.replace("{x}", "{" + str(len(loops_list) - i - 1) + "}")
                inverted_input_list.append(inverted_input)

            inverted_output = output_str[:]
            if invert_indices:
                for i in range(len(loops_list) // 2):
                    inverted_output = inverted_output.replace("{" + str(i) + "}", "{x}")
                    inverted_output = inverted_output.replace("{" + str(len(loops_list) - i - 1) + "}", "{" + str(i) + "}")
                    inverted_output = inverted_output.replace("{x}", "{" + str(len(loops_list) - i - 1) + "}")

            res = re.match(r"(\w+)", inverted_output)
            if res is None:
                raise ValueError(f"Unable to parse output for {inverted_output}")
            out_name = res[0]

            new_op = Operation(
                out_elem=inverted_output,
                inp_elem=inverted_input_list,
                op=OpDesc(
                    out_dtype=declarations[out_name].dtype,
                    func=template_input_op,
                    reduce=reduce_template,
                )
            )

            if non_fused_postfix_no_attr:
                new_loop_nest = loop_scope(
                    dims=non_fused_postfix_no_attr,
                    ops=new_op,
                )
            else:
                new_loop_nest = new_op
            
            # add attributes to the loop nest
            current_scope = new_loop_nest
            for i, attr in enumerate(non_fused_postfix_attr):
                if attr is not None:
                    if attr == "g":
                        current_scope.options["parallel"] = True
                        current_scope.options["parallel_level"] = "cuda_grid"
                    elif attr == "b":
                        current_scope.options["parallel"] = True
                        current_scope.options["parallel_level"] = "cuda_block"
                    elif attr == "w":
                        current_scope.options["parallel"] = True
                        current_scope.options["parallel_level"] = "cuda_warp"
                    elif attr == "f":
                        current_scope.options["parallel"] = True
                        current_scope.options["parallel_level"] = "cuda_fragment"
                    elif attr == "p":
                        current_scope.options["parallel"] = True
                    else:
                        raise ValueError(f"Unknown attribute: {attr}")
                current_scope = current_scope.ops[0]

            # find op list to attach to
            last_nest = root_ops
            for i in range(num_fused_scopes):
                last_nest = last_nest[-1].ops

            last_nest.append(new_loop_nest)

    program = RootScope(
        ops=root_ops,
        name=kernel_name,
        input_declarations=declarations,
        reusable_inputs=set(),
        used_outputs=set(output_names),
    )
    return program


def build_loop_reordering_sequence(op_name, before, after):
    assert len(before) == len(after)
    assert len(before) == len(set(before))
    assert set(after) == set(before)
    
    transform_sequence = []
    
    while len(before) > 1:
        desired_index = 0
        name = after[desired_index]
        original_index = before.index(name)
        
        num_swaps = original_index - desired_index
        scopes_in_nest = len(before)
        swap_start = scopes_in_nest - num_swaps
        for swap_target in range(swap_start, scopes_in_nest):
            scope_name = f'{op_name}#{swap_target}'
            transform_sequence.append(('swap_nested_scopes', scope_name))
        
        before = before[:original_index] + before[original_index + 1:]
        after = after[1:]
        
    return transform_sequence


def time_prof(repeats, func, init=lambda: (), finalize=lambda: (), number=1, warmup=0.3, time_budget=1.0):    
    times = []
    
    min_warmup_repeats = math.ceil(repeats * warmup)
    min_total_repeats = min_warmup_repeats + repeats
        
    i = 0
    total_elapsed = 0
    while True:
        init()
        t1 = time()
        for _ in range(number):
            func()
        finalize()
        t2 = time()
        elapsed = (t2 - t1) / number
        total_elapsed += elapsed
        times.append(elapsed)
        i += 1
        if total_elapsed > time_budget and i >= min_total_repeats:
            break
    
    assert len(times) >= min_total_repeats
    times = times[math.ceil((len(times) - min_warmup_repeats) * warmup):]
    assert len(times) >= repeats
    
    mean = np.mean(times)
    std = np.std(times)
    
    return mean, std, times
