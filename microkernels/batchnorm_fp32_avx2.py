
from gen import *
import torch


def run_batchnorm_handtuned(N, C, H, W):
    epsilon = 1e-4
    momentum = 0.9  # irrelevant for inference (training=False)
    x = np.random.uniform(-1, 1, (N, C, H, W)).astype(np.float32)
    scale = np.random.uniform(-1, 1, (C,)).astype(np.float32)
    bias = np.random.uniform(-1, 1, (C,)).astype(np.float32)
    mean = np.random.uniform(-1, 1, (C,)).astype(np.float32)
    var = np.random.uniform(0, 1, (C,)).astype(np.float32)
    
    def init_src():
        x[:] = np.random.uniform(-1, 1, (N, C, H, W)).astype(np.float32)
        scale[:] = np.random.uniform(-1, 1, (C,)).astype(np.float32)
        bias[:] = np.random.uniform(-1, 1, (C,)).astype(np.float32)
        mean[:] = np.random.uniform(-1, 1, (C,)).astype(np.float32)
        var[:] = np.random.uniform(0, 1, (C,)).astype(np.float32)
    
    tx = torch.from_numpy(x)
    tscale = torch.from_numpy(scale)
    tbias = torch.from_numpy(bias)
    tmean = torch.from_numpy(mean)
    tvar = torch.from_numpy(var)
    
    pt_mean, pt_std, pt_times = time_prof(10, lambda: torch.nn.functional.batch_norm(tx, tmean, tvar, weight=tscale, bias=tbias, training=False, momentum=1-momentum, eps=epsilon), init_src)
    print(f"Pytorch batchnorm time [ms]: avg {pt_mean * 1e3:.6f} std {pt_std * 1e3:.6f} reps {len(pt_times)}")
    
    ty = torch.nn.functional.batch_norm(tx, tmean, tvar, weight=tscale, bias=tbias, training=False, momentum=1-momentum, eps=epsilon)
    y = ty.numpy()
    
    def eval_func(program, ignore=True):
        if ignore:
            return
        print(program.text())
        time = test_native_code(
            program,
            {'x': x, 'mean': mean, 'var': var, 'scale': scale, 'bias': bias, 'y': y},
            min_reps=10,
            remove_tmp_files=False,
            silent=False,
        )
        time = time[max(len(time) // 3, 1):]
        print(f"My batchnorm [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")
    
    epsilon = 1e-4
    program_text = f"""
        Name: batchnorm_inference
        In: x, mean, var, scale, bias
        Out: y
        Declarations:
            x f32 [{N}, {C}, {H}, {W}] heap
            y f32 [{N}, {C}, {H}, {W}] heap
            mean f32 [{C}] heap
            var f32 [{C}] heap
            scale f32 [{C}] heap
            bias f32 [{C}] heap
            var_eps f32 [{C}] heap
            sqrt_var_eps f32 [{C}] heap
            alpha f32 [{C}] heap
            beta f32 [{C}] heap
        Code:
        {C} var_eps[{{0}}] = var[{{0}}] + {epsilon}
        {C} sqrt_var_eps[{{0}}] = sqrtf(var_eps[{{0}}])
        {C} alpha[{{0}}] = scale[{{0}}] / sqrt_var_eps[{{0}}]
        {C} beta[{{0}}] = - alpha[{{0}}] * mean[{{0}}] + bias[{{0}}]
        {N} {C} {H} {W} y[{{0}}, {{1}}, {{2}}, {{3}}] = alpha[{{1}}] * x[{{0}}, {{1}}, {{2}}, {{3}}] + beta[{{1}}]
    """
    
    program = parse_program(program_text); eval_func(program, ignore=False)
    
    swap_nested_scopes(program, 'y#3'); eval_func(program)
    swap_nested_scopes(program, 'y#2'); eval_func(program)
    apply_to_exhaustion(program, [(join_scopes, find_joinable_scopes)], silent=False, eval_func=eval_func)
    apply_to_exhaustion(program, [(reuse_arr_dims, find_reusable_arr_dims)], silent=False, eval_func=eval_func)
    apply_to_exhaustion(program, [(reuse_buffers, find_reusable_buffers)], silent=False, eval_func=eval_func)
    apply_to_exhaustion(program, [(move_buf_to_stack, lambda x: find_bufs_movable_to_stack(x, 4096))], silent=False, eval_func=eval_func)
    
    parallelize_scope(program, 'y#2'); eval_func(program, ignore=False)


if __name__ == "__main__":
    run_batchnorm_handtuned(8, 3, 2048, 2048)