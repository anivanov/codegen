import sqlite3
from enum import StrEnum


class RuntimeMeasurement:
    """
    Convention is that 'mean' and 'std' store time in milliseconds.
    Empty 'code' means non-existing entry, in which case all other fields are invalid
    'timeout' is a boolean flag that indicates that the measurement was not performed because of timeout,
    in which case 'mean' stores the timeout threshold.
    If measurement fails because of runtime error, 'mean' contains float('inf').
    """
    def __init__(self, code: str, mean: float, std: float, timeout: bool):
        self.code = code
        self.mean = mean
        self.std = std
        self.timeout = timeout

    @staticmethod
    def not_found():
        return RuntimeMeasurement('', 0.0, 0.0, False)
        
    def found(self) -> bool:
        return bool(self.code)
    
    def timed_out(self) -> bool:
        return self.found() and self.timeout
    
    def valid(self) -> bool:
        return self.found() and not self.timeout
    
    def __iter__(self):
        return iter((self.code, self.mean, self.std, self.timeout))
    
    def __repr__(self):
        return f'RuntimeMeasurement(code=..., mean={self.mean}, std={self.std}, timeout={self.timeout})'


class MeasurementStatus(StrEnum):
    MEASURED = 'measured'
    TIMED_OUT = 'timed out'
    FROM_CACHE = 'from cache'
    RUNTIME_ERROR = 'runtime error'


class SQLiteRuntimeCache:
    """
        This class is a simple SQLite-based cache for runtime measurements.
        The order of writing from multiple processes is not guaranteed.
    """
    
    def __init__(self, filename: str):
        self.db = sqlite3.connect(filename, timeout=300)
        self.db.execute('PRAGMA journal_mode=WAL')
        self.db.execute('''
            CREATE TABLE IF NOT EXISTS cache (
                hash TEXT PRIMARY KEY,
                code TEXT,
                mean REAL,
                std REAL,
                timeout BOOLEAN
            )
        ''')
        self.db.commit()
        
    def __setitem__(self, key: str, value: RuntimeMeasurement):
        code, mean, std, timeout = value
        self.db.execute('''
            REPLACE INTO cache (hash, code, mean, std, timeout) VALUES (?, ?, ?, ?, ?)
        ''', (key, code, mean, std, timeout))
        self.db.commit()
        
    def __getitem__(self, key: str) -> RuntimeMeasurement:
        cursor = self.db.execute('''
            SELECT code, mean, std, timeout FROM cache WHERE hash = ?
        ''', (key,))
        row = cursor.fetchone()
        if row is None:
            return RuntimeMeasurement.not_found()
        return RuntimeMeasurement(*row)
    
    def __contains__(self, key: str):
        cursor = self.db.execute('''
            SELECT 1 FROM cache WHERE hash = ?
        ''', (key,))
        row = cursor.fetchone()
        found = bool(row)
        return found
    
    def __iter__(self):
        cursor = self.db.execute('''
            SELECT hash, code, mean, std, timeout FROM cache
        ''')
        for row in cursor:
            yield row[0], RuntimeMeasurement(*row[1:])
            
    def __len__(self):
        cursor = self.db.execute('''
            SELECT COUNT(*) FROM cache
        ''')
        return cursor.fetchone()[0]


class RuntimeCache:
    """
        This class wraps SQLiteRuntimeCache and provides a local cache for the runtime measurements.
        It tries to perform less queries by relying on the assumption that all 
        not-timed out runtime measurements are equivalent.
    """
    
    def __init__(self, filename: str):
        self.base_cache = SQLiteRuntimeCache(filename)
        self.local_cache = {}

    def __setitem__(self, key: str, measurement: RuntimeMeasurement):
        if key in self.local_cache:
            old_measurement = self.local_cache[key]
            
            overwrite = False
            if not old_measurement.found():
                overwrite = True  # entry was attempted to be found earlier but was not found
            elif old_measurement.timed_out() and not measurement.timed_out():
                overwrite = True  # we overwrite because old is timed out and new is not
            if not overwrite:
                return
        # We may overwrite previous valid measurement that was potentially made by other process.
        # This is not a problem: while old and new measurements can differ slightly because of measurement accuracy,
        # they are expected to be close to each other
        self.base_cache[key] = measurement
        self.local_cache[key] = measurement

    def __getitem__(self, key: str) -> RuntimeMeasurement:
        if key in self.local_cache:
            return self.local_cache[key]
        measurement = self.base_cache[key]  # extracted value is either timed out or valid measurement
        # Even if measurement is not found, we expect caller to perform measurement and
        # store actual value, which will propagate to the base cache.
        # Therefore it is ok to store 'not found' measurements in the local cache.
        # This way we avoid querying the base cache multiple times for the same key.
        self.local_cache[key] = measurement
        return measurement

    def __contains__(self, key: str):
        measurement = self[key]
        return measurement.found()
    
    def __iter__(self):
        return iter(self.base_cache)
    
    def __len__(self):
        return len(self.base_cache)
