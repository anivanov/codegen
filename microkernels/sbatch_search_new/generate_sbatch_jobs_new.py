job_template = """\
#!/bin/bash

#SBATCH --job-name={kernel}_{size}_{search}_{cand}
#SBATCH --output=logs/daint/search/{kernel}_{size}_{search}_{cand}.log
#SBATCH --account=g34
#SBATCH --time={hours}:00:00
#SBATCH --constraint=mc
#SBATCH --partition=normal
#SBATCH --nodes=1

source ~/.bashrc
conda activate main
export LDFLAGS="-lm -L$(dirname $(which clang))/../lib/"
export CFLAGS="-O3 -I . -fopenmp -march=native -I$(dirname $(which clang))/../include/"
export LD_LIBRARY_PATH="$(dirname $(which clang))/../lib/"
export OMP_PROC_BIND=close
export CC=clang
CACHE=cache_clang.db

srun numactl -N 0 -m 0 -C 0-17 python heuristic_search_new.py --kernel {kernel} --size {size} --search {search} --candidate {cand} --cost heur_pass_runtime --timeout 5000 --min_repeats {reps} --cache $CACHE
"""

search_methods = [
    # 'greedy',
    'annealing',
    #'mcts',
    'greedy_top',
]

kernels = [
    ('conv', '8x10x3x512x512x5', 10, 1),
    ('layernorm', '4096x4096', 10, 1),
    ('batchnorm', '8x3x2048x2048', 10, 1),
    ('matmul', '768x1024x1024', 10, 1),
    ('reducemean', '4096x4096', 10, 1),
    # ('relu', '4096x4096'),
    ('softmax', '24576x512', 10, 1),  # ('softmax', '6x8x512x512'),
    ('rmsnorm', '3072x4096', 10, 1),  # ('rmsnorm', '6x512x4096'),
    ('bmm', '192x512x128x512', 10, 1),  # [bsz=6,n_heads=32,seq_len=512,head_dim=128] x [bsz=6,n_heads=32,head_dim=128,seq_len=512] -> [bsz=6,n_heads=32,seq_len=512,seq_len=512]
    ('matmul', '3072x4096x4096', 10, 8),  # M=bsz,seq_len=3072 K=dim=4096 N=n_heads*head_dim=4096
    # ('mul', '3072x4096'),
    # ('add', '3072x4096'),
    ('batchnorm', '8x64x300x300', 10, 1), # from efficientnet-b7
    ('conv', '8x64x64x56x56x3', 10, 1), # from resnet-152
    ('layernorm', '16384x1024', 10, 1), # from BERT-large
    # ('relu', '8x64x112x112'), # from resnet-152
    ('swiglu', '1x256x4096x14336', 3, 8), # llama 3 8B
    ('relu_ffn', '4x512x768x3072', 3, 8), # GPT-2 small feedforward
]

candidates = [
    'heur',
    'edges',
]

for kernel, size, reps, hours in kernels:
    for search in search_methods:
        for cand in candidates:
            with open(f'{kernel}_{size}_{search}_{cand}.sh', 'w') as f:
                f.write(job_template.format(kernel=kernel, size=size, search=search, cand=cand, reps=reps, hours=hours))
