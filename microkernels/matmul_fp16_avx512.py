from gen import *
import torch

from hgemm import hgemm


def matmul_half_avx512():
    
    M = 512
    N = 14336
    K = 4096
    
    x = np.random.uniform(-0.01, 0.01, (K, M)).astype(np.float16)
    y = np.random.uniform(-0.01, 0.01, (K, N)).astype(np.float16)
    
    def init_src():
        x[:] = np.random.uniform(-0.01, 0.01, (K, M)).astype(np.float16)
        y[:] = np.random.uniform(-0.01, 0.01, (K, N)).astype(np.float16)
    
    tx = torch.from_numpy(x)
    ty = torch.from_numpy(y)
    
    def eval_func(program, ignore=True):
        if ignore:
            return
        print(program.text())
        time = test_native_code(
            program,
            {'A': x, 'B': y, 'C': np.array(torch.einsum("km,kn->nm", tx, ty))},
            rep_number=10,
            min_reps=10,
            remove_tmp_files=False,
            silent=False,
        )
        time = time[max(len(time) // 3, 1):]
        print(f"My matmul [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f} reps {len(time)}")

    program_text = f"""
        Name: gemm
        In: A, B
        Out: C
        Declarations:
            A f16 [{K}, {M}] heap
            B f16 [{K}, {N}] heap
            C f16 [{N}, {M}] heap
        Code:
        {M} {N} {K} C[{{1}}, {{2}}] += A[{{0}}, {{2}}] * B[{{0}}, {{1}}]
    """

    program = parse_program(program_text, invert_indices=False)
    specialize_inputs(program, {})

    nc = 256
    kc = 512
    mc = 256
    nr = 16 # was 16
    mr = 32 # vector size
    

    create_temporary(program, 'C')
    tile_scope(program,'C0#2', mc)
    tile_scope(program,'C0#2', mr)
    tile_scope(program,'C0#1', nc)
    tile_scope(program,'C0#1', nr)
    tile_scope(program,'C0#0', kc)

    swap_nested_scopes(program, 'C0#1')
    create_dimension(program, 'C', 1)
    swap_nested_scopes(program, 'C0#1')
    
    tile_scope(program,'C#2', nc)
    tile_scope(program,'C#2', nr)
    tile_scope(program,'C#1', mc)
    tile_scope(program,'C#1', mr)

    reorder_loops(program, 'C0', before=['Mo', 'Mc', 'Mr', 'No', 'Nc', 'Nr','Ko', 'Kc'], after=['Ko', 'No', 'Mo', 'Nc', 'Mc', 'Kc', 'Nr', 'Mr'], silent=False)
    reorder_loops(program, 'C' , before=['No', 'Nc', 'Nr', 'Mo', 'Mc', 'Mr','Ko'],       after=['Ko', 'No', 'Mo', 'Nc', 'Mc', 'Nr', 'Mr'], silent=False)

    create_temporary(program, "A")
    create_temporary(program, "B")
    # tile B
    tile_scope(program,'B0#1', kc)
    tile_scope(program,'B0#0', nc)
    tile_scope(program,'B0#0', nr)
    reorder_loops(program, 'B0', before=['Ko', 'Kc', 'No', 'Nc', 'Nr'], after=['Ko', 'No','Nc', 'Kc', 'Nr'], silent=False)

    # tile A
    tile_scope(program,'A0#1', kc)
    tile_scope(program,'A0#0', mc)
    tile_scope(program,'A0#0', mr)
    shift_scope_to_pos(program, 'C0#6', 0)
    create_dimension(program, 'A0', 0)
    shift_scope_to_pos(program, 'C0#0', 6)
    shift_scope_to_pos(program, 'A0#0', 4)
    reorder_loops(program, 'A0', before=['Ko', 'Kc', 'Mo', 'Mc', 'Mr'], after=['Ko', 'Mo', 'Mc', 'Kc', 'Mr'], silent=False)
    
    # buffer transformations only allowed AFTER create dimension
    apply_to_exhaustion(program, [(tile_buffer, find_tileable_buffers)], silent=False)


    swap_buffer_dims(program, 'A0', 4)
    swap_buffer_dims(program, 'A0', 1)
    swap_buffer_dims(program, 'A0', 2)
    swap_buffer_dims(program, 'A0', 3)
    swap_buffer_dims(program, 'A0', 2)
    swap_buffer_dims(program, 'A0', 1)

    swap_buffer_dims(program, 'B0', 1)
    swap_buffer_dims(program, 'B0', 2)

    swap_buffer_dims(program, 'C0', 5)
    swap_buffer_dims(program, 'C0', 2)
    swap_buffer_dims(program, 'C0', 3)
    swap_buffer_dims(program, 'C0', 4)
    swap_buffer_dims(program, 'C0', 3)
    swap_buffer_dims(program, 'C0', 1)
    swap_buffer_dims(program, 'C0', 2)
    swap_buffer_dims(program, 'C0', 1)
    swap_buffer_dims(program, 'C0', 0)

    vectorize_loop(program, 'B0#0', 'avx')
    vectorize_loop(program, 'A0#0', 'avx512')
    vectorize_loop(program, 'C0#0', 'avx512')
    vectorize_loop(program, 'C#0', 'avx512')

    vectorize_buffer(program, 'C0')

    apply_to_exhaustion(program, [(join_scopes, find_joinable_scopes)], silent=True)
    apply_to_exhaustion(program, [(reuse_arr_dims, find_reusable_arr_dims)], silent=True)
    apply_to_exhaustion(program, [(move_buf_to_stack, lambda x: find_bufs_movable_to_stack(x, 4096))], silent=True)
    # apply_to_exhaustion(program, [(reuse_buffers, find_reusable_buffers)], silent=False)
    parallelize_scope(program, 'B0#2')
    parallelize_scope(program, 'A0#2')
    parallelize_scope(program, 'C0#4')
    eval_func(program, ignore=False)
    pt_mean, pt_std, pt_times = time_prof(10, lambda: torch.einsum("km,kn->nm", tx, ty), init_src)
    print(f"PyTorch matmul time [ms]: avg {pt_mean * 1e3:.6f} std {pt_std * 1e3:.6f} reps {len(pt_times)}")
    
    mkl_mean, mkl_std, mkl_times = time_prof(10, lambda: hgemm(1.0, np.ascontiguousarray(x.T), np.ascontiguousarray(y), 0.0, np.zeros((M,N), dtype=np.float16)), init_src)
    print(f"MKL matmul time [ms]: avg {mkl_mean * 1e3:.6f} std {mkl_std * 1e3:.6f} reps {len(mkl_times)}")

if __name__ == '__main__':
    matmul_half_avx512()
