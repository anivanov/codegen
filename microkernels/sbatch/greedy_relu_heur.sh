#!/bin/bash

#SBATCH --job-name=search
#SBATCH --output=logs/daint/search-relu-4096x4096-greedy-heur_norep-runtime.log
#SBATCH --account=g34
#SBATCH --time=1:00:00
#SBATCH --constraint=mc
#SBATCH --partition=normal
#SBATCH --nodes=1

source ~/.bashrc
conda activate main
export CC=gcc
export LDFLAGS="-lm -L$(dirname $(which clang))/../lib/"
export CFLAGS="-O3 -I . -fopenmp -march=native -I$(dirname $(which clang))/../include/"
export LD_LIBRARY_PATH="$(dirname $(which clang))/../lib/"

srun numactl -m 0 -N 0 -C 0-17 python heuristic_search_new.py --kernel relu --size=4096x4096 \
    --min_repeats 3 --search greedy --candidate heur_norep --cost runtime
