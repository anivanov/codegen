


job_template = """\
#!/bin/bash

#SBATCH --job-name=heuristic_search_{kernel}_{search_method}
#SBATCH --output=logs/heuristic_search_{kernel}_{search_method}.log
#SBATCH --account=g34
#SBATCH --time=1:00:00
#SBATCH --constraint=mc
#SBATCH --partition=normal
#SBATCH --nodes=1

source ~/.bashrc
conda activate main
export CC=clang
export LDFLAGS="-lm"
export CFLAGS="-O3 -I . -fopenmp -march=native -I$(dirname $(which clang))/../include/"
export LD_LIBRARY_PATH="$(dirname $(which clang))/../lib/"

srun numactl -m 0 -N 0 -C 0-17 python heuristic_search.py --kernel {kernel} --min_repeats 3 --search {search_method}
"""

search_methods = [
    'least_explored_or_fastest',
    'least_explored_paths',
    'prob_avg',
    'prob_limited_avg',
    'prob_limited_avg2',
    'prob_limited_min',
    'prob_rare_eval',
    'prob_rs',
    'prob_rsb',
    'prob_rsbp',
    'probabilistic',
    'simulated_annealing',
]

kernels = [
    'conv',
    'layernorm',
    'log_softmax',
    'matmul',
    'reducemean',
]

for kernel in kernels:
    for search_method in search_methods:
        with open(f'heuristic_search_{kernel}_{search_method}.sh', 'w') as f:
            f.write(job_template.format(kernel=kernel, search_method=search_method))