from kelgen.gen import *

def find_swappable_buffer_dims(root_scope):
    if 'frozen_buffers' in root_scope.concepts:
        return []
    inp, tmp, out = root_scope.get_inp_tmp_out()
    inp_out = inp | out
    result = []
    for buf_name, buf in root_scope.buffers.items():
        related_arrs = root_scope.buf_to_arr(buf_name)
        if inp_out & related_arrs:
            continue  # input and output layouts are frozen by external caller
        for dim_idx in range(len(buf.dims) - 1):
            result.append((buf_name, dim_idx,))
    return result


def swap_buffer_dims(root_scope, buffer_name, dim_idx):
    """
        Swaps two dimensions of the multi-dimensional buffer:
           [..., dim1, dim2, ...] -> [..., dim2, dim1, ...]
         dim_idx--^     ^--dim_idx+1
    """
    assert (buffer_name, dim_idx) in find_swappable_buffer_dims(root_scope)
    buf = root_scope.buffers[buffer_name]
    buf.dims[dim_idx], buf.dims[dim_idx + 1] = buf.dims[dim_idx + 1], buf.dims[dim_idx]
    buf.used_dims[dim_idx], buf.used_dims[dim_idx + 1] = buf.used_dims[dim_idx + 1], buf.used_dims[dim_idx]
    # all accesses to the buffer need to swap their indices
    arr_names = root_scope.buf_to_arr(buffer_name)
    for op in recursive_ops(root_scope):
        op.all_elem = [elem.swap_buf_dims(arr_names, dim_idx) for elem in op.all_elem]

