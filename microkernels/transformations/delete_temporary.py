from kelgen.gen import *

def find_deletable_temporaries(root_scope):
    """
        Applicable to any array initialized with move that is not output.

        Assumptions:
        - all scopes are split
        - no buffer-related transformations happened before, only arrays are considered
    """
    if 'frozen_scopes' in root_scope.concepts:
        return []

    if not check_all_scopes_split(root_scope):
        return []  # only one operation per scope is allowed

    result = []
    for op in recursive_ops(root_scope):
        if not op.op.is_move():
            continue
        if op.out_elem.name in root_scope.used_outputs:
            continue  # output cannot be deleted
        if not isinstance(op.inp_elem[0], Array):
            continue
        if op.out_elem.indices != op.inp_elem[0].indices:
            continue
        result.append((op.out_elem.name,))
    return result


def delete_temporary(root_scope, arr_name):
    assert (arr_name,) in find_deletable_temporaries(root_scope)
    # find the operation that initializes the temporary
    op_idx = None
    orig_name = None
    for idx, op in enumerate(recursive_ops(root_scope)):
        if op.out_elem.name == arr_name:
            orig_name = op.inp_elem[0].name
            op_idx = idx
        for elem in op.inp_elem:
            if isinstance(elem, Array) and elem.name == arr_name:
                elem.name = orig_name
    # remove write into temporary
    root_scope.ops.pop(op_idx)
    # remove temporary buffer
    del root_scope.arr_to_buf[arr_name]
    del root_scope.buffers[arr_name]
    # propagate used outputs after the scope tree modification
    root_scope.propagate_used_outputs(root_scope.used_outputs)
