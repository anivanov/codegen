from kelgen.gen import *

def find_creatable_temporaries(root_scope, max_heap_bytes=2**30):
    """
        Temporary can be created for any array.

        Assumptions:
        - all scopes are split
        - no buffer-related transformations happened before, only arrays are considered
    """
    if 'frozen_scopes' in root_scope.concepts:
        return []

    if not check_all_scopes_split(root_scope):
        return []  # only one operation per scope is allowed

    used_heap = compute_used_memory_size(root_scope, "heap")
    candidates = [
        (b,) 
        for b in root_scope.buffers
        if used_heap + root_scope.buffers[b].size_bytes() <= max_heap_bytes
    ]

    return sorted(candidates)


def create_temporary(root_scope, arr_name):
    """
        Temporary is added after the original write operation.
        For inputs, it is added as the first operation.

        Example (applied to x):
        Before:                    After:
        x = h(y)                   x = h(y)
                                   x0 = x
        z = f(x)                   z = f(x0)
        c = g(x)                   c = g(x0)

        Assumptions:
        - all scopes are split
        - no buffer-related transformations happened before, only arrays are considered
    """
    assert (arr_name,) in find_creatable_temporaries(root_scope)
    new_arr_name = generate_new_array_name(root_scope, arr_name)

    # replace all accesses to original array with the temporary
    insertion_idx = 0
    for idx, op in enumerate(recursive_ops(root_scope)):
        for elem in op.inp_elem:
            if isinstance(elem, Array) and elem.name == arr_name:
                elem.name = new_arr_name
        if op.out_elem.name == arr_name:
            if op.out_elem.name in root_scope.used_outputs:
                insertion_idx = len(root_scope.ops)
                op.out_elem.name = new_arr_name
            else:
                insertion_idx = insertion_idx or (idx + 1)
    # also replace all scope dimensions referring to the original array
    for s in recursive_scopes(root_scope):
        s.dim = s.dim.replace_arr_name(arr_name, new_arr_name)
    # add temporary buffer
    orig_buf_name = root_scope.arr_to_buf[arr_name]
    orig_buf = root_scope.buffers[orig_buf_name]
    tmp_buf = copy.deepcopy(orig_buf)
    root_scope.arr_to_buf[new_arr_name] = new_arr_name
    root_scope.buffers[new_arr_name] = tmp_buf
    # if temporary created for output, to keep the naming consistent,
    # we need to swap the names of the original and temporary
    if arr_name in root_scope.used_outputs:
        new_arr_name, arr_name = arr_name, new_arr_name
    # add temporary move operation
    array_idx = list(Index(x) for x in range(len(tmp_buf.dims)-1, -1, -1))
    new_op = Operation(
        Array(new_arr_name, array_idx),
        [Array(arr_name, copy.deepcopy(array_idx))],
        OpDesc(tmp_buf.dtype),
    )
    # wrap move operation in scope nest of required size
    scope_nest = new_op
    for dim in reversed(tmp_buf.dims):
        scope_nest = LoopScope([scope_nest], copy.deepcopy(dim))
    # insert the scope nest after the original write
    root_scope.ops.insert(insertion_idx, scope_nest)
    # propagate used outputs
    root_scope.propagate_used_outputs(root_scope.used_outputs)