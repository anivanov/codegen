from kelgen.gen import *

def find_tileable_scopes(root_scope, minimize_options=True):
    if 'frozen_scopes' in root_scope.concepts:
        return []

    tileable_scopes = []
    scopes = get_scopes(root_scope)

    for scope in scopes:
        if isinstance(scope.dim, ConstElem):
            assert scope.dim.dtype.literal == 'i'  # floating point number of iterations is meaningless, it should be integer
            # find all divisiors of the scope value
            upper_bound = math.floor(math.sqrt(scope.dim.value)) + 1 if minimize_options else scope.dim.value
            divisors = [i for i in range(2, upper_bound) if scope.dim.value % i == 0]
            tileable_scopes.extend((scope.name, tile_size) for tile_size in divisors)
        # TODO: we should consider symbolic products here too M*N

    return tileable_scopes


def tile_scope(root_scope, scope_name, tile_size):
    """
    First step of the tiling process:
    - From a scope of dim M it creates two scopes of dim M/tile_size and tile_size
    - Replaces all array accesses for the scope index an IndexExpr: i -> i*tile_size+j
    """
    assert (scope_name, tile_size) in find_tileable_scopes(root_scope, minimize_options=False),\
        f"Program:\n{root_scope.text()}\nScope {scope_name} cannot be tiled with size {tile_size}, available sizes: {find_tileable_scopes(root_scope)}"

    parent, idx = find_scope_by_name(root_scope, scope_name)
    scope = parent.ops[idx]
    orig_dim = scope.dim


    if isinstance(orig_dim, ConstElem):
        out_dim = ConstElem(orig_dim.value // tile_size, "i32")
    else:
        raise NotImplementedError("Symbolic tiling is not implemented yet")
    in_dim = ConstElem(tile_size, "i32")

    new_scope = LoopScope(scope.ops, in_dim)
    scope.ops = [new_scope]
    scope.dim = out_dim
    tile_array_accesses(new_scope, in_dim)
    new_scope.used_outputs = scope.used_outputs
    
    
def tile_nested_scopes(program, op_name, tiles, silent=True, eval_func=None):
    # process from outermost to innermost
    for inp_idx, scope_tiles in enumerate(tiles):
        target_idx = len(tiles) - inp_idx - 1
        for tile in scope_tiles:
            scope_name = f'{op_name}#{target_idx}'
            tile_scope(program, scope_name, tile)
            if not silent:
                print(f'tile_scope(program, "{scope_name}", {tile})')
            if eval_func is not None:
                eval_func(program)
