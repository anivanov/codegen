from kelgen.gen import *

def find_swappable_ops(program, scope=None):
    """
        Returns the list of scope/op names that can be swapped with the subsequent scope/op.
        The only condition is that there are no data dependencies from the first scope/op to the second.
    """
    if 'frozen_scopes' in program.concepts:
        return []
    if scope is None:
        scope = program
    result = []
    for op_idx, op in enumerate(scope.ops):
        # consider nested scopes
        if isinstance(op, Scope):
            result.extend(find_swappable_ops(program, op))
        # now consider current pair
        if op_idx == len(scope.ops) - 1:
            continue
        next_op = scope.ops[op_idx + 1]
        # check that there are no data dependencies from op to next_op
        inp1, tmp1, out1 = op.get_inp_tmp_out()
        inp2, tmp2, out2 = next_op.get_inp_tmp_out()
        if (tmp1 | out1) & (inp2 | tmp2):
            continue
        result.append((op.name,))
    return result


def swap_ops(program, op_name):
    """
        Swaps a sequential pair of operations/scopes.
        Provided op_name refers to the first operaiton in the pair.
    """
    assert (op_name,) in find_swappable_ops(program)
    parent_scope, scope_idx = find_anything_by_name(program, op_name)
    parent_scope.ops[scope_idx], parent_scope.ops[scope_idx + 1] = (
        parent_scope.ops[scope_idx + 1],
        parent_scope.ops[scope_idx],
    )