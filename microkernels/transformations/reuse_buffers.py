from kelgen.gen import *

def find_reusable_buffers(root_scope):
    """
    Returns set of pairs: 'buffer name for removal' and 'buffer name to keep'.
    The required condition is to have a pair of equivalent buffers that have non-overlapping uses.
    """
    if 'frozen_buffers' in root_scope.concepts:
        return []
    # group equivalent buffers
    buffer_groups = {}
    for buf_name, buf in root_scope.buffers.items():
        if not isinstance(buf, Buffer):
            continue
        buffer_groups.setdefault(buf, set()).add(buf_name)
    # analyze buffers within each group with more than one buffer
    buf_groups = {k: v for k, v in buffer_groups.items() if len(v) > 1}
    accesses_with_scopes = get_accesses_with_scopes(root_scope)

    buf_aliveness = find_live_ranges(root_scope)

    # now try to find reusable buffers
    result = set()
    for buf, buf_names in buf_groups.items():
        for buf1, buf2 in combinations(buf_names, 2):
            arrs1 = root_scope.buf_to_arr(buf1)
            arrs2 = root_scope.buf_to_arr(buf2)
            is_reusable = True
            # check that all arrays have same access dim nesting and same indices
            discovered_scope_nest = None
            discovered_indices = None
            for op_idx, elem_accesses in enumerate(accesses_with_scopes):
                for elem, access_scopes, is_write in elem_accesses:
                    if elem.name in (arrs1 | arrs2):
                        if discovered_scope_nest is None:
                            discovered_scope_nest = access_scopes
                        else:
                            discovered_dim_nest = [s.dim for s in discovered_scope_nest]
                            access_dims = [s.dim for s in access_scopes]
                            # all dimensions should match exactly
                            if discovered_dim_nest != access_dims:
                                is_reusable = False
                            # scope can have matching segment from root to some scope, after which no scopes should be overlapping
                            # access_scopes contains scopes from inner to outer, so the first refers to innermost leaf scope
                            reversed_discovered = list(reversed(discovered_scope_nest))
                            reversed_current = list(reversed(access_scopes))
                            matching_prefix = [x.name for x, y in zip(reversed_discovered, reversed_current) if x.name == y.name]
                            discovered_suffix = [x.name for x in reversed_discovered[len(matching_prefix):]]
                            access_suffix = [x.name for x in reversed_current[len(matching_prefix):]]
                            if set(discovered_suffix) & set(access_suffix):
                                is_reusable = False  # no overlapping scopes allowed
                        if discovered_indices is None:
                            discovered_indices = elem.indices
                        elif (
                            len(discovered_indices) != len(elem.indices)
                            # TODO: this is too strict, more flexible check can allow index mismatches when they refer to different (non-shared) scopes
                            or any(x != y for x, y in zip(discovered_indices, elem.indices))
                        ):
                            is_reusable = False

            # check than aliveness is not overlapping
            first_write1, last_read1 = buf_aliveness[buf1]
            first_write2, last_read2 = buf_aliveness[buf2]
            # this is tricky check to reason about:
            # there are two cases allowed:
            # first_write1 < last_read1 <= first_write2 < last_read2
            # OR
            # first_write2 < last_read2 <= first_write1 < last_read1
            # if both of them are false, then there is an overlap
            if not (last_read1 <= first_write2 or last_read2 <= first_write1):
                is_reusable = False

            if is_reusable:
                result.add((buf1, buf2))
    # clean (a,b) and (b,a) duplicates
    result = {(a, b) if a < b else (b, a) for a, b in result}
    # make order deterministic
    result = sorted(list(result))

    return result


def reuse_buffers(program, removed_buf, reused_buf, check=True):
    if check:
        reusable_list = find_reusable_buffers(program)
        assert (removed_buf, reused_buf) in reusable_list or (
            reused_buf,
            removed_buf,
        ) in reusable_list
    # reattach all arrays referring to removed buffer to reused buffer
    for k, v in program.arr_to_buf.items():
        if v == removed_buf:
            program.arr_to_buf[k] = reused_buf
    # finally remove buffer
    del program.buffers[removed_buf]
    program.concepts.add("frozen_scopes")