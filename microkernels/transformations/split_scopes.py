from kelgen.gen import *

def find_splittable_scopes(root_scope, current_scope=None):
    if "frozen_scopes" in root_scope.concepts:
        return []
    if current_scope is None:
        current_scope = root_scope
    result = []
    for op in current_scope.ops:
        if isinstance(op, Scope):
            result.extend(find_splittable_scopes(root_scope, op))
    if root_scope != current_scope:
        result.extend([(s.name,) for s in current_scope.ops[1:]])
    return result


def split_scopes(root_scope, target_name):
    """ Split scope according to the target point:
        parent_parent
            parent
                op1
                op2
                target
                op3
            
        becomes:
        parent_parent
            parent1
                op1
                op2
            parent2
                target
                op4
    """
    assert (target_name,) in find_splittable_scopes(root_scope)
    
    parent1, idx = find_anything_by_name(root_scope, target_name)
    
    parent_of_parent = [root_scope, *get_scopes_from_root_to_scope(root_scope, parent1)][-2]
    
    parent2 = LoopScope(
        ops=parent1.ops[idx:],
        dim=parent1.dim,
    )
    parent1.ops = parent1.ops[:idx]
    parent_of_parent.ops.insert(parent_of_parent.ops.index(parent1) + 1, parent2)
    
    parent_of_parent.propagate_used_outputs(parent_of_parent.used_outputs)
