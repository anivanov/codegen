from kelgen.gen import *

def find_unrollable_scopes(program, scope=None, max_size=10):
    """
        Returns names of the scopes that can be unrolled.
        The only condition is to have a constant number of iterations.
    """
    if scope is None:
        scope = program
    result = []
    for op in scope.ops:
        if not isinstance(op, Scope):
            continue
        result.extend(find_unrollable_scopes(program, op, max_size))
        if not all(isinstance(x, Operation) for x in op.ops):
            continue  # unrolling is only applicable to last level scopes
        if op.options.get("unroll", False):
            continue  # already unrolled
        if op.options.get("parallel", False):
            continue  # already parallelized
        if op.options.get("vectorize", False):
            continue
        if not isinstance(op.dim, ConstElem):
            continue
        if op.dim.value > max_size:
            continue
        result.append((op.name,))
    return result


def unroll_scope(program, scope_name, max_size=10):
    """
        Marks the scope as unrolled so that codegen will generate independent instance of the body for each iteration.
    """
    assert (scope_name,) in find_unrollable_scopes(program, max_size=max_size)
    parent, idx = find_scope_by_name(program, scope_name)
    scope = parent.ops[idx]
    scope.options["unroll"] = True
    program.concepts.add("frozen_scopes")
