from kelgen.gen import *

def find_activatable_ssr_by_idx(program):
    results = []
    ssr_conf = find_ssr_config(program)
    # overlapping SSR enumeration start
    ssr_overlaps = {
        ssr_idx: set() for ssr_idx in ssr_conf
    }  # ssr_idx: set of overlapping indices
    for ssr_idx1, conf1 in ssr_conf.items():
        for ssr_idx2, conf2 in ssr_conf.items():
            if ssr_idx1 >= ssr_idx2:
                continue
            start_depth1 = (
                len(conf1["repeats"]) - 1 - program.options["ssr"][ssr_idx1]["depth"]
            )
            start_depth2 = (
                len(conf2["repeats"]) - 1 - program.options["ssr"][ssr_idx2]["depth"]
            )
            is_overlapping = False
            rf1 = list(reversed(conf1["first_op"]))
            rl1 = list(reversed(conf1["last_op"]))
            rf2 = list(reversed(conf2["first_op"]))
            rl2 = list(reversed(conf2["last_op"]))
            for i, f1, l1, f2, l2 in zip(count(), rf1, rl1, rf2, rl2):
                if l1 < f2 or l2 < f1:
                    break  # no overlap at this depth, no need to check further
                elif i >= start_depth1 or i >= start_depth2:
                    # there is overlap and both SSR need to be active at this depth
                    # note: the condition is "or" because we need to check common parent
                    is_overlapping = True
            if is_overlapping:
                ssr_overlaps[ssr_idx2].add(ssr_idx1)
                ssr_overlaps[ssr_idx1].add(ssr_idx2)
    # overlapping SSR enumeration end
    for ssr_idx, conf in ssr_conf.items():
        # check that SSR is not already activated
        if "hw_idx" in program.options["ssr"][ssr_idx]:
            continue
        # check that overlapping activated SSRs do not exceed MAX_SSR_REGS
        overlapping_ssrs = {
            program.options["ssr"][x]["hw_idx"]
            for x in ssr_overlaps[ssr_idx]
            if "hw_idx" in program.options["ssr"][x]
        }     
        if len(overlapping_ssrs) >= MAX_SSR_REGS:
            continue
        # return all ids that are not present in overlapping SSRS
        hw_idx_candidates = set(range(MAX_SSR_REGS)) - overlapping_ssrs
        for hw_idx in hw_idx_candidates:
            results.append((ssr_idx, hw_idx))
    return results


def find_activatable_ssr(program):
    results_by_idx = find_activatable_ssr_by_idx(program)
    results = []
    for ssr_idx, hw_idx in results_by_idx:
        elems_with_ssr = find_elems_with_ssr_idx(program, ssr_idx)
        op, access_idx = elems_with_ssr[0]
        results.append((op.name, access_idx, hw_idx))
    return results


def activate_ssr_by_id(program, ssr_idx, hw_idx):
    assert (ssr_idx, hw_idx) in find_activatable_ssr_by_idx(program)
    program.options["ssr"][ssr_idx]["hw_idx"] = hw_idx


def activate_ssr(program, op_name, access_idx, hw_idx):
    elem = find_elem_by_location(program, op_name, access_idx)
    ssr_idx = elem.options["ssr"]
    activate_ssr_by_id(program, ssr_idx, hw_idx)