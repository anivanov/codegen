from kelgen.gen import *

def find_parallelizable_scopes(program, nested_parallelism=False):
    """
        The main constraint for parallelization is dimensions reuse.
    """
    # find all scopes across which dimension reuse is enabled
    # scopes_with_dim_reuse = find_scopes_with_dim_reuse(program)
    
    # scopes from scope nests with at least one parallelized scope should not be parallelized further
    scopes_in_parallel_nests = set()
    for scopes, op in recursive_scopes_with_ops(program):
        if any(s.options.get("parallel", False) for s in scopes):
            scopes_in_parallel_nests |= set(s.name for s in scopes)
    
    # scopes over which reduction is happenning can not be parallelized
    scopes_with_reduction = find_scopes_with_reduction(program)
    
    # now, return all scopes that can be reused
    result = []
    for scope in recursive_scopes(program):
        # allow parallelization over scopes with diemension reuse but make transformation undo dim reuse first
        # if scope.name in scopes_with_dim_reuse:
        #     continue
        if (not nested_parallelism) and (scope.name in scopes_in_parallel_nests):
            continue
        if scope.name in scopes_with_reduction:
            continue
        if scope.options.get("parallel", False):
            continue  # already parallelized
        if scope.options.get("unroll", False):
            continue  # unrolling and parallelization are mutually exclusive
        if scope.options.get("vectorize", False):
            continue
        result.append((scope.name,))
    return result


def parallelize_scope(program, scope_name):
    """
        Marks the scope as parallelizable.
    """
    assert (scope_name,) in find_parallelizable_scopes(program)
    parent_scope, scope_idx = find_scope_by_name(program, scope_name)
    scope = parent_scope.ops[scope_idx]
    scope.options["parallel"] = True
    undo_dim_reuse_for_scope(program, scope_name)
    program.concepts.add("frozen_scopes")
    program.concepts.add("frozen_buffers")