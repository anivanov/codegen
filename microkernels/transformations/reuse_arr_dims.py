from kelgen.gen import *

def find_reusable_arr_dims(root_scope):
    if 'frozen_buffers' in root_scope.concepts:
        return []
    res = find_reusable_arr_dims_impl(root_scope, root_scope)
    # make determenistic order
    return sorted(list(res))


def find_reusable_arr_dims_impl(root_scope, scope):
    """
    Returns set of tuples: arrays and their dimensions that can be reused.
    Return value format: set(tuple(arr_name, dim_idx))
    Example: D3 and D2 shared, but D1 and D0 are not:
        D3 D2 | D1 D0 x[{0}, {1}, {2}, {3}] = y[{0}, {1}, {2}, {3}]
              | D1 D0 z[{0}, {1}, {2}, {3}] = x[{0}, {1}, {2}, {3}]
    Here we can reuse D3 and D2, but not D1 and D0.
    Therefore this transformation can be applied only to the arrays that appear as temporary.
    In this case, reusable dimensions are those that use indices of scopes where arrays are temporary.
    """
    result = set()
    # check current scope
    if isinstance(scope, LoopScope):
        inp, tmp, out = scope.get_inp_tmp_out()
        for arr_name in tmp:
            buf_name = root_scope.arr_to_buf[arr_name]
            # find all related array names that reuse the same buffer
            related_arrs = root_scope.buf_to_arr(buf_name)
            # reject candidate if not all of related arrays in tmp
            if not all(x in tmp for x in related_arrs):
                continue
            # find all operators that access the buffer
            acc_ops = set()
            for an in related_arrs:
                acc_ops |= set(get_accessing_operators(scope, an))
            buf = root_scope.buffers[buf_name]
            # start by assuming that all existing dims can be reused
            reusable_dims = {
                x for x, y in enumerate(buf.used_dims) if y
            }  # don't check dims that are already reused

            # last dimension of buffer with vector type should never be reused
            buf_dtype = buf.dtype
            if buf_dtype.is_vector():
                reusable_dims -= {len(buf.used_dims) - 1}


            for op in acc_ops:
                loop_scope_idx = (
                    len(get_scopes_from_root_to_scope(scope, op)) - 1
                )  # index that refers to current 'scope'
                for elem in op.all_elem:
                    if not isinstance(elem, Array):
                        continue
                    if elem.name not in related_arrs:
                        continue
                    for idx_pos, idx in enumerate(elem.indices):
                        # remove dimension from the list of reusable if it
                        # uses something else than the current loop scope index
                        if not idx.only_pos(loop_scope_idx):
                            reusable_dims -= {idx_pos}
            result |= {(arr_name, d) for d in reusable_dims}
    # check all nested scopes
    for op in scope.ops:
        if isinstance(op, Scope):
            result |= find_reusable_arr_dims_impl(root_scope, op)
    return result


def reuse_arr_dims(program, arr_name, dim_idx):
    """
    This transformation saves memory by avoiding materializing dimensions of arrays.
    For example, in the following scope, array a[i] can be collapsed to a scalar:
        N | a[{0}] = b[{0}]
          | c[{0}] = a[{0}]
    However, it requires a[i] to not be used outside of the scope.
    """
    assert (arr_name, dim_idx) in find_reusable_arr_dims(program)
    buf_name = program.arr_to_buf[arr_name]
    used_dims = program.buffers[buf_name].used_dims
    target = used_dims[dim_idx]
    assert target  # should be in use before
    used_dims[dim_idx] = False
    program.concepts.add("frozen_scopes")