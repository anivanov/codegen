from kelgen.gen import *

def find_tileable_buffers(root_scope):
    #check when tiling again
    inp, tmp, out = root_scope.get_inp_tmp_out()
    tileable_buffers = []
    for buf_name, buf in root_scope.buffers.items():
        arrays = root_scope.buf_to_arr(buf_name)
        if arrays & (inp | out):
            continue  # input and output layouts are frozen by external caller
        if not isinstance(buf, Buffer):
            continue
        for dim_idx, dim in enumerate(buf.dims):
            if not isinstance(dim, ConstElem):
                continue
            if not dim.dtype.literal == 'i':
                continue

            # check that all the scopes have been tiled with the correct size
            tile_size = check_tiling_propagation(root_scope, root_scope, buf_name, dim_idx)
            if not tile_size:
                continue

            tileable_buffers.append((buf_name, dim_idx))

    return tileable_buffers

def tile_buffer(root_scope, buffer_name, dim_idx):
    """
    Second step of the tiling process:
    - Assert that all the scopes where the buffer appears are tiled for the same size
    - Add one dimension to the buffer
    - Replace the tiling index expressions with two separate index: i*N+j -> i, j
    """
    assert (buffer_name, dim_idx) in find_tileable_buffers(root_scope)

    tile_size = check_tiling_propagation(root_scope, root_scope, buffer_name, dim_idx)
    assert tile_size != 0
    #add new dimension
    orig_dim = root_scope.buffers[buffer_name].dims[dim_idx]

    out_dim, in_dim = tile_dim(orig_dim, tile_size)

    root_scope.buffers[buffer_name] = tile_array_dim(root_scope.buffers[buffer_name], dim_idx, out_dim, in_dim)

    #replace all accesses -> all the scopes have already been tiled
    #{1} * <tile_size> + {0} -> {1}, {0}

    propagate_tiling(root_scope, root_scope, buffer_name, dim_idx)
