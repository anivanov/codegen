from kelgen.gen import *

def find_bufs_movable_to_stack(root_scope, max_size=128):
    return find_bufs_movable_to_memory(root_scope, max_size, "stack")


def move_buf_to_stack(root_scope, buf_name):
    root_scope.concepts.add("data_locality")
    root_scope.internal_bufs[buf_name].allocation = "stack"