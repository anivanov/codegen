from kelgen.gen import *

def find_increasable_ssr_depth_by_idx(program):
    """
    We are checking if there is a loop scope that can receive extended depth.
    After extending the depth, this transformation needs to check that
    the number of dimensions doesn't exceed MAX_SSR_DEPTH=4.
    Lastly, make sure it is not participating in
    read-after-write or write-after-write happening across the depth of the scope.
    """
    ssr_config = find_ssr_config(program)
    result = []
    for ssr_idx, conf in ssr_config.items():
        if "hw_idx" in program.options["ssr"][ssr_idx]:
            continue  # SSR already mapped to HW register
        cur_depth = program.options["ssr"][ssr_idx]["depth"]
        repeat_dims = sum((r != 1) for r in conf["repeats"])
        if cur_depth + repeat_dims == MAX_SSR_DEPTH:
            continue  # SSR can't handle larger depth
        if cur_depth == len(conf["dims"]):
            continue  # no more dimensions to add to the SSR
        # check that SSR is not participating in read-after-write or write-after-write
        if has_raw_or_waw_ssr(program, ssr_idx, conf, cur_depth + 1):
            continue
        result.append((ssr_idx,))
    return result


def find_increasable_ssr_depth(program):
    ssr_indices = find_increasable_ssr_depth_by_idx(program)
    result = []
    for (ssr_idx,) in ssr_indices:
        elems_with_ssr = find_elems_with_ssr_idx(program, ssr_idx)
        op, access_idx = elems_with_ssr[0]
        result.append((op.name, access_idx))
    return result


def increase_ssr_depth_by_idx(program, ssr_idx):
    assert (ssr_idx,) in find_increasable_ssr_depth_by_idx(program)
    program.options["ssr"][ssr_idx]["depth"] += 1


def increase_ssr_depth(program, op_name, access_idx):
    scope, idx = find_op_by_name(program, op_name)
    op = scope.ops[idx]
    elem = op.all_elem[access_idx]
    ssr_idx = elem.options["ssr"]
    increase_ssr_depth_by_idx(program, ssr_idx)