from kelgen.gen import *

def find_applicable_frep(program, scope=None):
    """
    FREP scope relies on the following conditions:
    * last levels indices are not used anywhere inside the scope body (this usually means that SSR is used to do accesses)
    * this scope either contains only Operations or it contains a single LoopScope that recursively satisfies these properties
    * scope body (with potential unrolling) has less than max_ops=15 operations (HW limitation)
    * scope body doesn't contain sqrt or div operations (HW limitation)
    """
    scope = scope or program

    #if current scope has frep set, no need to check deeper
    if isinstance(scope, Scope) and scope.options.get("frep", False):
        return set()

    result = set()

    # check if current scope only has operations and satisfies FREP conditions
    if is_frep_applicable_to_scope(program, scope):
        result.add((scope.name,))

    # process nested scopes
    for op in scope.ops:
        if isinstance(op, Scope):
            result |= set(find_applicable_frep(program, op))

    # handle special case of a loop nest that can be entirely replaced by FREP. it can happen in two cases
    # - child of current is unrolled and its operations satisfy FREP conditions
    # - child of current is another scope that satisfy FREP conditions and discovered above
    if len(scope.ops) == 1 and isinstance(scope.ops[0], LoopScope) and ((scope.ops[0].name,) in result or scope.ops[0].options.get("unroll", False)):        
        # the current scope can use FREP if it additionally satisfies the following conditions:
        # - has no dependensies on the indices of current scope
        # - has no reductions to be innitialized below the current scope
        # - has no activated SSR accesses to be initialized below the current scope
        child_of_current = scope.ops[0]
        
        if child_of_current.options.get("unroll", False):
            unrolled_ops = get_unrolled_body(program, scope.ops[0])
            if not is_frep_applicable_to_ops(program, unrolled_ops):
                return result  # reject
        
        innermost_scope = child_of_current
        innermost_distance = 1
        while isinstance(innermost_scope.ops[0], Scope):
            innermost_scope = innermost_scope.ops[0]
            innermost_distance += 1
        
        for op in innermost_scope.ops:
            if not is_index_safe(program, op, innermost_distance):
                return result  # reject
        
        if get_reduce_allocation(program, child_of_current):
            return result  # reject
        
        ssr_conf = find_ssr_config(scope)
        for ssr_idx, val in ssr_conf.items():
            ssr_depth = program.options["ssr"][ssr_idx]["depth"]
            if "hw_idx" not in program.options["ssr"][ssr_idx]:
                continue  # SSR is not activated, it causes no issues
            # len(val["dims"]) gives the number of dimensions from the most nested to 'child_of_current' 
            # if it exactly matches number of dimensions discovered in SSR cofiguration,
            # SSR will need to be configured right before entering the child_of_current
            if ssr_depth == len(val["dims"]):
                return result  # reject
        
        result.add((scope.name,))

    return sorted(result)


def enable_frep(program, scope_name):
    assert (scope_name,) in find_applicable_frep(program)
    parent, idx = find_scope_by_name(program, scope_name)
    scope = parent.ops[idx]
    scope.options["frep"] = True