from kelgen.gen import *

def find_creatable_dimensions(root_scope, max_heap_bytes=2**30):
    # verify assumptions and early exit
    if not check_all_scopes_split(root_scope):
        return []

    if not check_no_reused_buffers(root_scope):
        return []
    
    if not check_no_unrolled_scopes(root_scope):
        return []
    
    result = []

    flat_list = get_flattened_ops_with_scopes_textual(root_scope)

    inp, tmp, out = root_scope.get_inp_tmp_out()

    # ------ analyze all read-to-write cases

    # for each read, find broadcast dimensions
    read_bcast_dims = {}  # array_name -> set(bcast_dims)
    for scopes, op in flat_list:
        for inp_elem in op.inp_elem:
            if not isinstance(inp_elem, Array):
                continue  # transformation applicable only to arrays
            if inp_elem.name in inp:
                continue  # layout of input is frozen
            bcast_indices = set(range(len(scopes))) - inp_elem.accessed_pos()
            bcast_dims = {scopes[-unused_idx-1].dim for unused_idx in sorted(bcast_indices)}
            if inp_elem.name not in read_bcast_dims:
                read_bcast_dims[inp_elem.name] = bcast_dims
            else:
                read_bcast_dims[inp_elem.name] &= bcast_dims

    for scopes, op in flat_list:
        if op.out_elem.name in out:
            continue  # layout of output is frozen
        if not read_bcast_dims.get(op.out_elem.name, set()):
            continue  # there should be at least one bcast dimension to propagate
        max_new_dim = max(const_elem.value for const_elem in read_bcast_dims[op.out_elem.name])
        # do not add dimension if it increases amount of used memory beyond the limit
        existing_buf = root_scope.buffers[root_scope.arr_to_buf[op.out_elem.name]]
        expected_buf = Buffer(existing_buf.dims + [max_new_dim], existing_buf.dtype)
        expected_size = compute_used_memory_size(root_scope, 'heap', {op.out_elem.name: expected_buf})
        if expected_size > max_heap_bytes:
            continue  # skip if memory limit is exceeded
        result.append((op.out_elem.name, 0))

    # ------ analyze all write-to-read cases

    # write-to-read starts from reductions, collect all such writes with their type
    write_reduce_dims = {}  # array_name -> (set(reduce_dims), reduce_type)
    for scopes, op in flat_list:
        if op.out_elem.name in out:
            continue  # layout of output is frozen
        if not op.op.reduce:
            continue  # there has to be reduction
        reduce_scopes = set(range(len(scopes))) - op.out_elem.accessed_pos()
        reduce_dims = {scopes[-reduce_idx-1].dim for reduce_idx in sorted(reduce_scopes)}
        write_reduce_dims[op.out_elem.name] = (reduce_dims, op.op)

    # now, for each read, make sure reduction can be propagated
    # there can be multiple reads and each of them should be applicable
    candidates = set()  # array names
    disabled_candidates = set()
    for scopes, op in flat_list:
        for inp_elem in op.inp_elem:
            if not isinstance(inp_elem, Array):
                continue  # transformation applicable only to arrays
            if inp_elem.name not in write_reduce_dims:
                continue  # no reduction to propagate
            reduce_dims, op_desc = write_reduce_dims[inp_elem.name]
            if op.op.func != '{0}':
                # reduction can't propagate through function, only plain assignments are allowed
                # if we detect non-plain read for current op, other plain reads candidates should be rejected
                disabled_candidates.add(inp_elem.name)
                continue
            if op.op.reduce and op.op.reduce != op_desc.reduce:
                # if some reduction is already present,
                # make sure it matches the reduction to be propagated
                disabled_candidates.add(inp_elem.name)
                continue
            # do not add dimension if it increases amount of used memory beyond the limit
            existing_buf = root_scope.buffers[root_scope.arr_to_buf[inp_elem.name]]
            max_reduce_dim = max(const_elem.value for const_elem in reduce_dims)
            expected_buf = Buffer(existing_buf.dims + [max_reduce_dim], existing_buf.dtype)
            expected_size = compute_used_memory_size(root_scope, 'heap', {inp_elem.name: expected_buf})
            if expected_size > max_heap_bytes:
                continue  # skip if memory limit is exceeded
            candidates.add(inp_elem.name)

    candidates -= disabled_candidates

    # now, for each array, find the first read and add it to the result
    for scopes, op in flat_list:
        for elem_idx, elem in enumerate(op.inp_elem):
            if isinstance(elem, Array) and elem.name in candidates:
                result.append((op.name, elem_idx + 1))
                candidates.remove(elem.name)  # to avoid duplication we add only the first occurence

    return result


def create_dimension(root_scope, op_name, access_idx):
    """
    Adds additional scope of iteration to the write operation.
    
    Transformation example (read to write, write is target):
    Before:                    After:
    M   x[m]=z[m]              M N x[m,n]=z[m]
    M N y[m,n]=x[m]            M N y[m,n]=x[m,n]
    
    Transformation example (write to read with reduction propagation, read is target):
    Before:                    After:
    M N x[m]+=y[m,n]           M N x[m,n]=y[m,n]
    M   z[m]=x[m]              M N z[m]+=x[m,n]

    Ambiguious case in which both ways (read to write, write to read) are available.
    To distiguish between them, the transformation should be applied to the location that requires
    additional scope to be added (write or read target).
    Before:           After (read to write):      After (write to read):
    N x+=y[{0}]       N N x[{0}]+=y[{1}]          N   x[{0}]=y[{0}]
    N z[{0}]=x        N   z[{0}]=x[{0}]           N N c[{1}]+=x[{0}]
    N c[{0}]=x        N   c[{0}]=x[{0}]           N N c[{1}]+=x[{0}]

    Location:
    The location of applying transformation is uniquely identified by the array access (x[n]) 
    specified by the op_name and access_idx. If such access is read, then the transformation 
    type is read-to-write, otherwise transformation is write-to-read. If more than one scope
    dimension are valid for the transformation, the innermost (closest to operation) occurence
    of the scope is chosen. When multiple candidates are available in the write-to-read case,
    the innermost scope relative to the first read is chosen.
    Example: outermost scope of the second operation is selected because innermost is not applicable.
        N   x[{0}]=z[{0}]        N N x[{1},{0}]=z[{1}]    
        N N y[{1},{0}]=x[{0}]    N N y[{1},{0}]=x[{1},{0}]
    
    Assumptions:
    - There are no buffer-related transformations happened before, 
      so each array has 1-to-1 mapping to the buffer and all buffer dimensions are materialized.
    - There are no joined scopes, so each scope contains exactly one scope or operation.

    Reachability:
    - Requires swap_nested_scopes to be executed before and after swap_nested_scopes to ensure
    flexibility of specifying which scope dimension will be added.

    Requirements:
    - All reads should be enclosed in the scope equivalent to the referenced scope.
    Example: not applicable because there is no scopes enclosing all read accesses
        M   x[m]=z[m]
        M N y[m,n]=x[m]
        M   t[m]=x[m]  # scope N is needed
    """
    assert (op_name, access_idx) in find_creatable_dimensions(root_scope, max_heap_bytes=float('inf'))
    
    tgt_parent, tgt_idx = find_op_by_name(root_scope, op_name)
    tgt_op = tgt_parent.ops[tgt_idx]
    tgt_elem = tgt_op.all_elem[access_idx]
    assert isinstance(tgt_elem, Array)

    flat_list = get_flattened_ops_with_scopes_textual(root_scope)
    [(write_op, write_scopes)] = [(op, scopes) for scopes, op in flat_list if op.out_elem.name == tgt_elem.name]
    read_op_scopes = [(scopes, op) for scopes, op in flat_list if any(isinstance(e, Array) and e.name == tgt_elem.name for e in op.inp_elem)]

    if access_idx == 0:
        # read to write, write is target

        # for each read, add operation and scopes that are enclosing it
        read_elems = []
        for scopes, op in read_op_scopes:
            for e in op.inp_elem:
                if isinstance(e, Array) and e.name == tgt_elem.name:
                    read_elems.append((e, op, scopes))

        # now, process every read and find scopes that are not using it
        common_unused_dims = None
        for elem, op, scopes in read_elems:
            unused_indices = set(range(len(scopes))) - elem.accessed_pos()
            # keep in the list of process from innermost to outermost (to guarantee deterministic order)
            unused_dims = [scopes[-unused_idx-1].dim for unused_idx in sorted(unused_indices)]
            if common_unused_dims is None:
                # first read, initialize unused_dims
                common_unused_dims = unused_dims
            else:
                # narrow with dimensions in unused in the subsequent reads
                unused_dims_set = set(common_unused_dims) & set(unused_dims)
                common_unused_dims = [dim for dim in common_unused_dims if dim in unused_dims_set]

        # select first (innermost) unused dimension
        unused_dim = common_unused_dims[0]

        # create new innermost scope, wrap the write into it, and increment references to all outer scopes
        new_op = copy.deepcopy(tgt_op)
        tgt_parent.ops[tgt_idx] = LoopScope([new_op], copy.deepcopy(unused_dim))
        new_op.all_elem = [elem.shift_index(+1) for elem in new_op.all_elem]
        new_op.out_elem = Array(new_op.out_elem.name, new_op.out_elem.indices + [Index(0)])

        # for all the reads, add the reference to the innermost associated dimension
        for scopes, op in read_op_scopes:
            new_inp_elems = []
            for elem in op.inp_elem:
                if isinstance(elem, Array) and elem.name == tgt_elem.name:                    
                    [*other, scope_idx] = [scope_idx for scope_idx, scope in enumerate(scopes) if scope.dim == unused_dim]
                    inverted_idx = len(scopes) - scope_idx - 1
                    new_elem = Array(elem.name, elem.indices + [Index(inverted_idx)])
                    new_elem.options = copy.deepcopy(elem.options)
                    new_inp_elems.append(new_elem)
                else:
                    new_inp_elems.append(copy.deepcopy(elem))
            op.inp_elem = new_inp_elems

    else:
        # write to read, read is target
        
        # find the innermost reduction dimension in the write op
        unused_indices = sorted(set(range(len(write_scopes))) - write_op.out_elem.accessed_pos())
        usunused_dims = [write_scopes[-unused_idx-1].dim for unused_idx in unused_indices]
        usused_write_idx = unused_indices[0]
        unused_dim = usunused_dims[0]

        # add dimension to the write access
        orig_write_elem = write_op.out_elem
        write_op.out_elem = Array(write_op.out_elem.name, write_op.out_elem.indices + [Index(usused_write_idx)])
        write_op.out_elem.options = copy.deepcopy(orig_write_elem.options)

        # wrap every read into the new scope and append the reference to the new dimension 
        for scopes, op in read_op_scopes:
            # add additional dim to array access
            new_elems = []
            new_op = copy.deepcopy(op)
            for elem in new_op.all_elem:
                new_elem_shifted = elem.shift_index(+1)
                if isinstance(elem, Array) and elem.name == tgt_elem.name:
                    new_elem = Array(elem.name, new_elem_shifted.indices + [Index(0)])
                    new_elem.options = copy.deepcopy(elem.options)
                    new_elems.append(new_elem)
                else:
                    new_elems.append(new_elem_shifted)
            new_op.all_elem = new_elems

            # propagate reduction if there is no reduction already
            if not new_op.op.reduce:
                assert new_op.op.is_move()
                assert write_op.op.reduce
                new_op.op.reduce = write_op.op.reduce

            # add additional scope to the read operation
            parent_scope = [root_scope, *scopes][-1]
            op_idx = parent_scope.ops.index(op)
            parent_scope.ops[op_idx] = LoopScope([new_op], copy.deepcopy(unused_dim))

        # optionally remove reduction from the write operation
        if len(unused_indices) == 1:
            write_op.op.reduce = ""
    
    # add dimension to the buffer
    buf_name = root_scope.arr_to_buf[tgt_elem.name]
    buf = root_scope.buffers[buf_name]
    buf.dims.append(copy.deepcopy(unused_dim))
    buf.used_dims.append(True)

    # after scope tree modification, we need to propagate used outputs
    root_scope.propagate_used_outputs(root_scope.used_outputs)