from kelgen.gen import *

def find_mergeable_ssr_by_idx(program):
    ssr_config = find_ssr_config(program)
    # do not consider SSR's that are already mapped to HW registers
    ssr_config = {
        k: v for k, v in ssr_config.items() if "hw_idx" not in program.options["ssr"][k]
    }
    result = []
    # now we need to consider pairs of SSRs to see if they can be merged
    for ssr_idx1, conf1 in ssr_config.items():
        for ssr_idx2, conf2 in ssr_config.items():
            if ssr_idx1 >= ssr_idx2:
                continue
            depth1 = program.options["ssr"][ssr_idx1]["depth"]
            depth2 = program.options["ssr"][ssr_idx2]["depth"]
            if depth1 != depth2:
                continue
            if conf1["dims"] != conf2["dims"]:
                continue
            if len(conf1["repeats"]) != len(conf2["repeats"]):
                continue
            if conf1["is_write"] != conf2["is_write"]:
                continue
            num_repeats = len(conf1["repeats"])
            # configurations should have common path from root to the SSR depth
            # so that first_op and last_op should match until common depth is reached
            # otherwise, their configuration happens in different subscopes and not mergeable
            no_common_root = False
            for rep_idx in range(num_repeats - 1, depth1, -1):
                if conf1["first_op"][rep_idx] != conf2["first_op"][rep_idx]:
                    no_common_root = True
            if no_common_root:
                continue
            # merging supposed to happen over the first dimension that has diverging first_op, last_op
            merge_dim = None
            for rep_idx in range(num_repeats - 1, -1, -1):
                if (
                    conf1["first_op"][rep_idx] != conf2["first_op"][rep_idx]
                    or conf1["last_op"][rep_idx] != conf2["last_op"][rep_idx]
                ):
                    merge_dim = rep_idx
                    break
            if merge_dim is None:
                # this is rare case when SSRs belong to the same operation
                # this implies they are both reads (either from same or different arrays)
                assert not conf1["is_write"]
                # we only consider merging a pair of single-instruction SSRs
                # conf_s should be limited to one operation (first_op == last_op, repeats=1)
                if conf1["first_op"] != conf1["last_op"] or conf1["repeats"] != [1] * num_repeats:
                    continue
                if conf2["first_op"] != conf2["last_op"] or conf2["repeats"] != [1] * num_repeats:
                    continue
                if not conf1["elem_start"].is_same(program, conf2["elem_start"]):
                    continue
                result.append((ssr_idx1, ssr_idx2))
                continue  # finish handling this special case here
            # scopes of candidate configurations should not overlap at merge_dim
            # otherwise it would accept configurations such as X, Y, X, Y
            # where both X belong to the conf1 and both Y belong to the conf2
            # this is required as the scope nesting structure of 
            # the first X Y pair may be different from the second X Y pair
            if (
                conf1["first_op"][merge_dim] <= conf2["last_op"][merge_dim] and
                conf2["first_op"][merge_dim] <= conf1["last_op"][merge_dim]
            ):
                continue
            # all repeats should match except over the merge dimension
            repeats_matching = True
            for i, (x, y) in enumerate(zip(conf1["repeats"], conf2["repeats"])):
                if i == merge_dim:
                    continue
                if x != y:
                    repeats_matching = False
            if not repeats_matching:
                continue
            # check that adding repetition dim will not exceed MAX_SSR_DEPTH
            repeat_dims = sum(
                (r != 1 or i == merge_dim) for i, r in enumerate(conf1["repeats"])
            )
            if depth1 + repeat_dims > MAX_SSR_DEPTH:
                continue
            # access indices should match otherwise we can't merge
            # (in case we want more precise check, we can allow matches
            # up to the current SSR depth, but then this will also require checking added dimensions
            # in increase_ssr_depth. for now we allow only full matches)
            if conf1["elem_start"].indices != conf2["elem_start"].indices:
                continue
            # check if we access different buffers
            buf_start1 = program.arr_to_buf[conf1["elem_start"].name]
            buf_start2 = program.arr_to_buf[conf2["elem_start"].name]
            if buf_start1 != buf_start2:
                # we can merge iteration over different arrays only once
                # therefore there can't be rep_offsets different from the elem_start
                if not all(conf1["rep_offsets"][dim].is_same(program, conf1["elem_start"]) for dim in range(num_repeats)):
                    continue
                if not all(conf2["rep_offsets"][dim].is_same(program, conf2["elem_start"]) for dim in range(num_repeats)):
                    continue
                # number of repeats over the merge dimension has to be 1
                if conf1["repeats"][merge_dim] != 1 or conf2["repeats"][merge_dim] != 1:
                    continue
                # number of repeats over non-merge dimensions should match exactly
                if conf1["repeats"] != conf2["repeats"]:
                    continue
                # the layout of buffers (materialized dimensions) should match
                if program.buffers[buf_start1].used_dims != program.buffers[buf_start2].used_dims:
                    continue
            else:
                # if we are doing iteration over the same array,
                # we need to check that offsets match (from potential earlier merge over different arrays)
                if not all(conf1["rep_offsets"][d].is_same(program, conf2["rep_offsets"][d]) for d in range(num_repeats)):
                    continue
                # while repeats over the merge dimension can be different, all other dimensions should have matching repetitions
                if any(conf1["repeats"][dim] != conf2["repeats"][dim] for dim in range(num_repeats) if dim != merge_dim):
                    continue
            # there should be no read-after-write or write-after-write dependencies in the widened SSR range after merging
            # it is irrelevant if candidates for merging refer to the same buffer or not
            if has_raw_or_waw_ssr_range(program, ssr_idx1, conf1, ssr_idx2, conf2, depth1):
                continue
            # at this point this pair should be mergeable
            result.append((ssr_idx1, ssr_idx2))
    return result


def find_mergeable_ssr(program):
    ssr_indices = find_mergeable_ssr_by_idx(program)
    result = []
    for ssr_idx1, ssr_idx2 in ssr_indices:
        elems_with_ssr1 = find_elems_with_ssr_idx(program, ssr_idx1)
        op1, access_idx1 = elems_with_ssr1[0]
        elems_with_ssr2 = find_elems_with_ssr_idx(program, ssr_idx2)
        op2, access_idx2 = elems_with_ssr2[0]
        # sort for deterministic result
        if (op1.name, access_idx1) > (op2.name, access_idx2):
            (op1, access_idx1), (op2, access_idx2) = (op2, access_idx2), (op1, access_idx1)
        result.append((op1.name, access_idx1, op2.name, access_idx2))
    return result


def merge_ssr_by_idx(program, ssr1_idx, ssr2_idx, check=True):
    if check:
        assert tuple(sorted((ssr1_idx, ssr2_idx))) in find_mergeable_ssr_by_idx(program)
    ssr2_elems = find_elems_with_ssr_idx(program, ssr2_idx)
    for op, elem_idx in ssr2_elems:
        elem = op.all_elem[elem_idx]
        elem.options["ssr"] = ssr1_idx
    program.options["ssr"].pop(ssr2_idx)


def merge_ssr(program, op1, acc1, op2, acc2, check=True):
    scope1, idx1 = find_op_by_name(program, op1)
    op1 = scope1.ops[idx1]
    elem1 = op1.all_elem[acc1]
    ssr1_idx = elem1.options["ssr"]

    scope2, idx2 = find_op_by_name(program, op2)
    op2 = scope2.ops[idx2]
    elem2 = op2.all_elem[acc2]
    ssr2_idx = elem2.options["ssr"]

    merge_ssr_by_idx(program, ssr1_idx, ssr2_idx, check)