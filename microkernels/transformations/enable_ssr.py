from kelgen.gen import *

def find_applicable_ssr(program, scope=None):
    """
    We say that SSR is applicable to certain Array if:
        * it is referring to the array
        * it is not already marked as SSR
        * it is accessed inside at least a single loop scope with
            iteration over the array being translated to memory accesses
        * it is not reduced into (SSR can only support read or write, but not both)
        * it is not participating in read-after-write or write-after-write happening in the current scope
    Return list [op_name, access_idx]
    """
    if scope is None:
        scope = program
    result = []
    for op_idx, op in enumerate(scope.ops):
        if isinstance(op, Operation):
            for elem_idx, elem in enumerate(op.all_elem):
                if not isinstance(elem, Array):
                    continue
                if "ssr" in elem.options:
                    continue
                if op.op.reduce and elem_idx == 0:
                    continue
                if elem.get_dtype(program) != "f64":
                    continue  # (support for f32 vectors is possible but not implemented)
                if elem_idx == 0:
                    # write: check reads later
                    if has_accesses_to_same_buf(
                        program,
                        [scope],
                        op,
                        elem_idx,
                        check_reads=True,
                        check_later=True,
                    ):
                        continue
                    # write: check writes earlier and later
                    if has_accesses_to_same_buf(
                        program,
                        [scope],
                        op,
                        elem_idx,
                        check_writes=True,
                        check_earlier=True,
                        check_later=True,
                    ):
                        continue
                else:
                    # read: check writes earlier
                    if has_accesses_to_same_buf(
                        program,
                        [scope],
                        op,
                        elem_idx,
                        check_writes=True,
                        check_earlier=True,
                    ):
                        continue
                buf_name = program.arr_to_buf[elem.name]
                buf = program.buffers[buf_name]
                if not isinstance(buf, Buffer):
                    continue
                if all(not used for used in buf.used_dims):
                    # WARNING: This condition is not really checking that
                    # array is iterated in the loop, but could be a good first approximation.
                    # Actually, iteration is not necessary, as it can be just a repeat in the innermost loop.
                    # Also, indices should be checked to make sure they form a linear expression.
                    continue
                result.append((op.name, elem_idx))
        elif isinstance(op, Scope):
            result.extend(find_applicable_ssr(program, op))
        else:
            raise ValueError("Unknown op type")
    return result


def enable_ssr(program, op_name, access_idx):
    assert (op_name, access_idx) in find_applicable_ssr(program)
    elem = find_elem_by_location(program, op_name, access_idx)
    ssr_idx = program.options.get("max_ssr_idx", 0)
    program.options["max_ssr_idx"] = ssr_idx + 1
    program.options.setdefault("ssr", {})[ssr_idx] = {
        "depth": 1,
    }
    elem.options["ssr"] = ssr_idx