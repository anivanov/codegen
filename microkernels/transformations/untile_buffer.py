from kelgen.gen import *

def find_untileable_buffers(root_scope):
    if 'frozen_buffers' in root_scope.concepts:
        return []
    untileable_buffers = []
    inp, tmp, out = root_scope.get_inp_tmp_out()
    for buf_name, buf in root_scope.buffers.items():
        if buf_name in inp | out:
            continue
        for dim_idx, (dim_usage, next_dim_usage) in enumerate(zip(buf.used_dims[:-1], buf.used_dims[1:])):
            # both current and next dim should be used or not simultaneously
            if dim_usage != next_dim_usage:
                continue
            untileable_buffers.append((buf_name, dim_idx))
    return untileable_buffers

def untile_buffer(root_scope, buffer_name, dim_idx):
    """
    dim_idx refers to the outer dimension to which it is applied
    buffer_name[..., M, N, ...] -> buffer_name[..., M*N, ...]
                     ^--dim_idx

    Behavior:
    - Replace two dimensions M and N with a single M*N dimension
    - Replaces all array accesses for the buffer with an IndexExpr: i, j -> i*N+j
    """
    assert (buffer_name, dim_idx) in find_untileable_buffers(root_scope)

    orig_buf = root_scope.buffers[buffer_name]
    if isinstance(orig_buf.dims[dim_idx], ConstElem) and isinstance(orig_buf.dims[dim_idx + 1], ConstElem):
        new_dim = ConstElem(orig_buf.dims[dim_idx].value * orig_buf.dims[dim_idx + 1].value, orig_buf.dims[dim_idx].dtype)
    else:
        new_dim = IndexExpr(orig_buf.dims[dim_idx], "*", orig_buf.dims[dim_idx +1])
    root_scope.buffers[buffer_name] = untile_buffer_dim(orig_buf, dim_idx, new_dim)
    propagate_untiling(root_scope, root_scope, buffer_name, dim_idx, orig_buf.dims[dim_idx +1])
    