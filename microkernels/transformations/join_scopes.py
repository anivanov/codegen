from kelgen.gen import *

def find_joinable_scopes(root_scope, container_scope=None):
    """Returns list of scope names that can be joined with subsequent scopes"""
    if "frozen_scopes" in root_scope.concepts:
        return []
    if container_scope is None:
        container_scope = root_scope
    result = []
    # check current scope
    for scope0, scope1 in zip(container_scope.ops[:-1], container_scope.ops[1:]):
        if not isinstance(scope0, Scope) or not isinstance(scope1, Scope):
            continue
        if not scope0.dim.is_same(root_scope, scope1.dim):
            continue
        # TODO: check for data dependencies, some examples to consider:
        # OK:  N a[i] = b[i]
        #      N c[i] = a[i]
        # BAD: N a += b[i]     # can't join because of the reduction
        #      N c[i] = a * d[i]
        # BAD: N a[i] = b[i]     # loop-carried dependency
        #      N c[i] = a[i + 1]
        # OK:  B N a[b] += c[b,n] (only OK once at dim B, but not at dim N)
        #      B N d[b,n] = a[b]
        # BAD: N B a[b] += c[b,n] (only OK once at dim B, but not at dim N)
        #      N B d[b,n] = a[b]
        # BAD: 2 2 a[i, j] = ...
        #      2 2 ... = a[j, i]
        # need to match output and input volume in target scopes

        fusion_possible = True
        # if two innermost scopes have different out dtypes 
        # (one is scalar, the other vector) then fusion is impossible
        if is_innermost_loop(scope0) and is_innermost_loop(scope1):
            if any_vector_operands_present(root_scope, scope0) != any_vector_operands_present(root_scope, scope1):
                fusion_possible = False
        _, _, out = scope0.get_inp_tmp_out()
        inp, _, _ = scope1.get_inp_tmp_out()
        for elem in inp & out:
            # we allow fusion if all properties are satisfied
            #   - access patterns are identical
            #   - no reduction dimensions in scopes of current or above levels
            ops0 = get_accessing_operators(scope0, elem)
            ops0 = [
                op for op in ops0 if op.out_elem.name == elem
            ]  # we don't care about read accesses in the scope0
            [
                write_op
            ] = ops0  # there should be a single elem because of single-assignment property
            write_elem = write_op.out_elem

            # find all reads of the same element in scope1
            ops1 = get_accessing_operators(scope1, elem)
            read_elems_with_ops = [(e, op) for op in ops1 for e in op.inp_elem if isinstance(e, Array) and e.name == elem]
            
            # make sure that write and all reads share the same access pattern
            write_elem_scopes = get_scopes_from_root_to_scope(root_scope, write_op)[:-1]  # drop op itself
            scope0_index_depth = len(write_elem_scopes) - write_elem_scopes.index(scope0) - 1
            write_accessed_pos_idx = [(pos, idx) for pos, idx in enumerate(write_elem.indices) if scope0_index_depth in idx.accessed_pos()]

            for read_elem, read_elem_op in read_elems_with_ops:
                if len(read_elem.indices) != len(write_elem.indices):
                    fusion_possible = False
                    break
                # indices may refer to differently ordered dimensions, we need to make sure that they are the same
                # indices referring to the fused dimension should be present at the same location in both read and write accesses
                read_elem_scopes = get_scopes_from_root_to_scope(root_scope, read_elem_op)[:-1]  # drop op itself
                scope1_index_depth = len(read_elem_scopes) - read_elem_scopes.index(scope1) - 1
                read_accessed_pos_idx = [(pos, idx) for pos, idx in enumerate(read_elem.indices) if scope1_index_depth in idx.accessed_pos()]
                                
                # accessed positions should be the same but actual indices may be different
                if [pos for pos, _ in read_accessed_pos_idx] != [pos for pos, _ in write_accessed_pos_idx]:
                    fusion_possible = False
                    break
                
                # accessed indices should be simple Index instances
                # in general case, index can be a complex expression, which is not supported at the moment
                rw_accessed_indices = [idx for _, idx in read_accessed_pos_idx] + [idx for _, idx in write_accessed_pos_idx]
                if not all(isinstance(idx, Index) for idx in rw_accessed_indices):
                    fusion_possible = False
                    break
                
            # now break from the outer loop if already discarded
            if not fusion_possible:
                break

            # check for reduction dimensions
            if write_op.op.reduce:
                nested_write_dims = get_dims_from_root_to_scope(scope0, write_op)
                dims_to_op = len(nested_write_dims)
                # if write_elem_dims doesn't contain all reduction dimensions, then we can't fuse, because some reduces happen in current (scope0) or above scopes
                reduce_indices, _ = get_reduce_indices(write_op)
                if not all(d < dims_to_op for d in reduce_indices):
                    fusion_possible = False
                    break

        if not fusion_possible:
            continue

        result.append((scope0.name,))
    # check all nested scopes
    for op in container_scope.ops:
        if isinstance(op, Scope):
            result.extend(find_joinable_scopes(root_scope, op))
    return result


def join_scopes(root_scope, target_scope_name, check=True):
    if check:
        assert (target_scope_name,) in find_joinable_scopes(root_scope)

    # join target scope with the subsequent scope
    parent_scope, idx = find_scope_by_name(root_scope, target_scope_name)

    curr_scope = parent_scope.ops[idx]
    next_scope = parent_scope.ops[idx + 1]

    new_scope = LoopScope(
        ops=curr_scope.ops + next_scope.ops,
        dim=curr_scope.dim,
    )

    parent_scope.ops[idx] = new_scope
    parent_scope.ops.pop(idx + 1)

    parent_scope.propagate_used_outputs(parent_scope.used_outputs)