from kelgen.gen import *

def find_vectorizable_loops(program: RootScope, simd: str) -> List[str]:
    """
    simd is a string that specifies target vector extension to use:
    e.g. "sse", "avx", "avx512", "neon"
    """
    supported_dtypes = get_supported_simd_dtypes(simd)
    simdlen = get_simdlen(simd)
    result = []
    for loop in recursive_scopes(program):
        if not is_innermost_loop(loop):
            continue
        if not has_one_op_in_scope(loop):
            continue
        if loop.options.get("parallel", False):
            continue
        if loop.options.get("unroll", False):
            continue
        if loop.options.get("vectorize", False):
            continue
        if not isinstance(loop.dim, ConstElem):
            continue
        operation = loop.ops[0]
        out_buf = program.buf_of_arr(operation.out_elem.name)
        output_dtype = out_buf.dtype
        
        if all(output_dtype != s.scalar() for s in supported_dtypes):
            continue
        loop_size = loop.dim.value
        dtype_size = output_dtype.size
        
        if loop_size * dtype_size != simdlen:
            continue
        
        # loop index should either be innermost or not used in the operation
        incompatible_array_found = False
        for array in operation.all_elem:
            if not isinstance(array, Array):
                continue
            # access that do not use the loop index either broadcasted or reduced
            if not any(0 in idx.accessed_pos() for idx in array.indices):
                continue
            # vector index and dimension is only allowed at the last position
            if (
                program.buf_of_arr(array.name).used_dims[-1] and 
                all(0 not in i.accessed_pos() for i in array.indices[:-1]) and
                index_is_over_innermost_dim(array.indices[-1])
            ):
                continue
            incompatible_array_found = True
        if incompatible_array_found:
            continue
        
        result.append((loop.name, simd))
            
    return result


def vectorize_loop(program: RootScope, loop_name: str, simd: str):
    assert (loop_name, simd) in find_vectorizable_loops(program, simd)
    parent, idx = find_scope_by_name(program, loop_name)
    loop: LoopScope = parent.ops[idx]
    loop.options["vectorize"] = True
    loop.options["simd"] = simd
    program.concepts.add("frozen_scopes")
    program.concepts.add("frozen_buffers")