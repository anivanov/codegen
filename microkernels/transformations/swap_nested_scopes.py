from kelgen.gen import *

def find_swappable_nested_scopes(program, scope=None):
    """ 
        This method returns the name of the outer scope in scope nest ...->outer->inner->...
        Condition for swapping is that inner scope is the only scope in the outer scope and
        inner scope size is independent of the outer scope iteration index.
    """
    if "frozen_scopes" in program.concepts:
        return []
    if scope is None:
        scope = program
    result = []
    for op in scope.ops:
        if not isinstance(op, Scope):
            continue
        # consider nested subtree
        result.extend(find_swappable_nested_scopes(program, op))
        # consider current scope
        if len(op.ops) != 1:
            continue
        [nested_scope] = op.ops
        if not isinstance(nested_scope, Scope):
            continue
        all_accessed_pos = set()
        if isinstance(nested_scope.dim, Array):
            for idx in nested_scope.dim.indices:
                all_accessed_pos |= idx.accessed_pos()
        if 0 in all_accessed_pos:
            continue
        result.append((op.name,))
    return result


def swap_nested_scopes(program, scope_name):
    """
        Given scope name is the outer scope.
        Before:
        ... -> current_scope -> nested_scope -> ...
        After:
        ... -> nested_scope -> current_scope -> ...
    """
    assert (scope_name,) in find_swappable_nested_scopes(program)
    parent, idx = find_scope_by_name(program, scope_name)
    current_scope = parent.ops[idx]
    [nested_scope] = current_scope.ops
    current_scope.ops = nested_scope.ops
    nested_scope.ops = [current_scope]
    parent.ops[idx] = nested_scope
    # adjust potentially changed outputs
    parent.propagate_used_outputs(parent.used_outputs)
    # fix array accesses referring to the swapped scopes
    swap_array_accesses(current_scope)


def reorder_loops(program, op_name, before, after, silent=True, eval_func=None):
    assert len(before) == len(after)
    assert len(before) == len(set(before))
    assert set(after) == set(before)
    
    while len(before) > 1:
        desired_index = 0
        name = after[desired_index]
        original_index = before.index(name)
        
        num_swaps = original_index - desired_index
        scopes_in_nest = len(before)
        swap_start = scopes_in_nest - num_swaps
        for swap_target in range(swap_start, scopes_in_nest):
            scope_name = f'{op_name}#{swap_target}'
            swap_nested_scopes(program, scope_name)
            if not silent:
                print(f'swap_nested_scopes(program, {scope_name})')
            if eval_func is not None:
                eval_func(program)
        
        before = before[:original_index] + before[original_index + 1:]
        after = after[1:]


def increase_scope_depth(program, scope_name, number=None, silent=True, eval_func=None):
    """ Returns the number of times the depth was increased """
    [op_name, scope_idx] = scope_name.split('#')
    scope_idx = int(scope_idx)
    num_increases = 0
    while True:
        if number is not None and num_increases == number:
            return num_increases
        current_scope_name = f'{op_name}#{scope_idx-num_increases}'
        if (current_scope_name,) not in find_swappable_nested_scopes(program):
            return num_increases
        swap_nested_scopes(program, current_scope_name)
        if not silent:
            print(f'swap_nested_scopes(program, "{current_scope_name}")')
        if eval_func is not None:
            eval_func(program)
        num_increases += 1


def decrease_scope_depth(program, scope_name, number=None, silent=True, eval_func=None):
    """ Returns the number of times the depth was decreased """
    [op_name, scope_idx] = scope_name.split('#')
    scope_idx = int(scope_idx)
    num_decreases = 0
    while True:
        if number is not None and num_decreases == number:
            return num_decreases
        current_scope_name = f'{op_name}#{scope_idx+num_decreases+1}'
        if (current_scope_name,) not in find_swappable_nested_scopes(program):
            return num_decreases
        swap_nested_scopes(program, current_scope_name)
        if not silent:
            print(f'swap_nested_scopes(program, "{current_scope_name}")')
        if eval_func is not None:
            eval_func(program)
        num_decreases += 1