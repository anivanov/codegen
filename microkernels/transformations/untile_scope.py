from kelgen.gen import *

def find_untileable_scopes(root_scope):
    if 'frozen_scopes' in root_scope.concepts:
        return []
    tileable_scopes = []
    scopes = get_scopes(root_scope)

    for scope in scopes:
        # here we have outer scope candidate for untiling
        assert isinstance(scope, LoopScope)
        # check that scope has exactly one child
        if len(scope.ops) != 1:
            continue
        # check that the only child is inner scope
        if not isinstance(scope.ops[0], LoopScope):
            continue

        tile_size = check_untiling_propagation(distance_to_outer=0, scope=scope, tile_size=None)
        if tile_size == 0:
            continue

        tileable_scopes.append((scope.name,))

    return tileable_scopes

def untile_scope(root_scope, scope_name):
    """
    Second step of the untiling process:
    - Assert that all the buffers that appear in the scope have been untiled
    - Merge two scopes of dim M and N into one scope of dim M*N
    - Replace the untiling index expressions with a single index: i*N+j -> (i/N)*N + i%N -> i
    """
    assert (scope_name,) in find_untileable_scopes(root_scope)

    parent, idx = find_scope_by_name(root_scope, scope_name)
    scope = parent.ops[idx]

    tile_size = scope.ops[0].dim.value

    if isinstance(scope.dim, ConstElem) and isinstance(scope.ops[0].dim, ConstElem):
        new_dim = scope.dim.value * scope.ops[0].dim.value
    else:
        new_dim = IndexExpr(scope.dim, "*", scope.ops[0].dim)

    new_scope = LoopScope(scope.ops[0].ops, new_dim)
    parent.ops[idx] = new_scope

    untile_array_accesses(new_scope, access_depth=1)
    new_scope.used_outputs = scope.used_outputs