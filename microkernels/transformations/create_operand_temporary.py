from kelgen.gen import *

def find_creatable_operand_temporaries(root_scope, max_heap_bytes=2**30):
    """ Requires all scopes to be split """
    
    if not check_all_scopes_split(root_scope):
        return []  # only one operation per scope is allowed

    used_heap = compute_used_memory_size(root_scope, "heap")

    result = []

    for op in recursive_ops(root_scope):
        for idx, elem in enumerate(op.all_elem):
            if not isinstance(elem, Array):
                continue
            buf = root_scope.buffers[root_scope.arr_to_buf[elem.name]]
            if used_heap + buf.size_bytes() > max_heap_bytes:
                continue
            result.append((op.name, idx))

    # no need to sort, order already deterministic
    return result


def create_operand_temporary(root_scope, op_name, operand_idx, check=True):
    """ Requires all scopes to be split """
    
    if check:
        assert (op_name, operand_idx) in find_creatable_operand_temporaries(root_scope)
    
    parent_op, op_idx = find_op_by_name(root_scope, op_name)
    op = parent_op.ops[op_idx]
    arr = op.all_elem[operand_idx]
    arr_name = arr.name
    new_arr_name = generate_new_array_name(root_scope, arr_name)
    
    # add temporary buffer
    orig_buf_name = root_scope.arr_to_buf[arr_name]
    orig_buf = root_scope.buffers[orig_buf_name]
    tmp_buf = copy.deepcopy(orig_buf)
    new_buf_name = new_arr_name[:]  # for temporary array we create buffer with the same name
    root_scope.arr_to_buf[new_buf_name] = new_arr_name
    root_scope.buffers[new_buf_name] = tmp_buf  
    
    # create operation that will perform elementwise temporary assignment
    new_op = Operation(
        copy.deepcopy(arr),
        [copy.deepcopy(arr)],
        OpDesc(tmp_buf.dtype),
    )   
    
    # build scope nest for the operation that will copy the original array into the temporary
    # it takes scopes from the original operation except reduction or broadcast dimensions
    [root_scope_, *scope_nest] = get_scopes_from_root_to_scope(root_scope, parent_op, with_root=True, with_scope=True)
    accessed_scope_pos = arr.accessed_pos()
    current_scope_pos = 0
    move_scope_nest = new_op
    
    # now iteratively wrap scopes into the new operation
    for scope_pos, scope in enumerate(reversed(scope_nest)):
        if scope_pos not in accessed_scope_pos:
            continue
        move_scope_nest = LoopScope([move_scope_nest], copy.deepcopy(scope.dim))
        # replace indices to account for skipped scopes
        new_op.out_elem = new_op.out_elem.replace_index(Index(current_scope_pos), scope_pos)
        new_op.inp_elem[0] = new_op.inp_elem[0].replace_index(Index(current_scope_pos), scope_pos)
        
        current_scope_pos += 1
    
    scope_nest_with_op = scope_nest + [op]  # correctly handle empty scope nests
    
    index_in_root = root_scope.ops.index(scope_nest_with_op[0])

    op.all_elem[operand_idx].name = new_arr_name
    # different behavior depending on the target operand position (read or write)
    if operand_idx == 0:
        # output operand: create temporary initialization right after the operation
        new_op.inp_elem[0].name = new_arr_name
        root_scope.ops.insert(index_in_root + 1, move_scope_nest)
    else:
        # input operand: create temporary initialization right before the operation
        new_op.out_elem.name = new_arr_name
        root_scope.ops.insert(index_in_root, move_scope_nest)
        
    # propagate used outputs
    root_scope.propagate_used_outputs(root_scope.used_outputs)