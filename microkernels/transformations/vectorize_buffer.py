from kelgen.gen import *

def find_vectorizable_buffers(program: RootScope) -> List[str]:
    # all buffer occurences should happen inside vectorized loops
    rejected_candidates = set()
    vector_buffer_accesses = set()
    for scopes, op in recursive_scopes_with_ops(program):
        if len(scopes) > 0 and scopes[-1].options.get("vectorize", False):
            # op is vectorized
            for elem in op.all_elem:
                if isinstance(elem, Array) and 0 in elem.accessed_pos():
                    buf_name = program.arr_to_buf[elem.name]
                    buf = program.buffers[buf_name]
                    if buf.dtype.scalars != 1:
                        continue  # already vectorized
                    if buf.dims[-1].value != scopes[-1].dim.value:
                        rejected_candidates.add(elem.name)
                        continue  # vector dimension should match loop dimension
                    vector_buffer_accesses.add(program.arr_to_buf[elem.name])            
        else:
            # op is not vectorized
            for elem in op.all_elem:
                if isinstance(elem, Array):
                    rejected_candidates.add(program.arr_to_buf[elem.name]) 
    return sorted((buf_name,) for buf_name in (vector_buffer_accesses - rejected_candidates))

def vectorize_buffer(program: RootScope, buf_name: str):
    assert (buf_name,) in find_vectorizable_buffers(program)
    buf = program.buffers[buf_name]
    buf.dtype = DataType(buf.dtype.literal, buf.dtype.size, buf.dims[-1].value)