import functools
from kelgen.features import avx_available, avx512_available, neon_available

from .activate_ssr import activate_ssr_by_id, find_activatable_ssr_by_idx
from .create_dimension import create_dimension, find_creatable_dimensions
from .create_operand_temporary import create_operand_temporary, find_creatable_operand_temporaries
from .create_temporary import create_temporary, find_creatable_temporaries
from .delete_dimension import delete_dimension, find_deletable_dimensions
from .delete_temporary import delete_temporary, find_deletable_temporaries
from .enable_frep import enable_frep, find_applicable_frep
from .enable_ssr import enable_ssr, find_applicable_ssr
from .increase_ssr_depth import increase_ssr_depth_by_idx, find_increasable_ssr_depth_by_idx
from .join_scopes import join_scopes, find_joinable_scopes
from .merge_ssr import merge_ssr_by_idx, find_mergeable_ssr_by_idx
from .move_buf_to_stack import move_buf_to_stack, find_bufs_movable_to_stack
from .parallelize_scope import parallelize_scope, find_parallelizable_scopes
from .reuse_arr_dims import reuse_arr_dims, find_reusable_arr_dims
from .reuse_buffers import reuse_buffers, find_reusable_buffers
from .split_scopes import split_scopes, find_splittable_scopes
from .swap_buffer_dims import swap_buffer_dims, find_swappable_buffer_dims
from .swap_nested_scopes import (
    swap_nested_scopes,
    find_swappable_nested_scopes,
    reorder_loops,
    increase_scope_depth,
    decrease_scope_depth,
)
from .swap_ops import swap_ops, find_swappable_ops
from .tile_buffer import tile_buffer, find_tileable_buffers
from .tile_scope import tile_scope, find_tileable_scopes, tile_nested_scopes
from .unroll_scope import unroll_scope, find_unrollable_scopes
from .untile_buffer import untile_buffer, find_untileable_buffers
from .untile_scope import untile_scope, find_untileable_scopes
from .vectorize_buffer import vectorize_buffer, find_vectorizable_buffers
from .vectorize_loop import vectorize_loop, find_vectorizable_loops


@functools.cache
def generic_transformations():
   return {
        "create_dimension": (create_dimension, find_creatable_dimensions),
        "create_operand_temporary": (create_operand_temporary, find_creatable_operand_temporaries),
        "create_temporary": (create_temporary, find_creatable_temporaries),
        "delete_dimension": (delete_dimension, find_deletable_dimensions),
        "delete_temporary": (delete_temporary, find_deletable_temporaries),
        "join_scopes": (join_scopes, find_joinable_scopes),
        "move_buf_to_stack": (move_buf_to_stack, find_bufs_movable_to_stack),
        "parallelize_scope": (parallelize_scope, find_parallelizable_scopes),
        "reuse_arr_dims": (reuse_arr_dims, find_reusable_arr_dims),
        "reuse_buffers": (reuse_buffers, find_reusable_buffers),
        "split_scopes": (split_scopes, find_splittable_scopes),
        "swap_buffer_dims": (swap_buffer_dims, find_swappable_buffer_dims),
        "swap_nested_scopes": (swap_nested_scopes, find_swappable_nested_scopes),
        "swap_ops": (swap_ops, find_swappable_ops),
        "tile_buffer": (tile_buffer, find_tileable_buffers),
        "tile_scope": (tile_scope, find_tileable_scopes),
        "unroll_scope": (unroll_scope, find_unrollable_scopes),
        "untile_buffer": (untile_buffer, find_untileable_buffers),
        "untile_scope": (untile_scope, find_untileable_scopes),
    }

@functools.cache
def vectorization_transformations():
    transformations = {}
    funcs = [
        ("avx", avx_available()),
        ("avx512", avx512_available()),
        ("neon", neon_available()),
    ]
    if any(available for name, available in funcs):
        transformations["vectorize_loop"] = (
            vectorize_loop,
            lambda p: (
                # keep braces to ensure correct order of operations
                (find_vectorizable_loops(p, 'avx') if avx_available() else []) +
                (find_vectorizable_loops(p, 'avx512') if avx512_available() else []) +
                (find_vectorizable_loops(p, 'neon') if neon_available() else [])
            )
        )
        transformations["vectorize_buffer"] = (vectorize_buffer, find_vectorizable_buffers)
    return transformations

@functools.cache
def native_transformations():
    return generic_transformations() | vectorization_transformations()
    

@functools.cache
def only_snitch_transformations():
    return {
        'enable_ssr': (enable_ssr, find_applicable_ssr),
        'increase_ssr_depth_by_idx': (increase_ssr_depth_by_idx, find_increasable_ssr_depth_by_idx),
        'merge_ssr_by_idx': (merge_ssr_by_idx, find_mergeable_ssr_by_idx),
        'activate_ssr_by_id': (activate_ssr_by_id, find_activatable_ssr_by_idx),
        'enable_frep': (enable_frep, find_applicable_frep),
    }

@functools.cache
def snitch_transformations():
    return generic_transformations() | only_snitch_transformations()

@functools.cache
def all_transformations():
    return generic_transformations() | vectorization_transformations() | only_snitch_transformations()
