from kelgen.gen import *

def find_deletable_dimensions(root_scope):
    # verify assumptions and early exit
    if not check_all_scopes_split(root_scope):
        return []

    if not check_no_reused_buffers(root_scope):
        return []
    
    if not check_no_unrolled_scopes(root_scope):
        return []
    result = []
    inp, tmp, out = root_scope.get_inp_tmp_out()
    flat_list = get_flattened_ops_with_scopes_textual(root_scope)    
    # sort tmp arrays to ensure deterministic order
    for array_name in sorted(tmp):
        discovered_rtw, discovered_wtr = analyze_deletable_dimension_candidates(root_scope, array_name)
        if discovered_rtw is not None:
            read_scopes_op = [(scopes, op) for scopes, op in flat_list if any(isinstance(e, Array) and e.name == array_name for e in op.inp_elem)]
            # mark first read of arr as target
            read_scope_idx, read_op = read_scopes_op[0]
            [read_idx, *_] = [idx for idx, e in enumerate(read_op.inp_elem) if isinstance(e, Array) and e.name == array_name]
            result.append((read_op.name, read_idx + 1))
        if discovered_wtr is not None:
            # mark write of arr as target
            [(write_scopes, write_op)] = [(scopes, op) for scopes, op in flat_list if op.out_elem.name == array_name]
            result.append((write_op.name, 0))
    return result


def delete_dimension(root_scope, op_name, access_idx):
    """
    This transformation supposed to inverse the changes introduced with create_dimension.
    Similarly, there are two modes of operation: read-to-write and write-to-read.

    Example: read-to-write, read is target.
    Before:            After:
    N x[n]=f(z)          x=f(z)
    N y[n]=g(x[n])     N y[n]=g(x)  # target is ("y", 1) -> "x[n]"
    N c[n]=h(x[n])     N c[n]=h(x)

    Example: write-to-read, write is target.
    Before:            After:
    N x[n]=z[n]        N x+=z[n]  # target is ("x", 0) -> "x[n]"
    N y+=x[n]            y=x
    N c+=x[n]            c=x

    Note: In read-to-write case, arbitrary functions f, g, h are allowed without restrictions.
    In write-to-read case, the function must be a simple assignment, and the target must be the only write to the array.
    Such constraint is necessary because f(a+b) != f(a)+f(b). 

    Deletion is applied to the innermost scope satisfying the requirements.
    """
    assert (op_name, access_idx) in find_deletable_dimensions(root_scope)

    tgt_parent, tgt_idx = find_op_by_name(root_scope, op_name)
    tgt_op = tgt_parent.ops[tgt_idx]
    tgt_elem = tgt_op.all_elem[access_idx]
    assert isinstance(tgt_elem, Array)

    discovered_rtw, discovered_wtr = analyze_deletable_dimension_candidates(root_scope, tgt_elem.name)

    discovered = discovered_wtr if access_idx == 0 else discovered_rtw
    assert discovered is not None

    flat_list = get_flattened_ops_with_scopes_textual(root_scope)
    [(write_scopes, write_op)] = [(scopes, op) for scopes, op in flat_list if op.out_elem.name == tgt_elem.name]
    read_scopes_op = [(scopes, op) for scopes, op in flat_list if any(isinstance(e, Array) and e.name == tgt_elem.name for e in op.inp_elem)]

    # remove dimension from the buffer
    buf_name = root_scope.arr_to_buf[tgt_elem.name]
    buf = root_scope.buffers[buf_name]
    scope_idx = len(write_scopes) - discovered[0] - 1
    [loc] = [loc for loc, idx in enumerate(write_op.out_elem.indices) if scope_idx in idx.accessed_pos()]
    buf.used_dims.pop(loc)
    buf.dims.pop(loc)

    # remove affected dimensions from all array accesses
    # also, when scopes removed, make sure to readjust the indices to keep relative positions correct
    remove_read_scopes = access_idx == 0
    for (scopes, op), (occurence_idx, scope_idx) in zip([(write_scopes, write_op), *read_scopes_op], enumerate(discovered)):
        is_read = occurence_idx > 0
        new_elems = []
        idx_to_remove = len(scopes) - scope_idx - 1
        for elem in op.all_elem:
            if is_read==remove_read_scopes:
                new_elem = elem.remove_index(idx_to_remove, with_shift=True)
            elif isinstance(elem, Array) and elem.name == tgt_elem.name:
                new_elem = elem.remove_index(idx_to_remove, with_shift=False)
            else:
                new_elem = copy.deepcopy(elem)
            assert new_elem is not None
            new_elems.append(new_elem)
        op.all_elem = new_elems

    if access_idx == 0:
        # write-to-read, write is target
        assert discovered is not None

        # if write was not reduced into initially, add it
        if not write_op.op.reduce:
            # make sure to deepcopy the op, because otherwise it is shared with reads and can be
            # modified below or in the future
            write_op.op = copy.deepcopy(read_scopes_op[0][1].op)

        # if deleted scope is the last over which reduction is happening, remove reduction from op
        for scopes, op in read_scopes_op:
            if len(op.out_elem.accessed_pos()) + 1 == len(scopes):
                op.op.reduce = ""

        # remove scope dimension from all reads
        for (scopes, op), scope_idx in zip(read_scopes_op, discovered[1:]):
            idx_in_root = root_scope.ops.index(scopes[0])
            idx_in_parent = idx_in_root if scope_idx == 0 else 0
            scopes_with_root = [root_scope, *scopes]
            scopes_with_root[scope_idx].ops[idx_in_parent] = scopes_with_root[scope_idx+2] if scope_idx+2 < len(scopes_with_root) else op

    else:
        # read-to-write, read is target

        # remove scope dimension from write
        scopes, op = write_scopes, write_op
        scope_idx = discovered[0]
        idx_in_root = root_scope.ops.index(scopes[0])
        idx_in_parent = idx_in_root if scope_idx == 0 else 0
        scopes_with_root = [root_scope, *scopes]
        scopes_with_root[scope_idx].ops[idx_in_parent] = scopes_with_root[scope_idx+2] if scope_idx+2 < len(scopes_with_root) else op

    # after scope tree modification, we need to propagate used outputs
    root_scope.propagate_used_outputs(root_scope.used_outputs)