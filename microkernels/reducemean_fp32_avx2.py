from gen import *
import torch

def reducemean_fp32_avx2(M, N):
    x = np.random.uniform(-10, 10, (M, N)).astype(np.float32)
    tx = torch.from_numpy(x)

    def init_src():
        x[:] = np.random.uniform(-10, 10, (M, N)).astype(np.float32)
    
    pt_mean, pt_std, pt_runtime = time_prof(10, lambda: torch.mean(tx, dim=1), init_src)
    
    print(f"Pytorch reducemean time [ms]: avg {pt_mean * 1e3:.6f} std {pt_std * 1e3:.6f} reps {len(pt_runtime)}")

    z = np.mean(x, axis=1)
    
    def eval_func(program, ignore=True):
        if ignore:
            return
        print(program.text())
        time = test_native_code(
            program,
            {'x': x, 'z': z},
            min_reps=100,
            rep_number=10,
            remove_tmp_files=False,
            silent=False,
        )
        time = time[max(len(time) // 3, 1):]
        print(f"My reducemean [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")
    
    program_text = f"""
        Name: reducemean
        In: x
        Out: z
        Declarations:
            x f32 [{M}, {N}] heap
            t f32 [{M}] heap
            z f32 [{M}] heap
        Code:
        {M} {N} t[{{0}}] += x[{{0}}, {{1}}]
        {M} z[{{0}}] = t[{{0}}] / {N}
    """
    
    program = parse_program(program_text); eval_func(program, ignore=False)

    # tile outer scope for parallelism
    apply_to_exhaustion(program, [(join_scopes, find_joinable_scopes)], silent=False, eval_func=eval_func)
    expected_num_cpus = 16
    tile_scope(program, 't#1', M // expected_num_cpus); eval_func(program)
    
    apply_to_exhaustion(program, [(tile_buffer, find_tileable_buffers)], silent=False, eval_func=eval_func)
    apply_to_exhaustion(program, [(reuse_arr_dims, find_reusable_arr_dims)], silent=False, eval_func=eval_func)
    apply_to_exhaustion(program, [(move_buf_to_stack, lambda x: find_bufs_movable_to_stack(x, 4096))], silent=False, eval_func=eval_func)
    parallelize_scope(program, 't#2'); eval_func(program, ignore=False)


if __name__ == '__main__':
    reducemean_fp32_avx2(4096, 4096)
