// Copyright 2020 ETH Zurich and University of Bologna.
// Licensed under the Apache License, Version 2.0, see LICENSE for details.
// SPDX-License-Identifier: Apache-2.0

// #include "blas.h"
// #include "dnn.h"
#include <snrt.h>
#include "snrt_mini.h"
#include <stdint.h>
#include <math.h>
#include <float.h>
#define u8 uint8_t
#define i32 int32_t
#define f32 float
#define f64 double
#include "snrt.h"
#include <printf.h>

#define min(a, b) ((a) < (b) ? (a) : (b))
#define max(a, b) ((a) > (b) ? (a) : (b))

/**
 * @struct conv_layer_struct
 * @brief This structure contains all parameters necessary for Convolutional
 * layers
 * @var conv_layer_struct::CO
 * Number of output channels
 * @var conv_layer_struct::CI
 * Number of input channels
 * @var conv_layer_struct::IH
 * Height of input feature map
 * @var conv_layer_struct::IW
 * Width of input feature map
 * @var conv_layer_struct::OH
 * Height of output feature map
 * @var conv_layer_struct::OW
 * Width of output feature map
 * @var conv_layer_struct::FH
 * Height of filter
 * @var conv_layer_struct::FW
 * Width of filter
 * @var conv_layer_struct::pad
 * Padding on all sides
 * @var conv_layer_struct::ifmap
 * Pointer to input feature map
 * @var conv_layer_struct::weights
 * Pointer to weights
 * @var conv_layer_struct::ofmap
 * Pointer to output feature map
 * @var conv_layer_struct::TILE_CI
 * Tiling factor of input channel
 * @var conv_layer_struct::cluster2cluster
 * Flag for enabling cluster 2 cluster communication
 * @var conv_layer_struct::im2col
 * Flag for enabling im2col + GEMM
 * @var conv_layer_struct::gamma
 * Pointer to gamma for BatchNorm
 * @var conv_layer_struct::beta
 * Pointer to beta for BatchNorm
 * @var gemm_layer_struct::dtype
 * Precision of Convolution layer
 */
// typedef struct conv_layer_struct {
//     // CONV2D
//     uint32_t CO;
//     uint32_t CI;
//     uint32_t IH;
//     uint32_t IW;
//     uint32_t OH;
//     uint32_t OW;
//     uint32_t FH;
//     uint32_t FW;
//     uint32_t pad;

//     double *ifmap;
//     double *weights;
//     double *ofmap;

//     uint32_t TILE_CI;
//     uint32_t cluster2cluster;
//     uint32_t im2col;

//     // BATCHNORM
//     double *gamma;
//     double *beta;

//     precision_t dtype;
// } conv_layer;

/**
 * @struct kernel_fp32
 * @brief parameters for single-precision fusedconv kernel
 *
 * @var kernel_fp32::pInBuffer
 * pointer to the input feature map
 * @var kernel_fp32::dim_in_x
 * width of input feature map
 * @var kernel_fp32::dim_in_y
 * height of input feature map
 * @var kernel_fp32::ch_in
 * number of input channels
 * @var kernel_fp32::pWeight
 * pointer to weights
 * @var kernel_fp32::ch_out
 * number of output channels
 * @var kernel_fp32::dim_kernel_x
 * width of kernel
 * @var kernel_fp32::dim_kernel_y
 * height of kernel
 * @var kernel_fp32::padding_y_top
 * number of pixels padded on the top
 * @var kernel_fp32::padding_y_bottom
 * number of pixels padded on the bottom
 * @var kernel_fp32::padding_x_left
 * number of pixels padded on the left
 * @var kernel_fp32::padding_x_right
 * number of pixels padded on the right
 * @var kernel_fp32::stride_x
 * stride in x direction
 * @var kernel_fp32::stride_y
 * stride in y direction
 * @var kernel_fp32::bias
 * bias of convolution (currently not used)
 * @var kernel_fp32::bias_shift
 * bias shift of convolution (currently not used)
 * @var kernel_fp32::out_shift
 * shift factor for requantization (not used for floating point)
 * @var kernel_fp32::out_mult
 * mult factor for requantization (not used for floating point)
 * @var kernel_fp32::pOutBuffer
 * pointer to output feature map
 * @var kernel_fp32::dim_out_x
 * width of output feature map
 * @var kernel_fp32::dim_out_y
 * height of output feature map
 * @var kernel_fp32::kappa
 * multiplication factor for BatchNorm
 * @var kernel_fp32::lambda
 * @var kernel_fp32::pIm2ColBuffer
 * pointer to im2col Buffer (not used)
 * bias for BatchNorm
 * @var kernel_fp32::flag_relu
 * RELU activation flag
 * @var kernel_fp32::flag_batch_norm
 * BatchNorm flag
 * @var kernel_fp32::flag_y_accumulate_start
 * indicates that output feature map is initizialized with zeros
 * @var kernel_fp32::flag_y_accumulate_end
 * indicates that BN, RELU can be performed
 * @var kernel_fp32::memory_chan
 * Not used
 */

// typedef struct {
//     uint32_t ch_in;
//     uint32_t ch_out;
//     uint32_t dim_in_x;
//     uint32_t dim_in_y;
//     uint32_t dim_kernel_x;
//     uint32_t dim_kernel_y;
//     uint32_t dim_out_x;
//     uint32_t dim_out_y;
//     uint32_t padding_y_top;
//     uint32_t padding_y_bottom;
//     uint32_t padding_x_left;
//     uint32_t padding_x_right;
//     uint32_t stride_x;
//     uint32_t stride_y;
//     uint32_t flag_relu;
//     int flag_batch_norm;
//     int depthwise;
//     int chw_layer;
//     int flag_y_accumulate_start;
//     int flag_y_accumulate_end;
//     float *pInBuffer;
//     float *pWeight;
//     float *lambda;
//     float *kappa;
//     float *pOutBuffer;
//     int8_t *bias;
//     uint8_t *pIm2ColBuffer;
//     unsigned int *memory_chan;
//     uint32_t bias_shift;
//     uint32_t out_shift;
//     uint32_t out_mult;
//     precision_t dtype;
// } kernel_fp32;

/**
 * @struct kernel_fp64
 * @brief parameters for double-precision fusedconv kernel
 *
 * @var kernel_fp64::pInBuffer
 * pointer to the input feature map
 * @var kernel_fp64::dim_in_x
 * width of input feature map
 * @var kernel_fp64::dim_in_y
 * height of input feature map
 * @var kernel_fp64::ch_in
 * number of input channels
 * @var kernel_fp64::pWeight
 * pointer to weights
 * @var kernel_fp64::ch_out
 * number of output channels
 * @var kernel_fp64::dim_kernel_x
 * width of kernel
 * @var kernel_fp64::dim_kernel_y
 * height of kernel
 * @var kernel_fp64::padding_y_top
 * number of pixels padded on the top
 * @var kernel_fp64::padding_y_bottom
 * number of pixels padded on the bottom
 * @var kernel_fp64::padding_x_left
 * number of pixels padded on the left
 * @var kernel_fp64::padding_x_right
 * number of pixels padded on the right
 * @var kernel_fp64::stride_x
 * stride in x direction
 * @var kernel_fp64::stride_y
 * stride in y direction
 * @var kernel_fp64::bias
 * bias of convolution (currently not used)
 * @var kernel_fp64::bias_shift
 * bias shift of convolution (currently not used)
 * @var kernel_fp64::out_shift
 * shift factor for requantization (not used for floating point)
 * @var kernel_fp64::out_mult
 * mult factor for requantization (not used for floating point)
 * @var kernel_fp64::pOutBuffer
 * pointer to output feature map
 * @var kernel_fp64::dim_out_x
 * width of output feature map
 * @var kernel_fp64::dim_out_y
 * height of output feature map
 * @var kernel_fp64::kappa
 * multiplication factor for BatchNorm
 * @var kernel_fp64::lambda
 * bias for BatchNorm
 * @var kernel_fp64::pIm2ColBuffer
 * pointer to im2col Buffer (not used)
 * @var kernel_fp64::flag_relu
 * RELU activation flag
 * @var kernel_fp64::flag_batch_norm
 * BatchNorm flag
 * @var kernel_fp64::flag_y_accumulate_start
 * indicates that output feature map is initizialized with zeros
 * @var kernel_fp64::flag_y_accumulate_end
 * indicates that BN, RELU can be performed
 * @var kernel_fp64::memory_chan
 * Not used
 */
typedef struct {
    double *pInBuffer;
    uint16_t dim_in_x;
    uint16_t dim_in_y;
    uint16_t ch_in;
    double *pWeight;
    uint16_t ch_out;
    uint16_t dim_kernel_x;
    uint16_t dim_kernel_y;
    uint16_t padding_y_top;
    uint16_t padding_y_bottom;
    uint16_t padding_x_left;
    uint16_t padding_x_right;
    uint16_t stride_x;
    uint16_t stride_y;
    int8_t *bias;
    uint16_t bias_shift;
    uint16_t out_shift;
    uint16_t out_mult;
    double *pOutBuffer;
    uint16_t dim_out_x;
    uint16_t dim_out_y;
    double *kappa;
    double *lambda;
    uint8_t *pIm2ColBuffer;
    int flag_relu;
    int flag_batch_norm;
    int flag_y_accumulate_start;
    int flag_y_accumulate_end;
    unsigned int *memory_chan;
} kernel_fp64;

/**
 * @brief helper function that implements Batch Normalization and ReLU
 * @param pBuffer pointer to the feature map
 * @param dim_x width of feature map
 * @param dim_y height of feature map
 * @param ch number of channels (SIMD restricts multiple of 2)
 * @param kappa multiplication factor for BatchNorm
 * @param lambda bias for BatchNorm
 * @param flag_relu RELU activation flag
 * @param flag_batch_norm BatchNorm flag
 */

/**
 * @brief implementation of a double-precision fp convolutional kernel
 * for DORY trials. Currently does a direct convolution without im2col.
 * The memory layout of input/output feature map is HxWxC, resp. CoxFhxFwxCi.
 * Fuses multiple layers together (Conv2d, Batchnorm, Relu) that can be enabled
 * with a flag
 * @param k kernel_fp64 struct reference that holds all parameters
 */

extern i32 W_in, H_in, W_out, H_out, C, M, kernel;

void func_conv2d(i32 dilation, f64* __restrict__ input, i32 stride, f64* __restrict__ weight, f64* __restrict__ output, void* __restrict__ _tmp) {
    kernel_fp64 pk;


    pk.pInBuffer = input;
    pk.dim_in_x = W_in;
    pk.dim_in_y = H_in;
    pk.ch_in = C;
    pk.pWeight = weight;
    pk.ch_out = M;
    pk.dim_kernel_x = kernel;
    pk.dim_kernel_y = kernel;
    pk.padding_y_top = 0;
    pk.padding_y_bottom = 0;
    pk.padding_x_left = 0;
    pk.padding_x_right = 0;
    pk.stride_x = stride;
    pk.stride_y = stride;
    // int8_t *bias;
    // uint16_t bias_shift;
    // uint16_t out_shift;
    // uint16_t out_mult;
    pk.pOutBuffer = output;
    //(input + 2 * pad - (dilation * (kernel - 1) + 1)) // stride + 1
    pk.dim_out_x = W_out;
    pk.dim_out_y = H_out;
    // double *kappa;
    // double *lambda;
    // uint8_t *pIm2ColBuffer;
    pk.flag_relu = 0;
    pk.flag_batch_norm = 0;
    pk.flag_y_accumulate_start = 1;
    pk.flag_y_accumulate_end = 0;
    // unsigned int *memory_chan;


    kernel_fp64* k = &pk;
    // Parallelization/Pipelining parameters
    //const uint32_t compute_id = snrt_cluster_compute_core_num();
    const uint32_t compute_num = 1;
    const uint32_t max_unroll = 8;  // Maximum number of unrolling
    const uint32_t cleanup_unroll = k->dim_out_y % max_unroll;

    // Calculate strides to access specific dimensions
    // of input/output feature map and weights
    // Input feature map (H x W x Ci)
    // Calculate effective H, W dimension including padding
    const uint32_t dim_in_eff_x =
        k->dim_in_x + k->padding_x_left + k->padding_x_right;
    const uint32_t dim_in_eff_y =
        k->dim_in_y + k->padding_y_top + k->padding_y_bottom;
    const uint32_t input_w_stride = k->ch_in;
    const uint32_t input_h_stride = input_w_stride * dim_in_eff_x;

    // Output feature map (H x W x Co)
    const uint32_t output_w_stride = k->ch_out;
    const uint32_t output_h_stride = output_w_stride * k->dim_out_x;

    // Weight (Co x Fh x Fw x Ci)
    const uint32_t kernel_w_stride = k->ch_in;
    const uint32_t kernel_h_stride = kernel_w_stride * k->dim_kernel_x;
    const uint32_t kernel_co_stride = kernel_h_stride * k->dim_kernel_y;

    // Reference Loops
    // for (uint32_t co = compute_id; co < k->ch_out; co += compute_num) {
    //     for (uint32_t h0 = 0; h0 < k->dim_in_y / max_unroll; h++) {
    //         for (uint32_t w = 0; w < k->dim_in_x; w += k->stride_x) {
    //             for (uint32_t fh = 0; fh < k->dim_kernel_y, fh++) {
    //                 for (uint32_t fw = 0; fw < k->dim_kernel_x, fw++) {
    //                     for (uint32_t ci = 0; ci < k->ch_in; ci++) {
    //                         for (uint32_t h1 = 0; h1 < max_unroll; h1++) {
    //                             k->pOutBuffer[(h-pad_t)/str_y][(w-pad_l)/str_x][co]
    //                                   +=  k->pInBuffer[h+fh][w+fw][ci] *
    //                                       k->pWeightBuffer[co][fh][fw][ci]
    //                         }
    //                     }
    //                 }
    //             }
    //         }
    //     }
    // }
    // Setup SSRs bounds and strides for input feature map
    const uint32_t ssr0_b[4] = {max_unroll, k->ch_in, k->dim_kernel_x,
                                k->dim_kernel_y};
    const uint32_t ssr0_i[4] = {
        input_h_stride * k->stride_y * sizeof(double), 1 * sizeof(double),
        input_w_stride * sizeof(double), input_h_stride * sizeof(double)};

    snrt_ssr_loop_4d(SNRT_SSR_DM0, ssr0_b[0], ssr0_b[1], ssr0_b[2], ssr0_b[3],
                     ssr0_i[0], ssr0_i[1], ssr0_i[2], ssr0_i[3]);

    // Setup SSRs bounds and strides for kernel
    // We use only 3D SSRs here as the inner most dimension is repeated
    const uint32_t ssr1_b[3] = {k->ch_in, k->dim_kernel_x, k->dim_kernel_y};
    const uint32_t ssr1_i[3] = {1 * sizeof(double),
                                kernel_w_stride * sizeof(double),
                                kernel_h_stride * sizeof(double)};

    snrt_ssr_loop_3d(SNRT_SSR_DM1, ssr1_b[0], ssr1_b[1], ssr1_b[2], ssr1_i[0],
                     ssr1_i[1], ssr1_i[2]);

    snrt_ssr_repeat(SNRT_SSR_DM1, max_unroll);


    // Output channel dimension `k->ch_out` is parallelized over cores
    for (uint32_t co = 0; co < k->ch_out; co++) {
        uint32_t h0 = 0;

        // If `k->dim_out_y` is not divisible by `unroll`, we have to clean up
        // at the end which modifies the SSR loops, thus initialize it again
        // correctly
        // if (cleanup_unroll) {
        //     snrt_ssr_loop_4d(SNRT_SSR_DM0, ssr0_b[0], ssr0_b[1], ssr0_b[2],
        //                      ssr0_b[3], ssr0_i[0], ssr0_i[1], ssr0_i[2],
        //                      ssr0_i[3]);

        //     snrt_ssr_loop_3d(SNRT_SSR_DM1, ssr1_b[0], ssr1_b[1], ssr1_b[2],
        //                      ssr1_i[0], ssr1_i[1], ssr1_i[2]);

        //     snrt_ssr_repeat(SNRT_SSR_DM1, max_unroll);
        // }

        // Output height dimension `k->dim_out_y` first split
        for (h0 = 0; h0 < k->dim_out_y / max_unroll; h0++) {
            // Output width dimension `k->dim_out_x`
            for (uint32_t w = 0; w < k->dim_out_x; w++) {
                // TODO: check if initialization needs to be unrolled by hand
                volatile double sum[max_unroll];
                // if (k->flag_y_accumulate_start) {
                if (1) {
                    for (uint32_t i = 0; i < max_unroll; i++) {
                        sum[i] = 0.0;
                    }
                } else {
                    for (uint32_t i = 0; i < max_unroll; i++) {
                        sum[i] = *(k->pOutBuffer +
                                   (h0 * max_unroll + i) * output_h_stride +
                                   w * output_w_stride + co);
                    }
                }

                // SSR address setup and enable
                snrt_ssr_read(
                    SNRT_SSR_DM0, SNRT_SSR_4D,
                    (void *)(k->pInBuffer +
                             h0 * max_unroll * k->stride_y * input_h_stride +
                             w * k->stride_x * input_w_stride));
                snrt_ssr_read(SNRT_SSR_DM1, SNRT_SSR_3D,
                              (void *)(k->pWeight + co * kernel_co_stride));
                snrt_ssr_enable();

                asm volatile(
                    FREP(frep_ops, n_frep)
                    "fmadd.d %[sum0], ft0, ft1, %[sum0] \n"
                    "fmadd.d %[sum1], ft0, ft1, %[sum1] \n"
                    "fmadd.d %[sum2], ft0, ft1, %[sum2] \n"
                    "fmadd.d %[sum3], ft0, ft1, %[sum3] \n"
                    "fmadd.d %[sum4], ft0, ft1, %[sum4] \n"
                    "fmadd.d %[sum5], ft0, ft1, %[sum5] \n"
                    "fmadd.d %[sum6], ft0, ft1, %[sum6] \n"
                    "fmadd.d %[sum7], ft0, ft1, %[sum7] \n"
                    : [ sum0 ] "+f"(sum[0]), [ sum1 ] "+f"(sum[1]),
                      [ sum2 ] "+f"(sum[2]), [ sum3 ] "+f"(sum[3]),
                      [ sum4 ] "+f"(sum[4]), [ sum5 ] "+f"(sum[5]),
                      [ sum6 ] "+f"(sum[6]), [ sum7 ] "+f"(sum[7])
                    : [ n_frep ] "r"(
                        k->dim_kernel_y * k->dim_kernel_x * k->ch_in - 1),
                        [frep_ops] "i"(7)
                    : "ft0", "ft1", "ft2");

                snrt_ssr_disable();

                // TODO: Check if needs to be unrolled manually
                for (uint32_t i = 0; i < max_unroll; i++) {
                    k->pOutBuffer[(h0 * max_unroll + i) * output_h_stride +
                                  w * output_w_stride + co] = sum[i];
                }
            }
        }

        // Clean up rows
        // if (cleanup_unroll) {
        //     // Modify most inner loop unrolling
        //     snrt_ssr_loop_4d(SNRT_SSR_DM0, cleanup_unroll, ssr0_b[1], ssr0_b[2],
        //                      ssr0_b[3], ssr0_i[0], ssr0_i[1], ssr0_i[2],
        //                      ssr0_i[3]);

        //     snrt_ssr_loop_3d(SNRT_SSR_DM1, ssr1_b[0], ssr1_b[1], ssr1_b[2],
        //                      ssr1_i[0], ssr1_i[1], ssr1_i[2]);

        //     snrt_ssr_repeat(SNRT_SSR_DM1, cleanup_unroll);

        //     // Output width dimension `k->dim_out_x`
        //     for (uint32_t w = 0; w < k->dim_out_x; w++) {
        //         volatile double sum[max_unroll];
        //         if (k->flag_y_accumulate_start) {
        //             for (uint32_t i = 0; i < cleanup_unroll; i++) {
        //                 sum[i] = 0.0;
        //             }
        //         } else {
        //             for (uint32_t i = 0; i < cleanup_unroll; i++) {
        //                 sum[i] = *(k->pOutBuffer +
        //                            (h0 * max_unroll + i) * output_h_stride +
        //                            w * output_w_stride + co);
        //             }
        //         }

        //         // SSR address setup and enable
        //         snrt_ssr_read(
        //             SNRT_SSR_DM0, SNRT_SSR_4D,
        //             (void *)(k->pInBuffer +
        //                      h0 * max_unroll * k->stride_y * input_h_stride +
        //                      w * k->stride_x * input_w_stride));
        //         snrt_ssr_read(SNRT_SSR_DM1, SNRT_SSR_3D,
        //                       (void *)(k->pWeight + co * kernel_co_stride));
        //         snrt_ssr_enable();

        //         switch (cleanup_unroll) {
        //             case 7:
        //                 asm volatile(
                            
        //                     FREP(frep_ops, n_frep)
        //                     "fmadd.d %[sum0], ft0, ft1, %[sum0] \n"
        //                     "fmadd.d %[sum1], ft0, ft1, %[sum1] \n"
        //                     "fmadd.d %[sum2], ft0, ft1, %[sum2] \n"
        //                     "fmadd.d %[sum3], ft0, ft1, %[sum3] \n"
        //                     "fmadd.d %[sum4], ft0, ft1, %[sum4] \n"
        //                     "fmadd.d %[sum5], ft0, ft1, %[sum5] \n"
        //                     "fmadd.d %[sum6], ft0, ft1, %[sum6] \n"
        //                     : [ sum0 ] "+f"(sum[0]), [ sum1 ] "+f"(sum[1]),
        //                       [ sum2 ] "+f"(sum[2]), [ sum3 ] "+f"(sum[3]),
        //                       [ sum4 ] "+f"(sum[4]), [ sum5 ] "+f"(sum[5]),
        //                       [ sum6 ] "+f"(sum[6])
        //                     : [ n_frep ] "r"(k->dim_kernel_y * k->dim_kernel_x *
        //                                          k->ch_in -
        //                                      1),
        //                       [frep_ops] "i"(6)
        //                     : "ft0", "ft1", "ft2");
        //                 break;
        //             case 6:
        //                 asm volatile(
        //                     FREP(frep_ops, n_frep)
        //                     "fmadd.d %[sum0], ft0, ft1, %[sum0] \n"
        //                     "fmadd.d %[sum1], ft0, ft1, %[sum1] \n"
        //                     "fmadd.d %[sum2], ft0, ft1, %[sum2] \n"
        //                     "fmadd.d %[sum3], ft0, ft1, %[sum3] \n"
        //                     "fmadd.d %[sum4], ft0, ft1, %[sum4] \n"
        //                     "fmadd.d %[sum5], ft0, ft1, %[sum5] \n"
        //                     : [ sum0 ] "+f"(sum[0]), [ sum1 ] "+f"(sum[1]),
        //                       [ sum2 ] "+f"(sum[2]), [ sum3 ] "+f"(sum[3]),
        //                       [ sum4 ] "+f"(sum[4]), [ sum5 ] "+f"(sum[5])
        //                     : [ n_frep ] "r"(k->dim_kernel_y * k->dim_kernel_x *
        //                                          k->ch_in -
        //                                      1),
        //                     [frep_ops] "i"(5)
        //                     : "ft0", "ft1", "ft2");
        //                 break;
        //             case 5:
        //                 asm volatile(
        //                     FREP(frep_ops, n_frep)
        //                     "fmadd.d %[sum0], ft0, ft1, %[sum0] \n"
        //                     "fmadd.d %[sum1], ft0, ft1, %[sum1] \n"
        //                     "fmadd.d %[sum2], ft0, ft1, %[sum2] \n"
        //                     "fmadd.d %[sum3], ft0, ft1, %[sum3] \n"
        //                     "fmadd.d %[sum4], ft0, ft1, %[sum4] \n"
        //                     : [ sum0 ] "+f"(sum[0]), [ sum1 ] "+f"(sum[1]),
        //                       [ sum2 ] "+f"(sum[2]), [ sum3 ] "+f"(sum[3]),
        //                       [ sum4 ] "+f"(sum[4])
        //                     : [ n_frep ] "r"(k->dim_kernel_y * k->dim_kernel_x *
        //                                          k->ch_in -
        //                                      1),
        //                     [frep_ops] "i"(4)
        //                     : "ft0", "ft1", "ft2");
        //                 break;
        //             case 4:
        //                 asm volatile(
        //                     FREP(frep_ops, n_frep)
        //                     "fmadd.d %[sum0], ft0, ft1, %[sum0] \n"
        //                     "fmadd.d %[sum1], ft0, ft1, %[sum1] \n"
        //                     "fmadd.d %[sum2], ft0, ft1, %[sum2] \n"
        //                     "fmadd.d %[sum3], ft0, ft1, %[sum3] \n"
        //                     : [ sum0 ] "+f"(sum[0]), [ sum1 ] "+f"(sum[1]),
        //                       [ sum2 ] "+f"(sum[2]), [ sum3 ] "+f"(sum[3])
        //                     : [ n_frep ] "r"(k->dim_kernel_y * k->dim_kernel_x *
        //                                          k->ch_in -
        //                                      1),
        //                     [frep_ops] "i"(3)
        //                     : "ft0", "ft1", "ft2");
        //                 break;
        //             case 3:
        //                 asm volatile(
        //                     FREP(frep_ops, n_frep)
        //                     "fmadd.d %[sum0], ft0, ft1, %[sum0] \n"
        //                     "fmadd.d %[sum1], ft0, ft1, %[sum1] \n"
        //                     "fmadd.d %[sum2], ft0, ft1, %[sum2] \n"
        //                     : [ sum0 ] "+f"(sum[0]), [ sum1 ] "+f"(sum[1]),
        //                       [ sum2 ] "+f"(sum[2])
        //                     : [ n_frep ] "r"(k->dim_kernel_y * k->dim_kernel_x *
        //                                          k->ch_in -
        //                                      1),
        //                     [frep_ops] "i"(2)
        //                     : "ft0", "ft1", "ft2");
        //                 break;
        //             case 2:
        //                 asm volatile(
        //                     FREP(frep_ops, n_frep)
        //                     "fmadd.d %[sum0], ft0, ft1, %[sum0] \n"
        //                     "fmadd.d %[sum1], ft0, ft1, %[sum1] \n"
        //                     : [ sum0 ] "+f"(sum[0]), [ sum1 ] "+f"(sum[1])
        //                     : [ n_frep ] "r"(k->dim_kernel_y * k->dim_kernel_x *
        //                                          k->ch_in -
        //                                      1),
        //                     [frep_ops] "i"(1)
        //                     : "ft0", "ft1", "ft2");
        //                 break;
        //             case 1:
        //                 asm volatile(
        //                     FREP(frep_ops, n_frep)
        //                     "fmadd.d %[sum0], ft0, ft1, %[sum0] \n"
        //                     : [ sum0 ] "+f"(sum[0])
        //                     : [ n_frep ] "r"(k->dim_kernel_y * k->dim_kernel_x *
        //                                          k->ch_in -
        //                                      1),
        //                     [frep_ops] "i"(0)
        //                     : "ft0", "ft1", "ft2");
        //                 break;
        //         }

        //         snrt_ssr_disable();

        //         // TODO: Check if needs to be unrolled manually
        //         for (uint32_t i = 0; i < cleanup_unroll; i++) {
        //             k->pOutBuffer[(h0 * max_unroll + i) * output_h_stride +
        //                           w * output_w_stride + co] = sum[i];
        //         }
        //     }
        // }
    }

    snrt_fpu_fence();
}

