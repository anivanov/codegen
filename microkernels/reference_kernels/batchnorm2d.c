#include <snrt.h>
#include "snrt_mini.h"
#include <stdint.h>
#include <math.h>
#include <float.h>

#include <snrt.h>
#include <stddef.h>
#include <printf.h>
#include <math.h>

#include <stdbool.h>

#define u8 uint8_t
#define i32 int32_t
#define f32 float
#define f64 double
#define SNRT_SSR_DM_ALL 31

extern i32 N, C, H, W;

static inline void __attribute__((always_inline))
batchnorm_collect_statistics_fp64_no_loop(
    double* ifmap_scratch, double* current_mean_scratch,
    double* current_var_scratch,
    uint32_t num_points,  // of all batches. represents N in computation of mean
                          // and var
    uint32_t num_bytes_per_point,
    uint32_t num_points_work_for_core,  // requires: > 0
    uint32_t work_div_4_sub_1, uint32_t work_mod_4,
    uint32_t num_channels_to_process,  //  requires: > 0
    uint32_t channel_stride) {
    // access pattern: iterate over the different channels,
    //   then over the different points
    // Split work over channels to maximize efficacy of frep and avoid tcdm
    // contention. outside loop: channels inside loop: points
    snrt_ssr_loop_3d(
        SNRT_SSR_DM0,
        num_points_work_for_core,  // dimension of inner loop
        2,                         // repeat values once
        num_channels_to_process,   // dimension of outer loop
        num_bytes_per_point,       // stride per inner loop iteration: 1 point
        0,                         // repeat values once per channel
        channel_stride * sizeof(double));  // stride per outer loop iteration

    register volatile uint32_t i =
        0;  // updated during frep for pseudo-dual issue
    register uint32_t frep = num_points_work_for_core >= 4;

    register double ZERO = 0;
    register double mean asm ("ft9");

    // asm("ft3");  // can consider fcvt instead
    // asm volatile("fcvt.d.w %[ZERO], zero\n"
    //              : [ZERO] "=r"(ZERO)::"ft0", "ft1", "ft2");
    register double sum0 = 0, sum1 = 0, sum2 = 0, sum3 = 0;
    register double num_points_double = num_points;

    snrt_ssr_read(SNRT_SSR_DM0, SNRT_SSR_3D, ifmap_scratch);
    snrt_ssr_enable();
    // do 1 loop
    do {  // while (i < num_channels_to_process)
        if (frep) {
            asm volatile(
                FREP(frep_ops, frep_rep)
                "fadd.d %[sum0], ft0, %[sum0]\n"
                "fadd.d %[sum1], ft0, %[sum1]\n"
                "fadd.d %[sum2], ft0, %[sum2]\n"
                "fadd.d %[sum3], ft0, %[sum3]\n"
                : [sum0] "+fr"(sum0), [sum1] "+fr"(sum1), [sum2] "+fr"(sum2),
                  [sum3] "+fr"(sum3)
                : [frep_rep] "r"(work_div_4_sub_1), [frep_ops] "i"(3)  // we repeat n_frep+1 times
                : "ft0", "ft1", "ft2");
        }

        register uint32_t channel_stride_in_bytes;
        asm volatile(
            "slli %[channel_stride_in_bytes], %[channel_stride], 3\n"  // log_2(sizeof(double))
            "beqz %[i], 1f\n"
            "add %[current_mean_scratch], %[current_mean_scratch],%[channel_stride_in_bytes]\n"
            "add %[current_var_scratch], %[current_var_scratch],%[channel_stride_in_bytes]\n"
            "1:\n"
            "addi %[i], %[i], 1\n"
            : [current_mean_scratch] "+r"(current_mean_scratch),
              [current_var_scratch] "+r"(current_var_scratch), [i] "+r"(i),
              [channel_stride_in_bytes] "=&r"(channel_stride_in_bytes)
            : [channel_stride] "r"(channel_stride),
              [num_channels_to_process] "r"(num_channels_to_process)
            : "ft0", "ft1", "ft2");

        register uint32_t mod_temp;
        asm volatile(
            "beqz %[work_mod_4], 0f\n"              // mod is 0
            "andi %[mod_temp], %[work_mod_4], 1\n"  // is last bit 1? if no,
                                                    // then mod is 2
            "beqz %[mod_temp], 2f\n"                // jump to 2 if no
            "andi %[mod_temp], %[work_mod_4], 2\n"  // is last bit 1? if no,
                                                    // then mod is 1
            "beqz %[mod_temp], 1f\n"                // jump to 1 if no
            "3:\n"
            "fadd.d %[sum0], ft0, %[sum0]\n"
            "fadd.d %[sum1], ft0, %[sum1]\n"
            "fadd.d %[sum2], ft0, %[sum2]\n"
            "j 0f\n"
            "2:\n"
            "fadd.d %[sum0], ft0, %[sum0]\n"
            "fadd.d %[sum1], ft0, %[sum1]\n"
            "j 0f\n"
            "1:\n"
            "fadd.d %[sum0], ft0, %[sum0]\n"
            "0:\n"
            "fadd.d %[sum2], %[sum2], %[sum3]\n"
            "fadd.d %[sum0], %[sum0], %[sum1]\n"
            "fsgnj.d %[sum3], %[ZERO], %[ZERO]\n"
            "fsgnj.d %[sum1], %[ZERO], %[ZERO]\n"
            "fadd.d %[sum0], %[sum0], %[sum2]\n"
            "fsgnj.d %[sum2], %[ZERO], %[ZERO]\n"
            "fdiv.d %[mean], %[sum0], %[num_points_double]\n"
            "fsgnj.d %[sum0], %[ZERO], %[ZERO]\n"
            : [mod_temp] "=r"(mod_temp), [sum0] "+fr"(sum0), [sum1] "+fr"(sum1),
              [sum2] "+fr"(sum2), [sum3] "+fr"(sum3), [mean] "=fr"(mean)
            : [work_mod_4] "r"(work_mod_4), [ZERO] "fr"(ZERO),
              [num_points_double] "fr"(num_points_double)
            : "ft0", "ft1", "ft2");
        if (frep) {
            asm volatile(
                FREP(frep_ops, frep_rep)
                "fsub.d ft3, ft0, %[mean]\n"
                "fsub.d ft4, ft0, %[mean]\n"
                "fsub.d ft5, ft0, %[mean]\n"
                "fsub.d ft6, ft0, %[mean]\n"
                "fmadd.d %[sum0], ft3, ft3, %[sum0]\n"
                "fmadd.d %[sum1], ft4, ft4, %[sum1]\n"
                "fmadd.d %[sum2], ft5, ft5, %[sum2]\n"
                "fmadd.d %[sum3], ft6, ft6, %[sum3]\n"
                : [sum0] "+fr"(sum0), [sum1] "+fr"(sum1), [sum2] "+fr"(sum2),
                  [sum3] "+fr"(sum3)
                : [frep_rep] "r"(work_div_4_sub_1),  // we repeat n_frep+1 times
                  [mean] "fr"(mean), [frep_ops] "i"(7)
                : "ft0", "ft1", "ft2", "ft3", "ft4", "ft5", "ft6");
        }
        register double pop_variance;
        asm volatile(
            "beqz %[work_mod_4], 0f\n"              // mod is 0
            "andi %[mod_temp], %[work_mod_4], 1\n"  // is last bit 1? if no,
                                                    // then mod is 2
            "beqz %[mod_temp], 2f\n"                // jump to 2 if no
            "andi %[mod_temp], %[work_mod_4], 2\n"  // is last bit 1? if no,
                                                    // then mod is 1
            "beqz %[mod_temp], 1f\n"                // jump to 1 if no
            "3:\n"
            "fsub.d ft3, ft0, %[mean]\n"
            "fsub.d ft4, ft0, %[mean]\n"
            "fsub.d ft5, ft0, %[mean]\n"
            "fmadd.d %[sum0], ft3, ft3, %[sum0]\n"
            "fmadd.d %[sum1], ft4, ft4, %[sum1]\n"
            "fmadd.d %[sum2], ft5, ft5, %[sum2]\n"
            "j 0f\n"
            "2:\n"
            "fsub.d ft3, ft0, %[mean]\n"
            "fsub.d ft4, ft0, %[mean]\n"
            "fmadd.d %[sum0], ft3, ft3, %[sum0]\n"
            "fmadd.d %[sum1], ft4, ft4, %[sum1]\n"
            "j 0f\n"
            "1:\n"
            "fsub.d ft3, ft0, %[mean]\n"
            "fmadd.d %[sum0], ft3, ft3, %[sum0]\n"
            "0:\n"
            "fadd.d %[sum2], %[sum2], %[sum3]\n"
            "fadd.d %[sum0], %[sum0], %[sum1]\n"
            "fsgnj.d %[sum3], %[ZERO], %[ZERO]\n"
            "fsgnj.d %[sum1], %[ZERO], %[ZERO]\n"
            "fadd.d %[sum0], %[sum0], %[sum2]\n"
            "fsgnj.d %[sum2], %[ZERO], %[ZERO]\n"
            "fdiv.d %[pop_variance], %[sum0], %[num_points_double]\n"
            "fsgnj.d %[sum0], %[ZERO], %[ZERO]\n"
            "fsd %[mean], 0(%[current_mean_scratch])\n"
            "fsd %[pop_variance], 0(%[current_var_scratch])\n"
            : [mod_temp] "=&r"(mod_temp), [sum0] "+fr"(sum0),
              [sum1] "+fr"(sum1), [sum2] "+fr"(sum2), [sum3] "+fr"(sum3),
              [pop_variance] "=&fr"(pop_variance)
            : [work_mod_4] "r"(work_mod_4), [ZERO] "fr"(ZERO),
              [num_points_double] "fr"(num_points_double), [mean] "fr"(mean),
              [current_mean_scratch] "r"(current_mean_scratch),
              [current_var_scratch] "r"(current_var_scratch)
            : "ft0", "ft1", "ft2", "ft3", "ft4", "ft5");
    } while (i < num_channels_to_process);
    // don't need to fpu_fence since last 3 instructions are inconsequential
    snrt_ssr_disable();
}


static inline void __attribute__((always_inline)) batchnorm_forward_fp64_no_loop(
    double* ifmap_scratch, double* ofmap_scratch,
    const double* gamma_scratch, const double* beta_scratch,
    uint32_t num_bytes_per_point,
    uint32_t num_points_work_for_core,  // requires: > 0
    uint32_t work_sub_1,
    uint32_t num_channels_to_process,  //  requires: > 0
    uint32_t channel_stride) {
    // access pattern: iterate over the different channels,
    //   then over the different points
    // Split work over channels to maximize efficacy of frep and avoid tcdm
    // contention. outside loop: channels inside loop: points
    snrt_ssr_loop_2d(
        SNRT_SSR_DM0,
        num_points_work_for_core,  // dimension of inner loop
        num_channels_to_process,   // dimension of outer loop
        num_bytes_per_point,       // stride per inner loop iteration: 1 point
        channel_stride * sizeof(double));  // stride per outer loop iteration
    snrt_ssr_loop_2d(
        SNRT_SSR_DM1,
        num_points_work_for_core,  // dimension of inner loop
        num_channels_to_process,   // dimension of outer loop
        num_bytes_per_point,       // stride per inner loop iteration: 1 point
        channel_stride * sizeof(double));  // stride per outer loop iteration

    register volatile uint32_t i =
        0;  // updated during frep for pseudo-dual issue
    snrt_ssr_read(SNRT_SSR_DM0, SNRT_SSR_2D, ifmap_scratch);
    snrt_ssr_write(SNRT_SSR_DM1, SNRT_SSR_2D, ofmap_scratch);
    snrt_ssr_enable();
    // do 1 loop
    do {  // while (i < num_channels_to_process)
        register double gamma = *gamma_scratch;
        register double beta = *beta_scratch;
        asm volatile(
            FREP(frep_ops, frep_rep)
            "fmadd.d ft1, ft0, %[gamma], %[beta]"
            :
            : [gamma] "fr"(gamma), [beta] "fr"(beta),
              [frep_rep] "r"(work_sub_1), [frep_ops] "i"(0)  // we repeat n_frep+1 times
            : "ft0", "ft1", "ft2");

        register uint32_t channel_stride_in_bytes;
        asm volatile(
            "slli %[channel_stride_in_bytes], %[channel_stride], 3\n"  // log_2(sizeof(double))
            "addi %[i], %[i], 1\n"
            "beq %[num_channels_to_process], %[i], 2f\n"  // shortcut when only
                                                          // 1 channel
            "add %[gamma_scratch], %[gamma_scratch], %[channel_stride_in_bytes]\n"
            "add %[beta_scratch], %[beta_scratch],%[channel_stride_in_bytes]\n"
            "2:\n"
            : [gamma_scratch] "+r"(gamma_scratch),
              [beta_scratch] "+r"(beta_scratch), [i] "+r"(i),
              [channel_stride_in_bytes] "+r"(channel_stride_in_bytes)
            : [channel_stride] "r"(channel_stride),
              [num_channels_to_process] "r"(num_channels_to_process)
            : "ft0", "ft1", "ft2");

    } while (i < num_channels_to_process);
    snrt_fpu_fence();
    snrt_ssr_barrier(SNRT_SSR_DM1);
    snrt_ssr_disable();
}


void batchnorm2d_no_training(f64* __restrict__ X, f64* __restrict__ bias, f64 epsilon, f64* __restrict__ var_eps, f64* __restrict__ input_var, f64 momentum, f64* __restrict__ scale, f64* __restrict__ diff, f64* __restrict__ running_mean, f64* __restrict__ running_var_l, void* __restrict__ _tmp)
{
    //(X, bias, epsilon, input_mean, input_var, momentum, scale, result_Y, result_running_mean, result_running_var, _tmp)
    // Calculate output dimensions

    uint32_t num_points = N * H * W;

    snrt_ssr_loop_2d(SNRT_SSR_DM0, 2, C, C * sizeof(double), sizeof(double));
    snrt_ssr_loop_2d(SNRT_SSR_DM1, 2, C, C * sizeof(double), sizeof(double));
    snrt_ssr_loop_2d(SNRT_SSR_DM2, 2, C, C * sizeof(double), sizeof(double));
    
    snrt_ssr_read(SNRT_SSR_DM0, SNRT_SSR_2D, input_var);
    snrt_ssr_write(SNRT_SSR_DM1, SNRT_SSR_2D, running_var_l);
    snrt_ssr_read(SNRT_SSR_DM2, SNRT_SSR_2D, scale);
    register double eps = epsilon;
    snrt_ssr_enable();
    // Reduce the four variables running_mean, running_var, weight, bias
    // into an fmadd for main loop see
    // https://github.com/pytorch/pytorch/blob/3acaf8564da4c2f0faaea33ce4572ad9b3715b47/aten/src/ATen/native/cpu/batch_norm_kernel.cpp#L45-L55
    asm volatile(
        FREP(frep_ops, frep_rep)
        "fadd.d ft3, ft0, %[eps]\n"
        "fsqrt.d ft3, ft3\n"
        "fdiv.d ft3, ft2, ft3\n"
        "fsgnj.d ft1, ft3, ft3\n"
        "fnmsub.d ft1, ft0, ft3, ft2\n"
        :
        : [eps] "fr"(eps),
            [frep_rep] "r"(C - 1), [frep_ops] "i"(5 - 1)  // we repeat n_frep+1 times
        : "ft0", "ft1", "ft2", "ft3");

    snrt_ssr_barrier(SNRT_SSR_DM1);

    batchnorm_forward_fp64_no_loop(
                X, diff, running_var_l, running_mean,
                C * sizeof(double), num_points, num_points - 1, C, 1);

    snrt_fpu_fence();

}



void batchnorm2d(f64* __restrict__ X, f64* __restrict__ bias, f64 epsilon, f64* __restrict__ var_eps, f64* __restrict__ input_var, f64 momentum, f64* __restrict__ scale, f64* __restrict__ diff, f64* __restrict__ running_mean, f64* __restrict__ running_var_l, void* __restrict__ _tmp)
{
    //(X, bias, epsilon, input_mean, input_var, momentum, scale, result_Y, result_running_mean, result_running_var, _tmp)

    uint32_t num_points = N * H * W;
    const double one_sub_momentum = 1 - momentum;
    const double momentum_times_unbiased_correction =
        momentum * ((double)num_points / (double)(num_points - 1));

    double *current_var_scratch = input_var;
    double *current_mean_scratch = var_eps;
    double *running_var_scratch = running_var_l;
    double *running_mean_scratch = running_mean;
    double *gamma_scratch = input_var;
    double *weight_scratch = scale;
    double *beta_scratch = var_eps;
    double *bias_scratch = bias;


    batchnorm_collect_statistics_fp64_no_loop(
        X, current_mean_scratch, current_var_scratch,
        num_points, C * sizeof(double), num_points, num_points / 4 - 1,
        num_points % 4, C, 1);
    // need to collect current_mean = sum(x) / N
    // need to collect current_var = sum((x-current_mean)^2) / N
    snrt_ssr_loop_2d(SNRT_SSR_DM1, 4, C, C * sizeof(double),
                     sizeof(double));
    snrt_ssr_loop_2d(SNRT_SSR_DM2, 4, C, C * sizeof(double),
                     sizeof(double));
    snrt_ssr_loop_2d(SNRT_SSR_DM0, 2, C, C * sizeof(double),
                     sizeof(double));
    snrt_ssr_repeat(SNRT_SSR_DM0, 2);
    // consume current var (twice) then current mean (twice)
    snrt_ssr_read(SNRT_SSR_DM0, SNRT_SSR_2D, current_var_scratch);
    // write to running_var, running_mean, gamma, beta
    snrt_ssr_write(SNRT_SSR_DM1, SNRT_SSR_2D, running_var_scratch);
    // consume running_var, running_mean, weight, bias
    snrt_ssr_read(SNRT_SSR_DM2, SNRT_SSR_2D, running_var_scratch);
    register double eps = epsilon;
    snrt_ssr_enable();
    // Reduce the four variables running_mean, running_var, weight, bias
    // into an fmadd for main loop see
    // https://github.com/pytorch/pytorch/blob/3acaf8564da4c2f0faaea33ce4572ad9b3715b47/aten/src/ATen/native/cpu/batch_norm_kernel.cpp#L45-L55
    // Moreover, update statistics for running_mean and running_var.
    // Use unbiased estimator for updating statistic, but biased
    // estimator for gamma/beta
    asm volatile(
        FREP(frep_ops, frep_rep)
        // current_var + eps
        "fadd.d ft3, ft0, %[eps]\n"
        // current_var * momentum
        "fmul.d ft4, ft0, %[momentum_times_unbiased_correction]\n"
        // current_mean * momentum
        "fmul.d ft5, ft0, %[momentum]\n"
        // sqrt(current_var + eps)
        "fsqrt.d ft3, ft3\n"
        // running_var update
        "fmadd.d ft1, ft2, %[one_sub_momentum], ft4\n"
        // running_mean update
        "fmadd.d ft1, ft2, %[one_sub_momentum], ft5\n"
        // weight / sqrt()
        "fdiv.d ft3, ft2, ft3\n"
        // write gamma (weight already consumed)
        "fsgnj.d ft1, ft3, ft3\n"
        // consume bias, current_mean and write beta
        "fnmsub.d ft1, ft0, ft3, ft2\n"
        :
        : [eps] "fr"(eps), [one_sub_momentum] "fr"(one_sub_momentum),
            [momentum] "fr"(momentum),
            [momentum_times_unbiased_correction] "fr"(
                momentum_times_unbiased_correction),
            [frep_rep] "r"(C - 1), [frep_ops] "i"(9 - 1)  // we repeat n_frep+1 times
        : "ft0", "ft1", "ft2", "ft3", "ft4", "ft5");

            snrt_fpu_fence();
            snrt_ssr_repeat(SNRT_SSR_DM0, 1);
            snrt_ssr_barrier(SNRT_SSR_DM1);
            snrt_ssr_disable();

    // calc gamma beta, dma loads in first tile for ofmap
            batchnorm_forward_fp64_no_loop(
                X, diff, gamma_scratch, beta_scratch,
                C * sizeof(double), num_points, num_points - 1, C, 1);
}

