#include "snrt.h"
#include "snrt_mini.h"
#include <stdint.h>
#include <math.h>
#include <float.h>
#define u8 uint8_t
#define i32 int32_t
#define f32 float
#define f64 double
//#include "math.h"


extern i32 B, N;

void softmax(f64* __restrict__ src, f64* __restrict__ src_unused1, void* __restrict__ _tmp) {

// static inline void softmax_fp32(float *input, float *output, int32_t ldI,
//                                 int32_t batch_offset, int32_t batch_size,
//                                 int32_t seq_len, int32_t input_samples) {
    f64 *input = src;
    f64 *output = src;
    i32 batch_offset = 0, batch_size = 1, seq_len = B, input_samples = N;
    i32 ldI = N;
    f64 max_core = 0.0;  // max value of the current core
    f64 sum = 0.0;       // sum of the exp values of the current core


    for (i32 b = 0; b < batch_size; b++) {
        for (i32 s = 0; s < seq_len; s++) {
            max_core = -DBL_MAX;
            sum = 0.0;

            for (i32 i = 0; i < input_samples; i++) {
                if (input[b * batch_offset + s * ldI + i] > max_core) {
                    max_core = input[b * batch_offset + s * ldI + i];
                }
            }

            // compute the shifted value of the current row
            for (i32 i = 0; i < input_samples; i++) {
                output[b * batch_offset + s * ldI + i] =
                    expf(input[b * batch_offset + s * ldI + i] - max_core);
                sum += output[b * batch_offset + s * ldI + i];
            }

            // compute the softmax value of the current row
            for (i32 i = 0; i < input_samples; i++) {
                output[b * batch_offset + s * ldI + i] = my_div(output[b * batch_offset + s * ldI + i], sum);
            }
        }
    }

    snrt_cluster_hw_barrier();
}