#include "snrt.h"
#include "snrt_mini.h"
#include <stdint.h>
#include <math.h>
#include <float.h>
#define u8 uint8_t
#define i32 int32_t
#define f32 float
#define f64 double


extern i32 B, N;

void func_sum(f64* __restrict__ src1, f64* __restrict__ src2, f64* __restrict__ dst, void* __restrict__ _tmp) {

    snrt_ssr_enable();
    // ssr 0 use start
    snrt_ssr_loop_1d(SNRT_SSR_DM0, B*N, sizeof(f64));
    snrt_ssr_repeat(SNRT_SSR_DM0, 1);
    snrt_ssr_write(SNRT_SSR_DM0, SNRT_SSR_1D, &dst[0]);
    // ssr 1 use start
    snrt_fpu_fence();
    snrt_ssr_loop_1d(SNRT_SSR_DM1, B*N, sizeof(f64));
    snrt_ssr_repeat(SNRT_SSR_DM1, 1);
    snrt_ssr_read(SNRT_SSR_DM1, SNRT_SSR_1D, &src1[0]);
    // ssr 2 use start
    snrt_fpu_fence();
    snrt_ssr_loop_1d(SNRT_SSR_DM2, B*N, sizeof(f64));
    snrt_ssr_repeat(SNRT_SSR_DM2, 1);
    snrt_ssr_read(SNRT_SSR_DM2, SNRT_SSR_1D, &src2[0]);
    asm volatile(
        FREP(frep_ops, frep_rep)
        "fadd.d ft0, ft1, ft2;"
        : 
        : [frep_rep] "r"(B*N - 1), [frep_ops] "i"(1 - 1)
        : "ft0", "ft1", "ft2", "memory"
    );
    snrt_ssr_barrier(SNRT_SSR_DM0);
    // ssr 0 use end
    // ssr 1 use end
    // ssr 2 use end
    snrt_ssr_disable();
}