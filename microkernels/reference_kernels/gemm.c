#include "snrt.h"
#include "snrt_mini.h"
#include <stdint.h>
#include <math.h>
#include <float.h>
#define u8 uint8_t
#define i32 int32_t
#define f32 float
#define f64 double

#define SINGLECORE_OPT

// Copyright 2023 ETH Zurich and University of Bologna.
// Licensed under the Apache License, Version 2.0, see LICENSE for details.
// SPDX-License-Identifier: Apache-2.0
//
// Author: Tim Fischer <fischeti@iis.ee.ethz.ch>
//         Luca Bertaccini <lbertaccini@iis.ee.ethz.ch>
//         Luca Colagrande <colluca@iis.ee.ethz.ch>
//         Viviane Potocnik <vivianep@iis.ee.ethz.ch>

extern i32 M, N, K;

void gemm(f64* __restrict__ A, f64* __restrict__ B, f64* __restrict__ D, f64 alpha, f64 beta, f64* __restrict__ D_unused1, void* __restrict__ _tmp) {
    f64* C = D;
    i32 ldA = K, ldB = N, ldC = N;
    i32 setup_SSR = 1;
    i32 ta = 0, tb = 0;


    // Unrolling factor of most inner loop.
    // Should be at least as high as the FMA delay
    // for maximum utilization
    // const uint32_t unroll = 8;
    const uint32_t unroll = 8;
    const double alp = alpha;
    const double bet = beta / alp;
    const uint32_t zero_beta = bet == 0.0;
    const uint32_t one_beta = bet == 1.0;
    register double ZERO = 0.0;

    // if (M==0)
    //     return;

    // SSR strides and bounds only have to be configured
    // once in the beginning
    if (setup_SSR) {
        // First matrix is stored in transposed format
        if (ta) {
            const uint32_t ssr0_b[4] = {unroll, K, N / unroll, M};
#ifdef MULTICORE_OPT
            const uint32_t ssr0_i[4] = {0, 8 * ldA, 0, 8 * 8};
#endif
#ifdef SINGLECORE_OPT
            const uint32_t ssr0_i[4] = {0, 8 * ldA, 0, 8 * 1};
#endif

            snrt_ssr_loop_3d(SNRT_SSR_DM0, ssr0_b[1], ssr0_b[2], ssr0_b[3],
                             ssr0_i[1], ssr0_i[2], ssr0_i[3]);
            snrt_ssr_repeat(SNRT_SSR_DM0, unroll);
        } else {
            const uint32_t ssr0_b[4] = {unroll, K, N / unroll, M};
            const uint32_t ssr0_i[4] = {0, 8, 0, 8 * ldA};

            // A[k + unroll * m * ldA]
            snrt_ssr_loop_3d(SNRT_SSR_DM0, ssr0_b[1], ssr0_b[2], ssr0_b[3],
                             ssr0_i[1], ssr0_i[2], ssr0_i[3]);
            snrt_ssr_repeat(SNRT_SSR_DM0, unroll);
        }

        // Second matrix is stored in transposed format
        if (tb) {
            const uint32_t ssr1_b[4] = {unroll, K, N / unroll, M};
            const uint32_t ssr1_i[4] = {8 * ldB, 8, 8 * ldB * unroll, 0};

            snrt_ssr_loop_4d(SNRT_SSR_DM1, ssr1_b[0], ssr1_b[1], ssr1_b[2],
                             ssr1_b[3], ssr1_i[0], ssr1_i[1], ssr1_i[2],
                             ssr1_i[3]);
        } else {
            const uint32_t ssr1_b[4] = {unroll, K, N / unroll, M};
            const uint32_t ssr1_i[4] = {8, 8 * ldB, 8 * unroll, 0};

            // B[k + unroll * n * ldB]
            snrt_ssr_loop_4d(SNRT_SSR_DM1, ssr1_b[0], ssr1_b[1], ssr1_b[2],
                             ssr1_b[3], ssr1_i[0], ssr1_i[1], ssr1_i[2],
                             ssr1_i[3]);
        }
    }

    // SSR start address need to be configured each time
    snrt_ssr_read(SNRT_SSR_DM0, SNRT_SSR_4D, A);
    snrt_ssr_read(SNRT_SSR_DM1, SNRT_SSR_4D, B);

    uint32_t n_u = N/unroll;
    

    for (uint32_t m = 0; m < M; m++) {
        uint32_t n = 0;
        for (uint32_t n0 = 0; n0 < n_u; n0++) {
            double c[unroll];


            snrt_ssr_enable();
            asm volatile(
                "bnez %[zero_beta], 1f\n"
                "bnez %[one_beta], 2f\n"

                "fld ft10, 0(%[sum_addr]) \n"
                "fmul.d ft10, ft10, %[beta] \n"
                "fld ft3, 8(%[sum_addr]) \n"
                "fmul.d ft3, ft3, %[beta]\n"
                "fld ft4, 16(%[sum_addr]) \n"
                "fmul.d ft4, ft4, %[beta]\n"
                "fld ft5, 24(%[sum_addr]) \n"
                "fmul.d ft5, ft5, %[beta]\n"
                "fld ft6, 32(%[sum_addr]) \n"
                "fmul.d ft6, ft6, %[beta]\n"
                "fld ft7, 40(%[sum_addr]) \n"
                "fmul.d ft7, ft7, %[beta]\n"
                "fld ft8, 48(%[sum_addr]) \n"
                "fmul.d ft8, ft8, %[beta]\n"
                "fld ft9, 56(%[sum_addr]) \n"
                "fmul.d ft9, ft9, %[beta]\n"
                "j 3f\n"

                "1:\n"
                "fcvt.d.w %[ZERO], zero\n"
                "fsgnj.d ft10,%[ZERO],%[ZERO]\n"
                "fsgnj.d ft3,%[ZERO],%[ZERO]\n"
                "fsgnj.d ft4,%[ZERO],%[ZERO]\n"
                "fsgnj.d ft5,%[ZERO],%[ZERO]\n"
                "fsgnj.d ft6,%[ZERO],%[ZERO]\n"
                "fsgnj.d ft7,%[ZERO],%[ZERO]\n"
                "fsgnj.d ft8,%[ZERO],%[ZERO]\n"
                "fsgnj.d ft9,%[ZERO],%[ZERO]\n"
                "j 3f\n"            

                "2:\n"
                "fld ft10, 0(%[sum_addr]) \n"
                "fld ft3, 8(%[sum_addr]) \n"
                "fld ft4, 16(%[sum_addr]) \n"
                "fld ft5, 24(%[sum_addr]) \n"
                "fld ft6, 32(%[sum_addr]) \n"
                "fld ft7, 40(%[sum_addr]) \n"
                "fld ft8, 48(%[sum_addr]) \n"
                "fld ft9, 56(%[sum_addr]) \n"

                "3:\n"
                FREP(unroll, n_frep)
                "fmadd.d ft10, ft0, ft1, ft10 \n"
                "fmadd.d ft3, ft0, ft1, ft3 \n"
                "fmadd.d ft4, ft0, ft1, ft4 \n"
                "fmadd.d ft5, ft0, ft1, ft5 \n"
                "fmadd.d ft6, ft0, ft1, ft6 \n"
                "fmadd.d ft7, ft0, ft1, ft7 \n"
                "fmadd.d ft8, ft0, ft1, ft8 \n"
                "fmadd.d ft9, ft0, ft1, ft9 \n"


                "fmul.d ft10, %[alpha], ft10 \n"
                "fmul.d ft3, %[alpha], ft3 \n"
                "fmul.d ft4, %[alpha], ft4 \n"
                "fmul.d ft5, %[alpha], ft5 \n"
                "fmul.d ft6, %[alpha], ft6 \n"
                "fmul.d ft7, %[alpha], ft7 \n"
                "fmul.d ft8, %[alpha], ft8 \n"
                "fmul.d ft9, %[alpha], ft9 \n"
                "fsd ft10, 0(%[sum_addr]) \n"
                "fsd ft3, 8(%[sum_addr]) \n"
                "fsd ft4, 16(%[sum_addr]) \n"
                "fsd ft5, 24(%[sum_addr]) \n"
                "fsd ft6, 32(%[sum_addr]) \n"
                "fsd ft7, 40(%[sum_addr]) \n"
                "fsd ft8, 48(%[sum_addr]) \n"
                "fsd ft9, 56(%[sum_addr]) \n"

                : 
                : [ n_frep ] "r"(K - 1), [ unroll ] "i"(unroll - 1), [ alpha ] "f"(alp), [ beta ] "f"(bet), [ zero_beta ] "r"(zero_beta), [ one_beta ] "r"(one_beta), [ sum_addr ] "r"(C + m * ldC + n), [ ZERO ] "f"(ZERO)
                : "ft0", "ft1", "ft10", "ft3", "ft5", "ft6", "ft7", "ft8", "ft9", "ft2", "a0");
            snrt_ssr_disable();

            n += unroll;
        }

        // Clean up of leftover columns


        for (; n < N; n++) {
            double c;
            if (!zero_beta) {
                if (one_beta)
                    c = C[m * ldC + n];
                else
                    c = C[m * ldC + n] * bet;
            } else {
                c = 0.0;
            }

                for (uint32_t k = 0; k < K; k++) {
                    c += A[m * ldA + k] * B[k * ldB + n];
                }


            C[m * ldC + n] = alp * c;
        }

        snrt_ssr_enable();
    }

    snrt_ssr_disable();
    snrt_fpu_fence();
}