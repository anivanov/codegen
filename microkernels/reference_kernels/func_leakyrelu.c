#include "snrt.h"
#include "snrt_mini.h"
#include <stdint.h>
#include <math.h>
#include <float.h>
#define u8 uint8_t
#define i32 int32_t
#define f32 float
#define f64 double


extern i32 B, N;

void func_leakyrelu(f64 alpha, f64* __restrict__ x, f64* __restrict__ dst, void* __restrict__ _tmp) {

    f64 max[4];
    f64 min[4];
    i32 unroll = 4;
    snrt_ssr_enable();
    // ssr 0 use start
    snrt_fpu_fence();
    snrt_ssr_loop_2d(SNRT_SSR_DM0, unroll, B * N / unroll, sizeof(f64), unroll * sizeof(f64));
    snrt_ssr_repeat(SNRT_SSR_DM0, 2);
    snrt_ssr_read(SNRT_SSR_DM0, SNRT_SSR_2D, x);
    // ssr 1 use start
    snrt_ssr_loop_2d(SNRT_SSR_DM1, unroll, B * N / unroll, sizeof(f64), unroll * sizeof(f64));
    snrt_ssr_write(SNRT_SSR_DM1, SNRT_SSR_2D, dst);
    asm volatile(
        FREP(frep_ops, frep_rep)
        "fmax.d %[c0], %[a0], ft0;"
        "fmin.d %[c1], %[a0], ft0;"
        "fmax.d %[c2], %[a0], ft0;"
        "fmin.d %[c3], %[a0], ft0;"
        "fmax.d %[c4], %[a0], ft0;"
        "fmin.d %[c5], %[a0], ft0;"
        "fmax.d %[c6], %[a0], ft0;"
        "fmin.d %[c7], %[a0], ft0;"
        "fmadd.d ft1, %[a8], %[c1], %[c0];"
        "fmadd.d ft1, %[a8], %[c3], %[c2];"
        "fmadd.d ft1, %[a8], %[c5], %[c4];"
        "fmadd.d ft1, %[a8], %[c7], %[c6];"
        : [c0] "+&f"(max[0]), [c1] "+&f"(min[0]), [c2] "+&f"(max[1]), [c3] "+&f"(min[1]), [c4] "+&f"(max[2]), [c5] "+&f"(min[2]), [c6] "+&f"(max[3]), [c7] "+&f"(min[3]), [a8] "+&f"(alpha)
        : [a0] "f"(0.0), [frep_rep] "r"(B * N / unroll - 1), [frep_ops] "i"(12 - 1)
        : "ft0", "ft1", "ft2", "memory"
    );
    // ssr 0 use end
    snrt_ssr_barrier(SNRT_SSR_DM1);
    // ssr 1 use end
    snrt_ssr_disable();
}
