from .gen import *

def apply_manual_gemm_transformations(program):
    #tile buffer for setting unroll length
    tile_size = 8
    tile_scope(program, 'acc#1', tile_size)
    tile_scope(program, 'alpha_acc#0', tile_size)
    tile_scope(program, 'D#0', tile_size)
    tile_buffer(program, 'acc', tile_size, 1)
    tile_buffer(program, 'alpha_acc', tile_size, 1)
    tile_buffer(program, 'B', tile_size, 1)
    tile_buffer(program, 'C', tile_size, 1)
    tile_buffer(program, 'D', tile_size, 1)
    reuse_buffers(program, 'C', 'D')

    # #enable ssr for reading input matrices
    enable_ssr(program, 'acc', 1)
    enable_ssr(program, 'acc', 2)
    increase_ssr_depth(program, 'acc', 2)
    increase_ssr_depth(program, 'acc', 2)
    increase_ssr_depth(program, 'acc', 2)
    increase_ssr_depth(program, 'acc', 1)
    increase_ssr_depth(program, 'acc', 1)
    increase_ssr_depth(program, 'acc', 1)
    activate_ssr(program, 'acc', 1, 0)
    activate_ssr(program, 'acc', 2, 1)

    # #apply unrolling over the orizontal direction of B
    swap_nested_scopes(program, 'acc#1')
    unroll_scope(program, 'acc#0')

    #enable frep for the accumulation loop
    enable_frep(program, 'acc#1',)

    #join scopes for reading and writing
    join_scopes(program, 'alpha_acc#2',)
    join_scopes(program, 'alpha_acc#1',)
    join_scopes(program, 'acc#3')
    join_scopes(program, 'acc#2')

    #move accumulation arrays to the stack
    reuse_arr_dims(program, 'acc', 0)
    reuse_arr_dims(program, 'alpha_acc', 0)
    reuse_arr_dims(program, 'alpha_acc', 1)
    reuse_arr_dims(program, 'acc', 1)
    move_buf_to_stack(program, 'acc')
    move_buf_to_stack(program, 'alpha_acc')

    #unroll write back loop
    unroll_scope(program, 'alpha_acc#0')
    unroll_scope(program, 'D#0')

    enable_ssr(program, 'D', 2)
    increase_ssr_depth(program, 'D', 2)
    increase_ssr_depth(program, 'D', 2)
    activate_ssr(program, 'D', 2, 2)



def apply_manual_softmax_transformations(program):

    #OPTIMIZE DIV
    swap_ops(program, 'inv_sum_val_inv_d2')
    swap_ops(program, 'inv_sum_val_inv_d2')
    swap_ops(program, 'inv_sum_val_inv_d2')
    swap_ops(program, 'inv_sum_val_inv_d2')
    swap_ops(program, 'inv_sum_val_inv_d2')
    swap_ops(program, 'inv_sum_val_inv_d2')
    split_scopes(program, 'inv_sum_val_inv_d2')

    #OPTIMIZE EXP
    apply_to_exhaustion(program, [
        (reuse_arr_dims, find_reusable_arr_dims),
        (reuse_buffers, find_reusable_buffers),
        (move_buf_to_stack, find_bufs_movable_to_stack),
    ])

    #OPTIMIZE EXP
    #use ssr to read and write from memory
    enable_ssr(program, 'exp_diff_v1', 1)
    activate_ssr(program, 'exp_diff_v1', 1, 0)
    enable_ssr(program, 'exp_diff', 0)
    activate_ssr(program, 'exp_diff', 0, 1)

    #enable frep
    enable_frep(program, 'exp_diff_v1#0')
    

    #OPTIMIZE DIV
    #apply ssr and frep to the fp computation
    enable_ssr(program, 'inv_sum_val_inv_d2', 1)
    enable_ssr(program, 'inv_sum_val', 2)
    merge_ssr(program, 'inv_sum_val_inv_d2', 1, 'inv_sum_val', 2)
    activate_ssr(program, 'inv_sum_val_inv_d2', 1, 0)


    enable_ssr(program, 'inv_sum_val_inv_f64_2', 1)
    enable_ssr(program, 'inv_sum_val_inv_f64_2', 2)
    enable_ssr(program, 'inv_sum_val_inv_inv1', 2)

    merge_ssr(program, 'inv_sum_val_inv_f64_2', 1, 'inv_sum_val_inv_f64_2', 2)
    merge_ssr(program, 'inv_sum_val_inv_f64_2', 1, 'inv_sum_val_inv_inv1', 2)
    activate_ssr(program, 'inv_sum_val_inv_f64_2', 1, 2)


    join_scopes(program, 'max_val#1')

    enable_ssr(program, 'sum_val', 1)
    increase_ssr_depth(program, 'sum_val', 1)
    activate_ssr(program, 'sum_val', 1, 1)
    enable_frep(program, 'sum_val#0')

    enable_ssr(program, 'dst', 0)
    enable_ssr(program, 'dst', 1)
    enable_ssr(program, 'dst', 2)
    increase_ssr_depth(program, 'dst', 0)
    increase_ssr_depth(program, 'dst', 1)
    increase_ssr_depth(program, 'dst', 2)
    activate_ssr(program, 'dst', 0, 0)
    activate_ssr(program, 'dst', 1, 1)
    activate_ssr(program, 'dst', 2, 2)
    enable_frep(program, 'dst#0')


def apply_manual_batchnorm_transformations(program):

    reuse_arr_dims(program, 'sqrt_var_eps_th_2', 0)
    reuse_arr_dims(program, 'sqrt_var_eps_th', 0)
    reuse_arr_dims(program, 'sqrt_var_eps_s2', 0)
    reuse_arr_dims(program, 'sqrt_var_eps_s1', 0)
    reuse_arr_dims(program, 'sqrt_var_eps_inv1', 0)
    reuse_arr_dims(program, 'sqrt_var_eps_i32', 0)
    reuse_arr_dims(program, 'sqrt_var_eps_f64_2_d2_2', 0)
    reuse_arr_dims(program, 'sqrt_var_eps_f64_2_d2', 0)
    reuse_arr_dims(program, 'sqrt_var_eps_f64_2_2', 0)
    reuse_arr_dims(program, 'sqrt_var_eps_f64_2', 0)
    reuse_arr_dims(program, 'sqrt_var_eps_f64', 0)
    reuse_arr_dims(program, 'sqrt_var_eps_f32_2', 0)
    reuse_arr_dims(program, 'sqrt_var_eps_f32', 0)
    reuse_arr_dims(program, 'sqrt_var_eps_d2', 0)
    reuse_buffers(program, 'sqrt_var_eps_th', 'sqrt_var_eps_th_2')
    reuse_buffers(program, 'sqrt_var_eps_s1', 'sqrt_var_eps_s2')
    reuse_buffers(program, 'sqrt_var_eps_i32', 'sqrt_var_eps_s2')
    reuse_buffers(program, 'sqrt_var_eps_f64_2_d2', 'sqrt_var_eps_th_2')
    reuse_buffers(program, 'sqrt_var_eps_f64_2_2', 'sqrt_var_eps_f64_2_d2_2')
    reuse_buffers(program, 'sqrt_var_eps_f64_2', 'sqrt_var_eps_th_2')
    reuse_buffers(program, 'sqrt_var_eps_f64', 'sqrt_var_eps_inv1')
    reuse_buffers(program, 'sqrt_var_eps_f32', 'sqrt_var_eps_f32_2')
    reuse_buffers(program, 'sqrt_var_eps', 'var_eps')
    reuse_buffers(program, 'running_var_r', 'var_eps')
    reuse_buffers(program, 'running_var', 'running_var_l')
    reuse_buffers(program, 'running_mean_r', 'var_eps')
    reuse_buffers(program, 'running_mean_l', 'running_var_l')
    reuse_buffers(program, 'input_mean', 'var_eps')
    reuse_buffers(program, 'diff2_sum', 'running_var_l')
    reuse_buffers(program, 'current_sum', 'running_var_l')
    reuse_buffers(program, 'beta', 'var_eps')
    reuse_buffers(program, 'alpha', 'input_var')
    #reuse_buffers(program, 'Y', 'diff')
    reuse_buffers(program, 'NHW_1', 'one_momentum')
    reuse_buffers(program, 'NH', 'one_momentum')
    reuse_buffers(program, 'INHW_1', 'NHW')

    reuse_arr_dims(program, 'diff', 0)
    #reuse_arr_dims(program, 'diff', 0)
    reuse_arr_dims(program, 'diff', 2)
    reuse_arr_dims(program, 'diff', 3)
    move_buf_to_stack(program, 'diff',)

    move_buf_to_stack(program, 'sqrt_var_eps_th_2',)
    move_buf_to_stack(program, 'sqrt_var_eps_s2',)
    move_buf_to_stack(program, 'sqrt_var_eps_inv1',)
    move_buf_to_stack(program, 'sqrt_var_eps_f64_2_d2_2',)
    move_buf_to_stack(program, 'sqrt_var_eps_f32_2',)
    move_buf_to_stack(program, 'sqrt_var_eps_d2',)

    swap_nested_scopes(program, "current_sum#3")
    tile_scope(program, "current_sum#3", 4)
    swap_nested_scopes(program, "current_sum#3")
    swap_nested_scopes(program, "current_sum#2")
    swap_nested_scopes(program, "current_sum#1")
    enable_ssr(program, "current_sum", 1)
    increase_ssr_depth(program, "current_sum", 1)
    increase_ssr_depth(program, "current_sum", 1)
    increase_ssr_depth(program, "current_sum", 1)
    #increase_ssr_depth(program, "current_sum", 1)
    activate_ssr(program, "current_sum", 1, 0)
    unroll_scope(program, "current_sum#0")
    enable_frep(program, "current_sum#3")

    tile_scope(program, "current_mean#0", 4)
    unroll_scope(program, "current_mean#0")
    join_scopes(program, "current_sum#4")


    swap_nested_scopes(program, "diff#3")
    tile_scope(program, "diff#3", 4)
    swap_nested_scopes(program, "diff#3")
    swap_nested_scopes(program, "diff#2")
    swap_nested_scopes(program, "diff#1")
    enable_ssr(program, "diff", 1)
    increase_ssr_depth(program, "diff", 1)
    increase_ssr_depth(program, "diff", 1)
    increase_ssr_depth(program, "diff", 1)
    activate_ssr(program, "diff", 1, 1)
    unroll_scope(program, "diff#0")

    swap_nested_scopes(program, "diff2_sum#3")
    tile_scope(program, "diff2_sum#3", 4)
    swap_nested_scopes(program, "diff2_sum#3")
    swap_nested_scopes(program, "diff2_sum#2")
    swap_nested_scopes(program, "diff2_sum#1")
    unroll_scope(program, "diff2_sum#0")


    join_scopes(program, "diff#4")
    join_scopes(program, "diff#3")
    join_scopes(program, "diff#2")
    join_scopes(program, "diff#1")

    enable_frep(program, "diff#3")

    join_scopes(program, 'current_var_biased#0',)
    join_scopes(program, 'current_var_biased#0',)
    join_scopes(program, 'current_var_biased#0',)
    join_scopes(program, 'current_var_biased#0',)
    join_scopes(program, 'current_var_biased#0',)
    join_scopes(program, 'current_var_biased#0',)
    join_scopes(program, 'current_var_biased#0',)
    join_scopes(program, 'current_var_biased#0',)
    join_scopes(program, 'current_var_biased#0',)
    join_scopes(program, 'current_var_biased#0',)
    join_scopes(program, 'current_var_biased#0',)

    swap_nested_scopes(program, "Y#3")
    enable_ssr(program, "Y", 0)
    increase_ssr_depth(program, "Y", 0)
    increase_ssr_depth(program, "Y", 0)
    increase_ssr_depth(program, "Y", 0)
    activate_ssr(program, "Y", 0, 0)
    enable_ssr(program, "Y", 1)
    increase_ssr_depth(program, "Y", 1)
    increase_ssr_depth(program, "Y", 1)
    increase_ssr_depth(program, "Y", 1)
    activate_ssr(program, "Y", 1, 1)
    enable_frep(program, "Y#2")


def apply_manual_elem_by_elem_bop_transformations(program):

    untile_buffer(program, 'dst', 0)
    untile_buffer(program, 'src1', 0)
    untile_buffer(program, 'src2', 0)
    untile_scope(program, 'dst#1')

    enable_ssr(program, 'dst', 2)
    enable_ssr(program, 'dst', 1)
    enable_ssr(program, 'dst', 0)
    activate_ssr(program, 'dst', 2, 2)
    activate_ssr(program, 'dst', 1, 1)
    activate_ssr(program, 'dst', 0, 0)

    enable_frep(program, 'dst#0')


def apply_manual_elem_by_elem_uop_transformations(program):
    untile_buffer(program, 'dst', 0)
    untile_buffer(program, 'src', 0)
    untile_scope(program, 'dst#1')
    enable_ssr(program, 'dst', 1)
    enable_ssr(program, 'dst', 0)
    activate_ssr(program, 'dst', 1, 2)
    activate_ssr(program, 'dst', 0, 1)
    enable_frep(program, 'dst#0')



def apply_manual_leakyrelu_transformations(program):
    #untile
    untile_buffer(program, 'x', 0)
    untile_buffer(program, 'min', 0)
    untile_buffer(program, 'max', 0)
    untile_buffer(program, 'dst', 0)
    untile_scope(program, 'max#1')

    #unroll
    tile_size = 4
    tile_scope(program, 'max#0', tile_size)
    tile_buffer(program, 'x', tile_size, 0)
    tile_buffer(program, 'max', tile_size, 0)
    tile_buffer(program, 'min', tile_size, 0)
    tile_buffer(program, 'dst', tile_size, 0)

    split_scopes(program, 'dst')
    unroll_scope(program, 'max#0')
    # #unroll_scope(program, 'min#0')
    unroll_scope(program, 'dst#0')
    # put buffers into stack
    reuse_arr_dims(program, 'max', 0)
    reuse_arr_dims(program, 'min', 0)
    move_buf_to_stack(program, 'max')
    move_buf_to_stack(program, 'min')

    #configure ssr
    enable_ssr(program, 'max', 2)
    enable_ssr(program, 'min', 2)
    merge_ssr(program, 'max', 2, 'min', 2)
    enable_ssr(program, 'dst', 0)
    increase_ssr_depth(program, 'max', 2)
    increase_ssr_depth(program, 'dst', 0)
    activate_ssr(program, 'max', 2, 0)
    activate_ssr(program, 'dst', 0, 1)

    enable_frep(program, 'max#1')


def apply_manual_conv2d_transformations(program):

    tile_size = 4
    tile_scope(program, 'tmp#3', tile_size)
    tile_scope(program, 'output#1', tile_size)
    tile_buffer(program, 'tmp', tile_size, 2)
    tile_buffer(program, 'output', tile_size, 2)
    #tile_buffer(program, 'input', tile_size, 2) #you can't tile a buffer if it's accessed with index expression


    swap_nested_scopes(program, 'tmp#3')
    swap_nested_scopes(program, 'tmp#2')
    swap_nested_scopes(program, 'tmp#1')

    swap_nested_scopes(program, 'tmp#5')
    swap_nested_scopes(program, 'tmp#4')
    swap_nested_scopes(program, 'tmp#3')
    swap_nested_scopes(program, 'tmp#2')

    swap_nested_scopes(program, 'output#1')

    join_scopes(program, 'tmp#7')
    join_scopes(program, 'tmp#6')
    join_scopes(program, 'tmp#5')
    join_scopes(program, 'tmp#4')

    unroll_scope(program, 'tmp#0')

    #reuse_arr_dims(program, 'tmp', 3)
    reuse_arr_dims(program, 'tmp', 0)
    reuse_arr_dims(program, 'tmp', 1)
    reuse_arr_dims(program, 'tmp', 2)
    reuse_arr_dims(program, 'tmp', 4)
    move_buf_to_stack(program, 'tmp',)


    enable_ssr(program, 'tmp', 1)
    enable_ssr(program, 'tmp', 2)
    increase_ssr_depth(program, 'tmp', 1)
    increase_ssr_depth(program, 'tmp', 2)
    increase_ssr_depth(program, 'tmp', 1)
    increase_ssr_depth(program, 'tmp', 2)
    increase_ssr_depth(program, 'tmp', 1)
    increase_ssr_depth(program, 'tmp', 2)
    activate_ssr(program, 'tmp', 1, 0)
    activate_ssr(program, 'tmp', 2, 1)

    enable_frep(program, 'tmp#3')




# def apply_manual_logsoftmax_transformations(program):
#     tile_buffer(program, "src", 0, 4)
#     swap_nested_scopes(program, "max_val#1")
#     swap_nested_scopes(program, "diff#1")
#     swap_nested_scopes(program, "exp_diff_v0#1")
#     swap_nested_scopes(program, "sum_val#1")
#     swap_nested_scopes(program, "x#1")
#     join_scopes(program, "diff#2")
#     join_scopes(program, "diff#1")
#     join_scopes(program, "diff#2")
#     join_scopes(program, "diff#1")
#     join_scopes(program, "diff#0")
#     join_scopes(program, "diff#0")
#     join_scopes(program, "x#2")
#     swap_nested_scopes(program, "dst_rt2x#1")
#     join_scopes(program, "x#1")
    
#     split_scopes(program, "exp_diff_v0")
#     split_scopes(program, "exp_diff_v1")
#     split_scopes(program, "exp_diff_v2")
#     split_scopes(program, "exp_diff_v3")
#     split_scopes(program, "exp_diff_v4")
#     split_scopes(program, "exp_diff_v5")
#     split_scopes(program, "exp_diff_v6")
#     split_scopes(program, "exp_diff_v7")
#     split_scopes(program, "exp_diff_v8")
#     split_scopes(program, "exp_diff_v9")
#     split_scopes(program, "exp_diff_v10")
#     split_scopes(program, "exp_diff")
#     split_scopes(program, "sum_val")
    
#     split_scopes(program, "dst_rt4x")
#     split_scopes(program, "dst_rt3_4x")
#     split_scopes(program, "dst_x_1")
#     split_scopes(program, "dst_rt4x32")
#     split_scopes(program, "dst_rt2x12")
#     split_scopes(program, "dst_rt3_4x32")
#     split_scopes(program, "dst_x7")
#     split_scopes(program, "dst_d1")
#     split_scopes(program, "dst_d2")
#     split_scopes(program, "dst_d3")
#     split_scopes(program, "dst_d4")
#     split_scopes(program, "dst_x_1_90")
#     split_scopes(program, "dst")
#     apply_to_exhaustion(program, [
#         (reuse_arr_dims, find_reusable_arr_dims),
#         (reuse_buffers, find_reusable_buffers),
#         (move_buf_to_stack, find_bufs_movable_to_stack),
#     ])
#     unroll_scope(program, 'max_val#0')
#     enable_ssr(program, "max_val", 1)
#     enable_ssr(program, "diff", 1)
#     enable_ssr(program, "x", 1)
#     enable_ssr(program, "dst", 0)
#     apply_to_exhaustion(program, [
#         (increase_ssr_depth, find_increasable_ssr_depth),
#         (merge_ssr, find_mergeable_ssr),
#         (activate_ssr, find_activatable_ssr),
#         (enable_frep, find_applicable_frep),
#     ])


def apply_manual_layernorm_transformations(program):
    apply_to_exhaustion(program, [(split_scopes, find_splittable_scopes)])
        
    # apply_to_exhaustion(program, [(join_scopes, find_joinable_scopes)])
    reuse_buffers(program, 'tmp', 'dst')
    reuse_buffers(program, 'src', 'dst')
        
    flags = [0]*len(program.ops)
    for loop, f in zip(program.ops, range(len(flags))):
        if isinstance(loop, LoopScope):
            try:
                tile_scope(program, loop.name, 4)
                for d in range(int(loop.name[-1]) - 1, 0, -1):
                    scope_name = loop.name[:-1] + str(d)
                    swap_nested_scopes(program, scope_name)
            except:
                flags[f] = 1

    tile_buffer(program, 'm', 4, 0)
    tile_buffer(program, 'mu', 4, 0)
    tile_buffer(program, 'diff', 4, 0)
    tile_buffer(program, 'q', 4, 0)
    tile_buffer(program, 'qeps', 4, 0)
    tile_buffer(program, 'sigma_d2', 4, 0)
    tile_buffer(program, 'sigma_f32', 4, 0)
    tile_buffer(program, 'sigma_i32', 4, 0)
    tile_buffer(program, 'sigma_s1', 4, 0)
    tile_buffer(program, 'sigma_s2', 4, 0)
    tile_buffer(program, 'sigma_f32_2', 4, 0)
    tile_buffer(program, 'sigma_f64', 4, 0)
    tile_buffer(program, 'sigma_f64_2', 4, 0)
    tile_buffer(program, 'sigma_th', 4, 0)
    tile_buffer(program, 'sigma_inv1', 4, 0)
    tile_buffer(program, 'sigma_f64_2_2', 4, 0)
    tile_buffer(program, 'sigma_th_2', 4, 0)
    tile_buffer(program, 'sigma', 4, 0)

    for _ in range(18):
        join_scopes(program , 'm#2',)
    join_scopes(program , 'diff#1',)
    join_scopes(program , 'diff#1',)
    
    reuse_arr_dims(program, 'diff', 0)
    reuse_arr_dims(program, 'diff', 2)
    
    apply_to_exhaustion(program, [
        (reuse_arr_dims, find_reusable_arr_dims),
        (reuse_buffers, find_reusable_buffers),
        (lambda p, b: move_buf_to_stack(p, b, 512), lambda p: find_bufs_movable_to_stack(p, 512)),
        (unroll_scope, find_unrollable_scopes),
    ])
        
    enable_ssr(program, 'm', 1) # src
    enable_ssr(program, 'diff', 1) # src
    enable_ssr(program, 'tmp', 0) # tmp
    enable_ssr(program, 'tmp', 2) # gamma
    
    enable_ssr(program, 'dst', 0) # dst
    enable_ssr(program, 'dst', 2) # tmp
    enable_ssr(program, 'dst', 3) # beta 
        
    increase_ssr_depth(program, 'm', 1)
    increase_ssr_depth(program, 'diff', 1)
    increase_ssr_depth(program, 'tmp', 0)
    increase_ssr_depth(program, 'tmp', 2)
    increase_ssr_depth(program, 'dst', 0)
    increase_ssr_depth(program, 'dst', 2)
    increase_ssr_depth(program, 'dst', 3)
    increase_ssr_depth(program, 'm', 1)
    increase_ssr_depth(program, 'diff', 1)
    increase_ssr_depth(program, 'tmp', 0)
    increase_ssr_depth(program, 'tmp', 2)
    increase_ssr_depth(program, 'dst', 0)
    increase_ssr_depth(program, 'dst', 2)
    increase_ssr_depth(program, 'dst', 3)
    
    merge_ssr(program, 'm', 1, 'diff', 1, check=False)  # TODO: fix applicability detection check to allow this
    merge_ssr(program, 'tmp', 2, 'dst', 3)
        
    apply_to_exhaustion(program, [
        (activate_ssr, find_activatable_ssr),
    ])    
    
    apply_to_exhaustion(program, [ (enable_frep, find_applicable_frep), ])


# def apply_manual_mish_transformations(program):
#     #tile_buffer(program, 'X', 1, 4)
#     splittable_scopes = find_splittable_scopes(program)
#     for s in splittable_scopes:
#         print(f"split_scopes(program, {args_as_str(s)})")
#         split_scopes(program, *s)
#     apply_to_exhaustion(program, [
#         (reuse_arr_dims, find_reusable_arr_dims),
#         (reuse_buffers, find_reusable_buffers),
#         (move_buf_to_stack, find_bufs_movable_to_stack),
#     ])
#     apply_to_exhaustion(program, [(unroll_scope, find_unrollable_scopes)])
#     # unroll_scope(program, 'Y_softplus_exp_v0#0')
#     # apply_to_exhaustion(program, [ (enable_ssr, find_applicable_ssr), ])
#     enable_ssr(program, 'Y', 0)
#     enable_ssr(program, 'Y', 1)
#     enable_ssr(program, 'Y_softplus_exp_v0', 1)
#     apply_to_exhaustion(program, [ (increase_ssr_depth, find_increasable_ssr_depth), ])
#     apply_to_exhaustion(program, [ (merge_ssr, find_mergeable_ssr), ])
#     apply_to_exhaustion(program, [ (activate_ssr, find_activatable_ssr), ])





# def apply_manual_batchnorm_transformations(program):
#     apply_to_exhaustion(program, [
#         (reuse_arr_dims, find_reusable_arr_dims),
#         (reuse_buffers, find_reusable_buffers),
#         (move_buf_to_stack, find_bufs_movable_to_stack),
#     ])

#     swap_nested_scopes(program, "current_sum#3")
#     enable_ssr(program, "current_sum", 1)
#     increase_ssr_depth(program, "current_sum", 1)
#     increase_ssr_depth(program, "current_sum", 1)
#     increase_ssr_depth(program, "current_sum", 1)
#     activate_ssr(program, "current_sum", 1, 0)
#     enable_frep(program, "current_sum#2")
#     join_scopes(program, "current_sum#3")


#     swap_nested_scopes(program, "diff#3")
#     enable_ssr(program, "diff", 1)
#     increase_ssr_depth(program, "diff", 1)
#     increase_ssr_depth(program, "diff", 1)
#     increase_ssr_depth(program, "diff", 1)
#     activate_ssr(program, "diff", 1, 1)
#     enable_ssr(program, "diff", 0)
#     increase_ssr_depth(program, "diff", 0)
#     increase_ssr_depth(program, "diff", 0)
#     increase_ssr_depth(program, "diff", 0)
#     activate_ssr(program, "diff", 0, 0)
#     enable_frep(program, "diff#2")


#     swap_nested_scopes(program, "diff2_sum#3")
#     enable_ssr(program, "diff2_sum", 1)
#     increase_ssr_depth(program, "diff2_sum", 1)
#     increase_ssr_depth(program, "diff2_sum", 1)
#     increase_ssr_depth(program, "diff2_sum", 1)
#     enable_ssr(program, "diff2_sum", 2)
#     increase_ssr_depth(program, "diff2_sum", 2)
#     increase_ssr_depth(program, "diff2_sum", 2)
#     increase_ssr_depth(program, "diff2_sum", 2)
#     merge_ssr(program, "diff2_sum", 2, "diff2_sum", 1)
#     activate_ssr(program, "diff2_sum", 2, 2)
#     enable_frep(program, "diff2_sum#2")

#     join_scopes(program, 'current_var_biased#0',)
#     join_scopes(program, 'current_var_biased#0',)
#     join_scopes(program, 'current_var_biased#0',)
#     join_scopes(program, 'current_var_biased#0',)
#     join_scopes(program, 'current_var_biased#0',)
#     join_scopes(program, 'current_var_biased#0',)
#     join_scopes(program, 'current_var_biased#0',)
#     join_scopes(program, 'current_var_biased#0',)
#     join_scopes(program, 'current_var_biased#0',)
#     join_scopes(program, 'current_var_biased#0',)
#     join_scopes(program, 'current_var_biased#0',)

#     swap_nested_scopes(program, "Y#3")
#     enable_ssr(program, "Y", 0)
#     increase_ssr_depth(program, "Y", 0)
#     increase_ssr_depth(program, "Y", 0)
#     increase_ssr_depth(program, "Y", 0)
#     activate_ssr(program, "Y", 0, 0)
#     enable_ssr(program, "Y", 1)
#     increase_ssr_depth(program, "Y", 1)
#     increase_ssr_depth(program, "Y", 1)
#     increase_ssr_depth(program, "Y", 1)
#     activate_ssr(program, "Y", 1, 1)
#     enable_frep(program, "Y#2")
#     #join_scopes(program, "current_sum#3")

#     print(program)
#     # join_scopes(program, 'Ysb#3',)
#     # join_scopes(program, 'Ysb#2',)
#     # join_scopes(program, 'Ysb#1',)
#     # join_scopes(program, 'Ysb#0',)


# def apply_manual_softplus_transformations(program):
#     tile_buffer(program, 'X', 1, 4)
#     splittable_scopes = find_splittable_scopes(program)
#     for s in splittable_scopes:
#         print(f"split_scopes(program, {args_as_str(s)})")
#         split_scopes(program, *s)
#     apply_to_exhaustion(program, [
#         (reuse_arr_dims, find_reusable_arr_dims),
#         (reuse_buffers, find_reusable_buffers),
#         (move_buf_to_stack, find_bufs_movable_to_stack),
#     ])
#     enable_ssr(program, 'Y_exp_v0', 1)
#     enable_ssr(program, 'Y', 0)
#     apply_to_exhaustion(program, [ (increase_ssr_depth, find_increasable_ssr_depth), ])
#     apply_to_exhaustion(program, [ (merge_ssr, find_mergeable_ssr), ])
#     apply_to_exhaustion(program, [ (activate_ssr, find_activatable_ssr), ])


def apply_manual_exp_transformations(program):

    untile_buffer(program, 'z', 0)
    #fmul stalls 1 cycle: should unroll by 2 and do two separate frep cycles

    #put temporaries on stack
    reuse_arr_dims(program,    'z_v0', 0)
    # reuse_arr_dims(program,    'z_v0', 1)
    move_buf_to_stack(program, 'z_v0')
    reuse_arr_dims(program,    'z_v1', 0)
    # reuse_arr_dims(program,    'z_v1', 1)
    move_buf_to_stack(program, 'z_v1')
    reuse_arr_dims(program,    'z_v2', 0)
    # reuse_arr_dims(program,    'z_v2', 1)
    move_buf_to_stack(program, 'z_v2')
    reuse_arr_dims(program,    'z_v3', 0)
    # reuse_arr_dims(program,    'z_v3', 1)
    move_buf_to_stack(program, 'z_v3')
    reuse_arr_dims(program,    'z_v4', 0)
    # reuse_arr_dims(program,    'z_v4', 1)
    move_buf_to_stack(program, 'z_v4')
    reuse_arr_dims(program,    'z_v5', 0)
    # reuse_arr_dims(program,    'z_v5', 1)
    move_buf_to_stack(program, 'z_v5')
    reuse_arr_dims(program,    'z_v6', 0)
    # reuse_arr_dims(program,    'z_v6', 1)
    move_buf_to_stack(program, 'z_v6')
    reuse_arr_dims(program,    'z_v7', 0)
    # reuse_arr_dims(program,    'z_v7', 1)
    move_buf_to_stack(program, 'z_v7')
    reuse_arr_dims(program,    'z_v8', 0)
    # reuse_arr_dims(program,    'z_v8', 1)
    move_buf_to_stack(program, 'z_v8')
    reuse_arr_dims(program,    'z_v9', 0)
    # reuse_arr_dims(program,    'z_v9', 1)
    move_buf_to_stack(program, 'z_v9')
    reuse_arr_dims(program,    'z_v10', 0)
    # reuse_arr_dims(program,    'z_v10', 1)
    move_buf_to_stack(program, 'z_v10')
    reuse_arr_dims(program,    'z_v11', 0)
    # reuse_arr_dims(program,    'z_v11', 1)
    move_buf_to_stack(program, 'z_v11')
    reuse_arr_dims(program,    'z_v12', 0)
    # reuse_arr_dims(program,    'z_v12', 1)
    move_buf_to_stack(program, 'z_v12')
    reuse_arr_dims(program,    'z_v13', 0)
    # reuse_arr_dims(program,    'z_v13', 1)
    move_buf_to_stack(program, 'z_v13')

    #remove useless buffers
    reuse_buffers(program, 'z_v1', 'z_v0')
    reuse_buffers(program, 'z_v2', 'z_v0')
    reuse_buffers(program, 'z_v3', 'z_v0')
    reuse_buffers(program, 'z_v4', 'z_v0')
    reuse_buffers(program, 'z_v5', 'z_v0')
    reuse_buffers(program, 'z_v6', 'z_v0')
    reuse_buffers(program, 'z_v7', 'z_v0')
    reuse_buffers(program, 'z_v8', 'z_v0')
    reuse_buffers(program, 'z_v9', 'z_v0')
    reuse_buffers(program, 'z_v10', 'z_v0')
    reuse_buffers(program, 'z_v11', 'z_v0')
    reuse_buffers(program, 'z_v12', 'z_v0')
    reuse_buffers(program, 'z_v13', 'z_v0')

    #use ssr to read and write from memory
    enable_ssr(program, 'z_v0', 1)
    # increase_ssr_depth(program, 'z_v0', 1)
    activate_ssr(program, 'z_v0', 1, 0)
    enable_ssr(program, 'z', 0)
    # increase_ssr_depth(program, 'z', 0)
    activate_ssr(program, 'z', 0, 1)

    #enable frep
    enable_frep(program, 'z_v0#0')


def apply_manual_log_transformations(program):
    #put temporaries on stack
    reuse_arr_dims(program,    'z_rt2x', 0)
    reuse_arr_dims(program,    'z_rt2x', 1)
    move_buf_to_stack(program, 'z_rt2x')
    reuse_arr_dims(program,    'z_rt4x', 0)
    reuse_arr_dims(program,    'z_rt4x', 1)
    move_buf_to_stack(program, 'z_rt4x')
    reuse_arr_dims(program,    'z_rt3_4x', 0)
    reuse_arr_dims(program,    'z_rt3_4x', 1)
    move_buf_to_stack(program, 'z_rt3_4x')
    reuse_arr_dims(program,    'z_x_1', 0)
    reuse_arr_dims(program,    'z_x_1', 1)
    move_buf_to_stack(program, 'z_x_1')
    reuse_arr_dims(program,    'z_rt4x32', 0)
    reuse_arr_dims(program,    'z_rt4x32', 1)
    move_buf_to_stack(program, 'z_rt4x32')
    reuse_arr_dims(program,    'z_rt2x12', 0)
    reuse_arr_dims(program,    'z_rt2x12', 1)
    move_buf_to_stack(program, 'z_rt2x12')
    reuse_arr_dims(program,    'z_rt3_4x32', 0)
    reuse_arr_dims(program,    'z_rt3_4x32', 1)
    move_buf_to_stack(program, 'z_rt3_4x32')
    reuse_arr_dims(program,    'z_x7', 0)
    reuse_arr_dims(program,    'z_x7', 1)
    move_buf_to_stack(program, 'z_x7')
    reuse_arr_dims(program,    'z_d1', 0)
    reuse_arr_dims(program,    'z_d1', 1)
    move_buf_to_stack(program, 'z_d1')
    reuse_arr_dims(program,    'z_d2', 0)
    reuse_arr_dims(program,    'z_d2', 1)
    move_buf_to_stack(program, 'z_d2')
    reuse_arr_dims(program,    'z_d3', 0)
    reuse_arr_dims(program,    'z_d3', 1)
    move_buf_to_stack(program, 'z_d3')
    reuse_arr_dims(program,    'z_d4', 0)
    reuse_arr_dims(program,    'z_d4', 1)
    move_buf_to_stack(program, 'z_d4')
    reuse_arr_dims(program,    'z_x_1_90', 0)
    reuse_arr_dims(program,    'z_x_1_90', 1)
    move_buf_to_stack(program, 'z_x_1_90')

    #remove useless buffers -> do liveness analysis
    swap_ops(program, 'z_x_1')
    swap_ops(program, 'z_x_1')
    swap_ops(program, 'z_x_1')
    swap_ops(program, 'z_x_1')
    swap_ops(program, 'z_x_1')
    swap_ops(program, 'z_x_1')
    swap_ops(program, 'z_x_1')
    swap_ops(program, 'z_x_1')
    reuse_buffers(program, 'z_rt4x32', 'z_rt4x')
    reuse_buffers(program, 'z_rt2x12', 'z_rt2x') 
    swap_ops(program, 'z_d1')
    swap_ops(program, 'z_x7')
    swap_ops(program, 'z_rt3_4x32')
    reuse_buffers(program, 'z_rt3_4x32', 'z_rt3_4x')
    reuse_buffers(program, 'z_d2', 'z_rt2x')
    reuse_buffers(program, 'z_x7', 'z_rt4x')
    reuse_buffers(program, 'z_d1', 'z_rt4x')
    reuse_buffers(program, 'z_d3', 'z_rt4x')
    reuse_buffers(program, 'z_d4', 'z_rt4x')
    reuse_buffers(program, 'z_x_1', 'z_rt2x')
    reuse_buffers(program, 'z_x_1_90', 'z_rt2x')

    #apply ssr
    enable_ssr(program, 'z_rt2x', 1)
    increase_ssr_depth(program, 'z_rt2x', 1)
    enable_ssr(program, 'z_x7', 1)
    increase_ssr_depth(program, 'z_x7', 1)
    enable_ssr(program, 'z_x_1', 1)
    increase_ssr_depth(program, 'z_x_1', 1)
    merge_ssr(program, 'z_rt2x', 1, 'z_x7', 1)
    merge_ssr(program, 'z_rt2x', 1, 'z_x_1', 1)
    activate_ssr(program, 'z_rt2x', 1, 0)

    #enable frep
    # enable_frep(program, 'z_v0#0')



def apply_manual_sqrt_transformations(program):

    untile_buffer(program, 'src', 0)
    untile_buffer(program, 'dst_inv_d2', 0)
    untile_buffer(program, 'dst_inv_f32', 0)
    untile_buffer(program, 'dst_inv_i32', 0)
    untile_buffer(program, 'dst_inv_s1', 0)
    untile_buffer(program, 'dst_inv_s2', 0)
    untile_buffer(program, 'dst_inv_f32_2', 0)
    untile_buffer(program, 'dst_inv_f64', 0)
    untile_buffer(program, 'dst_inv_f64_2', 0)
    untile_buffer(program, 'dst_inv_th', 0)
    untile_buffer(program, 'dst_inv_inv1', 0)
    untile_buffer(program, 'dst_inv_f64_2_2', 0)
    untile_buffer(program, 'dst_inv_th_2', 0)
    untile_buffer(program, 'dst_inv', 0)
    untile_buffer(program, 'dst', 0)
    untile_scope(program, 'dst_inv_d2#1')

    #separate floating point and integer
    swap_ops(program, 'dst_inv_d2')
    swap_ops(program, 'dst_inv_d2')
    swap_ops(program, 'dst_inv_d2')
    swap_ops(program, 'dst_inv_d2')
    swap_ops(program, 'dst_inv_d2')
    swap_ops(program, 'dst_inv_d2')
    split_scopes(program, 'dst_inv_d2')

    apply_to_exhaustion(program, [
        (reuse_arr_dims, find_reusable_arr_dims),
        (reuse_buffers, find_reusable_buffers),
        (move_buf_to_stack, find_bufs_movable_to_stack),
    ])

    #apply ssr and frep to the fp computation
    enable_ssr(program, 'dst_inv_d2', 1)
    enable_ssr(program, 'dst', 2)
    merge_ssr(program, 'dst_inv_d2', 1, 'dst', 2)
    activate_ssr(program, 'dst_inv_d2', 1, 0)

    enable_ssr(program, 'dst', 0)
    activate_ssr(program, 'dst', 0, 1)

    enable_ssr(program, 'dst_inv_f64_2', 1)
    enable_ssr(program, 'dst_inv_f64_2', 2)
    enable_ssr(program, 'dst_inv_inv1', 2)

    merge_ssr(program, 'dst_inv_f64_2', 1, 'dst_inv_f64_2', 2)
    merge_ssr(program, 'dst_inv_f64_2', 1, 'dst_inv_inv1', 2)
    activate_ssr(program, 'dst_inv_f64_2', 1, 2)

    enable_frep(program, 'dst_inv_d2#0')



def apply_manual_div_transformations(program):

    #TODO: apply unrolling
    untile_buffer(program, 'dst', 0) 

    #separate floating point and integer
    swap_ops(program, 'dst_d2')
    swap_ops(program, 'dst_d2')
    swap_ops(program, 'dst_d2')
    swap_ops(program, 'dst_d2')
    swap_ops(program, 'dst_d2')
    swap_ops(program, 'dst_d2')
    split_scopes(program, 'dst_d2')


    reuse_arr_dims(program, 'dst_th_2', 0)
    reuse_arr_dims(program, 'dst_th', 0)
    reuse_arr_dims(program, 'dst_s2', 0)
    reuse_arr_dims(program, 'dst_s1', 0)
    reuse_arr_dims(program, 'dst_inv1', 0)
    reuse_arr_dims(program, 'dst_inv', 0)
    reuse_arr_dims(program, 'dst_i32', 0)
    reuse_arr_dims(program, 'dst_f64_2_d2_2', 0)
    reuse_arr_dims(program, 'dst_f64_2_d2', 0)
    reuse_arr_dims(program, 'dst_f64_2_2', 0)
    reuse_arr_dims(program, 'dst_f64_2', 0)
    reuse_arr_dims(program, 'dst_f32_2', 0)
    reuse_arr_dims(program, 'dst_f32', 0)
    reuse_arr_dims(program, 'dst_div_u', 0)
    reuse_arr_dims(program, 'dst_div', 0)
    reuse_arr_dims(program, 'dst_d2', 0)
    reuse_buffers(program, 'dst_th', 'dst_th_2')
    reuse_buffers(program, 'dst_s1', 'dst_s2')
    reuse_buffers(program, 'dst_inv', 'dst_th_2')
    reuse_buffers(program, 'dst_i32', 'dst_s2')
    reuse_buffers(program, 'dst_f64_2_d2', 'dst_th_2')
    reuse_buffers(program, 'dst_f64_2_2', 'dst_f64_2_d2_2')
    reuse_buffers(program, 'dst_f64_2', 'dst_th_2')
    reuse_buffers(program, 'dst_f32', 'dst_f32_2')
    reuse_buffers(program, 'dst_div_u', 'dst_th_2')
    reuse_buffers(program, 'dst_div', 'dst_th_2')
    #reuse_buffers(program, 'dst', 'dst_f64')

    move_buf_to_stack(program, 'dst_th_2',)
    move_buf_to_stack(program, 'dst_s2',)
    move_buf_to_stack(program, 'dst_inv1',)
    move_buf_to_stack(program, 'dst_f64_2_d2_2',)
    move_buf_to_stack(program, 'dst_f32_2',)
    move_buf_to_stack(program, 'dst_d2',)

    #apply ssr and frep to the fp computation
    enable_ssr(program, 'dst_d2', 1)
    # increase_ssr_depth(program, 'dst_d2', 1)
    enable_ssr(program, 'dst', 2)
    # increase_ssr_depth(program, 'dst', 2)
    merge_ssr(program, 'dst_d2', 1, 'dst', 2)
    activate_ssr(program, 'dst_d2', 1, 0)

    enable_ssr(program, 'dst', 0)
    # increase_ssr_depth(program, 'dst', 0)
    activate_ssr(program, 'dst', 0, 1)

    enable_ssr(program, 'dst_f64_2', 1)
    #increase_ssr_depth(program, 'dst_f64_2', 1)
    enable_ssr(program, 'dst_f64_2', 2)
    #increase_ssr_depth(program, 'dst_f64_2', 2)
    enable_ssr(program, 'dst_inv1', 2)
    #increase_ssr_depth(program, 'dst_inv1', 2)

    merge_ssr(program, 'dst_f64_2', 1, 'dst_f64_2', 2)
    merge_ssr(program, 'dst_f64_2', 1, 'dst_inv1', 2)
    activate_ssr(program, 'dst_f64_2', 1, 2)
    print(program)
    #enable_frep(program, 'dst_d2#0')



