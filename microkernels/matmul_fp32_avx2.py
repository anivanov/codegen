from kelgen import *
import torch
from kelgen.transformations import *


def matmul_single_avx2():
    
    M = 768
    N = 1024
    K = 1024
    
    Kc = 256
    Mc = 192
    Nr = 16
    Mr = 6
    
    x = np.random.uniform(-1, 1, (K, M)).astype(np.float32)
    y = np.random.uniform(-1, 1, (K, N)).astype(np.float32)
    
    def init_src():
        x[:] = np.random.uniform(-1, 1, (K, M)).astype(np.float32)
        y[:] = np.random.uniform(-1, 1, (K, N)).astype(np.float32)
    
    tx = torch.from_numpy(x)
    ty = torch.from_numpy(y)
    
    pt_mean, pt_std, pt_times = time_prof(10, lambda: torch.einsum("km,kn->mn", tx, ty), init_src)
    print(f"PyTorch matmul time [ms]: avg {pt_mean * 1e3:.6f} std {pt_std * 1e3:.6f} reps {len(pt_times)}")
    
    def eval_func(program, ignore=True):
        if ignore:
            return
        print(program.text())
        time = test_native_code(
            program,
            {'x': x, 'y': y, 'z': np.einsum("km,kn->mn", x, y)},
            min_reps=3,
            remove_tmp_files=False,
            silent=False,
            timeout=60,
        )
        time = time[max(len(time) // 3, 1):]
        print(f"My matmul [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")
    
    program_text = f"""
        Name: matmul
        In: x, y
        Out: z
        Declarations:
            x f32 [{K}, {M}] heap
            y f32 [{K}, {N}] heap
            z f32 [{M}, {N}] heap
        Code:
        {M} {N} {K} z[{{0}}, {{1}}] += x[{{2}}, {{0}}] * y[{{2}}, {{1}}]
    """

    program = parse_program(program_text); eval_func(program, ignore=False)
    
    tile_nested_scopes(program, 'z', tiles=[[Mc, Mr], [Nr], [Kc]], silent=False, eval_func=eval_func)

    reorder_loops(program, 'z', before=['Mo', 'Mc', 'Mr', 'No', 'Nr', 'Ko', 'Kc'], after=['Ko', 'Mo', 'No', 'Mc', 'Kc', 'Mr', 'Nr'], silent=False, eval_func=eval_func)
    create_operand_temporary(program, 'z', 2); eval_func(program)
    create_operand_temporary(program, 'z', 1); eval_func(program)
    create_operand_temporary(program, 'z', 0); eval_func(program)

    num_depth_increases = increase_scope_depth(program, 'z0#6', silent=False, eval_func=eval_func)
    create_dimension(program, 'z', 1); eval_func(program)
    decrease_scope_depth(program, 'z0#0', num_depth_increases, silent=False, eval_func=eval_func)
    decrease_scope_depth(program, 'z#0', num_depth_increases-1, silent=False, eval_func=eval_func)
    
    apply_to_exhaustion(program, [(tile_buffer, find_tileable_buffers)], silent=False, eval_func=eval_func)
    apply_to_exhaustion(program, [(join_scopes, find_joinable_scopes)], silent=False, eval_func=eval_func)
    apply_to_exhaustion(program, [(reuse_arr_dims, find_reusable_arr_dims)], silent=False, eval_func=eval_func)
    apply_to_exhaustion(program, [(move_buf_to_stack, lambda x: find_bufs_movable_to_stack(x, 4096))], silent=False, eval_func=eval_func)

    swap_buffer_dims(program, 'x0', 1); eval_func(program)
    swap_buffer_dims(program, 'x0', 2); eval_func(program)
    swap_buffer_dims(program, 'y0', 1); eval_func(program)
    
    parallelize_scope(program, 'z0#4'); eval_func(program)
    parallelize_scope(program, 'y0#2'); eval_func(program)
    parallelize_scope(program, 'x0#2'); eval_func(program, ignore=False)


if __name__ == '__main__':
    matmul_single_avx2()
