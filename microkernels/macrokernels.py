from kernels import *


def create_allocations(program, ref_data, check_correctness=True):
    inp, tmp, out = program.get_inp_tmp_out()
    reused_out_as_inp = program.reused_out_as_inp()
    
    ref_data_init_code = []
    ref_fast_init_code = []
    for k, v in ref_data.items():
        size = v.size
        dtype = NP_TO_GEN_DTYPE[v.dtype.type]
        values = ', '.join(str(s) for s in v.flatten())
        # include non-scalar data in the global memory
        if check_correctness and size > 1:
            ref_data_init_code.append(f"{dtype} data_{k}[{size}] = {{ {values} }};")
        # allocate scalars directly in fast memory
        if size == 1:
            ref_fast_init_code.append(f"{dtype} data_{k} = {v};")
        # outputs require separate buffers as we don't want to modify reference data
        if k in out and k not in reused_out_as_inp:
            ref_fast_init_code.append(f"{dtype}* result_{k} = ({dtype}*) snrt_l3_next();")
            ref_fast_init_code.append(f"my_snrt_cluster_hw_barrier();")
            ref_fast_init_code.append(f"if(tid == 0) {{ \n {dtype}* r_{k} = ({dtype}*) snrt_l3alloc({size} * sizeof({dtype}));")
            ref_fast_init_code.append(f'if (!result_{k} || !r_{k} || result_{k} != r_{k}) {{ printf("ERROR: failed to allocate buffer result_{k} of size {size} * sizeof({dtype})\\n"); return 1; }}')
            if check_correctness:
                ref_fast_init_code.append(f"for (i32 i = 0; i < {size}; i++) result_{k}[i] = 0;")
            ref_fast_init_code.append(f"my_snrt_cluster_hw_barrier();\n}}")
            ref_fast_init_code.append(f"else my_snrt_cluster_hw_barrier();")
            
            #ref_fast_init_code.append("my_snrt_cluster_hw_barrier();\n}\nelse my_snrt_cluster_hw_barrier();")
        # when input is reused as output, we just alias such output to input
        # this will do in-place modifications of such input but since input is not needed anymore, it is ok to reuse memory
        if k in reused_out_as_inp:
            # output should point to input
            inp_name = reused_out_as_inp[k]
            ref_fast_init_code.append(f"{dtype}* result_{k} = data_{inp_name};")
            
    ref_data_init_code = "\n".join(ref_data_init_code)
    ref_fast_init_code = "\n".join(ref_fast_init_code)
    
    return ref_data_init_code, ref_fast_init_code


def create_verification_code(program, ref_data, check_correctness):
    array_verify_code = []
    inp, tmp, out = program.get_inp_tmp_out()
    if check_correctness:
        for k, v in ref_data.items():
            if k not in out:
                continue
            array_verify_code.append(f"for (int i = 0; i < {v.size}; i++) {{")
            array_verify_code.append(f"    if (fabs(result_{k}[i] - data_{k}[i]) > 1e-3) {{")
            array_verify_code.append(f"        int x1 = (int)(result_{k}[i]);")
            array_verify_code.append(f"        int x2 = ((int)(fabs(result_{k}[i]) * 1000)) % 1000;")
            array_verify_code.append(f"        int y1 = (int)(data_{k}[i]);")
            array_verify_code.append(f"        int y2 = ((int)(fabs(data_{k}[i]) * 1000)) % 1000;")
            array_verify_code.append(f'        printf("Error: mismatch at {k}, %d, %d.%03d (computed) != %d.%03d (expected) \\n", (int)i, x1, x2, y1, y2);')
            array_verify_code.append(f"        ok = 0;")
            array_verify_code.append(f"        break;")
            array_verify_code.append(f"    }}")
            array_verify_code.append(f"}}")
    array_verify_code = "\n".join(array_verify_code)
    return array_verify_code


def create_macrokernel_call_args(program):
    inp, tmp, out = program.get_inp_tmp_out()
    call_args = [f"data_{k}" for k in sorted(inp)] + [f"result_{k}" for k in sorted(out)]
    call_args = ", ".join(call_args)
    return call_args

def generate_dma_access(src, macro_program, micro_program, micro_inputs, direction):
    out_code = ""
    for buf_name in src:
        micro_buf = micro_program.buffers[buf_name]
        macro_buf = macro_program.buffers[buf_name]
        buffer_dims = macro_buf.dims

        tile_offset = ""
        for idp, (mi_name, mi_val) in enumerate(micro_inputs.items()):
            if mi_name in [dim.name for dim in buffer_dims]:
                tile_offset += f" + i{mi_name}"
                for idx in range(idp +1, len(buffer_dims)):
                    if buffer_dims[idx] in micro_inputs.keys():
                        tile_offset += f" * {micro_inputs[buffer_dims[idx]]}"
                    else:
                        tile_offset += f" * {buffer_dims[idx]}"
                
        if len(macro_buf.dims) == 0:
            dma_code = ""
        elif len(macro_buf.dims) == 1:
            macro_dim0 = macro_buf.dims[0]
            if macro_dim0.name in micro_inputs.keys():
                size = f"{micro_inputs[macro_dim0.name]}"
            else:
                size = f"macro_dim0.name"
            if direction == "to_tcdm":
                dma_code = f"snrt_dma_start_1d(local_{buf_name}, {buf_name}{tile_offset}, {size} * sizeof(f64));\n"
            elif direction == "from_tcdm":
                dma_code = f"snrt_dma_start_1d({buf_name}{tile_offset}, local_{buf_name}, {size} * sizeof(f64));\n"
            else:
                raise(ValueError)

        elif len(macro_buf.dims) == 2:
            macro_dim0 = macro_buf.dims[0]
            macro_dim1 = macro_buf.dims[1]
            if macro_dim0.name in micro_inputs.keys():
                micro_dim0 = micro_inputs[macro_dim0.name]
            else:
                micro_dim0 = macro_dim0.name
            if macro_dim1.name in micro_inputs.keys():
                micro_dim1 = micro_inputs[macro_dim1.name]
            else:
                micro_dim1 = macro_dim1.name

            if direction == "to_tcdm":
                dma_code = f"snrt_dma_start_2d(local_{buf_name}, {buf_name}{tile_offset}, {micro_dim1} * sizeof(f64), {micro_dim1} * sizeof(f64), {macro_dim1} * sizeof(f64), {micro_dim0});\n"
            elif direction == "from_tcdm":
                dma_code = f"snrt_dma_start_2d({buf_name}{tile_offset}, local_{buf_name}, {micro_dim1} * sizeof(f64), {macro_dim1} * sizeof(f64), {micro_dim1} * sizeof(f64), {micro_dim0});\n"
        else:
            raise(NotImplemented)
            # for dim_idx, macro_dim in enumerate(macro_buf.dims[2:]):
            #     if macro_dim.name in micro_inputs:
            #         tile_size = micro_inputs[macro_dim.name]
            #         total_size = ref_data[macro_dim.name]
            #         assert total_size % tile_size == 0
            #         # this dimension is tiled
            #         macro_tile_code += f"for (int i{dim_idx} = i{macro_dim.name}, li{dim_idx} = 0; i{dim_idx} < i{macro_dim.name} + {tile_size}; i{dim_idx}++, li{dim_idx}++) {{\n"
            #     else:
            #         # this dimension is not tiled
            #         macro_tile_code += f"for (int i{dim_idx} = 0; i{dim_idx} < {macro_dim.gen_access(macro_program, 0)}; i{dim_idx}++) {{\n"
            # dma_code = f"dma_call\n"
        out_code += dma_code
    return out_code + "snrt_dma_wait_all();\n"


def create_macrokernel_code(macro_program, micro_program, ref_data, micro_inputs):

    macro_signature = make_signature(macro_program)
    macro_tile_code = f"void {macro_program.name}({macro_signature}) {{\n"

    macro_tile_code += f"unsigned tid = snrt_cluster_core_idx();\n"
    macro_tile_code += f"unsigned tn = snrt_cluster_core_num();\n"
    
    micro_ref_data = {**ref_data, **micro_inputs}
    macro_tile_code += f"size_t ext_buf_size = {get_ext_buf_size(micro_program, micro_ref_data)};\n"
    
    inp, tmp, out = micro_program.get_inp_tmp_out()
    inp_buf_names = {micro_program.arr_to_buf[arr] for arr in inp}
    out_buf_names = {micro_program.arr_to_buf[arr] for arr in out}
    buf_names = inp_buf_names | out_buf_names

    # allocate memory for a single tile
    macro_tile_code += "\n".join(gen_microkernel_allocs(micro_program)) + "\n"
    
    # iterate over all tiles (entrance)
    for mi_name, mi_val in micro_inputs.items():
        macro_tile_code += f"for (int i{mi_name} = 0; i{mi_name} < {ref_data[mi_name]}; i{mi_name} += {mi_val}) {{\n"

    macro_tile_code += f"if (snrt_is_dm_core()) {{\n"
    
    # copy memory in single tile
    macro_tile_code += generate_dma_access(inp_buf_names, macro_program, micro_program, micro_inputs, "to_tcdm")


    macro_tile_code += f"my_snrt_cluster_hw_barrier();\nmy_snrt_cluster_hw_barrier();\n}}\n"
    macro_tile_code += f"else {{\nmy_snrt_cluster_hw_barrier();\n"

    # call micro kernel
    micro_call_args = sorted(inp) + sorted(out)
    micro_call_args = [macro_program.arr_to_buf[arr] for arr in micro_call_args]

    parallelized_dimension = list(micro_inputs.keys())[0]

    def thread_offset (x):
        micro_dims = micro_inputs
        buffer_dims = [d.name for d in macro_program.external_bufs[x].dims]
        offset = f" + tid * {micro_inputs[parallelized_dimension]//8}"

        try:
            idp = buffer_dims.index(parallelized_dimension)
            for idx in range(idp +1, len(buffer_dims)):
                if buffer_dims[idx] in micro_dims.keys():
                    offset += f" * {micro_dims[buffer_dims[idx]]}"
                else:
                    offset += f" * {macro_program.external_bufs[x].dims[idx]}"
            return offset
        except ValueError:
            return ""



        
    total_buf_size = get_all_buf_size_as_number(micro_program, micro_ref_data)

    micro_call_args = ", ".join([(x if macro_program.is_value_access_to_arr(x) else f"local_{x}{thread_offset(x)}") for x in micro_call_args])
    macro_tile_code += f"{micro_program.name}({micro_call_args}, _tmp + ext_buf_size + tid * (({total_buf_size} - ext_buf_size)/8));\n"

    macro_tile_code += f"my_snrt_cluster_hw_barrier();\n}}\n"
    macro_tile_code += f"if (snrt_is_dm_core()) {{\n"

    # copy memory out of single tile
    macro_tile_code += generate_dma_access(out_buf_names, macro_program, micro_program, micro_inputs, "from_tcdm")

    macro_tile_code += f"my_snrt_cluster_hw_barrier();\n}}\n"
    macro_tile_code += f"else {{\nmy_snrt_cluster_hw_barrier();\n}}\n"

    # iterate over all tiles (exit)
    for mi_name, mi_val in micro_inputs.items():
        macro_tile_code += "}\n"


    macro_tile_code += "}\n"
    return macro_tile_code


def test_macro_snitch_code(macro_program, parallelized_program, micro_program, ref_data, micro_inputs, simulator="rtl", check_correctness=True):
    if simulator not in ("banshee", "rtl"):
        raise ValueError("Unknown simulator")
        
    micro_code = gencode(parallelized_program)
    
    micro_signature = make_signature(micro_program)

    # this will override dupicate keys in ref_data with values from micro_inputs
    micro_ref_data = {**ref_data, **micro_inputs}

    total_buf_size = get_all_buf_size_as_number(micro_program, micro_ref_data)
    
    ref_data_init_code, ref_fast_init_code = create_allocations(macro_program, ref_data, check_correctness)
    verification_code = create_verification_code(macro_program, ref_data, check_correctness)

    macro_call_args = create_macrokernel_call_args(macro_program)
    macro_tile_code = create_macrokernel_code(macro_program, micro_program, ref_data, micro_inputs)

    main_code = f"""
        #include <snrt.h>
        #include <stddef.h>
        #include <printf.h>
        #include <math.h>
        #include "snrt_mini.h"
        #define u8 uint8_t
        #define f64 double
        #define i32 int32_t
        {ref_data_init_code}
        

        void {micro_program.name}({micro_signature});
        {macro_tile_code}
        int main() {{
            unsigned tid = snrt_cluster_core_idx();
            
            {ref_fast_init_code}
            size_t buf_size = {total_buf_size};

            void* _tmp = snrt_l1_next();
            my_snrt_cluster_hw_barrier();
            
            if (tid == 0) {{
                
                void* _t = snrt_l1alloc(buf_size);
                my_snrt_cluster_hw_barrier();
                if (!_tmp || !_t || _tmp != _t) {{ printf("ERROR: failed to allocate TCDM buffer _tmp\\n"); return 1; }}
            }}
            else my_snrt_cluster_hw_barrier();
            
            

            unsigned long t1 = read_csr(mcycle);
            {macro_program.name}({macro_call_args}, _tmp);
            unsigned long t2 = read_csr(mcycle);
            if (tid == 0) {{
                printf("Cycles: %lu\\n", t2 - t1);
                i32 ok = 1;
                {verification_code}
                if (ok) {{
                    printf("success, exitting...\\n");
                    return 0;
                }} else {{
                    printf("FAILURE, exitting...\\n");
                    return 1;
                }}
            }}
        }}
    """
    main_code_lines = [l.strip() for l in main_code.splitlines()]
    main_code_lines_indented = autoindent(main_code_lines)
    main_code = '\n'.join(main_code_lines_indented)

    # supposed to be relative to cwd to work both in container and outside
    generated = Path("generated")
    generated.mkdir(parents=True, exist_ok=True)

    main_src = generated / "snitch_main.c"
    code_src = generated / "snitch_code.c"
    bin_src = generated / "snitch_bin"

    with open(main_src, "w") as f:
        f.write(main_code)
    with open(code_src, "w") as f:
        f.write("#include <snrt.h>\n")
        f.write('#include "snrt_mini.h"\n')
        f.write(micro_code)

    return compile_and_run_snitch_code([main_src, code_src], bin_src, simulator=simulator)


def run_macro_kernel(creator, micro_inputs, reference, optimizer, runner, fixed_inputs={}):
    program = creator()
    micro_program = copy.deepcopy(program)
    micro_program.name = f"micro_{program.name}"
    specialize_inputs(micro_program, micro_inputs)

    parallelized_program = copy.deepcopy(program)
    parallelized_program.name = f"micro_{program.name}"
    parallelized_micro_inputs = copy.deepcopy(micro_inputs)
    parallelized_micro_inputs[list(parallelized_micro_inputs.keys())[0]] //= 8
    specialize_inputs(parallelized_program, parallelized_micro_inputs)

    input_data = generate_input_for_program(program, fixed_inputs)
    output_data = reference(input_data)
    optimizer(parallelized_program)
    optimizer(micro_program)
    result = runner(program, parallelized_program, micro_program, {**input_data, **output_data}, micro_inputs)
    return result


def test_macro_layernorm():
    inputs = {"B": 128, "N": 32}
    micro_inputs = {"B": 4}
    cycles = run_macro_kernel(create_layernorm_program, micro_inputs, ref_layernorm, greedy_opt, test_macro_snitch_code, fixed_inputs=inputs)
    print("cycles:", cycles)


def test_macro_gemm():
    inputs = {"M": 16, "N": 16, "K": 16}
    micro_inputs = {"M": 8, "N": 8}
    def opt(program):
        pass
    cycles = run_macro_kernel(create_gemm_program, micro_inputs, ref_gemm, opt, test_macro_snitch_code, fixed_inputs=inputs)
    print("cycles:", cycles)

def test_macro_leakyrelu():
    inputs = {"B": 32, "N": 32}
    micro_inputs = {"B": 32, "N": 32}
    def opt(program):
        apply_manual_leakyrelu_transformations(program)
    cycles = run_macro_kernel(create_leakyrelu_program, micro_inputs, ref_leakyrelu, opt, test_macro_snitch_code, fixed_inputs=inputs)
    print("cycles:", cycles)

def test_macro_sum():
    inputs = {"B": 16, "N": 16}
    micro_inputs = {"B": 8, "N": 8} #what happens if dims are untiled?
    def opt(program):
        pass
        #apply_manual_elem_by_elem_bop_transformations(program) -> untiling breaks micro dimensions
    cycles = run_macro_kernel(create_sum_program, micro_inputs, ref_sum_program, opt, test_macro_snitch_code, fixed_inputs=inputs)
    print("cycles:", cycles)


def test_macro_mish():
    inputs = {"B": 128, "N": 128}
    micro_inputs = {"B": 16, "N": 16}
    cycles = run_macro_kernel(create_mish_program, micro_inputs, ref_mish, greedy_opt, test_macro_snitch_code, fixed_inputs=inputs)
    print("cycles:", cycles)


if __name__ == "__main__":
    #test_macro_layernorm()
    #test_macro_gemm()
    test_macro_sum()
    #test_macro_mish()
