#!/bin/bash


source ~/.bashrc
conda activate main
export LDFLAGS="-lm -L$(dirname $(which clang))/../lib/"
export CFLAGS="-O3 -I . -fopenmp -march=native -I$(dirname $(which clang))/../include/"
export LD_LIBRARY_PATH="$(dirname $(which clang))/../lib/"
export OMP_PROC_BIND=close

export CC=clang
CACHE=cache_clang.db
# rm -f $CACHE

numactl -N 0 -m 0 -C 0-17 python heuristic_search_new.py \
    --kernel reducemean --size 4096x4096 --search no --candidate heur --timeout 5000 --cache $CACHE
numactl -N 0 -m 0 -C 0-17 python heuristic_search_new.py \
    --kernel matmul --size 768x1024x1024 --search no --candidate heur --timeout 5000 --cache $CACHE
numactl -N 0 -m 0 -C 0-17 python heuristic_search_new.py \
    --kernel batchnorm --size 8x3x2048x2048 --search no --candidate heur --timeout 5000 --cache $CACHE
numactl -N 0 -m 0 -C 0-17 python heuristic_search_new.py \
    --kernel conv --size 8x10x3x512x512x5 --search no --candidate heur --timeout 5000 --cache $CACHE
numactl -N 0 -m 0 -C 0-17 python heuristic_search_new.py \
    --kernel layernorm --size 4096x4096 --search no --candidate heur --timeout 5000 --cache $CACHE
numactl -N 0 -m 0 -C 0-17 python heuristic_search_new.py \
    --kernel relu --size 4096x4096 --search no --candidate heur --timeout 5000 --cache $CACHE

