from gen import *
import torch


def softmax_handoptimized(M, N):

    src = np.random.uniform(-1, 1, (M, N)).astype(np.float32)

    def init_src():
        src[:] = np.random.uniform(-1, 1, (M, N)).astype(np.float32)
      
    tsrc = torch.from_numpy(src)

    pt_mean, pt_std, _ = time_prof(10, lambda: torch.nn.functional.softmax(tsrc, dim=-1), init_src)
    print(f"Pytorch softmax time [ms]: avg {pt_mean * 1e3:.6f} std {pt_std * 1e3:.6f}")

    dst = torch.nn.functional.softmax(tsrc, dim=-1).numpy()

    def eval_func(program, ignore=True):
        if ignore:
            return
        print(program.text())
        time = test_native_code(
            program,
            {'src': src, 'dst': dst},
            min_reps=10,
            remove_tmp_files=False,
            silent=False,
            timeout=60,
            check_correctness=True,
            reinit_input=True,
        )
        time = time[max(len(time) // 3, 1):]
        print(f"My softmax [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")

    program_text = f"""
        Name: softmax
        In: src
        Out: dst
        Declarations:
            src f32 [{M}, {N}] heap
            dst f32 [{M}, {N}] heap
            max_val f32 [{M}] heap
            exp_sum f32 [{M}] heap
            tmp_exp f32 [{M}, {N}] heap
            diff f32 [{M}, {N}] heap
        Code:
        {M} {N} max_val[{{0}}] fmaxf= src[{{0}}, {{1}}]
        {M} {N} diff[{{0}}, {{1}}] = src[{{0}}, {{1}}] - max_val[{{0}}]
        {M} {N} tmp_exp[{{0}}, {{1}}] = expf(diff[{{0}}, {{1}}])
        {M} {N} exp_sum[{{0}}] += tmp_exp[{{0}}, {{1}}]
        {M} {N} dst[{{0}}, {{1}}] = tmp_exp[{{0}}, {{1}}] / exp_sum[{{0}}]
    """

    program = parse_program(program_text)
    # eval_func(program, ignore=False)
    
    tile_scope(program, 'diff#0', 4)
    tile_scope(program, 'tmp_exp#0', 4)
    tile_scope(program, 'dst#0', 4)
    apply_to_exhaustion(program, [(vectorize_loop, lambda p: find_vectorizable_loops(p, 'neon'))], silent=False, eval_func=eval_func)
    apply_to_exhaustion(program, [(join_scopes, find_joinable_scopes)], silent=False, eval_func=eval_func)
    print(program.text())
    
    # expected_cores = 16
    # tile_scope(program, 'm#1', expected_cores)
    # eval_func(program)
    # swap_nested_scopes(program, 'm#2')
    # eval_func(program)
    
    # vectorize_operation_neon(program, 'tmp_exp', 4)

    # reuse_buffers(program, 'tmp_exp', 'dst')
    # eval_func(program)
    apply_to_exhaustion(program, [(tile_buffer, find_tileable_buffers)], silent=False, eval_func=eval_func)
    apply_to_exhaustion(program, [(reuse_arr_dims, find_reusable_arr_dims)], silent=False, eval_func=eval_func)
        
    # apply_to_exhaustion(program, [(move_buf_to_stack, lambda x: find_bufs_movable_to_stack(x, 4096))], silent=False, eval_func=eval_func)
    parallelize_scope(program, 'max_val#1')
    eval_func(program, ignore=False)


if __name__ == "__main__":
    softmax_handoptimized(4096, 4096)