#pragma once

#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define i8 int8_t
#define i16 int16_t
#define i32 int32_t
#define i64 int64_t

#define u8 uint8_t
#define u16 uint16_t
#define u32 uint32_t
#define u64 uint64_t

#define f32 float
#define f64 double

#include "includes/avx.h"
#include "includes/neon.h"
#include "includes/cuda.h"

#if !defined(__CUDACC__)  // CUDA provides its own implementation
HOST_DEVICE
inline float rsqrtf(f32 x) {
    return 1.0f / sqrtf(x);
}
#endif

HOST_DEVICE
inline double my_div(double a, double b) {
    return a / b;
}

HOST_DEVICE
inline double my_sqrt(double a) {
    return sqrt(a);
}

HOST_DEVICE
inline double my_sgnjx(double a, double b) {
    return (b < 0) ? -a : a;
}

HOST_DEVICE
inline float reinterpret_f32_from_i32(int32_t i) {
    union {
        int32_t i;
        float f;
    } u;
    u.i = i;
    return u.f;
}

HOST_DEVICE
inline int32_t reinterpret_i32_from_f32(float f) {
    union {
        int32_t i;
        float f;
    } u;
    u.f = f;
    return u.i;
}

static inline void read_file(void* buf, size_t size, const char* name) {
    FILE* f = fopen(name, "rb");
    if (!f) {
        printf("ERROR: failed to open file %s\n", name);
        exit(1);
    }
    size_t read = fread(buf, 1, size, f);
    if (read != size) {
        printf("ERROR: failed to read file %s\n", name);
        exit(1);
    }
    fclose(f);
}

#define checkCudaErrors(expr) do { \
    cudaError_t err = (expr); \
    if (err != cudaSuccess) { \
        printf("Error in %s:%d. Message: %s\n", __FILE__, __LINE__, cudaGetErrorString(err)); \
        exit(1); \
    } \
} while (0)

#if !defined(TOC)
static inline double elapsed_seconds(struct timespec start) {{
    struct timespec end;
    clock_gettime(CLOCK_MONOTONIC, &end);
    return (end.tv_sec - start.tv_sec) + (end.tv_nsec - start.tv_nsec) / 1e9;
}}
#define TIC(name) struct timespec dt_ ##name; clock_gettime(CLOCK_MONOTONIC, &dt_ ##name)
#define TOC(name) elapsed_seconds(dt_ ## name)
#endif