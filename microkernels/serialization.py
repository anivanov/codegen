from gen import *
from kernels import *
import json
import copy
import random
import yaml
from manual_optimizations import *
from pprint import pprint

# class ProgramJSONEncoder(json.JSONEncoder):
#     def default(self, obj):
#         if isinstance(obj, gen.RootScope):
#             res = {
#                 "obj": "RootScope",
#                 "name": obj.name,
#                 "buffers": obj.buffers,
#                 "arr_to_buf": obj.arr_to_buf,
#                 "reusable_inputs": obj.reusable_inputs,
#                 "used_outptus": obj.used_outputs,
#                 "ops": obj.ops,
#                 "transform_group": obj.transform_group,
#                 "transforms": obj.transform_annotations,
#             }
#             assert not (obj.options.keys() - {'max_ssr_idx', 'ssr'})  # make sure I didn't miss any fields
#             if obj.options.get("ssr"):
#                 res["ssr"] = obj.options["ssr"]
#             return res
#         elif isinstance(obj, gen.LoopScope):
#             res = {
#                 "obj": "LoopScope",
#                 "dim": obj.dim,
#                 "ops": obj.ops,
#                 # "options": obj.options,
#                 "transforms": obj.transform_annotations,
#             }
#             assert not (obj.options.keys() - {'unroll', 'frep'})  # make sure I didn't miss any fields
#             if obj.options.get("unroll"):
#                 res["unroll"] = obj.options["unroll"]
#             if obj.options.get("frep"):
#                 res["frep"] = obj.options["frep"]
#             return res
#         elif isinstance(obj, gen.Buffer):
#             return {
#                 "obj": "Buffer",
#                 "dims": obj.dims,
#                 "dtype": obj.dtype,
#                 "dims_order": obj.dims_order,
#                 "allocation": obj.allocation,
#                 "transforms": obj.transform_annotations,
#             }
#         elif isinstance(obj, gen.Array):
#             res = {
#                 "obj": "Elem",
#                 "array": obj.name,
#                 "indices": obj.indices,
#                 # "options": obj.options,
#                 "transforms": obj.transform_annotations,
#             }
#             assert not (obj.options.keys() - {'ssr'})  # make sure I didn't miss any fields
#             if obj.options.get("ssr"):
#                 res["ssr"] = obj.options["ssr"]
#             return res
#         elif isinstance(obj, gen.Const):
#             return {
#                 "obj": "Const",
#                 "dtype": obj.dtype,
#                 "value": obj.value,
#             }
#         elif isinstance(obj, set):
#             return sorted(list(obj))
#         elif isinstance(obj, gen.Operation):
#             return {
#                 "obj": "Operation",
#                 "out": obj.out_elem,
#                 "inp": obj.inp_elem,
#                 "op": obj.op,
#                 "transforms": obj.transform_annotations,
#             }
#         elif isinstance(obj, gen.Index):
#             return obj.to_dict()
#         else:
#             return json.JSONEncoder.default(self, obj)

"""
# Transformations need to be applied in this order:
1. split_scopes
2. untile_buffer
3. untile_scope
4. delete_temporary
5. create_temporary
6. swap_ops
7. swap_nested_scopes
8. tile_scope
9. delete_dimension
10. create_dimension
11. tile_buffer
12. join_scopes
13. unroll_scope
14. swap_buffer_dims
15. reuse_arr_dims
16. reuse_buffers
17. move_buf_to_stack
"""

transforms = {
    # "split_scopes":  (split_scopes, find_splittable_scopes),
    # "untile_buffer": (untile_buffer, find_untileable_buffers),
    # "untile_scope":  (untile_scope, find_untileable_scopes),
    # "delete_temporary": (delete_temporary, find_deletable_temporaries),
    # "create_temporary": (create_temporary, find_creatable_temporaries),
    # "swap_ops": (swap_ops, find_swappable_ops),
    # "swap_nested_scopes": (swap_nested_scopes, find_swappable_nested_scopes),
    # "tile_scope": (tile_scope, find_tileable_scopes),
    # "delete_dimension": (delete_dimension, find_deletable_dimensions),
    # "create_dimension": (create_dimension, find_creatable_dimensions),
    # "tile_buffer": (tile_buffer, find_tileable_buffers),
    # "join_scope": (join_scopes, find_joinable_scopes),
    # "unroll_scope": (unroll_scope, find_unrollable_scopes),
    # "swap_buffer_dims": (swap_buffer_dims, find_swappable_buffer_dims),
    # "reuse_arr_dims": (reuse_arr_dims, find_reusable_arr_dims),
    # "reuse_buffers": (reuse_buffers, find_reusable_buffers),
    "move_buf_to_stack": (move_buf_to_stack, find_bufs_movable_to_stack),
}


if __name__ == "__main__":
    program = create_gemm_program()
    # sizes, fixed_inputs = create_gemm_inputs()
    program = create_batchnorm_program()
    sizes, fixed_inputs = create_batchnorm_inputs()
    # program = create_softmax_program()
    # sizes, fixed_inputs = create_softmax_inputs()
    # program = create_layernorm_program()
    # sizes, fixed_inputs = create_layernorm_inputs()
    # program = create_clip_program()
    # sizes, fixed_inputs = create_clip_inputs()
    # program = create_averagepool2d_nopad_program()
    # sizes, fixed_inputs = create_averagepool2d_nopad_inputs()
    # data_size = {'B': 32, 'N': 32}
    random.seed(0)
    specialize_inputs(program, sizes)  # needed to enable tiling
    # gen.apply_manual_layernorm_transformations(layernorm)

    # applicable_transforms = find_applicable_transforms(program)
    # add_transform_annotations(program, applicable_transforms)
    counter = 0
    print("[INFO] ============================== Program ==============================")
    clear_transform_annotations(program)
    print(program.text())
    for transform_name, (apply_transform, find_transform) in transforms.items():
        print(f"[INFO] Find transformation: {transform_name}")
        applicable_transforms = find_transform(program)
        print(applicable_transforms)
        clear_transform_annotations(program)

        if not applicable_transforms:
            continue
        # Annotate places at which the transformation can be applied
        # Adds annotation after the operation
        # FIXME Refactor this into a separate function
        if transform_name in ("split_scopes", "swap_ops", "swap_nested_scopes",
                              "join_scope", "unroll_scope", "untile_scope"):
            for tid, loc in enumerate(applicable_transforms):
                target_name = loc[0]
                res = find_anything_by_name(program, target_name)
                assert res is not None, f"Target {target_name} not found"
                parent, idx = res
                is_front = True
                parent.ops[idx].transform_annotations.append((is_front, f"${tid}"))
        elif transform_name in ("tile_scope"):
            for tid, loc in enumerate(applicable_transforms):
                target_name, tile_factor = loc
                res = find_anything_by_name(program, target_name)
                assert res is not None
                parent, idx = res
                is_front = True
                parent.ops[idx].transform_annotations.append((is_front, f"${tid} {tile_factor}"))
        elif transform_name in ("untile_buffer", "tile_buffer",
                                "reuse_arr_dims", "swap_buffer_dims"):
            for tid, loc in enumerate(applicable_transforms):
                buffer_name, dim_idx = loc
                assert buffer_name in program.buffers, f"Buffer {buffer_name} not found"
                program.buffers[buffer_name].transform_annotations.append((dim_idx, f"${tid}"))
        elif transform_name in ("reuse_buffers"):
            for tid, loc in enumerate(applicable_transforms):
                buffer_name, target_buffer_name = loc
                res = find_anything_by_name(program, buffer_name)
                # assert res is not None, f"Buffer {buffer_name} not found"
                is_front = False
                if res is not None:
                    parent, idx = res
                    parent.ops[idx].transform_annotations.append((is_front, f"${tid} {target_buffer_name}"))
                else:
                    # If the input buffer is referred to
                    assert buffer_name in program.buffers, f"Buffer {buffer_name} not found"
                    program.buffers[buffer_name].transform_annotations.append((is_front, f"${tid} {target_buffer_name}"))
        elif transform_name in ("create_temporary", "delete_temporary"):
            for tid, loc in enumerate(applicable_transforms):
                target_name = loc[0]
                res = find_anything_by_name(program, target_name)
                is_front = True
                if res is not None:
                    parent, idx = res
                    parent.ops[idx].transform_annotations.append((is_front, f"${tid}"))
                else:
                    # If the input buffer is referred to
                    assert target_name in program.buffers, f"Buffer {target_name} not found"
                    program.buffers[target_name].transform_annotations.append((is_front, f"${tid}"))
        elif transform_name in ("create_dimension", "delete_dimension"):
            for tid, loc in enumerate(applicable_transforms):
                target_name, operand_idx = loc
                # operand_idx refers to the index of the operand in the operation, not the buffer
                # 0 is the output, 1 is the first input, 2 is the second input, etc.
                res = find_anything_by_name(program, target_name)
                assert res is not None, f"Target {target_name} not found"
                parent, idx = res
                is_front = True
                operation = parent.ops[idx]
                assert isinstance(operation, Operation), f"Expected operation, got {type(operation)}"
                if operand_idx == 0:
                    operation.out_elem.transform_annotations.append((is_front, f"${tid}"))
                else:
                    operation.inp_elem[operand_idx - 1].transform_annotations.append((is_front, f"${tid}"))
                    
        elif transform_name in ("move_buf_to_stack"):
            for tid, loc in enumerate(applicable_transforms):
                buffer_name = loc[0]
                res = find_anything_by_name(program, buffer_name)
                assert res is not None, f"Buffer {buffer_name} not found"
                parent, idx = res
                is_front = True
                parent.ops[idx].transform_annotations.append((is_front, f"${tid}"))
        else:
            raise NotImplementedError(f"Transformation annotation not implemented: {transform_name}")

        print(program.text(True))
        apply_transform(program, *(applicable_transforms[0]))
        print(program.text())
        # parent.ops[idx].transform_annotations.add()
        counter += 1
        # print(f"[INFO] Apply transformation: {transform_name}")
        # add_transform_annotations(program, applicable_transforms)

    # counter = 0
    
    # while applicable_transforms:
    #     counter += 1
    #     print(f"================= {counter} =================")
    #     # print intermediate state
    #     # ln_json = json.dumps(program, cls=ProgramJSONEncoder)
    #     print(program.text())
    #     # print(yaml.dump(ln_json, default_flow_style=False, Dumper=YamlDumper))
        
    #     transform_idx = random.randint(0, len(applicable_transforms) - 1)
    #     transform, args = applicable_transforms[transform_idx]
        
    #     if counter == 3:
    #         break
        
    #     # if counter == 82:
    #     #     break
    #     print(f"Applying transform {transform_idx}: {transform.__name__}(program, {args_as_str(args)})")
    #     transform(program, *args)

    #     applicable_transforms = find_applicable_transforms(program)
    #     add_transform_annotations(program, applicable_transforms)

    # # ln_json = json.dumps(program, cls=ProgramJSONEncoder)
    # # pprint(json.loads(ln_json), width=200, compact=True, sort_dicts=False)

    print("Applied", counter, "transforms")
    
    # input_data = generate_input_for_program(program)
    # output_data = ref_layernorm({**input_data, **data_size})
    # cycles = test_snitch_code(program, {**input_data, **output_data}, simulator='rtl', check_correctness=True)
