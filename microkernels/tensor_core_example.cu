// nvcc -O3 -lineinfo tensor_core_example.cu -arch=sm_86 && ncu -fo 123 --set detailed ./a.out
// -maxrregcount=255 required without launch_bounds
#include <cstdio>
#include <vector>
#include <algorithm>
#include <cuda.h>
#include <cuda_fp16.h>
#include <mma.h>

using namespace nvcuda;





#define M 4096
#define N 4096
#define K 4096
#define WMMA_M 16
#define WMMA_N 16
#define WMMA_K 16
#define BLOCK_M 4
#define BLOCK_N 2
#define WARP_M 2
#define WARP_N 4
#define WARP_SIZE 32
#define GRID_M (M / (BLOCK_M * WARP_M * WMMA_M))
#define GRID_N (N / (BLOCK_N * WARP_N * WMMA_N))
#define STEP_K 4
#define TILES_K (K / (STEP_K * WMMA_K))
#define HALF_VEC 8 // (sizeof(int4) / sizeof(half))
#define FLOAT_VEC 4 // (sizeof(int4) / sizeof(float))
#define PAD_HALF 16



#define WARPS_PER_BLOCK (BLOCK_M * BLOCK_N)
#define THREADS_PER_BLOCK (WARP_SIZE * WARPS_PER_BLOCK)

#define CHUNK_LINE_BYTES (STEP_K * WMMA_K * sizeof(half))  // 4 * 16 * 2 = 128
#define WARP_COPY_BYTES (WARP_SIZE * sizeof(int4))  // 32 * 16 = 512
// #define CHUNK_COPY_LINES_PER_WARP (WARP_COPY_BYTES / CHUNK_LINE_BYTES)  // 512 / 128 = 4
#define CHUNK_COPY_LINES_PER_WARP ((WARP_SIZE * HALF_VEC) / (STEP_K * WMMA_K))
#define CHUNK_COPY_LINE_LANES ((STEP_K * WMMA_K) / HALF_VEC)  // 32 / 4 = 8

#define BLOCK_ROW_TILES (BLOCK_N * WARP_N)
#define BLOCK_COL_TILES (BLOCK_M * WARP_M)


#define cs_STRIDE (WMMA_N * BLOCK_N * WARP_N)
#define cs_OFFSET (WMMA_N * BLOCK_M)

__global__ void 
__launch_bounds__(WARP_SIZE * BLOCK_M * BLOCK_N, 1)
compute_gemm(const half *a, const half *b, float *c) {
    extern __shared__ float cs[];
    half* as = (half*)cs;
    half* bs = as + BLOCK_M * WARP_M * WMMA_M * (STEP_K * WMMA_K + PAD_HALF);

  // Warp and lane identification.
  const unsigned int warpId = threadIdx.x / WARP_SIZE;
  const unsigned int laneId = threadIdx.x % WARP_SIZE;

  // Offset in shared memory from which the B matrix is stored.
  const size_t cs_idx_b_off = BLOCK_COL_TILES * WMMA_M;

  // This pointer is used to access the C and D matrix tiles this warp computes.
  float *cs_warp_tile_ptr = ((float *)&cs) +
                               (warpId / 2) * cs_STRIDE * WMMA_K * 2 +
                               (warpId % 2) * cs_OFFSET;

  // This pointer is used to stream the C and D matrices block-wide tile to and
  // from shared memory.
    float *cs_warp_stream_ptr =
      ((float *)cs) + warpId * cs_STRIDE * WMMA_K;



    // These fragments will accumulate the result of A and B matrix fragment
    // multiplications along the K_GLOBAL dimension.
    wmma::fragment<wmma::matrix_a, WMMA_M, WMMA_N, WMMA_K, half, wmma::row_major> ar[BLOCK_N];
    wmma::fragment<wmma::matrix_b, WMMA_M, WMMA_N, WMMA_K, half, wmma::col_major> br[BLOCK_M];
    wmma::fragment<wmma::accumulator, WMMA_M, WMMA_N, WMMA_K, float> cr[BLOCK_N][BLOCK_M];

  // Each CTA slides along the 128 x 128 tiles from the top left corner of the
  // matrix to the right and down, and selects the next tile to compute. Once
  // there's no such tile, all warps in this CTA exit.
  for (int grid_mn = blockIdx.x; grid_mn < GRID_M * GRID_N; grid_mn += gridDim.x) {

    int grid_m = grid_mn / GRID_N;
    int grid_n = grid_mn % GRID_N;

    const unsigned int block_tile_i = grid_m * BLOCK_COL_TILES;
    const unsigned int block_tile_j = grid_n * BLOCK_ROW_TILES;
 
    // This warp's pointer to the C matrix data to copy memory from to shared
    // memory.
    // const size_t gmem_idx =
    //     (block_tile_i + warpId) * WMMA_M * N + block_tile_j * WMMA_N;

    // Load the C matrix tiles into fragments from shared memory.
#pragma unroll
    for (int i = 0; i < BLOCK_N; i++) {
#pragma unroll
      for (int j = 0; j < BLOCK_M; j++) {
        // const float *tile_ptr =
        //     cs_warp_tile_ptr + i * cs_STRIDE * K + j * N;

        // wmma::load_matrix_sync(c[i][j], tile_ptr, cs_STRIDE, C_LAYOUT);
        wmma::fill_fragment(cr[i][j], 0.0f);
      }
    }


    const half *warp_ptr_a = (&a[block_tile_i * WMMA_M * K] + WMMA_M * K * warpId);
    const half *warp_ptr_b = (&b[block_tile_j * WMMA_N * K] + WMMA_N * K * warpId);

    // Go through the global K dimension by a fixed step at a time.
#pragma unroll
    for (int tile_k = 0; tile_k < K / STEP_K / WMMA_K; tile_k += 1) {
        // Copy slices of the A and B matrices to shared memory.
        // The first half of the warps in the CTA copy the A matrix, the rest copy
        // the B matrix.

        // size_t cs_idx_a = (WMMA_M * warpId);
        // int4 *lane_ptr_a = (int4 *)(warp_ptr_a + tile_k * STEP_K * WMMA_K +
        //                             (laneId / ((STEP_K * WMMA_K) / HALF_VEC)) * K) +
        //                 (laneId % ((STEP_K * WMMA_K) / HALF_VEC));
        // cs_idx_a += laneId / ((STEP_K * WMMA_K) / HALF_VEC);
        // #pragma unroll
        // for (int i = 0; i < ((WARP_SIZE / 2) / ((WARP_SIZE * HALF_VEC) / (STEP_K * WMMA_K))); i++) {
        //     // Copy 16 bytes at once in each lane.
        //     *((int4 *)&as[cs_idx_a * (STEP_K * WMMA_K + PAD_HALF)] + (laneId % ((STEP_K * WMMA_K) / HALF_VEC))) = *lane_ptr_a;

        //     // Advance the global memory pointer and the shared memory index.
        //     lane_ptr_a = (int4 *)((half *)lane_ptr_a + K * ((WARP_SIZE * HALF_VEC) / (STEP_K * WMMA_K)));
        //     cs_idx_a += ((WARP_SIZE * HALF_VEC) / (STEP_K * WMMA_K));
        // }

        // load tile A into shared memory
        // #pragma unroll
        // for (int m = threadIdx.x / 8; m < BLOCK_M * WARP_M * WMMA_M; m += WARP_SIZE * BLOCK_M * BLOCK_N / 8) {
        //     #pragma unroll
        //     for (int k = (threadIdx.x % 8) * HALF_VEC; k < STEP_K * WMMA_K; k += HALF_VEC * 8) {
        //     // int k = (threadIdx.x % 8) * HALF_VEC;
        //     // {
        //         *(int4*)&as[
        //             (k) + (m) * (STEP_K * WMMA_K + PAD_HALF)
        //         ] = *(int4*)&a[
        //             (tile_k * STEP_K * WMMA_K + k) + 
        //             (grid_m * BLOCK_M * WARP_M * WMMA_M + m) * K
        //         ];
        //     }
        // }

        // this is actual iteration space
        // for (int m = 0; m < BLOCK_M * WARP_M * WMMA_M; m += 1) {
        //     for (int k = 0 * HALF_VEC; k < STEP_K * WMMA_K; k += HALF_VEC) {

        const int M_ITER = BLOCK_M * WARP_M * WMMA_M; // 128
        const int K_ITER = STEP_K * WMMA_K; // 64
        const int THREADS_PER_ROW = K_ITER / HALF_VEC; // 8
        const int WARP_ROWS = M_ITER / WARP_SIZE; // 4
        const int BLOCK_SIZE = WARP_SIZE * BLOCK_M * BLOCK_N; // 256
        int m1 = (threadIdx.x / THREADS_PER_ROW); {
        // for (int m1 = (threadIdx.x / THREADS_PER_ROW); m1 < M_ITER / WARP_ROWS; m1 += BLOCK_SIZE / THREADS_PER_ROW) { // parallel
            #pragma unroll
            for (int m0 = 0; m0 < WARP_ROWS; m0 += 1) { // sequential
                int m = m1 * WARP_ROWS + m0;
                int k = (threadIdx.x % THREADS_PER_ROW) * HALF_VEC; {
                // for (int k = (threadIdx.x % THREADS_PER_ROW) * HALF_VEC; k < K_ITER; k += HALF_VEC * THREADS_PER_ROW) { // parallel
                    *(int4*)&as[
                    (k) + (m) * (STEP_K * WMMA_K + PAD_HALF)
                ] = *(int4*)&a[
                    (tile_k * STEP_K * WMMA_K + k) + 
                    (grid_m * BLOCK_M * WARP_M * WMMA_M + m) * K
                ];
                }
            }
        }


        // for (int m = threadIdx.x / 8; m < BLOCK_M * WARP_M * WMMA_M; m += WARP_SIZE * BLOCK_M * BLOCK_N / 8) {
        //     for (int k = (threadIdx.x % 8) * HALF_VEC; k < STEP_K * WMMA_K; k += HALF_VEC * 8) {
        //         *(int4*)&as[
        //             (k) + (m) * (STEP_K * WMMA_K + PAD_HALF)
        //         ] = *(int4*)&a[
        //             (tile_k * STEP_K * WMMA_K + k) + 
        //             (grid_m * BLOCK_M * WARP_M * WMMA_M + m) * K
        //         ];
        //     }
        // }


    size_t cs_idx_b = (WMMA_N * (warpId) + cs_idx_b_off);
    int4 *lane_ptr_b = (int4 *)(warp_ptr_b + tile_k * STEP_K * WMMA_K +
                                (laneId / CHUNK_COPY_LINE_LANES) * K) +
                       (laneId % CHUNK_COPY_LINE_LANES);
    cs_idx_b += laneId / CHUNK_COPY_LINE_LANES;
    #pragma unroll
    for (int i = 0; i < ((WARP_SIZE / 2) / CHUNK_COPY_LINES_PER_WARP); i++) {
        // Copy 16 bytes at once in each lane.
        *((int4 *)&as[cs_idx_b * (STEP_K * WMMA_K + PAD_HALF)] + (laneId % CHUNK_COPY_LINE_LANES)) = *lane_ptr_b;

        // Advance the global memory pointer and the shared memory index.
        lane_ptr_b = (int4 *)((half *)lane_ptr_b + K * CHUNK_COPY_LINES_PER_WARP);
        cs_idx_b += CHUNK_COPY_LINES_PER_WARP;
    }

      __syncthreads();

      // Compute a grid of C matrix tiles in each warp.
#pragma unroll
      for (int k_step = 0; k_step < STEP_K; k_step++) {

        for (int j = 0; j < BLOCK_M; j++) {
              // Load the B matrix fragment once, because it is going to be
              // reused against the other A matrix fragments.
              size_t cs_idx_b = cs_idx_b_off +
                                   (BLOCK_M * WMMA_N) * (warpId % 2) +
                                   (j * WMMA_N);
              const half *tile_ptr = &as[cs_idx_b * (STEP_K * WMMA_K + PAD_HALF) + k_step * WMMA_K];

              wmma::load_matrix_sync(br[j], tile_ptr, WMMA_K * STEP_K + PAD_HALF);
        }

#pragma unroll
        for (int i = 0; i < BLOCK_N; i++) {
          size_t cs_idx_a = (warpId / 2) * WMMA_M * 2 + (i * WMMA_M);
          const half *tile_ptr = &as[cs_idx_a * (STEP_K * WMMA_K + PAD_HALF) + k_step * WMMA_K];

          wmma::load_matrix_sync(ar[i], tile_ptr, WMMA_K * STEP_K + PAD_HALF);

#pragma unroll
          for (int j = 0; j < BLOCK_M; j++) {

            wmma::mma_sync(cr[i][j], ar[i], br[j], cr[i][j]);
          }
        }
      }

      __syncthreads();
    }

      // Store the D fragments to shared memory.
#pragma unroll
    for (int i = 0; i < BLOCK_N; i++) {
#pragma unroll
      for (int j = 0; j < BLOCK_M; j++) {

        float *tile_ptr = cs_warp_tile_ptr + i * cs_STRIDE * WMMA_K + j * WMMA_N;

        wmma::store_matrix_sync(tile_ptr, cr[i][j], cs_STRIDE, wmma::mem_row_major);
      }
    }

    __syncthreads();

    float* c1 = &c[grid_m * BLOCK_M * WARP_M * WMMA_M * N + grid_n * BLOCK_N * WARP_N * WMMA_N];

    for (int m = threadIdx.x / 32; m < BLOCK_M * WARP_M * WMMA_M; m += WARP_SIZE * BLOCK_M * BLOCK_N / 32) {
        #pragma unroll
        for (int n = (threadIdx.x % 32) * FLOAT_VEC; n < BLOCK_N * WARP_N * WMMA_N; n += FLOAT_VEC * 32) {
            // *(int4*)(&c[(grid_m * BLOCK_M * WARP_M * WMMA_M + m) * N + (grid_n * BLOCK_N * WARP_N * WMMA_N + n)]) = 
            *(int4*)(&c1[(m) * (N) + (n)]) =
            *(int4*)(&cs[(m) * (BLOCK_N * WARP_N * WMMA_N) + (n)]);
        }
    }

    __syncthreads();
  }
}




// #if !defined(TOC)
// static inline double elapsed_seconds(struct timespec start) {
//     struct timespec end;
//     clock_gettime(CLOCK_MONOTONIC, &end);
//     return (end.tv_sec - start.tv_sec) + (end.tv_nsec - start.tv_nsec) / 1e9;
// }
// #define TIC(name) struct timespec dt_ ##name; clock_gettime(CLOCK_MONOTONIC, &dt_ ##name)
// #define TOC(name) elapsed_seconds(dt_ ## name)
// #endif


#define checkCudaErrors(expr) do { \
    cudaError_t err = (expr); \
    if (err != cudaSuccess) { \
        printf("Error in %s:%d. Message: %s\n", __FILE__, __LINE__, cudaGetErrorString(err)); \
        exit(1); \
    } \
} while (0)



__global__ void kernel1(half* a, half* b, float* c) {
    int m = (blockIdx.y * blockDim.y + threadIdx.y) * WMMA_M;
    int n = (blockIdx.z * blockDim.z + threadIdx.z) * WMMA_N;

    // Declare the fragments
    wmma::fragment<wmma::matrix_a, WMMA_M, WMMA_N, WMMA_K, half, wmma::col_major> a_frag;
    wmma::fragment<wmma::matrix_b, WMMA_M, WMMA_N, WMMA_K, half, wmma::row_major> b_frag;
    wmma::fragment<wmma::accumulator, WMMA_M, WMMA_N, WMMA_K, float> c_frag;

    // Initialize the output to zero
    wmma::fill_fragment(c_frag, (half)0.0f);

    for (int k = 0; k < K; k += WMMA_K) {
        // Load the inputs
        wmma::load_matrix_sync(a_frag, &a[k * M + m], M);
        wmma::load_matrix_sync(b_frag, &b[k * N + n], N);

        // Perform the matrix multiplication
        wmma::mma_sync(c_frag, a_frag, b_frag, c_frag);
    }

    // Store the output
    wmma::store_matrix_sync(&c[m * N + n], c_frag, N, wmma::mem_row_major);
}


__global__ void 
__launch_bounds__(WARP_SIZE * BLOCK_M * BLOCK_N, 1)
kernel(const half* a, const half* b, float* c) {
    // block will process an output tile of size (BLOCK_M * WARP_M * WMMA_M) x (BLOCK_N * WARP_N * WMMA_N) = 128 x 128
    // int grid_m = blockIdx.x / GRID_N;  // 0 to GRID_M
    // int grid_n = blockIdx.x % GRID_N;  // 0 to GRID_N
    
    // warp will process a tile of size (WARP_M * WMMA_M) x (WARP_N * WMMA_N) = 32 x 64
    // int lane = threadIdx.x % WARP_SIZE;
    int warp = threadIdx.x / WARP_SIZE;
    int block_m = warp / BLOCK_N;  // 0 to BLOCK_M
    int block_n = warp % BLOCK_N;  // 0 to BLOCK_N

    extern __shared__ float cs[]; // size: BLOCK_M * WARP_M * WMMA_M * BLOCK_N * WARP_N * WMMA_N
    half* as = (half*)cs; // size: BLOCK_M * WARP_M * WMMA_M * (STEP_K * WMMA_K + PAD_HALF)
    half* bs = as + BLOCK_M * WARP_M * WMMA_M * (STEP_K * WMMA_K + PAD_HALF); // size: BLOCK_N * WARP_N * WMMA_N * (STEP_K * WMMA_K + PAD_HALF)

    wmma::fragment<wmma::matrix_a, WMMA_M, WMMA_N, WMMA_K, half, wmma::row_major> ar[WARP_M];
    wmma::fragment<wmma::matrix_b, WMMA_M, WMMA_N, WMMA_K, half, wmma::col_major> br[WARP_N];
    wmma::fragment<wmma::accumulator, WMMA_M, WMMA_N, WMMA_K, float> cr[WARP_M][WARP_N];

    for (int grid_mn = blockIdx.x; grid_mn < GRID_M * GRID_N; grid_mn += gridDim.x) {

        int grid_m = grid_mn / GRID_N;
        int grid_n = grid_mn % GRID_N;

        #pragma unroll
        for (int warp_m = 0; warp_m < WARP_M; warp_m++) {
            #pragma unroll
            for (int warp_n = 0; warp_n < WARP_N; warp_n++) {
                wmma::fill_fragment(cr[warp_m][warp_n], 0.0f);
            }
        }

        for (int tile_k = 0; tile_k < TILES_K; tile_k++) {
            // load tile B into shared memory
            #pragma unroll
            for (int n = threadIdx.x / 8; n < BLOCK_N * WARP_N * WMMA_N; n += WARP_SIZE * BLOCK_M * BLOCK_N / 8) {
                // for (int k = (lane % 8) * HALF_VEC; k < STEP_K * WMMA_K; k += HALF_VEC * 8) {
                // #pragma unroll
                // for (int k = (threadIdx.x % 8) * HALF_VEC; k < STEP_K * WMMA_K; k += 8 * HALF_VEC) {
                int k = (threadIdx.x % 8) * HALF_VEC;
                {
                    *(int4*)&bs[
                        (k) + (n) * (STEP_K * WMMA_K + PAD_HALF)
                    ] = *(int4*)&b[
                        (tile_k * STEP_K * WMMA_K + k) + 
                        (grid_n * BLOCK_N * WARP_N * WMMA_N + n) * K
                    ];
                }
            }
            // load tile A into shared memory
            #pragma unroll
            for (int m = threadIdx.x / 8; m < BLOCK_M * WARP_M * WMMA_M; m += WARP_SIZE * BLOCK_M * BLOCK_N / 8) {
                // #pragma unroll
                // for (int k = (threadIdx.x % 8) * HALF_VEC; k < STEP_K * WMMA_K; k += HALF_VEC * 8) {
                int k = (threadIdx.x % 8) * HALF_VEC;
                {
                    *(int4*)&as[
                        (k) + (m) * (STEP_K * WMMA_K + PAD_HALF)
                    ] = *(int4*)&a[
                        (tile_k * STEP_K * WMMA_K + k) + 
                        (grid_m * BLOCK_M * WARP_M * WMMA_M + m) * K
                    ];
                }
            }
            __syncthreads();
            
            #pragma unroll
            for (int step_k = 0; step_k < STEP_K; step_k++) {
                #pragma unroll
                for (int warp_m = 0; warp_m < WARP_M; warp_m++) {
                    wmma::load_matrix_sync(ar[warp_m], 
                        &as[(step_k * WMMA_K) + (block_m * WARP_M * WMMA_M + warp_m * WMMA_M) * (STEP_K * WMMA_K + PAD_HALF)],
                        (STEP_K * WMMA_K + PAD_HALF)
                    );
                }
                #pragma unroll
                for (int warp_n = 0; warp_n < WARP_N; warp_n++) {
                    wmma::load_matrix_sync(br[warp_n], 
                        &bs[(step_k * WMMA_K) + (block_n * WARP_N * WMMA_N + warp_n * WMMA_N) * (STEP_K * WMMA_K + PAD_HALF)],
                        (STEP_K * WMMA_K + PAD_HALF)
                    );
                    #pragma unroll
                    for (int warp_m = 0; warp_m < WARP_M; warp_m++) {
                        wmma::mma_sync(cr[warp_m][warp_n], ar[warp_m], br[warp_n], cr[warp_m][warp_n]);
                    }
                }
                __syncthreads();
            }
        }
        // from registers to shared
        #pragma unroll
        for (int warp_m = 0; warp_m < WARP_M; warp_m++) {
            #pragma unroll
            for (int warp_n = 0; warp_n < WARP_N; warp_n++) {
                wmma::store_matrix_sync(&cs[
                    (block_m * WARP_M * WMMA_M + warp_m * WMMA_M) * (BLOCK_N * WARP_N * WMMA_N)
                            + (block_n * WARP_N * WMMA_N + warp_n * WMMA_N)],
                    cr[warp_m][warp_n], BLOCK_N * WARP_N * WMMA_N, wmma::mem_row_major);
            }
        }
        __syncthreads();
        // from shared to global
        // if (block_m == 0 && block_n == 0 && threadIdx.x == 0)
        // for (int m = 0; m < BLOCK_M * WARP_M * WMMA_M; m += 1) {
        //     for (int n = 0; n < BLOCK_N * WARP_N * WMMA_N; n += FLOAT_VEC) {
        #pragma unroll
        for (int m = threadIdx.x / 32; m < BLOCK_M * WARP_M * WMMA_M; m += WARP_SIZE * BLOCK_M * BLOCK_N / 32) {
            // #pragma unroll
            // for (int n = (threadIdx.x % 32) * FLOAT_VEC; n < BLOCK_N * WARP_N * WMMA_N; n += FLOAT_VEC * 32) {
            int n = (threadIdx.x % 32) * FLOAT_VEC;
            {
                *(int4*)(&c[(grid_m * BLOCK_M * WARP_M * WMMA_M + m) * N + 
                (grid_n * BLOCK_N * WARP_N * WMMA_N + n)]) = 
                *(int4*)(&cs[(m) * (BLOCK_N * WARP_N * WMMA_N) + (n)]);
            }
        }
        __syncthreads();
    }
}






int main() {
    cudaDeviceProp deviceProp;
    if (cudaSuccess != cudaGetDeviceProperties(&deviceProp, 0)) {
        printf("Error getting device properties\n");
        return 1;
    }
    printf("Multiprocessors: %d\n", deviceProp.multiProcessorCount);
    half *a;
    half *b;
    float *c;
    cudaMallocManaged(&a, sizeof(half) * K * M);
    cudaMallocManaged(&b, sizeof(half) * K * N);
    cudaMallocManaged(&c, sizeof(float) * M * N);
    // cudaMalloc(&a, sizeof(half) * K * M);
    // cudaMalloc(&b, sizeof(half) * K * N);
    // cudaMalloc(&c, sizeof(float) * M * N);
    // cudaMalloc(&d, sizeof(float) * M * N);
    // cudaMemset(a, 0, sizeof(half) * M * K);
    // cudaMemset(b, 0, sizeof(half) * K * N);
    // cudaMemset(c, 0, sizeof(float) * M * N);
    // initialize
    for (int i = 0; i < M * K; i++) {
        a[i] = __float2half(((float)rand() / RAND_MAX) - 0.5);
    }
    for (int i = 0; i < K * N; i++) {
        b[i] = __float2half(((float)rand() / RAND_MAX) - 0.5);
    }
    for (int i = 0; i < M * N; i++) {
        c[i] = 0;
    }
    int cs = BLOCK_M * WARP_M * WMMA_M * BLOCK_N * WARP_N * WMMA_N * sizeof(float);
    checkCudaErrors(cudaFuncSetAttribute(kernel, cudaFuncAttributeMaxDynamicSharedMemorySize, cs));
    checkCudaErrors(cudaFuncSetAttribute(compute_gemm, cudaFuncAttributeMaxDynamicSharedMemorySize, cs));

    cudaEvent_t start, stop;

    checkCudaErrors(cudaEventCreate(&start));
    checkCudaErrors(cudaEventCreate(&stop));

    // launch
    // int warmup = 3;
    // int repeats = 10;
    // int number = 10;
    int warmup = 1;
    int repeats = 1;
    int number = 1;
    std::vector<double> times;
    for (int r = 0; r < warmup + repeats; r++) {
        // TIC(1);
        checkCudaErrors(cudaEventRecord(start));
        for (int num = 0; num < number; num++) {
            // kernel<<<deviceProp.multiProcessorCount, WARP_SIZE * BLOCK_M * BLOCK_N, cs>>>(a, b, c);
            compute_gemm<<<deviceProp.multiProcessorCount, WARP_SIZE * BLOCK_M * BLOCK_N, cs>>>(a, b, c);
            checkCudaErrors(cudaGetLastError());
        }
        checkCudaErrors(cudaEventRecord(stop));
        checkCudaErrors(cudaEventSynchronize(stop));
        // checkCudaErrors(cudaDeviceSynchronize());
        float milliseconds = 0;
        cudaEventElapsedTime(&milliseconds, start, stop);
        double time = milliseconds / 1e3 / number;
        // double time = TOC(1) / number;
        // printf("Time [ms] events %f cpu %f\n", milliseconds / number, time * 1e3);
        if (r >= warmup) {
            times.push_back(time);
        }
    }
    double min_time = 1e10;
    double max_time = 0;
    double avg_time = 0;
    for (int i = 0; i < times.size(); i++) {
        if (times[i] < min_time) {
            min_time = times[i];
        }
        if (times[i] > max_time) {
            max_time = times[i];
        }
        avg_time += times[i];
    }
    avg_time /= times.size();
    printf("Time [ms] min: %.3f, avg: %.3f, max: %.3f\n", min_time * 1e3, avg_time * 1e3, max_time * 1e3);
    printf("tflop/s: %.3f\n", 1.0 / (avg_time * 1e12) * M * N * K);
    // (random) verify
    int elems_to_verify = 10000;
    for (int i = 0; i < elems_to_verify; i++) {
        int m = rand() % M;
        int n = rand() % N;
        float sum = 0;
        for (int k = 0; k < K; k++) {
            sum += (float)a[m * K + k] * (float)b[n * K + k];
        }
        float eps = 1e-3;
        float val = (float)c[m * N + n];
        if (fabs(val - sum) > eps && fabs(val - sum)/(fabs(sum)+eps) > eps) {
            printf("Error at (%d, %d): %f != %f\n", m, n, c[m * N + n], sum);
            return 1;
        }
    }
    printf("Verification passed\n");
}
