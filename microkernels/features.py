import subprocess
import os
import pathlib
import functools
import shutil
import torch
from pathlib import Path


@functools.cache
def get_compiler():
    CC = os.getenv("CC")
    if CC is not None:
        return CC
    if gcc_available():
        return "gcc"
    if clang_available():
        return "clang"
    return None

@functools.cache
def get_nvcc():
    NVCC = os.getenv("NVCC")
    if NVCC is not None:
        return NVCC
    if nvcc_available():
        return "nvcc"
    
@functools.cache
def get_cuda_path():
    NVCC = get_nvcc()
    if NVCC is None:
        return None
    nvcc_path = shutil.which(NVCC)
    if nvcc_path is None:
        return None
    nvcc_dir = os.path.dirname(nvcc_path)
    cuda_dir = os.path.realpath(os.path.abspath(os.path.join(nvcc_dir, "..")))
    return cuda_dir

@functools.cache
def get_include_dir():
    library_dir = get_library_dir()
    return (library_dir.parent / "include").resolve()

@functools.cache
def get_library_dir():
    return Path(torch.__file__).parent.parent.parent.parent.resolve()

@functools.cache
def get_cuda_cap():
    try:
        compute_capability_csv = subprocess.run('nvidia-smi --query-gpu=compute_cap --format=csv'.split(), check=True, capture_output=True, text=True).stdout  # i.e. 'compute_cap\n7.5\n7.5\n7.5\n'
    except subprocess.CalledProcessError:
        return None
    compute_capability_str = compute_capability_csv.split('\n')[1]  # i.e. '7.5'
    compute_capability = compute_capability_str.replace('.', '')  # i.e. '75'
    return compute_capability

@functools.cache
def cuda_available():
    return get_cuda_cap() is not None

@functools.cache
def gcc_available():
    try:
        subprocess.run(["gcc", "--version"], check=True, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    except subprocess.CalledProcessError:
        return False
    return True

@functools.cache
def clang_available():
    try:
        subprocess.run(["clang", "--version"], check=True, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    except subprocess.CalledProcessError:
        return False
    return True

@functools.cache
def nvcc_available():
    try:
        subprocess.run(["nvcc", "--version"], check=True, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    except subprocess.CalledProcessError:
        return False
    return True

@functools.cache
def neon_available():
    code = """
    #include <stdio.h>
    #include <arm_neon.h>

    int main() {
        float a[4] = {1, 2, 3, 4};
        float b[4] = {99, 98, 97, 96};
        float c[4] = {0};
        float32x4_t va = vld1q_f32(a);
        float32x4_t vb = vld1q_f32(b);
        float32x4_t vc = vaddq_f32(va, vb);
        vst1q_f32(c, vc);
        for (int i = 0; i < 4; i++) {
            if (c[i] != 100) {
                return 1;
            }
        }
        return 0;
    }
    """

    src_file = pathlib.Path("neon_test.c").resolve()
    bin_file = pathlib.Path("neon_test").resolve()

    CC = get_compiler()
    if CC is None:
        return False
    CFLAGS = os.getenv("CFLAGS", "-march=native")

    try:
        with open(src_file, "w") as f:
            f.write(code)
        subprocess.run([CC, "-o", bin_file, src_file] + CFLAGS.split(), check=True, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        subprocess.run([bin_file], check=True, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    except subprocess.CalledProcessError:
        return False
    finally:
        src_file.unlink(missing_ok=True)
        bin_file.unlink(missing_ok=True)
    return True

@functools.cache
def avx_available():
    code = """
    #include <stdio.h>
    #include <immintrin.h>
    
    int main() {
        float a[8] = {1, 2, 3, 4, 5, 6, 7, 8};
        float b[8] = {100, 99, 98, 97, 96, 95, 94, 93};
        float c[8] = {0};
        __m256 va = _mm256_loadu_ps(a);
        __m256 vb = _mm256_loadu_ps(b);
        __m256 vc = _mm256_add_ps(va, vb);
        _mm256_storeu_ps(c, vc);
        for (int i = 0; i < 8; i++) {
            if (c[i] != 100) {
                return 1;
            }
        }
        return 0;
    }
    """
    
    src_file = pathlib.Path("avx_test.c").resolve()
    bin_file = pathlib.Path("avx_test").resolve()
    
    CC = get_compiler()
    if CC is None:
        return False
    CFLASG = os.getenv("CFLAGS", "-march=native")
    
    try:
        with open(src_file, "w") as f:
            f.write(code)
        subprocess.run([CC, "-o", bin_file, src_file] + CFLASG.split(), check=True, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        subprocess.run([bin_file], check=True, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    except subprocess.CalledProcessError:
        return False
    finally:
        src_file.unlink(missing_ok=True)
        bin_file.unlink(missing_ok=True)
    return True

@functools.cache
def avx512_available():
    code = """
    #include <stdio.h>
    #include <immintrin.h>
    
    int main() {
        float a[16] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16};
        float b[16] = {99, 98, 97, 96, 95, 94, 93, 92, 91, 90, 89, 88, 87, 86, 85, 84};
        float c[16] = {0};
        __m512 va = _mm512_loadu_ps(a);
        __m512 vb = _mm512_loadu_ps(b);
        __m512 vc = _mm512_add_ps(va, vb);
        _mm512_storeu_ps(c, vc);
        for (int i = 0; i < 16; i++) {
            if (c[i] != 100) {
                return 1;
            }
        }
        return 0;
    }
    """
    
    src_file = pathlib.Path("avx512_test.c").resolve()
    bin_file = pathlib.Path("avx512_test").resolve()
    
    CC = get_compiler()
    if CC is None:
        return False
    CFLASG = os.getenv("CFLAGS", "-march=native")
    
    try:
        with open(src_file, "w") as f:
            f.write(code)
        subprocess.run([CC, "-o", bin_file, src_file] + CFLASG.split(), check=True, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        subprocess.run([bin_file], check=True, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    except subprocess.CalledProcessError:
        return False
    finally:
        src_file.unlink(missing_ok=True)
        bin_file.unlink(missing_ok=True)
    return True

def avx512fp16_available():
    code = """
    #include <stdio.h>
    #include <immintrin.h>
    
    int main() {
        _Float16 a[32] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32};
        _Float16 b[32] = {99, 98, 97, 96, 95, 94, 93, 92, 91, 90, 89, 88, 87, 86, 85, 84, 83, 82, 81, 80, 79, 78, 77, 76, 75, 74, 73, 72, 71, 70, 69, 68};
        _Float16 c[32] = {0};
        __m512h va = _mm512_loadu_ph(a);
        __m512h vb = _mm512_loadu_ph(b);
        __m512h vc = _mm512_add_ph(va, vb);
        _mm512_storeu_ph(c, vc);
        for (int i = 0; i < 32; i++) {
            if (c[i] != 100) {
                return 1;
            }
        }
        return 0;
    }
    """
    
    src_file = pathlib.Path("avx512fp16_test.c").resolve()
    bin_file = pathlib.Path("avx512fp16_test").resolve()
    
    CC = get_compiler()
    if CC is None:
        return False
    CFLAGS = os.getenv("CFLAGS", "-march=native")
    
    try:
        with open(src_file, "w") as f:
            f.write(code)
        subprocess.run([CC, "-o", bin_file, src_file] + CFLAGS.split(), check=True, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        subprocess.run([bin_file], check=True, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    except subprocess.CalledProcessError as e:
        return False
    finally:
        src_file.unlink(missing_ok=True)
        bin_file.unlink(missing_ok=True)
    return True


if __name__ == "__main__":
    print(f"GCC available: {gcc_available()}")
    print(f"Clang available: {clang_available()}")
    print(f"NEON available: {neon_available()}")
    print(f"AVX available: {avx_available()}")
    print(f"AVX512 available: {avx512_available()}")
    print(f"AVX512-FP16 available: {avx512fp16_available()}")
