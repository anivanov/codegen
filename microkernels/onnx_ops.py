from onnx import TensorProto
from onnx.helper import (
    make_model, make_node, set_model_props, make_tensor,
    make_graph, make_tensor_value_info, make_opsetid,
    make_function)
from onnx.checker import check_model
from onnxruntime import InferenceSession
import numpy as np
import re
import itertools
import math

from gen import RootScope, loop_scope, Operation, Buffer, Const
import gen
from textwrap import dedent
import subprocess

def onnx_elemwise_op(op_name, input):
    X = make_tensor_value_info('X', TensorProto.FLOAT, [None, None])
    Y = make_tensor_value_info('Y', TensorProto.FLOAT, [None, None])
    node = make_node(op_name, ['X'], ['Y'])
    graph = make_graph([node], f'graph_{op_name}', [X], [Y])
    new_domain = 'custom'
    opset_imports = [make_opsetid("", 14), make_opsetid(new_domain, 1)]
    onnx_model = make_model(graph, opset_imports=opset_imports)
    onnx_model.ir_version = 8
    check_model(onnx_model)
    sess = InferenceSession(
        onnx_model.SerializeToString(),
        providers=["CPUExecutionProvider"])
    [output] = sess.run(None, {'X': input})
    return output


def onnx_binary_op(op_name, src1, src2):
    dtype = {
        'float32': TensorProto.FLOAT,
        'bool': TensorProto.BOOL
    }[src1.dtype.name]

    X = make_tensor_value_info('X', dtype, [None, None])
    Y = make_tensor_value_info('Y', dtype, [None, None])
    Z = make_tensor_value_info('Z', dtype, [None, None])
    node = make_node(op_name, ['X', 'Y'], ['Z'])
    graph = make_graph([node], f'graph_{op_name}', [X, Y], [Z])
    new_domain = 'custom'
    opset_imports = [make_opsetid("", 14), make_opsetid(new_domain, 1)]
    onnx_model = make_model(graph, opset_imports=opset_imports)
    onnx_model.ir_version = 8
    check_model(onnx_model)
    sess = InferenceSession(
        onnx_model.SerializeToString(),
        providers=["CPUExecutionProvider"])
    [dst] = sess.run(None, {'X': src1, 'Y': src2})
    return dst


def test_snitch_code(program, ref_data):
    code = gen.gencode(program)

    signature = gen.make_signature(program)

    buf_size = gen.get_tmp_buf_size(program)

    srcs = {}
    dsts = {}
    for k, v in ref_data.items():
        if k.startswith("src"):
            srcs[k] = v
        elif k.startswith("dst"):
            dsts[k] = v
        else:
            raise Exception("Only arrays named as 'src*' or 'dst*' are supported")

    D = [v.shape for k, v in srcs.items()][0] 
    dtype = [v.dtype.name for k, v in srcs.items()][0]

    dtype = {
        'float32': 'f64',
        'int32': 'i32',
        'bool': 'i32'
    }[dtype]

    test_dtype = {
        'f64': 'float64',
        'i32': 'int32',
    }[dtype]
            
    src_data = {
        k: ', '.join(str(x) for x in v.flatten().astype(test_dtype).tolist())
        for k, v in srcs.items()
    }
    dst_data = {
        k: ', '.join(str(x) for x in v.flatten().astype(test_dtype).tolist())
        for k, v in dsts.items()
    }

    src_init_code = []
    for k, v in src_data.items():
        src_init_code.append(f"{dtype} ref_" + k + "[] = {" + v + "};")
    dst_init_code = []
    for k, v in dst_data.items():
        dst_init_code.append(f"{dtype} ref_" + k + "[] = {" + v + "};")

    src_init_code = "\n".join(src_init_code)
    dst_init_code = "\n".join(dst_init_code)

    shape_init_code = []
    for i, d in enumerate(D):
        shape_init_code.append(f'i32 _D{i} = {d};')
        shape_init_code.append(f'i32* D{i} = &_D{i};')
    shape_init_code = "\n".join(shape_init_code)

    total_size = " * ".join(f"(*D{i})" for i in range(len(D)))

    array_init_code = []
    for k, v in ref_data.items():
        array_init_code.append(f"{dtype}* {k} = ({dtype}*) snrt_l3alloc({total_size} * sizeof({dtype}));")
        if k in src_data:
            array_init_code.append(f"for (i32 i = 0; i < {total_size}; i++) {k}[i] = ref_{k}[i];")
    array_init_code = "\n".join(array_init_code)

    array_verify_code = []
    for k, v in dst_data.items():
        array_verify_code.append(f"for (int i = 0; i < {total_size}; i++) {{")
        array_verify_code.append(f"    if (({k}[i] - ref_{k}[i]) > 1e-3) {{")
        array_verify_code.append(f'        printf("Error: mismatch at {k}, %d, %f != %f\\n", i, {k}[i], ref_{k}[i]);')
        array_verify_code.append(f"        ok = 0;")
        array_verify_code.append(f"    }}")
        array_verify_code.append(f"}}")
    array_verify_code = "\n".join(array_verify_code)

    call_args = [f"D{i}" for i in range(len(D))] + [f"{k}" for k in ref_data.keys()]
    call_args = ", ".join(call_args)

    with open("snitch_main.c", "w") as f:
        f.write(
            gen.dedent(
                f"""
            #include <snrt.h>
            #include <stdlib.h>
            #define f64 double
            #define i32 int32_t
            {src_init_code}
            {dst_init_code}

            void {program.name}({signature});
            int main() {{
                unsigned tid = snrt_cluster_core_idx();
                if (tid != 0) {{
                    return 0;
                }}
                {shape_init_code}
                size_t buf_size = {buf_size};
                void* _tmp = snrt_l3alloc(buf_size);
                {array_init_code}
                unsigned long t1 = read_csr(mcycle);
                {program.name}({call_args}, _tmp);
                unsigned long t2 = read_csr(mcycle);
                printf("Cycles: %lu\\n", t2 - t1);
                i32 ok = 1;
                {array_verify_code}
                if (ok) {{
                    printf("success, exitting...\\n");
                    return 0;
                }} else {{
                    printf("FAILURE, exitting...\\n");
                    return 1;
                }}
            }}
        """
            )
        )
    with open("snitch_code.c", "w") as f:
        f.write("#include <snrt.h>\n")
        f.write(code)
    with open("Makefile", "w") as f:
        f.write(
            dedent(
                """
            APP     = snitch_bin
            SRCS    = snitch_main.c snitch_code.c
            SELECT_RUNTIME = banshee
            include /snitch/hw/system/snitch_cluster/sw/apps/common.mk
        """
            )
        )

    snitch_root = "../../snitch1"

    cmd = f"podman run --security-opt label=disable --rm --cap-add=SYS_PTRACE -it -v {snitch_root}:/snitch -v .:/work -w /work ghcr.io/pulp-platform/snitch@sha256:f43d2db7c97bdcd653deb567664032940e8d9051a80a52c22f239e19fe4c310b".split()

    result = subprocess.run(
        cmd + "make -C /snitch/hw/system/snitch_cluster/ sw".split(),
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        universal_newlines=True
    )  # initializes some requierd headers
    if result.returncode != 0:
        print('stdout')
        print(result.stdout)
        print('stderr')
        print(result.stderr)
        raise Exception("Snitch init failed")
    result = subprocess.run(
        cmd + "make".split(),
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        universal_newlines=True
    )
    if result.returncode != 0:
        print('stdout')
        print(result.stdout)
        print('stderr')
        print(result.stderr)
        raise Exception("Compilation failed")

    run_cmd = "RUST_MIN_STACK=134217728 SNITCH_LOG= banshee --configuration /snitch/sw/banshee/config/snitch_cluster.yaml --latency build/snitch_bin.elf"

    result = subprocess.run(
        cmd + "/bin/bash -c".split() + [run_cmd],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        universal_newlines=True
    )

    if result.returncode != 0:
        print('stdout')
        print(result.stdout)
        print('stderr')
        print(result.stderr)
        raise Exception("Execution failed")

    # find patter with regex: "Cycles: 1476"
    m = re.search(r"Cycles: (\d+)", result.stdout)
    if m is None:
        raise Exception("Could not find cycle count")
    cycles = int(m.group(1))
    return cycles


def test_abs():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["B", "N"],
                ops=Operation("dst[{1}, {0}]", ["src[{1}, {0}]"], "abs"),
            ),
        ],
        name="func_abs",
        input_declarations={
            "B": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "src": Buffer(dims=["B", "N"], dtype="f64"),
            "dst": Buffer(dims=["B", "N"], dtype="f64"),
        },
        reusable_inputs={"src"},
        used_outputs={"dst"},
    )

    X = np.random.randn(10, 10).astype(np.float32)
    Y = onnx_elemwise_op('Abs', X)
    assert np.allclose(Y, np.abs(X))
    ref_data = {"src": X, "dst": Y}

    cycles = test_snitch_code(program, ref_data)
    print("Abs cycles:", cycles)


def test_acos():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["B", "N"],
                ops=Operation("dst[{1}, {0}]", ["src[{1}, {0}]"], "acos"),
            ),
        ],
        name="func_acos",
        input_declarations={
            "B": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "src": Buffer(dims=["B", "N"], dtype="f64"),
            "dst": Buffer(dims=["B", "N"], dtype="f64"),
        },
        reusable_inputs={"src"},
        used_outputs={"dst"},
    )
    program.options["inline_asm"] = False

    X = np.random.uniform(-1, 1, (10, 10)).astype(np.float32)
    Y = onnx_elemwise_op('Acos', X)
    assert np.allclose(Y, np.arccos(X))
    ref_data = {"src": X, "dst": Y}

    cycles = test_snitch_code(program, ref_data)
    print("Acos cycles:", cycles)


def test_acosh():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["B", "N"],
                ops=Operation("dst[{1}, {0}]", ["src[{1}, {0}]"], "acosh"),
            ),
        ],
        name="func_acosh",
        input_declarations={
            "B": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "src": Buffer(dims=["B", "N"], dtype="f64"),
            "dst": Buffer(dims=["B", "N"], dtype="f64"),
        },
        reusable_inputs={"src"},
        used_outputs={"dst"},
    )
    program.options["inline_asm"] = False

    X = np.random.uniform(1, 10, (10, 10)).astype(np.float32)
    Y = onnx_elemwise_op('Acosh', X)
    assert np.allclose(Y, np.arccosh(X))
    ref_data = {"src": X, "dst": Y}

    cycles = test_snitch_code(program, ref_data)
    print("Acosh cycles:", cycles)


def test_add():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["B", "N"],
                ops=Operation("dst[{1}, {0}]", ["src1[{1}, {0}]", "src2[{1}, {0}]"], "add"),
            ),
        ],
        name="func_add",
        input_declarations={
            "B": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "src1": Buffer(dims=["B", "N"], dtype="f64"),
            "src2": Buffer(dims=["B", "N"], dtype="f64"),
            "dst": Buffer(dims=["B", "N"], dtype="f64"),
        },
        reusable_inputs={"src1", "src2"},
        used_outputs={"dst"},
    )

    X = np.random.randn(10, 10).astype(np.float32)
    Y = np.random.randn(10, 10).astype(np.float32)
    Z = onnx_binary_op('Add', X, Y)
    assert np.allclose(Z, X + Y)
    ref_data = {"src1": X, "src2": Y, "dst": Z}

    cycles = test_snitch_code(program, ref_data)
    print("Add cycles:", cycles)


def test_and():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["B", "N"],
                ops=Operation("dst[{1}, {0}]", ["src1[{1}, {0}]", "src2[{1}, {0}]"], "and"),
            ),
        ],
        name="func_add",
        input_declarations={
            "B": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "src1": Buffer(dims=["B", "N"], dtype="i32"),
            "src2": Buffer(dims=["B", "N"], dtype="i32"),
            "dst": Buffer(dims=["B", "N"], dtype="i32"),
        },
        reusable_inputs={"src1", "src2"},
        used_outputs={"dst"},
    )

    X = np.random.randint(0, 2, (10, 10)).astype(np.bool_)
    Y = np.random.randint(0, 2, (10, 10)).astype(np.bool_)
    Z = onnx_binary_op('And', X, Y)
    assert np.allclose(Z, np.logical_and(X, Y))
    ref_data = {"src1": X, "src2": Y, "dst": Z}

    cycles = test_snitch_code(program, ref_data)
    print("And cycles:", cycles)


def test_asin():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["B", "N"],
                ops=Operation("dst[{1}, {0}]", ["src[{1}, {0}]"], "asin"),
            ),
        ],
        name="func_asin",
        input_declarations={
            "B": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "src": Buffer(dims=["B", "N"], dtype="f64"),
            "dst": Buffer(dims=["B", "N"], dtype="f64"),
        },
        reusable_inputs={"src"},
        used_outputs={"dst"},
    )
    program.options["inline_asm"] = False

    X = np.random.uniform(-1, 1, (10, 10)).astype(np.float32)
    Y = onnx_elemwise_op('Asin', X)
    assert np.allclose(Y, np.arcsin(X))
    ref_data = {"src": X, "dst": Y}

    cycles = test_snitch_code(program, ref_data)
    print("Asin cycles:", cycles)


def test_asinh():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["B", "N"],
                ops=Operation("dst[{1}, {0}]", ["src[{1}, {0}]"], "asinh"),
            ),
        ],
        name="func_asinh",
        input_declarations={
            "B": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "src": Buffer(dims=["B", "N"], dtype="f64"),
            "dst": Buffer(dims=["B", "N"], dtype="f64"),
        },
        reusable_inputs={"src"},
        used_outputs={"dst"},
    )
    program.options["inline_asm"] = False

    X = np.random.uniform(-1, 1, (10, 10)).astype(np.float32)
    Y = onnx_elemwise_op('Asinh', X)
    assert np.allclose(Y, np.arcsinh(X))
    ref_data = {"src": X, "dst": Y}

    cycles = test_snitch_code(program, ref_data)
    print("Asinh cycles:", cycles)


def test_atan():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["B", "N"],
                ops=Operation("dst[{1}, {0}]", ["src[{1}, {0}]"], "atan"),
            ),
        ],
        name="func_atan",
        input_declarations={
            "B": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "src": Buffer(dims=["B", "N"], dtype="f64"),
            "dst": Buffer(dims=["B", "N"], dtype="f64"),
        },
        reusable_inputs={"src"},
        used_outputs={"dst"},
    )
    program.options["inline_asm"] = False

    X = np.random.uniform(-1, 1, (10, 10)).astype(np.float32)
    Y = onnx_elemwise_op('Atan', X)
    assert np.allclose(Y, np.arctan(X))
    ref_data = {"src": X, "dst": Y}

    cycles = test_snitch_code(program, ref_data)
    print("Atan cycles:", cycles)


def test_atanh():
    program = RootScope(
        ops=[
            loop_scope(
                dims=["B", "N"],
                ops=Operation("dst[{1}, {0}]", ["src[{1}, {0}]"], "atanh"),
            ),
        ],
        name="func_atanh",
        input_declarations={
            "B": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "src": Buffer(dims=["B", "N"], dtype="f64"),
            "dst": Buffer(dims=["B", "N"], dtype="f64"),
        },
        reusable_inputs={"src"},
        used_outputs={"dst"},
    )
    program.options["inline_asm"] = False

    X = np.random.uniform(-1, 1, (10, 10)).astype(np.float32)
    Y = onnx_elemwise_op('Atanh', X)
    assert np.allclose(Y, np.arctanh(X))
    ref_data = {"src": X, "dst": Y}

    cycles = test_snitch_code(program, ref_data)
    print("Atanh cycles:", cycles)


def onnx_averagepool_op(input, kernel, stride, pad, dilation):
    X = make_tensor_value_info('X', TensorProto.FLOAT, [None, None, None, None])
    Y = make_tensor_value_info('Y', TensorProto.FLOAT, [None, None, None, None])
    node = make_node("AveragePool", ['X'], ['Y'], kernel_shape=[kernel, kernel], strides=[stride, stride], pads=[pad, pad, pad, pad], dilations=[dilation, dilation])
    graph = make_graph([node], f'graph_AveragePool', [X], [Y])
    new_domain = 'custom'
    opset_imports = [make_opsetid("", 19), make_opsetid(new_domain, 0)]
    onnx_model = make_model(graph, opset_imports=opset_imports)
    onnx_model.ir_version = 8
    check_model(onnx_model)
    sess = InferenceSession(
        onnx_model.SerializeToString(),
        providers=["CPUExecutionProvider"])
    [output] = sess.run(None, {'X': input})
    return output


def my_averagepool(input, kernel, stride, pad, dilation):
    N, C, H_in, W_in = input.shape
    H_out = (H_in + 2 * pad - (dilation * (kernel - 1) + 1)) // stride + 1
    W_out = (W_in + 2 * pad - (dilation * (kernel - 1) + 1)) // stride + 1
    output = np.zeros((N, C, H_out, W_out), dtype=input.dtype)
    for n in range(N):
        for c in range(C):
            for ho in range(H_out):
                for wo in range(W_out):
                    # this would be the index of the first accessed element in the input (which may be out of bounds)
                    h_offset = ho * stride - pad
                    # first h in "h * dilation" that when added to offset actually give value inside the bounds
                    h_first = max(0, (- h_offset + dilation - 1) // dilation)
                    # last h in "h * dilation" that when added to offset actually give value inside the bounds
                    h_last = min(kernel - 1, (H_in - 1 - h_offset) // dilation)

                    # this is the actual index of the first accessed element in the input
                    # hstart = h_offset + h_first * dilation
                    # this is the index of the last accessed element in the input
                    # hend = h_offset + h_last * dilation
                    # assert (hend - hstart) % dilation == 0

                    # same for width dimension
                    w_offset = wo * stride - pad
                    w_first = max(0, (- w_offset + dilation - 1) // dilation)
                    w_last = min(kernel - 1, (W_in - 1 - w_offset) // dilation)

                    # wstart = wo * stride - pad + w_first * dilation
                    # wend = w_offset + w_last * dilation
                    # assert (wend - wstart) % dilation == 0

                    out_acc = 0
                    for hk_idx in range(h_last - h_first + 1):
                        for hw_idx in range(w_last - w_first + 1):
                            out_acc += input[n, c, h_offset + (hk_idx + h_first) * dilation, w_offset + (hw_idx + w_first) * dilation]
                    output[n, c, ho, wo] = out_acc / ((h_last - h_first + 1) * (w_last - w_first + 1))
    return output


def test_averagepool_indexing():
    # this is unrelated to the ONNX operator but it makes sure that I have correct index computations
    Hs = [15, 13, 20]
    Ws = [14, 12, 19]
    Ks = [2, 3, 4, 5]
    Ss = [1, 2, 3]
    Ps = [0, 1, 2, 3]
    Ds = [1, 2, 3]
    for H, W, K, S, P, D in itertools.product(Hs, Ws, Ks, Ss, Ps, Ds):
        if P >= K:
            continue
        N, C = 2, 3
        X = np.random.uniform(-1, 1, (N, C, H, W)).astype(np.float32)
        Y = onnx_averagepool_op(X, kernel=K, stride=S, pad=P, dilation=D)
        ref_Y = my_averagepool(X, kernel=K, stride=S, pad=P, dilation=D)
        assert np.allclose(Y, ref_Y, atol=1e-4, rtol=1e-4)


def test_averagepool():
    program = RootScope(
        ops=[
            Operation("kernel_1", ["kernel", "one"], "isum"),
            Operation("dil_ker_1", ["dilation", "kernel_1"], "imul"),
            Operation("dil_ker_1_1", ["dil_ker_1", "one"], "iadd"),
            Operation("pad2", ["pad", "pad"], "iadd"),
            Operation("dilation_1", ["dilation", "one"], "isub"),
            Operation("dil_ker", ["dil", "ker"], "imul"),
            Operation("kernel_1", ["kernel", "one"], "isub"),
            Operation("h_in_1", ["H_in", "one"], "isub"),
            Operation("w_in_1", ["W_in", "one"], "isub"),
            loop_scope(
                dims=["N", "C", "H_out", "W_out"],
                ops=[
                    # h_offset = ho * stride - pad
                    Operation("h_out_stride[{3}, {2}, {1}, {0}]", ["{1}", "stride"], "imul"),  
                    Operation("h_offset[{3}, {2}, {1}, {0}]", ["h_out_stride[{3}, {2}, {1}, {0}]", "pad"], "isub"),
                    # h_first = max(0, (- h_offset + dilation - 1) // dilation)
                    Operation("dil_h_off[{3}, {2}, {1}, {0}]", ["dilation", "h_offset[{3}, {2}, {1}, {0}]"], "isub"),
                    Operation("dil_h_off_1[{3}, {2}, {1}, {0}]", ["dil_h_off[{3}, {2}, {1}, {0}]", "one"], "isub"),
                    Operation("h_first_boundary[{3}, {2}, {1}, {0}]", ["dil_h_off_1[{3}, {2}, {1}, {0}]", "dilation"], "idiv"),
                    Operation("h_first[{3}, {2}, {1}, {0}]", ["h_first_boundary[{3}, {2}, {1}, {0}]", "zero"], "imax"),
                    # h_last = min(kernel - 1, (H_in - 1 - h_offset) // dilation)
                    Operation("h_in_1_off[{3}, {2}, {1}, {0}]", ["h_in_1", "h_offset[{3}, {2}, {1}, {0}]"], "isub"),
                    Operation("h_in_1_off_dil[{3}, {2}, {1}, {0}]", ["h_in_1_off[{3}, {2}, {1}, {0}]", "dilation"], "idiv"),
                    Operation("h_last[{3}, {2}, {1}, {0}]", ["kernel_1", "h_in_1_off_dil[{3}, {2}, {1}, {0}]"], "imin"),
                    # w_offset = wo * stride - pad
                    Operation("w_out_stride[{3}, {2}, {1}, {0}]", ["{0}", "stride"], "imul"),
                    Operation("w_offset[{3}, {2}, {1}, {0}]", ["w_out_stride[{3}, {2}, {1}, {0}]", "pad"], "isub"),
                    # w_first = max(0, (- w_offset + dilation - 1) // dilation)
                    Operation("dil_w_off[{3}, {2}, {1}, {0}]", ["dilation", "w_offset[{3}, {2}, {1}, {0}]"], "isub"),
                    Operation("dil_w_off_1[{3}, {2}, {1}, {0}]", ["dil_w_off[{3}, {2}, {1}, {0}]", "one"], "isub"),
                    Operation("w_first_boundary[{3}, {2}, {1}, {0}]", ["dil_w_off_1[{3}, {2}, {1}, {0}]", "dilation"], "idiv"),
                    Operation("w_first[{3}, {2}, {1}, {0}]", ["w_first_boundary[{3}, {2}, {1}, {0}]", "zero"], "imax"),
                    # w_last = min(kernel - 1, (W_in - 1 - w_offset) // dilation)
                    Operation("w_in_1_off[{3}, {2}, {1}, {0}]", ["w_in_1", "w_offset[{3}, {2}, {1}, {0}]"], "isub"),
                    Operation("w_in_1_off_dil[{3}, {2}, {1}, {0}]", ["w_in_1_off[{3}, {2}, {1}, {0}]", "dilation"], "idiv"),
                    Operation("w_last[{3}, {2}, {1}, {0}]", ["kernel_1", "w_in_1_off_dil[{3}, {2}, {1}, {0}]"], "imin"),
                    # h_k_iters = h_last - h_first + 1
                    Operation("h_k_diff[{3}, {2}, {1}, {0}]", ["h_last[{3}, {2}, {1}, {0}]", "h_first[{3}, {2}, {1}, {0}]"], "isub"),
                    Operation("h_k_iters[{3}, {2}, {1}, {0}]", ["h_k_diff[{3}, {2}, {1}, {0}]", "one"], "iadd"),
                    # w_k_iters = w_last - w_first + 1
                    Operation("w_k_diff[{3}, {2}, {1}, {0}]", ["w_last[{3}, {2}, {1}, {0}]", "w_first[{3}, {2}, {1}, {0}]"], "isub"),
                    Operation("w_k_iters[{3}, {2}, {1}, {0}]", ["w_k_diff[{3}, {2}, {1}, {0}]", "one[{3}, {2}, {1}, {0}]"], "iadd"),
                    loop_scope(dims=["h_k_iters[{3}, {2}, {1}, {0}]", "w_k_iters[{3}, {2}, {1}, {0}]"], ops=[
                        # hi = h_offset + h * dilation
                        Operation("h_dil[{5}, {4}, {3}, {2}, {1}, {0}]", ["{1}", "dilation"], "imul"),
                        Operation("hi[{5}, {4}, {3}, {2}, {1}, {0}]", ["h_offset[{5}, {4}, {3}, {2}]", "h_dil[{5}, {4}, {3}, {2}, {1}, {0}]"], "iadd"),
                        # wi = w_offset + w * dilation
                        Operation("w_dil[{5}, {4}, {3}, {2}, {1}, {0}]", ["{0}", "dilation"], "imul"),
                        Operation("wi[{5}, {4}, {3}, {2}, {1}, {0}]", ["w_offset[{5}, {4}, {3}, {2}]", "w_dil[{5}, {4}, {3}, {2}, {1}, {0}]"], "iadd"),
                        # acc += input[n, c, h_i, w_i]
                        Operation("acc[{5}, {4}, {3}, {2}]", ["input[hi[{5}, {4}, {3}, {2}, {1}, {0}], wi[{5}, {4}, {3}, {2}, {1}, {0}]]"], "reduce_add"),
                    ]),
                    # scale = h_k_iters * w_k_iters
                    Operation("scale[{3}, {2}, {1}, {0}]", ["h_k_iters[{3}, {2}, {1}, {0}]", "w_k_iters[{3}, {2}, {1}, {0}]"], "imul"),
                    # output[n, c, ho, wo] /= scale
                    Operation("output[{3}, {2}, {1}, {0}]", ["acc[{3}, {2}, {1}, {0}]", "scale[{3}, {2}, {1}, {0}]"], "div"),
                ]
            ),
        ],
        name="func_averagepool",
        input_declarations={
            "kernel": Buffer(dims=[], dtype="i32"),
            "stride": Buffer(dims=[], dtype="i32"),
            "pad": Buffer(dims=[], dtype="i32"),
            "dilation": Buffer(dims=[], dtype="i32"),
            "N": Buffer(dims=[], dtype="i32"),
            "C": Buffer(dims=[], dtype="i32"),
            "H_in": Buffer(dims=[], dtype="i32"),
            "W_in": Buffer(dims=[], dtype="i32"),
            "H_out": Buffer(dims=[], dtype="i32"),
            "W_out": Buffer(dims=[], dtype="i32"),
            "input": Buffer(dims=["N", "C", "H_in", "W_in"], dtype="f64"),
            "output": Buffer(dims=["N", "C", "H_out", "W_out"], dtype="f64"),
            "one": Const(value=1, dtype="f64"),
        },
        reusable_inputs=set(),
        used_outputs={"output"},
    )
    N, C, H, W = 2, 3, 20, 21
    K, S, P, D = 4, 2, 3, 2
    X = np.random.uniform(-1, 1, (N, C, H, W)).astype(np.float32)
    Y = onnx_averagepool_op(X, kernel=K, stride=S, pad=P, dilation=D)
    H_out = (H + 2 * P - (D * (K - 1) + 1)) // S + 1
    W_out = (W + 2 * P - (D * (K - 1) + 1)) // S + 1
    ref_Y = my_averagepool(input=X, kernel=K, stride=S, pad=P, dilation=D)
    assert np.allclose(Y, ref_Y, atol=1e-3, rtol=1e-3)
    import pdb; pdb.set_trace()
    ref_data = {
        "kernel": K,
        "stride": S,
        "pad": P,
        "dilation": D,
        "N": N,
        "C": C,
        "H_in": H,
        "W_in": W,
        "H_out": H_out,
        "W_out": W_out,
        "input": X,
        "output": ref_Y,
    }

    cycles = test_snitch_code(program, ref_data)
    print("AveragePool cycles:", cycles)


def main():
    X = np.random.randn(10, 10).astype(np.float32)
    Y = onnx_elemwise_op('Abs', X)
    assert np.allclose(Y, np.abs(X))

    X = np.random.uniform(-1, 1, (10, 10)).astype(np.float32)
    Y = onnx_elemwise_op('Acos', X)
    assert np.allclose(Y, np.arccos(X))

    X = np.random.uniform(1, 10, (10, 10)).astype(np.float32)
    Y = onnx_elemwise_op('Acosh', X)
    assert np.allclose(Y, np.arccosh(X))


if __name__ == "__main__":
    test_abs()
    test_acos()
    test_acosh()
    test_add()
    test_and()
    test_asin()
    test_asinh()
    test_atan()
    test_atanh()
    # test_averagepool()
    #main()
