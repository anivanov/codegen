import jax.experimental
from kernels import *

import jax

def make_heuristic_opt(sizes):
    def heuristic_opt(program):
        specialize_inputs(program, sizes)
        apply_to_exhaustion(program, [
            (join_scopes, find_joinable_scopes),
            (reuse_arr_dims, find_reusable_arr_dims),
            (reuse_buffers, find_reusable_buffers),
            (move_buf_to_stack, find_bufs_movable_to_stack),
            (enable_ssr, find_applicable_ssr),
            (increase_ssr_depth_by_idx, find_increasable_ssr_depth_by_idx),
            (merge_ssr_by_idx, find_mergeable_ssr_by_idx),
            (activate_ssr_by_id, find_activatable_ssr_by_idx),
            (enable_frep, find_applicable_frep),
        ])
        print(program)
    def better_heuristic_opt(program):
        specialize_inputs(program, sizes)
        flags = [0]*len(program.ops)
        for loop, f in zip(program.ops, range(len(flags))):
            if isinstance(loop, LoopScope):
                try:
                    tile_scope(program, loop.name, 4)
                    for d in range(int(loop.name[-1]) - 1, 0, -1):
                        scope_name = loop.name[:-1] + str(d)
                        swap_nested_scopes(program, scope_name)
                except:
                    flags[f] = 1

        apply_to_exhaustion(program, [
            (join_scopes, find_joinable_scopes),
            (reuse_arr_dims, find_reusable_arr_dims),
            (reuse_buffers, find_reusable_buffers),
            (move_buf_to_stack, find_bufs_movable_to_stack),
            (enable_ssr, find_applicable_ssr),
            (increase_ssr_depth_by_idx, find_increasable_ssr_depth_by_idx),
            (merge_ssr_by_idx, find_mergeable_ssr_by_idx),
            (activate_ssr_by_id, find_activatable_ssr_by_idx),
        ])
        print(program)
        
        for loop, f in zip(program.ops, flags):
            if isinstance(loop, LoopScope) and flags != 1:
                last_scope = loop
                for d in range(int(loop.name[-1]), 0, -1):
                    last_scope = last_scope.ops[0]
                for op in last_scope.ops[1:]:
                    #print(op)
                    split_scopes(program, op.out_elem.name)
                    unroll_scope(program, op.out_elem.name  + "#0")
                unroll_scope(program, last_scope.ops[0].out_elem.name  + "#0")

        apply_to_exhaustion(program, [(enable_frep, find_applicable_frep),
        ])
        print(program)
    return heuristic_opt


def rtl_tester(program, ref_data):
    #return test_snitch_code(program, ref_data, simulator="banshee", check_correctness=True)
    return test_snitch_code(program, ref_data, simulator="rtl", check_correctness=False)


def find_large_tilable_scopes(program):
    result = find_tileable_scopes(program)
    # filter out results with small sizes
    return [(scope_name, tile_size) for scope_name, tile_size in result if tile_size >= 10]


CPU_EXHAUSTIVE_HEURISTIC = {
    "tile_scope": (tile_scope, find_large_tilable_scopes),
    # "untile_scope": (untile_scope, find_untileable_scopes),
    "tile_buffer": (tile_buffer, find_tileable_buffers),
    # "untile_buffer": (untile_buffer, find_untileable_buffers),
    # "create_dimension": (create_dimension, find_creatable_dimensions),
    # "delete_dimension": (delete_dimension, find_deletable_dimensions),
    # "swap_nested_scopes": (swap_nested_scopes, find_swappable_nested_scopes),
    # "create_temporary": (create_temporary, find_creatable_temporaries),
    # "delete_temporary": (delete_temporary, find_deletable_temporaries),
    # "swap_ops": (swap_ops, find_swappable_ops),
    # "swap_buffer_dims": (swap_buffer_dims, find_swappable_buffer_dims),
    "join_scopes": (join_scopes, find_joinable_scopes),
    # "split_scopes": (split_scopes, find_splittable_scopes),
    "reuse_arr_dims": (reuse_arr_dims, find_reusable_arr_dims),
    # "unroll_scope": (unroll_scope, find_unrollable_scopes),
    "reuse_buffers": (reuse_buffers, find_reusable_buffers),
    "move_buf_to_stack": (move_buf_to_stack, find_bufs_movable_to_stack),
    "parallelize_scope": (parallelize_scope, find_parallelizable_scopes),
}


def run_sigmoid(M, N):
    sigmoid_text = f"""
        Name: sigmoid
        In: x
        Out: z
        Declarations:
            x f32 [{M}, {N}] heap
            z f32 [{M}, {N}] heap
            a f32 [{M}, {N}] heap
            b f32 [{M}, {N}] heap
            c f32 [{M}, {N}] heap
        Code:
        {M} {N} a[{{0}}, {{1}}] = -x[{{0}}, {{1}}]
        {M} {N} b[{{0}}, {{1}}] = expf(a[{{0}}, {{1}}])
        {M} {N} c[{{0}}, {{1}}] = 1 + b[{{0}}, {{1}}]
        {M} {N} z[{{0}}, {{1}}] = 1 / c[{{0}}, {{1}}]
    """
    
    sigmoid_program = parse_program(sigmoid_text)
    print(sigmoid_program.text())

    inputs = {"x": np.random.uniform(-10, 10, (M, N)).astype(np.float32)}
    
    apply_to_exhaustion(sigmoid_program, CPU_EXHAUSTIVE_HEURISTIC.values(), silent=False)    

    print(sigmoid_program.text())

    nx = inputs["x"]
    tx = torch.from_numpy(nx)
    jx = jax.numpy.asarray(nx)
    tz = torch.nn.functional.sigmoid(tx)
    z = tz.numpy()

    sp_runtime = timeit.repeat(lambda: scipy.special.expit(nx), repeat=10, number=1)[3:]
    print(f"Scipy sigmoid time [ms]: avg {np.mean(sp_runtime) * 1e3:.6f} std {np.std(sp_runtime) * 1e3:.6f}")
    
    np_runtime = timeit.repeat(lambda: 1 / (1 + np.exp(-nx)), repeat=10, number=1)[3:]
    print(f"Numpy sigmoid time [ms]: avg {np.mean(np_runtime) * 1e3:.6f} std {np.std(np_runtime) * 1e3:.6f}")
    
    jax_sigmoid = lambda x: 1 / (1 + jax.numpy.exp(x))
    jax_runtime = timeit.repeat(lambda: jax_sigmoid(jx), repeat=10, number=1)[3:]
    print(f"JAX sigmoid time [ms]: avg {np.mean(jax_runtime) * 1e3:.6f} std {np.std(jax_runtime) * 1e3:.6f}")
    
    jax_jit_sigmoid = jax.jit(jax_sigmoid)
    jax_jit_runtime = timeit.repeat(lambda: jax_jit_sigmoid(jx), repeat=10, number=1)[3:]
    print(f"JAX JIT sigmoid time [ms]: avg {np.mean(jax_jit_runtime) * 1e3:.6f} std {np.std(jax_jit_runtime) * 1e3:.6f}")
    
    jax_builtin_runtime = timeit.repeat(lambda: jax.nn.sigmoid(jx), repeat=10, number=1)[3:]
    print(f"JAX builtin sigmoid time [ms]: avg {np.mean(jax_builtin_runtime) * 1e3:.6f} std {np.std(jax_builtin_runtime) * 1e3:.6f}")
    
    pt_runtime = timeit.repeat(lambda: 1 / (1 + torch.exp(-tx)), repeat=10, number=1)[3:]
    print(f"Pytorch sigmoid time [ms]: avg {np.mean(pt_runtime) * 1e3:.6f} std {np.std(pt_runtime) * 1e3:.6f}")
    
    pt_jit_sigmoid = torch.jit.trace(lambda x: 1 / (1 + torch.exp(-x)), tx)
    pt_jit_runtime = timeit.repeat(lambda: pt_jit_sigmoid(tx), repeat=10, number=1)[3:]
    print(f"Pytorch JIT sigmoid time [ms]: avg {np.mean(pt_jit_runtime) * 1e3:.6f} std {np.std(pt_jit_runtime) * 1e3:.6f}")
    
    pt_builtin_runtime = timeit.repeat(lambda: torch.nn.functional.sigmoid(tx), repeat=10, number=1)[3:]
    print(f"Pytorch builtin sigmoid time [ms]: avg {np.mean(pt_builtin_runtime) * 1e3:.6f} std {np.std(pt_builtin_runtime) * 1e3:.6f}")
    
    outputs = ref_sigmoid(inputs)
    
    time = test_native_code(
        sigmoid_program,
        {**inputs, **outputs},
        min_reps=10,
        remove_tmp_files=False,
        silent=False,
    )[3:]
    
    print(f"My time [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")


def run_leakyrelu(M, N):
    program_text = f"""
        Name: leakyrelu
        In: x, alpha
        Out: z
        Declarations:
            x f32 [{M}, {N}] heap
            z f32 [{M}, {N}] heap
            a f32 [{M}, {N}] heap
            b f32 [{M}, {N}] heap
            alpha f32 [] heap
        Code:
        {M} {N} a[{{0}}, {{1}}] = fmaxf(x[{{0}}, {{1}}], 0)
        {M} {N} b[{{0}}, {{1}}] = fminf(x[{{0}}, {{1}}], 0)
        {M} {N} z[{{0}}, {{1}}] = alpha * b[{{0}}, {{1}}] + a[{{0}}, {{1}}]
    """
    
    program = parse_program(program_text)
    print(program.text())
    
    apply_to_exhaustion(program, CPU_EXHAUSTIVE_HEURISTIC.values(), silent=False)    

    print(program.text())

    alpha = np.array(0.1, dtype=np.float32)
    talpha = torch.from_numpy(alpha)
    nx = np.random.uniform(-10, 10, (M, N)).astype(np.float32)
    tx = torch.from_numpy(nx)
    jx = jax.numpy.asarray(nx)
    tz = torch.nn.functional.leaky_relu(tx, float(alpha))
    nz = tz.numpy()

    np_runtime = timeit.repeat(lambda: np.maximum(nx, 0) + alpha * np.minimum(nx, 0), repeat=10, number=1)[3:]
    print(f"Numpy leakyrelu time [ms]: avg {np.mean(np_runtime) * 1e3:.6f} std {np.std(np_runtime) * 1e3:.6f}")
    
    jax_func = lambda x, alpha: jax.numpy.maximum(x, 0) + alpha * jax.numpy.minimum(x, 0)
    jax_runtime = timeit.repeat(lambda: jax_func(jx, alpha), repeat=10, number=1)[3:]
    print(f"JAX leakyrelu time [ms]: avg {np.mean(jax_runtime) * 1e3:.6f} std {np.std(jax_runtime) * 1e3:.6f}")
    
    jax_jit = jax.jit(jax_func)
    jax_jit_runtime = timeit.repeat(lambda: jax_jit(jx, alpha), repeat=10, number=1)[3:]
    print(f"JAX JIT leakyrelu time [ms]: avg {np.mean(jax_jit_runtime) * 1e3:.6f} std {np.std(jax_jit_runtime) * 1e3:.6f}")
    
    jax_builtin_runtime = timeit.repeat(lambda: jax.nn.leaky_relu(jx, alpha), repeat=10, number=1)[3:]
    print(f"JAX builtin leakyrelu time [ms]: avg {np.mean(jax_builtin_runtime) * 1e3:.6f} std {np.std(jax_builtin_runtime) * 1e3:.6f}")
    
    pt_runtime = timeit.repeat(lambda: torch.where(tx > 0, tx, talpha * tx), repeat=10, number=1)[3:]
    print(f"Pytorch leakyrelu time [ms]: avg {np.mean(pt_runtime) * 1e3:.6f} std {np.std(pt_runtime) * 1e3:.6f}")
    
    pt_jit_func = torch.jit.trace(lambda x, alpha: torch.where(x > 0, x, alpha * x), (tx, talpha))
    pt_jit_runtime = timeit.repeat(lambda: pt_jit_func(tx, talpha), repeat=10, number=1)[3:]
    print(f"Pytorch JIT leakyrelu time [ms]: avg {np.mean(pt_jit_runtime) * 1e3:.6f} std {np.std(pt_jit_runtime) * 1e3:.6f}")
    
    pt_builtin_runtime = timeit.repeat(lambda: torch.nn.functional.leaky_relu(tx, float(alpha)), repeat=10, number=1)[3:]
    print(f"Pytorch builtin leakyrelu time [ms]: avg {np.mean(pt_builtin_runtime) * 1e3:.6f} std {np.std(pt_builtin_runtime) * 1e3:.6f}")
        
    time = test_native_code(
        program,
        {'x': nx, 'alpha': alpha, 'z': nz},
        min_reps=10,
        remove_tmp_files=False,
        silent=False,
    )[3:]
    
    print(f"My leakyrelu [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")


def run_celu(M, N):
    # max(0,x) + min(0,alpha*(exp(x/alpha)-1))
    program_text = f"""
        Name: celu
        In: x, alpha
        Out: z
        Declarations:
            alpha f32 [] heap
            x f32 [{M}, {N}] heap
            z f32 [{M}, {N}] heap
            px f32 [{M}, {N}] heap
            xa f32 [{M}, {N}] heap
            e f32 [{M}, {N}] heap
            y f32 [{M}, {N}] heap
            nx f32 [{M}, {N}] heap
        Code:
        {M} {N} px[{{0}}, {{1}}] = fmaxf(x[{{0}}, {{1}}], 0)
        {M} {N} xa[{{0}}, {{1}}] = x[{{0}}, {{1}}] / alpha
        {M} {N} e[{{0}}, {{1}}] = expf(xa[{{0}}, {{1}}])
        {M} {N} y[{{0}}, {{1}}] = alpha * e[{{0}}, {{1}}] - alpha
        {M} {N} nx[{{0}}, {{1}}] = fminf(y[{{0}}, {{1}}], 0)
        {M} {N} z[{{0}}, {{1}}] = px[{{0}}, {{1}}] + nx[{{0}}, {{1}}]
    """
    
    program = parse_program(program_text)
    print(program.text())
    
    apply_to_exhaustion(program, CPU_EXHAUSTIVE_HEURISTIC.values(), silent=False)    

    print(program.text())
    
    alpha = np.array(0.1, dtype=np.float32)
    talpha = torch.from_numpy(alpha)
    nx = np.random.uniform(-1.1, 1.1, (M, N)).astype(np.float32)
    tx = torch.from_numpy(nx)
    jx = jax.numpy.asarray(nx)
    tz = torch.nn.functional.celu(tx, float(alpha))
    nz = tz.numpy()
    
    np_runtime = timeit.repeat(lambda: np.maximum(nx, 0) + np.minimum(0, alpha * (np.exp(nx / alpha) - 1)), repeat=10, number=1)[3:]
    print(f"Numpy celu time [ms]: avg {np.mean(np_runtime) * 1e3:.6f} std {np.std(np_runtime) * 1e3:.6f}")
    
    jax_func = lambda x, alpha: jax.numpy.maximum(x, 0) + jax.numpy.minimum(0, alpha * (jax.numpy.exp(x / alpha) - 1))
    jax_runtime = timeit.repeat(lambda: jax_func(jx, alpha), repeat=10, number=1)[3:]
    print(f"JAX celu time [ms]: avg {np.mean(jax_runtime) * 1e3:.6f} std {np.std(jax_runtime) * 1e3:.6f}")
    
    jax_jit = jax.jit(jax_func)
    jax_jit_runtime = timeit.repeat(lambda: jax_jit(jx, alpha), repeat=10, number=1)[3:]
    print(f"JAX JIT celu time [ms]: avg {np.mean(jax_jit_runtime) * 1e3:.6f} std {np.std(jax_jit_runtime) * 1e3:.6f}")
    
    jax_builtin_runtime = timeit.repeat(lambda: jax.nn.celu(jx, alpha), repeat=10, number=1)[3:]
    print(f"JAX builtin celu time [ms]: avg {np.mean(jax_builtin_runtime) * 1e3:.6f} std {np.std(jax_builtin_runtime) * 1e3:.6f}")
    
    pt_runtime = timeit.repeat(lambda: torch.where(tx > 0, tx, talpha * (torch.exp(tx / talpha) - 1)), repeat=10, number=1)[3:]
    print(f"Pytorch celu time [ms]: avg {np.mean(pt_runtime) * 1e3:.6f} std {np.std(pt_runtime) * 1e3:.6f}")
    
    pt_jit_func = torch.jit.trace(lambda x, alpha: torch.where(x > 0, x, alpha * (torch.exp(x / alpha) - 1)), (tx, talpha))
    pt_jit_runtime = timeit.repeat(lambda: pt_jit_func(tx, talpha), repeat=10, number=1)[3:]
    print(f"Pytorch JIT celu time [ms]: avg {np.mean(pt_jit_runtime) * 1e3:.6f} std {np.std(pt_jit_runtime) * 1e3:.6f}")
    
    pt_builtin_runtime = timeit.repeat(lambda: torch.nn.functional.celu(tx, float(alpha)), repeat=10, number=1)[3:]
    print(f"Pytorch builtin celu time [ms]: avg {np.mean(pt_builtin_runtime) * 1e3:.6f} std {np.std(pt_builtin_runtime) * 1e3:.6f}")
    
    time = test_native_code(
        program,
        {'x': nx, 'alpha': alpha, 'z': nz},
        min_reps=10,
        remove_tmp_files=False,
        silent=False,
    )[3:]
    
    print(f"My celu [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")


def run_relu(M, N):
    program_text = f"""
        Name: relu
        In: x
        Out: z
        Declarations:
            x f32 [{M}, {N}] heap
            z f32 [{M}, {N}] heap
        Code:
        {M} {N} z[{{0}}, {{1}}] = fmaxf(x[{{0}}, {{1}}], 0)
    """
    
    program = parse_program(program_text)
    print(program.text())
    
    apply_to_exhaustion(program, CPU_EXHAUSTIVE_HEURISTIC.values(), silent=False)    

    print(program.text())
    
    nx = np.random.uniform(-10, 10, (M, N)).astype(np.float32)
    tx = torch.from_numpy(nx)
    jx = jax.numpy.asarray(nx)
    tz = torch.nn.functional.relu(tx)
    nz = tz.numpy()
    
    np_runtime = timeit.repeat(lambda: np.maximum(nx, 0), repeat=10, number=1)[3:]
    print(f"Numpy relu time [ms]: avg {np.mean(np_runtime) * 1e3:.6f} std {np.std(np_runtime) * 1e3:.6f}")
    
    jax_func = lambda x: jax.numpy.maximum(x, 0)
    jax_runtime = timeit.repeat(lambda: jax_func(jx), repeat=10, number=1)[3:]
    print(f"JAX relu time [ms]: avg {np.mean(jax_runtime) * 1e3:.6f} std {np.std(jax_runtime) * 1e3:.6f}")
    
    jax_jit = jax.jit(jax_func)
    jax_jit_runtime = timeit.repeat(lambda: jax_jit(jx), repeat=10, number=1)[3:]
    print(f"JAX JIT relu time [ms]: avg {np.mean(jax_jit_runtime) * 1e3:.6f} std {np.std(jax_jit_runtime) * 1e3:.6f}")
    
    jax_builtin_runtime = timeit.repeat(lambda: jax.nn.relu(jx), repeat=10, number=1)[3:]
    print(f"JAX builtin relu time [ms]: avg {np.mean(jax_builtin_runtime) * 1e3:.6f} std {np.std(jax_builtin_runtime) * 1e3:.6f}")
    
    pt_runtime = timeit.repeat(lambda: torch.maximum(tx, torch.tensor(0.0)), repeat=10, number=1)[3:]
    print(f"Pytorch relu time [ms]: avg {np.mean(pt_runtime) * 1e3:.6f} std {np.std(pt_runtime) * 1e3:.6f}")
    
    pt_jit_func = torch.jit.trace(lambda x: torch.maximum(x, torch.tensor(0.0)), tx)
    pt_jit_runtime = timeit.repeat(lambda: pt_jit_func(tx), repeat=10, number=1)[3:]
    print(f"Pytorch JIT relu time [ms]: avg {np.mean(pt_jit_runtime) * 1e3:.6f} std {np.std(pt_jit_runtime) * 1e3:.6f}")
    
    pt_builtin_runtime = timeit.repeat(lambda: torch.nn.functional.relu(tx), repeat=10, number=1)[3:]
    print(f"Pytorch builtin relu time [ms]: avg {np.mean(pt_builtin_runtime) * 1e3:.6f} std {np.std(pt_builtin_runtime) * 1e3:.6f}")
    
    time = test_native_code(
        program,
        {'x': nx, 'z': nz},
        min_reps=10,
        remove_tmp_files=False,
        silent=False,
    )[3:]
    
    print(f"My relu [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")
    
    


def run_pow(M, N):
    program_text = f"""
        Name: func_pow
        In: x1, x2
        Out: z
        Declarations:
            x1 f32 [{M}, {N}] heap
            x2 f32 [{M}, {N}] heap
            z f32 [{M}, {N}] heap
        Code:
        {M} {N} z[{{0}}, {{1}}] = powf(x1[{{0}}, {{1}}], x2[{{0}}, {{1}}])
    """
    
    program = parse_program(program_text)
    print(program.text())
    
    apply_to_exhaustion(program, CPU_EXHAUSTIVE_HEURISTIC.values(), silent=False)
    
    print(program.text())
    
    x1 = np.random.uniform(0.1, 10, (M, N)).astype(np.float32)
    x2 = np.random.uniform(-4, 4, (M, N)).astype(np.float32)
    tx1 = torch.from_numpy(x1)
    tx2 = torch.from_numpy(x2)
    jx1 = jax.numpy.asarray(x1)
    jx2 = jax.numpy.asarray(x2)
    tz = torch.pow(tx1, tx2)
    z = tz.numpy()
    
    np_runtime = timeit.repeat(lambda: np.power(x1, x2), repeat=10, number=1)[3:]
    print(f"Numpy pow time [ms]: avg {np.mean(np_runtime) * 1e3:.6f} std {np.std(np_runtime) * 1e3:.6f}")
    
    jax_runtime = timeit.repeat(lambda: jax.numpy.power(jx1, jx2), repeat=10, number=1)[3:]
    print(f"JAX pow time [ms]: avg {np.mean(jax_runtime) * 1e3:.6f} std {np.std(jax_runtime) * 1e3:.6f}")
    
    pt_runtime = timeit.repeat(lambda: torch.pow(tx1, tx2), repeat=10, number=1)[3:]
    print(f"Pytorch pow time [ms]: avg {np.mean(pt_runtime) * 1e3:.6f} std {np.std(pt_runtime) * 1e3:.6f}")
    
    time = test_native_code(
        program,
        {'x1': x1, 'x2': x2, 'z': z},
        min_reps=10,
        remove_tmp_files=False,
        silent=False,
    )[3:]
    print(f"My time [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")


def run_sqrt(M, N):
    program_text = f"""
        Name: func_sqrt
        In: x
        Out: z
        Declarations:
            x f32 [{M}, {N}] heap
            z f32 [{M}, {N}] heap
        Code:
        {M} {N} z[{{0}}, {{1}}] = sqrtf(x[{{0}}, {{1}}])
    """
    
    program = parse_program(program_text)
    print(program.text())
    
    apply_to_exhaustion(program, CPU_EXHAUSTIVE_HEURISTIC.values(), silent=False)
    
    print(program.text())
    
    x = np.random.uniform(0.1, 10, (M, N)).astype(np.float32)
    tx = torch.from_numpy(x)
    jx = jax.numpy.asarray(x)
    tz = torch.sqrt(tx)
    z = tz.numpy()
    
    np_runtime = timeit.repeat(lambda: np.sqrt(x), repeat=10, number=1)[3:]
    print(f"Numpy sqrt time [ms]: avg {np.mean(np_runtime) * 1e3:.6f} std {np.std(np_runtime) * 1e3:.6f}")
    
    jax_runtime = timeit.repeat(lambda: jax.numpy.sqrt(jx), repeat=10, number=1)[3:]
    print(f"JAX sqrt time [ms]: avg {np.mean(jax_runtime) * 1e3:.6f} std {np.std(jax_runtime) * 1e3:.6f}")
    
    pt_runtime = timeit.repeat(lambda: torch.sqrt(tx), repeat=10, number=1)[3:]
    print(f"Pytorch sqrt time [ms]: avg {np.mean(pt_runtime) * 1e3:.6f} std {np.std(pt_runtime) * 1e3:.6f}")
    
    time = test_native_code(
        program,
        {'x': x, 'z': z},
        min_reps=10,
        remove_tmp_files=False,
        silent=False,
    )[3:]
    print(f"My time [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")


def run_tanh(M, N):
    program_text = f"""
        Name: func_tanh
        In: x
        Out: z
        Declarations:
            x f32 [{M}, {N}] heap
            z f32 [{M}, {N}] heap
        Code:
        {M} {N} z[{{0}}, {{1}}] = tanhf(x[{{0}}, {{1}}])
    """
    
    program = parse_program(program_text)
    print(program.text())
    
    apply_to_exhaustion(program, CPU_EXHAUSTIVE_HEURISTIC.values(), silent=False)
    
    print(program.text())
    
    x = np.random.uniform(-10, 10, (M, N)).astype(np.float32)
    tx = torch.from_numpy(x)
    jx = jax.numpy.asarray(x)
    tz = torch.nn.functional.tanh(tx)
    z = tz.numpy()
    
    np_runtime = timeit.repeat(lambda: np.tanh(x), repeat=10, number=1)[3:]
    print(f"Numpy tanh time [ms]: avg {np.mean(np_runtime) * 1e3:.6f} std {np.std(np_runtime) * 1e3:.6f}")
    
    jax_runtime = timeit.repeat(lambda: jax.numpy.tanh(jx), repeat=10, number=1)[3:]
    print(f"JAX tanh time [ms]: avg {np.mean(jax_runtime) * 1e3:.6f} std {np.std(jax_runtime) * 1e3:.6f}")
    
    pt_runtime = timeit.repeat(lambda: torch.tanh(tx), repeat=10, number=1)[3:]
    print(f"Pytorch tanh time [ms]: avg {np.mean(pt_runtime) * 1e3:.6f} std {np.std(pt_runtime) * 1e3:.6f}")
    
    time = test_native_code(
        program,
        {'x': x, 'z': z},
        min_reps=10,
        remove_tmp_files=False,
        silent=False,
    )[3:]
    print(f"My time [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")


def run_log(M, N):
    program_text = f"""
        Name: func_log
        In: x
        Out: z
        Declarations:
            x f32 [{M}, {N}] heap
            z f32 [{M}, {N}] heap
        Code:
        {M} {N} z[{{0}}, {{1}}] = logf(x[{{0}}, {{1}}])
    """
    
    program = parse_program(program_text)
    print(program.text())
    
    apply_to_exhaustion(program, CPU_EXHAUSTIVE_HEURISTIC.values(), silent=False)
    
    print(program.text())
    
    x = np.random.uniform(0.1, 10, (M, N)).astype(np.float32)
    tx = torch.from_numpy(x)
    jx = jax.numpy.asarray(x)
    tz = torch.log(tx)
    z = tz.numpy()
    
    np_runtime = timeit.repeat(lambda: np.log(x), repeat=10, number=1)[3:]
    print(f"Numpy log time [ms]: avg {np.mean(np_runtime) * 1e3:.6f} std {np.std(np_runtime) * 1e3:.6f}")
    
    jax_runtime = timeit.repeat(lambda: jax.numpy.log(jx), repeat=10, number=1)[3:]
    print(f"JAX log time [ms]: avg {np.mean(jax_runtime) * 1e3:.6f} std {np.std(jax_runtime) * 1e3:.6f}")
    
    pt_runtime = timeit.repeat(lambda: torch.log(tx), repeat=10, number=1)[3:]
    print(f"Pytorch log time [ms]: avg {np.mean(pt_runtime) * 1e3:.6f} std {np.std(pt_runtime) * 1e3:.6f}")
    
    time = test_native_code(
        program,
        {'x': x, 'z': z},
        min_reps=10,
        remove_tmp_files=False,
        silent=False,
    )[3:]
    print(f"My time [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")


def run_div(M, N):
    program_text = f"""
        Name: func_div
        In: x1, x2
        Out: z
        Declarations:
            x1 f32 [{M}, {N}] heap
            x2 f32 [{M}, {N}] heap
            z f32 [{M}, {N}] heap
        Code:
        {M} {N} z[{{0}}, {{1}}] = x1[{{0}}, {{1}}] / x2[{{0}}, {{1}}]
    """
    
    program = parse_program(program_text)
    print(program.text())
    
    apply_to_exhaustion(program, CPU_EXHAUSTIVE_HEURISTIC.values(), silent=False)
    
    print(program.text())
    
    x1 = np.random.uniform(-10, 10, (M, N)).astype(np.float32)
    x2 = np.random.uniform(0.5, 2.0, (M, N)).astype(np.float32)
    tx1 = torch.from_numpy(x1)
    tx2 = torch.from_numpy(x2)
    jx1 = jax.numpy.asarray(x1)
    jx2 = jax.numpy.asarray(x2)
    tz = tx1 / tx2
    z = tz.numpy()
    
    np_runtime = timeit.repeat(lambda: x1 / x2, repeat=10, number=1)[3:]
    print(f"Numpy div time [ms]: avg {np.mean(np_runtime) * 1e3:.6f} std {np.std(np_runtime) * 1e3:.6f}")
    
    jax_runtime = timeit.repeat(lambda: jax.numpy.divide(jx1, jx2), repeat=10, number=1)[3:]
    print(f"JAX div time [ms]: avg {np.mean(jax_runtime) * 1e3:.6f} std {np.std(jax_runtime) * 1e3:.6f}")
    
    pt_runtime = timeit.repeat(lambda: tx1 / tx2, repeat=10, number=1)[3:]
    print(f"Pytorch div time [ms]: avg {np.mean(pt_runtime) * 1e3:.6f} std {np.std(pt_runtime) * 1e3:.6f}")
    
    time = test_native_code(
        program,
        {'x1': x1, 'x2': x2, 'z': z},
        min_reps=10,
        remove_tmp_files=False,
        silent=False,
    )[3:]
    print(f"My time [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")


def run_mul(M, N):
    program_text = f"""
        Name: func_mul
        In: x1, x2
        Out: z
        Declarations:
            x1 f32 [{M}, {N}] heap
            x2 f32 [{M}, {N}] heap
            z f32 [{M}, {N}] heap
        Code:
        {M} {N} z[{{0}}, {{1}}] = x1[{{0}}, {{1}}] * x2[{{0}}, {{1}}]
    """
    
    program = parse_program(program_text)
    print(program.text())
    
    apply_to_exhaustion(program, CPU_EXHAUSTIVE_HEURISTIC.values(), silent=False)
    
    print(program.text())
    
    x1 = np.random.uniform(-10, 10, (M, N)).astype(np.float32)
    x2 = np.random.uniform(-10, 10, (M, N)).astype(np.float32)
    tx1 = torch.from_numpy(x1)
    tx2 = torch.from_numpy(x2)
    jx1 = jax.numpy.asarray(x1)
    jx2 = jax.numpy.asarray(x2)
    tz = tx1 * tx2
    z = tz.numpy()
    
    np_runtime = timeit.repeat(lambda: x1 * x2, repeat=10, number=1)[3:]
    print(f"Numpy mul time [ms]: avg {np.mean(np_runtime) * 1e3:.6f} std {np.std(np_runtime) * 1e3:.6f}")
    
    jax_runtime = timeit.repeat(lambda: jax.numpy.multiply(jx1, jx2), repeat=10, number=1)[3:]
    print(f"JAX mul time [ms]: avg {np.mean(jax_runtime) * 1e3:.6f} std {np.std(jax_runtime) * 1e3:.6f}")
    
    pt_runtime = timeit.repeat(lambda: tx1 * tx2, repeat=10, number=1)[3:]
    print(f"Pytorch mul time [ms]: avg {np.mean(pt_runtime) * 1e3:.6f} std {np.std(pt_runtime) * 1e3:.6f}")
    
    time = test_native_code(
        program,
        {'x1': x1, 'x2': x2, 'z': z},
        min_reps=10,
        remove_tmp_files=False,
        silent=False,
    )[3:]
    print(f"My time [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")


def run_sub(M, N):
    program_text = f"""
        Name: func_sub
        In: x1, x2
        Out: z
        Declarations:
            x1 f32 [{M}, {N}] heap
            x2 f32 [{M}, {N}] heap
            z f32 [{M}, {N}] heap
        Code:
        {M} {N} z[{{0}}, {{1}}] = x1[{{0}}, {{1}}] - x2[{{0}}, {{1}}]
    """
    
    program = parse_program(program_text)
    print(program.text())
    
    apply_to_exhaustion(program, CPU_EXHAUSTIVE_HEURISTIC.values(), silent=False)
    
    print(program.text())

    x1 = np.random.uniform(-10, 10, (M, N)).astype(np.float32)
    x2 = np.random.uniform(-10, 10, (M, N)).astype(np.float32)
    tx1 = torch.from_numpy(x1)
    tx2 = torch.from_numpy(x2)
    jx1 = jax.numpy.asarray(x1)
    jx2 = jax.numpy.asarray(x2)
    tz = tx1 - tx2
    z = tz.numpy()
    
    np_runtime = timeit.repeat(lambda: x1 - x2, repeat=10, number=1)[3:]
    print(f"Numpy sub time [ms]: avg {np.mean(np_runtime) * 1e3:.6f} std {np.std(np_runtime) * 1e3:.6f}")
    
    jax_runtime = timeit.repeat(lambda: jax.numpy.subtract(jx1, jx2), repeat=10, number=1)[3:]
    print(f"JAX sub time [ms]: avg {np.mean(jax_runtime) * 1e3:.6f} std {np.std(jax_runtime) * 1e3:.6f}")
    
    pt_runtime = timeit.repeat(lambda: tx1 - tx2, repeat=10, number=1)[3:]
    print(f"Pytorch sub time [ms]: avg {np.mean(pt_runtime) * 1e3:.6f} std {np.std(pt_runtime) * 1e3:.6f}")
    
    time = test_native_code(
        program,
        {'x1': x1, 'x2': x2, 'z': z},
        min_reps=10,
        remove_tmp_files=False,
        silent=False,
    )[3:]
    print(f"My time [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")


def run_sum(M, N):
    program_text = f"""
        Name: func_sum
        In: x1, x2
        Out: z
        Declarations:
            x1 f32 [{M}, {N}] heap
            x2 f32 [{M}, {N}] heap
            z f32 [{M}, {N}] heap
        Code:
        {M} {N} z[{{0}}, {{1}}] = x1[{{0}}, {{1}}] + x2[{{0}}, {{1}}]
    """
    
    program = parse_program(program_text)
    print(program.text())
    
    apply_to_exhaustion(program, CPU_EXHAUSTIVE_HEURISTIC.values(), silent=False)
    
    print(program.text())
    
    x1 = np.random.uniform(-10, 10, (M, N)).astype(np.float32)
    x2 = np.random.uniform(-10, 10, (M, N)).astype(np.float32)
    tx1 = torch.from_numpy(x1)
    tx2 = torch.from_numpy(x2)
    jx1 = jax.numpy.asarray(x1)
    jx2 = jax.numpy.asarray(x2)
    tz = tx1 + tx2
    z = tz.numpy()
    
    np_runtime = timeit.repeat(lambda: x1 + x2, repeat=10, number=1)[3:]
    print(f"Numpy sum time [ms]: avg {np.mean(np_runtime) * 1e3:.6f} std {np.std(np_runtime) * 1e3:.6f}")
    
    jax_runtime = timeit.repeat(lambda: jax.numpy.add(jx1, jx2), repeat=10, number=1)[3:]
    print(f"JAX sum time [ms]: avg {np.mean(jax_runtime) * 1e3:.6f} std {np.std(jax_runtime) * 1e3:.6f}")
    
    pt_runtime = timeit.repeat(lambda: tx1 + tx2, repeat=10, number=1)[3:]
    print(f"Pytorch sum time [ms]: avg {np.mean(pt_runtime) * 1e3:.6f} std {np.std(pt_runtime) * 1e3:.6f}")
    
    time = test_native_code(
        program,
        {'x1': x1, 'x2': x2, 'z': z},
        min_reps=10,
        remove_tmp_files=False,
        silent=False,
    )[3:]
    print(f"My time [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")


def run_cosh(M, N):
    program_text = f"""
        Name: func_cosh
        In: x
        Out: z
        Declarations:
            x f32 [{M}, {N}] heap
            z f32 [{M}, {N}] heap
        Code:
        {M} {N} z[{{0}}, {{1}}] = coshf(x[{{0}}, {{1}}])
    """
    
    program = parse_program(program_text)
    print(program.text())
    
    apply_to_exhaustion(program, CPU_EXHAUSTIVE_HEURISTIC.values(), silent=False)
    
    print(program.text())
    
    x = np.random.uniform(-2, 2, (M, N)).astype(np.float32)
    tx = torch.from_numpy(x)
    jx = jax.numpy.asarray(x)
    tz = torch.cosh(tx)
    z = tz.numpy()
    
    np_runtime = timeit.repeat(lambda: np.cosh(x), repeat=10, number=1)[3:]
    print(f"Numpy cosh time [ms]: avg {np.mean(np_runtime) * 1e3:.6f} std {np.std(np_runtime) * 1e3:.6f}")
    
    jax_runtime = timeit.repeat(lambda: jax.numpy.cosh(jx), repeat=10, number=1)[3:]
    print(f"JAX cosh time [ms]: avg {np.mean(jax_runtime) * 1e3:.6f} std {np.std(jax_runtime) * 1e3:.6f}")
    
    pt_runtime = timeit.repeat(lambda: torch.cosh(tx), repeat=10, number=1)[3:]
    print(f"Pytorch cosh time [ms]: avg {np.mean(pt_runtime) * 1e3:.6f} std {np.std(pt_runtime) * 1e3:.6f}")
    
    time = test_native_code(
        program,
        {'x': x, 'z': z},
        min_reps=10,
        remove_tmp_files=False,
        silent=False,
    )[3:]
    print(f"My time [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")


def run_exp(M, N):
    program_text = f"""
        Name: func_exp
        In: x
        Out: z
        Declarations:
            x f32 [{M}, {N}] heap
            z f32 [{M}, {N}] heap
        Code:
        {M} {N} z[{{0}}, {{1}}] = expf(x[{{0}}, {{1}}])
    """
    
    program = parse_program(program_text)
    print(program.text())
    
    apply_to_exhaustion(program, CPU_EXHAUSTIVE_HEURISTIC.values(), silent=False)
    
    print(program.text())
    
    x = np.random.uniform(-10, 10, (M, N)).astype(np.float32)
    tx = torch.from_numpy(x)
    jx = jax.numpy.asarray(x)
    tz = torch.exp(tx)
    z = tz.numpy()
    
    np_runtime = timeit.repeat(lambda: np.exp(x), repeat=10, number=1)[3:]
    print(f"Numpy exp time [ms]: avg {np.mean(np_runtime) * 1e3:.6f} std {np.std(np_runtime) * 1e3:.6f}")
    
    jax_runtime = timeit.repeat(lambda: jax.numpy.exp(jx), repeat=10, number=1)[3:]
    print(f"JAX exp time [ms]: avg {np.mean(jax_runtime) * 1e3:.6f} std {np.std(jax_runtime) * 1e3:.6f}")
    
    pt_runtime = timeit.repeat(lambda: torch.exp(tx), repeat=10, number=1)[3:]
    print(f"Pytorch exp time [ms]: avg {np.mean(pt_runtime) * 1e3:.6f} std {np.std(pt_runtime) * 1e3:.6f}")
    
    time = test_native_code(
        program,
        {'x': x, 'z': z},
        min_reps=10,
        remove_tmp_files=False,
        silent=False,
    )[3:]
    print(f"My time [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")


def run_reducemean(M, N):
    program_text = f"""
        Name: reducemean
        In: x
        Out: z
        Declarations:
            x f32 [{M}, {N}] heap
            t f32 [{M}] heap
            z f32 [{M}] heap
        Code:
        {M} {N} t[{{0}}] += x[{{0}}, {{1}}]
        {M} z[{{0}}] = t[{{0}}] / {N}
    """
    
    program = parse_program(program_text)
    print(program.text())
    
    apply_to_exhaustion(program, CPU_EXHAUSTIVE_HEURISTIC.values(), silent=False)
    
    print(program.text())
    
    x = np.random.uniform(-10, 10, (M, N)).astype(np.float32)
    tx = torch.from_numpy(x)
    jx = jax.numpy.asarray(x)
    z = np.mean(x, axis=1)
    
    np_runtime = timeit.repeat(lambda: np.mean(x, axis=1), repeat=100, number=1)[30:]
    print(f"Numpy mean time [ms]: avg {np.mean(np_runtime) * 1e3:.6f} std {np.std(np_runtime) * 1e3:.6f}")
    
    pt_runtime = timeit.repeat(lambda: torch.mean(tx, dim=1), repeat=100, number=1)[30:]
    print(f"Pytorch mean time [ms]: avg {np.mean(pt_runtime) * 1e3:.6f} std {np.std(pt_runtime) * 1e3:.6f}")
    
    jax_runtime = timeit.repeat(lambda: jax.numpy.mean(jx, axis=1), repeat=100, number=1)[30:]
    print(f"JAX mean time [ms]: avg {np.mean(jax_runtime) * 1e3:.6f} std {np.std(jax_runtime) * 1e3:.6f}")
    
    time = test_native_code(
        program,
        {'x': x, 'z': z},
        min_reps=100,
        remove_tmp_files=False,
        silent=False,
    )[30:]
    print(f"My time [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")
    

def run_argmin(M, N):
    program_text = f"""
        Name: argmin
        In: x
        Out: z
        Declarations:
            x f32 [{M}, {N}] heap
            z i32 [{M}] heap
            min_vals f32 [{M}] heap
            min_mask f32 [{M}, {N}] heap
            idx i32 [{M}, {N}] heap
            inv_idx i32 [{M}, {N}] heap
            min_idx f32 [{M}] heap
            min_idx_i i32 [{M}] heap
        Code:
        {M} {N} min_vals[{{0}}] fminf= x[{{0}}, {{1}}]
        {M} {N} min_mask[{{0}}, {{1}}] = min_vals[{{0}}] - x[{{0}}, {{1}}]
        {M} {N} idx[{{0}}, {{1}}] = {N} - {{1}}
        {M} {N} inv_idx[{{0}}, {{1}}] = copysignf(idx[{{0}}, {{1}}], min_mask[{{0}}, {{1}}])
        {M} {N} min_idx[{{0}}] fmaxf= inv_idx[{{0}}, {{1}}]
        {M} min_idx_i[{{0}}] = (i32) min_idx[{{0}}]
        {M} z[{{0}}] = {N} - min_idx_i[{{0}}]
    """
    
    program = parse_program(program_text)
    print(program.text())
    
    apply_to_exhaustion(program, CPU_EXHAUSTIVE_HEURISTIC.values(), silent=False)
    
    print(program.text())
    
    x = np.random.uniform(-10, 10, (M, N)).astype(np.float32)
    tx = torch.from_numpy(x)
    jx = jax.numpy.asarray(x)
    z = np.argmin(x, axis=1).astype(np.int32)
    
    np_runtime = timeit.repeat(lambda: np.argmin(x, axis=1), repeat=10, number=1)[3:]
    print(f"Numpy argmin time [ms]: avg {np.mean(np_runtime) * 1e3:.6f} std {np.std(np_runtime) * 1e3:.6f}")
    
    pt_runtime = timeit.repeat(lambda: torch.argmin(tx, dim=1), repeat=10, number=1)[3:]
    print(f"Pytorch argmin time [ms]: avg {np.mean(pt_runtime) * 1e3:.6f} std {np.std(pt_runtime) * 1e3:.6f}")
    
    jax_runtime = timeit.repeat(lambda: jax.numpy.argmin(jx, axis=1), repeat=10, number=1)[3:]
    print(f"JAX argmin time [ms]: avg {np.mean(jax_runtime) * 1e3:.6f} std {np.std(jax_runtime) * 1e3:.6f}")
    
    time = test_native_code(
        program,
        {'x': x, 'z': z},
        min_reps=10,
        remove_tmp_files=False,
        silent=False,
    )[3:]
    print(f"My time [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")


def run_argmax(M, N):
    program_text = f"""
        Name: argmax
        In: x
        Out: z
        Declarations:
            x f32 [{M}, {N}] heap
            z i32 [{M}] heap
            max_vals f32 [{M}] heap
            max_mask f32 [{M}, {N}] heap
            idx i32 [{M}, {N}] heap
            inv_idx i32 [{M}, {N}] heap
            max_idx f32 [{M}] heap
            max_idx_i i32 [{M}] heap
        Code:
        {M} {N} max_vals[{{0}}] fmaxf= x[{{0}}, {{1}}]
        {M} {N} max_mask[{{0}}, {{1}}] = x[{{0}}, {{1}}] - max_vals[{{0}}]
        {M} {N} idx[{{0}}, {{1}}] = {N} - {{1}}
        {M} {N} inv_idx[{{0}}, {{1}}] = copysignf(idx[{{0}}, {{1}}], max_mask[{{0}}, {{1}}])
        {M} {N} max_idx[{{0}}] fmaxf= inv_idx[{{0}}, {{1}}]
        {M} max_idx_i[{{0}}] = (i32) max_idx[{{0}}]
        {M} z[{{0}}] = {N} - max_idx_i[{{0}}]
    """
    
    program = parse_program(program_text)
    print(program.text())
    
    apply_to_exhaustion(program, CPU_EXHAUSTIVE_HEURISTIC.values(), silent=False)
    
    print(program.text())
    
    x = np.random.uniform(-10, 10, (M, N)).astype(np.float32)
    tx = torch.from_numpy(x)
    jx = jax.numpy.asarray(x)
    z = np.argmax(x, axis=1).astype(np.int32)
    
    np_runtime = timeit.repeat(lambda: np.argmax(x, axis=1), repeat=10, number=1)[3:]
    print(f"Numpy argmax time [ms]: avg {np.mean(np_runtime) * 1e3:.6f} std {np.std(np_runtime) * 1e3:.6f}")
    
    pt_runtime = timeit.repeat(lambda: torch.argmax(tx, dim=1), repeat=10, number=1)[3:]
    print(f"Pytorch argmax time [ms]: avg {np.mean(pt_runtime) * 1e3:.6f} std {np.std(pt_runtime) * 1e3:.6f}")
    
    jax_runtime = timeit.repeat(lambda: jax.numpy.argmax(jx, axis=1), repeat=10, number=1)[3:]
    print(f"JAX argmax time [ms]: avg {np.mean(jax_runtime) * 1e3:.6f} std {np.std(jax_runtime) * 1e3:.6f}")
    
    time = test_native_code(
        program,
        {'x': x, 'z': z},
        min_reps=10,
        remove_tmp_files=False,
        silent=False,
    )[3:]
    print(f"My time [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")


def run_conv(N, M, C, H_in, W_in, K):
    
    H = pooling_out_dim(input=H_in, kernel=K, stride=1, pad=0, dilation=1)
    W = pooling_out_dim(input=W_in, kernel=K, stride=1, pad=0, dilation=1)
    
    program_text = f"""
        Name: conv2d
        In: input, weight
        Out: output
        Declarations:
            input f32 [{N}, {C}, {H_in}, {W_in}] heap
            weight f32 [{M}, {C}, {K}, {K}] heap
            output f32 [{N}, {M}, {H}, {W}] heap
        Code:
        {N} {M} {H} {W} {C} {K} {K} output[{{0}}, {{1}}, {{2}}, {{3}}] += input[{{0}}, {{4}}, {{2}} + {{5}}, {{3}} + {{6}}] * weight[{{1}}, {{4}}, {{5}}, {{6}}]
    """
    
    program = parse_program(program_text)
    print(program.text())
    
    apply_to_exhaustion(program, CPU_EXHAUSTIVE_HEURISTIC.values(), silent=False)
    print(program.text())
    
    input = np.random.uniform(-10, 10, (N, C, H_in, W_in)).astype(np.float32)
    weight = np.random.uniform(-10, 10, (M, C, K, K)).astype(np.float32)
    tinput = torch.from_numpy(input)
    tweight = torch.from_numpy(weight)
    jinput = jax.numpy.asarray(input)
    jweight = jax.numpy.asarray(weight)
    toutput = torch.nn.functional.conv2d(tinput, tweight)
    output = toutput.numpy()
        
    pt_builtin_runtime = timeit.repeat(lambda: torch.nn.functional.conv2d(tinput, tweight), repeat=10, number=1)[3:]
    print(f"Pytorch builtin conv2d time [ms]: avg {np.mean(pt_builtin_runtime) * 1e3:.6f} std {np.std(pt_builtin_runtime) * 1e3:.6f}")
    
    jout = jax.lax.conv(jinput, jweight, (1, 1), 'VALID')
    assert np.allclose(jout, output, atol=1e-3, rtol=1e-3)
    
    jax_builtin_runtime = timeit.repeat(lambda: jax.lax.conv(jinput, jweight, (1, 1), 'VALID'), repeat=10, number=1)[3:]
    print(f"JAX builtin conv time [ms]: avg {np.mean(jax_builtin_runtime) * 1e3:.6f} std {np.std(jax_builtin_runtime) * 1e3:.6f}")
    
    time = test_native_code(
        program,
        {'input': input, 'weight': weight, 'output': output},
        min_reps=10,
        remove_tmp_files=False,
        silent=False,
    )[3:]
    print(f"My time [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")
    
    
def run_clip(M, N):
    program_text = f"""
        Name: clip
        In: x, a, b
        Out: z
        Declarations:
            x f32 [{M}, {N}] heap
            a f32 [] heap
            b f32 [] heap
            z f32 [{M}, {N}] heap
            t f32 [{M}, {N}] heap
        Code:
        {M} {N} t[{{0}}, {{1}}] = fminf(x[{{0}}, {{1}}], b)
        {M} {N} z[{{0}}, {{1}}] = fmaxf(t[{{0}}, {{1}}], a)
    """
    
    program = parse_program(program_text)
    print(program.text())
    
    apply_to_exhaustion(program, CPU_EXHAUSTIVE_HEURISTIC.values(), silent=False)
    print(program.text())
    
    x = np.random.uniform(-10, 10, (M, N)).astype(np.float32)
    a = np.array(-4.5).astype(np.float32)
    b = np.array(7.0).astype(np.float32)
    z = np.clip(x, a, b)
    tx = torch.from_numpy(x)
    ta = torch.tensor(a)
    tb = torch.tensor(b)

    np_runtime = timeit.repeat(lambda: np.clip(x, a, b), repeat=10, number=1)[3:]
    print(f"Numpy clip time [ms]: avg {np.mean(np_runtime) * 1e3:.6f} std {np.std(np_runtime) * 1e3:.6f}")
    
    pt_runtime = timeit.repeat(lambda: torch.clamp(tx, ta, tb), repeat=10, number=1)[3:]
    print(f"Pytorch clip time [ms]: avg {np.mean(pt_runtime) * 1e3:.6f} std {np.std(pt_runtime) * 1e3:.6f}")
    
    jax_runtime = timeit.repeat(lambda: jax.numpy.clip(x, a, b), repeat=10, number=1)[3:]
    print(f"JAX clip time [ms]: avg {np.mean(jax_runtime) * 1e3:.6f} std {np.std(jax_runtime) * 1e3:.6f}")
    
    time = test_native_code(
        program,
        {'x': x, 'a': a, 'b': b, 'z': z},
        min_reps=10,
        remove_tmp_files=False,
        silent=False,
    )[3:]
    print(f"My clip [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")    


def run_blackman_window(N):
    two_pi_n1 = 2 * np.pi / (N - 1)
    four_pi_n1 = two_pi_n1 * 2
    program_text = f"""
        Name: blackman_window
        In:
        Out: w
        Declarations:
            arg1 f32 [{N}] heap
            cos1 f32 [{N}] heap
            arg2 f32 [{N}] heap
            cos2 f32 [{N}] heap
            tmp f32 [{N}] heap
            w f32 [{N}] heap
        Code:
        {N} arg1[{{0}}] = {two_pi_n1} * {{0}}
        {N} cos1[{{0}}] = cosf(arg1[{{0}}])
        {N} arg2[{{0}}] = {four_pi_n1} * {{0}}
        {N} cos2[{{0}}] = cosf(arg2[{{0}}])
        {N} tmp[{{0}}] = 0.08 * cos2[{{0}}] + 0.42
        {N} w[{{0}}] = -0.5 * cos1[{{0}}] + tmp[{{0}}]
    """
    program = parse_program(program_text)
    print(program.text())
    
    apply_to_exhaustion(program, CPU_EXHAUSTIVE_HEURISTIC.values(), silent=False)
    print(program.text())
    
    np_runtime = timeit.repeat(lambda: np.blackman(N), repeat=10, number=1)[3:]
    print(f"Numpy blackman time [ms]: avg {np.mean(np_runtime) * 1e3:.6f} std {np.std(np_runtime) * 1e3:.6f}")
    
    def torch_blackman_func():
        x = torch.arange(N, dtype=torch.float32)
        return 0.42 - 0.5 * torch.cos(two_pi_n1 * x) + 0.08 * torch.cos(four_pi_n1 * x)
    
    pt_runtime = timeit.repeat(lambda: torch_blackman_func(), repeat=10, number=1)[3:]
    print(f"Pytorch blackman time [ms]: avg {np.mean(pt_runtime) * 1e3:.6f} std {np.std(pt_runtime) * 1e3:.6f}")
    
    torch_jit_blackman_func = torch.jit.trace(torch_blackman_func, [])
    pt_jit_runtime = timeit.repeat(lambda: torch_jit_blackman_func(), repeat=10, number=1)[3:]
    print(f"Pytorch JIT blackman time [ms]: avg {np.mean(pt_jit_runtime) * 1e3:.6f} std {np.std(pt_jit_runtime) * 1e3:.6f}")
    
    pt_builtin_runtime = timeit.repeat(lambda: torch.blackman_window(N), repeat=10, number=1)[3:]
    print(f"Pytorch builtin blackman time [ms]: avg {np.mean(pt_builtin_runtime) * 1e3:.6f} std {np.std(pt_builtin_runtime) * 1e3:.6f}")
    
    def jax_blackman_func():
        x = jax.numpy.arange(N)
        return 0.42 - 0.5 * jax.numpy.cos(two_pi_n1 * x) + 0.08 * jax.numpy.cos(four_pi_n1 * x)
    
    jax_runtime = timeit.repeat(lambda: jax_blackman_func(), repeat=10, number=1)[3:]
    print(f"JAX blackman time [ms]: avg {np.mean(jax_runtime) * 1e3:.6f} std {np.std(jax_runtime) * 1e3:.6f}")
    
    jax_jit_blackman_func = jax.jit(jax_blackman_func)
    jax_jit_runtime = timeit.repeat(lambda: jax_jit_blackman_func(), repeat=10, number=1)[3:]
    print(f"JAX JIT blackman time [ms]: avg {np.mean(jax_jit_runtime) * 1e3:.6f} std {np.std(jax_jit_runtime) * 1e3:.6f}")
    
    jax_builtin_runtime = timeit.repeat(lambda: jax.numpy.blackman(N), repeat=10, number=1)[3:]
    print(f"JAX builtin blackman time [ms]: avg {np.mean(jax_builtin_runtime) * 1e3:.6f} std {np.std(jax_builtin_runtime) * 1e3:.6f}")
    
    ref_output = np.blackman(N).astype(np.float32)
    
    time = test_native_code(
        program,
        {'w': ref_output},
        min_reps=10,
        remove_tmp_files=False,
        silent=False,
    )[3:]
    print(f"My blackman [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")
    


def run_cos(M, N):
    program_text = f"""
        Name: func_cos
        In: x
        Out: z
        Declarations:
            x f32 [{M}, {N}] heap
            z f32 [{M}, {N}] heap
        Code:
        {M} {N} z[{{0}}, {{1}}] = cosf(x[{{0}}, {{1}}])
    """
    
    program = parse_program(program_text)
    print(program.text())
    
    apply_to_exhaustion(program, CPU_EXHAUSTIVE_HEURISTIC.values(), silent=False)
    print(program.text())
    
    x = np.random.uniform(-10, 10, (M, N)).astype(np.float32)
    tx = torch.from_numpy(x)
    jx = jax.numpy.asarray(x)
    
    np_runtime = timeit.repeat(lambda: np.cos(x), repeat=10, number=1)[3:]
    print(f"Numpy cos time [ms]: avg {np.mean(np_runtime) * 1e3:.6f} std {np.std(np_runtime) * 1e3:.6f}")
    
    pt_runtime = timeit.repeat(lambda: torch.cos(tx), repeat=10, number=1)[3:]
    print(f"Pytorch cos time [ms]: avg {np.mean(pt_runtime) * 1e3:.6f} std {np.std(pt_runtime) * 1e3:.6f}")
    
    jax_runtime = timeit.repeat(lambda: jax.numpy.cos(jx), repeat=10, number=1)[3:]
    print(f"JAX cos time [ms]: avg {np.mean(jax_runtime) * 1e3:.6f} std {np.std(jax_runtime) * 1e3:.6f}")
    
    time = test_native_code(
        program,
        {'x': x, 'z': np.cos(x)},
        min_reps=10,
        remove_tmp_files=False,
        silent=False,
    )[3:]
    print(f"My cos [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")
    
    
def run_floor(M, N):
    program_text = f"""
        Name: func_floor
        In: x
        Out: z
        Declarations:
            x f32 [{M}, {N}] heap
            z f32 [{M}, {N}] heap
        Code:
        {M} {N} z[{{0}}, {{1}}] = floorf(x[{{0}}, {{1}}])
    """
    
    program = parse_program(program_text)
    print(program.text())
    
    apply_to_exhaustion(program, CPU_EXHAUSTIVE_HEURISTIC.values(), silent=False)
    print(program.text())
    
    x = np.random.uniform(-10, 10, (M, N)).astype(np.float32)
    tx = torch.from_numpy(x)
    jx = jax.numpy.asarray(x)
    
    np_runtime = timeit.repeat(lambda: np.floor(x), repeat=10, number=1)[3:]
    print(f"Numpy floor time [ms]: avg {np.mean(np_runtime) * 1e3:.6f} std {np.std(np_runtime) * 1e3:.6f}")
    
    pt_runtime = timeit.repeat(lambda: torch.floor(tx), repeat=10, number=1)[3:]
    print(f"Pytorch floor time [ms]: avg {np.mean(pt_runtime) * 1e3:.6f} std {np.std(pt_runtime) * 1e3:.6f}")
    
    jax_runtime = timeit.repeat(lambda: jax.numpy.floor(jx), repeat=10, number=1)[3:]
    print(f"JAX floor time [ms]: avg {np.mean(jax_runtime) * 1e3:.6f} std {np.std(jax_runtime) * 1e3:.6f}")
    
    time = test_native_code(
        program,
        {'x': x, 'z': np.floor(x)},
        min_reps=10,
        remove_tmp_files=False,
        silent=False,
    )[3:]
    print(f"My floor [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")


def run_ceil(M, N):
    program_text = f"""
        Name: func_ceil
        In: x
        Out: z
        Declarations:
            x f32 [{M}, {N}] heap
            z f32 [{M}, {N}] heap
        Code:
        {M} {N} z[{{0}}, {{1}}] = ceilf(x[{{0}}, {{1}}])
    """
    
    program = parse_program(program_text)
    print(program.text())
    
    apply_to_exhaustion(program, CPU_EXHAUSTIVE_HEURISTIC.values(), silent=False)
    print(program.text())
    
    x = np.random.uniform(-10, 10, (M, N)).astype(np.float32)
    tx = torch.from_numpy(x)
    jx = jax.numpy.asarray(x)
    
    np_runtime = timeit.repeat(lambda: np.ceil(x), repeat=10, number=1)[3:]
    print(f"Numpy ceil time [ms]: avg {np.mean(np_runtime) * 1e3:.6f} std {np.std(np_runtime) * 1e3:.6f}")
    
    pt_runtime = timeit.repeat(lambda: torch.ceil(tx), repeat=10, number=1)[3:]
    print(f"Pytorch ceil time [ms]: avg {np.mean(pt_runtime) * 1e3:.6f} std {np.std(pt_runtime) * 1e3:.6f}")
    
    jax_runtime = timeit.repeat(lambda: jax.numpy.ceil(jx), repeat=10, number=1)[3:]
    print(f"JAX ceil time [ms]: avg {np.mean(jax_runtime) * 1e3:.6f} std {np.std(jax_runtime) * 1e3:.6f}")
    
    time = test_native_code(
        program,
        {'x': x, 'z': np.ceil(x)},
        min_reps=10,
        remove_tmp_files=False,
        silent=False,
    )[3:]
    print(f"My ceil [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")


def run_bitwise_xor(M, N):
    program_text = f"""
        Name: func_bitwise_xor
        In: x, y
        Out: z
        Declarations:
            x i32 [{M}, {N}] heap
            y i32 [{M}, {N}] heap
            z i32 [{M}, {N}] heap
        Code:
        {M} {N} z[{{0}}, {{1}}] = x[{{0}}, {{1}}] ^ y[{{0}}, {{1}}]
    """
    
    program = parse_program(program_text)
    print(program.text())
    
    apply_to_exhaustion(program, CPU_EXHAUSTIVE_HEURISTIC.values(), silent=False)
    print(program.text())
    
    x = np.random.randint(0, 1000, (M, N), dtype=np.int32)
    y = np.random.randint(0, 1000, (M, N), dtype=np.int32)
    tx = torch.from_numpy(x)
    ty = torch.from_numpy(y)
    jx = jax.numpy.asarray(x)
    jy = jax.numpy.asarray(y)
    
    np_runtime = timeit.repeat(lambda: x ^ y, repeat=10, number=1)[3:]
    print(f"Numpy xor time [ms]: avg {np.mean(np_runtime) * 1e3:.6f} std {np.std(np_runtime) * 1e3:.6f}")
    
    pt_runtime = timeit.repeat(lambda: tx ^ ty, repeat=10, number=1)[3:]
    print(f"Pytorch xor time [ms]: avg {np.mean(pt_runtime) * 1e3:.6f} std {np.std(pt_runtime) * 1e3:.6f}")
    
    jax_runtime = timeit.repeat(lambda: jx ^ jy, repeat=10, number=1)[3:]
    print(f"JAX xor time [ms]: avg {np.mean(jax_runtime) * 1e3:.6f} std {np.std(jax_runtime) * 1e3:.6f}")
    
    time = test_native_code(
        program,
        {'x': x, 'y': y, 'z': x ^ y},
        min_reps=10,
        remove_tmp_files=False,
        silent=False,
    )[3:]
    print(f"My xor [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")


def run_bitwise_or(M, N):
    program_text = f"""
        Name: func_bitwise_or
        In: x, y
        Out: z
        Declarations:
            x i32 [{M}, {N}] heap
            y i32 [{M}, {N}] heap
            z i32 [{M}, {N}] heap
        Code:
        {M} {N} z[{{0}}, {{1}}] = x[{{0}}, {{1}}] | y[{{0}}, {{1}}]
    """
    
    program = parse_program(program_text)
    print(program.text())
    
    apply_to_exhaustion(program, CPU_EXHAUSTIVE_HEURISTIC.values(), silent=False)
    print(program.text())
    
    x = np.random.randint(0, 1000, (M, N), dtype=np.int32)
    y = np.random.randint(0, 1000, (M, N), dtype=np.int32)
    tx = torch.from_numpy(x)
    ty = torch.from_numpy(y)
    jx = jax.numpy.asarray(x)
    jy = jax.numpy.asarray(y)
    
    np_runtime = timeit.repeat(lambda: x | y, repeat=10, number=1)[3:]
    print(f"Numpy or time [ms]: avg {np.mean(np_runtime) * 1e3:.6f} std {np.std(np_runtime) * 1e3:.6f}")
    
    pt_runtime = timeit.repeat(lambda: tx | ty, repeat=10, number=1)[3:]
    print(f"Pytorch or time [ms]: avg {np.mean(pt_runtime) * 1e3:.6f} std {np.std(pt_runtime) * 1e3:.6f}")
    
    jax_runtime = timeit.repeat(lambda: jx | jy, repeat=10, number=1)[3:]
    print(f"JAX or time [ms]: avg {np.mean(jax_runtime) * 1e3:.6f} std {np.std(jax_runtime) * 1e3:.6f}")
    
    time = test_native_code(
        program,
        {'x': x, 'y': y, 'z': x | y},
        min_reps=10,
        remove_tmp_files=False,
        silent=False,
    )[3:]
    print(f"My or [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")
    

def run_bitwise_and(M, N):
    program_text = f"""
        Name: func_bitwise_and
        In: x, y
        Out: z
        Declarations:
            x i32 [{M}, {N}] heap
            y i32 [{M}, {N}] heap
            z i32 [{M}, {N}] heap
        Code:
        {M} {N} z[{{0}}, {{1}}] = x[{{0}}, {{1}}] & y[{{0}}, {{1}}]
    """
    
    program = parse_program(program_text)
    print(program.text())
    
    apply_to_exhaustion(program, CPU_EXHAUSTIVE_HEURISTIC.values(), silent=False)
    print(program.text())
    
    x = np.random.randint(0, 1000, (M, N), dtype=np.int32)
    y = np.random.randint(0, 1000, (M, N), dtype=np.int32)
    tx = torch.from_numpy(x)
    ty = torch.from_numpy(y)
    jx = jax.numpy.asarray(x)
    jy = jax.numpy.asarray(y)
    
    np_runtime = timeit.repeat(lambda: x & y, repeat=10, number=1)[3:]
    print(f"Numpy and time [ms]: avg {np.mean(np_runtime) * 1e3:.6f} std {np.std(np_runtime) * 1e3:.6f}")
    
    pt_runtime = timeit.repeat(lambda: tx & ty, repeat=10, number=1)[3:]
    print(f"Pytorch and time [ms]: avg {np.mean(pt_runtime) * 1e3:.6f} std {np.std(pt_runtime) * 1e3:.6f}")
    
    jax_runtime = timeit.repeat(lambda: jx & jy, repeat=10, number=1)[3:]
    print(f"JAX and time [ms]: avg {np.mean(jax_runtime) * 1e3:.6f} std {np.std(jax_runtime) * 1e3:.6f}")
    
    time = test_native_code(
        program,
        {'x': x, 'y': y, 'z': x & y},
        min_reps=10,
        remove_tmp_files=False,
        silent=False,
    )[3:]
    print(f"My and [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")
    
    
def run_bitwise_not(M, N):
    program_text = f"""
        Name: func_bitwise_not
        In: x
        Out: z
        Declarations:
            x i32 [{M}, {N}] heap
            z i32 [{M}, {N}] heap
        Code:
        {M} {N} z[{{0}}, {{1}}] = ~x[{{0}}, {{1}}]
    """
    
    program = parse_program(program_text)
    print(program.text())
    
    apply_to_exhaustion(program, CPU_EXHAUSTIVE_HEURISTIC.values(), silent=False)
    print(program.text())
    
    x = np.random.randint(0, 1000, (M, N), dtype=np.int32)
    tx = torch.from_numpy(x)
    jx = jax.numpy.asarray(x)
    
    np_runtime = timeit.repeat(lambda: ~x, repeat=10, number=1)[3:]
    print(f"Numpy not time [ms]: avg {np.mean(np_runtime) * 1e3:.6f} std {np.std(np_runtime) * 1e3:.6f}")
    
    pt_runtime = timeit.repeat(lambda: ~tx, repeat=10, number=1)[3:]
    print(f"Pytorch not time [ms]: avg {np.mean(pt_runtime) * 1e3:.6f} std {np.std(pt_runtime) * 1e3:.6f}")
    
    jax_runtime = timeit.repeat(lambda: ~jx, repeat=10, number=1)[3:]
    print(f"JAX not time [ms]: avg {np.mean(jax_runtime) * 1e3:.6f} std {np.std(jax_runtime) * 1e3:.6f}")
    
    time = test_native_code(
        program,
        {'x': x, 'z': ~x},
        min_reps=10,
        remove_tmp_files=False,
        silent=False,
    )[3:]
    print(f"My not [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")


def run_bitshift_left(M, N):
    program_text = f"""
        Name: bitshift_left
        In: x, y
        Out: z
        Declarations:
            x i32 [{M}, {N}] heap
            y i32 [{M}, {N}] heap
            z i32 [{M}, {N}] heap
        Code:
        {M} {N} z[{{0}}, {{1}}] = x[{{0}}, {{1}}] << y[{{0}}, {{1}}]
    """
    
    program = parse_program(program_text)
    print(program.text())
    
    apply_to_exhaustion(program, CPU_EXHAUSTIVE_HEURISTIC.values(), silent=False)
    print(program.text())
    
    x = np.random.randint(0, 1000, (M, N), dtype=np.int32)
    y = np.random.randint(0, 32, (M, N), dtype=np.int32)
    tx = torch.from_numpy(x)
    ty = torch.from_numpy(y)
    jx = jax.numpy.asarray(x)
    jy = jax.numpy.asarray(y)
    
    np_runtime = timeit.repeat(lambda: x << y, repeat=10, number=1)[3:]
    print(f"Numpy bitshift left time [ms]: avg {np.mean(np_runtime) * 1e3:.6f} std {np.std(np_runtime) * 1e3:.6f}")
    
    pt_runtime = timeit.repeat(lambda: tx << ty, repeat=10, number=1)[3:]
    print(f"Pytorch bitshift left time [ms]: avg {np.mean(pt_runtime) * 1e3:.6f} std {np.std(pt_runtime) * 1e3:.6f}")
    
    jax_runtime = timeit.repeat(lambda: jx << jy, repeat=10, number=1)[3:]
    print(f"JAX bitshift left time [ms]: avg {np.mean(jax_runtime) * 1e3:.6f} std {np.std(jax_runtime) * 1e3:.6f}")
    
    time = test_native_code(
        program,
        {'x': x, 'y': y, 'z': x << y},
        min_reps=10,
        remove_tmp_files=False,
        silent=False,
    )[3:]
    print(f"My bitshift left [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")


def run_averagepool(N, C, H_in, W_in, K):
    
    H = pooling_out_dim(input=H_in, kernel=K, stride=1, pad=0, dilation=1)
    W = pooling_out_dim(input=W_in, kernel=K, stride=1, pad=0, dilation=1)
    
    program_text = f"""
        Name: averagepool2d
        In: input
        Out: output
        Declarations:
            input f32 [{N}, {C}, {H_in}, {W_in}] heap
            tmp f32 [{N}, {C}, {H}, {W}] heap
            output f32 [{N}, {C}, {H}, {W}] heap
        Code:
        {N} {C} {H} {W} {K} {K} tmp[{{0}}, {{1}}, {{2}}, {{3}}] += input[{{0}}, {{1}}, {{2}} + {{4}}, {{3}} + {{5}}]
        {N} {C} {H} {W} output[{{0}}, {{1}}, {{2}}, {{3}}] = tmp[{{0}}, {{1}}, {{2}}, {{3}}] / {K * K}
    """
    
    program = parse_program(program_text)
    print(program.text())
    
    apply_to_exhaustion(program, CPU_EXHAUSTIVE_HEURISTIC.values(), silent=False)
    print(program.text())
    
    def jax_avg_pool2d(jinput):
        return jax.lax.reduce_window(jinput, 0.0, jax.lax.add, window_dimensions=(1, 1, K, K), window_strides=[1 for _ in input.shape], padding=[(0,0) for _ in input.shape]) / (K * K)
    
    input = np.random.uniform(-10, 10, (N, C, H_in, W_in)).astype(np.float32)
    tinput = torch.from_numpy(input)
    toutput = torch.nn.functional.avg_pool2d(tinput, kernel_size=K, stride=1, padding=0)
    jinput = jax.numpy.asarray(input)
    joutput = jax_avg_pool2d(jinput)
    output = toutput.numpy()
    
    assert np.allclose(output, joutput, rtol=1e-3, atol=1e-3)
    
    pt_runtime = timeit.repeat(lambda: torch.nn.functional.avg_pool2d(tinput, K), repeat=10, number=1)[3:]
    print(f"Pytorch avg_pool2d time [ms]: avg {np.mean(pt_runtime) * 1e3:.6f} std {np.std(pt_runtime) * 1e3:.6f}")
    
    jax_runtime = timeit.repeat(lambda: jax_avg_pool2d(jinput), repeat=10, number=1)[3:]
    print(f"JAX avg_pool2d time [ms]: avg {np.mean(jax_runtime) * 1e3:.6f} std {np.std(jax_runtime) * 1e3:.6f}")
    
    jax_jit_avg_pool2d = jax.jit(jax_avg_pool2d)
    jax_jit_runtime = timeit.repeat(lambda: jax_jit_avg_pool2d(jinput), repeat=10, number=1)[3:]
    print(f"JAX JIT avg_pool2d time [ms]: avg {np.mean(jax_jit_runtime) * 1e3:.6f} std {np.std(jax_jit_runtime) * 1e3:.6f}")
    
    time = test_native_code(
        program,
        {'input': input, 'output': output},
        min_reps=10,
        remove_tmp_files=False,
        silent=False,
    )[3:]
    print(f"My avg_pool2d [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")


def run_atanh(M, N):
    program_text = f"""
        Name: func_atanh
        In: x
        Out: z
        Declarations:
            x f32 [{M}, {N}] heap
            z f32 [{M}, {N}] heap
        Code:
        {M} {N} z[{{0}}, {{1}}] = atanhf(x[{{0}}, {{1}}])
    """
    
    program = parse_program(program_text)
    print(program.text())
    
    apply_to_exhaustion(program, CPU_EXHAUSTIVE_HEURISTIC.values(), silent=False)
    print(program.text())
    
    x = np.random.uniform(-0.99, 0.99, (M, N)).astype(np.float32)
    tx = torch.from_numpy(x)
    jx = jax.numpy.asarray(x)
    
    np_runtime = timeit.repeat(lambda: np.arctanh(x), repeat=10, number=1)[3:]
    print(f"Numpy atanh time [ms]: avg {np.mean(np_runtime) * 1e3:.6f} std {np.std(np_runtime) * 1e3:.6f}")
    
    pt_runtime = timeit.repeat(lambda: torch.atanh(tx), repeat=10, number=1)[3:]
    print(f"Pytorch atanh time [ms]: avg {np.mean(pt_runtime) * 1e3:.6f} std {np.std(pt_runtime) * 1e3:.6f}")
    
    jax_runtime = timeit.repeat(lambda: jax.numpy.arctanh(jx), repeat=10, number=1)[3:]
    print(f"JAX atanh time [ms]: avg {np.mean(jax_runtime) * 1e3:.6f} std {np.std(jax_runtime) * 1e3:.6f}")
    
    time = test_native_code(
        program,
        {'x': x, 'z': np.arctanh(x)},
        min_reps=10,
        remove_tmp_files=False,
        silent=False,
    )[3:]
    print(f"My atanh [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")


def run_atan(M, N):
    program_text = f"""
        Name: func_atan
        In: x
        Out: z
        Declarations:
            x f32 [{M}, {N}] heap
            z f32 [{M}, {N}] heap
        Code:
        {M} {N} z[{{0}}, {{1}}] = atanf(x[{{0}}, {{1}}])
    """
    
    program = parse_program(program_text)
    print(program.text())
    
    apply_to_exhaustion(program, CPU_EXHAUSTIVE_HEURISTIC.values(), silent=False)
    print(program.text())
    
    x = np.random.uniform(-10, 10, (M, N)).astype(np.float32)
    tx = torch.from_numpy(x)
    jx = jax.numpy.asarray(x)
    
    np_runtime = timeit.repeat(lambda: np.arctan(x), repeat=10, number=1)[3:]
    print(f"Numpy atan time [ms]: avg {np.mean(np_runtime) * 1e3:.6f} std {np.std(np_runtime) * 1e3:.6f}")
    
    pt_runtime = timeit.repeat(lambda: torch.atan(tx), repeat=10, number=1)[3:]
    print(f"Pytorch atan time [ms]: avg {np.mean(pt_runtime) * 1e3:.6f} std {np.std(pt_runtime) * 1e3:.6f}")
    
    jax_runtime = timeit.repeat(lambda: jax.numpy.arctan(jx), repeat=10, number=1)[3:]
    print(f"JAX atan time [ms]: avg {np.mean(jax_runtime) * 1e3:.6f} std {np.std(jax_runtime) * 1e-3:.6f}")
    
    time = test_native_code(
        program,
        {'x': x, 'z': np.arctan(x)},
        min_reps=10,
        remove_tmp_files=False,
        silent=False,
    )[3:]
    print(f"My atan [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")


def run_asinh(M, N):
    program_text = f"""
        Name: func_asinh
        In: x
        Out: z
        Declarations:
            x f32 [{M}, {N}] heap
            z f32 [{M}, {N}] heap
        Code:
        {M} {N} z[{{0}}, {{1}}] = asinhf(x[{{0}}, {{1}}])
    """
    
    program = parse_program(program_text)
    print(program.text())
    
    apply_to_exhaustion(program, CPU_EXHAUSTIVE_HEURISTIC.values(), silent=False)
    print(program.text())
    
    x = np.random.uniform(-10, 10, (M, N)).astype(np.float32)
    tx = torch.from_numpy(x)
    jx = jax.numpy.asarray(x)
    
    np_runtime = timeit.repeat(lambda: np.arcsinh(x), repeat=10, number=1)[3:]
    print(f"Numpy asinh time [ms]: avg {np.mean(np_runtime) * 1e3:.6f} std {np.std(np_runtime) * 1e3:.6f}")
    
    pt_runtime = timeit.repeat(lambda: torch.asinh(tx), repeat=10, number=1)[3:]
    print(f"Pytorch asinh time [ms]: avg {np.mean(pt_runtime) * 1e3:.6f} std {np.std(pt_runtime) * 1e3:.6f}")
    
    jax_runtime = timeit.repeat(lambda: jax.numpy.arcsinh(jx), repeat=10, number=1)[3:]
    print(f"JAX asinh time [ms]: avg {np.mean(jax_runtime) * 1e3:.6f} std {np.std(jax_runtime) * 1e3:.6f}")
    
    time = test_native_code(
        program,
        {'x': x, 'z': np.arcsinh(x)},
        min_reps=10,
        remove_tmp_files=False,
        silent=False,
    )[3:]
    print(f"My asinh [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")
    
    
def run_asin(M, N):
    program_text = f"""
        Name: func_asin
        In: x
        Out: z
        Declarations:
            x f32 [{M}, {N}] heap
            z f32 [{M}, {N}] heap
        Code:
        {M} {N} z[{{0}}, {{1}}] = asinf(x[{{0}}, {{1}}])
    """
    
    program = parse_program(program_text)
    print(program.text())
    
    apply_to_exhaustion(program, CPU_EXHAUSTIVE_HEURISTIC.values(), silent=False)
    print(program.text())
    
    x = np.random.uniform(-1, 1, (M, N)).astype(np.float32)
    tx = torch.from_numpy(x)
    jx = jax.numpy.asarray(x)
    
    np_runtime = timeit.repeat(lambda: np.arcsin(x), repeat=10, number=1)[3:]
    print(f"Numpy asin time [ms]: avg {np.mean(np_runtime) * 1e3:.6f} std {np.std(np_runtime) * 1e3:.6f}")
    
    pt_runtime = timeit.repeat(lambda: torch.asin(tx), repeat=10, number=1)[3:]
    print(f"Pytorch asin time [ms]: avg {np.mean(pt_runtime) * 1e3:.6f} std {np.std(pt_runtime) * 1e3:.6f}")
    
    jax_runtime = timeit.repeat(lambda: jax.numpy.arcsin(jx), repeat=10, number=1)[3:]
    print(f"JAX asin time [ms]: avg {np.mean(jax_runtime) * 1e3:.6f} std {np.std(jax_runtime) * 1e3:.6f}")
    
    time = test_native_code(
        program,
        {'x': x, 'z': np.arcsin(x)},
        min_reps=10,
        remove_tmp_files=False,
        silent=False,
    )[3:]
    print(f"My asin [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")


def run_and(M, N):
    program_text = f"""
        Name: logical_and
        In: x, y
        Out: z
        Declarations:
            x i32 [{M}, {N}] heap
            y i32 [{M}, {N}] heap
            z i32 [{M}, {N}] heap
        Code:
        {M} {N} z[{{0}}, {{1}}] = x[{{0}}, {{1}}] && y[{{0}}, {{1}}]
    """
    
    program = parse_program(program_text)
    print(program.text())
    
    apply_to_exhaustion(program, CPU_EXHAUSTIVE_HEURISTIC.values(), silent=False)
    print(program.text())
    
    x = np.random.randint(0, 2, (M, N), dtype=np.int32)
    y = np.random.randint(0, 2, (M, N), dtype=np.int32)
    tx = torch.from_numpy(x)
    ty = torch.from_numpy(y)
    jx = jax.numpy.asarray(x)
    jy = jax.numpy.asarray(y)
    
    np_runtime = timeit.repeat(lambda: np.logical_and(x, y), repeat=10, number=1)[3:]
    print(f"Numpy logical_and time [ms]: avg {np.mean(np_runtime) * 1e3:.6f} std {np.std(np_runtime) * 1e3:.6f}")
    
    pt_runtime = timeit.repeat(lambda: torch.logical_and(tx, ty), repeat=10, number=1)[3:]
    print(f"Pytorch logical_and time [ms]: avg {np.mean(pt_runtime) * 1e3:.6f} std {np.std(pt_runtime) * 1e3:.6f}")
    
    jax_runtime = timeit.repeat(lambda: jax.numpy.logical_and(jx, jy), repeat=10, number=1)[3:]
    print(f"JAX logical_and time [ms]: avg {np.mean(jax_runtime) * 1e3:.6f} std {np.std(jax_runtime) * 1e3:.6f}")
    
    time = test_native_code(
        program,
        {'x': x, 'y': y, 'z': np.logical_and(x, y).astype(np.int32)},
        min_reps=10,
        remove_tmp_files=False,
        silent=False,
    )[3:]
    print(f"My logical_and [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")


def affine_grid_jax(theta, N, H, W):
    xs = jax.numpy.linspace(-1 + 1/W, 1 - 1/W, W)
    ys = jax.numpy.linspace(-1 + 1/H, 1 - 1/H, H)
    grid_x, grid_y = jax.numpy.meshgrid(xs, ys)
    x = jax.numpy.tile(grid_x, (N, 1, 1))
    y = jax.numpy.tile(grid_y, (N, 1, 1))
    t1 = x * theta[:, 0, 0][:, None, None] + theta[:, 0, 2][:, None, None]
    grid0 = y * theta[:, 0, 1][:, None, None] + t1
    t2 = x * theta[:, 1, 0][:, None, None] + theta[:, 1, 2][:, None, None]
    grid1 = y * theta[:, 1, 1][:, None, None] + t2
    return jax.numpy.stack([grid0, grid1], axis=-1)


def run_affine_grid(N, H, W):
    program_text = f"""
        Name: affine_grid
        In: theta
        Out: grid0, grid1
        Declarations:
            theta f32 [{N}, 2, 3] heap
            grid0 f32 [{N}, {H}, {W}] heap
            grid1 f32 [{N}, {H}, {W}] heap
            w21 f32 [{N}, {H}, {W}] heap
            x f32 [{N}, {H}, {W}] heap
            h21 f32 [{N}, {H}, {W}] heap
            y f32 [{N}, {H}, {W}] heap
            t1 f32 [{N}, {H}, {W}] heap
            t2 f32 [{N}, {H}, {W}] heap
        Code:
        {N} {H} {W} w21[{{0}}, {{1}}, {{2}}] = 2 * {{2}} + 1
        {N} {H} {W} x[{{0}}, {{1}}, {{2}}] = w21[{{0}}, {{1}}, {{2}}] * {1/W} - 1
        {N} {H} {W} h21[{{0}}, {{1}}, {{2}}] = 2 * {{1}} + 1
        {N} {H} {W} y[{{0}}, {{1}}, {{2}}] = h21[{{0}}, {{1}}, {{2}}] * {1/H} - 1
        {N} {H} {W} t1[{{0}}, {{1}}, {{2}}] = x[{{0}}, {{1}}, {{2}}] * theta[{{0}}, 0, 0] + theta[{{0}}, 0, 2]
        {N} {H} {W} grid0[{{0}}, {{1}}, {{2}}] = y[{{0}}, {{1}}, {{2}}] * theta[{{0}}, 0, 1] + t1[{{0}}, {{1}}, {{2}}]
        {N} {H} {W} t2[{{0}}, {{1}}, {{2}}] = x[{{0}}, {{1}}, {{2}}] * theta[{{0}}, 1, 0] + theta[{{0}}, 1, 2]
        {N} {H} {W} grid1[{{0}}, {{1}}, {{2}}] = y[{{0}}, {{1}}, {{2}}] * theta[{{0}}, 1, 1] + t2[{{0}}, {{1}}, {{2}}]
    """
    
    program = parse_program(program_text)
    print(program.text())
    
    apply_to_exhaustion(program, CPU_EXHAUSTIVE_HEURISTIC.values(), silent=False)
    print(program.text())
    
    theta = np.random.uniform(-1, 1, (N, 2, 3)).astype(np.float32)
    ttheta = torch.from_numpy(theta)
    
    tgrid = torch.nn.functional.affine_grid(ttheta, (N, 777, H, W), align_corners=False)
    grid0 = tgrid[:, :, :, 0].numpy()
    grid1 = tgrid[:, :, :, 1].numpy()
    
    pt_runtime = timeit.repeat(lambda: torch.nn.functional.affine_grid(ttheta, (N, 777, H, W), align_corners=False), repeat=10, number=1)[3:]
    print(f"Pytorch affine_grid time [ms]: avg {np.mean(pt_runtime) * 1e3:.6f} std {np.std(pt_runtime) * 1e3:.6f}")
    
    def pt_aff_grid(theta):
        return torch.nn.functional.affine_grid(theta, (N, 777, H, W), align_corners=False)
    
    pt_jit_affine_grid = torch.jit.trace(pt_aff_grid, [ttheta])
    pt_jit_runtime = timeit.repeat(lambda: pt_jit_affine_grid(ttheta), repeat=10, number=1)[3:]
    print(f"Pytorch JIT affine_grid time [ms]: avg {np.mean(pt_jit_runtime) * 1e3:.6f} std {np.std(pt_jit_runtime) * 1e3:.6f}")
    
    jtheta = jax.numpy.asarray(theta)
    jgrid = affine_grid_jax(jtheta, N, H, W)
    
    assert np.allclose(tgrid.numpy(), jgrid, rtol=1e-3, atol=1e-3)
    
    jax_runtime = timeit.repeat(lambda: affine_grid_jax(jtheta, N, H, W), repeat=10, number=1)[3:]
    print(f"JAX affine_grid time [ms]: avg {np.mean(jax_runtime) * 1e3:.6f} std {np.std(jax_runtime) * 1e3:.6f}")
    
    def wrapped_jax_affine_grid(theta):
        return affine_grid_jax(theta, N, H, W)
    
    jax_jit_affine_grid = jax.jit(wrapped_jax_affine_grid)
    jax_jit_runtime = timeit.repeat(lambda: jax_jit_affine_grid(jtheta), repeat=10, number=1)[3:]
    print(f"JAX JIT affine_grid time [ms]: avg {np.mean(jax_jit_runtime) * 1e3:.6f} std {np.std(jax_jit_runtime) * 1e3:.6f}")
    
    time = test_native_code(
        program,
        {'theta': theta, 'grid0': grid0, 'grid1': grid1},
        min_reps=10,
        remove_tmp_files=False,
        silent=False,
    )[3:]
    print(f"My affine_grid [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")
    

def run_add(M, N):
    program_text = f"""
        Name: add
        In: x, y
        Out: z
        Declarations:
            x f32 [{M}, {N}] heap
            y f32 [{M}, {N}] heap
            z f32 [{M}, {N}] heap
        Code:
        {M} {N} z[{{0}}, {{1}}] = x[{{0}}, {{1}}] + y[{{0}}, {{1}}]
    """
    
    program = parse_program(program_text)
    print(program.text())
    
    apply_to_exhaustion(program, CPU_EXHAUSTIVE_HEURISTIC.values(), silent=False)
    print(program.text())
    
    x = np.random.uniform(-10, 10, (M, N)).astype(np.float32)
    y = np.random.uniform(-10, 10, (M, N)).astype(np.float32)
    tx = torch.from_numpy(x)
    ty = torch.from_numpy(y)
    jx = jax.numpy.asarray(x)
    jy = jax.numpy.asarray(y)
    
    np_runtime = timeit.repeat(lambda: x + y, repeat=10, number=1)[3:]
    print(f"Numpy add time [ms]: avg {np.mean(np_runtime) * 1e3:.6f} std {np.std(np_runtime) * 1e3:.6f}")
    
    pt_runtime = timeit.repeat(lambda: tx + ty, repeat=10, number=1)[3:]
    print(f"Pytorch add time [ms]: avg {np.mean(pt_runtime) * 1e3:.6f} std {np.std(pt_runtime) * 1e3:.6f}")
    
    jax_runtime = timeit.repeat(lambda: jx + jy, repeat=10, number=1)[3:]
    print(f"JAX add time [ms]: avg {np.mean(jax_runtime) * 1e3:.6f} std {np.std(jax_runtime) * 1e3:.6f}")
    
    time = test_native_code(
        program,
        {'x': x, 'y': y, 'z': x + y},
        min_reps=10,
        remove_tmp_files=False,
        silent=False,
    )[3:]
    print(f"My add [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")


def run_acosh(M, N):
    program_text = f"""
        Name: func_acosh
        In: x
        Out: z
        Declarations:
            x f32 [{M}, {N}] heap
            z f32 [{M}, {N}] heap
        Code:
        {M} {N} z[{{0}}, {{1}}] = acoshf(x[{{0}}, {{1}}])
    """
    
    program = parse_program(program_text)
    print(program.text())
    
    apply_to_exhaustion(program, CPU_EXHAUSTIVE_HEURISTIC.values(), silent=False)
    print(program.text())
    
    x = np.random.uniform(1, 10, (M, N)).astype(np.float32)
    tx = torch.from_numpy(x)
    jx = jax.numpy.asarray(x)
    
    np_runtime = timeit.repeat(lambda: np.arccosh(x), repeat=10, number=1)[3:]
    print(f"Numpy acosh time [ms]: avg {np.mean(np_runtime) * 1e3:.6f} std {np.std(np_runtime) * 1e3:.6f}")
    
    pt_runtime = timeit.repeat(lambda: torch.acosh(tx), repeat=10, number=1)[3:]
    print(f"Pytorch acosh time [ms]: avg {np.mean(pt_runtime) * 1e3:.6f} std {np.std(pt_runtime) * 1e3:.6f}")
    
    jax_runtime = timeit.repeat(lambda: jax.numpy.arccosh(jx), repeat=10, number=1)[3:]
    print(f"JAX acosh time [ms]: avg {np.mean(jax_runtime) * 1e3:.6f} std {np.std(jax_runtime) * 1e3:.6f}")
    
    time = test_native_code(
        program,
        {'x': x, 'z': np.arccosh(x)},
        min_reps=10,
        remove_tmp_files=False,
        silent=False,
    )[3:]
    print(f"My acosh [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")


def run_acos(M, N):
    program_text = f"""
        Name: func_acos
        In: x
        Out: z
        Declarations:
            x f32 [{M}, {N}] heap
            z f32 [{M}, {N}] heap
        Code:
        {M} {N} z[{{0}}, {{1}}] = acosf(x[{{0}}, {{1}}])
    """
    
    program = parse_program(program_text)
    print(program.text())
    
    apply_to_exhaustion(program, CPU_EXHAUSTIVE_HEURISTIC.values(), silent=False)
    print(program.text())
    
    x = np.random.uniform(-1, 1, (M, N)).astype(np.float32)
    tx = torch.from_numpy(x)
    jx = jax.numpy.asarray(x)
    
    np_runtime = timeit.repeat(lambda: np.arccos(x), repeat=10, number=1)[3:]
    print(f"Numpy acos time [ms]: avg {np.mean(np_runtime) * 1e3:.6f} std {np.std(np_runtime) * 1e3:.6f}")
    
    pt_runtime = timeit.repeat(lambda: torch.acos(tx), repeat=10, number=1)[3:]
    print(f"Pytorch acos time [ms]: avg {np.mean(pt_runtime) * 1e3:.6f} std {np.std(pt_runtime) * 1e3:.6f}")
    
    jax_runtime = timeit.repeat(lambda: jax.numpy.arccos(jx), repeat=10, number=1)[3:]
    print(f"JAX acos time [ms]: avg {np.mean(jax_runtime) * 1e3:.6f} std {np.std(jax_runtime) * 1e3:.6f}")
    
    time = test_native_code(
        program,
        {'x': x, 'z': np.arccos(x)},
        min_reps=10,
        remove_tmp_files=False,
        silent=False,
    )[3:]
    print(f"My acos [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")


def run_abs(M, N):
    program_text = f"""
        Name: func_abs
        In: x
        Out: z
        Declarations:
            x f32 [{M}, {N}] heap
            z f32 [{M}, {N}] heap
        Code:
        {M} {N} z[{{0}}, {{1}}] = fabsf(x[{{0}}, {{1}}])
    """
    
    program = parse_program(program_text)
    print(program.text())
    
    apply_to_exhaustion(program, CPU_EXHAUSTIVE_HEURISTIC.values(), silent=False)
    print(program.text())
    
    x = np.random.uniform(-10, 10, (M, N)).astype(np.float32)
    tx = torch.from_numpy(x)
    jx = jax.numpy.asarray(x)
    
    np_runtime = timeit.repeat(lambda: np.abs(x), repeat=10, number=1)[3:]
    print(f"Numpy abs time [ms]: avg {np.mean(np_runtime) * 1e3:.6f} std {np.std(np_runtime) * 1e3:.6f}")
    
    pt_runtime = timeit.repeat(lambda: torch.abs(tx), repeat=10, number=1)[3:]
    print(f"Pytorch abs time [ms]: avg {np.mean(pt_runtime) * 1e3:.6f} std {np.std(pt_runtime) * 1e3:.6f}")
    
    jax_runtime = timeit.repeat(lambda: jax.numpy.abs(jx), repeat=10, number=1)[3:]
    print(f"JAX abs time [ms]: avg {np.mean(jax_runtime) * 1e3:.6f} std {np.std(jax_runtime) * 1e3:.6f}")
    
    time = test_native_code(
        program,
        {'x': x, 'z': np.abs(x)},
        min_reps=10,
        remove_tmp_files=False,
        silent=False,
    )[3:]
    print(f"My abs [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")


def run_log_softmax(M, N):
    program_text = f"""
        Name: func_log_softmax
        In: x
        Out: z
        Declarations:
            x f32 [{M}, {N}] heap
            z f32 [{M}, {N}] heap
            max_val f32 [{M}] heap
            diff f32 [{M}, {N}] heap
            exp_diff f32 [{M}, {N}] heap
            sum_val f32 [{M}] heap
            ln_sum f32 [{M}] heap
        Code:
        {M} {N} max_val[{{0}}] fmaxf= (x[{{0}}, {{1}}])
        {M} {N} diff[{{0}}, {{1}}] = x[{{0}}, {{1}}] - max_val[{{0}}]
        {M} {N} exp_diff[{{0}}, {{1}}] = expf(diff[{{0}}, {{1}}])
        {M} {N} sum_val[{{0}}] += exp_diff[{{0}}, {{1}}]
        {M} ln_sum[{{0}}] = logf(sum_val[{{0}}])
        {M} {N} z[{{0}}, {{1}}] = diff[{{0}}, {{1}}] - ln_sum[{{0}}]
    """
    
    program = parse_program(program_text)
    print(program.text())
    
    apply_to_exhaustion(program, CPU_EXHAUSTIVE_HEURISTIC.values(), silent=False)
    print(program.text())
    
    x = np.random.uniform(-10, 10, (M, N)).astype(np.float32)
    tx = torch.from_numpy(x)
    jx = jax.numpy.asarray(x)
    
    scipy_runtime = timeit.repeat(lambda: scipy.special.log_softmax(x, axis=1), repeat=10, number=1)[3:]
    print(f"Scipy log_softmax time [ms]: avg {np.mean(scipy_runtime) * 1e3:.6f} std {np.std(scipy_runtime) * 1e3:.6f}")
    
    def torch_log_softmax(x):
        max_val, _ = torch.max(x, dim=1, keepdim=True)
        diff = x - max_val
        exp_diff = torch.exp(diff)
        sum_val = torch.sum(exp_diff, dim=1, keepdim=True)
        ln_sum = torch.log(sum_val)
        return diff - ln_sum

    pt_runtime = timeit.repeat(lambda: torch_log_softmax(tx), repeat=10, number=1)[3:]
    print(f"Pytorch log_softmax time [ms]: avg {np.mean(pt_runtime) * 1e3:.6f} std {np.std(pt_runtime) * 1e3:.6f}")
    
    pt_jit_softmax = torch.jit.trace(torch_log_softmax, [tx])
    pt_jit_runtime = timeit.repeat(lambda: pt_jit_softmax(tx), repeat=10, number=1)[3:]
    print(f"Pytorch JIT log_softmax time [ms]: avg {np.mean(pt_jit_runtime) * 1e3:.6f} std {np.std(pt_jit_runtime) * 1e3:.6f}")
    
    pt_builtin_runtime = timeit.repeat(lambda: torch.nn.functional.log_softmax(tx, dim=1), repeat=10, number=1)[3:]
    print(f"Pytorch builtin log_softmax time [ms]: avg {np.mean(pt_builtin_runtime) * 1e3:.6f} std {np.std(pt_builtin_runtime) * 1e3:.6f}")

    def jax_log_softmax(x):
        max_val = jax.numpy.max(x, axis=1, keepdims=True)
        diff = x - max_val
        exp_diff = jax.numpy.exp(diff)
        sum_val = jax.numpy.sum(exp_diff, axis=1, keepdims=True)
        ln_sum = jax.numpy.log(sum_val)
        return diff - ln_sum

    jax_runtime = timeit.repeat(lambda: jax_log_softmax(jx), repeat=10, number=1)[3:]
    print(f"JAX log_softmax time [ms]: avg {np.mean(jax_runtime) * 1e3:.6f} std {np.std(jax_runtime) * 1e3:.6f}")
    
    jax_jit_softmax = jax.jit(jax_log_softmax)
    
    jax_jit_runtime = timeit.repeat(lambda: jax_jit_softmax(jx), repeat=10, number=1)[3:]
    print(f"JAX JIT log_softmax time [ms]: avg {np.mean(jax_jit_runtime) * 1e3:.6f} std {np.std(jax_jit_runtime) * 1e3:.6f}")
    
    jax_builtin_runtime = timeit.repeat(lambda: jax.nn.log_softmax(jx, axis=1), repeat=10, number=1)[3:]
    print(f"JAX builtin log_softmax time [ms]: avg {np.mean(jax_builtin_runtime) * 1e3:.6f} std {np.std(jax_builtin_runtime) * 1e3:.6f}")
    
    time = test_native_code(
        program,
        {'x': x, 'z': scipy.special.log_softmax(x, axis=1)},
        min_reps=10,
        remove_tmp_files=False,
        silent=False,
    )[3:]
    print(f"My log_softmax [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")


def run_softmax(M, N):
    program_text = f"""
        Name: func_softmax
        In: x
        Out: z
        Declarations:
            x f32 [{M}, {N}] heap
            z f32 [{M}, {N}] heap
            max_val f32 [{M}] heap
            diff f32 [{M}, {N}] heap
            exp_diff f32 [{M}, {N}] heap
            sum_val f32 [{M}] heap
        Code:
        {M} {N} max_val[{{0}}] fmaxf= (x[{{0}}, {{1}}])
        {M} {N} diff[{{0}}, {{1}}] = x[{{0}}, {{1}}] - max_val[{{0}}]
        {M} {N} exp_diff[{{0}}, {{1}}] = expf(diff[{{0}}, {{1}}])
        {M} {N} sum_val[{{0}}] += exp_diff[{{0}}, {{1}}]
        {M} {N} z[{{0}}, {{1}}] = exp_diff[{{0}}, {{1}}] / sum_val[{{0}}]
    """
    
    program = parse_program(program_text)
    print(program.text())
    
    # apply_to_exhaustion(program, CPU_EXHAUSTIVE_HEURISTIC.values(), silent=False)
    # print(program.text())
    
    x = np.random.uniform(-10, 10, (M, N)).astype(np.float32)
    tx = torch.from_numpy(x)
    jx = jax.numpy.asarray(x)
    
    scipy_runtime = timeit.repeat(lambda: scipy.special.softmax(x, axis=1), repeat=10, number=1)[3:]
    print(f"Scipy softmax time [ms]: avg {np.mean(scipy_runtime) * 1e3:.6f} std {np.std(scipy_runtime) * 1e3:.6f}")
    
    def torch_softmax(x):
        max_val, _ = torch.max(x, dim=1, keepdim=True)
        diff = x - max_val
        exp_diff = torch.exp(diff)
        sum_val = torch.sum(exp_diff, dim=1, keepdim=True)
        return exp_diff / sum_val

    pt_runtime = timeit.repeat(lambda: torch_softmax(tx), repeat=10, number=1)[3:]
    print(f"Pytorch softmax time [ms]: avg {np.mean(pt_runtime) * 1e3:.6f} std {np.std(pt_runtime) * 1e3:.6f}")
    
    pt_jit_softmax = torch.jit.trace(torch_softmax, [tx])
    pt_jit_runtime = timeit.repeat(lambda: pt_jit_softmax(tx), repeat=10, number=1)[3:]
    print(f"Pytorch JIT softmax time [ms]: avg {np.mean(pt_jit_runtime) * 1e3:.6f} std {np.std(pt_jit_runtime) * 1e3:.6f}")
    
    pt_builtin_runtime = timeit.repeat(lambda: torch.nn.functional.softmax(tx, dim=1), repeat=10, number=1)[3:]
    print(f"Pytorch builtin softmax time [ms]: avg {np.mean(pt_builtin_runtime) * 1e3:.6f} std {np.std(pt_builtin_runtime) * 1e3:.6f}")
    
    def jax_softmax(x):
        max_val = jax.numpy.max(x, axis=1, keepdims=True)
        diff = x - max_val
        exp_diff = jax.numpy.exp(diff)
        sum_val = jax.numpy.sum(exp_diff, axis=1, keepdims=True)
        return exp_diff / sum_val
    
    jax_runtime = timeit.repeat(lambda: jax_softmax(jx), repeat=10, number=1)[3:]
    print(f"JAX softmax time [ms]: avg {np.mean(jax_runtime) * 1e3:.6f} std {np.std(jax_runtime) * 1e3:.6f}")
    
    jax_jit_softmax = jax.jit(jax_softmax)
    jax_jit_runtime = timeit.repeat(lambda: jax_jit_softmax(jx), repeat=10, number=1)[3:]
    print(f"JAX JIT softmax time [ms]: avg {np.mean(jax_jit_runtime) * 1e3:.6f} std {np.std(jax_jit_runtime) * 1e3:.6f}")
    
    jax_builtin_runtime = timeit.repeat(lambda: jax.nn.softmax(jx, axis=1), repeat=10, number=1)[3:]
    print(f"JAX builtin softmax time [ms]: avg {np.mean(jax_builtin_runtime) * 1e3:.6f} std {np.std(jax_builtin_runtime) * 1e3:.6f}")
    
    time = test_native_code(
        program,
        {'x': x, 'z': scipy.special.softmax(x, axis=1)},
        min_reps=10,
        remove_tmp_files=False,
        silent=False,
    )[3:]
    print(f"My softmax [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")


def run_softplus(M, N):
    # softplus(x) = ln(1 + e^{x})
    program_text = f"""
        Name: func_softplus
        In: x
        Out: z
        Declarations:
            x f32 [{M}, {N}] heap
            ex f32 [{M}, {N}] heap
            ex1 f32 [{M}, {N}] heap
            z f32 [{M}, {N}] heap
        Code:
        {M} {N} ex[{{0}}, {{1}}] = expf(x[{{0}}, {{1}}])
        {M} {N} ex1[{{0}}, {{1}}] = ex[{{0}}, {{1}}] + 1
        {M} {N} z[{{0}}, {{1}}] = logf(ex1[{{0}}, {{1}}])
    """
    
    program = parse_program(program_text)
    print(program.text())
    
    apply_to_exhaustion(program, CPU_EXHAUSTIVE_HEURISTIC.values(), silent=False)
    print(program.text())
    
    x = np.random.uniform(-1, 1, (M, N)).astype(np.float32)
    tx = torch.from_numpy(x)
    jx = jax.numpy.asarray(x)
    
    np_runtime = timeit.repeat(lambda: np.log1p(np.exp(x)), repeat=10, number=1)[3:]
    print(f"Numpy softplus time [ms]: avg {np.mean(np_runtime) * 1e3:.6f} std {np.std(np_runtime) * 1e3:.6f}")
    
    def pt_softplus(x):
        return torch.log(1 + torch.exp(x))
    
    pt_runtime = timeit.repeat(lambda: pt_softplus(tx), repeat=10, number=1)[3:]
    print(f"Pytorch softplus time [ms]: avg {np.mean(pt_runtime) * 1e3:.6f} std {np.std(pt_runtime) * 1e3:.6f}")
    
    pt_jit_softplus = torch.jit.trace(pt_softplus, [tx])
    pt_jit_runtime = timeit.repeat(lambda: pt_jit_softplus(tx), repeat=10, number=1)[3:]
    print(f"Pytorch JIT softplus time [ms]: avg {np.mean(pt_jit_runtime) * 1e3:.6f} std {np.std(pt_jit_runtime) * 1e3:.6f}")
    
    pt_builtin_runtime = timeit.repeat(lambda: torch.nn.functional.softplus(tx), repeat=10, number=1)[3:]
    print(f"Pytorch builtin softplus time [ms]: avg {np.mean(pt_builtin_runtime) * 1e3:.6f} std {np.std(pt_builtin_runtime) * 1e3:.6f}")
    
    def jax_softplus(x):
        return jax.numpy.log(1 + jax.numpy.exp(x))
    
    jax_runtime = timeit.repeat(lambda: jax_softplus(jx), repeat=10, number=1)[3:]
    print(f"JAX softplus time [ms]: avg {np.mean(jax_runtime) * 1e3:.6f} std {np.std(jax_runtime) * 1e3:.6f}")
    
    jax_jit_softplus = jax.jit(jax_softplus)
    jax_jit_runtime = timeit.repeat(lambda: jax_jit_softplus(jx), repeat=10, number=1)[3:]
    print(f"JAX JIT softplus time [ms]: avg {np.mean(jax_jit_runtime) * 1e3:.6f} std {np.std(jax_jit_runtime) * 1e3:.6f}")
    
    jax_builtin_runtime = timeit.repeat(lambda: jax.nn.softplus(jx), repeat=10, number=1)[3:]
    print(f"JAX builtin softplus time [ms]: avg {np.mean(jax_builtin_runtime) * 1e3:.6f} std {np.std(jax_builtin_runtime) * 1e3:.6f}")
    
    time = test_native_code(
        program,
        {'x': x, 'z': np.log1p(np.exp(x))},
        min_reps=10,
        remove_tmp_files=False,
        silent=False,
    )[3:]
    print(f"My softplus [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")
    
    
# mish(x) =  x * tanh(softplus(x))
def run_mish(M, N):
    program_text = f"""
        Name: func_mish
        In: x
        Out: z
        Declarations:
            x f32 [{M}, {N}] heap
            ex f32 [{M}, {N}] heap
            ex1 f32 [{M}, {N}] heap
            softplus f32 [{M}, {N}] heap
            tanh f32 [{M}, {N}] heap
            z f32 [{M}, {N}] heap
        Code:
        {M} {N} ex[{{0}}, {{1}}] = expf(x[{{0}}, {{1}}])
        {M} {N} ex1[{{0}}, {{1}}] = ex[{{0}}, {{1}}] + 1
        {M} {N} softplus[{{0}}, {{1}}] = logf(ex1[{{0}}, {{1}}])
        {M} {N} tanh[{{0}}, {{1}}] = tanhf(softplus[{{0}}, {{1}}])
        {M} {N} z[{{0}}, {{1}}] = x[{{0}}, {{1}}] * tanh[{{0}}, {{1}}]
    """
    
    program = parse_program(program_text)
    print(program.text())
    
    apply_to_exhaustion(program, CPU_EXHAUSTIVE_HEURISTIC.values(), silent=False)
    print(program.text())
    
    x = np.random.uniform(-1, 1, (M, N)).astype(np.float32)
    tx = torch.from_numpy(x)
    jx = jax.numpy.asarray(x)
    
    def np_mish(x):
        return x * np.tanh(np.log1p(np.exp(x)))
    
    np_runtime = timeit.repeat(lambda: np_mish(x), repeat=10, number=1)[3:]
    print(f"Numpy mish time [ms]: avg {np.mean(np_runtime) * 1e3:.6f} std {np.std(np_runtime) * 1e3:.6f}")
    
    def pt_mish(x):
        return x * torch.tanh(torch.log(1 + torch.exp(x)))
    
    pt_runtime = timeit.repeat(lambda: pt_mish(tx), repeat=10, number=1)[3:]
    print(f"Pytorch mish time [ms]: avg {np.mean(pt_runtime) * 1e3:.6f} std {np.std(pt_runtime) * 1e3:.6f}")
    
    pt_jit_mish = torch.jit.trace(pt_mish, [tx])
    pt_jit_runtime = timeit.repeat(lambda: pt_jit_mish(tx), repeat=10, number=1)[3:]
    print(f"Pytorch JIT mish time [ms]: avg {np.mean(pt_jit_runtime) * 1e3:.6f} std {np.std(pt_jit_runtime) * 1e3:.6f}")
    
    pt_builtin_runtime = timeit.repeat(lambda: torch.nn.functional.mish(tx), repeat=10, number=1)[3:]
    print(f"Pytorch builtin mish time [ms]: avg {np.mean(pt_builtin_runtime) * 1e3:.6f} std {np.std(pt_builtin_runtime) * 1e3:.6f}")
    
    def jax_mish(x):
        return x * jax.numpy.tanh(jax.numpy.log(1 + jax.numpy.exp(x)))
    
    jax_runtime = timeit.repeat(lambda: jax_mish(jx), repeat=10, number=1)[3:]
    print(f"JAX mish time [ms]: avg {np.mean(jax_runtime) * 1e3:.6f} std {np.std(jax_runtime) * 1e3:.6f}")
    
    jax_jit_mish = jax.jit(jax_mish)
    jax_jit_runtime = timeit.repeat(lambda: jax_jit_mish(jx), repeat=10, number=1)[3:]
    print(f"JAX JIT mish time [ms]: avg {np.mean(jax_jit_runtime) * 1e3:.6f} std {np.std(jax_jit_runtime) * 1e3:.6f}")
    
    jax_builtin_runtime = timeit.repeat(lambda: jax.nn.mish(jx), repeat=10, number=1)[3:]
    print(f"JAX builtin mish time [ms]: avg {np.mean(jax_builtin_runtime) * 1e3:.6f} std {np.std(jax_builtin_runtime) * 1e3:.6f}")
    
    time = test_native_code(
        program,
        {'x': x, 'z': np_mish(x)},
        min_reps=10,
        remove_tmp_files=False,
        silent=False,
    )[3:]
    print(f"My mish [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")
    
    
def run_matmul(M, N, K):
    program_text = f"""
        Name: func_matmul
        In: x, y
        Out: z
        Declarations:
            x f32 [{M}, {K}] heap
            y f32 [{K}, {N}] heap
            z f32 [{M}, {N}] heap
        Code:
        {M} {N} {K} z[{{0}}, {{1}}] += x[{{0}}, {{2}}] * y[{{2}}, {{1}}]
    """
    
    program = parse_program(program_text)
    print(program.text())
    
    apply_to_exhaustion(program, CPU_EXHAUSTIVE_HEURISTIC.values(), silent=False)
    print(program.text())
    
    x = np.random.uniform(-1, 1, (M, K)).astype(np.float32)
    y = np.random.uniform(-1, 1, (K, N)).astype(np.float32)
    tx = torch.from_numpy(x)
    ty = torch.from_numpy(y)
    jx = jax.numpy.asarray(x)
    jy = jax.numpy.asarray(y)
    
    np_runtime = timeit.repeat(lambda: np.matmul(x, y), repeat=10, number=1)[3:]
    print(f"Numpy matmul time [ms]: avg {np.mean(np_runtime) * 1e3:.6f} std {np.std(np_runtime) * 1e3:.6f}")
    
    pt_runtime = timeit.repeat(lambda: torch.matmul(tx, ty), repeat=10, number=1)[3:]
    print(f"Pytorch matmul time [ms]: avg {np.mean(pt_runtime) * 1e3:.6f} std {np.std(pt_runtime) * 1e3:.6f}")
    
    jax_runtime = timeit.repeat(lambda: jax.numpy.matmul(jx, jy), repeat=10, number=1)[3:]
    print(f"JAX matmul time [ms]: avg {np.mean(jax_runtime) * 1e3:.6f} std {np.std(jax_runtime) * 1e3:.6f}")
    
    time = test_native_code(
        program,
        {'x': x, 'y': y, 'z': np.matmul(x, y)},
        min_reps=10,
        remove_tmp_files=False,
        silent=False,
    )[3:]
    print(f"My matmul [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")


def run_gemm(M, N, K):
    program_text = f"""
        Name: func_gemm
        In: a, b, c, alpha, beta
        Out: d
        Declarations:
            a f32 [{M}, {K}] heap
            b f32 [{K}, {N}] heap
            c f32 [{M}, {N}] heap
            d f32 [{M}, {N}] heap
            alpha_acc f32 [{M}, {N}] heap
            alpha f32 [] heap
            beta f32 [] heap
        Code:
        {M} {N} {K} acc[{{0}}, {{1}}] += a[{{0}}, {{2}}] * b[{{2}}, {{1}}]
        {M} {N} alpha_acc[{{0}}, {{1}}] = alpha * acc[{{0}}, {{1}}]
        {M} {N} d[{{0}}, {{1}}] = beta * c[{{0}}, {{1}}] + alpha_acc[{{0}}, {{1}}]
    """
    program = parse_program(program_text)
    print(program.text())
    
    apply_to_exhaustion(program, CPU_EXHAUSTIVE_HEURISTIC.values(), silent=False)
    print(program.text())
    
    a = np.random.uniform(-1, 1, (M, K)).astype(np.float32)
    b = np.random.uniform(-1, 1, (K, N)).astype(np.float32)
    c = np.random.uniform(-1, 1, (M, N)).astype(np.float32)
    
    alpha = 0.15
    beta = 0.7
    
    ta = torch.from_numpy(a)
    tb = torch.from_numpy(b)
    tc = torch.from_numpy(c)
    
    ja = jax.numpy.asarray(a)
    jb = jax.numpy.asarray(b)
    jc = jax.numpy.asarray(c)
    
    np_runtime = timeit.repeat(lambda: np.matmul(a, b) * alpha + c * beta, repeat=10, number=1)[3:]
    print(f"Numpy gemm time [ms]: avg {np.mean(np_runtime) * 1e3:.6f} std {np.std(np_runtime) * 1e3:.6f}")
    
    scipy_runtime = timeit.repeat(lambda: scipy.linalg.blas.sgemm(alpha, a, b, beta, c), repeat=10, number=1)[3:]
    print(f"Scipy gemm time [ms]: avg {np.mean(scipy_runtime) * 1e3:.6f} std {np.std(scipy_runtime) * 1e3:.6f}")
    
    def pt_gemm(a, b, c):
        return beta * c + alpha * torch.matmul(a, b)
    
    pt_runtime = timeit.repeat(lambda: pt_gemm(ta, tb, tc), repeat=10, number=1)[3:]
    print(f"Pytorch gemm time [ms]: avg {np.mean(pt_runtime) * 1e3:.6f} std {np.std(pt_runtime) * 1e3:.6f}")
    
    pt_jit_gemm = torch.jit.trace(pt_gemm, [ta, tb, tc])
    pt_jit_runtime = timeit.repeat(lambda: pt_jit_gemm(ta, tb, tc), repeat=10, number=1)[3:]
    print(f"Pytorch JIT gemm time [ms]: avg {np.mean(pt_jit_runtime) * 1e3:.6f} std {np.std(pt_jit_runtime) * 1e3:.6f}")
    
    def jax_gemm(a, b, c, alpha, beta):
        return alpha * jax.numpy.dot(a, b) + beta * c
    
    jax_runtime = timeit.repeat(lambda: jax_gemm(ja, jb, jc, alpha, beta), repeat=10, number=1)[3:]
    print(f"JAX gemm time [ms]: avg {np.mean(jax_runtime) * 1e3:.6f} std {np.std(jax_runtime) * 1e3:.6f}")
    
    jax_jit_gemm = jax.jit(jax_gemm)
    jax_jit_runtime = timeit.repeat(lambda: jax_jit_gemm(ja, jb, jc, alpha, beta), repeat=10, number=1)[3:]
    print(f"JAX JIT gemm time [ms]: avg {np.mean(jax_jit_runtime) * 1e3:.6f} std {np.std(jax_jit_runtime) * 1e3:.6f}")
    
    time = test_native_code(
        program,
        {'a': a, 'b': b, 'c': c, 'alpha': np.array(alpha, dtype=np.float32), 'beta': np.array(beta, dtype=np.float32), 'd': np.matmul(a, b) * alpha + c * beta},
        min_reps=10,
        remove_tmp_files=False,
        silent=False,
    )[3:]
    print(f"My gemm [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")
    

def run_batchnorm(N, C, H, W):
    epsilon = 1e-4
    program_text = f"""
        Name: func_batchnorm
        In: x, mean, var, scale, bias, momentum
        Out: y, running_mean, running_var
        Declarations:
            x f32 [{N}, {C}, {H}, {W}] heap
            y f32 [{N}, {C}, {H}, {W}] heap
            mean f32 [{C}] heap
            var f32 [{C}] heap
            scale f32 [{C}] heap
            bias f32 [{C}] heap
            momentum f32 [] heap
            one_momentum f32 [] heap
            current_sum f32 [{C}] heap
            current_mean f32 [{C}] heap
            diff f32 [{N}, {C}, {H}, {W}] heap
            diff2_sum f32 [{C}] heap
            current_var_biased f32 [{C}] heap
            current_var f32 [{C}] heap
            running_mean_l f32 [{C}] heap
            running_mean_r f32 [{C}] heap
            running_mean f32 [{C}] heap
            running_var_l f32 [{C}] heap
            running_var_r f32 [{C}] heap
            running_var f32 [{C}] heap
            var_eps f32 [{C}] heap
            sqrt_var_eps f32 [{C}] heap
            alpha f32 [{C}] heap
            beta f32 [{C}] heap
        Code:
        one_momentum = 1 - momentum
        {N} {C} {H} {W} current_sum[{{1}}] += x[{{0}}, {{1}}, {{2}}, {{3}}]
        {C} current_mean[{{0}}] = current_sum[{{0}}] / ({N * H * W})
        {N} {C} {H} {W} diff[{{0}}, {{1}}, {{2}}, {{3}}] = x[{{0}}, {{1}}, {{2}}, {{3}}] - current_mean[{{1}}]
        {N} {C} {H} {W} diff2_sum[{{1}}] += diff[{{0}}, {{1}}, {{2}}, {{3}}] * diff[{{0}}, {{1}}, {{2}}, {{3}}]
        {C} current_var_biased[{{0}}] = diff2_sum[{{0}}] / ({N * H * W})
        {C} current_var[{{0}}] = diff2_sum[{{0}}] / ({N * H * W - 1})
        {C} running_mean_l[{{0}}] = momentum * mean[{{0}}]
        {C} running_mean_r[{{0}}] = one_momentum * current_mean[{{0}}]
        {C} running_mean[{{0}}] = running_mean_l[{{0}}] + running_mean_r[{{0}}]
        {C} running_var_l[{{0}}] = momentum * var[{{0}}] 
        {C} running_var_r[{{0}}] = one_momentum * current_var[{{0}}]
        {C} running_var[{{0}}] = running_var_l[{{0}}] + running_var_r[{{0}}]
        {C} var_eps[{{0}}] = current_var_biased[{{0}}] + {epsilon}
        {C} sqrt_var_eps[{{0}}] = sqrtf(var_eps[{{0}}])
        {C} alpha[{{0}}] = scale[{{0}}] / sqrt_var_eps[{{0}}]
        {C} beta[{{0}}] = - alpha[{{0}}] * current_mean[{{0}}] + bias[{{0}}]
        {N} {C} {H} {W} y[{{0}}, {{1}}, {{2}}, {{3}}] = alpha[{{1}}] * x[{{0}}, {{1}}, {{2}}, {{3}}] + beta[{{1}}]
    """
    
    program = parse_program(program_text)
    print(program.text())
    
    apply_to_exhaustion(program, CPU_EXHAUSTIVE_HEURISTIC.values(), silent=False)
    print(program.text())
    
    momentum = 0.9
    x = np.random.uniform(-1, 1, (N, C, H, W)).astype(np.float32)
    scale = np.random.uniform(-1, 1, (C,)).astype(np.float32)
    bias = np.random.uniform(-1, 1, (C,)).astype(np.float32)
    mean = np.random.uniform(-1, 1, (C,)).astype(np.float32)
    var = np.random.uniform(-1, 1, (C,)).astype(np.float32)
    
    tx = torch.from_numpy(x)
    tscale = torch.from_numpy(scale)
    tbias = torch.from_numpy(bias)
    tmean = torch.from_numpy(mean)
    tvar = torch.from_numpy(var)
    
    ref_running_mean = tmean.clone().detach()
    ref_running_var = tvar.clone().detach()
    ty = torch.nn.functional.batch_norm(tx, ref_running_mean, ref_running_var, weight=tscale, bias=tbias, training=True, momentum=1-momentum, eps=epsilon)
    
    tm1 = tmean.clone().detach()
    tv1 = tvar.clone().detach()
    
    pt_runtime = timeit.repeat(lambda: torch.nn.functional.batch_norm(tx, tm1, tv1, weight=tscale, bias=tbias, training=True, momentum=1-momentum, eps=epsilon), repeat=10, number=1)[3:]
    print(f"Pytorch batchnorm time [ms]: avg {np.mean(pt_runtime) * 1e3:.6f} std {np.std(pt_runtime) * 1e3:.6f}")
    
    time = test_native_code(
        program,
        {'x': x, 'mean': mean, 'var': var, 'scale': scale, 'bias': bias, 'momentum': np.array(momentum, dtype=np.float32), 'y': ty.numpy(),
         'running_mean': ref_running_mean.numpy(), 'running_var': ref_running_var.numpy()},
        min_reps=10,
        remove_tmp_files=False,
        silent=False,
    )[3:]
    print(f"My batchnorm [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")


def run_layernorm(M, N):
    program_text = f"""
        Name: layernorm
        In: src, gamma, beta
        Out: dst
        Declarations:
            src f32 [{M}, {N}] heap
            gamma f32 [{N}] heap
            beta f32 [{N}] heap
            dst f32 [{M}, {N}] heap
            m f32 [{M}] heap
            mu f32 [{M}] heap
            diff f32 [{M}, {N}] heap
            q f32 [{M}] heap
            tmp f32 [{M}, {N}] heap
            qeps f32 [{M}] heap
            sigma f32 [{M}] heap
        Code:
        {M} {N} m[{{0}}] += src[{{0}}, {{1}}]
        {M} mu[{{0}}] = m[{{0}}] / {N}
        {M} {N} diff[{{0}}, {{1}}] = src[{{0}}, {{1}}] - mu[{{0}}]
        {M} {N} q[{{0}}] += diff[{{0}}, {{1}}] * diff[{{0}}, {{1}}]
        {M} {N} tmp[{{0}}, {{1}}] = diff[{{0}}, {{1}}] * gamma[{{1}}]
        {M} qeps[{{0}}] = q[{{0}}] * {1./(N - 1.)} + 0.00001
        {M} sigma[{{0}}] = 1 / sqrtf(qeps[{{0}}])
        {M} {N} dst[{{0}}, {{1}}] = sigma[{{0}}] * tmp[{{0}}, {{1}}] + beta[{{1}}]
    """
    
    program = parse_program(program_text)
    print(program.text())
    
    apply_to_exhaustion(program, CPU_EXHAUSTIVE_HEURISTIC.values(), silent=False)
    print(program.text())
    
    src = np.random.uniform(-1, 1, (M, N)).astype(np.float32)
    gamma = np.random.uniform(-1, 1, (N,)).astype(np.float32)
    beta = np.random.uniform(-1, 1, (N,)).astype(np.float32)
    
    tsrc = torch.from_numpy(src)
    tgamma  = torch.from_numpy(gamma)
    tbeta = torch.from_numpy(beta)
    
    pt_runtime = timeit.repeat(lambda: torch.nn.functional.layer_norm(tsrc, [N], weight=tgamma, bias=tbeta, eps=1e-5), repeat=10, number=1)[3:]
    print(f"Pytorch layernorm time [ms]: avg {np.mean(pt_runtime) * 1e3:.6f} std {np.std(pt_runtime) * 1e3:.6f}")
    
    
    tdst = torch.nn.functional.layer_norm(tsrc, [N], weight=tgamma, bias=tbeta, eps=1e-5)
    dst = tdst.numpy()
    
    time = test_native_code(
        program,
        {'src': src, 'gamma': gamma, 'beta': beta, 'dst': dst},
        min_reps=10,
        remove_tmp_files=False,
        silent=False,
    )[3:]
    print(f"My layernorm [ms]: avg {np.mean(time) * 1e3:.6f} std {np.std(time) * 1e3:.6f}")


if __name__ == '__main__':
    M = 4096
    N = 4096
    run_sigmoid(M, N)
    run_leakyrelu(M, N)
    run_celu(M, N)
    run_relu(M, N)
    run_pow(M, N)
    run_sqrt(M, N)
    run_tanh(M, N)
    run_log(M, N)
    run_div(M, N)
    run_mul(M, N)
    run_sub(M, N)
    run_sum(M, N)
    run_cosh(M, N)
    run_exp(M, N)
    run_reducemean(M, N)
    run_argmin(M, N)
    run_argmax(M, N)
    run_conv(N=8, M=10, C=3, H_in=512, W_in=512, K=5)
    run_clip(M, N)
    run_blackman_window(2**14)
    run_cos(M, N)
    run_floor(M, N)
    run_ceil(M, N)
    run_bitwise_xor(M, N)
    run_bitwise_or(M, N)
    run_bitwise_and(M, N)
    run_bitwise_not(M, N)
    run_bitshift_left(M, N)
    run_averagepool(N=8, C=3, H_in=512, W_in=512, K=5)
    run_atanh(M, N)
    run_atan(M, N)
    run_asinh(M, N)
    run_asin(M, N)
    run_and(M, N)
    run_affine_grid(64, 256, 256)
    run_add(M, N)
    run_acosh(M, N)
    run_acos(M, N)
    run_abs(M, N)
    run_log_softmax(M, N)
    run_softmax(M, N)
    run_softplus(M, N)
    run_mish(M, N)
    run_matmul(1024, 1024, 1024)
    run_gemm(1024, 1024, 1024)
    run_batchnorm(N=16, C=4, H=256, W=256)
    run_layernorm(4096, 4096)

