from kelgen.measurements import RuntimeMeasurement, MeasurementStatus
import math
import os
from kelgen.gen import test_code
import numpy as np
import subprocess


def eval_program(new_program, ref_data, min_repeats, min_seconds, timeout_ms, cache, verify, reinit_input, target):
    warmup = math.ceil(0.3 * min_repeats)
    new_program_hash = new_program.program_hash()
    measurement = RuntimeMeasurement.not_found() if cache is None else cache[new_program_hash]
    if (measurement.valid() or (measurement.timed_out() and timeout_ms <= measurement.mean)):
        # use cached result
        runtime_ms = measurement.mean
        runtime_std = measurement.std
        status = MeasurementStatus.FROM_CACHE
    else:
        keep_tmp = os.environ.get('KEEP_TMP', False)
        verbose = os.environ.get('VERBOSE', False)
        new_program_text = new_program.text()
        try:
            time = test_code(
                new_program,
                ref_data,
                min_reps=warmup+min_repeats,
                min_seconds=min_seconds,
                remove_tmp_files=not keep_tmp,
                check_correctness=verify,
                silent=not verbose,
                timeout=timeout_ms / 1e3,
                reinit_input=reinit_input,
                target=target,
            )[warmup:]
            runtime_ms = np.mean(time) * 1e3
            runtime_std = np.std(time) * 1e3
            if cache is not None:
                cache[new_program_hash] = RuntimeMeasurement(new_program_text, runtime_ms, runtime_std, False)
            status = MeasurementStatus.MEASURED
        except subprocess.TimeoutExpired as e:
            runtime_ms = timeout_ms
            runtime_std = 0.0
            if cache is not None:
                cache[new_program_hash] = RuntimeMeasurement(new_program_text, runtime_ms, runtime_std, True)
            status = MeasurementStatus.TIMED_OUT
        except RuntimeError as e:
            runtime_ms = float('inf')
            runtime_std = 0.0
            status = MeasurementStatus.RUNTIME_ERROR
    return status, runtime_ms, runtime_std


class Evaluator:
    def __init__(self, ref_data, min_repeats, min_seconds, timeout_ms, cache, timeout_adjust, verify, reinit_input, target='native'):
        self.ref_data = ref_data
        self.min_repeats = min_repeats
        self.min_seconds = min_seconds
        self.timeout_ms = timeout_ms
        self.cache = cache
        self.timeout_adjust = timeout_adjust
        self.verify = verify
        self.reinit_input = reinit_input
        self.target = target
    
    def eval(self, program):
        status, runtime_ms, runtime_std = eval_program(program, self.ref_data, self.min_repeats, self.min_seconds, self.timeout_ms, self.cache, self.verify, self.reinit_input, self.target)
        if self.timeout_adjust and 10 * runtime_ms < self.timeout_ms:
            self.timeout_ms = 10 * runtime_ms
        return status, runtime_ms, runtime_std


def eval_once(eval_class, input_str, transformations = lambda p: None, target='native'):
    kernel_params = eval_class.parse_params(input_str)
    canonicalized_param_str = 'x'.join(str(p) for p in kernel_params.values())
    base_kernel = eval_class.create_kernel(kernel_params)
    transformations(base_kernel)
    print(base_kernel.text())
    ref_data = eval_class.create_reference_data(kernel_params)
    ref_mean, ref_std, ref_times = eval_class.evaluate_baseline(ref_data, min_repeats=3, target=target)
    print(f"Pytorch {base_kernel.name} size {canonicalized_param_str} time [ms]: avg {ref_mean * 1e3:.6f} std {ref_std * 1e3:.6f} reps {len(ref_times)}")
    # cache = RuntimeCache(f'cache-{base_kernel.name}-{canonicalized_param_str}.db')
    cache = None
    evaluator = Evaluator(
        ref_data, min_repeats=3, min_seconds=1.0, timeout_ms=1000 * ref_mean * 1e3, cache=cache, timeout_adjust=False, verify=True, reinit_input=True, target=target
    )
    status, runtime_ms, runtime_std = evaluator.eval(base_kernel)
    print(f"Eval {base_kernel.name} size {canonicalized_param_str} status: {status}, runtime [ms]: avg {runtime_ms:.6f} std {runtime_std:.6f}")