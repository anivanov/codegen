from kernels import *

import hashlib
import torch
import json


TRANSFORMATIONS_LIST = {
    "tile_scope": (tile_scope, find_tileable_scopes),
    "untile_scope": (untile_scope, find_untileable_scopes),
    "tile_buffer": (tile_buffer, find_tileable_buffers),
    "untile_buffer": (untile_buffer, find_untileable_buffers),
    "create_dimension": (create_dimension, find_creatable_dimensions),
    "delete_dimension": (delete_dimension, find_deletable_dimensions),
    "swap_nested_scopes": (swap_nested_scopes, find_swappable_nested_scopes),
    "create_temporary": (create_temporary, find_creatable_temporaries),
    "delete_temporary": (delete_temporary, find_deletable_temporaries),
    "swap_ops": (swap_ops, find_swappable_ops),
    "swap_buffer_dims": (swap_buffer_dims, find_swappable_buffer_dims),
    "join_scopes": (join_scopes, find_joinable_scopes),
    "split_scopes": (split_scopes, find_splittable_scopes),
    "reuse_arr_dims": (reuse_arr_dims, find_reusable_arr_dims),
    "unroll_scope": (unroll_scope, find_unrollable_scopes),
    "reuse_buffers": (reuse_buffers, find_reusable_buffers),
    "move_buf_to_stack": (move_buf_to_stack, find_bufs_movable_to_stack),
    "parallelize_scope": (parallelize_scope, find_parallelizable_scopes),
}


def run_matmul(M, N, K):
    program_text = f"""
        Name: func_matmul
        In: x, y
        Out: z
        Declarations:
            x f32 [{M}, {K}] heap
            y f32 [{K}, {N}] heap
            z f32 [{M}, {N}] heap
        Code:
        {M} {N} {K} z[{{0}}, {{1}}] += x[{{0}}, {{2}}] * y[{{2}}, {{1}}]
    """
    
    program = parse_program(program_text)
    print(program.text())
    
    x = np.random.uniform(-1, 1, (M, K)).astype(np.float32)
    y = np.random.uniform(-1, 1, (K, N)).astype(np.float32)
    tx = torch.from_numpy(x)
    ty = torch.from_numpy(y)
    tz = torch.matmul(tx, ty)
    z = tz.numpy()

    pt_runtime = timeit.repeat(lambda: torch.matmul(tx, ty), repeat=10, number=1)[3:]
    print(f"Pytorch matmul time [ms]: avg {np.mean(pt_runtime) * 1e3:.6f} std {np.std(pt_runtime) * 1e3:.6f}")
    
    beam_size = 3
    seen_programs = {}  # hash -> cost
    
    cache_name = "cached_results.json"
    if os.path.exists(cache_name):
        with open(cache_name, "r") as f:
            cached_results = json.load(f)
    else:
        cached_results = {}
    
    time = test_native_code(
        program,
        {'x': x, 'y': y, 'z': z},
        min_reps=10,
        remove_tmp_files=False,
        silent=False,
    )[3:]
    cost = np.max(time) * 1e3
    current_beam = [(program, [], cost)]
    print(f"Initial cost: {cost:.6f}")
    best_cost = cost
    
    while current_beam:
        new_candidates = []
        for program_instance, transform_path, current_cost in current_beam:
            for tname, (tapply, tcheck) in TRANSFORMATIONS_LIST.items():
                for targs in tcheck(program_instance):
                    new_program = copy.deepcopy(program_instance)
                    tapply(new_program, *targs)
                    
                    # break cycles
                    new_program_hash = str(hashlib.sha1(new_program.text().encode("utf-8")).hexdigest())
                    if new_program_hash in seen_programs:
                        print("skipping already seen program")
                        continue
                    
                    print("Transformations: ", end="")
                    for tname_path, *targs_path in transform_path:
                        print(f"{tname_path}(program, {args_as_str(targs_path)}); ", end="")
                    print(f"{tname}(program, {args_as_str(targs)})")
                    print(new_program.text())
                    
                    if new_program_hash in cached_results:
                        # use cached result
                        cost = cached_results[new_program_hash]
                        print(f"Cost (from cache): {cost:.6f}")
                    else:
                        max_cost = 2 * best_cost
                        try:
                            time = test_native_code(
                                program,
                                {'x': x, 'y': y, 'z': z},
                                min_reps=10,
                                min_seconds=0.1,
                                remove_tmp_files=False,
                                silent=False,
                                timeout=max_cost / 1e3,
                            )[3:]
                            cost = np.max(time) * 1e3
                            print(f"Cost (measured): {cost:.6f}")
                        except subprocess.TimeoutExpired as e:
                            cost = max_cost
                            print(f"Cost (timed out): {cost:.6f}")
                        
                        cached_results[new_program_hash] = cost
                        with open(cache_name, "w") as f:
                            json.dump(cached_results, f)
                    
                    seen_programs[new_program_hash] = cost
                    
                    if cost < 0.98 * best_cost:
                        best_cost = cost
                        print(f"Cost improved. Old: {current_cost} -> New: {cost}")
                    
                    if cost < 1.05 * best_cost:
                        new_path = transform_path + [(tname, *targs)]
                        new_candidates.append((new_program, new_path, cost))
        
        new_candidates = sorted(new_candidates, key=lambda x: x[2])
        print(f"Beam search: {len(new_candidates)} new candidates " +', '.join([str(c) for _, _, c in new_candidates]))
        print(f"Current best cost: {best_cost:.6f}")
        new_candidates = new_candidates[:beam_size]
        current_beam = new_candidates
    
    print("Beam search exhausted.")

    
if __name__ == "__main__":
    run_matmul(128, 128, 128)