from typing import List, Optional, Dict, Tuple
import torch
import numpy as np


class LlmModel(object):
    """
    A class that represents the LLM model that encodes the
    microkernel and action into embedding vectors.
    TODO Change the embedding sizes to match the actual sizes
    """
    kernel_embedding_size: int = 2048
    action_embedding_size: int = 2048
    def __init__(self) -> None:
        pass

    
    def encode_kernel(self, kernel: str) -> torch.Tensor:
        """
        Encodes the given string representation of the 
        microkernel into an embedding vector
        """
        pass

    def encode_transformation(self, transformation: str) -> torch.Tensor:
        """
        Encodes the given string representation of the 
        transformation into an embedding vector
        """
        pass
    
    def encode_transformations(self, transformations: List[str]) -> torch.Tensor:
        """
        Encodes the given list of string representations of the 
        transformations into a batch of embedding vectors
        """
        pass


class DummyModel(LlmModel):
    """
    A dummy implementation of the LLM model that returns random
    embeddings for the kernel and transformations.
    """

    def __init__(self) -> None:
        super(DummyModel, self).__init__()
    
    def encode_kernel(self, kernel: str) -> torch.Tensor:
        return torch.tensor(np.random.rand(self.kernel_embedding_size))
    
    def encode_transformation(self, transformation: str) -> torch.Tensor:
        return torch.tensor(np.random.rand(self.action_embedding_size))

    def encode_transformations(self, transformations: List[str]) -> torch.Tensor:
        return torch.tensor(np.random.rand(len(transformations), self.action_embedding_size))


