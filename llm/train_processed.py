import json
import torch
from transformers import DistilBertTokenizer, DistilBertModel
from transformers import AutoTokenizer
import transformers
from torch import nn
import wandb
import time
from tqdm import tqdm
from torch.utils.data import DataLoader, Dataset, random_split
import os
import pandas as pd
import random
import numpy as np
import re

random.seed(0)
np.random.seed(0)
torch.manual_seed(0)

# wandb.init(mode='disabled')
wandb.init(project='LLM-predict')
wandb.run.log_code(".")

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


processed_path = 'datasets/dataset_embedding.pt'

dict_data = torch.load(processed_path, weights_only=False)  # {code: (runtime, kernel_name, transforms, emb)}

for code, (runtime, kernel_name, transforms, emb) in dict_data.items():
    print(f"code: {code}, runtime: {runtime}, kernel_name: {kernel_name}, transforms: {transforms}, emb: {emb}")
    [embedding_size] = emb.shape  # 4096 for CodeLlama-7b  
    break

# group by kernel_name
kernel_name_to_data = {}  # {(kernel_name, kernel_shape): [(code, runtime, transforms, emb)]}
for code, (runtime, kernel_name, transforms, emb) in dict_data.items():
    # In: gamma, x
    # Out: y
    inputs, outputs = re.search(r'\nIn: (.*)\nOut: (.*)\n', code).groups()
    external = inputs.split(', ') # + outputs.split(', ')  # I am including output shapes because of bug in transformations that allowed output buffer tiling
    # gamma f32 [1024] heap
    shapes = []
    for x in external:
        s = re.search(f'{x} .+ \[(.+)\] heap', code)
        if s is None:
            # y0 f32 [16384, 1024] heap -> tmp, y, y0
            s = re.search(f' .+ \[(.+)\] heap ->.* {x}[,\n]', code)
        [shape] = s.groups()
        shapes.append(shape)
    shapes = tuple(shapes)
    kernel_name_to_data.setdefault((kernel_name, shapes), []).append((code, runtime, transforms, emb))

print("Kernels with unique shapes: ", len(kernel_name_to_data))
for (k, s), v in kernel_name_to_data.items():
    print(f"kernel_name: {k} kernel_shape: {s}, num_data: {len(v)}")
    
# now extend the dataset to include pairwise speedups and speedups relative to baseline
for k, v in kernel_name_to_data.items():
    baseline_runtime = None
    v1 = v[:]
    random.shuffle(v1)
    for i, (d, d1) in enumerate(zip(v, v1)):
        code, runtime, transforms, emb = d
        code1, runtime1, transforms1, emb1 = d1
        if i == 0:
            baseline_runtime = runtime
            # print("baseline program:")
            # print(code)
            # print("----------------")
        speedup_over_baseline = baseline_runtime / runtime
        speedup_over_other = runtime1 / runtime
        kernel_name_to_data[k][i] = (code, runtime, transforms, emb, code1, runtime1, transforms1, emb1, speedup_over_baseline, speedup_over_other)

class CodeRuntimeDataset(Dataset):
    def __init__(self, data):
        self.data = data

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        code, runtime, transforms, emb, code1, runtime1, transforms1, emb1, speedup_over_baseline, speedup_over_other = self.data[idx]
        return emb, emb1, runtime, speedup_over_baseline, speedup_over_other


# Define a simple regression layer
class RegressionModel(nn.Module):

    def __init__(self, input_size, output_size):
        super(RegressionModel, self).__init__()
        # Define the layers
        layers = []
        in_features = input_size

        # Add hidden layers
        hidden_sizes = [512, 256, 128, 64]
        # hidden_sizes = [4096, 2048, 1024, 512, 128, 64, 1]
        for hidden_size in hidden_sizes:
            layers.append(nn.Linear(in_features, hidden_size))
            layers.append(nn.ReLU())  # Activation function
            in_features = hidden_size

        # Output layer
        layers.append(nn.Linear(in_features, output_size))

        # Combine into a sequential model
        self.model = nn.Sequential(*layers)

    def forward(self, x):
        return self.model(x)


regression_model = RegressionModel(input_size=2*embedding_size, output_size=1)
regression_model.train()
regression_model.to(device)

class MAPELoss(nn.Module):
  def __init__(self, epsilon=1e-8):
    super(MAPELoss, self).__init__()
    self.epsilon = epsilon

  def forward(self, output, target):
    absolute_percentage_errors = torch.abs((target - output) / (target + self.epsilon))
    mape = torch.mean(absolute_percentage_errors) * 100
    return mape

# criterion = nn.MSELoss()
criterion = MAPELoss()

criterion_mse = nn.MSELoss()
criterion_mape = MAPELoss()

# optimizer = torch.optim.Adam(regression_model.parameters(), lr=0.00001)
optimizer = torch.optim.Adam(regression_model.parameters(), lr=0.0001)

# processed_dataset = CodeRuntimeDataset(processed_data)
# train_dataset, test_dataset = random_split(processed_dataset, [0.9, 0.1], torch.Generator().manual_seed(42))

train_data = []
test_data = []
for (name, shape), v in kernel_name_to_data.items():
    # for x in v:
    #     if random.random() < 0.1:
    #         test_data.append(x)
    #     else:
    #         train_data.append(x)
    if name in ('softmax', 'batched_matmul'):
        test_data.extend(v)
    else:
        train_data.extend(v)

assert len(train_data) > 0
assert len(test_data) > 0

train_dataset = CodeRuntimeDataset(train_data)
test_dataset = CodeRuntimeDataset(test_data)

train_batch = 1024

train_dataloader = DataLoader(train_dataset, batch_size=train_batch, shuffle=True)
test_dataloader = DataLoader(test_dataset, batch_size=train_batch, shuffle=False)

num_epochs = 1000000
for epoch in range(num_epochs):
    regression_model.train()
    epoch_train_loss = 0
    for batch in train_dataloader:

        emb, emb1, runtime, speedup_over_baseline, speedup_over_other = batch
        
        # inp = emb.to(device=device).to(dtype=torch.float32)
        # target = speedup_over_baseline.to(device).to(dtype=torch.float32).unsqueeze(-1)
        
        # consider pairwise speedups
        inp = torch.cat((emb,  emb1), dim=1).to(device=device).to(dtype=torch.float32)
        target = speedup_over_other.unsqueeze(-1).to(device=device).to(dtype=torch.float32)
        
        output = regression_model(inp)

        loss = criterion(output, target)

        # Backward pass
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        epoch_train_loss += loss.item()
    epoch_train_loss /= len(train_dataloader)
    
    # validation loss
    regression_model.eval()
    epoch_test_mse_loss = 0
    epoch_test_mape_loss = 0
    with torch.no_grad():
        for batch in test_dataloader:
            emb, emb1, runtime, speedup_over_baseline, speedup_over_other = batch
            # inp = emb.to(device=device).to(dtype=torch.float32)
            # target = speedup_over_baseline.to(device).to(dtype=torch.float32).unsqueeze(-1)
            
            # consider pairwise speedups
            inp = torch.cat((emb, emb1), dim=1).to(device=device).to(dtype=torch.float32)
            target = speedup_over_other.unsqueeze(-1).to(device=device).to(dtype=torch.float32)

            output = regression_model(inp)

            loss_mse = criterion_mse(output, target)
            loss_mape = criterion_mape(output, target)
            epoch_test_mse_loss += loss_mse.item()
            epoch_test_mape_loss += loss_mape.item()
    epoch_test_mse_loss /= len(test_dataloader)
    epoch_test_mape_loss /= len(test_dataloader)

    # Calculate epoch time
    # Log epoch loss to wandb
    wandb.log({
        "train_loss": epoch_train_loss,
        "test_mse_loss": epoch_test_mse_loss,
        "test_mape_loss": epoch_test_mape_loss
    }) 

    print(f"Epoch {epoch + 1}, Train loss: {epoch_train_loss} Test mse loss: {epoch_test_mse_loss} Test mape loss: {epoch_test_mape_loss}")

# Finish wandb run
wandb.finish()
