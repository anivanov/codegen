import json
import torch
from transformers import DistilBertTokenizer, DistilBertModel
from transformers import AutoTokenizer
import transformers
from torch import nn
import wandb
import time
from tqdm import tqdm
from torch.utils.data import DataLoader, Dataset, random_split
import os
import pandas as pd
import random

dataset_path = 'datasets/dataset.json'
processed_path = 'datasets/dataset_embedding.pt'

with open(dataset_path, 'r') as f:
    dataset = json.load(f)

if os.path.exists(processed_path):
    processed_dataset = torch.load(processed_path)
else:
    processed_dataset = {}

model_name = "meta-llama/CodeLlama-7b-hf"
tokenizer = AutoTokenizer.from_pretrained(model_name)
pipeline = transformers.pipeline("feature-extraction", model=model_name, torch_dtype=torch.float16, device_map="auto", framework="pt")
model = pipeline.model
preprocess_batch = 4
# padding = False
padding = True
tokenizer.pad_token = tokenizer.eos_token
tokenizer.truncation_side='left'
trunc_length = 4096
prompt_prefix = ""

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

dataset_list = [(code, runtime, kernel_name, transforms) for code, (runtime, kernel_name, transforms) in dataset.items()]
print("Already processed:", len(processed_dataset))
print("Total:", len(dataset))

with torch.no_grad():
    unsaved_batches = 0
    for idx in tqdm(range(len(processed_dataset), len(dataset), preprocess_batch)):
        batch = dataset_list[idx:idx+preprocess_batch]
        codes = [c for c, _, _, _ in batch]
        inputs = tokenizer(list((prompt_prefix + c) for c in codes), return_tensors="pt", padding=padding, truncation=True, max_length=trunc_length)
        if inputs['attention_mask'].shape[1] == trunc_length:
            print('Warning: truncation happened')
        inputs = {key: val.to(device) for key, val in inputs.items()}
        outputs = model(**inputs)
        weights_for_non_padding = inputs['attention_mask'] * torch.arange(start=1, end=outputs.last_hidden_state.shape[1] + 1).unsqueeze(0).to(device)
        sum_embeddings = torch.sum(outputs.last_hidden_state.to(dtype=torch.float32) * weights_for_non_padding.unsqueeze(-1), dim=1)
        num_of_none_padding_tokens = torch.sum(weights_for_non_padding, dim=-1).unsqueeze(-1)
        embeddings = sum_embeddings / num_of_none_padding_tokens
        
        for (code, runtime, kernel_name, transforms), emb in zip(batch, embeddings):
            processed_dataset[code] = (runtime, kernel_name, transforms, emb)
        unsaved_batches += 1
        save_step = 1000
        if unsaved_batches == save_step or idx + preprocess_batch >= len(dataset):
            unsaved_batches = 0
            # saving 
            torch.save(processed_dataset, processed_path)
