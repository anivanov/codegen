import tvm
from tvm import te
import numpy as np
import timeit
import argparse
import os
import time


class layernormData:
    @staticmethod
    def ref_inputs(M, N):
        X_np = np.random.rand(M, N).astype("float32")
        gamma_np = np.random.rand(N).astype("float32")
        beta_np = np.random.rand(N).astype("float32")
        Y_np = __class__.ref_kernel(X_np, gamma_np, beta_np)
        return [X_np, gamma_np, beta_np], [Y_np]

    @staticmethod
    def ref_tvm_inputs(x_np, gamma_np, beta_np, Y_np):
        device = tvm.cpu()
        X_tvm = tvm.nd.array(x_np, device)
        gamma_tvm = tvm.nd.array(gamma_np, device)
        beta_tvm = tvm.nd.array(beta_np, device)
        Y_tvm = tvm.nd.empty(Y_np.shape, device=device)
        return [X_tvm, gamma_tvm, beta_tvm], [Y_tvm]

    @staticmethod
    def ref_kernel(X, gamma, beta):
        eps = 1e-5
        mean = np.mean(X, axis=1)
        variance = np.var(X, axis=1, ddof=1)
        X_norm = (X - mean[:, None]) / np.sqrt(variance[:, None] + eps)
        Y = X_norm * gamma[None, :] + beta[None, :]
        return Y


@tvm.auto_scheduler.register_workload  # Note the auto_scheduler decorator
def layernorm(M, N):
    eps = 1e-5

    X = te.placeholder((M, N), name="X", dtype="float32")
    gamma = te.placeholder((N,), name="gamma", dtype="float32")
    beta = te.placeholder((N,), name="beta", dtype="float32")

    n = te.reduce_axis((0, N), name="n")  # inner reduction axis
    mean_sum = te.compute((M,), lambda m: te.sum(X[m, n], axis=n), name="mean_sum")
    mean = te.compute((M,), lambda m: mean_sum[m] / N, name="mean")

    squared_diff = te.compute((M, N), lambda m, n: tvm.te.power((X[m, n] - mean[m]), 2), name="squared_diff")

    n2 = te.reduce_axis((0, N), name="n2")
    variance_sum = te.compute((M,), lambda m: te.sum(squared_diff[m, n2], axis=n2), name="variance_sum")
    variance = te.compute((M,), lambda m: variance_sum[m] / (N - 1), name="variance")

    X_norm = te.compute((M, N), lambda m, n: (X[m, n] - mean[m]) / te.sqrt(variance[m] + eps), name="X_norm")

    Y = te.compute((M, N), lambda m, n: X_norm[m, n] * gamma[n] + beta[n], name="Y")

    return [X, gamma, beta, Y]


class relu_ffnData:
    @staticmethod
    def ref_inputs(B, S, E, H):
        X_np = np.random.rand(B, S, E).astype("float32")
        W1_np = np.random.rand(E, H).astype("float32")
        B1_np = np.random.rand(H).astype("float32")
        W2_np = np.random.rand(H, E).astype("float32")
        B2_np = np.random.rand(E).astype("float32")
        Y_np = __class__.ref_kernel(X_np, W1_np, B1_np, W2_np, B2_np)
        return [X_np, W1_np, B1_np, W2_np, B2_np], [Y_np]
    
    @staticmethod
    def ref_tvm_inputs(x_np, w1_np, b1_np, w2_np, b2_np, Y_np):
        device = tvm.cpu()
        X_tvm = tvm.nd.array(x_np, device)
        W1_tvm = tvm.nd.array(w1_np, device)
        B1_tvm = tvm.nd.array(b1_np, device)
        W2_tvm = tvm.nd.array(w2_np, device)
        B2_tvm = tvm.nd.array(b2_np, device)
        Y_tvm = tvm.nd.empty(Y_np.shape, device=device)
        return [X_tvm, W1_tvm, B1_tvm, W2_tvm, B2_tvm], [Y_tvm]
    
    @staticmethod
    def ref_kernel(x, w1, b1, w2, b2):
        t1 = np.einsum("bse,eh->bsh", x, w1)
        t2 = np.maximum(t1 + b1, 0)
        t3 = np.einsum("bsh,he->bse", t2, w2)
        y = t3 + b2
        return y


@tvm.auto_scheduler.register_workload
def relu_ffn(B, S, E, H):
    X = te.placeholder((B, S, E), name="X", dtype="float32")
    W1 = te.placeholder((E, H), name="W1", dtype="float32")
    B1 = te.placeholder((H,), name="B1", dtype="float32")
    W2 = te.placeholder((H, E), name="W2", dtype="float32")
    B2 = te.placeholder((E,), name="B2", dtype="float32")
    
    e = te.reduce_axis((0, E), name="e")
    t1 = te.compute((B, S, H), lambda b, s, h: te.sum(X[b, s, e] * W1[e, h], axis=e), name="t1")
    t2 = te.compute((B, S, H), lambda b, s, h: te.max(t1[b, s, h] + B1[h], 0), name="t2")
    h = te.reduce_axis((0, H), name="h")
    t3 = te.compute((B, S, E), lambda b, s, e: te.sum(t2[b, s, h] * W2[h, e], axis=h), name="t3")
    Y = te.compute((B, S, E), lambda b, s, e: t3[b, s, e] + B2[e], name="Y")
    
    return [X, W1, B1, W2, B2, Y]


def main(args):
    kernel = globals()[args.kernel]
    kd = globals()[f'{args.kernel}Data']
    
    shape_args = list(map(int, args.size.split('x')))

    tvm_args = kernel(*shape_args)

    device = tvm.cpu()
    target = tvm.target.Target.from_device(device)
    
    log_file = args.log if args.log is not None else f'{kernel.__name__}_{args.size}.json'
    
    if args.default:
        s = te.create_schedule(tvm_args[-1].op)
        mod = tvm.lower(s, tvm_args, name=kernel.__name__)

        func = tvm.build(mod, target=target)
    else:
        task = tvm.auto_scheduler.SearchTask(func=kernel, args=shape_args, target=target)
        
        if args.search or not os.path.exists(log_file):
            tune_option = tvm.auto_scheduler.TuningOptions(
                num_measure_trials=args.trials,
                measure_callbacks=[tvm.auto_scheduler.RecordToFile(log_file)],
            )

            task.tune(tune_option)
        
        schedule, func_args = task.apply_best(log_file)

        func = tvm.build(schedule, func_args, target)
    
    if args.correctness:
        assert os.path.exists(log_file)
        records = list(tvm.auto_scheduler.load_records(log_file))
        evals_per_error = {} # error_no -> records
        for inp, result in records:
            evals_per_error.setdefault(result.error_no, []).append((inp, result))
        for error_no, inp_res_list in evals_per_error.items():
            # we use data structure from AutoTVM instead of AutoScheduler because it allows printing the error message
            err_str = repr(tvm.autotvm.measure.MeasureErrorNo(error_no))
            print(f'Error {err_str}, evaluations {len(inp_res_list)} out of {len(records)}')
        # now let's evaluate all candidates that produced no error
        correct_candidates = evals_per_error[tvm.auto_scheduler.measure.MeasureErrorNo.NO_ERROR]
        task = tvm.auto_scheduler.SearchTask(func=kernel, args=shape_args, target=target)
        for instance_idx, (inp, result) in enumerate(records):
            if result.error_no in {6, 7}:
                print(f'TVM {kernel.__name__} time (timeout) [ms]: avg 1e10 std 0.0')
                continue
            elif result.error_no == 4:
                print(f'TVM {kernel.__name__} time (error) [ms]: avg 1e10 std 0.0')
                continue
            elif result.error_no != 0:
                raise ValueError(f'Unexpected error code {result.error_no}')
            # print(f'Evaluating candidate {instance_idx + 1} / {len(correct_candidates)}')
            schedule, func_args = task.compute_dag.apply_steps_from_state(inp.state, task.layout_rewrite_option)
            func = tvm.build(schedule, func_args, target)
            
            times = []
            for i in range(args.repeat):
                np_inp_args, np_out_args = kd.ref_inputs(*shape_args)
                tvm_inp_args, tvm_out_args = kd.ref_tvm_inputs(*np_inp_args, *np_out_args)
                t1 = time.time()
                func(*tvm_inp_args, *tvm_out_args)
                t2 = time.time()
                for n, t in zip(np_out_args, tvm_out_args):
                    assert np.allclose(t.asnumpy(), n, rtol=1e-3, atol=1e-4)
                if i >= args.warmup:
                    times.append(t2 - t1)
                    
            print(f'TVM {kernel.__name__} time (measure) [ms]: avg {np.mean(times) * 1e3:.6f} std {np.std(times) * 1e3:.6f}')
            
        print('Correctness test passed for all candidates')
        exit(0)
    
    times = []
    for i in range(args.repeat):
        np_inp_args, np_out_args = kd.ref_inputs(*shape_args)
        tvm_inp_args, tvm_out_args = kd.ref_tvm_inputs(*np_inp_args, *np_out_args)
        t1 = time.time()
        func(*tvm_inp_args, *tvm_out_args)
        t2 = time.time()
        for n, t in zip(np_out_args, tvm_out_args):
            assert np.allclose(t.asnumpy(), n, rtol=1e-3, atol=1e-4)
        if i >= args.warmup:
            times.append(t2 - t1)
    print(f"TVM {kernel.__name__} time [ms]: avg {np.mean(times) * 1e3:.6f} std {np.std(times) * 1e3:.6f}")


if __name__ == "__main__":
    
    if "TVM_NUM_THREADS" not in os.environ:
        raise Exception("Please set export TVM_NUM_THREADS=18")
    
    parser = argparse.ArgumentParser()
    parser.add_argument("--search", action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument("--default", action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument("--correctness", action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument("--repeat", type=int, default=15)
    parser.add_argument("--warmup", type=int, default=5)
    parser.add_argument("--trials", type=int, default=1000)
    parser.add_argument("--kernel", type=str, default='layernorm')
    parser.add_argument("--size", type=str, default='4096x4096')
    parser.add_argument("--log", type=str, default=None)
    args = parser.parse_args()
    
    main(args)