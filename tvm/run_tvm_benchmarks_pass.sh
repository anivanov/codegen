#!/bin/bash

source ~/.bashrc
conda activate tvm

export TVM_NUM_THREADS=18
export OMP_PROC_BIND=close
NCA='numactl -m 0 -N 0 -C 0-17'

# arbitrary sizes
$NCA python batchnorm.py --size 8x3x2048x2048 --trials 2 --log batchnorm1.json
$NCA python layernorm.py --size 4096x4096 --trials 2 --log layernorm1.json
$NCA python matmul.py --size 768x1024x1024 --trials 2 --log matmul1.json
$NCA python relu.py --size 4096x4096 --trials 2 --log relu1.json
$NCA python reducemean.py --size 4096x4096 --trials 2 --log reducemean1.json
$NCA python conv.py --size 8x10x3x512x512x5 --trials 2 --log conv1.json
# sizes from concrete models
$NCA python softmax.py --size 24576x512 --trials 2 --log conv1_24576x512.json
$NCA python rmsnorm.py --size 3072x4096 --trials 2 --log rmsnorm1_3072x4096.json
$NCA python bmm.py --size 192x512x128x512 --trials 2 --log bmm1_192x512x128x512.json
$NCA python matmul.py --size 3072x4096x4096 --trials 5 --log matmul1_3072x4096x4096.json
$NCA python mul.py --size 6x14336 --trials 2 --log mul1_6x14336.json
$NCA python add.py --size 3072x4096 --trials 2 --log add1_3072x4096.json
$NCA python batchnorm.py --size 8x64x300x300 --trials 2 --log batchnorm1_8x64x300x300.json
$NCA python conv.py --size 8x64x64x56x56x3 --trials 2 --log conv1_8x64x64x56x56x3.json
$NCA python layernorm.py --size 16384x1024 --trials 2 --log layernorm1_16384x1024.json
$NCA python relu.py --size 8x64x112x112 --trials 2 --log relu1_8x64x112x112.json
