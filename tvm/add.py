import tvm
from tvm import te
import numpy as np
import timeit
import argparse
import os
import time
import itertools


def ref_add(X, Y):
    return X + Y


@tvm.auto_scheduler.register_workload  # Note the auto_scheduler decorator
def add(M, N):
    X = te.placeholder((M, N), name="X", dtype="float32")
    Y = te.placeholder((M, N), name="Y", dtype="float32")
    Z = te.compute((M, N), lambda i, j: X[i, j] + Y[i, j], name="Z")
    return X, Y, Z


if __name__ == "__main__":
    if "TVM_NUM_THREADS" not in os.environ:
        raise Exception("Please set export TVM_NUM_THREADS=18")
    
    parser = argparse.ArgumentParser()
    parser.add_argument("--search", action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument("--default", action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument("--repeat", type=int, default=15)
    parser.add_argument("--warmup", type=int, default=5)
    parser.add_argument("--trials", type=int, default=1000)
    parser.add_argument("--size", type=str, default='3072x4096')
    parser.add_argument("--log", type=str, default=None)
    args = parser.parse_args()

    M_val, N_val = map(int, args.size.split('x'))

    X_np = np.random.rand(M_val, N_val).astype("float32")
    Y_np = np.random.rand(M_val, N_val).astype("float32")
    Z_np = ref_add(X_np, Y_np)

    device = tvm.cpu()

    X_tvm = tvm.nd.array(X_np, device)
    Y_tvm = tvm.nd.array(Y_np, device)
    Z_tvm = tvm.nd.empty(Z_np.shape, device=device)

    X, Y, Z = add(M_val, N_val)

    target = tvm.target.Target.from_device(device)  # llvm -mcpu=broadwell
    
    log_file = args.log if args.log is not None else f'add_{args.size}.json'
    
    if args.default:
        s = te.create_schedule(Z.op)
        mod = tvm.lower(s, [X, Y, Z], name="add")

        func = tvm.build(mod, target=target)
    else:
        task = tvm.auto_scheduler.SearchTask(func=add, args=(M_val, N_val), target=target)
        
        if args.search or not os.path.exists(log_file):
            tune_option = tvm.auto_scheduler.TuningOptions(
                num_measure_trials=args.trials,
                measure_callbacks=[tvm.auto_scheduler.RecordToFile(log_file)],
            )

            task.tune(tune_option)
        
        schedule, func_args = task.apply_best(log_file)

        func = tvm.build(schedule, func_args, target)
        
    func(X_tvm, Y_tvm, Z_tvm)

    assert np.allclose(Z_tvm.asnumpy(), Z_np, rtol=1e-3, atol=1e-4)

    times = []
    for i in range(args.repeat):
        X_np = np.random.rand(M_val, N_val).astype("float32")
        Y_np = np.random.rand(M_val, N_val).astype("float32")
        X_tvm = tvm.nd.array(X_np, device)
        Y_tvm = tvm.nd.array(Y_np, device)
        Z_tvm = tvm.nd.empty(Z_np.shape, device=device)
        t1 = time.time()
        func(X_tvm, Y_tvm, Z_tvm)
        t2 = time.time()
        if i >= args.warmup:
            times.append(t2 - t1)

    print(f"TVM add {args.size} trials={args.trials} time [ms]: avg {np.mean(times) * 1e3:.6f} std {np.std(times) * 1e3:.6f}")

