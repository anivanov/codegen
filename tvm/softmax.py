import tvm
from tvm import te
import numpy as np
import timeit
import argparse
import os
import time
import itertools


def ref_inputs(M, N):
    X_np = np.random.rand(M, N).astype("float32")
    Y_np = ref_kernel(X_np)
    return [X_np], [Y_np]


def ref_tvm_inputs(x_np, Y_np):
    device = tvm.cpu()
    X_tvm = tvm.nd.array(x_np, device)
    Y_tvm = tvm.nd.empty(Y_np.shape, device=device)
    return [X_tvm], [Y_tvm]


def ref_kernel(X):
    max_val = np.max(X, axis=1)
    diff = X - max_val[:, None]
    exp_diff = np.exp(diff)
    sum_val = np.sum(exp_diff, axis=1)
    Y = exp_diff / sum_val[:, None]
    return Y


@tvm.auto_scheduler.register_workload  # Note the auto_scheduler decorator
def softmax(M, N):
    X = te.placeholder((M, N), name="X", dtype="float32")
    n = te.reduce_axis((0, N), name="n")
    max_val = te.compute((M,), lambda m: te.max(X[m, n], axis=n), name="max_val")
    diff = te.compute((M, N), lambda m, n: X[m, n] - max_val[m], name="diff")
    exp_diff = te.compute((M, N), lambda m, n: te.exp(diff[m, n]), name="exp_diff")
    n2 = te.reduce_axis((0, N), name="n2")
    sum_val = te.compute((M,), lambda m: te.sum(exp_diff[m, n2], axis=n2), name="sum_val")
    Y = te.compute((M, N), lambda m, n: exp_diff[m, n] / sum_val[m], name="Y")
    return X, Y


if __name__ == "__main__":
    if "TVM_NUM_THREADS" not in os.environ:
        raise Exception("Please set export TVM_NUM_THREADS=18")
    
    parser = argparse.ArgumentParser()
    parser.add_argument("--search", action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument("--default", action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument("--correctness", action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument("--repeat", type=int, default=15)
    parser.add_argument("--warmup", type=int, default=5)
    parser.add_argument("--trials", type=int, default=1000)
    parser.add_argument("--size", type=str, default='24576x512')
    parser.add_argument("--log", type=str, default=None)
    args = parser.parse_args()

    M_val, N_val = map(int, args.size.split('x'))
    shape_args = (M_val, N_val)    

    X_np = np.random.rand(M_val, N_val).astype("float32")
    Y_np = ref_kernel(X_np)

    device = tvm.cpu()

    X_tvm = tvm.nd.array(X_np, device)
    Y_tvm = tvm.nd.empty(Y_np.shape, device=device)

    X, Y = softmax(M_val, N_val)

    target = tvm.target.Target.from_device(device)  # llvm -mcpu=broadwell
    
    log_file = args.log if args.log is not None else f'softmax_{args.size}.json'
    
    if args.default:
        s = te.create_schedule(Y.op)
        mod = tvm.lower(s, [X, Y], name="kernel")

        func = tvm.build(mod, target=target)
    else:
        task = tvm.auto_scheduler.SearchTask(func=softmax, args=shape_args, target=target)
        
        if args.search or not os.path.exists(log_file):
            tune_option = tvm.auto_scheduler.TuningOptions(
                num_measure_trials=args.trials,
                measure_callbacks=[tvm.auto_scheduler.RecordToFile(log_file)],
            )

            task.tune(tune_option)
        
        schedule, func_args = task.apply_best(log_file)

        func = tvm.build(schedule, func_args, target)
        
    if args.correctness:
        assert os.path.exists(log_file)
        records = list(tvm.auto_scheduler.load_records(log_file))
        evals_per_error = {} # error_no -> records
        for inp, result in records:
            evals_per_error.setdefault(result.error_no, []).append((inp, result))
        for error_no, inp_res_list in evals_per_error.items():
            # we use data structure from AutoTVM instead of AutoScheduler because it allows printing the error message
            err_str = repr(tvm.autotvm.measure.MeasureErrorNo(error_no))
            print(f'Error {err_str}, evaluations {len(inp_res_list)} out of {len(records)}')
        # now let's evaluate all candidates that produced no error
        correct_candidates = evals_per_error[tvm.auto_scheduler.measure.MeasureErrorNo.NO_ERROR]
        task = tvm.auto_scheduler.SearchTask(func=softmax, args=shape_args, target=target)
        for instance_idx, (inp, result) in enumerate(correct_candidates):
            print(f'Evaluating candidate {instance_idx + 1} / {len(correct_candidates)}')
            schedule, func_args = task.compute_dag.apply_steps_from_state(inp.state, task.layout_rewrite_option)
            func = tvm.build(schedule, func_args, target)
            
            np_inp_args, np_out_args = ref_inputs(*shape_args)
            tvm_inp_args, tvm_out_args = ref_tvm_inputs(*np_inp_args, *np_out_args)
            
            func(*tvm_inp_args, *tvm_out_args)
            for n, t in zip(np_out_args, tvm_out_args):
                assert np.allclose(t.asnumpy(), n, rtol=1e-3, atol=1e-4)
            
        print('Correctness test passed for all candidates')
        exit(0)
        
    func(X_tvm, Y_tvm)

    assert np.allclose(Y_tvm.asnumpy(), Y_np, rtol=1e-3, atol=1e-4)

    times = []
    for i in range(args.repeat):
        X_np = np.random.rand(M_val, N_val).astype("float32")
        X_tvm = tvm.nd.array(X_np, device)
        Y_tvm = tvm.nd.empty(Y_np.shape, device=device)
        t1 = time.time()
        func(X_tvm, Y_tvm)
        t2 = time.time()
        if i >= args.warmup:
            times.append(t2 - t1)

    print(f"TVM {softmax.__name__} {args.size} trials={args.trials} time [ms]: avg {np.mean(times) * 1e3:.6f} std {np.std(times) * 1e3:.6f}")

