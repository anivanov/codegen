import tvm
from tvm import te
import numpy as np
import timeit
import argparse
import os
import time
import itertools


def pooling_out_dim(input, kernel, stride, pad, dilation):
    return (input + 2 * pad - (dilation * (kernel - 1) + 1)) // stride + 1


def ref_inputs(N, M, C, H_in, W_in, K, H, W):
    X_np = np.random.rand(N, C, H_in, W_in).astype("float32")
    F_np = np.random.rand(M, C, K, K).astype("float32")
    Y_np = ref_kernel(X_np, F_np)
    return [X_np, F_np], [Y_np]


def ref_tvm_inputs(x_np, f_np, Y_np):
    device = tvm.cpu()
    X_tvm = tvm.nd.array(x_np, device)
    F_tvm = tvm.nd.array(f_np, device)
    Y_tvm = tvm.nd.empty(Y_np.shape, device=device)
    return [X_tvm, F_tvm], [Y_tvm]


def ref_kernel(X, F):
    N, C, H_in, W_in = X.shape
    M, C, K, K = F.shape
    H_out = pooling_out_dim(input=H_in, kernel=K, stride=1, pad=0, dilation=1)
    W_out = pooling_out_dim(input=W_in, kernel=K, stride=1, pad=0, dilation=1)
    Y = np.zeros((N, M, H_out, W_out), dtype="float32")
    Xv = X.reshape(N, 1, C, H_in, W_in)
    Fv = F.reshape(1, M, C, K, K)
    for c, kh, kw in itertools.product(range(C), range(K), range(K)):
        Y[:, :, :, :] += Xv[:, :, c, kh:kh+H_out, kw:kw+W_out] * Fv[:, :, c, None, None, kh, kw]
    return Y


@tvm.auto_scheduler.register_workload  # Note the auto_scheduler decorator
def conv(N, M, C, H_in, W_in, K, H, W):
    X = te.placeholder((N, C, H_in, W_in), name="X", dtype="float32")
    F = te.placeholder((M, C, K, K), name="W", dtype="float32")
    c = te.reduce_axis((0, C), name="c")
    kh = te.reduce_axis((0, K), name="kh")
    kw = te.reduce_axis((0, K), name="kw")

    Y = te.compute((N, M, H, W), lambda n, m, h, w: te.sum(X[n, c, h+kh, w+kw] * F[m, c, kh, kw], axis=[c, kh, kw]), name="Y")
    return [X, F, Y]


kernel = conv


if __name__ == "__main__":
    if "TVM_NUM_THREADS" not in os.environ:
        raise Exception("Please set export TVM_NUM_THREADS=18")
    
    parser = argparse.ArgumentParser()
    parser.add_argument("--search", action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument("--default", action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument("--correctness", action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument("--repeat", type=int, default=15)
    parser.add_argument("--warmup", type=int, default=5)
    parser.add_argument("--trials", type=int, default=1000)
    parser.add_argument("--size", type=str, default='8x10x3x512x512x5')
    parser.add_argument("--log", type=str, default=None)
    args = parser.parse_args()

    N_val, M_val, C_val, H_in_val, W_in_val, K_val = map(int, args.size.split('x'))
    
    H_val = pooling_out_dim(input=H_in_val, kernel=K_val, stride=1, pad=0, dilation=1)
    W_val = pooling_out_dim(input=W_in_val, kernel=K_val, stride=1, pad=0, dilation=1)
    shape_args = (N_val, M_val, C_val, H_in_val, W_in_val, K_val, H_val, W_val)

    X_np = np.random.rand(N_val, C_val, H_in_val, W_in_val).astype("float32")
    F_np = np.random.rand(M_val, C_val, K_val, K_val).astype("float32")
    Y_np = ref_kernel(X_np, F_np)

    device = tvm.cpu()

    X_tvm = tvm.nd.array(X_np, device)
    F_tvm = tvm.nd.array(F_np, device)
    Y_tvm = tvm.nd.empty(Y_np.shape, device=device)

    X, F, Y = kernel(*shape_args)

    target = tvm.target.Target.from_device(device)  # llvm -mcpu=broadwell
    
    log_file = args.log if args.log is not None else f'{kernel.__name__}_{args.size}.json'
    
    if args.default:
        s = te.create_schedule(Y.op)
        mod = tvm.lower(s, [X, F, Y], name=kernel.__name__)

        func = tvm.build(mod, target=target)
    else:
        task = tvm.auto_scheduler.SearchTask(func=kernel, args=shape_args, target=target)
        
        if args.search or not os.path.exists(log_file):
            tune_option = tvm.auto_scheduler.TuningOptions(
                num_measure_trials=args.trials,
                measure_callbacks=[tvm.auto_scheduler.RecordToFile(log_file)],
            )

            task.tune(tune_option)
        
        schedule, func_args = task.apply_best(log_file)

        func = tvm.build(schedule, func_args, target)
    
    if args.correctness:
        assert os.path.exists(log_file)
        records = list(tvm.auto_scheduler.load_records(log_file))
        evals_per_error = {} # error_no -> records
        for inp, result in records:
            evals_per_error.setdefault(result.error_no, []).append((inp, result))
        for error_no, inp_res_list in evals_per_error.items():
            # we use data structure from AutoTVM instead of AutoScheduler because it allows printing the error message
            err_str = repr(tvm.autotvm.measure.MeasureErrorNo(error_no))
            print(f'Error {err_str}, evaluations {len(inp_res_list)} out of {len(records)}')
        # now let's evaluate all candidates that produced no error
        correct_candidates = evals_per_error[tvm.auto_scheduler.measure.MeasureErrorNo.NO_ERROR]
        task = tvm.auto_scheduler.SearchTask(func=kernel, args=shape_args, target=target)
        for instance_idx, (inp, result) in enumerate(correct_candidates):
            print(f'Evaluating candidate {instance_idx + 1} / {len(correct_candidates)}')
            schedule, func_args = task.compute_dag.apply_steps_from_state(inp.state, task.layout_rewrite_option)
            func = tvm.build(schedule, func_args, target)
            
            np_inp_args, np_out_args = ref_inputs(*shape_args)
            tvm_inp_args, tvm_out_args = ref_tvm_inputs(*np_inp_args, *np_out_args)
            
            func(*tvm_inp_args, *tvm_out_args)
            for n, t in zip(np_out_args, tvm_out_args):
                assert np.allclose(t.asnumpy(), n, rtol=1e-3, atol=1e-4)
            
        print('Correctness test passed for all candidates')
        exit(0)
    
    func(X_tvm, F_tvm, Y_tvm)

    assert np.allclose(Y_tvm.asnumpy(), Y_np, rtol=1e-3, atol=1e-4)

    times = []
    for i in range(args.repeat):
        X_np = np.random.rand(N_val, C_val, H_in_val, W_in_val).astype("float32")
        F_np = np.random.rand(M_val, C_val, K_val, K_val).astype("float32")
        X_tvm = tvm.nd.array(X_np, device)
        F_tvm = tvm.nd.array(F_np, device)
        Y_tvm = tvm.nd.empty(Y_np.shape, device=device)
        t1 = time.time()
        func(X_tvm, F_tvm, Y_tvm)
        t2 = time.time()
        if i >= args.warmup:
            times.append(t2 - t1)

    # times = timeit.repeat(lambda: func(X_tvm, F_tvm, Y_tvm), repeat=args.repeat, number=1)[args.warmup:]
    print(f"TVM {kernel.__name__} {args.size} trials={args.trials} time [ms]: avg {np.mean(times) * 1e3:.6f} std {np.std(times) * 1e3:.6f}")

