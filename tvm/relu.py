import tvm
from tvm import te
import numpy as np
import timeit
import argparse
import os
import time
import functools
import operator


@tvm.auto_scheduler.register_workload  # Note the auto_scheduler decorator
def relu(N):
    X = te.placeholder((N,), name="X", dtype="float32")
    Y = te.compute((N,), lambda n: te.if_then_else(X[n] > 0, X[n], 0), name="Y")
    return [X, Y]


if __name__ == "__main__":
    if "TVM_NUM_THREADS" not in os.environ:
        raise Exception("Please set export TVM_NUM_THREADS=18")

    parser = argparse.ArgumentParser()
    parser.add_argument("--search", action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument("--default", action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument("--repeat", type=int, default=15)
    parser.add_argument("--warmup", type=int, default=5)
    parser.add_argument("--trials", type=int, default=1000)
    parser.add_argument("--size", type=str, default='4096x4096')
    parser.add_argument("--log", type=str, default=None)
    args = parser.parse_args()
    
    N_val = functools.reduce(operator.mul, map(int, args.size.split('x')), 1)

    X_np = np.random.rand(N_val).astype("float32")
    Y_np = np.maximum(X_np, 0)

    device = tvm.cpu()

    X_tvm = tvm.nd.array(X_np, device)
    Y_tvm = tvm.nd.empty(Y_np.shape, device=device)

    X, Y = relu(N_val)

    target = tvm.target.Target.from_device(device)  # llvm -mcpu=broadwell
    
    log_file = args.log if args.log is not None else f'relu_{args.size}.json'
    
    if args.default:
        s = te.create_schedule(Y.op)
        mod = tvm.lower(s, [X, Y], name="relu")

        func = tvm.build(mod, target=target)
    else:
        task = tvm.auto_scheduler.SearchTask(func=relu, args=(N_val,), target=target)
        
        if args.search or not os.path.exists(log_file):
            tune_option = tvm.auto_scheduler.TuningOptions(
                num_measure_trials=args.trials,
                measure_callbacks=[tvm.auto_scheduler.RecordToFile(log_file)],
            )

            task.tune(tune_option)
        
        schedule, func_args = task.apply_best(log_file)

        func = tvm.build(schedule, func_args, target)
        
    func(X_tvm, Y_tvm)

    assert np.allclose(Y_tvm.asnumpy(), Y_np, rtol=1e-3, atol=1e-4)

    times = []
    for i in range(args.repeat):
        X_np = np.random.rand(N_val).astype("float32")
        X_tvm = tvm.nd.array(X_np, device)
        Y_tvm = tvm.nd.empty(Y_np.shape, device=device)
        t1 = time.time()
        func(X_tvm, Y_tvm)
        t2 = time.time()
        if i >= args.warmup:
            times.append(t2 - t1)

    # times = timeit.repeat(lambda: func(X_tvm, Y_tvm), repeat=args.repeat, number=1)[args.warmup:]
    print(f"TVM relu {args.size} trials={args.trials} time [ms]: avg {np.mean(times) * 1e3:.6f} std {np.std(times) * 1e3:.6f}")

