#!/bin/bash -l

# run this script as "bash run_tvm_benchmarks_clariden.sh"
# it will schedule all needed jobs on the Clariden cluster

export TVM_NUM_THREADS=`nproc`

abs_script_dir=$(dirname "$(readlink -f "$0")")

commands=(
  # -- measure execution of a single pass --
  # arbitrary sizes
  "python batchnorm.py --size 8x3x2048x2048 --trials 2 --log batchnorm1_8x3x2048x2048.json"
  "python layernorm.py --size 4096x4096 --trials 2 --log layernorm1_4096x4096.json"
  "python matmul.py --size 768x1024x1024 --trials 2 --log matmul1_768x1024x1024.json"
  "python relu.py --size 4096x4096 --trials 2 --log relu1_4096x4096.json"
  "python reducemean.py --size 4096x4096 --trials 2 --log reducemean1_4096x4096.json"
  "python conv.py --size 8x10x3x512x512x5 --trials 2 --log conv1_8x10x3x512x512x5.json"
  "python swiglu.py --size 1x256x4096x448 --trials 2 --log swiglu1_1x256x4096x448.json"
  # sizes from concrete models
  "python softmax.py --size 24576x512 --trials 2 --log softmax1_24576x512.json"
  "python rmsnorm.py --size 3072x4096 --trials 2 --log rmsnorm1_3072x4096.json"
  "python bmm.py --size 192x512x128x512 --trials 2 --log bmm1_192x512x128x512.json"
  "python matmul.py --size 3072x4096x4096 --trials 5 --log matmul1_3072x4096x4096.json"
  "python mul.py --size 6x14336 --trials 2 --log mul1_6x14336.json"
  "python add.py --size 3072x4096 --trials 2 --log add1_3072x4096.json"
  "python batchnorm.py --size 8x64x300x300 --trials 2 --log batchnorm1_8x64x300x300.json"
  "python conv.py --size 8x64x64x56x56x3 --trials 2 --log conv1_8x64x64x56x56x3.json"
  "python layernorm.py --size 16384x1024 --trials 2 --log layernorm1_16384x1024.json"
  "python relu.py --size 8x64x112x112 --trials 2 --log relu1_8x64x112x112.json"
  # -- measure execution of a full search --
  # arbitrary sizes
  "python batchnorm.py --size 8x3x2048x2048"
  "python layernorm.py --size 4096x4096"
  "python matmul.py --size 768x1024x1024"
  "python relu.py --size 4096x4096"
  "python reducemean.py --size 4096x4096"
  "python conv.py --size 8x10x3x512x512x5"
  "python swiglu.py --size 1x256x4096x448"
  # sizes from concrete models
  "python softmax.py --size 24576x512"
  "python rmsnorm.py --size 3072x4096"
  "python bmm.py --size 192x512x128x512"
  "python matmul.py --size 3072x4096x4096"
  "python mul.py --size 6x14336"
  "python add.py --size 3072x4096"
  "python batchnorm.py --size 8x64x300x300"
  "python conv.py --size 8x64x64x56x56x3"
  "python layernorm.py --size 16384x1024"
  "python relu.py --size 8x64x112x112"
)

# Loop through the commands and submit each one as a separate Slurm job
for command in "${commands[@]}"; do

sbatch << EOF
#!/bin/bash
#SBATCH --job-name "TVM benchmark"
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH -A a-g34
#SBATCH --mem=256G
#SBATCH --time=8:00:00
#SBATCH --partition=normal
#SBATCH --environment=codegen

cd $abs_script_dir

$command 

# print the time it took to run the job
echo "Elapsed time: \$(sacct -j \$SLURM_JOB_ID --format=Elapsed | tail -n 1)"
EOF

done
