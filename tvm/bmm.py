import tvm
from tvm import te
import numpy as np
import timeit
import argparse
import os
import time
import itertools


def ref_bmm(X, Y):
    Z = np.einsum("bkm,bkn->bmn", X, Y)
    return Z


@tvm.auto_scheduler.register_workload  # Note the auto_scheduler decorator
def bmm(B, M, K, N):
    X = te.placeholder((B, K, M), name="X", dtype="float32")
    Y = te.placeholder((B, K, N), name="Y", dtype="float32")
    k = te.reduce_axis((0, K), name="k")
    Z = te.compute((B, M, N), lambda b, m, n: te.sum(X[b, k, m] * Y[b, k, n], axis=k), name="Z")
    return X, Y, Z


if __name__ == "__main__":
    if "TVM_NUM_THREADS" not in os.environ:
        raise Exception("Please set export TVM_NUM_THREADS=18")
        
    parser = argparse.ArgumentParser()
    parser.add_argument("--search", action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument("--default", action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument("--repeat", type=int, default=15)
    parser.add_argument("--warmup", type=int, default=5)
    parser.add_argument("--trials", type=int, default=1000)
    parser.add_argument("--size", type=str, default='192x512x128x512')
    parser.add_argument("--log", type=str, default=None)
    args = parser.parse_args()

    B_val, M_val, K_val, N_val = map(int, args.size.split('x'))

    X_np = np.random.rand(B_val, K_val, M_val).astype("float32")
    Y_np = np.random.rand(B_val, K_val, N_val).astype("float32")
    Z_np = ref_bmm(X_np, Y_np)

    device = tvm.cpu()

    X_tvm = tvm.nd.array(X_np, device)
    Y_tvm = tvm.nd.array(Y_np, device)
    Z_tvm = tvm.nd.empty(Z_np.shape, device=device)

    X, Y, Z = bmm(B_val, M_val, K_val, N_val)
    
    target = tvm.target.Target.from_device(device)  # llvm -mcpu=broadwell
    
    log_file = args.log if args.log is not None else f'bmm_{args.size}.json'
    
    if args.default:
        s = te.create_schedule(Z.op)
        mod = tvm.lower(s, [X, Y, Z], name="bmm")
        func = tvm.build(mod, target=target)
    else:
        task = tvm.auto_scheduler.SearchTask(func=bmm, args=(B_val, M_val, K_val, N_val), target=target)
        
        if args.search or not os.path.exists(log_file):
            tune_option = tvm.auto_scheduler.TuningOptions(
                num_measure_trials=args.trials,
                measure_callbacks=[tvm.auto_scheduler.RecordToFile(log_file)],
            )

            task.tune(tune_option)
        
        schedule, func_args = task.apply_best(log_file)

        func = tvm.build(schedule, func_args, target)
        
    func(X_tvm, Y_tvm, Z_tvm)

    assert np.allclose(Z_tvm.asnumpy(), Z_np, rtol=1e-3, atol=1e-4)

    times = []
    for i in range(args.repeat):
        X_np = np.random.rand(B_val, K_val, M_val).astype("float32")
        Y_np = np.random.rand(B_val, K_val, N_val).astype("float32")
        X_tvm = tvm.nd.array(X_np, device)
        Y_tvm = tvm.nd.array(Y_np, device)
        Z_tvm = tvm.nd.empty(Z_np.shape, device=device)
        t1 = time.time()
        func(X_tvm, Y_tvm, Z_tvm)
        t2 = time.time()
        if i >= args.warmup:
            times.append(t2 - t1)

    print(f"TVM bmm {args.size} trials={args.trials} time [ms]: avg {np.mean(times) * 1e3:.6f} std {np.std(times) * 1e3:.6f}")

