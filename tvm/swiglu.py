import tvm
from tvm import te
import numpy as np
import timeit
import argparse
import os
import time


def ref_inputs(B, S, E, H):
    W1 = np.random.rand(E, H).astype("float32")
    W2 = np.random.rand(H, E).astype("float32")
    W3 = np.random.rand(E, H).astype("float32")
    X = np.random.rand(B, S, E).astype("float32")
    Y = ref_kernel(X, W1, W2, W3)
    return [X, W1, W2, W3], [Y]

def ref_swish(x):
    return x / (1 + np.exp(-x))


def ref_kernel(X, W1, W2, W3):
    T1 = np.einsum("bse,eh->bsh", X, W1)
    T2 = ref_swish(T1)
    T3 = np.einsum("bse,eh->bsh", X, W3)
    T4 = T2 * T3
    Y = np.einsum("bsh,he->bse", T4, W2)
    return Y


def ref_tvm_inputs(X_np, W1_np, W2_np, W3_np, Y_np):
    device = tvm.cpu()
    X_tvm = tvm.nd.array(X_np, device)
    W1_tvm = tvm.nd.array(W1_np, device)
    W2_tvm = tvm.nd.array(W2_np, device)
    W3_tvm = tvm.nd.array(W3_np, device)
    Y_tvm = tvm.nd.empty(Y_np.shape, device=device)
    return [X_tvm, W1_tvm, W2_tvm, W3_tvm], [Y_tvm]
    


@tvm.auto_scheduler.register_workload
def swiglu(B, S, E, H):
    X = te.placeholder((B, S, E), name="X", dtype="float32")
    W1 = te.placeholder((E, H), name="W1", dtype="float32")
    W2 = te.placeholder((H, E), name="W2", dtype="float32")
    W3 = te.placeholder((E, H), name="W3", dtype="float32")
    
    e1 = te.reduce_axis((0, E), name="e1")
    T1 = te.compute((B, S, H), lambda b, s, h: te.sum(X[b, s, e1] * W1[e1, h], axis=e1), name="T1")
    T2 = te.compute((B, S, H), lambda b, s, h: T1[b, s, h] / (1 + te.exp(-T1[b, s, h])), name="T2")
    e3 = te.reduce_axis((0, E), name="e3")
    T3 = te.compute((B, S, H), lambda b, s, h: te.sum(X[b, s, e3] * W3[e3, h], axis=e3), name="T3")
    T4 = te.compute((B, S, H), lambda b, s, h: T2[b, s, h] * T3[b, s, h], name="T4")
    h5 = te.reduce_axis((0, H), name="h5")
    Y = te.compute((B, S, E), lambda b, s, e: te.sum(T4[b, s, h5] * W2[h5, e], axis=h5), name="T5")
    
    return X, W1, W2, W3, Y


kernel = swiglu


if __name__ == "__main__":
    if "TVM_NUM_THREADS" not in os.environ:
        raise Exception("Please set export TVM_NUM_THREADS=18")
    
    parser = argparse.ArgumentParser()
    parser.add_argument("--search", action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument("--default", action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument("--correctness", action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument("--repeat", type=int, default=15)
    parser.add_argument("--warmup", type=int, default=5)
    parser.add_argument("--trials", type=int, default=1000)
    parser.add_argument("--size", type=str, default='4x512x768x3072')
    # example llama 3 8B sizes: 4x512x4096x14336
    # exampel GPT-2 small sizes: 4x512x768x3072
    parser.add_argument("--log", type=str, default=None)
    args = parser.parse_args()

    shape_args = list(map(int, args.size.split('x')))

    device = tvm.cpu()

    tvm_args = kernel(*shape_args)

    target = tvm.target.Target.from_device(device)  # llvm -mcpu=broadwell
    
    log_file = args.log if args.log is not None else f'{kernel.__name__}_{args.size}.json'
    
    if args.default:
        s = te.create_schedule(tvm_args[-1].op)
        mod = tvm.lower(s, tvm_args, name=kernel.__name__)

        func = tvm.build(mod, target=target)
    else:
        task = tvm.auto_scheduler.SearchTask(func=kernel, args=shape_args, target=target)
        
        if args.search or not os.path.exists(log_file):
            tune_option = tvm.auto_scheduler.TuningOptions(
                num_measure_trials=args.trials,
                measure_callbacks=[tvm.auto_scheduler.RecordToFile(log_file)],
            )

            task.tune(tune_option)
        
        schedule, func_args = task.apply_best(log_file)
        # print(tvm.lower(schedule, func_args, simple_mode=True))
        mod = tvm.lower(schedule, func_args, simple_mode=True)
        func = tvm.build(mod, func_args, target)
    
    if args.correctness:
        assert os.path.exists(log_file)
        records = list(tvm.auto_scheduler.load_records(log_file))
        evals_per_error = {} # error_no -> records
        for inp, result in records:
            evals_per_error.setdefault(result.error_no, []).append((inp, result))
        for error_no, inp_res_list in evals_per_error.items():
            # we use data structure from AutoTVM instead of AutoScheduler because it allows printing the error message
            err_str = repr(tvm.autotvm.measure.MeasureErrorNo(error_no))
            print(f'Error {err_str}, evaluations {len(inp_res_list)} out of {len(records)}')
        # now let's evaluate all candidates that produced no error
        correct_candidates = evals_per_error[tvm.auto_scheduler.measure.MeasureErrorNo.NO_ERROR]
        for instance_idx, (inp, result) in enumerate(correct_candidates):
            print(f'Evaluating candidate {instance_idx + 1} / {len(correct_candidates)}')
            schedule, func_args = task.compute_dag.apply_steps_from_state(inp.state, task.layout_rewrite_option)
            func = tvm.build(schedule, func_args, target)
            
            np_inp_args, np_out_args = ref_inputs(*shape_args)
            tvm_inp_args, tvm_out_args = ref_tvm_inputs(*np_inp_args, *np_out_args)
            
            func(*tvm_inp_args, *tvm_out_args)
            for n, t in zip(np_out_args, tvm_out_args):
                assert np.allclose(t.asnumpy(), n, rtol=1e-3, atol=1e-4)
            
        print('Correctness test passed for all candidates')
        exit(0)
    
    times = []
    for i in range(args.repeat):
        print("Running iteration", i)
        np_inp_args, np_out_args = ref_inputs(*shape_args)
        tvm_inp_args, tvm_out_args = ref_tvm_inputs(*np_inp_args, *np_out_args)
        print("Inputs ready, starting time measurement...")
        t1 = time.time()
        func(*tvm_inp_args, *tvm_out_args)
        t2 = time.time()
        if i >= args.warmup:
            times.append(t2 - t1)
        print("Measurement done, checking numerical correctness...")
        for n, t in zip(np_out_args, tvm_out_args):
            assert np.allclose(t.asnumpy(), n, rtol=1e-3, atol=1e-4)

    # times = timeit.repeat(lambda: func(A_tvm, B_tvm, C_tvm), repeat=args.repeat, number=1)[args.warmup:]
    times_str = " ".join(f"{time * 1e3:.3f}" for time in times)
    print(f"TVM {kernel.__name__} {args.size} trials={args.trials} time [ms]: avg {np.mean(times) * 1e3:.6f} std {np.std(times) * 1e3:.6f} times {times_str}")
