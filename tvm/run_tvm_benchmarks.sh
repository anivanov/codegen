#!/bin/bash

source ~/.bashrc
conda activate tvm

export TVM_NUM_THREADS=18
export OMP_PROC_BIND=close
NCA='numactl -m 0 -N 0 -C 0-17'

# arbitrary sizes
$NCA python batchnorm.py --size 8x3x2048x2048
$NCA python layernorm.py --size 4096x4096
$NCA python matmul.py --size 768x1024x1024
$NCA python relu.py --size 4096x4096
$NCA python reducemean.py --size 4096x4096
$NCA python conv.py --size 8x10x3x512x512x5
# sizes from concrete models
$NCA python softmax.py --size 24576x512
$NCA python rmsnorm.py --size 3072x4096
$NCA python bmm.py --size 192x512x128x512
$NCA python matmul.py --size 3072x4096x4096
$NCA python mul.py --size 6x14336
$NCA python add.py --size 3072x4096
$NCA python batchnorm.py --size 8x64x300x300
$NCA python conv.py --size 8x64x64x56x56x3
$NCA python layernorm.py --size 16384x1024
$NCA python relu.py --size 8x64x112x112
