import tvm
from tvm import te
import numpy as np
import timeit
import argparse
import os
import time


def ref_batchnorm(X, mean, var, scale, bias):
    eps = 1e-5
    Y = (X - mean[None, :, None, None]) * scale[None, :, None, None] / np.sqrt(var[None, :, None, None] + eps) + bias[None, :, None, None]
    return Y


@tvm.auto_scheduler.register_workload  # Note the auto_scheduler decorator
def batchnorm(N, C, H, W):
    eps = 1e-5

    X = te.placeholder((N, C, H, W), name="X", dtype="float32")
    mean = te.placeholder((C,), name="mean", dtype="float32")
    var = te.placeholder((C,), name="var", dtype="float32")
    scale = te.placeholder((C,), name="scale", dtype="float32")
    bias = te.placeholder((C,), name="bias", dtype="float32")

    Y = te.compute((N, C, H, W), lambda n, c, h, w: (X[n, c, h, w] - mean[c]) * scale[c] / te.sqrt(var[c] + eps) + bias[c], name="Y")

    return [X, mean, var, scale, bias, Y]


if __name__ == "__main__":
    if "TVM_NUM_THREADS" not in os.environ:
        raise Exception("Please set export TVM_NUM_THREADS=18")
    
    parser = argparse.ArgumentParser()
    parser.add_argument("--search", action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument("--default", action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument("--repeat", type=int, default=15)
    parser.add_argument("--warmup", type=int, default=5)
    parser.add_argument("--trials", type=int, default=1000)
    parser.add_argument("--size", type=str, default='8x3x2048x2048')
    parser.add_argument("--log", type=str, default=None)
    args = parser.parse_args()
    
    N_val, C_val, H_val, W_val = map(int, args.size.split('x'))

    device = tvm.cpu()

    X_np = np.random.rand(N_val, C_val, H_val, W_val).astype("float32")
    mean_np = np.random.rand(C_val).astype("float32")
    var_np = np.random.uniform(0, 1, size=(C_val,)).astype("float32")
    scale_np = np.random.rand(C_val).astype("float32")
    bias_np = np.random.rand(C_val).astype("float32")
    Y_np = ref_batchnorm(X_np, mean_np, var_np, scale_np, bias_np)

    X_tvm = tvm.nd.array(X_np, device)
    mean_tvm = tvm.nd.array(mean_np, device)
    var_tvm = tvm.nd.array(var_np, device)
    scale_tvm = tvm.nd.array(scale_np, device)
    bias_tvm = tvm.nd.array(bias_np, device)
    Y_tvm = tvm.nd.empty(Y_np.shape, device=device)

    X, mean, var, scale, bias, Y = batchnorm(N_val, C_val, H_val, W_val)

    target = tvm.target.Target.from_device(device)  # llvm -mcpu=broadwell
    
    log_file = args.log if args.log is not None else f'batchnorm_{args.size}.json'
    
    if args.default:
        s = te.create_schedule(Y.op)
        mod = tvm.lower(s, [X, mean, var, scale, bias, Y], name="batchnorm")

        func = tvm.build(mod, target=target)
    else:
        task = tvm.auto_scheduler.SearchTask(func=batchnorm, args=(N_val, C_val, H_val, W_val), target=target)
        
        if args.search or not os.path.exists(log_file):
            tune_option = tvm.auto_scheduler.TuningOptions(
                num_measure_trials=args.trials,
                measure_callbacks=[tvm.auto_scheduler.RecordToFile(log_file)],
            )

            task.tune(tune_option)
        
        schedule, func_args = task.apply_best(log_file)

        func = tvm.build(schedule, func_args, target)
        
    func(X_tvm, mean_tvm, var_tvm, scale_tvm, bias_tvm, Y_tvm)

    assert np.allclose(Y_tvm.asnumpy(), Y_np, rtol=1e-3, atol=1e-4)

    times = []
    for i in range(args.repeat):
        X_np = np.random.rand(N_val, C_val, H_val, W_val).astype("float32")
        mean_np = np.random.rand(C_val).astype("float32")
        var_np = np.random.uniform(0, 1, size=(C_val,)).astype("float32")
        scale_np = np.random.rand(C_val).astype("float32")
        bias_np = np.random.rand(C_val).astype("float32")

        X_tvm = tvm.nd.array(X_np, device)
        mean_tvm = tvm.nd.array(mean_np, device)
        var_tvm = tvm.nd.array(var_np, device)
        scale_tvm = tvm.nd.array(scale_np, device)
        bias_tvm = tvm.nd.array(bias_np, device)
        Y_tvm = tvm.nd.empty(Y_np.shape, device=device)

        t1 = time.time()
        func(X_tvm, mean_tvm, var_tvm, scale_tvm, bias_tvm, Y_tvm)
        t2 = time.time()
        if i >= args.warmup:
            times.append(t2 - t1)

    # times = timeit.repeat(lambda: func(X_tvm, mean_tvm, var_tvm, scale_tvm, bias_tvm, Y_tvm), repeat=args.repeat, number=1)[args.warmup:]
    print(f"TVM batchnorm {args.size} trials={args.trials} time [ms]: avg {np.mean(times) * 1e3:.6f} std {np.std(times) * 1e3:.6f}")

