import tvm
from tvm import te
import numpy as np
import timeit
import argparse
import os
import time


def ref_inputs(M, K, N):
    A_np = np.random.rand(K, M).astype("float32")
    B_np = np.random.rand(K, N).astype("float32")
    C_np = np.einsum("km,kn->mn", A_np, B_np)
    return [A_np, B_np], [C_np]


def ref_tvm_inputs(a_np, b_np, C_np):
    device = tvm.cpu()
    A_tvm = tvm.nd.array(a_np, device)
    B_tvm = tvm.nd.array(b_np, device)
    C_tvm = tvm.nd.empty(C_np.shape, device=device)
    return [A_tvm, B_tvm], [C_tvm]


@tvm.auto_scheduler.register_workload
def matmul(M, K, N):
    A = te.placeholder((K, M), name="A", dtype="float32")
    B = te.placeholder((K, N), name="B", dtype="float32")

    k = te.reduce_axis((0, K), name="k")
    C = te.compute((M, N), lambda m, n: te.sum(A[k, m] * B[k, n], axis=k), name="C")

    return [A, B, C]


kernel = matmul


if __name__ == "__main__":
    if "TVM_NUM_THREADS" not in os.environ:
        raise Exception("Please set export TVM_NUM_THREADS=18")
    
    parser = argparse.ArgumentParser()
    parser.add_argument("--search", action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument("--default", action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument("--correctness", action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument("--repeat", type=int, default=15)
    parser.add_argument("--warmup", type=int, default=5)
    parser.add_argument("--trials", type=int, default=1000)
    parser.add_argument("--size", type=str, default='768x1024x1024')
    parser.add_argument("--log", type=str, default=None)
    args = parser.parse_args()

    M_val, K_val, N_val = map(int, args.size.split('x'))
    shape_args = (M_val, K_val, N_val)

    A_np = np.random.rand(K_val, M_val).astype("float32")
    B_np = np.random.rand(K_val, N_val).astype("float32")
    C_np = np.einsum("km,kn->mn", A_np, B_np)

    device = tvm.cpu()

    A_tvm = tvm.nd.array(A_np, device)
    B_tvm = tvm.nd.array(B_np, device)
    C_tvm = tvm.nd.empty(C_np.shape, device=device)

    A, B, C = kernel(M_val, K_val, N_val)

    target = tvm.target.Target.from_device(device)  # llvm -mcpu=broadwell
    
    log_file = args.log if args.log is not None else f'{kernel.__name__}_{args.size}.json'
    
    if args.default:
        s = te.create_schedule(C.op)
        mod = tvm.lower(s, [A, B, C], name=kernel.__name__)

        func = tvm.build(mod, target=target)
    else:
        task = tvm.auto_scheduler.SearchTask(func=kernel, args=(M_val, K_val, N_val), target=target)
        
        if args.search or not os.path.exists(log_file):
            tune_option = tvm.auto_scheduler.TuningOptions(
                num_measure_trials=args.trials,
                measure_callbacks=[tvm.auto_scheduler.RecordToFile(log_file)],
            )

            task.tune(tune_option)
        
        schedule, func_args = task.apply_best(log_file)
        # print(tvm.lower(schedule, func_args, simple_mode=True))
        mod = tvm.lower(schedule, func_args, simple_mode=True)
        func = tvm.build(mod, func_args, target)
    
    if args.correctness:
        assert os.path.exists(log_file)
        records = list(tvm.auto_scheduler.load_records(log_file))
        evals_per_error = {} # error_no -> records
        for inp, result in records:
            evals_per_error.setdefault(result.error_no, []).append((inp, result))
        for error_no, inp_res_list in evals_per_error.items():
            # we use data structure from AutoTVM instead of AutoScheduler because it allows printing the error message
            err_str = repr(tvm.autotvm.measure.MeasureErrorNo(error_no))
            print(f'Error {err_str}, evaluations {len(inp_res_list)} out of {len(records)}')
        # now let's evaluate all candidates that produced no error
        correct_candidates = evals_per_error[tvm.auto_scheduler.measure.MeasureErrorNo.NO_ERROR]
        for instance_idx, (inp, result) in enumerate(correct_candidates):
            print(f'Evaluating candidate {instance_idx + 1} / {len(correct_candidates)}')
            schedule, func_args = task.compute_dag.apply_steps_from_state(inp.state, task.layout_rewrite_option)
            func = tvm.build(schedule, func_args, target)
            
            np_inp_args, np_out_args = ref_inputs(*shape_args)
            tvm_inp_args, tvm_out_args = ref_tvm_inputs(*np_inp_args, *np_out_args)
            
            func(*tvm_inp_args, *tvm_out_args)
            for n, t in zip(np_out_args, tvm_out_args):
                assert np.allclose(t.asnumpy(), n, rtol=1e-3, atol=1e-4)
            
        print('Correctness test passed for all candidates')
        exit(0)
    
    func(A_tvm, B_tvm, C_tvm)
    
    assert np.allclose(C_tvm.asnumpy(), C_np, rtol=1e-3, atol=1e-4)

    times = []
    for i in range(args.repeat):
        A_np = np.random.rand(K_val, M_val).astype("float32")
        B_np = np.random.rand(K_val, N_val).astype("float32")
        A_tvm = tvm.nd.array(A_np, device)
        B_tvm = tvm.nd.array(B_np, device)
        C_tvm = tvm.nd.empty(C_np.shape, device=device)
        t1 = time.time()
        func(A_tvm, B_tvm, C_tvm)
        t2 = time.time()
        if i >= args.warmup:
            times.append(t2 - t1)

    # times = timeit.repeat(lambda: func(A_tvm, B_tvm, C_tvm), repeat=args.repeat, number=1)[args.warmup:]
    times_str = " ".join(f"{time * 1e3:.3f}" for time in times)
    print(f"TVM {kernel.__name__} {args.size} trials={args.trials} time [ms]: avg {np.mean(times) * 1e3:.6f} std {np.std(times) * 1e3:.6f} times {times_str}")
