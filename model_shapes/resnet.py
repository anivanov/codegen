from transformers import AutoFeatureExtractor, ResNetForImageClassification
import torch
from datasets import load_dataset

feature_extractor = AutoFeatureExtractor.from_pretrained("microsoft/resnet-152")
model = ResNetForImageClassification.from_pretrained("microsoft/resnet-152")

print(model)