#include <cstdio>
#include <vector>
#include <random>
#include <dnnl.hpp>
#include "common.hpp"

int main() {
    using namespace dnnl;

    engine eng(engine::kind::cpu, 0);
    stream s(eng);

    const int M=4096, N=4096;
    memory::dims src_dims = {M, N};
    memory::dims gamma_dims = {M};

    std::vector<float> src_data(M * N);
    init_vector(src_data);
    std::vector<float> gamma_data(M);
    init_vector(gamma_data);
    std::vector<float> beta_data(M);
    init_vector(beta_data);
    std::vector<float> dst_data(M * N);

    memory::desc src_md(src_dims, memory::data_type::f32, memory::format_tag::ab);
    memory::desc gamma_md(gamma_dims, memory::data_type::f32, memory::format_tag::a);

    memory src_mem(src_md, eng, src_data.data());
    memory gamma_mem(gamma_md, eng, gamma_data.data());
    memory beta_mem(gamma_md, eng, beta_data.data());
    memory dst_mem(src_md, eng, dst_data.data());

    float eps = 1e-5f;

    auto pd = layer_normalization_forward::primitive_desc(
        eng,
        prop_kind::forward_inference, src_md, src_md, eps,
        normalization_flags::use_scale | normalization_flags::use_shift
    );

    layer_normalization_forward ln_fwd(pd);

    int warmup = 3;
    int repeats = 10;
    int number = 1;
    std::vector<double> times;
    for (int r = 0; r < warmup + repeats; r++) {
        init_vector(src_data);
        init_vector(gamma_data);
        init_vector(beta_data);
        init_vector(dst_data);
        TIC(1);
        for (int num = 0; num < number; num++) {
            ln_fwd.execute(s, {
                {DNNL_ARG_SRC, src_mem},
                {DNNL_ARG_SCALE, gamma_mem},
                {DNNL_ARG_SHIFT, beta_mem},
                {DNNL_ARG_DST, dst_mem}
            });
        }
        s.wait();
        double time = TOC(1) / number;
        times.push_back(time);
    }

    times = vec_remove_n(warmup, times);
    double mean = vec_mean(times);
    double std = vec_std(times);
    printf("OneDNN layernorm time [ms]: mean %.6f std %.6f reps %d\n", mean, std, (int)times.size());
    for (double t : times) { printf("%.3f ", t); }
    printf("\n");

    return 0;
}