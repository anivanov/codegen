#include <cstdio>
#include <vector>
#include <random>
#include <dnnl.hpp>
#include "common.hpp"


int main() {
    using namespace dnnl;

    engine eng(engine::kind::cpu, 0);
    stream s(eng);

    const int M = 4096, N = 4096;
    memory::dims src_dims = {M, N};
    memory::dims dst_dims = {M, 1};

    memory::desc src_md(src_dims, memory::data_type::f32, memory::format_tag::ab);
    memory::desc dst_md(dst_dims, memory::data_type::f32, memory::format_tag::ab);

    std::vector<float> src_data(M * N);
    init_vector(src_data);
    std::vector<float> dst_data(M);

    memory src_mem(src_md, eng, src_data.data());
    memory dst_mem(dst_md, eng, dst_data.data());

    float alpha = 0.0f;

    // unused
    float power = 0.0f;
    float eps = 0.0f;

    auto reduction_pd = reduction::primitive_desc(eng, algorithm::reduction_mean, src_md, dst_md, power, eps);
    auto reduction_prim = reduction(reduction_pd);

    int warmup = 3;
    int repeats = 10;
    int number = 1;
    std::vector<double> times;
    for (int r = 0; r < warmup + repeats; r++) {
        init_vector(src_data);
        init_vector(dst_data);
        TIC(1);
        for (int num = 0; num < number; num++) {
            reduction_prim.execute(s, {{DNNL_ARG_SRC, src_mem}, {DNNL_ARG_DST, dst_mem}});
            s.wait();
        }
        double time = TOC(1) / number;
        times.push_back(time);
    }

    times = vec_remove_n(warmup, times);
    double mean_time = vec_mean(times);
    double std_time = vec_std(times);
    printf("OneDNN reducemean time [ms]: avg %.6f std %.6f reps %d\n", mean_time, std_time, (int)times.size());
    for (double t : times) { printf("%.6f ", t); }
    printf("\n");

    return 0;
}