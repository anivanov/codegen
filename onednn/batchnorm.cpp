#include <cstdio>
#include <vector>
#include <dnnl.hpp>
#include "common.hpp"


int main() {
    using namespace dnnl;

    engine eng(engine::kind::cpu, 0);
    stream s(eng);

    const int N=8, C=3, H=2048, W=2048;
    memory::dims src_dims = {N, C, H, W};
    memory::dims gamma_dims = {C};

    std::vector<float> src_data(N * C * H * W);
    init_vector(src_data);
    std::vector<float> gamma_data(C);
    init_vector(gamma_data);
    std::vector<float> beta_data(C);
    init_vector(beta_data);
    std::vector<float> mean_data(C);
    init_vector(mean_data);
    std::vector<float> variance_data(C);
    init_vector(variance_data, 0, 1);
    std::vector<float> dst_data(N * C * H * W);

    memory::desc src_md(src_dims, memory::data_type::f32, memory::format_tag::nchw);
    memory::desc gamma_md(gamma_dims, memory::data_type::f32, memory::format_tag::x);

    memory src_mem(src_md, eng, src_data.data());
    memory gamma_mem(gamma_md, eng, gamma_data.data());
    memory beta_mem(gamma_md, eng, beta_data.data());
    memory mean_mem(gamma_md, eng, mean_data.data());
    memory variance_mem(gamma_md, eng, variance_data.data());
    memory dst_mem(src_md, eng, dst_data.data());

    float eps = 1e-5f;

    auto pd = batch_normalization_forward::primitive_desc(
        eng,
        prop_kind::forward_inference, src_md, src_md, eps,
        normalization_flags::use_scale | normalization_flags::use_shift
    );

    batch_normalization_forward bn_fwd(pd);
 
    int warmup = 3;
    int repeats = 10;
    int number = 1;
    std::vector<double> times;
    for (int r = 0; r < warmup + repeats; r++) {
        init_vector(src_data);
        init_vector(gamma_data);
        init_vector(beta_data);
        init_vector(mean_data);
        init_vector(variance_data, 0, 1);
        init_vector(dst_data);
        TIC(1);
        for (int num = 0; num < number; num++) {
            bn_fwd.execute(s, {
                {DNNL_ARG_SRC, src_mem},
                {DNNL_ARG_SCALE, gamma_mem},
                {DNNL_ARG_SHIFT, beta_mem},
                {DNNL_ARG_MEAN, mean_mem},
                {DNNL_ARG_VARIANCE, variance_mem},
                {DNNL_ARG_DST, dst_mem}
            });
        }
        s.wait();
        double time = TOC(1) / number;
        times.push_back(time);
    }

    times = vec_remove_n(warmup, times);
    double mean_time = vec_mean(times);
    double std_time = vec_std(times);

    printf("OneDNN batchnorm time [ms]: avg %.6f std %.6f reps %d\n", mean_time, std_time, (int)times.size());
    for (double t : times) { printf("%.6f ", t); }
    printf("\n");

    return 0;
}