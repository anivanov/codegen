#include <cstdio>
#include <vector>
#include <random>
#include <dnnl.hpp>
#include "common.hpp"

int main() {
    using namespace dnnl;

    engine eng(engine::kind::cpu, 0);
    stream s(eng);

    const int M = 4096, N = 4096;
    memory::dims src_dims = {M, N};

    std::vector<float> src_data(M * N, 1.0);
    init_vector(src_data);
    std::vector<float> dst_data(src_data.size(), 0.0);

    memory::desc src_md(src_dims, memory::data_type::f32, memory::format_tag::ab);
    memory src_mem(src_md, eng, src_data.data());
    memory dst_mem(src_md, eng, dst_data.data());

    float alpha = 0.0f;
    
    eltwise_forward::primitive_desc relu_pd(eng, prop_kind::forward_inference,
                                            algorithm::eltwise_relu, src_md, src_md, alpha);

    eltwise_forward relu_prim(relu_pd);

    int warmup = 3;
    int repeats = 10;
    int number = 1;
    std::vector<double> times;
    for (int r = 0; r < warmup + repeats; r++) {
        init_vector(src_data);
        init_vector(dst_data);
        TIC(1);
        for (int num = 0; num < number; num++) {
            relu_prim.execute(s, {{DNNL_ARG_SRC, src_mem}, {DNNL_ARG_DST, dst_mem}});
            s.wait();
        }
        double time = TOC(1) / number;
        times.push_back(time);
    }

    times = vec_remove_n(warmup, times);
    double mean_time = vec_mean(times);
    double std_time = vec_std(times);
    printf("OneDNN relu time [ms]: avg %.6f std %.6f reps %d\n", mean_time, std_time, (int)times.size());
    for (double t : times) { printf("%.6f ", t); }
    printf("\n");

    return 0;
}