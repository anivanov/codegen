#pragma once

#include <vector>
#include <time.h>
#include <random>

#if !defined(TOC)
static inline double elapsed_ms(struct timespec start) {
    struct timespec end;
    clock_gettime(CLOCK_MONOTONIC, &end);
    return (end.tv_sec - start.tv_sec) * 1e3 + (end.tv_nsec - start.tv_nsec) / 1e6;
}
#define TIC(name) struct timespec dt_ ##name; clock_gettime(CLOCK_MONOTONIC, &dt_ ##name)
#define TOC(name) elapsed_ms(dt_ ## name)
#endif

inline void init_vector(std::vector<float> &v, float min=-1, float max=1) {
    std::mt19937 gen;
    std::uniform_real_distribution<float> u(min, max);
    for (size_t i = 0; i < v.size(); i++) {
        v[i] = u(gen);
    }
}

inline double vec_mean(const std::vector<double> &v) {
    double sum = 0;
    for (size_t i = 0; i < v.size(); i++) {
        sum += v[i];
    }
    return sum / v.size();
}

inline double vec_std(const std::vector<double> &v) {
    double m = vec_mean(v);
    double sum = 0;
    for (size_t i = 0; i < v.size(); i++) {
        sum += (v[i] - m) * (v[i] - m);
    }
    return sqrt(sum / v.size());
}

inline std::vector<double> vec_remove_n(size_t n, const std::vector<double> &v) {
    // remove first n elements from vector
    std::vector<double> res;
    for (size_t i = n; i < v.size(); i++) {
        res.push_back(v[i]);
    }
    return res;
}
