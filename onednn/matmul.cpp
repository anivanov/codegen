#include <cstdio>
#include <vector>
#include <cstdlib>
#include <random>
#include <dnnl.hpp>
#include "common.hpp"

int main() {
    using namespace dnnl;

    engine eng(engine::kind::cpu, 0);
    stream s(eng);

    const int M = 768, K = 1024, N = 1024;
    memory::dims A_dims = {M, K};
    memory::dims B_dims = {K, N};
    memory::dims C_dims = {M, N};

    memory::desc A_md(A_dims, memory::data_type::f32, memory::format_tag::ba);
    memory::desc B_md(B_dims, memory::data_type::f32, memory::format_tag::ab);
    memory::desc C_md(C_dims, memory::data_type::f32, memory::format_tag::ab);

    std::vector<float> A_data(M * K);
    init_vector(A_data);
    std::vector<float> B_data(K * N);
    init_vector(B_data);
    std::vector<float> C_data(M * N);

    memory A_mem(A_md, eng, A_data.data());
    memory B_mem(B_md, eng, B_data.data());
    memory C_mem(C_md, eng, C_data.data());

    auto matmul_pd = matmul::primitive_desc(eng, A_md, B_md, C_md);
    auto matmul_prim = matmul(matmul_pd);

    int warmup = 5;
    int repeats = 10;
    int number = 1;
    std::vector<double> times;
    for (int r = 0; r < warmup + repeats; r++) {
        init_vector(A_data);
        init_vector(B_data);
        init_vector(C_data);
        TIC(1);
        for (int num = 0; num < number; num++) {
            matmul_prim.execute(s, {{DNNL_ARG_SRC, A_mem}, {DNNL_ARG_WEIGHTS, B_mem}, {DNNL_ARG_DST, C_mem}});
            s.wait();
        }
        double time = TOC(1) / number;
        times.push_back(time);
    }

    times = vec_remove_n(warmup, times);
    double mean_time = vec_mean(times);
    double std_time = vec_std(times);
    printf("OneDNN matmul time [ms]: avg %.6f std %.6f reps %d\n", mean_time, std_time, (int)times.size());
    for (double t : times) { printf("%.6f ", t); }
    printf("\n");

    return 0;
}