#include <cstdio>
#include <vector>
#include <dnnl.hpp>
#include <random>
#include "common.hpp"

int pooling_out_dim(int input, int kernel, int stride, int pad, int dilation) {
    return (input + 2 * pad - (dilation * (kernel - 1) + 1)) / stride + 1;
}

int main() {
    using namespace dnnl;

    engine eng(engine::kind::cpu, 0);
    stream s(eng);

    const int N=8, M=10, C=3, H_in=512, W_in=512, K=5;
    const int H = pooling_out_dim(H_in, K, 1, 0, 1);
    const int W = pooling_out_dim(W_in, K, 1, 0, 1);
    memory::dims src_dims = {N, C, H_in, W_in};
    memory::dims weight_dims = {M, C, K, K};
    memory::dims dst_dims = {N, M, H, W};

    memory::desc src_md(src_dims, memory::data_type::f32, memory::format_tag::nchw);
    memory::desc weight_md(weight_dims, memory::data_type::f32, memory::format_tag::oihw);
    memory::desc dst_md(dst_dims, memory::data_type::f32, memory::format_tag::nchw);

    std::vector<float> src_data(N * C * H_in * W_in);
    init_vector(src_data);
    std::vector<float> weight_data(M * C * K * K);
    init_vector(weight_data);
    std::vector<float> dst_data(N * M * H * W);

    memory src_mem(src_md, eng, src_data.data());
    memory weight_mem(weight_md, eng, weight_data.data());
    memory dst_mem(dst_md, eng, dst_data.data());

    memory::dims strides = {1, 1};
    memory::dims padding = {0, 0};

    auto conv_pd = convolution_forward::primitive_desc(
        eng,
        prop_kind::forward_inference,
        algorithm::convolution_direct,
        src_md,
        weight_md,
        dst_md,
        strides,
        padding,
        padding
    );

    convolution_forward conv_fwd(conv_pd);

    int warmup = 5;
    int repeats = 10;
    int number = 1;
    std::vector<double> times;
    for (int r = 0; r < warmup + repeats; r++) {
        init_vector(src_data);
        init_vector(weight_data);
        init_vector(dst_data);
        TIC(1);
        for (int num = 0; num < number; num++) {
            conv_fwd.execute(s, {
                {DNNL_ARG_SRC, src_mem},
                {DNNL_ARG_WEIGHTS, weight_mem},
                {DNNL_ARG_DST, dst_mem}
            });
        }
        s.wait();
        double time = TOC(1) / number;
        times.push_back(time);
    }

    times = vec_remove_n(warmup, times);
    double mean_time = vec_mean(times);
    double std_time = vec_std(times);
    printf("OneDNN conv time [ms]: avg %.6f std %.6f reps %d\n", mean_time, std_time, (int)times.size());
    for (double t : times) { printf("%.6f ", t); }
    printf("\n");

    return 0;
}