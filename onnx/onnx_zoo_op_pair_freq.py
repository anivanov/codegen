

from onnx import hub
import onnx
import onnxruntime as ort
import re
import numpy as np
import os
import glob
import pathlib
import json
from onnx_zoo_op_frequency import find_latest_unique_hub_models


if __name__ == "__main__":
    latest_models = find_latest_unique_hub_models()

    all_op_pairs = {}

    for i, name in enumerate(latest_models):
        model_info = hub.get_model_info(model=name)
        model = hub.load(name)

        # for each array, find the next and previous operators
        next_op = {}
        prev_op = {}
        for x in model.graph.node:
            for inp in x.input:
                next_op[inp] = x.op_type
            for out in x.output:
                prev_op[out] = x.op_type
                
        # find pairs of operators
        op_pairs = {}
        intermediates = set(next_op.keys()) & set(prev_op.keys())
        for i in intermediates:
            op1 = prev_op[i]
            op2 = next_op[i]
            key = (op1, op2)
            op_pairs.setdefault(key, 0)
            op_pairs[key] += 1
        
        # output the pairs for current model
        op_pairs = sorted(op_pairs.items(), key=lambda x: x[1], reverse=True)
        print(f"Model {name}:")
        for pair, count in op_pairs:
            print(f"{pair}: {count}")    

        # merge the pairs into the global dictionary
        for pair, count in op_pairs:
            all_op_pairs.setdefault(pair, 0)
            all_op_pairs[pair] += count
            
    # output the global pairs
    all_op_pairs = sorted(all_op_pairs.items(), key=lambda x: x[1], reverse=True)
    print(f"All models:")
    for pair, count in all_op_pairs:
        print(f"{pair}: {count}")

