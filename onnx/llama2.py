import torch
from transformers import AutoTokenizer, AutoModelForCausalLM, AutoConfig
import onnx
from torch.onnx import export

# 1. Download the LLaMA 2 7B model and tokenizer
model_name = "meta-llama/Llama-2-7b-hf"
tokenizer = AutoTokenizer.from_pretrained(model_name)
config = AutoConfig.from_pretrained(model_name)
model = AutoModelForCausalLM.from_config(config)

for param in model.parameters():
    param.data.fill_(0)

# 2. Set up fixed input dimensions (batch size = 8, sequence length = 1024)
batch_size = 1
sequence_length = 1024

# 3. Prepare dummy inputs with the specified batch size and sequence length
dummy_input_ids = torch.randint(0, tokenizer.vocab_size, (batch_size, sequence_length), dtype=torch.int64)
dummy_attention_mask = torch.ones((batch_size, sequence_length), dtype=torch.int64)  # Mask of 1s for active tokens

# 4. Convert the model to ONNX with fixed batch size and sequence length
onnx_model_path = f"llama2_7b_fixed_{batch_size}x{sequence_length}.onnx"

# Export with fixed shapes (batch size = 8, sequence length = 1024)
export(
    model,
    (dummy_input_ids, dummy_attention_mask),
    onnx_model_path,
    input_names=["input_ids", "attention_mask"],
    output_names=["logits"],
    opset_version=17,  # You can adjust the ONNX opset version if needed
    dynamic_axes=None  # No dynamic axes, fixed batch size and sequence length
)

print(f"Model exported to ONNX at {onnx_model_path}")