from onnx import hub

print('Text models:')
print(len(hub.list_models(tags=["text"])))

print('Vision models:')
print(len(hub.list_models(tags=["vision"])))

print("Classification models:")
print(len(hub.list_models(tags=["classification"])))