

from onnx import hub
import onnx
import onnxruntime as ort
import re
import numpy as np
import os
import glob
import pathlib
import json


def find_latest_unique_hub_models():

    all_models = hub.list_models()

    print(f"Total models: {len(all_models)}")
    print(f"{sorted([m.model for m in all_models])}")

    # avoid duplicates to reduce loading time
    unique_models = set(m.model for m in all_models)

    base_models = set()
    for n in unique_models:
        is_base = True
        for n1 in unique_models:
            if n1 in n and n1 != n:
                is_base = False
                break
        if is_base:
            base_models.add(n)
            
    print(f"Base models: {len(base_models)}")
    print(f"{sorted(base_models)}")

    latest_models = set()
    pattern = re.compile(r'(.*[^0-9.])([0-9.]+)$')
    for n in base_models:
        is_latest = True
        # separate name and integer/floating point numbers at the end
        m = re.match(pattern, n)
        if m is None:
            latest_models.add(n)
            continue  # skip checking without version
        n_without_version, n_version = m.groups()    
        for n1 in base_models:
            m1 = re.match(pattern, n1)
            if m1 is None:
                continue  # probably n1 is an earlier version or something else
            n1_without_version, n1_version = m1.groups()
            if n_without_version == n1_without_version and n1_version > n_version:  # ok to compare strings
                is_latest = False
                break
        if is_latest:
            latest_models.add(n)

    latest_models = sorted(latest_models)
    print(f"Latest models: {len(latest_models)}")
    print(f"{latest_models}")
    return latest_models



if __name__ == '__main__':
    latest_models = find_latest_unique_hub_models()
    
    all_ops_counter = {}

    for i, name in enumerate(latest_models):
        model_info = hub.get_model_info(model=name)
        model = hub.load(name)

        ops_counter = {}

        # count the operators
        for x in model.graph.node:
            ops_counter.setdefault(x.op_type, 0)
            ops_counter[x.op_type] += 1
        
        print(f"Model {name}:")
            
        # sort the operators by frequency
        sorted_ops = sorted(ops_counter.items(), key=lambda x: x[1], reverse=True)
        for name, value in sorted_ops:
            print(f"{name}: {value}")
            
        for op, count in ops_counter.items():
            all_ops_counter.setdefault(op, 0)
            all_ops_counter[op] += count
        
    print(f"All models:")
    # sort the operators by frequency
    sorted_ops = sorted(ops_counter.items(), key=lambda x: x[1], reverse=True)
    for name, value in sorted_ops:
        print(f"{name}: {value}")

