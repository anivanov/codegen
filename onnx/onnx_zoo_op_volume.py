

from onnx import hub
import onnx
import onnxruntime as ort
import re
import numpy as np
import os
import glob
import pathlib
import json
from onnx_zoo_op_frequency import find_latest_unique_hub_models


latest_models = find_latest_unique_hub_models()


# for every operator, find how many times it appears in the models


# default_params = {
#     'height': 512,
#     'width': 512,
#     'batch': 4,
# }


for i, name in enumerate(latest_models):
    if name in ('BiDAF', 'ResNet-preproc'):
        continue  # broken, ONNX can't load inputs
    model_info = hub.get_model_info(model=name)
    # print(f"Model info: {model_info}")
    model_dir = hub.download_model_with_test_data(name)

    if 'ShuffleNet-v2' == name:
        model_dir = os.path.join(model_dir, 'test_shufflenetv2')  # ONNX hub bug
    [model_onnx_path] = glob.glob(os.path.join(model_dir, '*.onnx'))
    model = onnx.load(model_onnx_path)
        
    test_data_dir = pathlib.Path(model_dir) / 'test_data_set_0'
    inputs = {}
    inputs_num = len(glob.glob(os.path.join(test_data_dir, 'input_*.pb')))
    input_names = hub.get_model_info(model=name).metadata['io_ports']['inputs']
    assert inputs_num == len(input_names)
    for i, input_dict in enumerate(input_names):
        input_name = input_dict['name']
        input_file = os.path.join(test_data_dir, 'input_{}.pb'.format(i))
        tensor = onnx.TensorProto()
        with open(input_file, 'rb') as f:
            tensor.ParseFromString(f.read())
        inputs[input_name] = onnx.numpy_helper.to_array(tensor)
        [graph_input] = [x for x in model.graph.input if x.name == input_name]
        graph_input.type.CopyFrom(onnx.helper.make_tensor_type_proto(elem_type=onnx.helper.np_dtype_to_tensor_dtype(inputs[input_name].dtype), shape=inputs[input_name].shape))

    onnx.shape_inference.infer_shapes(model)

    input_names = set(x.name for x in model.graph.input)
    print("input_names:", input_names)
    output_names = set(x.name for x in model.graph.output)
    print("output_names:", output_names)
    init_names = set(x.name for x in model.graph.initializer)
    print("init_names:", init_names)
    real_input_names = set(input_names) - set(init_names)
    print("real_input_names:", real_input_names)
    
    interm_input_names = set()
    interm_output_names = set()
    for x in model.graph.node:
        interm_input_names.update(x.input)
        interm_output_names.update(x.output)
    interm_inout_names = interm_input_names & interm_output_names
    print('interm_input_names:', interm_input_names)
    interm_onlyout_names = interm_output_names - interm_inout_names
    print('interm_onlyout_names:', interm_onlyout_names)
    unused_output_names = interm_onlyout_names - output_names
    print('unused_output_names:', unused_output_names)
    used_names = (interm_input_names | interm_output_names) - unused_output_names
    print('used_names:', used_names)
    
    inferred_names = set(x.name for x in model.graph.value_info)
    print('inferred_names:', inferred_names)
    unknown_names = used_names - output_names - input_names - init_names
    print('unknown_names:', unknown_names)
    unknown_after_inference = unknown_names - inferred_names
    print('unknown_after_inference:', unknown_after_inference)

    # determine names of intermediate tensors to extract
    assert set(output_names) == set([x['name'] for x in hub.get_model_info(model=name).metadata['io_ports']['outputs']])
    assert real_input_names == set([x['name'] for x in hub.get_model_info(model=name).metadata['io_ports']['inputs']])
    
    assert not unknown_after_inference
    
    graph_outputs = set([x.name for x in model.graph.output])
    shapes_to_extract = set()
    for x in model.graph.node:
        node_outputs = set(x.output) - graph_outputs
        shapes_to_extract.update(node_outputs)
    shapes_to_extract = {x for x in shapes_to_extract if x in model.graph.value_info}
    shapes_to_extract = sorted(shapes_to_extract)
    shape_to_dtype = {x.name: x.type.tensor_type.elem_type for x in model.graph.value_info}
    for s in shapes_to_extract:
        if s not in shape_to_dtype:
            continue
        intermediate_node_output = onnx.helper.make_tensor_value_info(
            name=s, elem_type=shape_to_dtype[s], shape=None)
        model.graph.output.extend([intermediate_node_output])
        
    # model_inputs = hub.get_model_info(model=name).metadata['io_ports']['inputs']
    # dummy_inputs = {}
    # import pdb; pdb.set_trace()
    # for x in model_inputs:
    #     input_name = x['name']
    #     input_shape = x['shape']
    #     input_dtype = type_converter[x['type']]
    #     dummy_inputs[input_name] = np.random.randn(*input_shape).astype(input_dtype)
    # for input_info in model.graph.input:
    #     input_name = input_info.name
    #     input_shape = [(dim.dim_value or default_params[dim.dim_param]) for dim in input_info.type.tensor_type.shape.dim]
    #     input_dtype = onnx.mapping.TENSOR_TYPE_TO_NP_TYPE[input_info.type.tensor_type.elem_type]
    #     dummy_inputs[input_name] = np.random.randn(*input_shape).astype(input_dtype)
    
    
    
    output_names = [x['name'] for x in hub.get_model_info(model=name).metadata['io_ports']['outputs']]
    
    # run inference to collect intermediate tensors
    options = ort.SessionOptions()
    # options.enable_profiling = True
    ort_session = ort.InferenceSession(model.SerializeToString(), sess_options=options)
    intermediate_outputs = ort_session.run(output_names + shapes_to_extract, inputs)
    # profile_file = ort_session.end_profiling()
    
    collected_shapes = {n: s.shape for n, s in zip(output_names + shapes_to_extract, intermediate_outputs)}
    
    # with open(profile_file, 'r') as f:
    #     profiling_data = json.load(f)
    # os.remove(profile_file)
        
    # extract intermediate tensors
    # intermediate_shapes = []
    # nodes_with_shapes = {}
    # for x in profiling_data:
    #     try:
    #         op_name = x['args']['op_name']
    #         input_type_shape = x['args']['input_type_shape']
    #         output_type_shape = x['args']['output_type_shape']
    #         nodes_with_shapes.setdefault(op_name, [])
    #         nodes_with_shapes[op_name].append([*output_type_shape, *input_type_shape])
    #         intermediate_shapes.append((op_name,[*output_type_shape, *input_type_shape]))
    #     except KeyError:
    #         pass
             

