#!/bin/bash

export CC=clang
export LDFLAGS="-lm"
export CFLAGS="-O3 -fopenmp -march=native -I $(dirname $(which clang))/../include/"
# export CFLAGS="-O3 -fopenmp -march=sapphirerapids -I $(dirname $(which clang))/../include/"
export LD_LIBRARY_PATH="$(dirname $(which clang))/../lib/"
export OMP_PROC_BIND=close
export TOKENIZERS_PARALLELISM=false
