#!/bin/bash

module load sarus
export DOCKER=sarus
export DOCKER_OPTS=
export VERILATOR_THREADS=16
export NUMACTL_PREFIX="numactl -m 0 -C 0-15"
