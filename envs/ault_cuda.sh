#!/bin/bash

module load cmake/3.21.1
module load cuda
module load gcc
export CC=gcc
export LDFLAGS="-L$(dirname $(which nvcc))/../lib64/ -lcudart -lm"
export CFLAGS="-O3 -I ."
