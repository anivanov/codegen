import jax
import numpy as np
from common import time_prof
    
M = 768
N = 1024
K = 1024

x = np.random.uniform(-1, 1, (K, M)).astype(np.float32)
y = np.random.uniform(-1, 1, (K, N)).astype(np.float32)

@jax.jit
def jax_matmul(x, y):
    return jax.numpy.einsum("km,kn->mn", x, y)

jx = jax.numpy.asarray(x, copy=False)
jy = jax.numpy.asarray(y, copy=False)

def init_src():
    x[:] = np.random.uniform(-1, 1, (K, M)).astype(np.float32)
    y[:] = np.random.uniform(-1, 1, (K, N)).astype(np.float32)

jax_mean, jax_std, jax_times = time_prof(100, lambda: jax_matmul(jx, jy).block_until_ready(), init_src)
print(f"JAX matmul time [ms]: avg {jax_mean * 1e3:.6f} std {jax_std * 1e3:.6f} reps {len(jax_times)}")
