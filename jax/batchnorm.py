import jax
import numpy as np
from common import time_prof
import jax.numpy as jnp


def ref_batchnorm(X, mean, var, scale, bias):
    eps = 1e-5
    Y = (X - mean[None, :, None, None]) * scale[None, :, None, None] / np.sqrt(var[None, :, None, None] + eps) + bias[None, :, None, None]
    return Y


@jax.jit
def jax_batchnorm_inference(X, mean, var, scale, bias):
    eps = 1e-5
    t0 = scale / jnp.sqrt(var + eps)
    t1 = - mean * t0 + bias
    Y = X * t0[None, :, None, None] + t1[None, :, None, None]
    return Y


def run_batchnorm_jax(N, C, H, W):
    x = np.random.uniform(-1, 1, (N, C, H, W)).astype(np.float32)
    scale = np.random.uniform(-1, 1, (C,)).astype(np.float32)
    bias = np.random.uniform(-1, 1, (C,)).astype(np.float32)
    mean = np.random.uniform(-1, 1, (C,)).astype(np.float32)
    var = np.random.uniform(0, 1, (C,)).astype(np.float32)
    
    def init_src():
        x[:] = np.random.uniform(-1, 1, (N, C, H, W)).astype(np.float32)
        scale[:] = np.random.uniform(-1, 1, (C,)).astype(np.float32)
        bias[:] = np.random.uniform(-1, 1, (C,)).astype(np.float32)
        mean[:] = np.random.uniform(-1, 1, (C,)).astype(np.float32)
        var[:] = np.random.uniform(0, 1, (C,)).astype(np.float32)
    
    jx = jax.numpy.asarray(x, copy=False)
    jscale = jax.numpy.asarray(scale, copy=False)
    jbias = jax.numpy.asarray(bias, copy=False)
    jmean = jax.numpy.asarray(mean, copy=False)
    jvar = jax.numpy.asarray(var, copy=False)
    
    jax_mean, jax_std, jax_times = time_prof(10, lambda: jax_batchnorm_inference(jx, jmean, jvar, jscale, jbias).block_until_ready(), init_src)
    print(f"JAX batchnorm time [ms]: avg {jax_mean * 1e3:.6f} std {jax_std * 1e3:.6f} reps {len(jax_times)}")
    
    assert np.allclose(jax_batchnorm_inference(jx, jmean, jvar, jscale, jbias), ref_batchnorm(x, mean, var, scale, bias), rtol=1e-3, atol=1e-4)


if __name__ == "__main__":
    run_batchnorm_jax(8, 3, 2048, 2048)