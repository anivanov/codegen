import jax
import numpy as np
from common import time_prof
import jax.numpy as jnp


@jax.jit
def jax_layernorm(x, gamma, beta):
    m = jax.numpy.sum(x, axis=1)
    mu = m / x.shape[1]
    diff = x - mu[:, None]
    q = jax.numpy.sum(diff ** 2, axis=1)
    qeps = q * (1. / (x.shape[1] - 1)) + 0.00001
    sigma = 1 / jax.numpy.sqrt(qeps)
    return sigma[:, None] * diff * gamma + beta


def run_layernorm_jax(M, N):
    src = np.random.uniform(-1, 1, (M, N)).astype(np.float32)
    gamma = np.random.uniform(-1, 1, (N,)).astype(np.float32)
    beta = np.random.uniform(-1, 1, (N,)).astype(np.float32)
    
    def init_src():
        src[:] = np.random.uniform(-1, 1, (M, N)).astype(np.float32)
        gamma[:] = np.random.uniform(-1, 1, (N,)).astype(np.float32)
        beta[:] = np.random.uniform(-1, 1, (N,)).astype(np.float32)
        
    jsrc = jax.numpy.asarray(src, copy=False)
    jgamma = jax.numpy.asarray(gamma, copy=False)
    jbeta = jax.numpy.asarray(beta, copy=False)
        
    jax_mean, jax_std, jax_times = time_prof(10, lambda: jax_layernorm(jsrc, jgamma, jbeta).block_until_ready(), init_src)
    print(f"JAX layernorm time [ms]: avg {jax_mean * 1e3:.6f} std {jax_std * 1e3:.6f} reps {len(jax_times)}")


if __name__ == "__main__":
    run_layernorm_jax(4096, 4096)
