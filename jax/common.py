import math
from time import time
import numpy as np


def time_prof(repeats, func, init=lambda: (), number=1, warmup=0.3, time_budget=0.1):    
    times = []
    
    min_warmup_repeats = math.ceil(repeats * warmup)
    min_total_repeats = min_warmup_repeats + repeats
        
    i = 0
    total_elapsed = 0
    while True:
        init()
        t1 = time()
        for _ in range(number):
            func()
        t2 = time()
        elapsed = (t2 - t1) / number
        total_elapsed += elapsed
        times.append(elapsed)
        i += 1
        if total_elapsed > time_budget and i >= min_total_repeats:
            break
    
    assert len(times) >= min_total_repeats
    times = times[math.ceil((len(times) - min_warmup_repeats) * warmup):]
    assert len(times) >= repeats
    
    mean = np.mean(times)
    std = np.std(times)
    
    return mean, std, times