import jax
import numpy as np
from common import time_prof
import jax.numpy as jnp


def run_reducemean_jax(M, N):   
    x = np.random.uniform(-10, 10, (M, N)).astype(np.float32)

    def init_src():
        x[:] = np.random.uniform(-10, 10, (M, N)).astype(np.float32)
    
    jx = jax.numpy.asarray(x, copy=False)
        
    jax_reducemean = jax.jit(lambda x: jnp.mean(x, axis=1))
        
    jax_mean, jax_std, jax_times = time_prof(10, lambda: jax_reducemean(jx).block_until_ready(), init_src)
    print(f"JAX reducemean time [ms]: avg {jax_mean * 1e3:.6f} std {jax_std * 1e3:.6f} reps {len(jax_times)}")


if __name__ == "__main__":
    run_reducemean_jax(4096, 4096)
