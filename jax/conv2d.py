import jax
import numpy as np
from common import time_prof
import jax.numpy as jnp


def run_conv_jax(N, M, C, H_in, W_in, K):

    x = np.random.uniform(-10, 10, (N, C, H_in, W_in)).astype(np.float32)
    w = np.random.uniform(-10, 10, (M, C, K, K)).astype(np.float32)

    def init_src():
        x[:] = np.random.uniform(-10, 10, (N, C, H_in, W_in)).astype(np.float32)
        w[:] = np.random.uniform(-10, 10, (M, C, K, K)).astype(np.float32)
        
    jx = jax.numpy.asarray(x, copy=False)
    jw = jax.numpy.asarray(w, copy=False)
        
    jax_conv = jax.jit(lambda x, w: jax.lax.conv(x, w, (1, 1), 'VALID'))
        
    jax_mean, jax_std, jax_times = time_prof(10, lambda: jax_conv(jx, jw).block_until_ready(), init_src)
    print(f"JAX conv time [ms]: avg {jax_mean * 1e3:.6f} std {jax_std * 1e3:.6f} reps {len(jax_times)}")


if __name__ == "__main__":
    run_conv_jax(N=8, M=10, C=3, H_in=512, W_in=512, K=5)
