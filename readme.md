# Transformation-Centric Micro-Kernel Optimizations

### Prerequisites

* Container engine such as docker or podman.

* Verilator model of snitch cluster:

```
make snitch_cluster.vlt
```

* Snitch runtime:

```
make snRuntime-gcc-build/libsnRuntime-cluster.a
```