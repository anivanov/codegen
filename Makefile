DOCKER ?= podman
DOCKER_OPTS ?= --security-opt label=disable --rm
DOCKER_RUN ?= $(DOCKER) run $(DOCKER_OPTS)
DOCKER_IMG ?= ghcr.io/pulp-platform/snitch@sha256:f43d2db7c97bdcd653deb567664032940e8d9051a80a52c22f239e19fe4c310b
VERILATOR_THREADS ?= 4

.SECONDARY: 
# no target is removed because it is considered intermediate

.SECONDEXPANSION:
#https://www.gnu.org/software/make/manual/make.html#Secondary-Expansion


.PHONY: clean
clean:
	rm -rf snRuntime-build logs dnn_kernels/build-cluster dnn_kernels/build-banshee


snRuntime-build/libsnRuntime-cluster.a snRuntime-build/libsnRuntime-banshee.a:
	$(DOCKER_RUN)  \
		--mount type=bind,source=`pwd`,target=/repo \
		-w /repo \
		$(DOCKER_IMG) \
		/bin/bash -c "\
			rm -rf /repo/snRuntime-build/ && \
			mkdir -p /repo/snRuntime-build && \
			cd /repo/snRuntime-build && \
			cmake /repo/snitch/sw/snRuntime \
				-DSNITCH_BANSHEE=/repo/banshee \
				-DSNITCH_SIMULATOR=/repo/snitch_cluster.vlt \
				-DBUILD_TESTS=ON \
				-DSNITCH_RUNTIME=snRuntime-cluster \
				-DCMAKE_TOOLCHAIN_FILE=toolchain-llvm && \
			cmake --build . -j `nproc`"


snRuntime-gcc-build/libsnRuntime-cluster.a snRuntime-gcc-build/libsnRuntime-banshee.a:
	$(DOCKER_RUN)  \
		--mount type=bind,source=`pwd`,target=/repo \
		-w /repo \
		$(DOCKER_IMG) \
		/bin/bash -c "\
			rm -rf /repo/snRuntime-gcc-build/ && \
			mkdir -p /repo/snRuntime-gcc-build && \
			cd /repo/snRuntime-gcc-build && \
			cmake /repo/snitch/sw/snRuntime \
				-DSNITCH_BANSHEE=/repo/banshee \
				-DSNITCH_SIMULATOR=/repo/snitch_cluster.vlt \
				-DBUILD_TESTS=ON \
				-DSNITCH_RUNTIME=snRuntime-cluster \
				-DCMAKE_TOOLCHAIN_FILE=toolchain-gcc && \
			cmake --build . -j `nproc`"


snrt_mini.a: snrt_mini.c
	$(DOCKER_RUN)  \
		--mount type=bind,source=`pwd`,target=/repo \
		-w /repo \
		$(DOCKER_IMG) \
		/bin/bash -c "\
			/tools/riscv/bin/riscv32-unknown-elf-gcc \
			-O3 -march=rv32imafd -mabi=ilp32d -mcmodel=medany -ffast-math -fno-builtin-printf -fno-common \
            -nostdinc -ffixed-ft0 -ffixed-ft1 -ffixed-ft2 \
			-c snrt_mini.c -o snrt_mini.o && \
			/tools/riscv/bin/riscv32-unknown-elf-gcc-ar rcs snrt_mini.a snrt_mini.o"


# old and tested version
# snitch_cluster.vlt:
# 	$(DOCKER_RUN)  \
# 		--mount type=bind,source=`pwd`,target=/repo \
# 		-w /repo \
# 		$(DOCKER_IMG) /bin/bash -c "\
# 		mkdir /workspace && cd /workspace && \
# 		git clone https://github.com/pulp-platform/snitch.git && \
# 		cd snitch/hw/system/snitch_cluster && \
# 		git checkout ed24b24 && \
# 		sed -i -e 's;isa: \"rv32ima\";isa: \"rv32imafd\";g' -e 's;// Xdiv_sqrt: true;Xdiv_sqrt: true;g' cfg/cluster.default.hjson && \
# 		VLT_FLAGS='--threads $(VERILATOR_THREADS)' VLT_COBJ=work-vlt/vlt/verilated_threads.o VLT_CFLAGS=-DVL_THREADED make bin/snitch_cluster.vlt && \
# 		cp bin/snitch_cluster.vlt /repo/snitch_cluster.vlt"


# multithreaded version, faster but known to hang (race conditions?)
# snitch_cluster.vlt:
# 	$(DOCKER_RUN)  \
# 		--mount type=bind,source=`pwd`,target=/repo \
# 		-w /repo \
# 		ghcr.io/pulp-platform/snitch_cluster:main@sha256:d3ddea7f6a69ffb422a1b6c1cf70f27362795297363b6068bbaece2dc32a11fd /bin/bash -c "\
# 		mkdir /workspace && cd /workspace && \
# 		git clone https://github.com/pulp-platform/snitch_cluster.git && \
# 		cd snitch_cluster && \
# 		git checkout 11d0534 && \
# 		sed -i -e 's;wget;wget --no-check-certificate;g' target/common/common.mk && \
# 		VLT_FLAGS='--threads $(VERILATOR_THREADS)' VLT_COBJ=work-vlt/vlt/verilated_threads.o VLT_CFLAGS=-DVL_THREADED \
# 		VLT_BENDER=-DCOMMON_CELLS_ASSERTS_OFF CFG_OVERRIDE=cfg/fdiv.hjson \
# 		make -C target/snitch_cluster bin/snitch_cluster.vlt && \
# 		cp target/snitch_cluster/bin/snitch_cluster.vlt /repo/snitch_cluster.vlt"


# single threaded version
snitch_cluster.vlt:
	$(DOCKER_RUN)  \
		--mount type=bind,source=`pwd`,target=/repo \
		-w /repo \
		ghcr.io/pulp-platform/snitch_cluster:main@sha256:d3ddea7f6a69ffb422a1b6c1cf70f27362795297363b6068bbaece2dc32a11fd /bin/bash -c "\
		mkdir /workspace && cd /workspace && \
		git clone https://github.com/pulp-platform/snitch_cluster.git && \
		cd snitch_cluster && \
		git checkout 11d0534 && \
		sed -i -e 's;wget;wget --no-check-certificate;g' target/common/common.mk && \
		VLT_BENDER=-DCOMMON_CELLS_ASSERTS_OFF CFG_OVERRIDE=cfg/fdiv.hjson \
		make -C target/snitch_cluster bin/snitch_cluster.vlt && \
		cp target/snitch_cluster/bin/snitch_cluster.vlt /repo/snitch_cluster.vlt"

