#!/bin/bash

SCRIPT_DIR=$(dirname "$(realpath "$0")")
CODE_ROOT=$(realpath "$SCRIPT_DIR/..")

# plots for intro

KERNELS=(
    "conv 8x64x64x56x56x3"
    "bmm 192x512x128x512"
)


for kernel_size in "${KERNELS[@]}"; do
    IFS=' ' read kernel_name kernel_size <<< "$kernel_size"
    # python "$CODE_ROOT/plots/process_my_search.py" "$CODE_ROOT/microkernels/logs/daint/search/${kernel_name}_${kernel_size}_greedy_top_heur.log" "$CODE_ROOT/plots/intro_data/${kernel_name}_${kernel_size}_greedy_top_heur.csv"
    # python "$CODE_ROOT/plots/process_tvm_search.py" "$CODE_ROOT/tvm/${kernel_name}_${kernel_size}.json" "$CODE_ROOT/plots/intro_data/${kernel_name}_${kernel_size}_tvm.csv"
    # python "$CODE_ROOT/plots/plot_search_curve.py" --kernel "$kernel_name" --size "$kernel_size" --my "$CODE_ROOT/plots/data/${kernel_name}_${kernel_size}_greedy_top_heur.csv" --tvm "$CODE_ROOT/plots/data/${kernel_name}_${kernel_size}_tvm.csv" --output "$CODE_ROOT/plots/figures/${kernel_name}_${kernel_size}_search_curve.png"
done

# python "$CODE_ROOT/plots/plot_intro_search_curves.py" --data "$CODE_ROOT/plots/intro_data" --output "$CODE_ROOT/plots/figures/intro_curves.png"
python "$CODE_ROOT/plots/plot_intro_search_curves.py" --data "$CODE_ROOT/plots/intro_data" --output "$CODE_ROOT/plots/figures/intro_curves.pdf"




KERNELS=(
    # "add 3072x4096"
    "batchnorm 8x3x2048x2048"
    "batchnorm 8x64x300x300"
    # "bmm 192x512x128x512"  # in intro
    "conv 8x10x3x512x512x5"
    # "conv 8x64x64x56x56x3"  # in intro
    "layernorm 16384x1024"
    "layernorm 4096x4096"
    "matmul 3072x4096x4096"  # in intro
    "matmul 768x1024x1024"
    # "mul 3072x4096"
    "reducemean 4096x4096"
    # "relu 8x64x112x112"
    # relu 4096x4096"  # remove to fit 4x4 figure
    "rmsnorm 3072x4096"
    "softmax 24576x512"
    # "swiglu 4x512x768x3072"
    "swiglu 1x256x4096x14336"
    "relu_ffn 4x512x768x3072"
)

for kernel_size in "${KERNELS[@]}"; do
    IFS=' ' read kernel_name kernel_size <<< "$kernel_size"
    # python "$CODE_ROOT/plots/process_my_search.py" "$CODE_ROOT/microkernels/logs/daint/search/${kernel_name}_${kernel_size}_greedy_top_heur.log" "$CODE_ROOT/plots/data/${kernel_name}_${kernel_size}_greedy_top_heur.csv"
    # python "$CODE_ROOT/plots/process_tvm_search.py" "$CODE_ROOT/tvm/${kernel_name}_${kernel_size}.json" "$CODE_ROOT/plots/data/${kernel_name}_${kernel_size}_tvm.csv"
    # python "$CODE_ROOT/plots/plot_search_curve.py" --kernel "$kernel_name" --size "$kernel_size" --my "$CODE_ROOT/plots/data/${kernel_name}_${kernel_size}_greedy_top_heur.csv" --tvm "$CODE_ROOT/plots/data/${kernel_name}_${kernel_size}_tvm.csv" --output "$CODE_ROOT/plots/figures/${kernel_name}_${kernel_size}_search_curve.png"
done

# python "$CODE_ROOT/plots/plot_all_search_curves.py" --data "$CODE_ROOT/plots/data" --output "$CODE_ROOT/plots/figures/all_curves.png"
python "$CODE_ROOT/plots/plot_all_search_curves.py" --data "$CODE_ROOT/plots/data" --output "$CODE_ROOT/plots/figures/all_curves.pdf"


