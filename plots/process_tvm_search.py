import re
import argparse
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import math
import pathlib
from cycler import cycler
import csv
import json


parser = argparse.ArgumentParser(description='Process search runtimes')
parser.add_argument('input', type=str, help='Input file')
parser.add_argument('output', type=str, help='Output file')

args = parser.parse_args()

# 0 - ok, 1 - timeout, 2 - error, 3 - build timeout
# TVM: https://github.com/apache/tvm/blob/b38417cd0047dc27d562b63bfac9f93227db3491/include/tvm/auto_scheduler/measure.h#L57
status_code_map = {
    0: 0,
    7: 1,  # run timeout
    4: 2,  # runtime error
    6: 3,  # build timeout
}

with open(args.input, "r", encoding="utf8", errors='ignore') as f:
    evals = []
    for line in f:
        data = json.loads(line)
        runtimes, status, _, _ = data['r']
        if status != 0:
            runtimes = [1e10]
        # convert from s to ms
        runtimes = [r * 1e3 for r in runtimes]
        mean = np.mean(runtimes)
        std = np.std(runtimes)
        evals.append((status_code_map[status], mean, std))

with open(args.output, 'w', newline='') as csvfile:
    writer = csv.writer(csvfile)
    writer.writerow(["status", "mean", "std"])
    for status, avg, std in evals:
        writer.writerow([status, avg, std])
