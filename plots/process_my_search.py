import re
import argparse
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import math
import pathlib
from cycler import cycler
import csv


parser = argparse.ArgumentParser(description='Process search runtimes')
parser.add_argument('input', type=str, help='Input file')
parser.add_argument('output', type=str, help='Output file')

args = parser.parse_args()

# 0 - ok, 1 - timeout, 2 - error
status_code_map = {
    "measured": 0,
    "from cache": 0,
    "timed out": 1,
    "runtime error": 2,
}

with open(args.input, "r", encoding="utf8", errors='ignore') as f:
    evals = []
    for line in f:
        # Runtime (from cache) [ms]: avg 0.978630 std 0.230425
        # Runtime (timed out) [ms]: avg 0.978630 std 0.230425
        # Runtime (runtime error) [ms]: avg 0.978630 std 0.230425
        # Runtime (measured) [ms]: avg 0.978630 std 0.230425
        match = re.search(r'Runtime \((.*)\) \[ms\]: avg ([0-9.]+) std ([0-9.]+)$', line)
        if match:
            status, avg, std = match.groups()
            if status == "from cache" and std == "0.0":
                # cached timeout
                status = "timed out"
            
            evals.append((status_code_map[status], avg, std))

with open(args.output, 'w', newline='') as csvfile:
    writer = csv.writer(csvfile)
    writer.writerow(["status", "mean", "std"])
    for status, avg, std in evals:
        writer.writerow([status, avg, std])
