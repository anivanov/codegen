# plots search curve normalized to the fastest
import re
import argparse
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import math
import pathlib
from cycler import cycler
import csv


# python plot_search_curve.py --kernel softmax --size 24576x512 --my "$CODE_ROOT/microkernels/logs/daint/search/softmax_24576x512_greedy_top_heur.log" --tvm "$CODE_ROOT/plots/data/softmax_24576x512_tvm.csv"
parser = argparse.ArgumentParser(description='Plot search curves')
parser.add_argument("--kernel", type=str, help="Kernel name")
parser.add_argument("--size", type=str, help="Kernel size")
parser.add_argument("--my", type=str, help="My search log")
parser.add_argument("--tvm", type=str, help="TVM search log")
parser.add_argument("--output", type=str, help="Output file")
args = parser.parse_args()


df_my = pd.read_csv(args.my)
df_tvm = pd.read_csv(args.tvm)

min_length = max(min(len(df_my), len(df_tvm)), 1000)

df_my = df_my.iloc[:1000]
df_tvm = df_tvm.iloc[:1000]

# pad df_tvm with errors if it is shorter
if len(df_tvm) < 1000:
    df_tvm = pd.concat([df_tvm, pd.DataFrame({
        'status': [2] * (1000 - len(df_tvm)),
        'mean': [1e10] * (1000 - len(df_tvm))
    })], ignore_index=True)

my_min = df_my['mean'].min()
tvm_min = df_tvm['mean'].min() if df_tvm[df_tvm['status'] == 0].shape[0] > 0 else 1e10

df_my['norm'] = my_min / df_my['mean']
df_tvm['norm'] = tvm_min / df_tvm['mean']

df_my['best'] = df_my['norm'].cummax()
df_tvm['best'] = df_tvm['norm'].cummax()

merged_df = pd.concat([df_my['status'], df_my['best'], df_tvm['status'], df_tvm['best']], axis=1)
merged_df.columns = ['status_my', 'best_my', 'status_tvm', 'best_tvm']

my_ok = merged_df[merged_df['status_my'] == 0]
my_timeout = merged_df[merged_df['status_my'] == 1]
my_error = merged_df[merged_df['status_my'] == 2]

tvm_ok = merged_df[merged_df['status_tvm'] == 0]
tvm_timeout = merged_df[merged_df['status_tvm'] == 1]
tvm_error = merged_df[merged_df['status_tvm'] == 2]

fig, ax = plt.subplots(layout='constrained', figsize=(4, 2.2))
# ax.set_prop_cycle(cycler('linestyle', ['-','--',':','-.']) * cycler('color', list('rbgykcm')))  # https://stackoverflow.com/a/7799661
# indices = 1 + np.arange(min_length)

colormap = plt.get_cmap('Paired')
blue, dark_blue, green, dark_green, red, dark_red, orange, dark_orange, purple, dark_purple, yellow, dark_yellow = [colormap(i) for i in range(12)]


ax.step(merged_df.index, merged_df['best_my'], label=f"Our ({len(my_ok)})", linestyle='-', color=dark_green, zorder=1)
ax.step(merged_df.index, merged_df['best_tvm'], label=f"TVM ({len(tvm_ok)})", linestyle='-', color=blue, zorder=1)
dot_size = 3
ax.scatter(my_ok.index, my_ok['best_my'], s=dot_size, label='_nolegend_', color=dark_green, zorder=2)
ax.scatter(tvm_ok.index, tvm_ok['best_tvm'], s=dot_size, label='_nolegend_', color=blue, zorder=2)
# ax.scatter(my_timeout.index, my_timeout['best_my'], s=dot_size, label="Timeouts", color=dark_blue, zorder=3)
# ax.scatter(tvm_timeout.index, tvm_timeout['best_tvm'], s=dot_size, label='_nolegend_', color=dark_blue, zorder=3)
ax.scatter(my_error.index, my_error['best_my'], s=dot_size, label=f"Errors ({len(tvm_error)})", color=dark_red, zorder=4)
ax.scatter(tvm_error.index, tvm_error['best_tvm'], s=dot_size, label='_nolegend_', color=dark_red, zorder=4)

# num_tvm_errors = len(tvm_error)
# plt.text(0.95, 0.05, f'TVM Errors: {num_tvm_errors}', fontsize=12,
#          verticalalignment='bottom', horizontalalignment='right',
#          transform=plt.gca().transAxes)

# ax.set_yscale('log')
# ax.set_xscale('log')
ax.set_xlabel('Measurement number')
ax.set_ylabel('Normalized perforamnce')
ax.legend()
plt.tight_layout()
# ax.set_title('Search Runtimes')
fig.savefig(args.output)
