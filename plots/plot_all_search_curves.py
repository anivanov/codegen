# plots search curve normalized to the fastest
import re
import argparse
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import math
import pathlib
from cycler import cycler
import csv




name_map = {
    "layernorm": "LayerNorm",
    "matmul": "MatMul",
    "softmax": "Softmax",
    "conv": "Conv2D",
    "batchnorm": "BatchNorm",
    "reducemean": "ReduceMean",
    "relu_ffn": "FFN-ReLU",
    "swiglu": "SwiGLU",
    "rmsnorm": "RMSNorm",
    "bmm": "Batched Matmul",
}


# python plot_search_curve.py --kernel softmax --size 24576x512 --my "$CODE_ROOT/microkernels/logs/daint/search/softmax_24576x512_greedy_top_heur.log" --tvm "$CODE_ROOT/plots/data/softmax_24576x512_tvm.csv"
parser = argparse.ArgumentParser(description='Plot search curves')
parser.add_argument("--data", type=str, help="directory with files")
parser.add_argument("--output", type=str, help="Output file")
args = parser.parse_args()


all_files = pathlib.Path(args.data).rglob('*.csv')
# group files to include My and TVM plots one the same tile
prefixes = set()
for file_path in all_files:
    file_name = file_path.stem
    if "tvm" in file_name:
        prefix = file_name[:file_name.find("_tvm")]
    else:
        prefix = file_name[:file_name.find("_greedy")]
    prefixes.add(prefix)
    
file_groups = {}
for prefix in prefixes:
    f1, f2 = pathlib.Path(args.data).rglob(f'{prefix}*.csv')
    if "tvm" in f1.stem:
        file_groups[prefix] = (f2, f1)
    else:
        file_groups[prefix] = (f1, f2)

fig, axes = plt.subplots(3, 4, layout='constrained', figsize=(16, 5), sharex=True, sharey=True)

for (kernel_size, (my_file, tvm_file)), ax in zip(file_groups.items(), axes.flatten()):
    name_split = kernel_size.rfind("_")
    kernel, size = kernel_size[:name_split], kernel_size[name_split+1:]

    df_my = pd.read_csv(my_file)
    df_tvm = pd.read_csv(tvm_file)

    min_length = max(min(len(df_my), len(df_tvm)), 1000)

    df_my = df_my.iloc[:1000]
    df_tvm = df_tvm.iloc[:1000]

    # some tvm runs killed by OOM, treat it as error
    if len(df_tvm) < 1000:
        df_tvm = pd.concat([df_tvm, pd.DataFrame({
            'status': [2] * (1000 - len(df_tvm)),
            'mean': [1e10] * (1000 - len(df_tvm))
        })], ignore_index=True)
    
    # some our runs stoppped by timeout, treat it as runtime timeout
    if len(df_my) < 1000:
        df_my = pd.concat([df_my, pd.DataFrame({
            'status': [1] * (1000 - len(df_my)),
            'mean': [1e10] * (1000 - len(df_my))
        })], ignore_index=True)

    my_min = df_my['mean'].min()
    tvm_min = df_tvm['mean'].min() if df_tvm[df_tvm['status'] == 0].shape[0] > 0 else 1e10

    print(f"{kernel} {size} My min: {my_min}, TVM min: {tvm_min}")

    df_my['norm'] = my_min / df_my['mean']
    df_tvm['norm'] = tvm_min / df_tvm['mean']

    df_my['best'] = df_my['norm'].cummax()
    df_tvm['best'] = df_tvm['norm'].cummax()

    merged_df = pd.concat([df_my['status'], df_my['best'], df_tvm['status'], df_tvm['best']], axis=1)
    merged_df.columns = ['status_my', 'best_my', 'status_tvm', 'best_tvm']

    my_ok = merged_df[merged_df['status_my'] == 0]
    my_timeout = merged_df[merged_df['status_my'] == 1]
    my_error = merged_df[merged_df['status_my'] == 2]

    tvm_ok = merged_df[merged_df['status_tvm'] == 0]
    tvm_timeout = merged_df[merged_df['status_tvm'] == 1]
    tvm_error = merged_df[merged_df['status_tvm'] == 2]
    tvm_build_timeout = merged_df[merged_df['status_tvm'] == 3]

    # ax.set_prop_cycle(cycler('linestyle', ['-','--',':','-.']) * cycler('color', list('rbgykcm')))  # https://stackoverflow.com/a/7799661
    # indices = 1 + np.arange(min_length)

    colormap = plt.get_cmap('Paired')
    blue, dark_blue, green, dark_green, red, dark_red, orange, dark_orange, purple, dark_purple, yellow, dark_yellow = [colormap(i) for i in range(12)]


    ax.step(merged_df.index, merged_df['best_my'], label='_nolegend_', linestyle='-', color=dark_green, zorder=1)
    ax.step(merged_df.index, merged_df['best_tvm'], label='_nolegend_', linestyle='-', color=dark_purple, zorder=1)
    dot_size = 3
    # ax.scatter(my_ok.index, my_ok['best_my'], s=dot_size, label='_nolegend_', color=dark_green, zorder=2)
    # ax.scatter(tvm_ok.index, tvm_ok['best_tvm'], s=dot_size, label='_nolegend_', color=dark_purple, zorder=2)
    # ax.scatter(my_timeout.index, my_timeout['best_my'], s=dot_size, label="Timeouts", color=dark_blue, zorder=3)
    # ax.scatter(tvm_timeout.index, tvm_timeout['best_tvm'], s=dot_size, label='_nolegend_', color=dark_blue, zorder=3)
    ax.scatter(my_error.index, my_error['best_my'], s=dot_size, label=f"TVM runtime errors ({len(tvm_error)})", color=dark_red, zorder=4)
    ax.scatter(tvm_build_timeout.index, tvm_build_timeout['best_tvm'], s=dot_size, label=f'TVM build timeouts ({len(tvm_build_timeout)})', color=dark_orange, zorder=3)
    ax.scatter(tvm_error.index, tvm_error['best_tvm'], s=dot_size, label='_nolegend_', color=dark_red, zorder=4)

    # num_tvm_errors = len(tvm_error)
    # plt.text(0.95, 0.05, f'TVM Errors: {num_tvm_errors}', fontsize=12,
    #          verticalalignment='bottom', horizontalalignment='right',
    #          transform=plt.gca().transAxes)

    # ax.set_yscale('log')
    # ax.set_xscale('log')
    # ax.set_xlabel('Measurement number')
    # ax.set_ylabel('Normalized perforamnce')
    ax.set_title(f"{name_map[kernel]} {size}")
    ax.legend(loc='lower right')

from matplotlib.lines import Line2D
legend_elements = [
    Line2D([0], [0], color=dark_green, label='Our'),
    Line2D([0], [0], color=dark_purple, label='TVM'),
]
fig.legend(handles=legend_elements, loc='lower right', ncol=2)
    
fig.supxlabel('Autotuning evaluation number')
sup_ylabel = fig.supylabel('Normalized performance')
sup_ylabel.set_position((0.01, 0.5))  # shift it away from the y-axis, by default it is 0.02, 0.5
# ax.set_title('Search Runtimes')
fig.tight_layout()
fig.savefig(args.output)
