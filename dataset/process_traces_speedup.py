import os
import pathlib
import re
import json
import math

script_dir = os.path.dirname(__file__)
root_dir = pathlib.Path(script_dir).resolve().parent
# traces_dir = root_dir / "microkernels/logs/daint/search"
traces_dir = root_dir / "microkernels/logs/daint/fast_traces"
traces = list(traces_dir.glob("*.log"))

dataset = {}

for i, trace in enumerate(traces):
    print(f"Processing trace {i+1}/{len(traces)}: {trace}")
    with open(trace, "r") as f:
        current_kernel_code = []
        reading_kernel_code = False
        baseline_kernel = None
        baseline_runtime = None
        for line_num, line in enumerate(f):
            # match pattern "Name: something"
            match = re.search(r"^Name: (.+)$", line)
            if match:
                # print("found kernel start")
                reading_kernel_code = True
                current_kernel_code = []
            # match pattern "Compiling ..."
            match = re.search(r"^Compiling (.+)$", line)
            if match:
                reading_kernel_code = False
            # match pattern "Runtime (timed out) [ms]: avg 36.302000 std 0.0"
            match = re.search(r"^Runtime \((.*)\) \[ms\]: avg (.*) std (.*)$", line)
            if match:
                reading_kernel_code = False
                kernel_code = "".join(current_kernel_code)
                reason = match.group(1)
                # print("found runtime")
                # print(kernel_code)
                
                if reason in {"timed out", "runtime error"}:
                    current_kernel_code = []
                    continue  # skip timed out kernels
                if reason not in {"measured", "from cache"}:
                    import pdb; pdb.set_trace()
                runtime = float(match.group(2))
                
                if baseline_kernel is None:
                    # print('registering baseline')
                    baseline_kernel = kernel_code
                    baseline_runtime = runtime
                
                # if baseline_kernel == kernel_code or not kernel_code:
                if not kernel_code:
                    current_kernel_code = []
                    continue
                
                # key = "Baseline program:\n" + baseline_kernel + "\n\n" + "Target program:\n" + kernel_code
                key = kernel_code
                speedup = baseline_runtime / runtime
                
                # if speedup == 1 and key not in dataset:
                #     import pdb; pdb.set_trace()
                
                if math.isnan(speedup):
                    continue
                
                dataset[key] = speedup
            if reading_kernel_code:
                current_kernel_code.append(line)
    print("Entries in dataset:", len(dataset))

json.dump(dataset, open("dataset_speedup.json", "w"), indent=4)