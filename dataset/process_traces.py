import os
import pathlib
import re
import json
import math

script_dir = os.path.dirname(__file__)
root_dir = pathlib.Path(script_dir).resolve().parent
# traces_dir = root_dir / "microkernels/logs/daint/search"
traces_dir = root_dir / "microkernels/logs/daint/fast_traces"
traces = list(traces_dir.glob("*.log"))

dataset = {}

for i, trace in enumerate(traces):
    print(f"Processing trace {i+1}/{len(traces)}: {trace}")
    with open(trace, "r") as f:
        current_kernel_code = []
        reading_kernel_code = False
        transformation_sequence = None
        kernel_name = None
        for line_num, line in enumerate(f):
            # match transformation sequence "Transformations: join_scopes(program, 'sum_val#1'); ...."
            match = re.search(r"^Transformations: (.+)$", line)
            if match:
                transformation_sequence = match.group(1)
                reading_kernel_code = False
                current_kernel_code = []
                kernel_name = None
            # match pattern "Name: something"
            match = re.search(r"^Name: (.+)$", line)
            if match:
                # print("found kernel start")
                reading_kernel_code = True
                current_kernel_code = []
                kernel_name = match.group(1)
            # match pattern "Compiling ..."
            match = re.search(r"^Compiling (.+)$", line)
            if match:
                reading_kernel_code = False
            # match pattern "Runtime (timed out) [ms]: avg 36.302000 std 0.0"
            match = re.search(r"^Runtime \((.*)\) \[ms\]: avg (.*) std (.*)$", line)
            if match:
                reading_kernel_code = False
                
                assert current_kernel_code
                assert transformation_sequence
                assert kernel_name
                
                kernel_code = "".join(current_kernel_code)
                reason = match.group(1)
                
                if reason in {"timed out", "runtime error"}:
                    current_kernel_code = []
                    continue  # skip timed out kernels
                assert reason in {"measured", "from cache"}

                runtime = float(match.group(2))
                
                key = kernel_code
                
                assert runtime > 0 and runtime < 1000000
                
                dataset[key] = (runtime, kernel_name, transformation_sequence)
                
                current_kernel_code = []
                reading_kernel_code = False
                transformation_sequence = None
                kernel_name = None
            if reading_kernel_code:
                current_kernel_code.append(line)
    print("Entries in dataset:", len(dataset))

json.dump(dataset, open("dataset.json", "w"), indent=4)