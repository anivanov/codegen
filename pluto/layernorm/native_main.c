
#include <stddef.h>
#include <stdio.h>
#include <math.h>
#include <stdint.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <float.h>
#include <immintrin.h>
#include "generic_mini.h"

#if !defined(TOC)
static inline double elapsed_seconds(struct timespec start) {
    struct timespec end;
    clock_gettime(CLOCK_MONOTONIC, &end);
    return (end.tv_sec - start.tv_sec) + (end.tv_nsec - start.tv_nsec) / 1e9;
}
#define TIC(name) struct timespec dt_ ##name; clock_gettime(CLOCK_MONOTONIC, &dt_ ##name)
#define TOC(name) elapsed_seconds(dt_ ## name)
#endif


void layernorm(f32* __restrict__ beta, f32* __restrict__ gamma, f32* __restrict__ src, f32* __restrict__ dst, void* __restrict__ _tmp) {
    
    f32* m = (f32*)((u8*)_tmp + 0);  // size = 4096
    f32* mu = (f32*)((u8*)_tmp + 16384);  // size = 4096
    f32* diff = (f32*)((u8*)_tmp + 32768);  // size = 16777216
    f32* q = (f32*)((u8*)_tmp + 67141632);  // size = 4096
    f32* tmp = (f32*)((u8*)_tmp + 67158016);  // size = 16777216
    f32* qeps = (f32*)((u8*)_tmp + 134266880);  // size = 4096
    f32* sigma = (f32*)((u8*)_tmp + 134283264);  // size = 4096
    #pragma scop
    for (int i0 = 0; i0 < 2048; i0++) {
        for (int i1 = 0; i1 < 2; i1++) {
            m[((i0 * 2) + i1)] = 0;  // init reduce
            for (int i2 = 0; i2 < 4096; i2++) {
                m[((i0 * 2) + i1)] += src[((i0 * 2) + i1) * 4096 + i2];
            }
        }
    }
    
    for (int i0 = 0; i0 < 4096; i0++) {
        mu[i0] = m[i0] / 4096;
    }
    
    for (int i0 = 0; i0 < 4096; i0++) {
        for (int i1 = 0; i1 < 4096; i1++) {
            diff[i0 * 4096 + i1] = src[i0 * 4096 + i1] - mu[i0];
        }
    }
    
    for (int i0 = 0; i0 < 4096; i0++) {
        q[i0] = 0;  // init reduce
        for (int i1 = 0; i1 < 4096; i1++) {
            q[i0] += diff[i0 * 4096 + i1] * diff[i0 * 4096 + i1];
        }
    }
    
    for (int i0 = 0; i0 < 4096; i0++) {
        for (int i1 = 0; i1 < 4096; i1++) {
            tmp[i0 * 4096 + i1] = diff[i0 * 4096 + i1] * gamma[i1];
        }
    }
    for (int i0 = 0; i0 < 4096; i0++) {
        qeps[i0] = q[i0] * 0.0002442002442002442 + 1e-05;
    }
    for (int i0 = 0; i0 < 4096; i0++) {
        sigma[i0] = 1 / sqrtf(qeps[i0]);
    }
    for (int i0 = 0; i0 < 4096; i0++) {
        for (int i1 = 0; i1 < 4096; i1++) {
            dst[i0 * 4096 + i1] = sigma[i0] * tmp[i0 * 4096 + i1] + beta[i1];
        }
    }
    #pragma endscop
    
}
int main() {
    f32* src = (f32*) aligned_alloc(4096, 16777216 * sizeof(f32));
    if (!src) { printf("ERROR: failed to allocate buffer src of size 16777216 * sizeof(f32)\n"); return 1; }
    read_file(src, 16777216 * sizeof(f32), "data_src.bin");
    f32* gamma = (f32*) aligned_alloc(4096, 4096 * sizeof(f32));
    if (!gamma) { printf("ERROR: failed to allocate buffer gamma of size 4096 * sizeof(f32)\n"); return 1; }
    read_file(gamma, 4096 * sizeof(f32), "data_gamma.bin");
    f32* beta = (f32*) aligned_alloc(4096, 4096 * sizeof(f32));
    if (!beta) { printf("ERROR: failed to allocate buffer beta of size 4096 * sizeof(f32)\n"); return 1; }
    read_file(beta, 4096 * sizeof(f32), "data_beta.bin");
    f32* dst = (f32*) aligned_alloc(4096, 16777216 * sizeof(f32));
    if (!dst) { printf("ERROR: failed to allocate buffer dst of size 16777216 * sizeof(f32)\n"); return 1; }
    read_file(dst, 16777216 * sizeof(f32), "data_dst.bin");
    f32* result_dst = (f32*) aligned_alloc(4096, 16777216 * sizeof(f32));
    if (!result_dst) { printf("ERROR: failed to allocate buffer result_dst of size 16777216 * sizeof(f32)\n"); return 1; }
    memset(result_dst, 0, 16777216 * sizeof(f32));
    size_t buf_size = 134299648;
    int ok = 1;
    void* _tmp = aligned_alloc(4096, buf_size);
    if (!_tmp) { printf("ERROR: failed to allocate buffer _tmp\n"); return 1; }
    
    TIC(time_correctness);
    layernorm(beta, gamma, src, result_dst, _tmp);
    double correctness_elapsed = TOC(time_correctness);
    if (correctness_elapsed > 0.820526) {
        printf("Seconds: %f\n", correctness_elapsed);
        printf("success, exitting...\n");
        return 0;
    }
    for (int i = 0; i < 16777216; i++) {
        double eps = 1e-3;
        double abs_err = fabs(result_dst[i] - dst[i]);
        double rel_err = abs_err / (fabs(dst[i]) + eps);
        if (abs_err > eps && rel_err > eps) {
            printf("Error: mismatch at dst, %d, %f (computed) != %f (expected) \n", (int)i, (double)result_dst[i], (double)dst[i]);
            ok = 0;
            break;
        }
    }
    if (!ok) {
        printf("FAILURE, exitting...\n");
        return 1;
    }
    
    TIC(total);
    for (int i = 0; i < 13 || TOC(total) < 0.1; i++) {
        read_file(src, 16777216 * sizeof(f32), "data_src.bin");
        read_file(gamma, 4096 * sizeof(f32), "data_gamma.bin");
        read_file(beta, 4096 * sizeof(f32), "data_beta.bin");
        memset(result_dst, 0, 16777216 * sizeof(f32));
        TIC(1);
        for (int j = 0; j < 1; j++) {
            layernorm(beta, gamma, src, result_dst, _tmp);
        }
        double seconds = TOC(1) / 1;
        printf("Seconds: %f\n", seconds);
    }
    printf("success, exitting...\n");
    return 0;
}
