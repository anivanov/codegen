#include <omp.h>
#include <math.h>
#define ceild(n,d)  (((n)<0) ? -((-(n))/(d)) : ((n)+(d)-1)/(d))
#define floord(n,d) (((n)<0) ? -((-(n)+(d)-1)/(d)) : (n)/(d))
#define max(x,y)    ((x) > (y)? (x) : (y))
#define min(x,y)    ((x) < (y)? (x) : (y))


#include <stddef.h>
#include <stdio.h>
#include <math.h>
#include <stdint.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <float.h>
#include <immintrin.h>
#include "generic_mini.h"

#if !defined(TOC)
static inline double elapsed_seconds(struct timespec start) {
    struct timespec end;
    clock_gettime(CLOCK_MONOTONIC, &end);
    return (end.tv_sec - start.tv_sec) + (end.tv_nsec - start.tv_nsec) / 1e9;
}
#define TIC(name) struct timespec dt_ ##name; clock_gettime(CLOCK_MONOTONIC, &dt_ ##name)
#define TOC(name) elapsed_seconds(dt_ ## name)
#endif


void layernorm(f32* __restrict__ beta, f32* __restrict__ gamma, f32* __restrict__ src, f32* __restrict__ dst, void* __restrict__ _tmp) {
    
    f32* m = (f32*)((u8*)_tmp + 0);  // size = 4096
    f32* mu = (f32*)((u8*)_tmp + 16384);  // size = 4096
    f32* diff = (f32*)((u8*)_tmp + 32768);  // size = 16777216
    f32* q = (f32*)((u8*)_tmp + 67141632);  // size = 4096
    f32* tmp = (f32*)((u8*)_tmp + 67158016);  // size = 16777216
    f32* qeps = (f32*)((u8*)_tmp + 134266880);  // size = 4096
    f32* sigma = (f32*)((u8*)_tmp + 134283264);  // size = 4096
  int t1, t2, t3, t4, t5, t6, t7;
 int lb, ub, lbp, ubp, lb2, ub2;
 register int lbv, ubv;
lbp=0;
ubp=127;
#pragma omp parallel for private(lbv,ubv,t3,t4,t5,t6,t7)
for (t2=lbp;t2<=ubp;t2++) {
  lbv=32*t2;
  ubv=32*t2+31;
#pragma ivdep
#pragma vector always
  for (t3=lbv;t3<=ubv;t3++) {
    q[t3] = 0;;
  }
}
lbp=0;
ubp=63;
#pragma omp parallel for private(lbv,ubv,t3,t4,t5,t6,t7)
for (t2=lbp;t2<=ubp;t2++) {
  for (t4=32*t2;t4<=32*t2+31;t4++) {
    lbv=0;
    ubv=1;
#pragma ivdep
#pragma vector always
    for (t5=lbv;t5<=ubv;t5++) {
      m[((t4 * 2) + t5)] = 0;;
    }
  }
}
lbp=0;
ubp=63;
#pragma omp parallel for private(lbv,ubv,t3,t4,t5,t6,t7)
for (t2=lbp;t2<=ubp;t2++) {
  for (t4=0;t4<=127;t4++) {
    for (t5=32*t2;t5<=32*t2+31;t5++) {
      for (t6=0;t6<=1;t6++) {
        for (t7=32*t4;t7<=32*t4+31;t7++) {
          m[((t5 * 2) + t6)] += src[((t5 * 2) + t6) * 4096 + t7];;
        }
      }
    }
  }
}
lbp=0;
ubp=127;
#pragma omp parallel for private(lbv,ubv,t3,t4,t5,t6,t7)
for (t2=lbp;t2<=ubp;t2++) {
  lbv=32*t2;
  ubv=32*t2+31;
#pragma ivdep
#pragma vector always
  for (t3=lbv;t3<=ubv;t3++) {
    mu[t3] = m[t3] / 4096;;
  }
}
for (t2=0;t2<=262527;t2++) {
  lbp=ceild(2049*t2-1,2050);
  ubp=min(floord(2049*t2+2112,2050),t2);
#pragma omp parallel for private(lbv,ubv,t4,t5,t6,t7)
  for (t3=lbp;t3<=ubp;t3++) {
    if ((t2 <= 127) && (t2 == t3)) {
      for (t5=32*t2;t5<=32*t2+31;t5++) {
        diff[0 * 4096 + t5] = src[0 * 4096 + t5] - mu[0];;
      }
    }
    if ((t2 <= floord(2050*t3-192,2049)) && (t2 >= ceild(2050*t3-256,2049))) {
      if ((63*t2+2*t3)%64 == 0) {
        for (t5=32*t3;t5<=floord(-2049*t2+4098*t3-192,64);t5++) {
          diff[((-t2+2*t3-128)/64) * 4096 + ((2049*t2-4098*t3+64*t5+262272)/64)] = src[((-t2+2*t3-128)/64) * 4096 + ((2049*t2-4098*t3+64*t5+262272)/64)] - mu[((-t2+2*t3-128)/64)];;
        }
      }
    }
    if (t2 == t3+128) {
      for (t5=max(8392702,32*t2-4096);t5<=min(8396797,32*t2-4065);t5++) {
        q[4095] += diff[4095 * 4096 + (t5-8392702)] * diff[4095 * 4096 + (t5-8392702)];;
      }
    }
    for (t4=max(max(1,ceild(32*t3-4093,2049)),32*t2-32*t3);t4<=min(4095,floord(32*t3-4063,2049));t4++) {
      for (t5=32*t3;t5<=2049*t4+4093;t5++) {
        diff[t4 * 4096 + (-2049*t4+t5)] = src[t4 * 4096 + (-2049*t4+t5)] - mu[t4];;
        q[(t4-1)] += diff[(t4-1) * 4096 + (-2049*t4+t5+2)] * diff[(t4-1) * 4096 + (-2049*t4+t5+2)];;
      }
      for (t5=2049*t4+4094;t5<=min(32*t3+31,2049*t4+4095);t5++) {
        diff[t4 * 4096 + (-2049*t4+t5)] = src[t4 * 4096 + (-2049*t4+t5)] - mu[t4];;
      }
    }
    for (t4=max(max(1,ceild(32*t3-4062,2049)),32*t2-32*t3);t4<=min(min(4095,floord(-t2+2*t3-1,64)),32*t2-32*t3+31);t4++) {
      for (t5=32*t3;t5<=32*t3+31;t5++) {
        diff[t4 * 4096 + (-2049*t4+t5)] = src[t4 * 4096 + (-2049*t4+t5)] - mu[t4];;
        q[(t4-1)] += diff[(t4-1) * 4096 + (-2049*t4+t5+2)] * diff[(t4-1) * 4096 + (-2049*t4+t5+2)];;
      }
    }
    if ((t2 >= 2050) && (t2 <= 260350) && (2049*t2 == 2050*t3)) {
      if (16*t2%1025 == 0) {
        for (t5=ceild(32784*t2,1025);t5<=floord(32784*t2+31775,1025);t5++) {
          if (t2%2050 == 0) {
            diff[(16*t2/1025) * 4096 + ((-32784*t2+1025*t5)/1025)] = src[(16*t2/1025) * 4096 + ((-32784*t2+1025*t5)/1025)] - mu[(16*t2/1025)];;
          }
          if (t2%2050 == 0) {
            q[((16*t2-1025)/1025)] += diff[((16*t2-1025)/1025) * 4096 + ((-32784*t2+1025*t5+2050)/1025)] * diff[((16*t2-1025)/1025) * 4096 + ((-32784*t2+1025*t5+2050)/1025)];;
          }
        }
      }
    }
    if ((t2 <= floord(2050*t3-64,2049)) && (t2 >= max(ceild(2050*t3-1984,2049),2*t3-262080))) {
      if ((63*t2+2*t3)%64 == 0) {
        for (t5=max(ceild(-2049*t2+4098*t3-128,64),32*t3);t5<=floord(-2049*t2+4098*t3-64,64);t5++) {
          q[((-t2+2*t3-64)/64)] += diff[((-t2+2*t3-64)/64) * 4096 + ((2049*t2-4098*t3+64*t5+128)/64)] * diff[((-t2+2*t3-64)/64) * 4096 + ((2049*t2-4098*t3+64*t5+128)/64)];;
        }
        for (t5=ceild(-2049*t2+4098*t3,64);t5<=32*t3+31;t5++) {
          diff[((-t2+2*t3)/64) * 4096 + ((2049*t2-4098*t3+64*t5)/64)] = src[((-t2+2*t3)/64) * 4096 + ((2049*t2-4098*t3+64*t5)/64)] - mu[((-t2+2*t3)/64)];;
          q[((-t2+2*t3-64)/64)] += diff[((-t2+2*t3-64)/64) * 4096 + ((2049*t2-4098*t3+64*t5+128)/64)] * diff[((-t2+2*t3-64)/64) * 4096 + ((2049*t2-4098*t3+64*t5+128)/64)];;
        }
      }
    }
    if (t2 >= max(ceild(2050*t3-63,2049),2*t3-262079)) {
      if ((63*t2+2*t3+1)%64 == 0) {
        for (t5=ceild(-2049*t2+4098*t3+1921,64);t5<=32*t3+31;t5++) {
          q[((-t2+2*t3-63)/64)] += diff[((-t2+2*t3-63)/64) * 4096 + ((2049*t2-4098*t3+64*t5-1921)/64)] * diff[((-t2+2*t3-63)/64) * 4096 + ((2049*t2-4098*t3+64*t5-1921)/64)];;
        }
      }
    }
  }
}
    for (int i0 = 0; i0 < 4096; i0++) {
        for (int i1 = 0; i1 < 4096; i1++) {
            tmp[i0 * 4096 + i1] = diff[i0 * 4096 + i1] * gamma[i1];
        }
    }
    for (int i0 = 0; i0 < 4096; i0++) {
        qeps[i0] = q[i0] * 0.0002442002442002442 + 1e-05;
    }
    for (int i0 = 0; i0 < 4096; i0++) {
        sigma[i0] = 1 / sqrtf(qeps[i0]);
    }
    for (int i0 = 0; i0 < 4096; i0++) {
        for (int i1 = 0; i1 < 4096; i1++) {
            dst[i0 * 4096 + i1] = sigma[i0] * tmp[i0 * 4096 + i1] + beta[i1];
        }
    }
    
}
int main() {
    f32* src = (f32*) aligned_alloc(4096, 16777216 * sizeof(f32));
    if (!src) { printf("ERROR: failed to allocate buffer src of size 16777216 * sizeof(f32)\n"); return 1; }
    read_file(src, 16777216 * sizeof(f32), "data_src.bin");
    f32* gamma = (f32*) aligned_alloc(4096, 4096 * sizeof(f32));
    if (!gamma) { printf("ERROR: failed to allocate buffer gamma of size 4096 * sizeof(f32)\n"); return 1; }
    read_file(gamma, 4096 * sizeof(f32), "data_gamma.bin");
    f32* beta = (f32*) aligned_alloc(4096, 4096 * sizeof(f32));
    if (!beta) { printf("ERROR: failed to allocate buffer beta of size 4096 * sizeof(f32)\n"); return 1; }
    read_file(beta, 4096 * sizeof(f32), "data_beta.bin");
    f32* dst = (f32*) aligned_alloc(4096, 16777216 * sizeof(f32));
    if (!dst) { printf("ERROR: failed to allocate buffer dst of size 16777216 * sizeof(f32)\n"); return 1; }
    read_file(dst, 16777216 * sizeof(f32), "data_dst.bin");
    f32* result_dst = (f32*) aligned_alloc(4096, 16777216 * sizeof(f32));
    if (!result_dst) { printf("ERROR: failed to allocate buffer result_dst of size 16777216 * sizeof(f32)\n"); return 1; }
    memset(result_dst, 0, 16777216 * sizeof(f32));
    size_t buf_size = 134299648;
    int ok = 1;
    void* _tmp = aligned_alloc(4096, buf_size);
    if (!_tmp) { printf("ERROR: failed to allocate buffer _tmp\n"); return 1; }
    
    TIC(time_correctness);
    layernorm(beta, gamma, src, result_dst, _tmp);
    double correctness_elapsed = TOC(time_correctness);
    if (correctness_elapsed > 0.820526) {
        printf("Seconds: %f\n", correctness_elapsed);
        printf("success, exitting...\n");
        return 0;
    }
    for (int i = 0; i < 16777216; i++) {
        double eps = 1e-3;
        double abs_err = fabs(result_dst[i] - dst[i]);
        double rel_err = abs_err / (fabs(dst[i]) + eps);
        if (abs_err > eps && rel_err > eps) {
            printf("Error: mismatch at dst, %d, %f (computed) != %f (expected) \n", (int)i, (double)result_dst[i], (double)dst[i]);
            ok = 0;
            break;
        }
    }
    if (!ok) {
        printf("FAILURE, exitting...\n");
        return 1;
    }
    
    TIC(total);
    for (int i = 0; i < 13 || TOC(total) < 0.1; i++) {
        read_file(src, 16777216 * sizeof(f32), "data_src.bin");
        read_file(gamma, 4096 * sizeof(f32), "data_gamma.bin");
        read_file(beta, 4096 * sizeof(f32), "data_beta.bin");
        memset(result_dst, 0, 16777216 * sizeof(f32));
        TIC(1);
        for (int j = 0; j < 1; j++) {
            layernorm(beta, gamma, src, result_dst, _tmp);
        }
        double seconds = TOC(1) / 1;
        printf("Seconds: %f\n", seconds);
    }
    printf("success, exitting...\n");
    return 0;
}
