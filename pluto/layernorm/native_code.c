#include "generic_mini.h"
#include <stdio.h>
#include <stdint.h>
#include <math.h>
#include <float.h>
#include <immintrin.h>
#define u8 uint8_t
#define i32 int32_t
#define f32 float
#define f64 double


void layernorm(f32* __restrict__ beta, f32* __restrict__ gamma, f32* __restrict__ src, f32* __restrict__ dst, void* __restrict__ _tmp) {

    f32* m = (f32*)((u8*)_tmp + 0);  // size = 4096
    f32* mu = (f32*)((u8*)_tmp + 16384);  // size = 4096
    f32* diff = (f32*)((u8*)_tmp + 32768);  // size = 16777216
    f32* q = (f32*)((u8*)_tmp + 67141632);  // size = 4096
    f32* tmp = (f32*)((u8*)_tmp + 67158016);  // size = 16777216
    f32* qeps = (f32*)((u8*)_tmp + 134266880);  // size = 4096
    f32* sigma = (f32*)((u8*)_tmp + 134283264);  // size = 4096
    for (i32 i0 = 0; i0 < 2048; i0++) {
        for (i32 i1 = 0; i1 < 2; i1++) {
            m[((i0 * 2) + i1)] = 0;  // init reduce
            for (i32 i2 = 0; i2 < 4096; i2++) {
                m[((i0 * 2) + i1)] += src[((i0 * 2) + i1) * 4096 + i2];
            }
        }
    }
    for (i32 i0 = 0; i0 < 4096; i0++) {
        mu[i0] = m[i0] / 4096;
    }
    for (i32 i0 = 0; i0 < 4096; i0++) {
        for (i32 i1 = 0; i1 < 4096; i1++) {
            diff[i0 * 4096 + i1] = src[i0 * 4096 + i1] - mu[i0];
        }
    }
    for (i32 i0 = 0; i0 < 4096; i0++) {
        q[i0] = 0;  // init reduce
        for (i32 i1 = 0; i1 < 4096; i1++) {
            q[i0] += diff[i0 * 4096 + i1] * diff[i0 * 4096 + i1];
        }
    }
    for (i32 i0 = 0; i0 < 4096; i0++) {
        for (i32 i1 = 0; i1 < 4096; i1++) {
            tmp[i0 * 4096 + i1] = diff[i0 * 4096 + i1] * gamma[i1];
        }
    }
    for (i32 i0 = 0; i0 < 4096; i0++) {
        qeps[i0] = q[i0] * 0.0002442002442002442 + 1e-05;
    }
    for (i32 i0 = 0; i0 < 4096; i0++) {
        sigma[i0] = 1 / sqrtf(qeps[i0]);
    }
    for (i32 i0 = 0; i0 < 4096; i0++) {
        for (i32 i1 = 0; i1 < 4096; i1++) {
            dst[i0 * 4096 + i1] = sigma[i0] * tmp[i0 * 4096 + i1] + beta[i1];
        }
    }
}

