
#include <stddef.h>
#include <stdio.h>
#include <math.h>
#include <stdint.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <immintrin.h>
#include "generic_mini.h"

#if !defined(TOC)
static inline double elapsed_seconds(struct timespec start) {
    struct timespec end;
    clock_gettime(CLOCK_MONOTONIC, &end);
    return (end.tv_sec - start.tv_sec) + (end.tv_nsec - start.tv_nsec) / 1e9;
}
#define TIC(name) struct timespec dt_ ##name; clock_gettime(CLOCK_MONOTONIC, &dt_ ##name)
#define TOC(name) elapsed_seconds(dt_ ## name)
#endif


void conv2d(f32* __restrict__ input, f32* __restrict__ weight, f32* __restrict__ output, void* __restrict__ _tmp) {
    #pragma scop
    for (int i0 = 0; i0 < 8; i0++) {
        for (int i1 = 0; i1 < 10; i1++) {
            for (int i2 = 0; i2 < 508; i2++) {
                for (int i3 = 0; i3 < 508; i3++) {
                    output[i0 * 10 * 508 * 508 + i1 * 508 * 508 + i2 * 508 + i3] = 0;  // init reduce
                    for (int i4 = 0; i4 < 3; i4++) {
                        for (int i5 = 0; i5 < 5; i5++) {
                            for (int i6 = 0; i6 < 5; i6++) {
                                output[i0 * 10 * 508 * 508 + i1 * 508 * 508 + i2 * 508 + i3] += input[i0 * 3 * 512 * 512 + i4 * 512 * 512 + (i2 + i5) * 512 + (i3 + i6)] * weight[i1 * 3 * 5 * 5 + i4 * 5 * 5 + i5 * 5 + i6];
                            }
                        }
                    }
                }
            }
        }
    }
    #pragma endscop
}
int main() {
    f32* input = (f32*) aligned_alloc(4096, 6291456 * sizeof(f32));
    if (!input) { printf("ERROR: failed to allocate buffer input of size 6291456 * sizeof(f32)\n"); return 1; }
    read_file(input, 6291456 * sizeof(f32), "data_input.bin");
    f32* weight = (f32*) aligned_alloc(4096, 750 * sizeof(f32));
    if (!weight) { printf("ERROR: failed to allocate buffer weight of size 750 * sizeof(f32)\n"); return 1; }
    read_file(weight, 750 * sizeof(f32), "data_weight.bin");
    f32* output = (f32*) aligned_alloc(4096, 20645120 * sizeof(f32));
    if (!output) { printf("ERROR: failed to allocate buffer output of size 20645120 * sizeof(f32)\n"); return 1; }
    read_file(output, 20645120 * sizeof(f32), "data_output.bin");
    f32* result_output = (f32*) aligned_alloc(4096, 20645120 * sizeof(f32));
    if (!result_output) { printf("ERROR: failed to allocate buffer result_output of size 20645120 * sizeof(f32)\n"); return 1; }
    memset(result_output, 0, 20645120 * sizeof(f32));
    size_t buf_size = 0;
    int ok = 1;
    void* _tmp = aligned_alloc(4096, buf_size);
    if (!_tmp) { printf("ERROR: failed to allocate buffer _tmp\n"); return 1; }
    
    TIC(time_correctness);
    conv2d(input, weight, result_output, _tmp);
    double correctness_elapsed = TOC(time_correctness);
    if (correctness_elapsed > 5.0) {
        printf("Seconds: %f\n", correctness_elapsed);
        printf("success, exitting...\n");
        return 0;
    }
    for (int i = 0; i < 20645120; i++) {
        double eps = 1e-3;
        double abs_err = fabs(result_output[i] - output[i]);
        double rel_err = abs_err / (fabs(output[i]) + eps);
        if (abs_err > eps && rel_err > eps) {
            printf("Error: mismatch at output, %d, %f (computed) != %f (expected) \n", (int)i, (double)result_output[i], (double)output[i]);
            ok = 0;
            break;
        }
    }
    if (!ok) {
        printf("FAILURE, exitting...\n");
        return 1;
    }
    
    TIC(total);
    for (int i = 0; i < 13 || TOC(total) < 0.1; i++) {
        read_file(input, 6291456 * sizeof(f32), "data_input.bin");
        read_file(weight, 750 * sizeof(f32), "data_weight.bin");
        memset(result_output, 0, 20645120 * sizeof(f32));
        TIC(1);
        for (int j = 0; j < 1; j++) {
            conv2d(input, weight, result_output, _tmp);
        }
        double seconds = TOC(1) / 1;
        printf("Seconds: %f\n", seconds);
    }
    printf("success, exitting...\n");
    return 0;
}
