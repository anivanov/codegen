#include "generic_mini.h"
#include <stdio.h>
#include <stdint.h>
#include <math.h>
#include <float.h>
#include <immintrin.h>
#define u8 uint8_t
#define i32 int32_t
#define f32 float
#define f64 double


void conv2d(f32* __restrict__ input, f32* __restrict__ weight, f32* __restrict__ output, void* __restrict__ _tmp) {

    for (i32 i0 = 0; i0 < 8; i0++) {
        for (i32 i1 = 0; i1 < 10; i1++) {
            for (i32 i2 = 0; i2 < 508; i2++) {
                for (i32 i3 = 0; i3 < 508; i3++) {
                    output[i0 * 10 * 508 * 508 + i1 * 508 * 508 + i2 * 508 + i3] = 0;  // init reduce
                    for (i32 i4 = 0; i4 < 3; i4++) {
                        for (i32 i5 = 0; i5 < 5; i5++) {
                            for (i32 i6 = 0; i6 < 5; i6++) {
                                output[i0 * 10 * 508 * 508 + i1 * 508 * 508 + i2 * 508 + i3] += input[i0 * 3 * 512 * 512 + i4 * 512 * 512 + (i2 + i5) * 512 + (i3 + i6)] * weight[i1 * 3 * 5 * 5 + i4 * 5 * 5 + i5 * 5 + i6];
                            }
                        }
                    }
                }
            }
        }
    }
}

