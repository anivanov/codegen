#include "generic_mini.h"
#include <stdio.h>
#include <stdint.h>
#include <math.h>
#include <float.h>
#include <immintrin.h>
#define u8 uint8_t
#define i32 int32_t
#define f32 float
#define f64 double


void batchnorm_inference(f32* __restrict__ bias, f32* __restrict__ mean, f32* __restrict__ scale, f32* __restrict__ var, f32* __restrict__ x, f32* __restrict__ y, void* __restrict__ _tmp) {

    f32* var_eps = (f32*)((u8*)_tmp + 0);  // size = 3
    f32* sqrt_var_eps = (f32*)((u8*)_tmp + 12);  // size = 3
    f32* alpha = (f32*)((u8*)_tmp + 24);  // size = 3
    f32* beta = (f32*)((u8*)_tmp + 36);  // size = 3
    for (i32 i0 = 0; i0 < 3; i0++) {
        var_eps[i0] = var[i0] + 0.0001;
    }
    for (i32 i0 = 0; i0 < 3; i0++) {
        sqrt_var_eps[i0] = sqrtf(var_eps[i0]);
    }
    for (i32 i0 = 0; i0 < 3; i0++) {
        alpha[i0] = scale[i0] / sqrt_var_eps[i0];
    }
    for (i32 i0 = 0; i0 < 3; i0++) {
        beta[i0] = - alpha[i0] * mean[i0] + bias[i0];
    }
    for (i32 i0 = 0; i0 < 8; i0++) {
        for (i32 i1 = 0; i1 < 3; i1++) {
            for (i32 i2 = 0; i2 < 2048; i2++) {
                for (i32 i3 = 0; i3 < 2048; i3++) {
                    y[i0 * 3 * 2048 * 2048 + i1 * 2048 * 2048 + i2 * 2048 + i3] = alpha[i1] * x[i0 * 3 * 2048 * 2048 + i1 * 2048 * 2048 + i2 * 2048 + i3] + beta[i1];
                }
            }
        }
    }
}

