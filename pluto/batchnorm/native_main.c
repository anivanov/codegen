
#include <stddef.h>
#include <stdio.h>
#include <math.h>
#include <stdint.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <immintrin.h>
#include "generic_mini.h"

#if !defined(TOC)
static inline double elapsed_seconds(struct timespec start) {
    struct timespec end;
    clock_gettime(CLOCK_MONOTONIC, &end);
    return (end.tv_sec - start.tv_sec) + (end.tv_nsec - start.tv_nsec) / 1e9;
}
#define TIC(name) struct timespec dt_ ##name; clock_gettime(CLOCK_MONOTONIC, &dt_ ##name)
#define TOC(name) elapsed_seconds(dt_ ## name)
#endif


void batchnorm_inference(f32* __restrict__ bias, f32* __restrict__ mean, f32* __restrict__ scale, f32* __restrict__ var, f32* __restrict__ x, f32* __restrict__ y, void* __restrict__ _tmp) {
    
    f32* var_eps = (f32*)((u8*)_tmp + 0);  // size = 3
    f32* sqrt_var_eps = (f32*)((u8*)_tmp + 12);  // size = 3
    f32* alpha = (f32*)((u8*)_tmp + 24);  // size = 3
    f32* beta = (f32*)((u8*)_tmp + 36);  // size = 3
    #pragma scop
    for (int i0 = 0; i0 < 3; i0++) {
        var_eps[i0] = var[i0] + 0.0001;
    }
    for (int i0 = 0; i0 < 3; i0++) {
        sqrt_var_eps[i0] = sqrtf(var_eps[i0]);
    }
    for (int i0 = 0; i0 < 3; i0++) {
        alpha[i0] = scale[i0] / sqrt_var_eps[i0];
    }
    for (int i0 = 0; i0 < 3; i0++) {
        beta[i0] = - alpha[i0] * mean[i0] + bias[i0];
    }
    for (int i0 = 0; i0 < 8; i0++) {
        for (int i1 = 0; i1 < 3; i1++) {
            for (int i2 = 0; i2 < 2048; i2++) {
                for (int i3 = 0; i3 < 2048; i3++) {
                    y[i0 * 3 * 2048 * 2048 + i1 * 2048 * 2048 + i2 * 2048 + i3] = alpha[i1] * x[i0 * 3 * 2048 * 2048 + i1 * 2048 * 2048 + i2 * 2048 + i3] + beta[i1];
                }
            }
        }
    }
    #pragma endscop
}
int main() {
    f32* x = (f32*) aligned_alloc(4096, 100663296 * sizeof(f32));
    if (!x) { printf("ERROR: failed to allocate buffer x of size 100663296 * sizeof(f32)\n"); return 1; }
    read_file(x, 100663296 * sizeof(f32), "data_x.bin");
    f32* mean = (f32*) aligned_alloc(4096, 3 * sizeof(f32));
    if (!mean) { printf("ERROR: failed to allocate buffer mean of size 3 * sizeof(f32)\n"); return 1; }
    read_file(mean, 3 * sizeof(f32), "data_mean.bin");
    f32* var = (f32*) aligned_alloc(4096, 3 * sizeof(f32));
    if (!var) { printf("ERROR: failed to allocate buffer var of size 3 * sizeof(f32)\n"); return 1; }
    read_file(var, 3 * sizeof(f32), "data_var.bin");
    f32* scale = (f32*) aligned_alloc(4096, 3 * sizeof(f32));
    if (!scale) { printf("ERROR: failed to allocate buffer scale of size 3 * sizeof(f32)\n"); return 1; }
    read_file(scale, 3 * sizeof(f32), "data_scale.bin");
    f32* bias = (f32*) aligned_alloc(4096, 3 * sizeof(f32));
    if (!bias) { printf("ERROR: failed to allocate buffer bias of size 3 * sizeof(f32)\n"); return 1; }
    read_file(bias, 3 * sizeof(f32), "data_bias.bin");
    f32* y = (f32*) aligned_alloc(4096, 100663296 * sizeof(f32));
    if (!y) { printf("ERROR: failed to allocate buffer y of size 100663296 * sizeof(f32)\n"); return 1; }
    read_file(y, 100663296 * sizeof(f32), "data_y.bin");
    f32* result_y = (f32*) aligned_alloc(4096, 100663296 * sizeof(f32));
    if (!result_y) { printf("ERROR: failed to allocate buffer result_y of size 100663296 * sizeof(f32)\n"); return 1; }
    memset(result_y, 0, 100663296 * sizeof(f32));
    size_t buf_size = 48;
    int ok = 1;
    void* _tmp = aligned_alloc(4096, buf_size);
    if (!_tmp) { printf("ERROR: failed to allocate buffer _tmp\n"); return 1; }
    
    TIC(time_correctness);
    batchnorm_inference(bias, mean, scale, var, x, result_y, _tmp);
    double correctness_elapsed = TOC(time_correctness);
    if (correctness_elapsed > 5.0) {
        printf("Seconds: %f\n", correctness_elapsed);
        printf("success, exitting...\n");
        return 0;
    }
    for (int i = 0; i < 100663296; i++) {
        double eps = 1e-3;
        double abs_err = fabs(result_y[i] - y[i]);
        double rel_err = abs_err / (fabs(y[i]) + eps);
        if (abs_err > eps && rel_err > eps) {
            printf("Error: mismatch at y, %d, %f (computed) != %f (expected) \n", (int)i, (double)result_y[i], (double)y[i]);
            ok = 0;
            break;
        }
    }
    if (!ok) {
        printf("FAILURE, exitting...\n");
        return 1;
    }
    
    TIC(total);
    for (int i = 0; i < 13 || TOC(total) < 0.1; i++) {
        read_file(x, 100663296 * sizeof(f32), "data_x.bin");
        read_file(mean, 3 * sizeof(f32), "data_mean.bin");
        read_file(var, 3 * sizeof(f32), "data_var.bin");
        read_file(scale, 3 * sizeof(f32), "data_scale.bin");
        read_file(bias, 3 * sizeof(f32), "data_bias.bin");
        memset(result_y, 0, 100663296 * sizeof(f32));
        TIC(1);
        for (int j = 0; j < 1; j++) {
            batchnorm_inference(bias, mean, scale, var, x, result_y, _tmp);
        }
        double seconds = TOC(1) / 1;
        printf("Seconds: %f\n", seconds);
    }
    printf("success, exitting...\n");
    return 0;
}
