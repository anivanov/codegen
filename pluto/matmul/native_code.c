#include "generic_mini.h"
#include <stdio.h>
#include <stdint.h>
#include <math.h>
#include <float.h>
#include <immintrin.h>
#define u8 uint8_t
#define f32 float
#define f64 double


void matmul(f32* __restrict__ x, f32* __restrict__ y, f32* __restrict__ z, void* __restrict__ _tmp) {
    #pragma scop
    for (int i0 = 0; i0 < 768; i0++) {
        for (int i1 = 0; i1 < 1024; i1++) {
            z[i0 * 1024 + i1] = 0;  // init reduce
            for (int i2 = 0; i2 < 1024; i2++) {
                z[i0 * 1024 + i1] += x[i2 * 768 + i0] * y[i2 * 1024 + i1];
            }
        }
    }
    #pragma scop
}

