#include <omp.h>
#include <math.h>
#define ceild(n,d)  (((n)<0) ? -((-(n))/(d)) : ((n)+(d)-1)/(d))
#define floord(n,d) (((n)<0) ? -((-(n)+(d)-1)/(d)) : (n)/(d))
#define max(x,y)    ((x) > (y)? (x) : (y))
#define min(x,y)    ((x) < (y)? (x) : (y))


#include <stddef.h>
#include <stdio.h>
#include <math.h>
#include <stdint.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <immintrin.h>
#include "generic_mini.h"

#if !defined(TOC)
static inline double elapsed_seconds(struct timespec start) {
    struct timespec end;
    clock_gettime(CLOCK_MONOTONIC, &end);
    return (end.tv_sec - start.tv_sec) + (end.tv_nsec - start.tv_nsec) / 1e9;
}
#define TIC(name) struct timespec dt_ ##name; clock_gettime(CLOCK_MONOTONIC, &dt_ ##name)
#define TOC(name) elapsed_seconds(dt_ ## name)
#endif


void matmul(f32* __restrict__ x, f32* __restrict__ y, f32* __restrict__ z, void* __restrict__ _tmp) {
  int t1, t2, t3, t4, t5, t6, t7;
 int lb, ub, lbp, ubp, lb2, ub2;
 register int lbv, ubv;
lbp=0;
ubp=23;
#pragma omp parallel for private(lbv,ubv,t3,t4,t5,t6,t7)
for (t2=lbp;t2<=ubp;t2++) {
  for (t3=0;t3<=31;t3++) {
    for (t4=32*t2;t4<=32*t2+31;t4++) {
      lbv=32*t3;
      ubv=32*t3+31;
#pragma ivdep
#pragma vector always
      for (t5=lbv;t5<=ubv;t5++) {
        z[t4 * 1024 + t5] = 0;;
      }
    }
  }
}
lbp=0;
ubp=23;
#pragma omp parallel for private(lbv,ubv,t3,t4,t5,t6,t7)
for (t2=lbp;t2<=ubp;t2++) {
  for (t3=0;t3<=31;t3++) {
    for (t4=0;t4<=31;t4++) {
      for (t5=32*t2;t5<=32*t2+31-7;t5+=8) {
        for (t6=32*t4;t6<=32*t4+31-7;t6+=8) {
          lbv=32*t3;
          ubv=32*t3+31;
#pragma ivdep
#pragma vector always
          for (t7=lbv;t7<=ubv;t7++) {
            z[t5 * 1024 + t7] += x[t6 * 768 + t5] * y[t6 * 1024 + t7];;
            z[(t5+1) * 1024 + t7] += x[t6 * 768 + (t5+1)] * y[t6 * 1024 + t7];;
            z[(t5+2) * 1024 + t7] += x[t6 * 768 + (t5+2)] * y[t6 * 1024 + t7];;
            z[(t5+3) * 1024 + t7] += x[t6 * 768 + (t5+3)] * y[t6 * 1024 + t7];;
            z[(t5+4) * 1024 + t7] += x[t6 * 768 + (t5+4)] * y[t6 * 1024 + t7];;
            z[(t5+5) * 1024 + t7] += x[t6 * 768 + (t5+5)] * y[t6 * 1024 + t7];;
            z[(t5+6) * 1024 + t7] += x[t6 * 768 + (t5+6)] * y[t6 * 1024 + t7];;
            z[(t5+7) * 1024 + t7] += x[t6 * 768 + (t5+7)] * y[t6 * 1024 + t7];;
            z[t5 * 1024 + t7] += x[(t6+1) * 768 + t5] * y[(t6+1) * 1024 + t7];;
            z[(t5+1) * 1024 + t7] += x[(t6+1) * 768 + (t5+1)] * y[(t6+1) * 1024 + t7];;
            z[(t5+2) * 1024 + t7] += x[(t6+1) * 768 + (t5+2)] * y[(t6+1) * 1024 + t7];;
            z[(t5+3) * 1024 + t7] += x[(t6+1) * 768 + (t5+3)] * y[(t6+1) * 1024 + t7];;
            z[(t5+4) * 1024 + t7] += x[(t6+1) * 768 + (t5+4)] * y[(t6+1) * 1024 + t7];;
            z[(t5+5) * 1024 + t7] += x[(t6+1) * 768 + (t5+5)] * y[(t6+1) * 1024 + t7];;
            z[(t5+6) * 1024 + t7] += x[(t6+1) * 768 + (t5+6)] * y[(t6+1) * 1024 + t7];;
            z[(t5+7) * 1024 + t7] += x[(t6+1) * 768 + (t5+7)] * y[(t6+1) * 1024 + t7];;
            z[t5 * 1024 + t7] += x[(t6+2) * 768 + t5] * y[(t6+2) * 1024 + t7];;
            z[(t5+1) * 1024 + t7] += x[(t6+2) * 768 + (t5+1)] * y[(t6+2) * 1024 + t7];;
            z[(t5+2) * 1024 + t7] += x[(t6+2) * 768 + (t5+2)] * y[(t6+2) * 1024 + t7];;
            z[(t5+3) * 1024 + t7] += x[(t6+2) * 768 + (t5+3)] * y[(t6+2) * 1024 + t7];;
            z[(t5+4) * 1024 + t7] += x[(t6+2) * 768 + (t5+4)] * y[(t6+2) * 1024 + t7];;
            z[(t5+5) * 1024 + t7] += x[(t6+2) * 768 + (t5+5)] * y[(t6+2) * 1024 + t7];;
            z[(t5+6) * 1024 + t7] += x[(t6+2) * 768 + (t5+6)] * y[(t6+2) * 1024 + t7];;
            z[(t5+7) * 1024 + t7] += x[(t6+2) * 768 + (t5+7)] * y[(t6+2) * 1024 + t7];;
            z[t5 * 1024 + t7] += x[(t6+3) * 768 + t5] * y[(t6+3) * 1024 + t7];;
            z[(t5+1) * 1024 + t7] += x[(t6+3) * 768 + (t5+1)] * y[(t6+3) * 1024 + t7];;
            z[(t5+2) * 1024 + t7] += x[(t6+3) * 768 + (t5+2)] * y[(t6+3) * 1024 + t7];;
            z[(t5+3) * 1024 + t7] += x[(t6+3) * 768 + (t5+3)] * y[(t6+3) * 1024 + t7];;
            z[(t5+4) * 1024 + t7] += x[(t6+3) * 768 + (t5+4)] * y[(t6+3) * 1024 + t7];;
            z[(t5+5) * 1024 + t7] += x[(t6+3) * 768 + (t5+5)] * y[(t6+3) * 1024 + t7];;
            z[(t5+6) * 1024 + t7] += x[(t6+3) * 768 + (t5+6)] * y[(t6+3) * 1024 + t7];;
            z[(t5+7) * 1024 + t7] += x[(t6+3) * 768 + (t5+7)] * y[(t6+3) * 1024 + t7];;
            z[t5 * 1024 + t7] += x[(t6+4) * 768 + t5] * y[(t6+4) * 1024 + t7];;
            z[(t5+1) * 1024 + t7] += x[(t6+4) * 768 + (t5+1)] * y[(t6+4) * 1024 + t7];;
            z[(t5+2) * 1024 + t7] += x[(t6+4) * 768 + (t5+2)] * y[(t6+4) * 1024 + t7];;
            z[(t5+3) * 1024 + t7] += x[(t6+4) * 768 + (t5+3)] * y[(t6+4) * 1024 + t7];;
            z[(t5+4) * 1024 + t7] += x[(t6+4) * 768 + (t5+4)] * y[(t6+4) * 1024 + t7];;
            z[(t5+5) * 1024 + t7] += x[(t6+4) * 768 + (t5+5)] * y[(t6+4) * 1024 + t7];;
            z[(t5+6) * 1024 + t7] += x[(t6+4) * 768 + (t5+6)] * y[(t6+4) * 1024 + t7];;
            z[(t5+7) * 1024 + t7] += x[(t6+4) * 768 + (t5+7)] * y[(t6+4) * 1024 + t7];;
            z[t5 * 1024 + t7] += x[(t6+5) * 768 + t5] * y[(t6+5) * 1024 + t7];;
            z[(t5+1) * 1024 + t7] += x[(t6+5) * 768 + (t5+1)] * y[(t6+5) * 1024 + t7];;
            z[(t5+2) * 1024 + t7] += x[(t6+5) * 768 + (t5+2)] * y[(t6+5) * 1024 + t7];;
            z[(t5+3) * 1024 + t7] += x[(t6+5) * 768 + (t5+3)] * y[(t6+5) * 1024 + t7];;
            z[(t5+4) * 1024 + t7] += x[(t6+5) * 768 + (t5+4)] * y[(t6+5) * 1024 + t7];;
            z[(t5+5) * 1024 + t7] += x[(t6+5) * 768 + (t5+5)] * y[(t6+5) * 1024 + t7];;
            z[(t5+6) * 1024 + t7] += x[(t6+5) * 768 + (t5+6)] * y[(t6+5) * 1024 + t7];;
            z[(t5+7) * 1024 + t7] += x[(t6+5) * 768 + (t5+7)] * y[(t6+5) * 1024 + t7];;
            z[t5 * 1024 + t7] += x[(t6+6) * 768 + t5] * y[(t6+6) * 1024 + t7];;
            z[(t5+1) * 1024 + t7] += x[(t6+6) * 768 + (t5+1)] * y[(t6+6) * 1024 + t7];;
            z[(t5+2) * 1024 + t7] += x[(t6+6) * 768 + (t5+2)] * y[(t6+6) * 1024 + t7];;
            z[(t5+3) * 1024 + t7] += x[(t6+6) * 768 + (t5+3)] * y[(t6+6) * 1024 + t7];;
            z[(t5+4) * 1024 + t7] += x[(t6+6) * 768 + (t5+4)] * y[(t6+6) * 1024 + t7];;
            z[(t5+5) * 1024 + t7] += x[(t6+6) * 768 + (t5+5)] * y[(t6+6) * 1024 + t7];;
            z[(t5+6) * 1024 + t7] += x[(t6+6) * 768 + (t5+6)] * y[(t6+6) * 1024 + t7];;
            z[(t5+7) * 1024 + t7] += x[(t6+6) * 768 + (t5+7)] * y[(t6+6) * 1024 + t7];;
            z[t5 * 1024 + t7] += x[(t6+7) * 768 + t5] * y[(t6+7) * 1024 + t7];;
            z[(t5+1) * 1024 + t7] += x[(t6+7) * 768 + (t5+1)] * y[(t6+7) * 1024 + t7];;
            z[(t5+2) * 1024 + t7] += x[(t6+7) * 768 + (t5+2)] * y[(t6+7) * 1024 + t7];;
            z[(t5+3) * 1024 + t7] += x[(t6+7) * 768 + (t5+3)] * y[(t6+7) * 1024 + t7];;
            z[(t5+4) * 1024 + t7] += x[(t6+7) * 768 + (t5+4)] * y[(t6+7) * 1024 + t7];;
            z[(t5+5) * 1024 + t7] += x[(t6+7) * 768 + (t5+5)] * y[(t6+7) * 1024 + t7];;
            z[(t5+6) * 1024 + t7] += x[(t6+7) * 768 + (t5+6)] * y[(t6+7) * 1024 + t7];;
            z[(t5+7) * 1024 + t7] += x[(t6+7) * 768 + (t5+7)] * y[(t6+7) * 1024 + t7];;
          }
        }
        for (;t6<=32*t4+31;t6++) {
          lbv=32*t3;
          ubv=32*t3+31;
#pragma ivdep
#pragma vector always
          for (t7=lbv;t7<=ubv;t7++) {
            z[t5 * 1024 + t7] += x[t6 * 768 + t5] * y[t6 * 1024 + t7];;
            z[(t5+1) * 1024 + t7] += x[t6 * 768 + (t5+1)] * y[t6 * 1024 + t7];;
            z[(t5+2) * 1024 + t7] += x[t6 * 768 + (t5+2)] * y[t6 * 1024 + t7];;
            z[(t5+3) * 1024 + t7] += x[t6 * 768 + (t5+3)] * y[t6 * 1024 + t7];;
            z[(t5+4) * 1024 + t7] += x[t6 * 768 + (t5+4)] * y[t6 * 1024 + t7];;
            z[(t5+5) * 1024 + t7] += x[t6 * 768 + (t5+5)] * y[t6 * 1024 + t7];;
            z[(t5+6) * 1024 + t7] += x[t6 * 768 + (t5+6)] * y[t6 * 1024 + t7];;
            z[(t5+7) * 1024 + t7] += x[t6 * 768 + (t5+7)] * y[t6 * 1024 + t7];;
          }
        }
      }
      for (;t5<=32*t2+31;t5++) {
        for (t6=32*t4;t6<=32*t4+31;t6++) {
          lbv=32*t3;
          ubv=32*t3+31;
#pragma ivdep
#pragma vector always
          for (t7=lbv;t7<=ubv;t7++) {
            z[t5 * 1024 + t7] += x[t6 * 768 + t5] * y[t6 * 1024 + t7];;
          }
        }
      }
    }
  }
}
}

int main() {
    f32* x = (f32*) aligned_alloc(4096, 786432 * sizeof(f32));
    if (!x) { printf("ERROR: failed to allocate buffer x of size 786432 * sizeof(f32)\n"); return 1; }
    read_file(x, 786432 * sizeof(f32), "data_x.bin");
    f32* y = (f32*) aligned_alloc(4096, 1048576 * sizeof(f32));
    if (!y) { printf("ERROR: failed to allocate buffer y of size 1048576 * sizeof(f32)\n"); return 1; }
    read_file(y, 1048576 * sizeof(f32), "data_y.bin");
    f32* z = (f32*) aligned_alloc(4096, 786432 * sizeof(f32));
    if (!z) { printf("ERROR: failed to allocate buffer z of size 786432 * sizeof(f32)\n"); return 1; }
    read_file(z, 786432 * sizeof(f32), "data_z.bin");
    f32* result_z = (f32*) aligned_alloc(4096, 786432 * sizeof(f32));
    if (!result_z) { printf("ERROR: failed to allocate buffer result_z of size 786432 * sizeof(f32)\n"); return 1; }
    memset(result_z, 0, 786432 * sizeof(f32));
    size_t buf_size = 0;
    i32 ok = 1;
    void* _tmp = aligned_alloc(4096, buf_size);
    if (!_tmp) { printf("ERROR: failed to allocate buffer _tmp\n"); return 1; }
    
    TIC(time_correctness);
    matmul(x, y, result_z, _tmp);
    double correctness_elapsed = TOC(time_correctness);
    if (correctness_elapsed > 5.0) {
        printf("Seconds: %f\n", correctness_elapsed);
        printf("success, exitting...\n");
        return 0;
    }
    for (int i = 0; i < 786432; i++) {
        double eps = 1e-3;
        double abs_err = fabs(result_z[i] - z[i]);
        double rel_err = abs_err / (fabs(z[i]) + eps);
        if (abs_err > eps && rel_err > eps) {
            printf("Error: mismatch at z, %d, %f (computed) != %f (expected) \n", (int)i, (double)result_z[i], (double)z[i]);
            ok = 0;
            break;
        }
    }
    if (!ok) {
        printf("FAILURE, exitting...\n");
        return 1;
    }
    
    TIC(total);
    for (int i = 0; i < 13 || TOC(total) < 0.1; i++) {
        read_file(x, 786432 * sizeof(f32), "data_x.bin");
        read_file(y, 1048576 * sizeof(f32), "data_y.bin");
        memset(result_z, 0, 786432 * sizeof(f32));
        TIC(1);
        for (int j = 0; j < 1; j++) {
            matmul(x, y, result_z, _tmp);
        }
        double seconds = TOC(1) / 1;
        printf("%f\n", seconds);
    }
    printf("success, exitting...\n");
    return 0;
}
