
#include <stddef.h>
#include <stdio.h>
#include <math.h>
#include <stdint.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <immintrin.h>
#include "generic_mini.h"

#if !defined(TOC)
static inline double elapsed_seconds(struct timespec start) {
    struct timespec end;
    clock_gettime(CLOCK_MONOTONIC, &end);
    return (end.tv_sec - start.tv_sec) + (end.tv_nsec - start.tv_nsec) / 1e9;
}
#define TIC(name) struct timespec dt_ ##name; clock_gettime(CLOCK_MONOTONIC, &dt_ ##name)
#define TOC(name) elapsed_seconds(dt_ ## name)
#endif


void matmul(f32* __restrict__ x, f32* __restrict__ y, f32* __restrict__ z, void* __restrict__ _tmp) {
    #pragma scop
    for (int i0 = 0; i0 < 768; i0++) {
        for (int i1 = 0; i1 < 1024; i1++) {
            z[i0 * 1024 + i1] = 0;  // init reduce
            for (int i2 = 0; i2 < 1024; i2++) {
                z[i0 * 1024 + i1] += x[i2 * 768 + i0] * y[i2 * 1024 + i1];
            }
        }
    }
    #pragma endscop
}

int main() {
    f32* x = (f32*) aligned_alloc(4096, 786432 * sizeof(f32));
    if (!x) { printf("ERROR: failed to allocate buffer x of size 786432 * sizeof(f32)\n"); return 1; }
    read_file(x, 786432 * sizeof(f32), "data_x.bin");
    f32* y = (f32*) aligned_alloc(4096, 1048576 * sizeof(f32));
    if (!y) { printf("ERROR: failed to allocate buffer y of size 1048576 * sizeof(f32)\n"); return 1; }
    read_file(y, 1048576 * sizeof(f32), "data_y.bin");
    f32* z = (f32*) aligned_alloc(4096, 786432 * sizeof(f32));
    if (!z) { printf("ERROR: failed to allocate buffer z of size 786432 * sizeof(f32)\n"); return 1; }
    read_file(z, 786432 * sizeof(f32), "data_z.bin");
    f32* result_z = (f32*) aligned_alloc(4096, 786432 * sizeof(f32));
    if (!result_z) { printf("ERROR: failed to allocate buffer result_z of size 786432 * sizeof(f32)\n"); return 1; }
    memset(result_z, 0, 786432 * sizeof(f32));
    size_t buf_size = 0;
    i32 ok = 1;
    void* _tmp = aligned_alloc(4096, buf_size);
    if (!_tmp) { printf("ERROR: failed to allocate buffer _tmp\n"); return 1; }
    
    TIC(time_correctness);
    matmul(x, y, result_z, _tmp);
    double correctness_elapsed = TOC(time_correctness);
    if (correctness_elapsed > 5.0) {
        printf("Seconds: %f\n", correctness_elapsed);
        printf("success, exitting...\n");
        return 0;
    }
    for (int i = 0; i < 786432; i++) {
        double eps = 1e-3;
        double abs_err = fabs(result_z[i] - z[i]);
        double rel_err = abs_err / (fabs(z[i]) + eps);
        if (abs_err > eps && rel_err > eps) {
            printf("Error: mismatch at z, %d, %f (computed) != %f (expected) \n", (int)i, (double)result_z[i], (double)z[i]);
            ok = 0;
            break;
        }
    }
    if (!ok) {
        printf("FAILURE, exitting...\n");
        return 1;
    }
    
    TIC(total);
    for (int i = 0; i < 13 || TOC(total) < 0.1; i++) {
        read_file(x, 786432 * sizeof(f32), "data_x.bin");
        read_file(y, 1048576 * sizeof(f32), "data_y.bin");
        memset(result_z, 0, 786432 * sizeof(f32));
        TIC(1);
        for (int j = 0; j < 1; j++) {
            matmul(x, y, result_z, _tmp);
        }
        double seconds = TOC(1) / 1;
        printf("Seconds: %f\n", seconds);
    }
    printf("success, exitting...\n");
    return 0;
}
