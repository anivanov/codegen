#include "generic_mini.h"
#include <stdio.h>
#include <stdint.h>
#include <math.h>
#include <float.h>
#include <immintrin.h>
#define u8 uint8_t
#define i32 int32_t
#define f32 float
#define f64 double


void relu(f32* __restrict__ x, f32* __restrict__ z, void* __restrict__ _tmp) {

    for (i32 i0 = 0; i0 < 16777216; i0++) {
        z[i0] = fmaxf(x[i0], 0);
    }
}

