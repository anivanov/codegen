#pragma once

#include "math.h"
#include <stdint.h>
#include <immintrin.h>
#include <stdio.h>
#include <stdlib.h>

#if __CUDACC__
#include <cuda.h>
#include <cuda_fp16.h>
#include <mma.h>
using namespace nvcuda;  // wmma
#endif

#if __CUDACC__
    #define HOST_DEVICE __host__ __device__
#else
    #define HOST_DEVICE
#endif

#if __CUDACC__
    #define f16 __half
#else
#define f16 _Float16
#endif
#define f32 float
#define f64 double

#define i8 int8_t
#define i16 int16_t
#define i32 int32_t
#define i64 int64_t
#define s256 __m256
#define s512 __m512
#define d256 __m256d
#define d512 __m512d
#define h512 __m512h
#define h256 __m256h
#define u8 uint8_t
#define u16 uint16_t
#define u32 uint32_t
#define u64 uint64_t

HOST_DEVICE
inline double my_div(double a, double b) {
    return a / b;
}

HOST_DEVICE
inline double my_sqrt(double a) {
    return sqrt(a);
}

HOST_DEVICE
inline double my_sgnjx(double a, double b) {
    return (b < 0) ? -a : a;
}

HOST_DEVICE
inline float reinterpret_f32_from_i32(int32_t i) {
    union {
        int32_t i;
        float f;
    } u;
    u.i = i;
    return u.f;
}

inline __m256 allreduce_max(__m256 x) {
    __m256 y = _mm256_permute2f128_ps(x, x, 1);
    __m256 m1 = _mm256_max_ps(x, y);
    __m256 m2 = _mm256_permute_ps(m1, _MM_SHUFFLE(2, 3, 0, 1));
    __m256 m3 = _mm256_max_ps(m1, m2);
    __m256 m4 = _mm256_permute_ps(m3, _MM_SHUFFLE(1, 0, 3, 2));
    __m256 result = _mm256_max_ps(m3, m4);
    return result;
}

inline float reduce_add(__m256 x) {
    float values[8] = {};
    _mm256_store_ps(values, x);
    float sum = 0;
    for(int i = 0; i < 8; i++){
        sum += values[i];
    }
    return sum;
}

inline float vec_to_scalar_s256(s256 x, int idx){
    union {
      s256 m;
      float a[8];
    } vector;
    vector.m = x;
    return vector.a[idx];
}
inline double vec_to_scalar_d256(d256 x, int idx) {
  union {
    d256 m;
    double a[4];
  } vector;
  vector.m = x;
  return vector.a[idx];
}
inline f16 vec_to_scalar_h256(h256 x, int idx) {
  union {
    h256 m;
    f16 a[16];
  } vector;
  vector.m = x;
  return vector.a[idx];
}

inline float vec_to_scalar_s512(s512 x, int idx) {
  union {
    s512 m;
    float a[16];
  } vector;
  vector.m = x;
  return vector.a[idx];
}

inline f16 vec_to_scalar_h512(h512 x, int idx) {
  union {
    h512 m;
    f16 a[32];
  } vector;
  vector.m = x;
  return vector.a[idx];
}

inline double vec_to_scalar_d512(d512 x, int idx) {
  union {
    d512 m;
    double a[8];
  } vector;
  vector.m = x;
  return vector.a[idx];
}

HOST_DEVICE
inline int32_t reinterpret_i32_from_f32(float f) {
    union {
        int32_t i;
        float f;
    } u;
    u.f = f;
    return u.i;
}

static inline void read_file(void* buf, size_t size, const char* name) {
    FILE* f = fopen(name, "rb");
    if (!f) {
        printf("ERROR: failed to open file %s\n", name);
        exit(1);
    }
    size_t read = fread(buf, 1, size, f);
    if (read != size) {
        printf("ERROR: failed to read file %s\n", name);
        exit(1);
    }
    fclose(f);
}

#define checkCudaErrors(expr) do { \
    cudaError_t err = (expr); \
    if (err != cudaSuccess) { \
        printf("Error in %s:%d. Message: %s\n", __FILE__, __LINE__, cudaGetErrorString(err)); \
        exit(1); \
    } \
} while (0)
