#include "generic_mini.h"
#include <stdio.h>
#include <stdint.h>
#include <math.h>
#include <float.h>
#include <immintrin.h>
#define u8 uint8_t
#define i32 int32_t
#define f32 float
#define f64 double


void reducemean(f32* __restrict__ x, f32* __restrict__ z, void* __restrict__ _tmp) {

    f32* t = (f32*)((u8*)_tmp + 0);  // size = 4096
    for (i32 i0 = 0; i0 < 4096; i0++) {
        t[i0] = 0;  // init reduce
        for (i32 i1 = 0; i1 < 4096; i1++) {
            t[i0] += x[i0 * 4096 + i1];
        }
    }
    for (i32 i0 = 0; i0 < 4096; i0++) {
        z[i0] = t[i0] / 4096;
    }
}

