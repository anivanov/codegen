from setuptools import setup

setup(
    packages=["kelgen"],
    package_dir={"kelgen": "microkernels"},
)