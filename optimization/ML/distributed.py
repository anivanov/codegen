from dnn_kernels.microkernels.gen import *
from dnn_kernels.microkernels.kernels import *
import random
import yaml
import ray
import time

class YamlDumper(yaml.Dumper):
    def ignore_aliases(self, data):
        return True

# @ray.remote
# def evaluate_program(program, data_size):
#     input_data = generate_input_for_program(program)
#     output_data = ref_layernorm({**input_data, **data_size})
#     return test_snitch_code(program, {**input_data, **output_data}, simulator='rtl', check_correctness=True)


if __name__ == "__main__":
    start = time.time()
    program = create_layernorm_program()
    data_size = {'B': 32, 'N': 32}
    specialize_inputs(program, data_size)
    end = time.time()
    print("Time to generate program:", end - start)

    start = time.time()
    applicable_transforms = find_applicable_transforms(program)
    add_transform_annotations(program, applicable_transforms)
    end = time.time()
    print("Time to find applicable transforms:", end - start)

    counter = 0

    random.seed(0)
    
    sum_transform = 0
    sum_find = 0
    start = time.time()
    while applicable_transforms:
        counter += 1
        print(f"================= {counter} =================")
        # print intermediate state
        # ln_json = json.dumps(program, cls=ProgramJSONEncoder)
        ln_json = program.to_dict()
        print(yaml.dump(ln_json, default_flow_style=False, Dumper=YamlDumper))
        
        transform_idx = random.randint(0, len(applicable_transforms) - 1)
        transform, args = applicable_transforms[transform_idx]
        
        if counter == 100:
            break
        
        # if counter == 82:
        #     break
        start = time.time()
        print(f"Applying transform {transform_idx}: {transform.__name__}(program, {args_as_str(args)})")
        transform(program, *args)
        end = time.time()
        sum_transform += end - start

        start = time.time()
        applicable_transforms = find_applicable_transforms(program)
        add_transform_annotations(program, applicable_transforms)
        end = time.time()
        sum_find += end - start

    end = time.time()
    print("Time to search transforms:", end - start)
    print("Time to apply transforms:", sum_transform)
    print("Time to find applicable transforms:", sum_find)
    # ln_json = json.dumps(program, cls=ProgramJSONEncoder)
    # pprint(json.loads(ln_json), width=200, compact=True, sort_dicts=False)

    print("Applied", counter, "transforms")
    start = time.time()
    input_data = generate_input_for_program(program)
    output_data = ref_layernorm({**input_data, **data_size})
    cycles = test_snitch_code(program, {**input_data, **output_data}, simulator='rtl', check_correctness=True)
    end = time.time()
    print("Time to run program:", end - start)
