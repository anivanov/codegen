import torch
from transformers import LLama2Tokenizer
from trl import AutoModelForCausalLMWithValueHead, PPOConfig, PPOTrainer
from dnn_kernels.microkernels.gen import *
from dnn_kernels.microkernels.kernels import *
import copy
import yaml

class YamlDumper(yaml.Dumper):
    def ignore_aliases(self, data):
        return True

model = AutoModelForCausalLMWithValueHead.from_pretrained("codellama/CodeLlama-7b-hf")
model_ref = AutoModelForCausalLMWithValueHead.from_pretrained("codellama/CodeLlama-7b-hf")
tokenizer = LLama2Tokenizer.from_pretrained("codellama/CodeLlama-7b-hf")
tokenizer.pad_token = tokenizer.eos_token

ppo_config = {"mini_batch_size": 100, "batch_size": 100}
config = PPOConfig(**ppo_config)
ppo_trainer = PPOTrainer(config, model, model_ref, tokenizer)

original_program = create_layernorm_program()
data_size = {'B': 32, 'N': 32}
specialize_inputs(original_program, data_size)
normalization_factor = 1000
random.seed(0)

def select_random_transform(applicable_transforms):
    return random.randint(0, len(applicable_transforms) - 1)

def select_neural_transform(program, applicable_transforms):
    N = len(applicable_transforms)
    preamble = f"You are a senior performance engineer. You are tasked with optimizing the following code by selecting a transformation with index 0 to {N-1}. Select one transformation and provide the index. The program is: \n"
    query = preamble + program
    query_tensor = tokenizer.encode(query, return_tensors="pt").to(model.pretrained_model.device)
    generation_kwargs = {
        "min_length": -1,
        "top_k": 0.0,
        "top_p": 1.0,
        "do_sample": True,
        "pad_token_id": tokenizer.eos_token_id,
        "max_new_tokens": 1,
    }
    response_tensor = ppo_trainer.generate([item for item in query_tensor], return_prompt=False, **generation_kwargs)
    index_choice = int(tokenizer.decode(response_tensor[0]))
    return query_tensor, response_tensor, index_choice

iters = 0
while (iters < 100):
    iters += 1
    program = copy.deepcopy(original_program)
    applicable_transforms = find_applicable_transforms(program)
    add_transform_annotations(program, applicable_transforms)

    rewards = []
    queries = []
    responses = []

    counter = 0
    while applicable_transforms or counter > 200:
        counter += 1
        ln_json = program.to_dict()
        query, response, transform_idx = select_neural_transform(yaml.dump(ln_json, default_flow_style=False, Dumper=YamlDumper), applicable_transforms) # select_random_transform(applicable_transforms)
        queries.append(query[0])
        responses.append(response[0])
        transform, args = applicable_transforms[transform_idx]
        transform(program, *args)
        applicable_transforms = find_applicable_transforms(program)
        add_transform_annotations(program, applicable_transforms)
        rewards.append(torch.tensor(-0.01, device=model.pretrained_model.device))

    input_data = generate_input_for_program(program)
    output_data = ref_layernorm({**input_data, **data_size})
    normalized_cycles = test_snitch_code(program, {**input_data, **output_data}, simulator='rtl', check_correctness=True) / normalization_factor
    rewards.append(torch.tensor(normalized_cycles, device=model.pretrained_model.device))
    train_stats = ppo_trainer.step(queries, responses, rewards)