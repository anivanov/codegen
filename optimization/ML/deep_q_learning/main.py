import argparse
import json
import os
import torch
import sys
from trainer import Trainer
from logger import Logger
from kelgen.kernels import KERNEL_DICT
import numpy as np
import random


def flatten_kernel_list(kernels):
    kernel_list = []
    for kernel_name, kernel_sizes in kernels.items():
        for kernel_size in kernel_sizes:
            kernel_cls = KERNEL_DICT[kernel_name]
            kernel_list.append((kernel_cls, kernel_size))
    return kernel_list


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Deep Q Learning approach for microkernel optimization')

    parser.add_argument('-c', '--config', type=str, default='configs/config.json', help='Path to the configuration file')
    
    args = parser.parse_args()
    
    assert args.config is not None, "Please provide a configuration file using the -c flag"

    assert os.path.exists(args.config), f"Configuration file {args.config} does not exist"

    # Reads the configuration file and initializes the trainer
    with open(args.config) as f:
        config = json.load(f)

    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    if config['verbose']:
        print("Training Configuration:")
        print(json.dumps(config, indent=4))
        print(f"Device: {device}")
    
    if config['checkpoint_dir'] is not None:
        # create directory if it does not exist
        os.makedirs(config['checkpoint_dir'] , exist_ok=True)
    
    random_seed = config.get('random_seed', 42)
    random.seed(random_seed)
    np.random.seed(random_seed)
    torch.manual_seed(random_seed)
    
    trainer = Trainer(config, device=device)
    
    training_kernels = config.get("training_kernels", {
        "layernorm": ['4096x4096'],
    })
    eval_kernels = config.get("eval_kernels", {
        "layernorm": ['4096x4096'],
    })
    
    training_kernels_list = flatten_kernel_list(training_kernels)
    eval_kernels_list = flatten_kernel_list(eval_kernels)
    
    training_res = trainer.train(training_kernels_list, eval_kernels_list)
