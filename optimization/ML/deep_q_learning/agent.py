# This file contains the agent class that performs optimization on the
# microkernel and learns from the environment.
import numpy as np
import torch
from typing import List, Optional, Dict, Tuple
from numpy.random import Generator
import sys
import os
from dqn import BasicDQN
from replay_buffer import PrioritizedReplayBuffer, Transition
from kelgen import RootScope
from kelgen.transformations import native_transformations
import copy
from pathlib import Path
from model import DummyModel, LLMModel, BERTModel


def stop_transformation(kernel):
    """
    A transformation that does not apply any changes to the kernel
    It is used as a placeholder for the stop action in the DQN
    """
    pass


def transformation_to_str(kernel, transform, loc):
    kernel_copy = copy.deepcopy(kernel)
    transform(kernel_copy, *loc)
    kernel_str = kernel_copy.text()
    return kernel_str


class Agent(object):
    """
    A class that represents the reinforcement learning agent that
    performs optimization on the microkernel and learns from the
    environment.
    """
    def __init__(self, device: str,
                 model_name: str,
                 model_type: str,
                 batch_size: int,
                 fine_tune: bool,
                 hidden_layers: List[int],
                 lr: float, gamma: float, replay_buffer_size: int,
                 update_freq: int, use_target_net: bool = True,
                 clip_gradient: bool = False, per_alpha: float = 0.7,
                 per_beta_start: float = 0.5, per_beta_steps: int = 1000,
                 max_q_learning: bool = False,
                 rng: Optional[Generator] = None,
                 checkpoint_dir: str = "checkpoints",
                 logger=None,
                 checkpoint_freq: int = 100,
                 ) -> None:
        """
        Initializes the agent with the given hyperparameters
        @param device: The device on which the agent will run
        @kernel_embedding_size: The size of the kernel embedding
        @action_embedding_size: The size of the action embedding
        @param hidden_layers: A list of integers that represent the number of neurons in each hidden layer
        @param lr: The learning rate of the optimizer
        @param gamma: The discount factor of the rewards
        @param replay_buffer_size: The maximum number of transitions the replay buffer can store
        @param update_freq: The frequency of updating the target network
        @param use_target_net: A boolean that determines whether to use a target network
        @param max_q_learning: A boolean that determines whether to use the max Q-learning algorithm
        @param clip_gradient: A boolean that determines whether to clip the gradients
        @param per_alpha: A float between 0.0 and 1.0 that is used to scale the priorities in order
        to convert them into sampling probabilities. If uniform sampling is used (i.e. no PER),
        the value of alpha will be 0.
        @param per_beta_start: The initial value of beta for prioritized experience replay
        @param per_beta_steps: The number of steps to anneal beta to 1.0
        @param rng: A numpy random number generator object
        """
        
        ModelClass = {
            'dummy': DummyModel,
            'llm': LLMModel,
            'bert': BERTModel,
        }[model_type]
        self.model = ModelClass(model_name=model_name, batch_size=batch_size, trainable=fine_tune)
        self.fine_tune = fine_tune
        
        self.device = device
        self.gamma = gamma
        input_size = self.model.embedding_size * 2
        self.policy_net = BasicDQN(input_size, 1, hidden_layers).to(device=device)

        if use_target_net:
            self.target_net = BasicDQN(input_size, 1, hidden_layers).to(device=device)
            # Load the weights of the policy network to the target network
            self.target_net.load_state_dict(self.policy_net.state_dict())
        else:
            self.target_net = None

        self.update_freq = update_freq
        self.clip_gradient = clip_gradient

        self.loss_fn = torch.nn.MSELoss()
        
        self.scaler = torch.amp.GradScaler('cuda') if self.device == 'cuda' else None

        self.trainable_parameters = list(self.policy_net.parameters())
        if self.fine_tune:
            self.trainable_parameters += list(self.model.parameters())
        self.optimizer = torch.optim.Adam(self.trainable_parameters, lr=lr)

        # Prioritized Experience Replay parameters
        # See https://paperswithcode.com/method/prioritized-experience-replay for more details
        self.replay_buffer = PrioritizedReplayBuffer(replay_buffer_size, per_alpha, rng)
        self.per_beta = per_beta_start
        # The value of beta that is incremented at each step
        self.per_beta_increment = (1.0 - per_beta_start) / per_beta_steps

        self.rng = rng

        self.step = 0
        
        self.logger = logger
        
        self.checkpoint_freq = checkpoint_freq

        self.max_q_learning = max_q_learning

        self.checkpoint_dir = checkpoint_dir
        
    def save(self) -> Dict[str, object]:
        """
        Saves the agent to the given directory
        """
        checkpoint = {
            'policy_net': self.policy_net.state_dict(),
            'target_net': self.target_net.state_dict() if self.target_net is not None else None,
            'optimizer': self.optimizer.state_dict(),
            'step': self.step,
            'replay_buffer': self.replay_buffer.save(),
            'model': self.model.state_dict() if self.fine_tune else None,
            'scaler': self.scaler.state_dict() if self.scaler is not None else None
        }
        return checkpoint
        
    def load(self, checkpoint: Dict[str, object]) -> None:
        self.policy_net.load_state_dict(checkpoint['policy_net'])
        if self.target_net is not None:
            self.target_net.load_state_dict(checkpoint['target_net'])
        self.optimizer.load_state_dict(checkpoint['optimizer'])
        self.step = checkpoint['step']
        self.replay_buffer.load(checkpoint['replay_buffer'])
        if self.fine_tune:
            self.model.load_state_dict(checkpoint['model'])
        if self.scaler is not None:
            self.scaler.load_state_dict(checkpoint['scaler'])

    def store_transition(self, state: np.ndarray, next_state: np.ndarray, next_actions: np.ndarray, reward: float) -> None:
        """
        Stores the given transition into the replay buffer
        """
        new_transition = Transition(state, next_state, next_actions, reward)
        self.replay_buffer.push(new_transition)

    def get_actions(self, kernel: RootScope, stop: bool = False):
        transformations = [(stop_transformation, tuple())]
        if not stop:
            transformations += [
                (tapply, targs)
                for tname, (tapply, tcheck) in native_transformations().items()
                for targs in tcheck(kernel)
            ]
            
        transformations = [(stop_transformation, tuple())]
        if not stop:
            transformations += [
                (tapply, targs)
                for tname, (tapply, tcheck) in native_transformations().items()
                for targs in tcheck(kernel)
            ]
        
        return transformations
        
        # transformation_strs = []
        # for transform, loc in transformations:
        #     transformation_strs.append(transformation_to_str(kernel, transform, loc))        

        # transformation_embeddings = model.encode_transformations(transformation_strs)
        
        # return transformations, transformation_embeddings


    def compute_q_values(self, kernel_embedding: torch.Tensor, transformation_embeddings: torch.Tensor, use_target=False) -> torch.Tensor:
        """
        Computes the Q-values for each transformation given the kernel and transformation embeddings
        @param kernel_embedding: The embedding vector of the kernel
        @param transformation_embeddings: The embedding vectors of the transformations
        @return A tensor containing the Q-values for each transformation
        """
        # Concatenate the kernel to each transformation embedding
        input = torch.cat(
            (kernel_embedding.unsqueeze(0).expand(transformation_embeddings.shape[0], -1),
            transformation_embeddings), dim=1
        ).to(self.device, dtype=torch.float32)

        with torch.no_grad():
            if use_target and self.target_net is not None:
                q_values = self.target_net(input)
            else:
                q_values = self.policy_net(input)
        
        return q_values
        

    def select_action(
        self,
        kernel_embedding: torch.Tensor,
        transformation_embeddings: torch.Tensor,
        epsilon: float = 0.0,
        use_target: bool = False,
    ) -> Tuple:
        """
        Selects an action based on the given kernel to optimize and
        the LLM model. This is done by first converting the kernel
        and transformations into embedding vectors and then passing them
        through the policy network to get the Q-values for each
        action. The transformation with the highest Q-value is then selected.
        @param kernel_embedding: The embedding vector of the kernel
        @param kernel: The kernel to optimize
        @param model: The LLM model that encodes the kernel and transformations
        @param epsilon: The epsilon value for epsilon-greedy action selection
        @return A tuple containing two elements where the first elements
        contains the selected transformation and its location, and the second
        element contains the embedding vectors of all the transformations,
        and the index of the selected transformation.
        """

        sample = self.rng.random() if self.rng else np.random.random()

        num_transforms = transformation_embeddings.shape[0]

        if sample < epsilon:
            # Select a random transformation
            action_idx = self.rng.integers(0, num_transforms) if self.rng else np.random.randint(0, num_transforms)
        else:
            # Pass the input through the policy network
            q_values = self.compute_q_values(kernel_embedding, transformation_embeddings, use_target=use_target)
            # Select the transformation with the highest Q-value
            _, action_idx = torch.max(q_values, dim=0)
            action_idx = action_idx.item()
        
        return action_idx


    def optimize(self, batch_size: int = 32) -> float:
        """
        Performs one optimization step on the DQN by training from
        the samples stored in the replay buffer.
        Returns the loss of the optimization step. If the replay buffer
        does not have enough samples to fill a batch, inf will be returned.
        """

        self.step += 1

        if not self.replay_buffer.has_enough_samples(batch_size):
            return float('inf')
        
        transitions, indices, weights = self.replay_buffer.sample(batch_size, self.per_beta)
        batch = Transition(*zip(*transitions))

        target_net = self.target_net if self.target_net is not None else self.policy_net

        with torch.autocast(device_type=self.device, dtype=torch.float16, enabled=self.scaler is not None):
            with torch.set_grad_enabled(self.fine_tune):
                if self.fine_tune:
                    program_texts = batch.state
                    next_program_texts = batch.next_state
                    embeddings = self.model.encode_strings(program_texts, train=self.fine_tune).to(self.device, dtype=torch.float32)
                    next_embeddings = self.model.encode_strings(next_program_texts, train=self.fine_tune).to(self.device, dtype=torch.float32)
                else:
                    embeddings = torch.stack(batch.state).to(self.device, dtype=torch.float32)
                    next_embeddings = torch.stack(batch.next_state).to(self.device, dtype=torch.float32)
                actions = torch.cat((embeddings, next_embeddings), dim=1)
                rewards = torch.tensor(batch.reward).to(self.device, dtype=torch.float32)

            # Computes Q(s, a) directly from the policy network
            state_action_values = self.policy_net(actions)

            expected_values = []
            # Computes the V(s')
            with torch.no_grad():
                for transition, next_kernel_embedding in zip(transitions, next_embeddings):
                    if len(transition.next_actions) == 0:
                        expected_values.append(0.0)
                        continue
                    if self.fine_tune:
                        next_transform_texts = transition.next_actions
                        next_transform_embeddings = self.model.encode_strings(next_transform_texts, train=False).to(self.device, dtype=torch.float32)
                    else:
                        next_transform_embeddings = transition.next_actions.to(self.device, dtype=torch.float32)
                    next_input = torch.cat((next_kernel_embedding.unsqueeze(0).expand_as(next_transform_embeddings), next_transform_embeddings), dim=1)
                    next_state_action_values = target_net(next_input)
                    expected_values.append(torch.max(next_state_action_values).item())
                
                if not self.max_q_learning:
                    # Q-learning algorithm
                    expected_values = torch.tensor(expected_values).to(self.device) * self.gamma + rewards
                else:
                    # Max Q-learning algorithm
                    expected_values = torch.max(torch.tensor(expected_values).to(self.device) * self.gamma, rewards)

            if self.replay_buffer.alpha > 0.0:
                # If prioritized experience replay is used
                errors = state_action_values.squeeze(1) - expected_values
                # Updates sample priorities
                self.per_beta = min(1.0, self.per_beta + self.per_beta_increment)
                self.replay_buffer.update_priorities(indices, errors.abs().detach().cpu().numpy(), 0.1)
                loss = (errors.pow(2) * torch.tensor(weights).to(self.device)).mean()
            else:
                loss = self.loss_fn(state_action_values.squeeze(), expected_values)
        
        if self.scaler is not None:
            self.scaler.scale(loss).backward()
            self.scaler.step(self.optimizer)
            self.scaler.update()
        else:
            loss.backward()
            self.optimizer.step()
        
        if self.clip_gradient:
            for param in self.trainable_parameters:
                param.grad.data.clamp_(-1, 1)
        
        self.optimizer.zero_grad()

        if (self.step > 0) and (self.step % self.update_freq == 0) and \
            (self.target_net is not None):
            # Update the target network
            self.target_net.load_state_dict(self.policy_net.state_dict())

        return loss.item()
    
