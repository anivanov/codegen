import torch.nn as nn
import torch.nn.functional as F
from typing import List


class BasicDQN(nn.Module):
	"""
	The most basic deep neural network that consists of only fully-connected layers.
	"""
	def __init__(self, input_size: int, output_size: int, hidden_layers: List[int]):
		super(BasicDQN, self).__init__()
		self.input_layer = nn.Linear(input_size, hidden_layers[0])
		if not hidden_layers:
			hidden_layers.append(input_size)
		hidden = []
		for i in range(len(hidden_layers) - 1):
			hidden.append(nn.Linear(hidden_layers[i], hidden_layers[i + 1]))
		self.hidden_layers = nn.ModuleList(hidden)
		self.output_layer = nn.Linear(hidden_layers[-1], output_size)

	def forward(self, x):
		x = F.relu(self.input_layer(x))
		for layer in self.hidden_layers:
			x = F.relu(layer(x))

		return self.output_layer(x)