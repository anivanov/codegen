from typing import List, Optional, Dict, Tuple
from transformers import AutoModel, AutoTokenizer
import torch
import numpy as np
import transformers
from tqdm import tqdm



class FeatureExtractionModel(object):
    """
    Interface class for feature extraction models.
    """
    def __init__(self) -> None:
        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    def encode_kernel(self, kernel: str) -> torch.Tensor:
        return self.encode_strings([kernel])[0]
    
    def encode_transformation(self, transformation: str) -> torch.Tensor:
        return self.encode_strings([transformation])[0]
    
    def encode_transformations(self, transformations: List[str]) -> torch.Tensor:
        return self.encode_strings(transformations)
    
    def parameters(self):
        if hasattr(self, 'model'):
            return self.model.parameters()
        else:
            return []
        
    def state_dict(self):
        if hasattr(self, 'model'):
            return self.model.state_dict()
        else:
            return {}
        
    def load_state_dict(self, state_dict):
        if hasattr(self, 'model'):
            self.model.load_state_dict(state_dict)


class DummyModel(FeatureExtractionModel):
    """
    A dummy implementation of the LLM model that returns random vectors of correct shape.
    """

    def __init__(self, model_name, batch_size, trainable) -> None:
        super().__init__()
        self.embedding_size = 2048
    
    def encode_strings(self, strings: List[str], train: bool = False) -> torch.Tensor:
        return torch.randn(len(strings), self.embedding_size, device=self.device)


def sentence_embeddings_bert(lash_hidden_state):
    # expected input: [batch, seq_len, hidden_size]
    # expected output: [batch, hidden_size]
    # we return the first token's hidden state which supposedly represents the whole sequence for BERT-like models
    return lash_hidden_state[:, 0, :]


def sentence_embeddings_llm(last_hidden_state, attention_mask, weighted=False):
    # expected last_hidden_state: [batch, seq_len, hidden_size]
    # expected attention_mask: [batch, seq_len]  (0 for padding, 1 for non-padding)
    # expected output: [batch, hidden_size]
    batch, seq_len, hidden_size = last_hidden_state.shape
    assert attention_mask.shape == (batch, seq_len)
    # example: batch = 3, seq_len = 6
    # example: attention_mask = [[1, 1, 1, 0, 0, 0], [1, 1, 1, 1, 1, 1], [1, 1, 1, 1, 1, 0]]
    
    # some inputs can be padded to the length of the longest sentence in the batch
    # we should ignore corresponding output tokens, by setting their weight to 0
    if weighted:
        # we expect that each token has a weight of 1, 2, 3, ... and so on
        # this is based on intuition that each subsequent token contains more information about context
        # https://stackoverflow.com/a/77441536
        # 
        increasing_seq = torch.arange(start=1, end=seq_len + 1, device=attention_mask.device)  # [1, 2, 3, 4, 5, 6], shape = [seq_len]
        increasing_seq_with_batch = increasing_seq.unsqueeze(0)  # shape = [1, seq_len]
        # example: unscaled_weights = [[1, 2, 3, 0, 0, 0], [1, 2, 3, 4, 5, 6], [1, 2, 3, 4, 5, 0]]
        unscaled_weights = attention_mask * increasing_seq_with_batch  # shape = [batch, seq_len]
    else:
        # here we use simpler method: compute mean over non-padding tokens
        # https://stackoverflow.com/a/77389626
        # example: unscaled_weights = [[1, 1, 1, 0, 0, 0], [1, 1, 1, 1, 1, 1], [1, 1, 1, 1, 1, 0]]
        unscaled_weights = attention_mask  # shape = [batch, seq_len]

    # example: weights_sum = [[1+2+3], [1+2+3+4+5+6], [1+2+3+4+5]] = [[6], [21], [15]]   (assuming weighted=True)
    # example: weights_sum = [[1+1+1], [1+1+1+1+1+1], [1+1+1+1+1]] = [[3], [6], [5]]   (assuming weighted=False)
    weights_sum = torch.sum(unscaled_weights, dim=-1).unsqueeze(-1)  # shape = [batch, 1]
    # example: weights = [[1/6, 2/6, 3/6, 0, 0, 0], [1/21, 2/21, 3/21, 4/21, 5/21, 6/21], [1/15, 2/15, 3/15, 4/15, 5/15, 0]]  (assuming weighted=True)
    # example: weights = [[1/3, 1/3, 1/3, 0, 0, 0], [1/6, 1/6, 1/6, 1/6, 1/6, 1/6], [1/5, 1/5, 1/5, 1/5, 1/5, 0]]  (assuming weighted=False)
    weights = unscaled_weights / weights_sum  # shape = [batch, seq_len]
    unsqueezed_weights = weights.unsqueeze(-1)  # shape = [batch, seq_len, 1]
    precise_embeddings = last_hidden_state.to(dtype=torch.float32) # shape = [batch, seq_len, hidden_size]
    seq_embeddings = torch.sum(precise_embeddings * unsqueezed_weights, dim=1) # [batch, hidden_size]
    return seq_embeddings


class LLMModel(FeatureExtractionModel):
    """
    Interface class for LLM models.
    """
    def __init__(self, model_name, batch_size, trainable) -> None:
        super().__init__()
        self.trainable = trainable
        self.dtype = torch.float32 if trainable else torch.float16
        pipeline = transformers.pipeline("feature-extraction", model=model_name, torch_dtype=self.dtype, device_map="auto", framework="pt")
        self.model = pipeline.model
        self.tokenizer = transformers.AutoTokenizer.from_pretrained(model_name)
        self.tokenizer.truncation_side = "left"
        self.batch_size = batch_size
    
    @property
    def trunc_length(self) -> int:
        return self.model.config.max_position_embeddings
    
    @property
    def embedding_size(self) -> int:
        return self.model.config.hidden_size
    
    def encode_strings(self, strings: List[str], train: bool = False) -> torch.Tensor:
        with torch.autocast(device_type=self.device.type, enabled=self.trainable), torch.set_grad_enabled(train):
            embeddings_list = []
            for batch_idx in range(0, len(strings), self.batch_size):
                batch = strings[batch_idx:batch_idx + self.batch_size]
                inputs = self.tokenizer(batch, return_tensors="pt", padding=True, truncation=True, max_length=self.trunc_length)
                inputs = {key: val.to(self.device) for key, val in inputs.items()}
                outputs = self.model(**inputs)
                embeddings_batch = sentence_embeddings_llm(outputs.last_hidden_state, inputs['attention_mask'], weighted=False)
                embeddings_list.append(embeddings_batch)
            embeddings = torch.cat(embeddings_list, dim=0) if embeddings_list else torch.empty(0, self.embedding_size, device=self.device)
            return embeddings
        
        
class BERTModel(FeatureExtractionModel):
    """
    Interface class for BERT-like models.
    """
    def __init__(self, model_name, batch_size, trainable) -> None:
        super().__init__()
        self.trainable = trainable
        self.dtype = torch.float32 if trainable else torch.float16
        # pipeline = transformers.pipeline("feature-extraction", model=model_name, torch_dtype=self.dtype, device_map=self.device, framework="pt", trust_remote_code=True)
        self.model = AutoModel.from_pretrained(model_name, torch_dtype=self.dtype, device_map=self.device, trust_remote_code=True)
        self.tokenizer = AutoTokenizer.from_pretrained(model_name)
        self.tokenizer.truncation_side = "left"
        # self.model = pipeline.model
        # self.tokenizer = pipeline.tokenizer
        # self.tokenizer.truncation_side = "left"
        self.batch_size = batch_size
        
    @property
    def trunc_length(self) -> int:
        return self.model.config.max_position_embeddings
    
    @property
    def embedding_size(self) -> int:
        if hasattr(self.model.config, 'dim'):
            return self.model.config.dim
        else:
            return self.model.config.hidden_size
        
    def encode_strings(self, strings: List[str], train: bool = False) -> torch.Tensor:
        with torch.autocast(device_type=self.device.type, enabled=self.trainable), torch.set_grad_enabled(train):
            embeddings_list = []
            for batch_idx in range(0, len(strings), self.batch_size):
                batch = strings[batch_idx:batch_idx + self.batch_size]
                inputs = self.tokenizer(batch, return_tensors="pt", padding=True, truncation=True, max_length=self.trunc_length)
                inputs = {key: val.to(self.device) for key, val in inputs.items()}
                outputs = self.model(**inputs)
                embeddings_batch = sentence_embeddings_bert(outputs.last_hidden_state)
                embeddings_list.append(embeddings_batch)
            embeddings = torch.cat(embeddings_list, dim=0) if embeddings_list else torch.empty(0, self.embedding_size, device=self.device)
            return embeddings

