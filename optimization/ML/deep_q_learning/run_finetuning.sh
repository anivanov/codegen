#!/bin/bash -l
#SBATCH --job-name "Fine-tuning speedup prediction"
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH -A a-g34
#SBATCH --mem=256G
#SBATCH --time=8:00:00
#SBATCH --partition=normal

if [[ $# -eq 0 ]]; then
  echo "Usage: sbatch run_finetuning.sh <config>"
  exit 1
fi

config_path="$1"

if [[ ! -f "$config_path" ]]; then
  echo "Error: File '$config_path' does not exist or is not a regular file."
  exit 1
fi

srun --environment=codegen python3 llm_finetuning.py "$config_path"
