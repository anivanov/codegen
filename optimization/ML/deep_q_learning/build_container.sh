#!/bin/bash -l
#SBATCH --job-name "Build container for codegen"
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH -A a-g34
#SBATCH --partition=debug
#SBATCH --time=0:30:00

srun podman build -t codegen .
export XDG_RUNTIME_DIR=/dev/shm/$USER/xdg_runtime_dir
mkdir -p $XDG_RUNTIME_DIR

enroot import -o codegen_new.sqsh podman://codegen

# remove sqsh if exists
rm -f codegen.sqsh

mv codegen_new.sqsh codegen.sqsh
