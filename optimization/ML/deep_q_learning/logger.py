from pathlib import Path
import shutil
from typing import List, Callable


class Logger(object):
    """
    A class that encapsulates all the functions needed for logging
    """
    def __init__(self, log_dir: Path, overwrite: bool = False, write_fn: Callable = print) -> None:
        """
        Initializes the Logger object with the given log directory
        """
        self.log_dir = log_dir
        
        write_fn(f"Logs saved to '{self.log_dir}'")
        
        self.output_file = self.log_dir / "output.log"
        
        self.log_file = open(self.output_file, 'w' if overwrite else 'a')
        
        def write_fn_wrapper(msg: str) -> None:
            write_fn(msg)
            self.log_file.write(msg + "\n")
            self.log_file.flush()
        self.write = write_fn_wrapper
