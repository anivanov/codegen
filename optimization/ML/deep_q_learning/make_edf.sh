#!/bin/bash

SCRIPT_PATH=$(readlink -f "$0")
SCRIPT_DIR=$(dirname "$SCRIPT_PATH")
CODEGEN_DIR=$(readlink -f "$SCRIPT_DIR/../../../")

mkdir -p "$HOME/.edf"

cat << EOF > "$HOME/.edf/codegen.toml"
image = "$SCRIPT_DIR/codegen.sqsh"
mounts = ["/capstor", "/iopsstor", "/users", "$CODEGEN_DIR:/workspace/codegen"]

workdir = "$SCRIPT_DIR"

writable = true

[annotations]
com.hooks.aws_ofi_nccl.enabled = "true"
com.hooks.aws_ofi_nccl.variant = "cuda12"

[env]
CUDA_CACHE_DISABLE = "1"
NCCL_NET = "AWS Libfabric"
NCCL_CROSS_NIC = "1"
NCCL_NET_GDR_LEVEL = "PHB"
FI_CXI_DISABLE_HOST_REGISTER = "1"
FI_MR_CACHE_MONITOR = "userfaultfd"
FI_CXI_DEFAULT_CQ_SIZE = "131072"
FI_CXI_DEFAULT_TX_SIZE = "32768"
FI_CXI_RX_MATCH_MODE = "software"
FI_CXI_SAFE_DEVMEM_COPY_THRESHOLD = "16777216"
FI_CXI_COMPAT = "0"
TOKENIZERS_PARALLELISM = "false"
CC = "gcc"
EOF