import numpy as np
import torch
import random
from tqdm import tqdm
from typing import List, Optional, Dict, Tuple, Callable
from copy import deepcopy
import sys
import os
from agent import Agent, stop_transformation
from replay_buffer import Transition
from logger import Logger
import kelgen
from kelgen import RootScope, specialize_inputs
from kelgen.evaluator import Evaluator
from kelgen.measurements import RuntimeCache, RuntimeMeasurement, MeasurementStatus
import copy
import pickle
from pathlib import Path
import psutil
from agent import transformation_to_str
import wandb
import json
import hashlib
import pathlib
import time


def estimate_object_size(obj):
    try:
        serialized_data = pickle.dumps(obj)
        return len(serialized_data) / 2**20
    except Exception as e:
        print(f"Error serializing object: {e}")
        return -1


def process_memory_usage():
    process = psutil.Process()
    memory_info = process.memory_info()
    memory_in_mb = memory_info.rss / (1024 ** 2)
    return memory_in_mb
    

def leaky_reward(prev: float, curr: float, n: int = 5, timeout: bool = False) -> float:
    speedup = prev / curr
    if timeout:
        return -1.0
    if speedup >= 1:
        return speedup ** n - 1
    return 1 / n * (speedup - 1)


def discounted_log_reward(prev: float, curr: float, discount: float, timeout: bool = False) -> float:
    speedup = prev / curr
    if timeout:
        return -1.0
    return np.log(speedup) - discount


def inverse_reward(prev: float, curr: float, baseline: float, timeout: bool = False) -> float:
    if timeout:
        return 0
    return baseline / curr


def get_kernel_runtime(evaluator, program) -> float:
    status, runtime_ms, runtime_std = evaluator.eval(program)
    if status in (MeasurementStatus.MEASURED, MeasurementStatus.FROM_CACHE):
        return runtime_ms
    elif status == MeasurementStatus.TIMED_OUT:
        return evaluator.timeout_ms
    elif status == MeasurementStatus.RUNTIME_ERROR:
        raise RuntimeError(f"Runtime/validation error")
    else:
        raise ValueError(f"Unexpected status: {status}")


class Trainer(object):
    """
    A class that encapsulates all the functions needed for
    training and testing microkernel optimization agents.
    """

    def __init__(self, config: Dict, device: str = "cpu") -> None:
        """
        Initializes the Trainer object with the given agent and configuration
        """
        random_seed = config.get('random_seed', 42)
        self.rng = np.random.default_rng(random_seed)
        
        self.config = config
                
        self.checkpoint_freq = config.get('checkpoint_freq', 10)
        self.checkpoint_dir = Path(config.get('checkpoint_dir', 'checkpoints'))
        
        # create checkpoint directory if it does not exist
        self.checkpoint_dir.mkdir(parents=True, exist_ok=True)
        
        self.episode = self.find_checkpoint_episode(self.checkpoint_dir)
        overwrite_logs = self.episode == 0  # we will append if we are starting from checkpoint
        self.logger = Logger(self.checkpoint_dir, overwrite=overwrite_logs, write_fn=tqdm.write)
        if self.episode != 0:
            self.logger.write(f"Resuming training from checkpoint at episode {self.episode}")
        else:
            self.logger.write(f"No checkpoints found. Starting training from scratch")
        
        agent_args = dict(
            device=device,
            model_name=config['model_name'],
            model_type=config['model_type'],
            batch_size=config['batch_size'],
            fine_tune=config['fine_tune'],
            hidden_layers=config['hidden_layers'],
            lr=config['lr'],
            gamma=config['gamma'],
            replay_buffer_size=config['replay_buffer_size'],
            update_freq=config['update_freq'],
            use_target_net=config['use_target_net'],
            clip_gradient=config['clip_gradient'],
            per_alpha=config['per_alpha'],
            per_beta_start=config['per_beta_start'],
            per_beta_steps=config['per_beta_steps'],
            max_q_learning=config.get('max_q_learning', False),
            rng=self.rng,
            checkpoint_dir=self.checkpoint_dir,
            logger=self.logger,
            checkpoint_freq=self.checkpoint_freq,
        )

        # Initialize the agent
        self.agent = Agent(**agent_args)
        
        self.elapsed_seconds = 0
        self.running_min_runtime = {}  # {(kernel name, kernel size) -> min runtime}
        
        if self.episode > 0:
            self.load()
        
        self.reward_fn = {
            'leaky': lambda x, y, b, t: leaky_reward(x, y, n=config.get('leaky_reward_n', 5), timeout=t),
            'discounted_log': lambda x, y, b, t: discounted_log_reward(x, y, discount=config.get('reward_discount', 0.01), timeout=t),
            "inverse": lambda x, y, b, t: inverse_reward(x, y, b, timeout=t),
        }[config.get('reward', 'discounted_log')]
            
        adjacent_kernels_path = self.checkpoint_dir / 'adjacent_kernels.log'
        self.adjacent_kernels_file = open(adjacent_kernels_path, 'a')
            
        config_hash = hashlib.sha1(json.dumps(config, sort_keys=True).encode('utf-8')).hexdigest()[:8]
                
        wandb.init(
            entity="mycoolteam",
            project="rl-program-opt",
            config=config,
            id=f'run_{config_hash}',
            resume="allow",
            # mode='disabled',
        )
        
        wandb.define_metric("episode")
        wandb.define_metric("train/total_reward", step_metric="episode")
        wandb.define_metric("train/min_runtime", step_metric="episode")
        wandb.define_metric("train/avg_runtime", step_metric="episode")
        wandb.define_metric("train/max_runtime", step_metric="episode")
        wandb.define_metric("train/last_runtime", step_metric="episode")
        wandb.define_metric("eval/total_reward", step_metric="episode")
        wandb.define_metric("eval/min_runtime", step_metric="episode")
        wandb.define_metric("eval/avg_runtime", step_metric="episode")
        wandb.define_metric("eval/max_runtime", step_metric="episode")
        wandb.define_metric("eval/last_runtime", step_metric="episode")
        
    
    def find_checkpoint_episode(self, checkpoint_dir: Path) -> int:
        """
        Finds the episode of the latest checkpoint in the given directory
        """
        available_checkpoints = self.find_available_checkpoints(checkpoint_dir)
        if available_checkpoints:
            return available_checkpoints[-1]
        else:
            return 0
    
    def find_available_checkpoints(self, checkpoint_dir: Path) -> List[int]:
        """
        Finds the episodes of all available checkpoints in the given directory
        """
        episodes = []
        for file in os.listdir(checkpoint_dir):
            if file.startswith('checkpoint_'):
                episode = int(file.split('_')[-1].split('.')[0])
                episodes.append(episode)
        return sorted(episodes)

    def load(self) -> None:
        """
        Loads the agent from the given checkpoint directory
        """
        path = self.checkpoint_dir / f'checkpoint_{self.episode}.pth'
        if not path.exists():
            raise FileNotFoundError(f"Trainer checkpoint expected at {path} but not found")
        self.logger.write(f"Loading trainer from {path}")
        checkpoint = torch.load(path, weights_only=False)
        self.rng.bit_generator.state = checkpoint['rng']
        self.agent.load(checkpoint['agent'])
        self.elapsed_seconds = checkpoint['elapsed_seconds']
        self.running_min_runtime = checkpoint['running_min_runtime']
        
    def save(self) -> None:
        """
        Saves the agent to the given checkpoint directory
        """
        path = os.path.join(self.checkpoint_dir, 'checkpoint_' + str(self.episode) + '.pth')
        self.logger.write(f"Saving trainer to {path}")
        checkpoint = {
            'rng': self.rng.bit_generator.state,
            'agent': self.agent.save(),
            'elapsed_seconds': self.elapsed_seconds,
            'running_min_runtime': self.running_min_runtime,
        }
        torch.save(checkpoint, path)
        # delete old checkpoints that are not needed
        self.remove_old_checkpoints()
    
    def remove_old_checkpoints(self) -> None:
        available_checkpoints = self.find_available_checkpoints(self.checkpoint_dir)
        for episode in available_checkpoints[:-2]:
            path = os.path.join(self.checkpoint_dir, 'checkpoint_' + str(episode) + '.pth')
            os.remove(path)
            self.logger.write(f"Removed old checkpoint at {path}")

    def prepare_kernel_data(self, kernels):
        kernel_list = []
        for kernel_cls, kernel_size_str in kernels:
            kernel_params = kernel_cls.parse_params(kernel_size_str)
            base_kernel = kernel_cls.create_kernel(kernel_params)
            ref_data = kernel_cls.create_reference_data(kernel_params)
            ref_mean, ref_std, ref_times = kernel_cls.evaluate_baseline(ref_data, min_repeats=10)
            self.logger.write(f"Reference {base_kernel.name} size {kernel_size_str} runtime [ms]: avg {ref_mean * 1e3:.3f} std {ref_std * 1e3:.3f} reps {len(ref_times)}")
            ref_ms = ref_mean * 1e3
            cache = RuntimeCache(f'cache-{base_kernel.name}-{kernel_size_str}.db')
            evaluator = Evaluator(
                ref_data, min_repeats=3, min_seconds=1.0, timeout_ms=1000 * ref_ms, cache=cache, timeout_adjust=False, verify=True, reinit_input=True
            )
            basline_runtime = get_kernel_runtime(evaluator, base_kernel)
            self.logger.write(f"Baseline {base_kernel.name} size {kernel_size_str} runtime [ms]: avg {basline_runtime:.3f}")
            evaluator.timeout_ms = basline_runtime * 20
            kernel_list.append((kernel_cls, kernel_size_str, base_kernel, basline_runtime, ref_ms, evaluator))
        return kernel_list

    def train(self, training_kernels_list, eval_kernels_list) -> Dict:
        """
        Trains the RL agent using the given configuration and the given
        list of kernels.
        Returns a dictionary that maps each kernel to the 
        a tuple of training losses and rewards.
        """
        
        res = {}

        episodes = self.config['episodes']
        epsilon = self.config['epsilon_start']
        epsilon_decay = self.config['epsilon_decay']
        min_epsilon = self.config['min_epsilon']
        batch_size = self.config['batch_size']
        train_steps = self.config['train_steps']

        traininig_kernels = self.prepare_kernel_data(training_kernels_list)
        eval_kernels = self.prepare_kernel_data(eval_kernels_list)
        
        adjacent_tranformations = {}  # kernel_hash -> list[ kernel_hashes ]

        with tqdm(range(self.episode, episodes), desc="Epoch", leave=False, initial=self.episode, total=episodes) as tqdm_range:
            for episode in tqdm_range:
                # sample kernel to use in current episode
                kernel_cls, kernel_size_str, base_kernel, base_ms, ref_ms, evaluator = self.rng.choice(traininig_kernels)
                
                self.logger.write(f"===================== Kernel: {base_kernel.name} Size: {kernel_size_str} =====================")
                
                visited_kernel_hashes = set()
                
                kernel = copy.deepcopy(base_kernel)
                runtime = get_kernel_runtime(evaluator, kernel)
                kernel_hash = kernel.program_hash()
                kernel_text = kernel.text()
                kernel_embedding = self.agent.model.encode_kernel(kernel_text)
                transformations = self.agent.get_actions(kernel, stop=False)
                
                transformation_texts = []
                for transform, loc in transformations:
                    new_kernel = copy.deepcopy(kernel)
                    transform(new_kernel, *loc)
                    transformation_texts.append(new_kernel.text())
                transformation_embeddings = self.agent.model.encode_transformations(transformation_texts)

                visited_kernel_hashes.add(kernel_hash)

                episode_losses = []
                episode_rewards = []
                episode_runtimes = [runtime]

                episode_start_time = time.time()
                with tqdm(range(train_steps), leave=False, desc="Step") as epoch_steps:
                    for step in epoch_steps:
                        # Chooses an action
                        action_idx = self.agent.select_action(kernel_embedding, transformation_embeddings, epsilon=epsilon)
                        
                        action = transformations[action_idx]
                    
                        # Applies the action
                        transform_fn, loc1 = action

                        try:
                            transform_fn(kernel, *loc1)
                        except Exception as e:
                            self.logger.write(f"Error applying transformation {transform_fn.__name__} to kernel {kernel.name} with size {kernel_size_str}: {e}")
                            self.logger.write(kernel.text())
                            break
                        
                        new_kernel_text = kernel.text()
                        new_kernel_hash = kernel.program_hash()
                        
                        # Calculates the reward
                        try:
                            new_runtime = get_kernel_runtime(evaluator, kernel)
                        except Exception as e:
                            self.logger.write(f"Error evaluating kernel {kernel.name} with size {kernel_size_str} after transformation {transform_fn.__name__}: {e}")
                            self.logger.write(kernel.text())
                            break
                        
                        reward = self.reward_fn(runtime, new_runtime, base_ms, new_runtime == evaluator.timeout_ms)

                        new_kernel_embedding = self.agent.model.encode_kernel(new_kernel_text)
                        
                        if transform_fn != stop_transformation:
                            is_stop = step == train_steps - 2
                            try:
                                new_transformations = self.agent.get_actions(kernel, stop=is_stop)
                            except Exception as e:
                                self.logger.write(f"Error getting actions for kernel {kernel.name} with size {kernel_size_str} after transformation {transform_fn.__name__}: {e}")
                                self.logger.write(kernel.text())
                                break
                        else:
                            new_transformations = []
                            
                        # remove transformations that lead to previously visited kernels
                        new_transformations_filtered = []
                        for transform, loc in new_transformations:
                            new_kernel = copy.deepcopy(kernel)
                            transform(new_kernel, *loc)
                            if new_kernel.program_hash() in visited_kernel_hashes:
                                continue
                            new_transformations_filtered.append((transform, loc))
                        new_transformations = new_transformations_filtered
                        # add current kernel to visited kernels
                        visited_kernel_hashes.add(new_kernel_hash)
                        
                        new_transformation_texts = []
                        for transform, loc in new_transformations:
                            new_kernel = copy.deepcopy(kernel)
                            transform(new_kernel, *loc)
                            new_transformation_texts.append(new_kernel.text())
                        new_transformation_embeddings = self.agent.model.encode_transformations(new_transformation_texts)
                        
                        # Stores the transition
                        if self.config['fine_tune']:
                            self.agent.store_transition(kernel_text, new_kernel_text, new_transformation_texts, reward)
                        else:
                            self.agent.store_transition(kernel_embedding, new_kernel_embedding, new_transformation_embeddings, reward)

                        # append adjacenct transformations to the list of transformations
                        adjacent_tranformations.setdefault(kernel_hash, []).append(new_kernel_hash)

                        # Optimizes the agent
                        step_loss = self.agent.optimize(batch_size)
                        episode_losses.append(step_loss)
                        episode_rewards.append(reward)
                        episode_runtimes.append(new_runtime)
                        
                        self.logger.write(' '.join([
                            f"Episode {episode}",
                            f"Step {step}",
                            f"Transform {transform_fn.__name__}(program, {kelgen.args_as_str(loc1)})",
                            f"Reward: {reward:.3f}",
                            f"Runtime: {new_runtime:.3f}",
                            f"Relative: {ref_ms / new_runtime:.3f}",
                            f"Loss: {step_loss:.3f}",
                        ]))
                        
                        wandb.log({
                            "train_step/episode": episode,
                            "train_step/step": step,
                            "train_step/reward": reward,
                            "train_step/loss": step_loss,
                            "train_step/runtime": new_runtime,
                            "train_step/relative": ref_ms / new_runtime,
                        })
                        
                        # Updates the previous embeddings
                        runtime = new_runtime
                        kernel_embedding = new_kernel_embedding
                        transformations = new_transformations
                        transformation_embeddings = new_transformation_embeddings
                        kernel_text = new_kernel_text
                        kernel_hash = new_kernel_hash
                        
                        # Updates the epsilon value
                        epsilon = max(epsilon * epsilon_decay, min_epsilon)

                        if transform_fn == stop_transformation:
                            break

                        if new_runtime >= evaluator.timeout_ms:
                            break
                episode_end_time = time.time()
                self.elapsed_seconds += episode_end_time - episode_start_time
                
                if self.config.get("max_q_learning", False):
                    train_reward = np.sum(episode_rewards)
                else:
                    train_reward = np.max(episode_rewards)
                
                min_runtime = np.min(episode_runtimes)
                avg_runtime = np.mean(episode_runtimes)
                max_runtime = np.max(episode_runtimes)
                last_runtime = episode_runtimes[-1]
                max_relative = ref_ms / min_runtime
                avg_relative = ref_ms / avg_runtime
                min_relative = ref_ms / max_runtime
                last_relative = ref_ms / last_runtime
                
                current_min_runtime = min(self.running_min_runtime.get((base_kernel.name, kernel_size_str), min_runtime), min_runtime)
                current_max_relative = ref_ms / current_min_runtime
                self.running_min_runtime[(base_kernel.name, kernel_size_str)] = current_min_runtime
                
                wandb.log({
                    "episode": episode,
                    "elapsed_seconds": self.elapsed_seconds,
                    "train/total_reward": train_reward,
                    "train/min_runtime": min_runtime,
                    "train/avg_runtime": avg_runtime,
                    "train/max_runtime": max_runtime,
                    "train/last_runtime": last_runtime,
                    "train/max_relative": max_relative,
                    "train/avg_relative": avg_relative,
                    "train/min_relative": min_relative,
                    "train/last_relative": last_relative,
                    f"running_max_speedup/{base_kernel.name}_{kernel_size_str}": current_max_relative,
                    f"running_min_runtime/{base_kernel.name}_{kernel_size_str}": current_min_runtime,
                })
                
                # now that we have finished the episode, we can increment the episode counter before saving checkpoints and evaluating
                self.episode = episode + 1
                
                # save checkpoint
                if self.episode % self.checkpoint_freq == 0:
                    self.save()
                    
                    for kernel_hash, adjacent_kernel_hashes in adjacent_tranformations.items():
                        self.adjacent_kernels_file.write(f"{kernel_hash} {','.join(adjacent_kernel_hashes)}\n")
                    self.adjacent_kernels_file.flush()
                    adjacent_tranformations = {}

                if self.episode % self.config.get('eval_freq', 10) == 0:
                    all_eval_rewards = []
                    all_eval_runtimes = []
                    all_eval_relative = []
                    
                    for kernel_cls, kernel_size_str, base_kernel, base_ms, ref_ms, evaluator in eval_kernels:
                    
                        # evaluate the kernel
                        self.logger.write(f"Evaluating kernel {base_kernel.name} size {kernel_size_str} at episode {self.episode}")
                        visited_kernel_hashes = set()
                        eval_rewards = []
                        eval_runtimes = []
                        with tqdm(range(train_steps), leave=False, desc="Step") as epoch_steps:
                            kernel = copy.deepcopy(base_kernel)
                            runtime = get_kernel_runtime(evaluator, kernel)
                            eval_runtimes.append(runtime)
                            for step in epoch_steps:
                                kernel_text = kernel.text()
                                kernel_hash = kernel.program_hash()
                                kernel_embedding = self.agent.model.encode_kernel(kernel_text)
                                is_stop = step == train_steps - 2
                                try:
                                    transformations = self.agent.get_actions(kernel, stop=is_stop)
                                except Exception as e:
                                    self.logger.write(f"Error getting actions for kernel {kernel.name} with size {kernel_size_str}: {e}")
                                    self.logger.write(kernel.text())
                                    break
                                
                                # remove transformations that lead to previously visited kernels
                                new_transformations_filtered = []
                                for transform, loc in transformations:
                                    new_kernel = copy.deepcopy(kernel)
                                    transform(new_kernel, *loc)
                                    if new_kernel.program_hash() in visited_kernel_hashes:
                                        continue
                                    new_transformations_filtered.append((transform, loc))
                                # self.logger.write(f"Before filtering: {len(transformations)} After filtering: {len(new_transformations_filtered)}")
                                transformations = new_transformations_filtered
                                # add current kernel to visited kernels
                                visited_kernel_hashes.add(kernel_hash)
                                
                                transformations_texts = [
                                    transformation_to_str(kernel, transform, loc) for transform, loc in transformations
                                ]
                                transformation_embeddings = self.agent.model.encode_transformations(transformations_texts)
                                action_idx = self.agent.select_action(kernel_embedding, transformation_embeddings, epsilon=0.0)
                                transform_fn, loc = transformations[action_idx]
                                try:
                                    transform_fn(kernel, *loc)
                                except Exception as e:
                                    self.logger.write(f"Error applying transformation {transform_fn.__name__} to kernel {kernel.name} with size {kernel_size_str}: {e}")
                                    self.logger.write(kernel.text())
                                    break
                                try:
                                    new_runtime = get_kernel_runtime(evaluator, kernel)
                                except Exception as e:
                                    self.logger.write(f"Error evaluating kernel {kernel.name} with size {kernel_size_str} after transformation {transform_fn.__name__}: {e}")
                                    self.logger.write(kernel.text())
                                    break
                                reward = self.reward_fn(runtime, new_runtime, base_ms, new_runtime == evaluator.timeout_ms)
                                eval_rewards.append(reward)
                                eval_runtimes.append(new_runtime)
                                runtime = new_runtime
                                self.logger.write(' '.join([
                                    f"Eval",
                                    f"Episode {self.episode}",
                                    f"Step {step}",
                                    f"Transform {transform_fn.__name__}(program, {kelgen.args_as_str(loc)})",
                                    f"Runtime: {new_runtime:.3f}",
                                    f"Reward: {reward:.3f}",
                                    f"Relative: {ref_ms / new_runtime:.3f}",
                                ]))
                                wandb.log({
                                    "eval_step/episode": episode,
                                    "eval_step/step": step,
                                    "eval_step/reward": reward,
                                    "eval_step/runtime": new_runtime,
                                    "eval_step/relative": ref_ms / new_runtime,
                                })
                                if transform_fn == stop_transformation:
                                    break
                                if new_runtime >= evaluator.timeout_ms:
                                    break
                        
                        if self.config.get("max_q_learning", False):
                            eval_rewards = np.sum(eval_rewards)
                        else:
                            eval_rewards = np.max(eval_rewards)
                        
                        all_eval_rewards.append(eval_rewards)
                        all_eval_runtimes.append((np.min(eval_runtimes), np.mean(eval_runtimes), np.max(eval_runtimes), eval_runtimes[-1]))
                        all_eval_relative.append((ref_ms / np.max(eval_runtimes), ref_ms / np.mean(eval_runtimes), ref_ms / np.min(eval_runtimes), ref_ms / eval_runtimes[-1]))
                        
                    min_eval_runtimes = [runtimes[0] for runtimes in all_eval_runtimes]
                    avg_eval_runtimes = [runtimes[1] for runtimes in all_eval_runtimes]
                    max_eval_runtimes = [runtimes[2] for runtimes in all_eval_runtimes]
                    last_eval_runtimes = [runtimes[3] for runtimes in all_eval_runtimes]
                    min_eval_relative = [relative[0] for relative in all_eval_relative]
                    avg_eval_relative = [relative[1] for relative in all_eval_relative]
                    max_eval_relative = [relative[2] for relative in all_eval_relative]
                    last_eval_relative = [relative[3] for relative in all_eval_relative]
                    wandb.log({
                        "episode": episode + 1,
                        "eval/total_reward": np.mean(all_eval_rewards),
                        "eval/min_runtime": np.mean(min_eval_runtimes),
                        "eval/avg_runtime": np.mean(avg_eval_runtimes),
                        "eval/max_runtime": np.mean(max_eval_runtimes),
                        "eval/last_runtime": np.mean(last_eval_runtimes),
                        "eval/min_relative": np.mean(min_eval_relative),
                        "eval/avg_relative": np.mean(avg_eval_relative),
                        "eval/max_relative": np.mean(max_eval_relative),
                        "eval/last_relative": np.mean(last_eval_relative),
                    })
                    self.logger.write(f"Evaluated kernels at episode {self.episode} with total reward {np.mean(all_eval_rewards):.3f}")
                
        return res



