# LLM fine-tuning with speedup prediction

import argparse
import pathlib
import json
import torch
from transformers import DistilBertTokenizer, DistilBertModel
from transformers import AutoTokenizer, AutoModel
import transformers
from torch import nn
import wandb
import time
from tqdm import tqdm
from torch.utils.data import DataLoader, Dataset, random_split
import os
import pandas as pd
import random
from model import sentence_embeddings_bert, sentence_embeddings_llm
from kelgen.measurements import RuntimeCache
import numpy as np
from codediff import codediff
import shutil


class CodeSpeedupDataset(Dataset):
    def __init__(self, data):
        self.data = data

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        return self.data[idx]


# Define a simple regression layer
class RegressionModel(nn.Module):

    def __init__(self, input_size, output_size):
        super(RegressionModel, self).__init__()
        # Define the layers
        layers = []
        in_features = input_size

        # Add hidden layers
        hidden_sizes = [512, 256, 128, 64]
        # hidden_sizes = [4096, 2048, 1024, 512, 128, 64, 1]
        for hidden_size in hidden_sizes:
            layers.append(nn.Linear(in_features, hidden_size))
            layers.append(nn.ReLU())  # Activation function
            in_features = hidden_size

        # Output layer
        layers.append(nn.Linear(in_features, output_size))

        # Combine into a sequential model
        self.model = nn.Sequential(*layers)

    def forward(self, x):
        return self.model(x)
    
    
class MAPELoss(nn.Module):
    def __init__(self, epsilon=1e-8):
        super(MAPELoss, self).__init__()
        self.epsilon = epsilon

    def forward(self, output, target):
        absolute_percentage_errors = torch.abs((target - output) / (target + self.epsilon))
        mape = torch.mean(absolute_percentage_errors) * 100
        return mape


def speedup_normalization(speedup, eps=1e-4):
    "brings speedup from [0, +inf) to normalized range [-1, 1]"
    speedup = speedup.clamp(min=eps)
    # log_speedup = torch.log(speedup)
    # return log_speedup / 1 + log_speedup.abs()
    log_speedup = torch.log(speedup.clamp(min=0.01))
    return 2 / torch.pi * torch.atan(log_speedup)


def speedup_denormalization(normalized_speedup, eps=1e-4):
    "Brings normalized speedup from range [-1, 1] to [0, +inf)"
    normalized_speedup = normalized_speedup.clamp(min=-1 + eps, max=1 - eps)
    # log_speedup = normalized_speedup / 1 - normalized_speedup.abs()
    # return torch.exp(log_speedup)
    log_speedup = torch.atan(normalized_speedup * torch.pi / 2)
    return torch.exp(log_speedup)

def last_token_pool(last_hidden_states: torch.Tensor, attention_mask: torch.Tensor) -> torch.Tensor:
    left_padding = (attention_mask[:, -1].sum() == attention_mask.shape[0])
    if left_padding:
        return last_hidden_states[:, -1]
    else:
        sequence_lengths = attention_mask.sum(dim=1) - 1
        batch_size = last_hidden_states.shape[0]
        return last_hidden_states[torch.arange(batch_size, device=last_hidden_states.device), sequence_lengths]

class SpeedupLoss(nn.Module):
    """
    Expects output and target within the range [0, +inf).
    Low penalty for correct direction prediction (speedup/slowdown).
    Moderate penalty for 'far from 1.0' prediction or target when one of them is around 1.0.
    Large penality for wrong direction prediction.
    Parameter 'alpha' controls sharpness of change around zero.
    """
    def __init__(self, alpha=20):
        super().__init__()
        self.alpha = alpha

    def forward(self, y, t):
        # we multiply by 1000 to avoid numerical instability and vanishing gradients
        signed_loss = 1000 * (0.5 + torch.sigmoid(-self.alpha*y*t)) * torch.square(y - t)
        return signed_loss.mean()


def find_checkpoints(save_dir_path):
    checkpoint_indices = []
    for checkpoint in save_dir_path.glob("llm_finetuned_model_*"):
        checkpoint_counter = int(checkpoint.stem.split("_")[-1])
        checkpoint_indices.append(checkpoint_counter)
    checkpoint_indices.sort()
    return checkpoint_indices


def remove_old_checkpoints(save_dir_path, checkpoint_freq):
    checkpoint_indices = find_checkpoints(save_dir_path)
    for checkpoint_counter in checkpoint_indices[:-2]:
        llm_dir = save_dir_path / f"llm_finetuned_model_{checkpoint_counter}"
        shutil.rmtree(llm_dir)
        regression_path = save_dir_path / f"regression_model_{checkpoint_counter}.pt"
        os.remove(regression_path)


def save_checkpoint(model, tokenizer, regression_model, save_dir_path, save_counter):
    finetuned_path = save_dir_path / f"llm_finetuned_model_{save_counter}"
    model.save_pretrained(finetuned_path)
    tokenizer.save_pretrained(finetuned_path)
    regression_path = save_dir_path / f"regression_model_{save_counter}.pt"
    torch.save(regression_model.state_dict(), regression_path)


def load_dataset(path_str: str):
    path = pathlib.Path(path_str)
    assert path.exists(), f"Code runtime dataset {path} does not exist"
    return {
        code_hash: (measurement.code, measurement.mean) 
        for code_hash, measurement 
        in RuntimeCache(path)
        if measurement.valid()
    }


def find_connected_components(edges):
    graph = {}
    for u, v in edges:
        graph.setdefault(u, []).append(v)
        graph.setdefault(v, []).append(u)

    visited = set()
    components = []

    def dfs(node, component):
        visited.add(node)
        component.add(node)
        for neighbor in graph.get(node, []):
            if neighbor not in visited:
                dfs(neighbor, component)

    for node in tqdm(graph, desc="Building connected components"):
        if node not in visited:
            component = set()
            dfs(node, component)
            components.append(component)

    return components


def adjacent_kernels_to_adjacency_list(adjacent_kernels):
    adjacency_list = []
    for src, dsts in adjacent_kernels.items():
        for dst in dsts:
            adjacency_list.append((src, dst))
    adjacency_list = list(set(adjacency_list))  # make each occurence unique
    return adjacency_list


def preprocess_dataset_no_adjacent(dataset, adjacent_kernels, training_inputs, repetitions):
    # group kernels into connected components to make sure they correspond to a the program
    adjacency_sets = find_connected_components(adjacent_kernels_to_adjacency_list(adjacent_kernels))
    processed_data = []
    for program_variants in tqdm(adjacency_sets, desc=f"Pairing programs for speedup prediction"):
        program_dataset = [dataset[code_hash] for code_hash in program_variants if code_hash in dataset]
        for rep in range(repetitions):
            random.shuffle(program_dataset)
            # now form pairs out of shuffled dataset
            for program, next_program in zip(program_dataset[:len(program_dataset)//2], program_dataset[len(program_dataset)//2:]):
                code, runtime = program
                next_code, next_runtime = next_program
                if training_inputs == "diffs":
                    diff = codediff(code, next_code)
                else:
                    diff = ''
                speedup = runtime / next_runtime
                processed_data.append([diff, code, next_code, speedup])
    return processed_data
    

def preprocess_dataset(dataset, adjacent_kernels, training_inputs):
    processed_data = []
    skipped_src_hashes = 0
    skipped_dst_hashes = 0
    for code_hash, next_code_hashes in tqdm(adjacent_kernels.items(), desc=f"Pairing programs for speedup prediction"):
        if code_hash not in dataset:
            skipped_src_hashes += 1
            continue
        code, runtime = dataset[code_hash]
        for next_code_hash in next_code_hashes:
            if next_code_hash not in dataset:
                skipped_dst_hashes += 1
                continue
            next_code, next_runtime = dataset[next_code_hash]
            if training_inputs == "diffs":
                # this is compute intensive, so keep it optional
                diff = codediff(code, next_code)
            else:
                diff = ''
            speedup = runtime / next_runtime
            processed_data.append([diff, code, next_code, speedup])
    return processed_data


def main():    
    parser = argparse.ArgumentParser(description='LLM fine-tuning with speedup prediction')
    parser.add_argument('config', type=str, help='Path to the configuration file')
    
    args = parser.parse_args()
    config_path = pathlib.Path(args.config)
    assert config_path.exists(), f"Configuration file {config_path} does not exist"

    config = json.load(open(config_path, 'r'))
    model_name = config.get("model", "deepseek-ai/DeepSeek-R1-Distill-Qwen-1.5B")
    train_code_runtime_dataset_name = config.get("train_dataset", "cache-layernorm-4096x4096.db")
    test_code_runtime_dataset_name = config.get("test_dataset", "cache-layernorm-4096x4096.db")
    
    save_dir = config.get("save_dir", "deepseek-qwen-1.5B-finetuned")

    training_criterion = {
        "mse": nn.MSELoss,
        "mape": MAPELoss,
        "speedup": SpeedupLoss,
    }[config.get("loss", "mape")]()

    training_inputs = config.get('training_inputs', 'pairs')
    assert training_inputs in ["pairs", "diffs"], f"Invalid training inputs: {training_inputs}"

    num_epochs = config.get("num_epochs", 1000)    
    checkoint_freq = config.get("checkpoint_freq", 10)
    
    seed = config.get("seed", 42)
    random.seed(seed)
    torch.manual_seed(seed)
    np.random.seed(seed)

    model_type = config.get("model_type", "llm")

    sentence_embedding_fn = {
        'llm': lambda h, m: sentence_embeddings_llm(h, m, weighted=False),
        'bert': lambda h, m: sentence_embeddings_bert(h),
        'last': lambda h, m: last_token_pool(h, m),
    }[model_type]
    
    save_dir_path = pathlib.Path(save_dir)
    save_dir_path.mkdir(parents=True, exist_ok=True)

    assert torch.cuda.is_available(), "CUDA is not available"
    device = torch.device("cuda")
    dtype = torch.float32

    device_map = {
        "llm": "auto",
        "bert": "cuda",
        'last': "auto"
    }[model_type]

    model = AutoModel.from_pretrained(model_name, torch_dtype=dtype, device_map=device_map, trust_remote_code=True)
    tokenizer = AutoTokenizer.from_pretrained(model_name)
    tokenizer.truncation_side = "left"

    if training_inputs == "pairs":
        regression_input_size = model.config.hidden_size * 2
    elif training_inputs == "diffs":
        regression_input_size = model.config.hidden_size
    else:
        raise ValueError(f"Invalid training inputs: {training_inputs}")
    regression_model = RegressionModel(input_size=regression_input_size, output_size=1)
    regression_model.to(device)

    criterion_mse = nn.MSELoss()
    criterion_mape = MAPELoss()
    criterion_speedup = SpeedupLoss()
    optimizer = torch.optim.Adam([
        {"params": model.parameters(), "lr": config.get("model_lr", 1e-5)},
        {"params": regression_model.parameters(), "lr": config.get("regression_lr", 1e-3)}
    ], lr=config.get("model_lr", 1e-5))

    grad_scaler = torch.amp.GradScaler("cuda")

    adjacent_kernels = {}
    with open(config.get("adjacent_kernels", "adjacent_kernels.log"), 'r') as f:
        for line in f:
            # line has the format: "hash hash,hash,hash"
            src_hash, dst_hashes = line.rstrip('\n').split(" ")
            dst_hashes = dst_hashes.split(",")
            assert dst_hashes
            adjacent_kernels.setdefault(src_hash, set()).update(dst_hashes)

    train_code_runtime_dataset = load_dataset(train_code_runtime_dataset_name)
    test_code_runtime_dataset = load_dataset(test_code_runtime_dataset_name)
    
    if config.get("allow_non_adjacent", False):
        data_reps = config.get("data_reps", 1)
        train_processed_data = preprocess_dataset_no_adjacent(train_code_runtime_dataset, adjacent_kernels, training_inputs, data_reps)
        test_processed_data = preprocess_dataset_no_adjacent(test_code_runtime_dataset, adjacent_kernels, training_inputs, repetitions=1)
    else:
        train_processed_data = preprocess_dataset(train_code_runtime_dataset, adjacent_kernels, training_inputs)
        test_processed_data = preprocess_dataset(test_code_runtime_dataset, adjacent_kernels, training_inputs)

    train_dataset = CodeSpeedupDataset(train_processed_data)
    tqdm.write(f"Train speedup dataset size: {len(train_dataset)}")
    test_dataset = CodeSpeedupDataset(test_processed_data)
    tqdm.write(f"Test speedup dataset size: {len(test_dataset)}")

    train_batch = config.get("batch_size", 1)
    train_dataloader = DataLoader(train_dataset, batch_size=train_batch, shuffle=True)
    test_dataloader = DataLoader(test_dataset, batch_size=train_batch)

    wandb.init(
        entity="mycoolteam",
        project="llm-finetuning",
        config=config,
        # mode='disabled',
    )

    save_counter = 0
    for epoch in tqdm(range(num_epochs), desc="Epoch"):
        wandb.log({"epoch": epoch})
        model.train()
        regression_model.train()
        epoch_train_loss_mse = 0
        epoch_train_loss_mape = 0
        epoch_train_loss_speedup = 0
        for batch in tqdm(train_dataloader, desc=f"Sample", unit="batch", leave=False):
            with torch.autocast(device_type='cuda', dtype=torch.float16):
                diff, code1, code2, speedup = batch
                targets = speedup.unsqueeze(1).to(device=device, dtype=dtype)
                
                if training_inputs == "pairs":
                    tokens1 = tokenizer(code1, return_tensors="pt", padding=True, truncation=True, max_length=model.config.max_position_embeddings)
                    tokens1 = {key: val.to(device) for key, val in tokens1.items()}
                    
                    tokens2 = tokenizer(code2, return_tensors="pt", padding=True, truncation=True, max_length=model.config.max_position_embeddings)
                    tokens2 = {key: val.to(device) for key, val in tokens2.items()}
                
                    outputs1 = model(**tokens1)
                    outputs2 = model(**tokens2)
                    emb1 = sentence_embedding_fn(outputs1.last_hidden_state, tokens1['attention_mask'])
                    emb2 = sentence_embedding_fn(outputs2.last_hidden_state, tokens2['attention_mask'])

                    emb = torch.cat([emb1, emb2], dim=1)
                else:
                    tokens = tokenizer(diff, return_tensors="pt", padding=True, truncation=True, max_length=model.config.max_position_embeddings)
                    tokens = {key: val.to(device) for key, val in tokens.items()}
                    outputs = model(**tokens)
                    emb = sentence_embedding_fn(outputs.last_hidden_state, tokens['attention_mask'])

                ntargets = speedup_normalization(targets)
                if isinstance(training_criterion, SpeedupLoss):
                    noutput = regression_model(emb)
                    loss = training_criterion(noutput, ntargets)
                    output = speedup_denormalization(noutput)
                else:
                    output = regression_model(emb)
                    loss = training_criterion(output, targets)
                    noutput = speedup_normalization(output)
                    
                loss_mse = criterion_mse(output, targets)
                loss_mape = criterion_mape(output, targets)
                loss_speedup = criterion_speedup(noutput, ntargets)

            # Backward pass
            
            # without grad scaler
            # loss.backward()
            # optimizer.step()
            
            # with grad scaler
            grad_scaler.scale(loss).backward()
            grad_scaler.step(optimizer)
            grad_scaler.update()
            
            optimizer.zero_grad(set_to_none=True)
            epoch_train_loss_mse += loss_mse.item() / len(train_dataloader)
            epoch_train_loss_mape += loss_mape.item() / len(train_dataloader)
            epoch_train_loss_speedup += loss_speedup.item() / len(train_dataloader)
            
            wandb.log({
                "batch/mse_loss": loss_mse.item(),
                "batch/mape_loss": loss_mape.item(),
                "batch/speedup_loss": loss_speedup.item()
            })
            tqdm.write(f"Batch MSELoss: {loss_mse.item():.3f} MAPELoss: {loss_mape.item():.3f} SpeedupLoss: {loss_speedup.item():.3f}")

            if (save_counter + 1) % checkoint_freq == 0:
                save_checkpoint(model, tokenizer, regression_model, save_dir_path, save_counter + 1)
                remove_old_checkpoints(save_dir_path, checkoint_freq)
                
            save_counter += 1
            
        wandb.log({
            "epoch/train_mse_loss": epoch_train_loss_mse,
            "epoch/train_mape_loss": epoch_train_loss_mape,
            "epoch/train_speedup_loss": epoch_train_loss_speedup,
        })
        
        tqdm.write(
            f"Epoch {epoch + 1} Train MSELoss {epoch_train_loss_mse:.3f} MAPELoss {epoch_train_loss_mape:.3f} SpeedupLoss {epoch_train_loss_speedup:.3f}"
        )

        # validation loss
        model.eval()
        regression_model.eval()
        epoch_test_loss_mse = 0
        epoch_test_loss_mape = 0
        epoch_test_loss_speedup = 0
        with torch.no_grad():
            for batch in test_dataloader:
                diffs, code1, code2, speedup = batch
                targets = speedup.unsqueeze(1).to(device=device, dtype=dtype)
                
                if training_inputs == "pairs":
                    tokens1 = tokenizer(code1, return_tensors="pt", padding=True, truncation=True, max_length=model.config.max_position_embeddings)
                    tokens1 = {key: val.to(device) for key, val in tokens1.items()}
                    
                    tokens2 = tokenizer(code2, return_tensors="pt", padding=True, truncation=True, max_length=model.config.max_position_embeddings)
                    tokens2 = {key: val.to(device) for key, val in tokens2.items()}
                
                    outputs1 = model(**tokens1)
                    outputs2 = model(**tokens2)
                    emb1 = sentence_embedding_fn(outputs1.last_hidden_state, tokens1['attention_mask'])
                    emb2 = sentence_embedding_fn(outputs2.last_hidden_state, tokens2['attention_mask'])
                    
                    emb = torch.cat([emb1, emb2], dim=1)
                else:
                    tokens = tokenizer(diffs, return_tensors="pt", padding=True, truncation=True, max_length=model.config.max_position_embeddings)
                    tokens = {key: val.to(device) for key, val in tokens.items()}
                    outputs = model(**tokens)
                    emb = sentence_embedding_fn(outputs.last_hidden_state, tokens['attention_mask'])
                
                ntargets = speedup_normalization(targets)
                if isinstance(training_criterion, SpeedupLoss):
                    noutput = regression_model(emb)
                    output = speedup_denormalization(noutput)
                else:
                    output = regression_model(emb)
                    noutput = speedup_normalization(output)

                loss_mse = criterion_mse(output, targets)
                loss_mape = criterion_mape(output, targets)
                loss_speedup = criterion_speedup(noutput, ntargets)
                epoch_test_loss_mape += loss_mape.item() / len(test_dataloader)
                epoch_test_loss_mse += loss_mse.item() / len(test_dataloader)
                epoch_test_loss_speedup += loss_speedup.item() / len(test_dataloader)

        # Calculate epoch time
        # Log epoch loss to wandb
        wandb.log({
            "epoch/test_mse_loss": epoch_test_loss_mse,
            "epoch/test_mape_loss": epoch_test_loss_mape,
            "epoch/test_speedup_loss": epoch_test_loss_speedup,
        })

        tqdm.write(
            f"Epoch {epoch + 1} Test MSELoss {epoch_test_loss_mse:.3f} MAPELoss {epoch_test_loss_mape:.3f} SpeedupLoss {epoch_test_loss_speedup:.3f}"
        )

    # Finish wandb run
    wandb.finish()


if __name__ == '__main__':
    main()