
import argparse
import pathlib
import re
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from cycler import cycler

def weighted_ema(data, alpha=0.9):

    ema = np.zeros_like(data.values, dtype=float)
    ema[0] = data.iloc[0]

    for i in range(1, len(data)):
        ema[i] = alpha * ema[i - 1] + (1 - alpha) * data.iloc[i]

    return pd.Series(ema, index=data.index)

parser = argparse.ArgumentParser(description='parse log from deep_q_learning')
parser.add_argument('log', type=str, nargs='+', help='Path to the log file')

args = parser.parse_args()
log_files = [pathlib.Path(log_file) for log_file in args.log]
for log_file in log_files:
    assert log_file.exists(), f"Log file {log_file} does not exist"


train_data_dict = {}  # log file name -> list of training data
eval_data_dict = {}  # log file name -> list of evaluation data

for log_file in log_files:    
    log_file_name = log_file.parent.name
    train_data_dict[log_file_name] = []
    eval_data_dict[log_file_name] = []
    
    train_data_list = train_data_dict[log_file_name]
    eval_data_list = eval_data_dict[log_file_name]
    
    with open(log_file) as f:
        for l in f.readlines():
            # parse training step
            # Episode 20 Step 10 Transform unroll_scope(program, 'src1#0',) Reward: -0.049 Runtime: 93.897 Loss: inf
            match = re.match(r"^Episode (\d+) Step (\d+) Transform (.*) Reward: (.*) Runtime: (.+) Loss: (.*)", l)
            if match:
                episode, step, transform, reward, runtime, loss = match.groups()
                # print(f"Episode {episode} Step {step} Transform {transform} Reward: {reward} Runtime: {runtime} Loss: {loss}")
                train_data_list.append({'episode': int(episode), 'step': step, 'transform': transform, 'reward': reward, 'runtime': float(runtime), 'loss': loss})
                        
            # parse evaluation step
            # Eval Episode 30 Step 3 Transform create_temporary(program, 'src',) Runtime: 82.441 Reward: 4.105
            match = re.match(r"^Eval Episode (\d+) Step (\d+) Transform (.*) Runtime: (.*) Reward: (.+)", l)
            if match:
                episode, step, transform, runtime, reward = match.groups()
                # print(f"Eval Episode {episode} Step {step} Transform {transform} Runtime: {runtime} Reward: {reward}")
                eval_data_list.append({'episode': int(episode), 'step': step, 'transform': transform, 'reward': reward, 'runtime': float(runtime)})
            
    assert train_data_list, f"No training data found in log file {log_file}"
    assert eval_data_list, f"No evaluation data found in log file {log_file}"

# we want to build a single dataframe with all the data
# we want to add a column with the log file name, column with the type of data (train/eval)

ref_runtime = 3.93

df = pd.DataFrame()
for log_file in log_files:
    train_data = pd.DataFrame(train_data_dict[log_file.parent.name])
    eval_data = pd.DataFrame(eval_data_dict[log_file.parent.name])
    
    # here we post-process the data, e.g. calculate running minimum
    train_data_grouped = train_data.groupby('episode')
    train_data_episode = pd.DataFrame()
    train_data_episode['episode'] = train_data_grouped['episode'].transform('first')
    train_data_episode['min_runtime'] = train_data_grouped['runtime'].transform('min')
    train_data_episode['last_runtime'] = train_data_grouped['runtime'].transform('last')
    train_data_episode['min_runtime_acc'] = train_data_episode['min_runtime'].cummin()
    train_data_episode = train_data_episode.drop_duplicates(subset=['episode'])
    train_data_episode = train_data_episode.reset_index(drop=True)
    train_data_episode['max_speedup'] = ref_runtime / train_data_episode['min_runtime']
    train_data_episode['max_speedup_smooth'] = weighted_ema(train_data_episode['max_speedup'])
    train_data_episode['last_speedup'] = ref_runtime / train_data_episode['last_runtime']
    train_data_episode['max_speedup_acc'] = ref_runtime / train_data_episode['min_runtime_acc']
    
    eval_data_grouped = eval_data.groupby('episode')
    eval_data_episode = pd.DataFrame()
    eval_data_episode['episode'] = eval_data_grouped['episode'].transform('first')
    eval_data_episode['min_runtime'] = eval_data_grouped['runtime'].transform('min')
    eval_data_episode['last_runtime'] = eval_data_grouped['runtime'].transform('last')
    eval_data_episode['min_runtime_acc'] = eval_data_episode['min_runtime'].cummin()
    eval_data_episode = eval_data_episode.drop_duplicates(subset=['episode'])
    eval_data_episode = eval_data_episode.reset_index(drop=True)
    eval_data_episode['max_speedup'] = ref_runtime / eval_data_episode['min_runtime']
    eval_data_episode['max_speedup_smooth'] = weighted_ema(eval_data_episode['max_speedup'])
    eval_data_episode['last_speedup'] = ref_runtime / eval_data_episode['last_runtime']
    eval_data_episode['max_speedup_acc'] = ref_runtime / eval_data_episode['min_runtime_acc']
    
    train_data_episode['type'] = 'train'
    eval_data_episode['type'] = 'eval'
    data_episode = pd.concat([train_data_episode, eval_data_episode])
    data_episode['name'] = log_file.parent.name
    df = pd.concat([df, data_episode])

print(df.head())

# find running minimum runtime per episode

# create plot
fig, axes = plt.subplots(figsize=(15, 8), nrows=2, ncols=2, sharex=True)

# ax.set_prop_cycle(cycler('linestyle', ['-','--',':','-.']) * cycler('color', list('rbgykcm')))
# ax.set_prop_cycle(cycler('color', list('rbgykcm')) * cycler('linestyle', ['-', '--']))
# for log_file in log_files:
#     cdf = df[df['name'] == log_file.parent.name]
#     # ax.plot(cdf[cdf['type'] == 'train']['episode'], cdf[cdf['type'] == 'train']['max_speedup_acc'], label=f'Train {log_file.parent.name}')
#     # ax.plot(cdf[cdf['type'] == 'eval']['episode'], cdf[cdf['type'] == 'eval']['max_speedup_acc'], label=f'Eval {log_file.parent.name}')
#     # ax.plot(cdf[cdf['type'] == 'train']['episode'], cdf[cdf['type'] == 'train']['max_speedup_smooth'], label=f'Train {log_file.parent.name}')
#     ax.plot(cdf[cdf['type'] == 'eval']['episode'], cdf[cdf['type'] == 'eval']['max_speedup_smooth'], label=f'Eval {log_file.parent.name}')

axes[0][0].set_prop_cycle(cycler('color', list('rbgykcm')) * cycler('linestyle', ['-']))
axes[0][0].set_title('Training Running Maximum')
axes[0][0].set_ylabel('Fraction of Reference Performance')
axes[0][0].set_xlabel('Episode')
for log_file in log_files:
    cdf = df[df['name'] == log_file.parent.name]
    axes[0][0].plot(cdf[cdf['type'] == 'train']['episode'], cdf[cdf['type'] == 'train']['max_speedup_acc'], label=f'Train {log_file.parent.name}')
axes[0][0].legend()


axes[0][1].set_prop_cycle(cycler('color', list('rbgykcm')) * cycler('linestyle', ['-']))
axes[0][1].set_title('Evaluation Running Maximum')
axes[0][1].set_ylabel('Fraction of Reference Performance')
axes[0][1].set_xlabel('Episode')
for log_file in log_files:
    cdf = df[df['name'] == log_file.parent.name]
    axes[0][1].plot(cdf[cdf['type'] == 'eval']['episode'], cdf[cdf['type'] == 'eval']['max_speedup_acc'], label=f'Eval {log_file.parent.name}')
    
axes[1][0].set_prop_cycle(cycler('color', list('rbgykcm')) * cycler('linestyle', ['-']))
axes[1][0].set_title('Training Maximum (Smoothed)')
axes[1][0].set_ylabel('Fraction of Reference Performance')
axes[1][0].set_xlabel('Episode')
for log_file in log_files:
    cdf = df[df['name'] == log_file.parent.name]
    axes[1][0].plot(cdf[cdf['type'] == 'train']['episode'], cdf[cdf['type'] == 'train']['max_speedup_smooth'], label=f'Train {log_file.parent.name}')
    
axes[1][1].set_prop_cycle(cycler('color', list('rbgykcm')) * cycler('linestyle', ['-']))
axes[1][1].set_title('Evaluation Maximum (Smoothed)')
axes[1][1].set_ylabel('Fraction of Reference Performance')
axes[1][1].set_xlabel('Episode')
for log_file in log_files:
    cdf = df[df['name'] == log_file.parent.name]
    axes[1][1].plot(cdf[cdf['type'] == 'eval']['episode'], cdf[cdf['type'] == 'eval']['max_speedup_smooth'], label=f'Eval {log_file.parent.name}')

# ax.plot(episodes, min_runtime, label='Training Runtime (episode min)')
# ax.plot(episodes_eval, min_eval_runtime, label='Evaluation Runtime (episode min)')
# ax.plot(episodes, last_runtime, label='Training Runtime (episode last)')
# ax.plot(episodes_eval, last_eval_runtime, label='Evaluation Runtime (episode last)')
# ax.plot(episodes, last_runtime_acc, label='Training Runtime (running min)')
# ax.plot(episodes_eval, last_eval_runtime, label='Evaluation Runtime')
# ax.plot(episodes_eval, last_eval_runtime_acc, label='Evaluation Runtime (running min)')
# ax.set_xlabel('Episode')
# ax.set_ylabel('Fraction of Reference Performance')
# plt.legend(loc='upper center', bbox_to_anchor=(0.5, 2.2), ncol=3, fontsize='small')
plt.tight_layout()
# save plot
plt.savefig('logs.pdf')