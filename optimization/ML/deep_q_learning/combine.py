import sqlite3
import sys
import os
import tqdm

if len(sys.argv) < 3:
    print("Usage: python combine_databases.py <input_db1> <input_db2> ... <output_db>")
else:
    # Initialize output DB
    output_file = sys.argv[-1]
    # check that output file does not exist
    if os.path.exists(output_file):
        # prevent appending to existing file
        print(f"Output file {output_file} already exists. Please remove it before running this script.")
        sys.exit(1)
    
    output_db = sqlite3.connect(output_file, timeout=300)
    output_db.execute('PRAGMA journal_mode=WAL')
    output_db.execute('''
        CREATE TABLE IF NOT EXISTS cache (
            hash TEXT PRIMARY KEY,
            code TEXT,
            mean REAL,
            std REAL,
            timeout BOOLEAN
        )
    ''')
    output_db.commit()

    # Iterate over input files
    total_error_count = 0
    for input_file in tqdm.tqdm(sys.argv[1:-1]):
        input_db = sqlite3.connect(f"file:{input_file}?mode=ro", uri=True)
        try:
            for row in input_db.execute(f"SELECT * FROM cache"):
                placeholders = ', '.join(['?'] * len(row))
                try:
                    output_db.execute(f"INSERT INTO cache VALUES ({placeholders})", row)
                except Exception as e:
                    #print(f"Error inserting row from {input_file}: {e}")
                    #print(f"Row: {row}")
                    total_error_count += 1
        except Exception as e:
            #print(f"Error on file {input_file}: {e}")
            total_error_count += 1
        input_db.close()

        # Commit the changes
        output_db.commit()

    # Print the final statistics
    print(f"Errors: {total_error_count}")
    print(f"Succefully inserted rows: {output_db.execute('SELECT COUNT(*) FROM cache').fetchone()[0]}")
    output_db.close()