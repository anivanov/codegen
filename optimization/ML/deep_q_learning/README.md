## RL for Micro-kernel Optimization

#### Setup container on Clariden

Follow the instructions below to run reinforcement learning micro-kernel optimization on Clariden.

Firstly, configure Podman's storage by executing command below. Alternatively, follow the __preliminary step__ on [this page](https://confluence.cscs.ch/display/KB/Building+container+images+on+Alps). 

```console
> bash init_container_storage.sh
```

After container storage is initialized, build container using the __Dockerfile__.

```console
> sbatch build_container.sh
```

After running the command above, a sqsh file named __codegen.sqsh__ should be created in the directory.

Initialize __codegen.toml__ in `~/.edf` with correct paths to your working directory by running __make_edf.sh__ script.
```console
> bash make_edf.sh
```

Set your wandb token, accessible from [this page](https://wandb.ai/settings).
```console
> export WANDB_API_KEY="123456ABC"
```

Alternatively, disable wandb logging.
```console
> export WANDB_MODE=disabled
```

#### RL Training on Clariden

Start training:
```console
> sbatch run_rl.sh configs/distilbert.json
```

Start training in interactive mode:
```console
WANDB_MODE=disabled srun --environment=codegen -A a-g34 --mem=256G --reservation=interactive_jobs -t 8:00:00 --pty python main.py -c configs/distilbert.json
```

### LLM finetuning on Clariden

Start training:
```console
sbatch run_finetuning.sh configs/finetuning/gte-1.5-speedup.json
```

Start training in interactive mode:
```console
WANDB_MODE=disabled srun --environment=codegen -A a-g34 --mem=256G --reservation=interactive_jobs -t 8:00:00 --pty python llm_finetuning.py configs/finetuning/gte-1.5-speedup.json
```

### Interactive session

For interactive session, run
```console
srun --environment=codegen -A a-g34 --mem=256G --reservation=interactive_jobs -t 8:00:00 --pty bash
```

