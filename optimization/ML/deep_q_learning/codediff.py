import difflib
import re


def tokenize_code(code):
    """
    Simple tokenizer that preserves numbers and words.
    """
    
    token_pattern = re.compile(r"([a-zA-Z_][a-zA-Z_0-9]*|\d+\.\d+|\d+)")

    tokens = []

    last_end = 0
    for match in token_pattern.finditer(code):
        start, end = match.start(), match.end()
        
        if last_end < start:
            tokens.extend(code[last_end:start])
        
        tokens.append(code[start:end])
        
        last_end = end

    if last_end < len(code):
        tokens.extend(code[last_end:])
        
    return tokens


def get_token_diff(a, b):
    matcher = difflib.SequenceMatcher(None, a, b, autojunk=False)

    result = []

    for opcode, a0, a1, b0, b1 in matcher.get_opcodes():
        if opcode == "equal":
            result += a[a0:a1]
        elif opcode == "replace":
            result += ['#'] + a[a0:a1] + ['$'] + b[b0:b1] + ['@']
        elif opcode == "delete":
            result += ['#'] + a[a0:a1] + ['$', '@'] 
        elif opcode == "insert":
            result += ['#', '$'] + b[b0:b1] + ['@']

    return result


def codediff(a, b):
    ta = tokenize_code(a)
    tb = tokenize_code(b)
    tokenized_diff = get_token_diff(ta, tb)
    return ''.join(tokenized_diff)  # untokens the diff
