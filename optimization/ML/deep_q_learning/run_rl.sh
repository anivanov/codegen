#!/bin/bash -l
#SBATCH --job-name "RL microkernel optimization"
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH -A a-g34
#SBATCH --mem=256G
#SBATCH --time=8:00:00
#SBATCH --partition=normal

if [[ $# -eq 0 ]]; then
  echo "Usage: sbatch run_rl.sh <config>"
  exit 1
fi

config_path="$1"

if [[ ! -f "$config_path" ]]; then
  echo "Error: File '$config_path' does not exist or is not a regular file."
  exit 1
fi

srun --environment=codegen python3 main.py -c "$config_path"
