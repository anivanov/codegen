import numpy as np
from numpy.random import Generator
from collections import deque, namedtuple
from typing import List, Optional, Union, Tuple, Dict, Any
import warnings

Transition = namedtuple('Transition', ('state', 'next_state', 'next_actions', 'reward'))
warnings.filterwarnings("ignore")


class BasicReplayBuffer(object):
	"""
	The replay buffer that stores all the transitions for training agents with
	experience replay
	"""
	def __init__(self, capacity: int, rng: Optional[Generator] = None):
		"""
		:param capacity: The maximum number of transitions the buffer can store
		:param rng: A numpy random number generator object
		"""
		self.memory = deque([], maxlen=capacity)
		self.rng = rng

	def push(self, transition: Transition):
		"""
		Stores the given transition into memory
		"""
		self.memory.append(transition)

	def has_enough_samples(self, batch_size: int) -> bool:
		"""
		Returns True if the number of transitions in memory is able fill at least one batch
		of training samples.
		"""
		return len(self.memory) >= batch_size

	def sample(self, batch_size: int) -> List[Transition]:
		"""
		Returns a random list of Transitions sampled from the buffer
		"""
		if self.rng:
			return list(self.rng.choice(self.memory, batch_size))
		else:
			return list(np.random.choice(self.memory, batch_size))

	def __len__(self) -> int:
		return len(self.memory)


class PrioritizedReplayBuffer(object):
	"""
	A ReplayBuffer implementation that leverages priority to sample transitions
	Code from:
	https://github.com/the-computer-scientist/OpenAIGym/blob/master/PrioritizedExperienceReplayInOpenAIGym.ipynb
	https://colab.research.google.com/github/davidrpugh/stochastic-expatriate-descent/blob/2020-04-14-prioritized-experience-replay/
	https://github.com/higgsfield/RL-Adventure/blob/master/4.prioritized%20dqn.ipynb
	"""
	def __init__(self, capacity: int, alpha: float = 1.0, rng: Optional[Generator] = None):
		"""
		:param capacity: The maximum number of transitions the buffer can store
		:param alpha: A float between 0.0 and 1.0 that is used to scale the priorities in order
		to convert them into sampling probabilities. If uniform sampling is used (i.e. no PER),
		the value of alpha will be 0.
		:param rng: A numpy random number generator object
		"""
		self.memory = deque([], maxlen=capacity)
		self.priorities = deque([], maxlen=capacity)
		self.alpha = alpha
		self.rng = rng
  
	def save(self) -> Dict[str, Any]:
		return {
			"memory": self.memory,
			"priorities": self.priorities
		}
  
	def load(self, data: Dict[str, Any]):
		self.memory = data["memory"]
		self.priorities = data["priorities"]

	def push(self, transition: Transition):
		"""
		Stores the given transition into memory, its priority will be the maximum priority
		"""
		self.memory.append(transition)
		self.priorities.append(max(self.priorities, default=1))

	def _get_probabilities(self) -> np.ndarray:
		"""
		Converts priorities to sampling probabilities with the given priority scale
		"""
		if self.alpha == 0.0:
			# If uniform sampling is used
			return np.ones(len(self.priorities)) / len(self.priorities)
		scaled_priorities = np.array(self.priorities) ** self.alpha
		return scaled_priorities / np.sum(scaled_priorities)

	def _get_importance(self, probabilities: np.ndarray, beta: float) -> np.ndarray:
		"""
		Calculates the normalized importance weights of the samples from the given probabilities.
		The weights can be calculated with the formula:
		w_i = ((1/N) * (1 / P))^(beta)
		"""
		importance = (len(self.memory) * probabilities) ** (-beta)
		return importance / np.max(importance)

	def sample(self, batch_size: int, beta: float) -> Tuple[List[Transition], np.ndarray, np.ndarray]:
		"""
		Returns a random list of Transitions sampled from the buffer, a tuple that contains an array of
		indices of the samples as well as the importance weights of the samples
		"""
		# if self.alpha == 0.0:
		# 	# If uniform sampling is used
		# 	samples = self.rng.choice(self.memory, batch_size) if self.rng else np.random.choice(self.memory, batch_size)
		# 	return samples, np.array([]), np.array([])

		sample_probabilities = self._get_probabilities()
		if self.rng:
			sample_indices = self.rng.choice(len(self.priorities), batch_size, p=sample_probabilities)
		else:
			sample_indices = np.random.choice(len(self.priorities), batch_size, p=sample_probabilities)
		
		samples = [self.memory[i] for i in sample_indices]
		weights = self._get_importance(sample_probabilities[sample_indices], beta)
		return samples, sample_indices, weights

	def has_enough_samples(self, batch_size: int) -> bool:
		"""
		Returns True if the number of transitions in memory is able fill at least one batch
		of training samples.
		"""
		return len(self.memory) >= batch_size

	def __len__(self) -> int:
		return len(self.memory)

	def update_priorities(self, indices: np.ndarray, errors: np.ndarray, offset: float = 1e-5):
		"""
		Updates the priorities associated with particular samples
		"""
		for i, error in zip(indices, errors):
			self.priorities[i] = error + offset