#!/bin/bash

mkdir -p "$HOME/.config/containers"

cat << EOF > "$HOME/.config/containers/storage.conf"
[storage]
driver = "overlay"
runroot = "/dev/shm/$USER/runroot"
graphroot = "/dev/shm/$USER/root"

[storage.options.overlay]
mount_program = "/usr/bin/fuse-overlayfs-1.13"
EOF