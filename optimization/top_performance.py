# Run command: OMP_NUM_THREADS=1 python3 top_performance.py

import numpy as np
import scipy
import os
import timeit

M = 1024
K = 1024
N = 1024

A = np.random.randn(M, K)
B = np.random.randn(K, N)
C = np.random.randn(M, N)
alpha = 0.2
beta = 0.8
ref_seconds_list = timeit.repeat('D = scipy.linalg.blas.dgemm(alpha, A, B, beta, C)', globals={**globals(), **locals()}, number=1, repeat=10)
ref_seconds_list = [round(x, 6) for x in ref_seconds_list]

print(f"Reference seconds with {os.environ.get('OMP_NUM_THREADS')} threads: {ref_seconds_list}")
