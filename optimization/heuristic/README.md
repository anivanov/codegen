To run the cluster simply execute:
`sbatch -N <number of nodes> -t <time to run the cluster> cluster.sh`
Then read the log file produced by Slurm `slurm-<job id>.out` and find the line `head_node_ip=<head node ip>`. To run the benchmarks execute:
`RAY_HEAD=<head ip address from slurm log> python3 applicability_tests.py`