# Description: A simple test to check if Ray is working properly.
# use by running python3 simple_test.py <cluster_ip_address>

import ray
import sys
ray.init(address=f"ray://{sys.argv[1]}:10001")

@ray.remote
def f(x):
    return x * x

futures = [f.remote(i) for i in range(4)]
print(ray.get(futures)) # [0, 1, 4, 9]
