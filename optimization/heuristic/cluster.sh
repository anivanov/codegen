#!/bin/bash
# shellcheck disable=SC2206
#SBATCH --job-name=ray_search
#SBATCH --ntasks-per-node=1
#SBATCH --account=g34
set -x

convert_to_seconds() {
    input=$1

    # Initialize variables
    days=0
    hours=0
    minutes=0
    seconds=0

    # Splitting the input by "-"
    IFS='-' read -r part1 part2 <<< "$input"

    # If part1 contains ":" then it has hours:minutes:seconds format
    if [[ $part1 == *:* ]]; then
        time=$part1
        # Splitting the time by ":"
        IFS=':' read -r hours minutes seconds <<< "$time"
    else
        # Extracting days from part1
        days=$part1

        # If part2 is not empty, then it has hours:minutes:seconds format
        if [[ ! -z "$part2" ]]; then
            time=$part2
            # Splitting the time by ":"
            IFS=':' read -r hours minutes seconds <<< "$time"
        fi
    fi

    # Calculating total seconds
    total_seconds=$(( (days * 86400) + (hours * 3600) + (minutes * 60) + seconds ))

    echo $total_seconds
}

# Getting the node names
nodes=$(scontrol show hostnames "$SLURM_JOB_NODELIST")
nodes_array=($nodes)

head_node=${nodes_array[0]}
head_node_ip=$(srun --nodes=1 --ntasks=1 -w "$head_node" hostname --ip-address)

# if we detect a space character in the head node IP, we'll
# convert it to an ipv4 address. This step is optional.
if [[ "$head_node_ip" == *" "* ]]; then
IFS=' ' read -ra ADDR <<<"$head_node_ip"
if [[ ${#ADDR[0]} -gt 16 ]]; then
  head_node_ip=${ADDR[1]}
else
  head_node_ip=${ADDR[0]}
fi
echo "IPV6 address detected. We split the IPV4 address as $head_node_ip"
fi

num_worker_per_node="1"
port=6379
ip_head=$head_node_ip:$port
export ip_head
echo "IP Head: $ip_head"

echo "Starting HEAD at $head_node"
srun --nodes=1 --ntasks=1 -w "$head_node" \
    ray start --head --node-ip-address="$head_node_ip" --port=$port \
    --num-cpus "64" --num-gpus "0" --block &

# optional, though may be useful in certain versions of Ray < 1.0.
sleep 10

# number of nodes other than the head node
worker_num=$((SLURM_JOB_NUM_NODES - 1))
for ((i = 1; i <= worker_num; i++)); do
    node_i=${nodes_array[$i]}
    # Fetches the number of numa nodes in the current node
    srun --nodes=1 --ntasks=1 -w "$node_i" \
        ./launch_ray_on_node.sh "$ip_head" &
    sleep 5
done

input=$(squeue -j $SLURM_JOB_ID -h --Format TimeLimit)
seconds=$(convert_to_seconds "$input")

sleep $seconds
