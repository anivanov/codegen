#!/bin/bash
set -x

convert_to_seconds() {
    input=$1

    # Initialize variables
    days=0
    hours=0
    minutes=0
    seconds=0

    # Splitting the input by "-"
    IFS='-' read -r part1 part2 <<< "$input"

    # If part1 contains ":" then it has hours:minutes:seconds format
    if [[ $part1 == *:* ]]; then
        time=$part1
        # Splitting the time by ":"
        IFS=':' read -r hours minutes seconds <<< "$time"
    else
        # Extracting days from part1
        days=$part1

        # If part2 is not empty, then it has hours:minutes:seconds format
        if [[ ! -z "$part2" ]]; then
            time=$part2
            # Splitting the time by ":"
            IFS=':' read -r hours minutes seconds <<< "$time"
        fi
    fi

    # Calculating total seconds
    total_seconds=$(( (days * 86400) + (hours * 3600) + (minutes * 60) + seconds ))

    echo $total_seconds
}


ip_head=$1
# Fetches the physical CPUs in each NUMA domains

# Fetches the number of numa nodes in the current node
numa_nodes=$(lscpu | grep "NUMA node(s)" | awk '{print $3}')
echo "NUMA nodes: $numa_nodes"

# Fetches the physical CPU cores in each NUMA node
# 'NUMA node$n CPU(s): ${CPU_CORES},_' is the pattern we are looking for
# where n is the numa node number
# CPU_CORES is the number of physical CPU cores in the numa node
num_worker_per_node="1"
for ((n = 0; n < numa_nodes; n++)); do
    # We are using awk to extract the CPU_CORES
    #
    # Example:
    # NUMA node0 CPU(s): 0-7,16-23
    # NUMA node1 CPU(s): 8-15,24-31
    #
    # In the above example, we have 2 numa nodes with 8 physical CPU cores each
    # We are extracting the CPU cores using awk
    # awk '{print $3}' will print the 3rd column
    numa_cores=$(lscpu | grep "NUMA node$n" | grep "CPU(s):" | awk '{print $4}' | awk -F, '{print $1}')
    start_core=$(echo $numa_cores | awk -F- '{print $1}')
    end_core=$(echo $numa_cores | awk -F- '{print $2}')
    echo "NUMA node$n CPU(s): start $start_core, end $end_core"
    # For each core in the NUMA node, we start a worker
    numactl --all -N $n -m $n -C $numa_cores \
            ray start --address "$ip_head" \
            --num-cpus $(($end_core - $start_core + 1)) --num-gpus "0" --block &
    sleep 5
    # for ((j = $start_core; j <= $end_core; j++)); do
    #     echo "Starting worker $j in NUMA node$n"
    #     numactl --all -N $n -m $n -C $j \
    #         ray start --address "$ip_head" \
    #         --num-cpus ${num_worker_per_node} --num-gpus 0 --block &
    #     sleep 5
    #     # srun --nodes=1 --ntasks=1 -w "$node_i" \
    #     #     ray start --address "$ip_head" \
    #     #     --num-cpus ${num_worker_per_node} --block &
    #     # sleep 5
    # done
done


input=$(squeue -j $SLURM_JOB_ID -h --Format TimeLimit)
seconds=$(convert_to_seconds "$input")

sleep $seconds