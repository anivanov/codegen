# Description: This script is used to check the available resources in the Ray cluster.
# use by running python3 resources.py <cluster_ip_address>
import ray
import sys

ray.init(address=f"ray://{sys.argv[1]}:10001")

print(ray.available_resources())
print(ray.cluster_resources())

