#include <cstdio>
#include <vector>
#include <time.h>
#if !defined(TOC)
static inline double elapsed_ms(struct timespec start) {
    struct timespec end;
    clock_gettime(CLOCK_MONOTONIC, &end);
    return (end.tv_sec - start.tv_sec) * 1e3 + (end.tv_nsec - start.tv_nsec) / 1e6;
}
#define TIC(name) struct timespec dt_ ##name; clock_gettime(CLOCK_MONOTONIC, &dt_ ##name)
#define TOC(name) elapsed_ms(dt_ ## name)
#endif

#include "blis/blis.h"
// #include "cblas.h"

#ifndef _M
#define _M 1024
#endif
#ifndef _N
#define _N 1024
#endif
#ifndef _K
#define _K 1024
#endif


int main() {
    const int M = _M;
    const int N = _N;
    const int K = _K;

    float alignment = 4096;

    float *A = (float*) aligned_alloc(alignment, M * K * sizeof(float));
    float *B = (float*) aligned_alloc(alignment, N * K * sizeof(float));
    float *C = (float*) aligned_alloc(alignment, M * N * sizeof(float));

    for (int i = 0; i < M * K; i++) {
        A[i] = ((float)rand() / (float)RAND_MAX) - 0.5;
    }
    for (int i = 0; i < K * N; i++) {
        B[i] = ((float)rand() / (float)RAND_MAX) - 0.5;
    }
    for (int i = 0; i < M * N; i++) {
        C[i] = 0;
    }

    float alpha = 1.0f;
    float beta = 0.0f;
    
    int warmup = 1;
    int repeats = 5;
    int number = 1;
    std::vector<double> times;
    for (int r = 0; r < warmup + repeats; r++) {
        TIC(1);
        for (int num = 0; num < number; num++) {
            bli_sgemm(BLIS_TRANSPOSE, BLIS_NO_TRANSPOSE, M, N, K,
              &alpha, A, M, 1, B, N, 1, &beta, C, N, 1);
        }
        double time = TOC(1) / number;
        times.push_back(time);
    }

    double min_time = 1e10;
    double max_time = 0;
    double avg_time = 0;
    for (int i = warmup; i < warmup + repeats; i++) {
        if (times[i] < min_time) {
            min_time = times[i];
        }
        if (times[i] > max_time) {
            max_time = times[i];
        }
        avg_time += times[i];
    }
    avg_time /= repeats;
    printf("Time [ms] min: %.3f, avg: %.3f, max: %.3f all: [ ", min_time, avg_time, max_time);
    for (double t : times) { printf("%.3f ", t); }
    printf("] gflop/s: %.3f\n", 1.0 / ((avg_time / 1e3) * 1e9) * M * N * K);

    free(A);
    free(B);
    free(C);

    return 0;
}