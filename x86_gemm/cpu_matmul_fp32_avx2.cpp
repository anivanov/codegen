
// clear && clang++ cpu_matmul_fp32_avx2.cpp -std=c++20 -fopenmp -march=native -O3 && numactl -m 0 -N 0 -C 0-17 ./a.out

#include <cstdio>
#include <vector>
#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <immintrin.h>
#include <emmintrin.h>

#include <time.h>
#if !defined(TOC)
static inline double elapsed_ms(struct timespec start) {
    struct timespec end;
    clock_gettime(CLOCK_MONOTONIC, &end);
    return (end.tv_sec - start.tv_sec) * 1e3 + (end.tv_nsec - start.tv_nsec) / 1e6;
}
#define TIC(name) struct timespec dt_ ##name; clock_gettime(CLOCK_MONOTONIC, &dt_ ##name)
#define TOC(name) elapsed_ms(dt_ ## name)
#endif


constexpr int VEC_LEN = 8;  // sizeof(__m256) / sizeof(float)


struct Config {
    constexpr Config(int M, int N, int K, int Nc, int Kc, int Mc, int Nr, int Mr, int sMr, int sNr, int sKc, int sK, int sM, int sN)
        : M(M), N(N), K(K), Nc(Nc), Kc(Kc), Mc(Mc), Nr(Nr), Mr(Mr), sMr(sMr), sNr(sNr), sKc(sKc), sK(sK), sM(sM), sN(sN) {}

    const int M, N, K;
    const int Nc;
    const int Kc;
    const int Mc;
    const int Nr;
    const int Mr;

    // strides
    const int sMr;
    const int sNr;
    const int sKc;
    const int sK, sM, sN;
};

template <Config x> 
// void __attribute__((always_inline)) inline
void __attribute__ ((noinline))
microkernel(float* __restrict__ c, float* __restrict__ a, float* __restrict__ b) {
    constexpr int paddedNv = (x.Nr - 1) / VEC_LEN + 1;
    constexpr int Nv = x.Nr / VEC_LEN;
    constexpr int remNr = x.Nr % VEC_LEN;
    __m256i mask = _mm256_setr_epi32(
        (remNr > 0) << 31, (remNr > 1) << 31, (remNr > 2) << 31, (remNr > 3) << 31,
        (remNr > 4) << 31, (remNr > 5) << 31, (remNr > 6) << 31, (remNr > 7) << 31
    );
    __m256 cv[x.Mr][paddedNv];
    for (int i = 0; i < x.Mr; i++) {
        for (int jv = 0; jv < paddedNv; jv++) {
            cv[i][jv] = _mm256_setzero_ps();
        }
    }
    __m256 bv[paddedNv];
    _Pragma("GCC unroll 4")
    for (int pr = 0; pr < x.Kc; pr++) {
        for (int jv = 0; jv < paddedNv; jv++) {
            int j = jv * VEC_LEN;
            bv[jv] = _mm256_load_ps(&b[pr * x.sNr + j]);
        }
        _mm_prefetch(&a[(pr + 1) * x.sMr], _MM_HINT_T0);
        for (int i = 0; i < x.Mr; i++) {
            __m256 av = _mm256_broadcast_ss(&a[pr * x.sMr + i]);
            for (int jv = 0; jv < paddedNv; jv++) {
                cv[i][jv] = _mm256_fmadd_ps(av, bv[jv], cv[i][jv]);
            }
        }
    }
    for (int i = 0; i < x.Mr; i++) {
        _mm_prefetch(&c[i * x.sN], _MM_HINT_T0);
    }
    for (int i = 0; i < x.Mr; i++) {
        for (int jv = 0; jv < Nv; jv++) {
            int j = jv * VEC_LEN;
            _mm256_storeu_ps(&c[i * x.sN + j], _mm256_add_ps(_mm256_loadu_ps(&c[i * x.sN + j]), cv[i][jv]));
        }
        if (remNr != 0) {
            int jv = Nv;
            int j = jv * VEC_LEN;
            _mm256_maskstore_ps(&c[i * x.sN + j], mask, _mm256_add_ps(_mm256_maskload_ps(&c[i * x.sN + j], mask), cv[i][jv]));
        }
    }
}

template <int Nc, int Kc, int Nr, int K, int sKc, int sN, int sNr>
// void __attribute__((always_inline)) inline 
void __attribute__ ((noinline))
pack(float* __restrict__ packed_b, float* __restrict__ b) {
    _Pragma("omp parallel")
    {
    _Pragma("omp for nowait")
    for (int jr = 0; jr < Nc / Nr * Nr; jr += Nr) {
        for (int pr = 0; pr < Kc; pr++) {
            for (int j = 0; j < Nr; j++) {
                packed_b[jr * sKc + pr * sNr + j] = b[(jr + j) + pr * sN];
            }
            for (int j = 0; j < Nr / VEC_LEN * VEC_LEN; j += VEC_LEN) {
                _mm256_storeu_ps(&packed_b[jr * sKc + pr * sNr + j], _mm256_loadu_ps(&b[(jr + j) + pr * sN]));
            }
            constexpr int rem = Nr % VEC_LEN;
            if (rem) {
                int j = Nr / VEC_LEN * VEC_LEN;
                __m256i mask = _mm256_setr_epi32(
                    (rem > 0) << 31, (rem > 1) << 31, (rem > 2) << 31, (rem > 3) << 31,
                    (rem > 4) << 31, (rem > 5) << 31, (rem > 6) << 31, (rem > 7) << 31
                );
                _mm256_maskstore_ps(&packed_b[jr * sKc + pr * sNr + j], mask, _mm256_maskload_ps(&b[(jr + j) + pr * sN], mask));
            }
        }
    }
    constexpr int remNr = Nc % Nr;
    constexpr int remNrVec = remNr % VEC_LEN;
    __m256i mask = _mm256_setr_epi32(
        (remNrVec > 0) << 31, (remNrVec > 1) << 31, (remNrVec > 2) << 31, (remNrVec > 3) << 31,
        (remNrVec > 4) << 31, (remNrVec > 5) << 31, (remNrVec > 6) << 31, (remNrVec > 7) << 31
    );
    _Pragma("omp single nowait")
    if (remNr != 0) {
        int jr = Nc / Nr * Nr;
        for (int pr = 0; pr < Kc; pr++) {
            // for (int j = 0; j < remNr; j++) {
            //     packed_b[jr * sKc + pr * sNr + j] = b[(jr + j) + pr * sN];
            // }
            for (int j = 0; j < remNr / VEC_LEN * VEC_LEN; j += VEC_LEN) {
                _mm256_storeu_ps(&packed_b[jr * sKc + pr * sNr + j], _mm256_loadu_ps(&b[(jr + j) + pr * sN]));
            }
            if (remNrVec) {
                int j = remNr / VEC_LEN * VEC_LEN;
                _mm256_maskstore_ps(&packed_b[jr * sKc + pr * sNr + j], mask, _mm256_maskload_ps(&b[(jr + j) + pr * sN], mask));
            }
        }
    }
    } // pragma omp parallel
}


template <Config x>
void __attribute__((always_inline)) inline
matmul_loop4_body(float* __restrict__ c, float* __restrict__ pa, float* __restrict__ pb) {
    for (int ir = 0; ir < x.Mc / x.Mr * x.Mr; ir += x.Mr) {
        microkernel<x>(&c[ir * x.sN], &pa[ir * x.sKc], pb);
    }
    constexpr int remMr = x.Mc % x.Mr;
    if (remMr != 0) {
        int ir = x.Mc / x.Mr * x.Mr;
        constexpr Config paddedConf = Config(remMr, x.N, x.K, x.Nc, x.Kc, remMr, x.Nr, remMr, x.sMr, x.sNr, x.sKc, x.sK, x.sM, x.sN);
        microkernel<paddedConf>(&c[ir * x.sN], &pa[ir * x.sKc], pb);
    }
}


template <Config x>
void __attribute__((always_inline)) inline 
matmul_loop3_body(float* __restrict__ c, float* __restrict__ a, float* __restrict__ pa, float* __restrict__ pb) {
    // pack A: [Mc, Kc] = [Mc/Mr, Mr, Kc] -> [Mc/Mr, Kc, Mr]
    pack<x.Mc, x.Kc, x.Mr, x.K, x.sKc, x.sM, x.sMr>(pa, a);
    _Pragma("omp parallel")
    {
    _Pragma("omp for nowait")
    for (int jr = 0; jr < x.Nc / x.Nr * x.Nr; jr += x.Nr) {
        matmul_loop4_body<x>(&c[jr], pa, &pb[jr * x.sKc]);
    }
    constexpr int remNr = x.Nc % x.Nr;
    _Pragma("omp single nowait")
    if (remNr != 0) {
        int jr = x.Nc / x.Nr * x.Nr;
        constexpr Config paddedConf = Config(x.M, remNr, x.K, remNr, x.Kc, x.Mc, remNr, x.Mr, x.sMr, x.sNr, x.sKc, x.sK, x.sM, x.sN);
        matmul_loop4_body<paddedConf>(&c[jr], pa, &pb[jr * x.sKc]);
    }
    }
}


template <Config x>
void __attribute__((always_inline)) inline 
matmul_loop2_body(float* __restrict__ c, float* __restrict__ a, float* __restrict__ b, float* __restrict__ pa, float* __restrict__ pb) {
    // pack B: [Nc, Kc] = [Nc/Nr, Nr, Kc] -> [Nc/Nr, Kc, Nr]
    pack<x.Nc, x.Kc, x.Nr, x.K, x.sKc, x.sN, x.sNr>(pb, b);
    for (int ic = 0; ic < x.M / x.Mc * x.Mc; ic += x.Mc) {
        matmul_loop3_body<x>(&c[ic * x.sN], &a[ic], pa, pb);
    }
    constexpr int remMc = x.M % x.Mc;
    if (remMc != 0) {
        int ic = x.M / x.Mc * x.Mc;
        constexpr Config paddedConf = Config(remMc, x.N, x.K, x.Nc, x.Kc, remMc, x.Nr, x.Mr, x.sMr, x.sNr, x.sKc, x.sK, x.sM, x.sN);
        matmul_loop3_body<paddedConf>(&c[ic * x.sN], &a[ic], pa, pb);
    }
}


template <Config x>
void __attribute__((always_inline)) inline 
matmul_loop1_body(float* __restrict__ c, float* __restrict__ a, float* __restrict__ b, float* __restrict__ pa, float* __restrict__ pb) {
    for (int pc = 0; pc < x.K / x.Kc * x.Kc; pc += x.Kc) {
        matmul_loop2_body<x>(c, &a[pc * x.sM], &b[pc * x.sN], pa, pb);
    }
    constexpr int remKc = x.K % x.Kc;
    if (remKc != 0) {
        int pc = x.K / x.Kc * x.Kc;
        constexpr Config paddedConf = Config(x.M, x.N, remKc, x.Nc, remKc, x.Mc, x.Nr, x.Mr, x.sMr, x.sNr, x.sKc, x.sK, x.sM, x.sN);
        matmul_loop2_body<paddedConf>(c, &a[pc * x.sM], &b[pc * x.sN], pa, pb);
    }
}


template <Config x>
void
compute_matmul(float* __restrict__ a, float* __restrict__ b, float* __restrict__ c, float* __restrict__ pa, float* __restrict__ pb) {
    for (int i = 0; i < x.M * x.N; i++) {
        c[i] = 0;
    }

    for (int jc = 0; jc < x.N / x.Nc * x.Nc; jc += x.Nc) {
        matmul_loop1_body<x>(&c[jc], a, &b[jc], pa, pb);
    }
    constexpr int remNc = x.N % x.Nc;
    if (remNc != 0) {
        constexpr Config paddedConf = Config(x.M, remNc, x.K, remNc, x.Kc, x.Mc, x.Nr, x.Mr, x.sMr, x.sNr, x.sKc, x.sK, x.sM, x.sN);
        int jc = x.N / x.Nc * x.Nc;
        matmul_loop1_body<paddedConf>(&c[jc], a, &b[jc], pa, pb);
    }
}


#ifndef _M
#define _M 1024
#endif
#ifndef _N
#define _N 1024
#endif
#ifndef _K
#define _K 1024
#endif
#ifndef _Nc
#define _Nc 1024
#endif
#ifndef _Kc
#define _Kc 512
#endif
#ifndef _Mc
#define _Mc 256
#endif
#ifndef _Nr
#define _Nr 16
#endif
#ifndef _Mr
#define _Mr 4
#endif


int main() {
    constexpr int M = _M;
    constexpr int N = _N;
    constexpr int K = _K;
    constexpr int Nc = _Nc;
    constexpr int Kc = _Kc;
    constexpr int Mc = _Mc;
    constexpr int Nr = _Nr;
    constexpr int Mr = _Mr;

    printf("Config: M %d N %d K %d Nc %d Kc %d Mc %d Nr %d Mr %d\n", M, N, K, Nc, Kc, Mc, Nr, Mr);

    constexpr int alignment = 4096;

    float *a = (float *)aligned_alloc(alignment, sizeof(float) * M * K);
    float *b = (float *)aligned_alloc(alignment, sizeof(float) * K * N);
    float *c = (float *)aligned_alloc(alignment, sizeof(float) * M * N);

    // initialize
    for (int i = 0; i < M * K; i++) {
        a[i] = ((float)rand() / (float)RAND_MAX) - 0.5;
    }
    for (int i = 0; i < K * N; i++) {
        b[i] = ((float)rand() / (float)RAND_MAX) - 0.5;
    }
    for (int i = 0; i < M * N; i++) {
        c[i] = 0;
    }

    constexpr Config conf(M, N, K, Nc, Kc, Mc, Nr, Mr, Mr, Nr, Kc, K, M, N);

    constexpr int paddedMc = ((Mc - 1) / Mr + 1) * Mr;
    constexpr int paddedNc = ((Nc - 1) / Nr + 1) * Nr;

    float* pb = (float*)aligned_alloc(alignment, sizeof(float) * Kc * paddedNc);
    float* pa = (float*)aligned_alloc(alignment, sizeof(float) * paddedMc * Kc);

    // launch
    // int repeats = 10;
    // int warmup = repeats / 3;
    // int number = 10;
    int warmup = 1;
    int repeats = 5;
    int number = 1;
    std::vector<double> times;
    for (int r = 0; r < warmup + repeats; r++) {
        TIC(1);
        for (int num = 0; num < number; num++) {
            compute_matmul<conf>(a, b, c, pa, pb);
        }
        double time = TOC(1) / number;
        times.push_back(time);
    }

    double min_time = 1e10;
    double max_time = 0;
    double avg_time = 0;
    for (int i = warmup; i < warmup + repeats; i++) {
        if (times[i] < min_time) {
            min_time = times[i];
        }
        if (times[i] > max_time) {
            max_time = times[i];
        }
        avg_time += times[i];
    }
    avg_time /= repeats;
    printf("Time [ms] min: %.3f, avg: %.3f, max: %.3f all: [ ", min_time, avg_time, max_time);
    for (double t : times) { printf("%.3f ", t); }
    printf("] gflop/s: %.3f\n", 1.0 / ((avg_time / 1e3) * 1e9) * M * N * K);
    // (random) verify
    int elems_to_verify = 10000;
    for (int i = 0; i < elems_to_verify; i++) {
        int m = rand() % M;
        int n = rand() % N;
        float sum = 0;
        for (int k = 0; k < K; k++) {
            sum += (float)a[m + k * M] * (float)b[n + k * N];
        }
        float eps = 1e-3;
        float val = (float)c[m * N + n];
        if (fabs(val - sum) > eps && fabs(val - sum)/(fabs(sum)+eps) > eps) {
            printf("Error at (%d, %d): %f != %f\n", m, n, c[m * N + n], sum);
            return 1;
        }
    }

    free(pa);
    free(pb);
    free(a);
    free(b);
    free(c);
}
