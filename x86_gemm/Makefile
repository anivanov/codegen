

NT ?= $(shell nproc)
NT1 := $(shell expr $(NT) - 1)

NUMACTL_PRESENT := $(shell command -v numactl > /dev/null 2>&1 && echo 1 || echo 0)
ifeq ($(NUMACTL_PRESENT), 1)
    NC1 := numactl -m 0 -N 0 -C 0-0
	NCA := numactl -m 0 -N 0 -C 0-$(NT1)
else
    NC1 := OMP_NUM_THREADS=1
	NCA := OMP_NUM_THREADS=$(NT)
endif

AFF1 := GOMP_CPU_AFFINITY=0-0
AFFA := GOMP_CPU_AFFINITY=0-$(NT1)


CXX := g++
ROOTBIN := $(shell dirname $$(which cc))
ROOTINC := $(ROOTBIN)/../include
ROOTLIB := $(ROOTBIN)/../lib
CFLAGS := -std=c++20 -fopenmp -march=native -O3 -Wfatal-errors -I$(ROOTINC) -L$(ROOTLIB)

M ?= 1024
N ?= 1024
K ?= 1024

Nc ?= 4080
Kc ?= 256
Mc ?= 168
Nr ?= 16
Mr ?= 6

all: single multi

single: my-single mkl-single blis-single openblas-single

multi: my mkl blis openblas

my-all-single:
	$(CXX) cpu_matmul_fp32_avx2.cpp -D_M=$M -D_N=$N -D_K=$K -D_Nc=320 -D_Kc=320 -D_Mc=12 -D_Nr=16 -D_Mr=4 $(CFLAGS) && $(AFF1) $(NC1) ./a.out
	$(CXX) cpu_matmul_fp32_avx2.cpp -D_M=$M -D_N=$N -D_K=$K -D_Nc=4080 -D_Kc=256 -D_Mc=144 -D_Nr=16 -D_Mr=6 $(CFLAGS) && $(AFF1) $(NC1) ./a.out
	$(CXX) cpu_matmul_fp32_avx2.cpp -D_M=$M -D_N=$N -D_K=$K -D_Nc=4080 -D_Kc=256 -D_Mc=168 -D_Nr=16 -D_Mr=6 $(CFLAGS) && $(AFF1) $(NC1) ./a.out
	$(CXX) cpu_matmul_fp32_avx2.cpp -D_M=$M -D_N=$N -D_K=$K -D_Nc=1024 -D_Kc=320 -D_Mc=320 -D_Nr=16 -D_Mr=6 $(CFLAGS) && $(AFF1) $(NC1) ./a.out
	$(CXX) cpu_matmul_fp32_avx2.cpp -D_M=$M -D_N=$N -D_K=$K -D_Nc=1024 -D_Kc=320 -D_Mc=320 -D_Nr=16 -D_Mr=4 $(CFLAGS) && $(AFF1) $(NC1) ./a.out
	$(CXX) cpu_matmul_fp32_avx2.cpp -D_M=$M -D_N=$N -D_K=$K -D_Nc=1024 -D_Kc=320 -D_Mc=320 -D_Nr=16 -D_Mr=5 $(CFLAGS) && $(AFF1) $(NC1) ./a.out
	$(CXX) cpu_matmul_fp32_avx2.cpp -D_M=$M -D_N=$N -D_K=$K -D_Nc=320 -D_Kc=320 -D_Mc=12 -D_Nr=16 -D_Mr=16 $(CFLAGS) && $(AFF1) $(NC1) ./a.out
	$(CXX) cpu_matmul_fp32_avx2.cpp -D_M=$M -D_N=$N -D_K=$K -D_Nc=1024 -D_Kc=512 -D_Mc=256 -D_Nr=16 -D_Mr=4 $(CFLAGS) && $(AFF1) $(NC1) ./a.out
	$(CXX) cpu_matmul_fp32_avx2.cpp -D_M=$M -D_N=$N -D_K=$K -D_Nc=320 -D_Kc=320 -D_Mc=12 -D_Nr=16 -D_Mr=4 $(CFLAGS) && $(AFF1) $(NC1) ./a.out
	$(CXX) cpu_matmul_fp32_avx2.cpp -D_M=$M -D_N=$N -D_K=$K -D_Nc=320 -D_Kc=256 -D_Mc=16 -D_Nr=16 -D_Mr=4 $(CFLAGS) && $(AFF1) $(NC1) ./a.out
	$(CXX) cpu_matmul_fp32_avx2.cpp -D_M=$M -D_N=$N -D_K=$K -D_Nc=320 -D_Kc=256 -D_Mc=8 -D_Nr=16 -D_Mr=4 $(CFLAGS) && $(AFF1) $(NC1) ./a.out
	$(CXX) cpu_matmul_fp32_avx2.cpp -D_M=$M -D_N=$N -D_K=$K -D_Nc=320 -D_Kc=256 -D_Mc=12 -D_Nr=16 -D_Mr=4 $(CFLAGS) && $(AFF1) $(NC1) ./a.out
	$(CXX) cpu_matmul_fp32_avx2.cpp -D_M=$M -D_N=$N -D_K=$K -D_Nc=1024 -D_Kc=1024 -D_Mc=1024 -D_Nr=24 -D_Mr=4 $(CFLAGS) && $(AFF1) $(NC1) ./a.out
	$(CXX) cpu_matmul_fp32_avx2.cpp -D_M=$M -D_N=$N -D_K=$K -D_Nc=320 -D_Kc=320 -D_Mc=12 -D_Nr=24 -D_Mr=4 $(CFLAGS) && $(AFF1) $(NC1) ./a.out

my-all-multi:
	$(CXX) cpu_matmul_fp32_avx2.cpp -D_M=$M -D_N=$N -D_K=$K -D_Nc=4080 -D_Kc=256 -D_Mc=144 -D_Nr=16 -D_Mr=6 $(CFLAGS) && $(AFFA) $(NCA) ./a.out
	$(CXX) cpu_matmul_fp32_avx2.cpp -D_M=$M -D_N=$N -D_K=$K -D_Nc=4080 -D_Kc=256 -D_Mc=168 -D_Nr=16 -D_Mr=6 $(CFLAGS) && $(AFFA) $(NCA) ./a.out
	$(CXX) cpu_matmul_fp32_avx2.cpp -D_M=$M -D_N=$N -D_K=$K -D_Nc=1024 -D_Kc=320 -D_Mc=320 -D_Nr=16 -D_Mr=6 $(CFLAGS) && $(AFFA) $(NCA) ./a.out
	$(CXX) cpu_matmul_fp32_avx2.cpp -D_M=$M -D_N=$N -D_K=$K -D_Nc=1024 -D_Kc=1024 -D_Mc=512 -D_Nr=16 -D_Mr=4 $(CFLAGS) && $(AFFA) $(NCA) ./a.out
	$(CXX) cpu_matmul_fp32_avx2.cpp -D_M=$M -D_N=$N -D_K=$K -D_Nc=1024 -D_Kc=1024 -D_Mc=1024 -D_Nr=16 -D_Mr=4 $(CFLAGS) && $(AFFA) $(NCA) ./a.out
	$(CXX) cpu_matmul_fp32_avx2.cpp -D_M=$M -D_N=$N -D_K=$K -D_Nc=1024 -D_Kc=1024 -D_Mc=1024 -D_Nr=24 -D_Mr=4 $(CFLAGS) && $(AFFA) $(NCA) ./a.out
	$(CXX) cpu_matmul_fp32_avx2.cpp -D_M=$M -D_N=$N -D_K=$K -D_Nc=1024 -D_Kc=1024 -D_Mc=1024 -D_Nr=24 -D_Mr=3 $(CFLAGS) && $(AFFA) $(NCA) ./a.out
	$(CXX) cpu_matmul_fp32_avx2.cpp -D_M=$M -D_N=$N -D_K=$K -D_Nc=1024 -D_Kc=1024 -D_Mc=1024 -D_Nr=24 -D_Mr=3 $(CFLAGS) && $(AFFA) $(NCA) ./a.out
	$(CXX) cpu_matmul_fp32_avx2.cpp -D_M=$M -D_N=$N -D_K=$K -D_Nc=1024 -D_Kc=1024 -D_Mc=512 -D_Nr=16 -D_Mr=4 $(CFLAGS) && $(AFFA) $(NCA) ./a.out

mkl-single:
	$(CXX) mkl_sgemm.cpp -D_M=$M -D_N=$N -D_K=$K $(CFLAGS) -lmkl_rt
	$(AFF1) $(NC1) ./a.out

mkl:
	$(CXX) mkl_sgemm.cpp -D_M=$M -D_N=$N -D_K=$K $(CFLAGS) -I$$SCRATCH/conda/envs/main/include/ -L$$SCRATCH/conda/envs/main/lib/ -lmkl_rt
	$(AFFA) $(NCA) ./a.out

my-single:
	$(CXX) cpu_matmul_fp32_avx2.cpp -D_M=$M -D_N=$N -D_K=$K -D_Nc=$(Nc) -D_Kc=$(Kc) -D_Mc=$(Mc) -D_Nr=$(Nr) -D_Mr=$(Mr) $(CFLAGS)
	objdump -d ./a.out > objdump.txt
	$(AFF1) $(NC1) ./a.out

my:
	$(CXX) cpu_matmul_fp32_avx2.cpp -D_M=$M -D_N=$N -D_K=$K -D_Nc=$(Nc) -D_Kc=$(Kc) -D_Mc=$(Mc) -D_Nr=$(Nr) -D_Mr=$(Mr) $(CFLAGS)
	$(AFFA) $(NCA) ./a.out

openblas-single:
	$(CXX) openblas_sgemm.cpp -D_M=$M -D_N=$N -D_K=$K $(CFLAGS) -lopenblas
	$(AFF1) $(NC1) ./a.out

openblas:
	$(CXX) openblas_sgemm.cpp -D_M=$M -D_N=$N -D_K=$K $(CFLAGS) -lopenblas
	$(AFFA) $(NCA) ./a.out

blis-single:
	$(CXX) blis_sgemm.cpp -D_M=$M -D_N=$N -D_K=$K $(CFLAGS) -lblis
	$(AFF1) $(NC1) ./a.out

blis:
	$(CXX) blis_sgemm.cpp -D_M=$M -D_N=$N -D_K=$K $(CFLAGS) -lblis
	$(AFFA) BLIS_NUM_THREADS=$(NT) $(NCA) ./a.out